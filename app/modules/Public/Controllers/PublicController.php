<?php

namespace Public\Controllers;

//use app\controllers\ControllerBase;

class PublicController extends ControllerBase
{

    # set this to containing folder in order to have
    # "index.volt" in top-level namespace folder.
    # Depends on this class having the same name as 
    # toplevel namespace
    protected $_viewBasePath = "modules";

    public function initialize()
    {

        parent::initialize();
    }

    public function indexAction()
    {
    }

    public function page2Action()
    {
    }

}