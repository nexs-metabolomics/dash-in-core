<?php

namespace Public\Controllers\Sections;

use Public\Controllers\ControllerBase;

class SectionsControllerBase extends ControllerBase
{
    public function initialize()
    {
        $this->_viewBasePath .= "/sections";
        parent::initialize();
    }

}