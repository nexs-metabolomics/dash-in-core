<?php

namespace Public\Controllers\Sections;

use App\Library\Utils\UniqueId;
use Public\Helpers\Sections\SearchHelper;

class SearchController extends SectionsControllerBase
{

    /**
     * @var SearchHelper
     */
    private $_helper;

    /**
     * @return SearchHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new SearchHelper();
        }

        return $this->_helper;
    }

    public function indexAction()
    {
    }

    /**
     * Entry point for landing page
     * 
     * @param $page
     *
     * @return void
     */
    public function landingPageAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);
        $page = (int)$page;
        $nrows = 10;

        $action = $this->Btn->getAction();
        if ($action === "reset") {
            $reset = true;
        } elseif ($action === "view") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->localsession->setKeyValue("public_study_id", $studyId);
                $this->response->redirect("/public/studyview/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

            }
        }

        $results = $this->_getHelper()->landingPage($page, $nrows, $reset);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["search_result"]) && $results["search_result"]) {
            $this->view->pagination = $results["search_result"];
        }
    }

    /**
     * Entry point for main search page (page after front page / landing page)
     * 
     * @return void
     */
    public function mainSearchAction()
    {

        $reset = false;
        $action = $this->Btn->getAction();
        if ($action === "reset") {
            $reset = true;
        }
        $results = $this->_getHelper()->mainSearch($reset);

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["search_result"]) && $results["search_result"]) {
            $this->view->search_result = $results["search_result"];
        }

    }

    /**
     * Entry point for view and download study
     * 
     * @return void
     */
    public function studyViewAction()
    {

        $studyId = $this->localsession->getKeyValue("public_study_id");

        $results = $this->_getHelper()->studyDetailedView($studyId);

        if (isset($results["study_details"]) && $results["study_details"]) {
            $this->view->study_details = (object)$results["study_details"];
        }

    }

}