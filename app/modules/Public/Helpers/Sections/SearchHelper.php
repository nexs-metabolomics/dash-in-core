<?php

namespace Public\Helpers\Sections;

use App\Library\Utils\Csv;
use App\Library\Utils\Json;
use App\Library\Utils\JsonException;
use Public\Forms\LandingPageSearchForm;
use Public\Helpers\HelperBase;
use ZipArchive;

class SearchHelper extends HelperBase
{

    /**
     * @var SearchRepository
     */
    private $_repository;

    /**
     * @return SearchRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new SearchRepository();
        }

        return $this->_repository;
    }

    /**
     * @param $reset
     *
     * @return array
     */
    public function landingPage($page, $nrows, $reset)
    {

        $form = new LandingPageSearchForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }

            } else {

            }

        }

        $searchTerm = $this->getSearchTerm("public_studyy_search_simple_search_term", $reset);
        $form->setSearchTerm($searchTerm);

        $result = $this->_getRepository()->getStudyPaginated($page, $nrows, $searchTerm);
        if ($result && $result->items) {
            foreach ($result->items as &$row) {
                try {
                    $row->datasets = Json::jsonDecode($row->datasets);
                } catch (JsonException $e) {
                    $this->flashSession->error($e);
                    $row->supportfiles = false;
                }
                try {
                    $row->supportfiles = Json::jsonDecode($row->supportfiles);

                } catch (JsonException $e) {
                    $this->flashSession->error($e);
                    $row->supportfiles = false;
                }
            }
        }

        return [
            "form"          => $form,
            "search_result" => $result,
        ];
    }

    /**
     * @param $reset
     *
     * @return array
     */
    public function mainSearch($reset = false)
    {

        $form = new LandingPageSearchForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }

            } else {

            }

        }

        $searchTerm = $this->getSearchTerm("public_studyy_search_simple_search_term", $reset);
        $form->setSearchTerm($searchTerm);

        $result = $this->_getRepository()->studySimpleSearch($searchTerm);
        if ($result) {
            foreach ($result as &$row) {
                $row = (object)$row;
            }
        }

        return [
            "form"          => $form,
            "search_result" => $result,
        ];

    }

    /**
     * Business logic for detailed view and download of study and
     *
     * @param $studyId
     *
     * @return array
     */
    public function studyDetailedView($studyId)
    {

        $studyMetadata = $this->_getRepository()->studyDetailedView($studyId);
        if ($studyMetadata) {
            try {
                $studyMetadata["datasets"] = Json::jsonDecode($studyMetadata["datasets"]);
            } catch (JsonException $e) {
                $this->flashSession->error($e);
                $studyMetadata["datasets"] = false;
            }
            try {
                $studyMetadata["supportfiles"] = Json::jsonDecode($studyMetadata["supportfiles"]);

            } catch (JsonException $e) {
                $this->flashSession->error($e);
                $studyMetadata["supportfiles"] = false;
            }
        }

        $action = $this->Btn->getAction();
        if ($action === "downloadstudy") {
            $this->wholestudyExporter($studyId, $studyMetadata);
        }

        return [
            "study_details" => $studyMetadata,
        ];

    }

    /**
     * Download whole study
     * including public support files and all public datasets
     *
     * @param $studyId
     * @param $studyMetadata
     *
     * @return void
     * @throws \App\Library\Utils\StorageException
     */
    public function wholestudyExporter($studyId, $studyMetadata)
    {

        # Turn studyMetadata into a two-column array (name, value)
        $studyArr = [];
        $datasets = [];
        $supportfiles = [];
        foreach ($studyMetadata as $key => $value) {
            if ($key === "datasets") {
                $datasets = $value;
            } elseif ($key === "supportfiles") {
                $supportfiles = $value;
            } else {
                $studyArr[] = ["name" => $key, "value" => $value];
            }
        }
        unset($key, $value);

        # define header
        $header = ["name", "value"];

        # set separator
        $sep = ",";
        $sepNum = (int)$this->request->getPost("sep");
        if ($sepNum === 2) {
            $sep = ";";
        } elseif ($sepNum === 3) {
            $sep = "\t";
        }

        $Csv = new Csv();
        $studyMetaCsv = $Csv->arrayToCsv($studyArr, $sep, $header);

        # ------------------------------------------------------------------
        # create zip
        # ------------------------------------------------------------------
        # filename
        $date = date('Y-m-d His');
        $basename = "study_($studyId)_{$date}";
        $outname = "$basename.zip";

        # zip-archive
        $zip = new ZipArchive();

        $tmpdir = $this->storage->getTempdirLocation();
        $tempZipfilePath = tempnam($tmpdir, "wholestudy_export_");
        $zip->open($tempZipfilePath, ZipArchive::CREATE);
        $zip->addFromString("$basename/studymetadata.csv", $studyMetaCsv);

        # ------------------------------------------------------------------
        # datasets
        # ------------------------------------------------------------------
        $chkDatasets = $this->request->getPost("chk_dataset");
        if ($chkDatasets && $datasets) {

            $selectedDatasets = array_keys($chkDatasets);

            foreach ($datasets as $dkey => $drow) {
                $datasetsId = $drow->dataset_id;
                if (in_array($datasetsId, $selectedDatasets)) {
                    $datasetBasename = $drow->name;
                    if (!strlen($datasetBasename)) {
                        $datasetBasename = "dataset-{$datasetsId}";
                    }
                    $datasetName = $datasetBasename . ".csv";
                    $datasetCsv = $this->_getRepository()->getDatasetAsCsvString($datasetsId, $sep);

                    $zip->addFromString("$basename/data/$datasetName", $datasetCsv);

                }
            }
            unset($dkey, $drow);
        }

        # ------------------------------------------------------------------
        # support files
        # ------------------------------------------------------------------
        $chkFiles = $this->request->getPost("chk_supportfile");
        if ($supportfiles) {

            $selectedFiles = array_keys($chkFiles);

            foreach ($supportfiles as $fkey => $frow) {

                $supportfileId = $frow->supportfile_id;
                if (in_array($supportfileId, $selectedFiles)) {

                    $filename = $frow->name;

                    $params = ["study_id" => $studyId, "type" => "study"];

                    $fileContents = $this->supportFiles->getStudySupportFileContents($params, $supportfileId);
                    $zip->addFromString("$basename/files/$filename", $fileContents);
                }

            }
            unset($fkey, $frow);
        }

        $zip->close();
        $zipfileContents = file_get_contents($tempZipfilePath);
        $this->downloader->send($zipfileContents, $outname, "zip");
    }

}