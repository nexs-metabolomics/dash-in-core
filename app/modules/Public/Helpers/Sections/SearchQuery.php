<?php

namespace Public\Helpers\Sections;

use Public\Helpers\QueryBase;

class SearchQuery extends QueryBase
{

    static string $landing_page_list_studies =
        <<<'EOD'
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.start_date
                 , a.endpoint
                 , a.objectives
                 , a.conclusion
                 , a.exclusion
                 , a.inclusion
                 , a.institute
                 , a.country_id
                 , b.name                                                      AS country_name
                 , a.consortium_id
                 , c.name                                                      AS consortium_name
                 , a.published
                 , a.researchdesign_id
                 , d.name                                                      AS researchdesign_name
                 , a.num_treat
                 , a.num_factor
                 , a.num_arm
                 , a.researchdesign_text
                 , a.num_volun
                 , a.num_volun_terminated
                 , a.recruit_start_year
                 , a.recruit_end_year
                 , a.blinding
                 , CASE WHEN a.blinding = 1 THEN 'Yes' ELSE 'No' END           AS blinding
                 , CASE WHEN a.blinding = 1 THEN a.blinding_method ELSE '' END AS blinding_method
                 , a.owner_org
                 , a.owner_user
                 , a.created_at
                 , a.updated_at
                 , a.details
            FROM dashin.study                    a
                 LEFT JOIN admin.country         b ON a.country_id = b.country_id
                 LEFT JOIN dashin.consortium     c ON a.consortium_id = c.consortium_id
                 LEFT JOIN dashin.researchdesign d ON a.researchdesign_id = d.researchdesign_id
            WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
            ORDER BY updated_at DESC LIMIT 10;
        EOD;

    static string $restricted_list_study =
        <<<'EOD'
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.start_date
                 , a.endpoint
                 , a.objectives
                 , a.conclusion
                 , a.exclusion
                 , a.inclusion
                 , a.institute
                 , a.country_id
                 , b.name                                                      AS country_name
                 , a.consortium_id
                 , c.name                                                      AS consortium_name
                 , a.published
                 , a.researchdesign_id
                 , d.name                                                      AS researchdesign_name
                 , a.num_treat
                 , a.num_factor
                 , a.num_arm
                 , a.researchdesign_text
                 , a.num_volun
                 , a.num_volun_terminated
                 , a.recruit_start_year
                 , a.recruit_end_year
                 , a.blinding
                 , CASE WHEN a.blinding = 1 THEN 'Yes' ELSE 'No' END           AS blinding
                 , CASE WHEN a.blinding = 1 THEN a.blinding_method ELSE '' END AS blinding_method
                 , a.owner_org
                 , a.owner_user
                 , a.created_at
                 , a.updated_at
                 , a.details
            FROM dashin.study                    a
                 LEFT JOIN admin.country         b ON a.country_id = b.country_id
                 LEFT JOIN dashin.consortium     c ON a.consortium_id = c.consortium_id
                 LEFT JOIN dashin.researchdesign d ON a.researchdesign_id = d.researchdesign_id
               , LATERAL ( SELECT a.name
                                    || ' ' || coalesce(a.title, '')
                                    || ' ' || coalesce(a.description, '')
                                    || ' ' || coalesce(a.endpoint, '')
                                    || ' ' || coalesce(a.objectives, '')
                                    || ' ' || coalesce(a.conclusion, '')
                                    || ' ' || coalesce(a.exclusion, '')
                                    || ' ' || coalesce(a.inclusion, '')
                                    || ' ' || coalesce(a.institute, '')
                                    || ' ' || coalesce(a.published, '')
                                    || ' ' || coalesce(a.researchdesign_text, '')
              AS search_field
              )                                  s (search_field)
            WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
              AND ((:search_term)::TEXT = '' OR s.search_field ~* (:search_term)::TEXT)
            ORDER BY s.search_field;
        EOD;

    static string $study_detailed_view =
        <<<'EOD'
            WITH
              base_query          AS (
                                       SELECT a.study_id
                                            , a.name
                                            , a.title
                                            , a.description
                                            , a.start_date
                                            , a.endpoint
                                            , a.objectives
                                            , a.conclusion
                                            , a.exclusion
                                            , a.inclusion
                                            , a.institute
                                            , a.country_id
                                            , b.name                                                      AS country_name
                                            , a.consortium_id
                                            , c.name                                                      AS consortium_name
                                            , a.published
                                            , a.researchdesign_id
                                            , d.name                                                      AS researchdesign_name
                                            , a.num_treat
                                            , a.num_factor
                                            , a.num_arm
                                            , a.researchdesign_text
                                            , a.num_volun
                                            , a.num_volun_terminated
                                            , a.recruit_start_year
                                            , a.recruit_end_year
                                            , CASE WHEN a.blinding = 1 THEN 'Yes' ELSE 'No' END           AS blinding
                                            , CASE WHEN a.blinding = 1 THEN a.blinding_method ELSE '' END AS blinding_method
                                            , a.updated_at
                                       FROM dashin.study                 a
                                         LEFT JOIN admin.country         b ON a.country_id = b.country_id
                                         LEFT JOIN dashin.consortium     c ON a.consortium_id = c.consortium_id
                                         LEFT JOIN dashin.researchdesign d ON a.researchdesign_id = d.researchdesign_id
                                       WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                                       AND a.study_id = :study_id
                                     )
              , dataset_query     AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('dataset_id', c.dataset_id,
                                                                           'name', c.name,
                                                                           'description', c.description)) AS datasets
                                       FROM base_query                    a
                                         LEFT JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.dataset         c ON b.dataset_id = c.dataset_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
              , supportfile_query AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('supportfile_id', c.supportfile_id,
                                                                           'name', c.name,
                                                                           'description', c.description,
                                                                           'filename', c.filename)) AS supportfiles
                                       FROM base_query                        a
                                         LEFT JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.supportfile         c ON b.supportfile_id = c.supportfile_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.start_date
                 , a.endpoint
                 , a.objectives
                 , a.conclusion
                 , a.exclusion
                 , a.inclusion
                 , a.institute
                 , a.country_id
                 , a.country_name
                 , a.consortium_id
                 , a.consortium_name
                 , a.published
                 , a.researchdesign_id
                 , a.researchdesign_name
                 , a.num_treat
                 , a.num_factor
                 , a.num_arm
                 , a.researchdesign_text
                 , a.num_volun
                 , a.num_volun_terminated
                 , a.recruit_start_year
                 , a.recruit_end_year
                 , a.blinding
                 , a.blinding
                 , a.blinding_method
                 , a.updated_at
                 , b.datasets
                 , c.supportfiles
            FROM base_query               a
              LEFT JOIN dataset_query     b ON a.study_id = b.study_id
              LEFT JOIN supportfile_query c ON a.study_id = c.study_id;
        EOD;

    static string $get_study_list_paginated_count =
        <<<'EOD'
            SELECT count(*)
            FROM dashin.study
            WHERE jsonb_extract_path_text(details,'permissions','public') = 'yes';
        EOD;

    static string $get_study_list_paginated =
        <<<'EOD'
            WITH
              basefilter_query    AS (
                                       SELECT a.study_id
                                       FROM dashin.study a
                                       WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                                       ORDER BY a.updated_at DESC
                                       LIMIT :limit OFFSET :offset
                                     )
              , base_query        AS (
                                       SELECT a.study_id
                                            , a.name
                                            , a.title
                                            , a.description
                                            , a.start_date
                                            , a.endpoint
                                            , a.objectives
                                            , a.conclusion
                                            , a.exclusion
                                            , a.inclusion
                                            , a.institute
                                            , a.country_id
                                            , c.name                                                      AS country_name
                                            , a.consortium_id
                                            , d.name                                                      AS consortium_name
                                            , a.published
                                            , a.researchdesign_id
                                            , e.name                                                      AS researchdesign_name
                                            , a.num_treat
                                            , a.num_factor
                                            , a.num_arm
                                            , a.researchdesign_text
                                            , a.num_volun
                                            , a.num_volun_terminated
                                            , a.recruit_start_year
                                            , a.recruit_end_year
                                            , CASE WHEN a.blinding = 1 THEN 'Yes' ELSE 'No' END           AS blinding
                                            , CASE WHEN a.blinding = 1 THEN a.blinding_method ELSE '' END AS blinding_method
                                            , a.updated_at
                                       FROM dashin.study                  a
                                         INNER JOIN basefilter_query      b ON a.study_id = b.study_id
                                         LEFT JOIN  admin.country         c ON a.country_id = c.country_id
                                         LEFT JOIN  dashin.consortium     d ON a.consortium_id = d.consortium_id
                                         LEFT JOIN  dashin.researchdesign e ON a.researchdesign_id = e.researchdesign_id
                                       WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                                     )
              , subject_count     AS (
                                       SELECT a.study_id
                                            , count(*) AS n_subjects
                                       FROM base_query                  a
                                         LEFT JOIN dashin.study_subject b ON a.study_id = b.study_id
                                       GROUP BY a.study_id
                                     )
              
              , dataset_query     AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('dataset_id', c.dataset_id,
                                                                           'name', c.name,
                                                                           'description', c.description)) AS datasets
                                       FROM base_query                    a
                                         LEFT JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.dataset         c ON b.dataset_id = c.dataset_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
              , supportfile_query AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('supportfile_id', c.supportfile_id,
                                                                           'name', c.name,
                                                                           'description', c.description,
                                                                           'filename', c.filename)) AS supportfiles
                                       FROM base_query                        a
                                         LEFT JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.supportfile         c ON b.supportfile_id = c.supportfile_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.start_date
                 , a.endpoint
                 , a.objectives
                 , a.conclusion
                 , a.exclusion
                 , a.inclusion
                 , a.institute
                 , a.country_id
                 , a.country_name
                 , a.consortium_id
                 , a.consortium_name
                 , a.published
                 , a.researchdesign_id
                 , a.researchdesign_name
                 , a.num_treat
                 , a.num_factor
                 , a.num_arm
                 , a.researchdesign_text
                 , a.num_volun
                 , a.num_volun_terminated
                 , a.recruit_start_year
                 , a.recruit_end_year
                 , d.n_subjects
                 , a.blinding
                 , a.blinding
                 , a.blinding_method
                 , a.updated_at
                 , b.datasets
                 , c.supportfiles
            FROM base_query               a
              LEFT JOIN dataset_query     b ON a.study_id = b.study_id
              LEFT JOIN supportfile_query c ON a.study_id = c.study_id
              LEFT JOIN subject_count     d ON a.study_id = d.study_id
            ORDER BY a.updated_at DESC;
        EOD;

    static string $get_study_list_paginated_search_count =
        <<<'EOD'
            SELECT count(*)
            FROM (
                   SELECT a.study_id
                        , a.name
                            || ' ' || coalesce(a.title, '')
                            || ' ' || coalesce(a.description, '')
                            || ' ' || coalesce(a.endpoint, '')
                            || ' ' || coalesce(a.objectives, '')
                            || ' ' || coalesce(a.conclusion, '')
                            || ' ' || coalesce(a.exclusion, '')
                            || ' ' || coalesce(a.inclusion, '')
                            || ' ' || coalesce(a.institute, '')
                            || ' ' || coalesce(a.published, '')
                            || ' ' || coalesce(a.researchdesign_text, '')
                     AS search_field
                   FROM dashin.study a
                   WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                 ) s
            WHERE ((:search_term)::TEXT = '' OR s.search_field ~* (:search_term)::TEXT)
       EOD;

    static string $get_study_list_paginated_search =
        <<<'EOD'
            WITH
              basefilter_query    AS (
                                       SELECT study_id
                                       FROM (
                                              SELECT a.study_id
                                                   , a.name
                                                       || ' ' || coalesce(a.title, '')
                                                       || ' ' || coalesce(a.description, '')
                                                       || ' ' || coalesce(a.endpoint, '')
                                                       || ' ' || coalesce(a.objectives, '')
                                                       || ' ' || coalesce(a.conclusion, '')
                                                       || ' ' || coalesce(a.exclusion, '')
                                                       || ' ' || coalesce(a.inclusion, '')
                                                       || ' ' || coalesce(a.institute, '')
                                                       || ' ' || coalesce(a.published, '')
                                                       || ' ' || coalesce(a.researchdesign_text, '')
                                                AS search_field
                                              FROM dashin.study a
                                              WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                                            ) a
                                       WHERE ((:search_term)::TEXT = '' OR a.search_field ~* (:search_term)::TEXT)
                                       LIMIT :limit OFFSET :offset
                                     )
              , base_query        AS (
                                       SELECT a.study_id
                                            , a.name
                                            , a.title
                                            , a.description
                                            , a.start_date
                                            , a.endpoint
                                            , a.objectives
                                            , a.conclusion
                                            , a.exclusion
                                            , a.inclusion
                                            , a.institute
                                            , a.country_id
                                            , c.name                                                      AS country_name
                                            , a.consortium_id
                                            , d.name                                                      AS consortium_name
                                            , a.published
                                            , a.researchdesign_id
                                            , e.name                                                      AS researchdesign_name
                                            , a.num_treat
                                            , a.num_factor
                                            , a.num_arm
                                            , a.researchdesign_text
                                            , a.num_volun
                                            , a.num_volun_terminated
                                            , a.recruit_start_year
                                            , a.recruit_end_year
                                            , CASE WHEN a.blinding = 1 THEN 'Yes' ELSE 'No' END           AS blinding
                                            , CASE WHEN a.blinding = 1 THEN a.blinding_method ELSE '' END AS blinding_method
                                            , a.updated_at
                                       FROM dashin.study                  a
                                         INNER JOIN basefilter_query      b ON a.study_id = b.study_id
                                         LEFT JOIN  admin.country         c ON a.country_id = c.country_id
                                         LEFT JOIN  dashin.consortium     d ON a.consortium_id = d.consortium_id
                                         LEFT JOIN  dashin.researchdesign e ON a.researchdesign_id = e.researchdesign_id
                                       WHERE jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes'
                                     )
              , subject_count     AS (
                                       SELECT a.study_id
                                            , count(*) AS n_subjects
                                       FROM base_query                   a
                                         LEFT JOIN  dashin.study_subject b ON a.study_id = b.study_id
                                       GROUP BY a.study_id
                                     )
              
              , dataset_query     AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('dataset_id', c.dataset_id,
                                                                           'name', c.name,
                                                                           'description', c.description)) AS datasets
                                       FROM base_query                    a
                                         LEFT JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.dataset         c ON b.dataset_id = c.dataset_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
              , supportfile_query AS (
                                       SELECT a.study_id
                                            , jsonb_agg(jsonb_build_object('supportfile_id', c.supportfile_id,
                                                                           'name', c.name,
                                                                           'description', c.description,
                                                                           'filename', c.filename)) AS supportfiles
                                       FROM base_query                        a
                                         LEFT JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                                         LEFT JOIN dashin.supportfile         c ON b.supportfile_id = c.supportfile_id
                                       WHERE jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes'
                                       GROUP BY a.study_id
                                     )
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.start_date
                 , a.endpoint
                 , a.objectives
                 , a.conclusion
                 , a.exclusion
                 , a.inclusion
                 , a.institute
                 , a.country_id
                 , a.country_name
                 , a.consortium_id
                 , a.consortium_name
                 , a.published
                 , a.researchdesign_id
                 , a.researchdesign_name
                 , a.num_treat
                 , a.num_factor
                 , a.num_arm
                 , a.researchdesign_text
                 , a.num_volun
                 , a.num_volun_terminated
                 , a.recruit_start_year
                 , a.recruit_end_year
                 , d.n_subjects
                 , a.blinding
                 , a.blinding
                 , a.blinding_method
                 , a.updated_at
                 , b.datasets
                 , c.supportfiles
            FROM base_query               a
              LEFT JOIN dataset_query     b ON a.study_id = b.study_id
              LEFT JOIN supportfile_query c ON a.study_id = c.study_id
              LEFT JOIN subject_count     d ON a.study_id = d.study_id
            ORDER BY a.updated_at DESC;
        EOD;

    static string $get_dataset_as_csv_string =
        <<<'EOD'
            WITH
              gatekeeper_query AS (
                                    SELECT 1
                                    FROM dashin.dataset
                                    WHERE jsonb_extract_path_text(details, 'permissions', 'public') = 'yes'
                                      AND dataset_id = :dataset_id
                                  )
              , data_query     AS (
                                    SELECT string_agg(dat, E'\n') AS dataset_csv
                                    FROM (
                                           SELECT dataset_id                                       AS datarow_id
                                                , 0                                                AS ordinal
                                                , string_agg(name, (:sep)::TEXT ORDER BY local_id) AS dat
                                           FROM dashin.variable
                                           WHERE dataset_id = :dataset_id
                                           GROUP BY dataset_id
                                           UNION ALL
                                           SELECT datarow_id
                                                , ordinal
                                                , string_agg(value, (:sep)::TEXT ORDER BY ordinality) AS dat
                                           FROM (
                                                  SELECT a.datarow_id
                                                       , a.ordinal
                                                       , c.value
                                                       , c.ordinality
                                                  FROM dashin.datatable                                   a
                                                       INNER JOIN dashin.dataset                          b ON a.dataset_id = b.dataset_id
                                                     , jsonb_array_elements_text(datarow) WITH ORDINALITY c (value, ordinality)
                                                  WHERE jsonb_extract_path_text(b.details, 'permissions', 'public') = 'yes'
                                                    AND a.dataset_id = :dataset_id
                                                ) x0
                                           GROUP BY datarow_id
                                                  , ordinal
                                           ORDER BY ordinal
                                         ) x1
                                  )
            SELECT dataset_csv
            FROM data_query
            WHERE exists
              (
                SELECT
                FROM gatekeeper_query
              );
            EOD;
}