<?php

namespace Public\Helpers\Sections;

use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use Public\Helpers\RepositoryBase;

class SearchRepository extends RepositoryBase
{

    public function landingPageSearch()
    {

        try {
            $sql = SearchQuery::$landing_page_list_studies;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function studySimpleSearch($searchTerm)
    {

        try {
            $sql = SearchQuery::$restricted_list_study;
            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "search_term" => $searchTerm,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    public function studyDetailedView($studyid)
    {

        if (!UniqueId::uuidValidate($studyid)) {
            return false;
        }

        try {
            $sql = SearchQuery::$study_detailed_view;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyid,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    public function getStudyPaginated($page, $nrows, $searchTerm)
    {

        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = SearchQuery::$get_study_list_paginated_search_count;
            $itemQuery = SearchQuery::$get_study_list_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = SearchQuery::$get_study_list_paginated_count;
            $itemQuery = SearchQuery::$get_study_list_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    public function getDatasetAsCsvString($datasetId, $sep = ",")
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = SearchQuery::$get_dataset_as_csv_string;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "sep"        => $sep,
            ]);

            if (isset($result["dataset_csv"])) {
                return $result["dataset_csv"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

}