<?php
use App\Library\RecursiveConfig\Yaml as ConfigLoader;

/** @var $di Phalcon\Di\FactoryDefault */
$di->setShared('public_config', function () {
    return new ConfigLoader(__DIR__ . '/config.yml');
});
