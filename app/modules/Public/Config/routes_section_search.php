<?php

/** @var Phalcon\Mvc\Router $router */
$router->add(
    "/public/index/:params",
    [
        "namespace"  => 'Public\Controllers\Sections',
        "controller" => "Search",
        "action"     => "landingPage",
        "params"     => 1,
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/public/search/:params",
    [
        "namespace"  => 'Public\Controllers\Sections',
        "controller" => "Search",
        "action"     => "mainSearch",
        "params"     => 1,
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/public/studyview/:params",
    [
        "namespace"  => 'Public\Controllers\Sections',
        "controller" => "Search",
        "action"     => "studyView",
        "params"     => 1,
        "seclab_id"  => SECLAB_ALL,
    ]
);
