<?php

namespace Public\Forms;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;

class LandingPageSearchForm extends FormBase
{

    public function initialize()
    {
        $txtFld = new Text("search_term",[
            "placeholder"=>"Search studies",
            "class"=>"landing-page-search-term",
        ]);
//        $txtFld->setLabel("Search studies");
        $this->add($txtFld);
    }

}