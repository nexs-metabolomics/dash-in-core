<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers;


use App\Library\ApplicationBase\ApplicationRepositoryBase;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;

class RepositoryBase extends ApplicationRepositoryBase
{
    /**
     * Generic pagination done in db
     * Prepares a pagination object.
     * DOES NOT add items, but
     * provides offset and limit for items-query
     *
     * @param $countQuery
     * @param $countParams
     * @param $currentPos
     * @param $limit
     * @return array|bool
     */
    protected function _preparePagination($countQuery, $countParams, $currentPos, $limit)
    {
        try {
            #---------------------------------------------------------
            # count number of entries
            #---------------------------------------------------------
            $count = $this->db->fetchOne($countQuery, Enum::FETCH_NUM, $countParams);
            if (!(isset($count[0])) && $count[0]) {
                return false;
            }
            $totalItems = $count[0];

            #---------------------------------------------------------
            # number of entries pr chunk >=1
            #---------------------------------------------------------
            $limit = (int)$limit;
            $limit = ($limit < 1) ? 1 : $limit;

            #---------------------------------------------------------
            # total number of chunks
            #---------------------------------------------------------
            $lastChunkEntries = $totalItems % $limit;
            $totalChunks = intdiv(floatval($totalItems), floatval($limit));
            $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;

            #---------------------------------------------------------
            # currentPos >=0, <= totalChunks
            #---------------------------------------------------------
            $currentPos = (int)$currentPos;
            if ($currentPos < 1) {
                $currentPos = 1;
            } elseif ($currentPos > $totalChunks) {
                $currentPos = $totalChunks;
            }

            # previous chunk
            $before = ($currentPos > 1) ? $currentPos - 1 : 1;

            # next chunk
            $next = ($currentPos < $totalChunks) ? $currentPos + 1 : $totalChunks;

            # item offset
            $offset = ($currentPos - 1) * $limit;
            if ($offset >= ($totalItems) && $totalItems > 0) {
                $offset = $totalItems - 1;
            } elseif ($offset < 0 || $totalItems <= 0) {
                $offset = 0;
            }

            # first and last item on page (eg: row a to b of n)
            $pageItemFirst = $offset + 1;
            $pageItemLast = $offset + $limit;
            if ($pageItemLast > $totalItems) {
                $pageItemLast = $totalItems;
            }

            return [
                "items" => null,
                "offset" => $offset,
                "first" => 1,
                "before" => $before,
                "previous" => $before,
                "current" => $currentPos,
                "last" => $totalChunks,
                "next" => $next,
                "total_pages" => $totalChunks,
                "total_items" => $totalItems,
                "limit" => $limit,
                "page_item_first" => $pageItemFirst,
                "page_item_last" => $pageItemLast,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
//    protected function _preparePagination2(Db\Adapter\Pdo\Postgresql &$db, $countQuery, $countParams, $itemQuery, $itemParams, $currentPos, $limit)
//    {
//        try {
//            #---------------------------------------------------------
//            # count number of entries
//            #---------------------------------------------------------
//            $count = $db->fetchOne($countQuery, Enum::FETCH_NUM, $countParams);
//            if (!(isset($count[0])) && $count[0]) {
//                return false;
//            }
//            $totalItems = $count[0];
//
//            #---------------------------------------------------------
//            # offset >=0
//            #---------------------------------------------------------
//            $currentPos = (int)$currentPos;
//            if ($currentPos < 1) {
//                $currentPos = 1;
//            } elseif ($currentPos > $totalItems) {
//                $currentPos = $totalItems;
//            }
//
//            #---------------------------------------------------------
//            # number of entries pr chunk >=1
//            #---------------------------------------------------------
//            $limit = (int)$limit;
//            $limit = ($limit < 1) ? 1 : $limit;
//
//            #---------------------------------------------------------
//            # total number of chunks
//            #---------------------------------------------------------
//            $lastChunkEntries = $totalItems % $limit;
//            $totalChunks = intdiv(floatval($totalItems), floatval($limit));
//            $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;
//
//            # before
//            $before = ($currentPos > 1) ? $currentPos - 1 : 1;
//
//            # next
//            $next = ($currentPos < $totalChunks) ? $currentPos + 1 : $totalChunks;
//
//            # offset
//            $offset = ($currentPos - 1) * $limit;
//            if ($offset >= ($totalItems) && $totalItems > 0) {
//                $offset = $totalItems - 1;
//            } elseif ($offset < 0 || $totalItems <= 0) {
//                $offset = 0;
//            }
//            # first and last item on page (eg: row a to b of n)
//            $pageFirst = $offset + 1;
//            $pageLast = $offset + $limit;
//            if ($pageLast > $totalItems) {
//                $pageLast = $totalItems;
//            }
//
//            $itemParams["limit"] = $limit;
//            $itemParams["offset"] = $offset;
//
//            $items = $db->fetchAll($itemQuery, Enum::FETCH_OBJ, $itemParams);
//
//            return (object)[
//                "items" => $items,
//                "offset" => $offset,
//                "first" => 1,
//                "before" => $before,
//                "previous" => $before,
//                "current" => $currentPos,
//                "last" => $totalChunks,
//                "next" => $next,
//                "total_pages" => $totalChunks,
//                "total_items" => $totalItems,
//                "limit" => $limit,
//                "page_item_first" => $pageFirst,
//                "page_item_last" => $pageLast,
//            ];
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//        return false;
//    }

    /**
     * Get minimal info on a dataset
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatasetMinimal($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->other->dashin_get_dataset_minimal;
            
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get minimal info on studydataset
     * indicator whether dataset is in a study
     *
     * @param $datasetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getDatasetStudyindicatorMinimal($datasetId, $studyId=null)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (($studyId!==null && !UniqueId::uuidValidate($studyId))) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->other->dashin_get_dataset_studyindicator_minimal;
            
            $organizationId = $this->SU->getActiveManagerOrgId();
            
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId,
                "dataset_id" => $datasetId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get minimal info on a study
     * 
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyInfo($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->other->dashin_get_study_info;
            
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get the summary of the design structure of a dataset
     * defined by the set of design variables
     * 
     * 
     * @param $datasetId
     * @return array|false
     */
    public function datadesignGetSummary($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datadesign_summary;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Create a paginated list of studyvariables
     * 
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyvariablesPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["study_id"] = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariables_search_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariables_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariables_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariables_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    /**
     * Get minimal info on study-variable
     * 
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyvariablesetMinimal($studyvariablesetId, $studyId)
    {
        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariableset_minimal;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;

    }

    /**
     * Get datasets in a study
     * 
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyDatasets($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydata->get_studydatasets;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStudySupportFiles($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydata->get_studysupportfiles;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

}