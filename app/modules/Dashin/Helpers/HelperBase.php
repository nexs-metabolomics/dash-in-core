<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers;

use App\Library\ApplicationBase\ApplicationHelperBase;
use App\Library\Utils\Json;

class HelperBase extends ApplicationHelperBase
{
    public function serviceSelect($returnUri, $key, $serviceName)
    {
        $this->setKeyValue("dashin_owner_service_select_{$serviceName}", [
            "key"        => $key,
            "return_uri" => $returnUri,
        ]);
//        $this->setKeyValue("dashin_owner_service_selectx_menu", ["submenu" => $submenu, "current" => $current, "menu_partial" => $menuPartial]);
        $querystring = $this->localsession->getQuerystring("?");
        $redirectUri = "/dashin/owner/service/select/{$serviceName}/{$querystring}";
        $this->response->redirect($redirectUri);
        $this->response->send();
        return false;
    }

    public function serviceSelectReturn($returnUri, $key, $itemId)
    {
        $returnUri = rtrim($returnUri, "/");
        $this->localsession->setKeyValue($key, $itemId);
        $querystring = $this->localsession->getQuerystring("?");
        $returnUri2 = "$returnUri/$querystring";
        $this->response->redirect($returnUri2);
        $this->response->send();
//        $this->removeKeyValue("dashin_owner_service_selectx_menu");
        return false;
    }

    public function serviceSelectSeclabel($returnUri, $key, $return = false, $seclabelId = null)
    {
        if ($return) {

            $this->setKeyValue($key, $seclabelId);
            $this->response->redirect($returnUri);
            $this->response->send();
            return false;

        } else {
            $this->setKeyValue("dashin_owner_service_select_seclabel", [
                "key"        => $key,
                "return_uri" => $returnUri,
            ]);
            $this->response->redirect("/dashin/owner/service/selectseclab/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
    }

    protected function _csvValidateSeparator(\SplFileObject $file, $sep, $nrow = 10)
    {
        $file->rewind();
        for ($i = 0; $i < $nrow; ++$i) {
            # get number of columns in (up to) first 10 rows
            
            # if last line is a single newline then stop
            # if last line is a dataline followed by a newline - don't stop
            $x = $file->fgetcsv($sep);
            $c = $file->current();
            if($file->eof() && !$c) {
                break;
            }
            $nCols[$i] = count($x);
        }
        $file->rewind();
        # do all rows have the same number of columns?
        $min = min($nCols);
        $max = max($nCols);
        $res = ($min === $max) ? $min : false;
        return $res;
    }

    /**
     * Try to determine the seperator from data
     *
     * returns selected separator or false
     *
     * @param \SplFileObject $file
     * @param null $sep if $sep == null try to determine sep
     * @return bool|string
     */
    protected function _guessSeparator(\SplFileObject $file, $sep = null)
    {
        $separators = [",", ";", "\t", " "];
        if (in_array($sep, $separators)) {
            $res = $this->_csvValidateSeparator($file, $sep);
            if ($res > 1) {
                # return first likely correct sep
                return $sep;
            }
        } else {
            foreach ($separators as $sep) {
                # returns
                #   false:  unequal number of columns
                #   1:      one column in all rows
                #   1 < x:  equal of columns (number more than 1) in all rows
                $res = $this->_csvValidateSeparator($file, $sep);
                if ($res > 1) {
                    # return first likely correct sep
                    return $sep;
                }
            }
        }
        if ($res === 1) {
            $this->flashSession->error("The selected separator results in 1 variable");
            return $sep;
        }
        $this->flashSession->error("The correct separator could not be determined");
        return false;
    }

    protected function _getAdditionalFieldsAsJson($additionalFieldNames)
    {
        $additionalFieldsJson = null;
        if (is_array($additionalFieldNames)) {
            $tmpFieldValues = [];
            foreach (array_keys($additionalFieldNames) as $fieldName) {
                if ($this->request->has($fieldName)) {
                    $tmpFieldValues[$fieldName] = $this->request->getPost($fieldName);
                }
            }
            if ($tmpFieldValues) {
                $additionalFieldsJson = Json::jsonEncode(["fields" => $tmpFieldValues]);
            }
        }
        return $additionalFieldsJson;
    }

}