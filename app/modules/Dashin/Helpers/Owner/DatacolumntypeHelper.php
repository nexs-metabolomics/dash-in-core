<?php


namespace Dashin\Helpers\Owner;


use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\DatacolumntypeForm;
use Dashin\Helpers\HelperBase;

class DatacolumntypeHelper extends HelperBase
{
    /**
     * @var DatacolumntypeRepository
     */
    private $_repository;

    /**
     * @return DatacolumntypeRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new DatacolumntypeRepository();
        }
        return $this->_repository;
    }

    /**
     * Create data column type
     * 
     * @return array
     */
    public function createDatacolumntype()
    {
        $form = new DatacolumntypeForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $datacolumnTypeId = $this->_getRepository()->createDatacolumntype($post);
                if ($datacolumnTypeId) {
                    $this->localsession->setKeyValue("dashin_owner_datacolumntype_id", $datacolumnTypeId);
                    # redirect
                }
            }

        }
        return [
            "form" => $form
        ];
    }

    /**
     * Edit column type
     * 
     * @param $datacolumntypeId string UUID of the selected data column type
     * @return array|false
     */
    public function editDatacolumntype($datacolumntypeId)
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/datacolumntype/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $form = new DatacolumntypeForm();
        if ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                $post = $this->request->getPost();
                if (!$form->isValid($post)) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $ok = $this->_getRepository()->updateDatacolumntype($datacolumntypeId);
                    if ($action === "submit" && $ok) {
                        $this->response->redirect("/dashin/owner/datacolumntype/list/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();
                        return false;
                    }
                }
            }
        }

        $data = $this->_getRepository()->getDatacolumntype($datacolumntypeId);
        if($data) {
            $form->bindValues($data);
        }
        
        return [
            "form" => $form
        ];
    }

    /**
     * List of existing column types
     * 
     * @param $page
     * @param $nrows
     * @param $reset
     * @return array
     */
    public function listDatacolumntypes($page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            # delete - 1st run
            $datacolumntypeId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($datacolumntypeId)) {
                $this->setKeyValue("dashin_owner_datacolumntype_delete", $datacolumntypeId);
            } else {
                $this->removeKeyValue("dashin_owner_datacolumntype_delete");
            }
        } elseif ($action === "confirm") {
            # delete 2nd run
            $datacolumntypeId = $this->Btn->getValue();
            $datacolumntypeIdSaved = $this->getKeyValue("dashin_owner_datacolumntype_delete");
            # if not equal, user has clicked to delete a different entity
            # don't delete, reset and return
            if ($datacolumntypeId === $datacolumntypeIdSaved) {
                $this->_getRepository()->deleteDatacolumntype($datacolumntypeId);
            }
            $this->removeKeyValue("dashin_owner_datacolumntype_delete");
            $this->localsession->removeKeyValue("dashin_owner_datacolumntype_id");
            # reset buttons in ui even if deletion fails
            $datacolumntypeId = false;
        } else {
            $datacolumntypeId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_datacolumntype_search_term", $reset);
        $datacolumntypes = $this->_getRepository()->getDatacolumntypesPaginated($page, $nrows, $searchTerm);
        if ($datacolumntypes) {
            foreach ($datacolumntypes->items as &$row) {
                if ($row->datacolumntype_id === $datacolumntypeId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
            }
        }

        return [
            "datacolumntypes" => $datacolumntypes,
        ];
    }
}