<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-09
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\Csv;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\SearchExtendedType02;
use Dashin\Forms\Owner\SearchMetabolomicsForm;
use Dashin\Forms\Owner\SaveResultsetForm;
use Dashin\Helpers\HelperBase;
use ZipArchive;

class SearchHelper extends HelperBase
{
    /**
     * @var SearchRepository
     */
    private $_repository;

    /**
     * @return SearchRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new SearchRepository();
        }
        return $this->_repository;
    }

    public function saveMetabolomics($searchParams)
    {
        $form = new SaveResultsetForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {

                $action = $this->Btn->getAction();
                if ($action === "save") {

                    $resultsetId = $this->_getRepository()->saveMetabolomicsResultset($searchParams);
                    if ($resultsetId) {
                        $this->localsession->setKeyValue("dashin_owner_resultset_id", $resultsetId);
                        $this->response->redirect("/dashin/owner/search/viewresultset/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();
                        return false;
                    }
                }
            }
        }
        $summaryResult = $this->_getRepository()->searchMetabolomicsSummary($searchParams);
        if ($searchParams["mz_center"] . $searchParams["mz_range"] . $searchParams["rt_center"] . $searchParams["rt_range"]) {
            if ($form->has("name")) {
                $name = "Metabolomics. " .
                    "(Mz: " . $searchParams["mz_center"] . " +/- " . $searchParams["mz_range"] . ") " .
                    "(Rt: " . $searchParams["rt_center"] . " +/- " . $searchParams["rt_range"] . ") " .
                    "(N: " . $summaryResult[0]->n_all . ")";
                $form->get("name")->setAttribute("value", $name);
            }
        }

        return [
            "form"    => $form,
            "summary" => $summaryResult,
        ];
    }

    public function saveMetabolomicsType02($searchParams)
    {
        $form = new SaveResultsetForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {

                $action = $this->Btn->getAction();
                if ($action === "save") {

                    $resultsetId = $this->_getRepository()->saveMetabolomicsType02Resultset($searchParams);
                    if ($resultsetId) {
                        $this->localsession->setKeyValue("dashin_owner_resultset_id", $resultsetId);
                        $this->response->redirect("/dashin/owner/search/viewresultset/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();
                        return false;
                    }
                }
            }
        }
        $searchData = true;
        $summaryResult = $this->_getRepository()->searchMetabolomicsType02Summary($searchParams,$searchData);
        if ($searchParams["mz_center"] . $searchParams["mz_range"] . $searchParams["rt_center"] . $searchParams["rt_range"]) {
            if ($form->has("name")) {
                $name = "Metabolomics. " .
                    "(Mz: " . $searchParams["mz_center"] . " +/- " . $searchParams["mz_range"] . ") " .
                    "(Rt: " . $searchParams["rt_center"] . " +/- " . $searchParams["rt_range"] . ") " .
                    "(N: " . $summaryResult[0]->n_all . ")";
                $form->get("name")->setAttribute("value", $name);
            }
        }

        return [
            "form"    => $form,
            "summary" => $summaryResult,
        ];
    }

    /**
     * Filter,sanitize the center,range pair into low,high
     *
     * Returns a sanitized array [low,high,center,range]
     *
     * @param $center
     * @param $range
     * @return float[]|null[]
     */
    private function _searchRangeFilter($center, $range)
    {
        if (strlen($center) === 0) {
            return [null, null];
        } elseif (strlen($range) === 0) {
            $center = (float)$center;
            return [$center, null];
        } else {
            $center = (float)$center;
            $range = (float)$range;
            return [$center, $range];
        }
    }

    public function searchMetabolomics($page, $nrows, $reset)
    {
        $form = new SearchMetabolomicsForm();

        $action = $this->Btn->getAction();
        if ($action === "search") {
            $mzCenter = $this->request->getPost("mz_center");
            $mzRange = $this->request->getPost("mz_range");
            $rtCenter = $this->request->getPost("rt_center");
            $rtRange = $this->request->getPost("rt_range");
            $mz = $this->_searchRangeFilter($mzCenter, $mzRange);
            $rt = $this->_searchRangeFilter($rtCenter, $rtRange);
            $searchParams = [
                "mz_center" => $mz[0],
                "mz_range"  => $mz[1],
                "rt_center" => $rt[0],
                "rt_range"  => $rt[1],
            ];
            $this->setKeyValue("dashin_owner_metabolomics_search_params", $searchParams);


        }
        if ($reset) {
            $this->removeKeyValue("dashin_owner_metabolomics_search_params");
            # reset pagination to the first page when search result is changed
            $searchParams = false;
            $page = 1;
        } else {
            $searchParams = $this->getKeyValue("dashin_owner_metabolomics_search_params");
            if (is_array($searchParams)) {
                $form->bindValues($searchParams);
            }
        }

        $searchSummary = $this->_getRepository()->searchMetabolomicsSummary($searchParams);
        if ($searchSummary) {
            $totalItems = $searchSummary[0]->n_all;
        } else {
            $totalItems = 0;
        }
        $resultsPaginated = $this->_getRepository()->searchMetabolomicsPaginated($totalItems, $searchParams, $page, $nrows);

        $formStudy = new SearchExtendedType02();

        return [
            "form"              => $form,
            "form_study"        => $formStudy,
            "summary"           => $searchSummary,
            "results_paginated" => $resultsPaginated,
        ];
    }

    public function searchMetabolomicsType02($page, $nrows, $reset)
    {
        $form = new SearchMetabolomicsForm();

        $action = $this->Btn->getAction();
        if ($action === "search") {
            $mzCenter = $this->request->getPost("mz_center");
            $mzRange = $this->request->getPost("mz_range");
            $rtCenter = $this->request->getPost("rt_center");
            $rtRange = $this->request->getPost("rt_range");
            
            $mz = $this->_searchRangeFilter($mzCenter, $mzRange);
            $rt = $this->_searchRangeFilter($rtCenter, $rtRange);
            
            # search only studies (don't search data) if no data parameters
            $dataSearch = true;
            if(strlen($mzCenter.$mzRange.$rtCenter.$rtRange)===0) {
                $dataSearch = false;
            }

            if ($dataSearch) {
                $searchParams = [
                    "mz_center" => $mz[0],
                    "mz_range"  => $mz[1],
                    "rt_center" => $rt[0],
                    "rt_range"  => $rt[1],
                ];
            }
            $searchParams["studyname"] = $this->request->getPost("studyname");
            $searchParams["studytitle"] = $this->request->getPost("studytitle");
            $searchParams["studydescription"] = $this->request->getPost("studydescription");
            $searchParams["studyendpoint"] = $this->request->getPost("studyendpoint");
            $searchParams["studyobjectives"] = $this->request->getPost("studyobjectives");
            $searchParams["studyconclusion"] = $this->request->getPost("studyconclusion");
            $searchParams["studyexclusion"] = $this->request->getPost("studyexclusion");
            $searchParams["studyresearchdesign"] = $this->request->getPost("studyresearchdesign");
            $this->setKeyValue("dashin_owner_metabolomics_type02_search_params", $searchParams);

        }
        if ($reset) {
            $this->removeKeyValue("dashin_owner_metabolomics_type02_search_params");
            # reset pagination to the first page when search result is changed
            $searchParams = false;
            $page = 1;
        } else {
            $searchParams = $this->getKeyValue("dashin_owner_metabolomics_type02_search_params");
        }
        
        $searchData = true;
        $searchStudy = true;
        # data search parameters
        if(strlen($searchParams["mz_center"].$searchParams["mz_range"].$searchParams["rt_center"].$searchParams["rt_range"]) === 0) {
            $searchData = false;
        }
        # study search parameters
        if(strlen($searchParams["studyname"].
            $searchParams["studytitle"].
            $searchParams["studydescription"].
            $searchParams["studyendpoint"].
            $searchParams["studyobjectives"].
            $searchParams["studyconclusion"].
            $searchParams["studyexclusion"].
            $searchParams["studyresearchdesign"]) === 0) {
            $searchStudy = false;
        }
        # remove data search parameters
        if(!$searchData) {
            unset($searchParams["mz_center"]);
            unset($searchParams["mz_range"]);
            unset($searchParams["rt_center"]);
            unset($searchParams["rt_range"]);
        }

        # only bind values if any values
        if($searchData || $searchStudy) {
            if (is_array($searchParams)) {
                $form->bindValues($searchParams);
            }
        } else {
            $searchParams = false;
        }

        if($searchParams) {
            $searchSummary = $this->_getRepository()->searchMetabolomicsType02Summary($searchParams,$searchData);
            if ($searchData) {
                if ($searchSummary) {
                    $totalItems = $searchSummary[0]->n_all;
                } else {
                    $totalItems = 0;
                }
                $resultsPaginated = $this->_getRepository()->searchMetabolomicsType02Paginated($totalItems, $searchParams, $page, $nrows);
            } else {
                $resultsPaginated = false;
            }
        }

        $formStudy = new SearchExtendedType02();

        return [
            "form"              => $form,
            "form_study"        => $formStudy,
            "summary"           => $searchSummary,
            "results_paginated" => $resultsPaginated,
        ];
    }

    public function listResultsets($page, $nrows, $reset)
    {
        $data = $this->_getRepository()->getResultsetsPaginated($page, $nrows);
        if (!$data) {
            return false;
        }
        return [
            "data" => $data,
        ];
    }

    public function viewResultset($resultsetId)
    {
        $data = $this->_getRepository()->getResultset($resultsetId);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        $resultsetInfo = $this->_getRepository()->getResultsetMinimal($resultsetId);

        return [
            "data"           => $data,
            "resultset_info" => $resultsetInfo,
        ];
    }

    public function editResultsetDataset($resultsetId, $datasetId, $selectedPage, $selectedNrows, $unselectedPage, $unselectedNrows, $reset1, $reset2)
    {
        $action = $this->Btn->getAction();
        if ($action === "add") {
            $variableId = $this->Btn->getValue();
            $ok = $this->_getRepository()->addResultsetVariable($resultsetId, $variableId);
            if($ok) {
                $this->flashSession->success("Variable added sucessfully");
            }
        } elseif ($action === "remove") {
            $variableId = $this->Btn->getValue();
            $ok = $this->_getRepository()->removeResultsetVariable($resultsetId, $variableId);
            if($ok) {
                $this->flashSession->success("Variable removed sucessfully");
            }
        }

        $searchTerm1 = $this->getSearchTerm("dashin_owner_editmetabolresult_search_term1", $reset1,"search_term1");
        $searchTerm2 = $this->getSearchTerm("dashin_owner_editmetabolresult_search_term2", $reset2,"search_term2");
        $selected = $this->_getRepository()->getResultsetDatasetVarsSelectedPaginated($resultsetId, $datasetId, $selectedPage, $selectedNrows, $searchTerm1);
        $unselected = $this->_getRepository()->getResultsetDatasetVarsUnselectedPaginated($resultsetId, $datasetId, $unselectedPage, $unselectedNrows, $searchTerm2);
        $resultsetInfo = $this->_getRepository()->getResultsetMinimal($resultsetId);
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        return [
            "selected"       => $selected,
            "unselected"     => $unselected,
            "resultset_info" => $resultsetInfo,
            "dataset_info"   => $datasetInfo,
        ];
    }

    public function exportResultset($resultsetId)
    {
        $datasetIds = $this->_getRepository()->getResultsetDatasetIds($resultsetId);
        if (!isset($datasetIds[0]["dataset_id"])) {
            return false;
        }

        $prefix = substr(md5($resultsetId), 0, 10);
        $resultsetName = substr($prefix . "_" . preg_replace('#[^a-zA-Z0-9_-]#', "", $datasetIds[0]["resultset_name"]), 0, 100);
        $zip = new \ZipArchive();
        $outname = "{$resultsetName}.zip";

        $tmpdir = $this->storage->getTempdirLocation();
        $tempZipfilePath = $tmpdir ."/" . UniqueId::uuid4();

//        $tempZipfile = tmpfile();
//        $tempZipfilePath = stream_get_meta_data($tempZipfile)['uri'];

        $zip->open($tempZipfilePath, ZipArchive::CREATE);
        for ($i = 0, $imax = count($datasetIds); $i < $imax; ++$i) {
            $datasetId = $datasetIds[$i]["dataset_id"];
            $prefix = substr(md5($datasetId), 0, 10);
            $datasetName = substr($prefix . "_" . preg_replace('#[^a-zA-Z0-9_-]#', "", $datasetIds[$i]["dataset_name"]), 0, 100);
            $data = $this->_getRepository()->extractDataset($resultsetId, $datasetId);
            if (!$data) {
                return false;
            }
            $CSV = new Csv();
            $hdr = array_keys($data[0]);
            $x = $CSV->arrayToCsv($data, ";", $hdr);
            $zip->addFromString("{$resultsetName}/{$datasetName}.csv", $x);
        }
        $zip->close();
        $zipfile = file_get_contents($tempZipfilePath);
        $this->downloader->send($zipfile, $outname, "zip");
    }

}