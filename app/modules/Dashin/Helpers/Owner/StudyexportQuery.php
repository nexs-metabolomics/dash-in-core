<?php

namespace Dashin\Helpers\Owner;

use Dashin\Helpers\QueryBase;

class StudyexportQuery extends QueryBase
{

    public static string $export_study_summary =
        <<<'EOD'
            WITH
              input_query   AS (
                                 SELECT study_id::UUID AS _study_id
                                      , maxlength      AS _text_maxlength
                               )
              , base_query  AS (
                                 SELECT name
                                      , title
                                      , description
                                      , start_date
                                      , endpoint
                                      , objectives
                                      , conclusion
                                      , exclusion
                                      , inclusion
                                      , institute
                                      , country_id
                                      , consortium_id
                                      , published
                                      , researchdesign_id
                                      , num_treat
                                      , num_factor
                                      , num_arm
                                      , researchdesign_text
                                      , num_volun
                                      , num_volun_terminated
                                      , recruit_start_year
                                      , recruit_end_year
                                      , blinding
                                      , blinding_method
                                      , details
                                      , _text_maxlength
                                 FROM dashin.study a
                                    , input_query  i
                                 WHERE a.study_id = _study_id
                               )
              , trunc_query AS (
                                 SELECT CASE WHEN length(name) > _text_maxlength THEN substring(name FOR _text_maxlength - 3) || '...' END                               AS name_trunc
                                      , name
                                      , CASE WHEN length(title) > _text_maxlength THEN substring(title FOR _text_maxlength - 3) || '...' END                             AS title_trunc
                                      , title
                                      , CASE WHEN length(description) > _text_maxlength THEN substring(description FOR _text_maxlength - 3) || '...' END                 AS description_trunc
                                      , description
                                      , start_date
                                      , CASE WHEN length(endpoint) > _text_maxlength THEN substring(endpoint FOR _text_maxlength - 3) || '...' END                       AS endpoint_trunc
                                      , endpoint
                                      , CASE WHEN length(objectives) > _text_maxlength THEN substring(objectives FOR _text_maxlength - 3) || '...' END                   AS objectives_trunc
                                      , objectives
                                      , CASE WHEN length(conclusion) > _text_maxlength THEN substring(conclusion FOR _text_maxlength - 3) || '...' END                   AS conclusion_trunc
                                      , conclusion
                                      , CASE WHEN length(exclusion) > _text_maxlength THEN substring(exclusion FOR _text_maxlength - 3) || '...' END                     AS exclusion_trunc
                                      , exclusion
                                      , CASE WHEN length(inclusion) > _text_maxlength THEN substring(inclusion FOR _text_maxlength - 3) || '...' END                     AS inclusion_trunc
                                      , inclusion
                                      , institute
                                      , country_id
                                      , consortium_id
                                      , published
                                      , researchdesign_id
                                      , num_treat
                                      , num_factor
                                      , num_arm
                                      , CASE WHEN length(researchdesign_text) > _text_maxlength THEN substring(researchdesign_text FOR _text_maxlength - 3) || '...' END AS researchdesign_text_trunc
                                      , researchdesign_text
                                      , num_volun
                                      , num_volun_terminated
                                      , recruit_start_year
                                      , recruit_end_year
                                      , blinding
                                      , blinding_method
                                      , details
                                 FROM base_query
                               )
            SELECT name_trunc
                 , title_trunc
                 , description_trunc
                 , endpoint_trunc
                 , objectives_trunc
                 , conclusion_trunc
                 , exclusion_trunc
                 , inclusion_trunc
                 , researchdesign_text_trunc
            
                 , name
                 , title
                 , description
                 , endpoint
                 , objectives
                 , conclusion
                 , exclusion
                 , inclusion
                 , researchdesign_text
            
                 , start_date
                 , institute
                 , country_id
                 , consortium_id
                 , published
                 , researchdesign_id
                 , num_treat
                 , num_factor
                 , num_arm
                 , num_volun
                 , num_volun_terminated
                 , recruit_start_year
                 , recruit_end_year
                 , blinding
                 , blinding_method
                 , details
            FROM trunc_query;
        EOD;

    public static string $export_designvar_summary =
        <<<'EOD'
            WITH
              input_query                       AS (
                                                     -- SELECT :study_id::UUID AS _study_id
                                                     SELECT study_id AS _study_id
                                                     FROM dashin.study
                                                     WHERE study_id = :study_id
                                                       AND jsonb_extract_path_text(details, 'permissions', 'public') = 'yes'
                                                   )
              , startgroup_query                AS (
                                                     SELECT 'startgroup'                     AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'startgroup_id', startgroup_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_startgroup
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , center_query                    AS (
                                                     SELECT 'center'                         AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'center_id', center_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_center
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , event_query                     AS (
                                                     SELECT 'event'                          AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_event
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , subevent_query                  AS (
                                                     SELECT 'subevent'                       AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'subevent_id', subevent_id
                                                              , 'subeventtype_id', subevent_type_id
                                                              , 'interventiontype_id', intervention_type_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_subevent
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , samplingevent_query             AS (
                                                     SELECT 'samplingevent'                  AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'samplingevent_id', samplingevent_id
                                                              , 'subeventtype_id', sampling_type_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_samplingevent
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , samplingtime_query              AS (
                                                     SELECT 'samplingtime'                            AS objectname
                                                          , jsonb_agg(row_obj ORDER BY sename,stname) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'samplingtime_id', a.samplingtime_id
                                                              , 'samplingevent_id', a.samplingevent_id
                                                              , 'ordinal_time', a.ordinal_time
                                                              , 'name', a.name
                                                              , 'description', a.description
                                                                   )      AS row_obj
                                                                 , a.name AS stname
                                                                 , b.name AS sename
                                                            FROM dashin.study_samplingtime a
                                                              INNER JOIN (
                                                                           SELECT z.samplingevent_id
                                                                                , z.name
                                                                           FROM dashin.study_samplingevent z
                                                                              ,                            input_query
                                                                           WHERE z.study_id = _study_id
                                                                         )                 b ON a.samplingevent_id = b.samplingevent_id
                                                          ) x
                                                   )
              , event_subevent_startgroup_query AS (
                                                     SELECT 'eveent_subevent_startgroup' AS objectname
                                                          , jsonb_agg(row_obj)           AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'subevent_id', subevent_id
                                                              , 'startgroup_id', startgroup_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_event_subevent_x_startgroup a
                                                               ,                                          input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , event_subevent_query            AS (
                                                     SELECT 'eveent_subevent'  AS objectname
                                                          , jsonb_agg(row_obj) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'subevent_id', subevent_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_event_x_subevent a
                                                               ,                               input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , subevent_samplingevent_query    AS (
                                                     SELECT 'subevent_samplingevent' AS objectname
                                                          , jsonb_agg(row_obj)       AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'subevent_id', subevent_id
                                                              , 'samplingevent_id', samplingevent_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_subevent_x_samplingevent a
                                                               ,                                       input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , studycontact                    AS (
                                                     SELECT 'studycontact' AS objectname
                                                          , jsonb_build_object(
                                                       'studycontact_id', studycontact_id
                                                       , 'first_name', first_name
                                                       , 'last_name', last_name
                                                       , 'email', email
                                                       , 'description', description
                                                       , 'study_role_id', study_role_id
                                                            )              AS obj
                                                     FROM dashin.studycontact a
                                                        ,                     input_query
                                                     WHERE study_id = _study_id
                                                   )
              , combine_query                   AS (
                                                     SELECT objectname
                                                          , obj
                                                     FROM startgroup_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM center_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM subevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM samplingevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM samplingtime_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_subevent_startgroup_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_subevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM subevent_samplingevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM studycontact
                                                   )
            SELECT *
            FROM combine_query;
        EOD;

    public static string $get_studyexportset_single_data_query =
        <<<'EOD'
            WITH
              -- always add all design variables to all datasets - use for join
              designvar_query        AS (
                                          SELECT studyvariable_id
                                               , name AS studyvariable_name
                                               , variabletype_id
                                               , ordinal
                                               , study_id
                                          FROM dashin.studyvariable a
                                          WHERE a.study_id = :study_id
                                            AND a.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
                                        )
               -- generate export name
              , exportname_query     AS (
                                          SELECT a.name || '_' || b.name AS exportname
                                          FROM dashin.studyexportset a
                                            INNER JOIN dashin.study  b ON a.study_id = b.study_id
                                          WHERE (a.studyexportset_id, a.study_id) = (:studyexportset_id, :study_id)
                                        )
              -- get variables
              , variable_query       AS (
                                          SELECT f.studyrowset_id
                                               , d.studyvariable_id
                                               , d.study_id
                                               , d.ordinal
                                               , d.datatype_id
                                               , d.name
                                               , d.label
                                               , d.description
                                               , d.variabletype_id
                                          FROM (
                                                 SELECT a.studyvariable_id
                                                      , a.study_id
                                                 FROM designvar_query a
                                                 UNION
                                                 SELECT b1.studyvariable_id
                                                      , b0.study_id
                                                 FROM dashin.studyexportset                           b0
                                                   INNER JOIN dashin.studyvariableset_x_studyvariable b1 ON b0.studyvariableset_id = b1.studyvariableset_id
                                                 WHERE (b0.studyexportset_id, b0.study_id) = (:studyexportset_id, b1.study_id)
                                               )                             c
                                            INNER JOIN dashin.studyvariable  d ON (c.studyvariable_id, c.study_id) = (d.studyvariable_id, d.study_id)
                                            INNER JOIN dashin.studyexportset e ON (e.studyexportset_id, e.study_id) = (:studyexportset_id, :study_id)
                                            INNER JOIN dashin.studyrowset    f ON (e.studyrowset_id, e.study_id) = (f.studyrowset_id, f.study_id)
                                          WHERE exists
                                            (
                                              SELECT
                                              FROM dashin.study z
                                              WHERE (z.study_id, z.owner_org) = (:study_id, :owner_org)
                                            )
                                        )
              -- combine design variables and other selected variables
              , basetable            AS (
                                          SELECT a.studyrowset_id
                                               , b.study_id
                                               , a.datatype_id
                                               , c.local_id
                                               , b.variable_id
                                               , a.name                                            AS variablename
                                               , e.dataset_id
                                               , e.name                                            AS dataset_name
                                               , a.variabletype_id
                                               , (a.variabletype_id IN (2, 3, 4, 5, 6, 7, 8))::INT AS is_designvar
                                               , a.ordinal
                                          FROM variable_query                          a
                                            INNER JOIN dashin.studyvariable_x_variable b ON (a.studyvariable_id, a.study_id) = (b.studyvariable_id, b.study_id)
                                            INNER JOIN dashin.variable                 c ON (b.variable_id, b.dataset_id) = (c.variable_id, c.dataset_id)
                                            INNER JOIN dashin.dataset_x_study          d ON (b.dataset_id, b.study_id) = (d.dataset_id, d.study_id)
                                            INNER JOIN dashin.dataset                  e ON d.dataset_id = e.dataset_id
                                        
                                        )
              -- get selected data rows
              , datarows             AS (
                                          SELECT DISTINCT E'INNER JOIN (SELECT bc.datarow_id\n' ||
                                                          E'FROM dashin.studyrowset_get_studyrows(' || quote_literal(studyrowset_id) || ',' || quote_literal(study_id) || ',' || quote_literal(:owner_org) || E') ba\n' ||
                                                          E' INNER JOIN dashin.studyrow_x_datarow      bc ON ba.studyrow_id = bc.studyrow_id\n' ||
                                                          E'WHERE bc.dataset_id = '                   AS datarows_part_1
                                                        , ')        b ON a.datarow_id = b.datarow_id' AS datarows_part_2
                                          FROM basetable
                                        )
              -- generate code to extract variables from json array
              , generate_vars_part   AS (
                                          SELECT CASE
                                                   WHEN datatype_id = 2 THEN 'text_to_numeric(jsonb_array_element_text(datarow,' || local_id || ')) AS '
                                                   ELSE 'jsonb_array_element_text(datarow,' || local_id || ') AS '
                                                 END || quote_ident(variablename) AS query_vars_part
                                               , variablename
                                               , variable_id
                                               , dataset_id
                                               , dataset_name
                                               , local_id
                                               , ordinal
                                               , is_designvar
                                          FROM basetable
                                        )
              -- create a data query for each dataset
              , data_queries         AS (
                                          SELECT a.dataset_id
                                               , a.dataset_vars_part || E'\n' ||
                                                 b.datarows_part_1 || quote_literal(a.dataset_id) || E'\n' ||
                                                 b.datarows_part_2 || E'\n' ||
                                                 a.dataset_where_part          AS data_query
                                               , 'qry' || row_number() OVER () AS query_alias
                                          FROM (
                                                 SELECT dataset_id
                                                      , E'SELECT \n  ' || string_agg(query_vars_part, E'\n , ' ORDER BY is_designvar DESC, ordinal) || E' \nFROM dashin.datatable a' AS dataset_vars_part
                                                      , 'WHERE dataset_id = ' || quote_literal(dataset_id)                                                                           AS dataset_where_part
                                                 FROM generate_vars_part
                                                 GROUP BY dataset_id
                                               )        a
                                             , datarows b
                                        )
              -- create a header row - for csv output
              , header_queries       AS (
                                          SELECT dataset_id
                                               , dataset_name
                                               , array_agg(quote_ident(variablename)) AS header_array
                                          FROM generate_vars_part
                                          GROUP BY dataset_id
                                                 , dataset_name
                                        )
              -- create alias and variabe blocks (design variables) to JOIN data queries
              , joinvar_query        AS (
                                          SELECT string_agg(joinvar, ',' ORDER BY ordinal) AS joinvars
                                               , query_alias
                                               , dataset_id
                                          FROM (
                                                 SELECT query_alias || '.' || quote_ident(a1.studyvariable_name) AS joinvar
                                                      , query_alias
                                                      , a0.dataset_id
                                                      , a1.ordinal
                                                 FROM (
                                                        SELECT query_alias
                                                             , dataset_id
                                                        FROM data_queries
                                                      )               a0
                                                    , designvar_query a1
                                               ) a
                                          GROUP BY query_alias
                                                 , dataset_id
                                        )
              -- for each dataset query: collect all elements needed to concatenate all queries into a FULL JOIN statement 
              , combine_query        AS (
                                          SELECT a.dataset_id
                                               , b.dataset_name
                                               , b.header_array
                                               , a.data_query
                                               , c.joinvars
                                               , c.query_alias
                                          FROM data_queries           a
                                            INNER JOIN header_queries b ON a.dataset_id = b.dataset_id
                                            INNER JOIN joinvar_query  c ON a.dataset_id = c.dataset_id
                                        )
              -- final preparation of all elements for the FULL JOIN concatenation
              -- OBS: lag(x) used to JOIN current query to the next ON (q1.x1, q1.x2,...) = (q2.x1, q2.x2,...). 
              , prepare_join_query   AS (
                                          SELECT dataset_id
                                               , dataset_name
                                               , header_array
                                               , data_query
                                               , joinvars
                                               , query_alias
                                               , joinvars_lag
                                               , CASE WHEN joinvars_lag IS NOT NULL THEN join_cond ELSE '' END AS join_cond
                                          FROM (
                                                 SELECT dataset_id
                                                      , dataset_name
                                                      , header_array
                                                      , data_query
                                                      , query_alias
                                                      , joinvars_lag
                                                      , CASE WHEN joinvars_lag IS NOT NULL THEN joinvars ELSE '' END AS joinvars
                                                      , format($q$ ON (%1$s) = (%2$s)$q$, joinvars_lag, joinvars)    AS join_cond
                                                 FROM (
                                                        SELECT dataset_id
                                                             , dataset_name
                                                             , header_array
                                                             , data_query
                                                             , joinvars
                                                             , query_alias
                                                             , lag(joinvars) OVER () AS joinvars_lag
                                                        FROM combine_query
                                                      ) x0
                                               ) x1
                                        )
              -- combine all data-query elements for each dataset
              , prepare_data_query   AS (
                                          SELECT format(
                                            $qry$(%1$s) %2$s %3$s$qry$, a.data_query, query_alias, join_cond) AS singledata_qry
                                               , query_alias
                                               , header_array
                                          FROM prepare_join_query a
                                        )
              -- concatenate headers from each dataset into one array
              , header_query         AS (
                                          SELECT array_agg(header_var ORDER BY a.query_alias,b.hidx) AS header_array
                                          FROM prepare_data_query                   a
                                             , unnest(header_array) WITH ORDINALITY b(header_var, hidx)
                                        )
              -- concatenate into FULL JOIN
              , construct_join_query AS (
                                          SELECT E'SELECT * \nFROM\n ' || string_agg(singledata_qry, E'\n FULL JOIN \n' ORDER BY query_alias) AS data_query
                                          FROM (
                                                 SELECT *
                                                 FROM prepare_data_query
                                               ) x
                                        )
            SELECT a.data_query
                 , b.exportname AS dataset_name
                 , to_json(c.header_array) AS header_array
            FROM construct_join_query a
               , exportname_query     b
               , header_query         c;
        EOD;

    public static string $wholestudyexport_get_studydatatable_query =
        <<<'EOD'
            SELECT jsonb_build_object(
              'studyrow_id', a.studyrow_id
              , 'event_id', a.event_id
              , 'subevent_id', a.subevent_id
              , 'samplingevent_id', a.samplingevent_id
              , 'samplingtime_id', a.samplingtime_id
              , 'center_id', a.center_id
              , 'startgroup_id', a.startgroup_id
              , 'subject_id', a.subject_id) AS studydatatable
            FROM dashin.studydatatable a
            INNER JOIN dashin.study b ON a.study_id = b.study_id
            WHERE (a.study_id,b.owner_org) = (:study_id, :owner_org);
        EOD;

}