<?php

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class StudycreationguideRepository extends RepositoryBase
{

    public function getDatasetInfo($datasetid)
    {

    }

    public function createStudy($params)
    {

        $name = (string)$params["name"];
        $description = (string)$params["description"];
        $title = (string)$params["title"];

        try {
            $sql = StudycreationguideQuery::$study_guide_create_study;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $name,
                "title"       => $title,
                "description" => $description,
                "owner_org"   => $organizationId,
            ]);

            if (isset($result["study_id"])) {
                return $result["study_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Imports a data design into a study
     * from a dataset
     *
     * @param $studyId
     * @param $datasetId
     *
     * @return array|false
     */
    public function importDatadesign($studyId, $datasetId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = StudycreationguideQuery::$study_guide_import_datadesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Validate dataset design against the studydata design
     * 
     * @param $studyId
     * @param $datasetId
     *
     * @return array|false
     */
    public function guideStudydesignValidateData($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = StudycreationguideQuery::$study_guide_validate_datadesign;
            
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    public function studyAddDataset($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = StudycreationguideQuery::$study_guide_add_dataset;
            
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }


}