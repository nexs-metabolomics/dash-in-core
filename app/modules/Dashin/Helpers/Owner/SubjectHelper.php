<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Forms\FormBase;
use Dashin\Forms\Owner\SubjectEditForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Messages\Message;

/**
 * Class SubjectHelper
 *
 * @package Dashin\Helpers\Owner
 */
class SubjectHelper extends HelperBase
{
    /**
     * @var SubjectRepository
     */
    private $_repository;

    /**
     * @return SubjectRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new SubjectRepository();
        }
        return $this->_repository;
    }

    /**
     * @param $studyId
     *
     * @return array|false
     */
    public function createStudysubject($studyId)
    {
        $action = $this->Btn->getAction();
        $form = new SubjectEditForm();
        if ($action === "submit") {
            # don't add form-messages to flashSession
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $data = $this->_getRepository()->createStudysubject($studyId);
                if (isset($data["subject_id"]) && UniqueId::uuidValidate($data["subject_id"])) {
                    $this->localsession->setKeyValue("dashin_owner_subject_id", $data["subject_id"]);
                    $this->response->redirect("/dashin/owner/subject/editstudysubject/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                } elseif (isset($data["error"])) {
                    if ($form->has("name")) {
                        $form->get("name")->appendMessage(new Message($data["error"]));
                    }
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_info" => $studyInfo,
            "form"       => $form,
        ];
    }

    /**
     * @param $studyId
     *
     * @return array|false|string[]
     */
    private function _getStartgroupSelectChoices($studyId)
    {
        $startgroups = $this->_getRepository()->getStartgroups($studyId);
        if ($startgroups) {
            # prepare choices
            $sgNames = array_column($startgroups, "name");
            $sgIds = array_column($startgroups, "startgroup_id");
            array_unshift($sgNames, "");
            array_unshift($sgIds, "");
            $sgChoices = array_combine($sgIds, $sgNames);
        } else {
            $sgChoices = ["" => "No startgroups"];
        }
        return $sgChoices;
    }

    /**
     * @param $data
     * @param $nameColumn
     * @param $idColumn
     * @param $emptyName
     *
     * @return array|false
     */
    private function _prepareChoices($data, $nameColumn, $idColumn, $emptyName)
    {
        if ($data) {
            $names = array_column($data, $nameColumn);
            $ids = array_column($data, $idColumn);
            array_unshift($names, "Not assigned");
            array_unshift($ids, "");
        } else {
            $names = [$emptyName];
            $ids = [0];
        }
        $choices = array_combine($ids, $names);
        return $choices;
    }

    /**
     * @param FormBase $form
     * @param          $addDummy
     * @param          $data
     * @param          $nameColumn
     * @param          $idColumn
     * @param          $emptyName
     * @param          $label
     * @param          $name
     * @param          $value
     */
    private function _addSelectField(FormBase &$form, $addDummy, $data, $nameColumn, $idColumn, $emptyName, $label, $name, $value)
    {
        $choices = $this->_prepareChoices($data, $nameColumn, $idColumn, $emptyName);
        if ($addDummy) {
            $selectFld = new Select("dummy$name", $choices);
            $selectFld->setLabel($label);
            $selectFld->setAttribute("disabled", "disabled");
            $hiddenFld = new Hidden($name, ["value" => $value]);
            $form->add($hiddenFld);
        } else {
            $selectFld = new Select($name, $choices);
            $selectFld->setLabel($label);
        }
        $selectFld->setDefault($value);
        $form->add($selectFld);
    }

    /**
     * @param $subjectId
     * @param $studyId
     *
     * @return array
     */
    public function editStudysubject($subjectId, $studyId)
    {
        # if submit, then update
        $form = new SubjectEditForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $this->_getRepository()->updateStudysubject($subjectId, $studyId);
            }
        }

        # get the subject to be edited
        $subjectInfo = $this->_getRepository()->getStudysubjectMinimal($subjectId, $studyId);
        if ($subjectInfo) {
            #-----------------------------------------------------
            # Add "select startgroup/select center" to form
            #-----------------------------------------------------
            # startgroup
            $startgroups = $this->_getRepository()->getStartgroups($studyId);
            $this->_addSelectField($form, ($subjectInfo["n_datasets"] > 0), $startgroups, "name", "startgroup_id", "No startgroups", "Select startgroup", "startgroup", $subjectInfo["startgroup_id"]);
            # center
            $centers = $this->_getRepository()->getCenters($studyId);
            $this->_addSelectField($form, ($subjectInfo["n_datasets"] > 0), $centers, "name", "center_id", "No centers", "Select center", "center", $subjectInfo["center_id"]);

            $form->bindValues($subjectInfo);
            $form->setReadonly("name", $subjectInfo["n_datasets"] > 0);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"         => $form,
            "study_info"   => $studyInfo,
            "subject_info" => $subjectInfo,
        ];
    }

    /**
     * @param $page
     * @param $nrows
     * @param $reset
     * @param $studyId
     *
     * @return array
     */
    public function listStudysubjects($page, $nrows, $reset, $studyId)
    {
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        # initialize subjectId (used to signal deletion/confirmation)
        $subjectId = false;
        $action = $this->Btn->getAction();
        if ($action === "update") {
            $subjectId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($subjectId)) {
                $this->_getRepository()->updateStudysubjectStartgroup($subjectId, $studyId);
            }
        } elseif ($action === "updateall") {
            $this->_getRepository()->updateallStudysubjectStartgroup($studyId);
        } elseif ($action === "delete") {
            $subjectId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($subjectId)) {
                $this->setKeyValue("dashin_owner_delete_studysubject", $subjectId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_studysubject");
            }
        } elseif ($action === "confirm") {
            $subjectId = $this->Btn->getValue();
            $subjectIdSaved = $this->getKeyValue("dashin_owner_delete_studysubject");
            if ($subjectId === $subjectIdSaved) {
                $ok = $this->_getRepository()->deleteSubject($subjectId, $studyId);
                if ($ok) {
                    $this->removeKeyValue("dashin_owner_delete_studysubject");
                    $this->flashSession->success("Subject successfully deleted");
                } else {
                    $this->removeKeyValue("dashin_owner_delete_studysubject");
                    $this->flashSession->warning("Could not delete subject");
                    $subjectId = false;
                }
            } else {
                $this->removeKeyValue("dashin_owner_delete_studysubject");
                $this->flashSession->warning("Could not delete subject");
                $subjectId = false;
            }
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_studysubjects_search_term", $reset);
        $subjects = $this->_getRepository()->getStudysubjectsPaginated($page, $nrows, $searchTerm, $studyId);
        if (isset($subjects->items)) {
            $sgChoices = $this->_getStartgroupSelectChoices($studyId);
            $i = 0;
            foreach ($subjects->items as &$item) {
                if ($item->startgroup_is_locked === 0) {
                    $sgSelectFld = new Select("startgroup[" . $item->subject_id . "]", $sgChoices, ["onchange" => " this.dataset.changed = 'changed'; "]);
                    $sgSelectFld->setDefault($item->startgroup_id);
                    $sgHiddenFld = "";
                } else {
                    $sgSelectFld = new Select("dummy", $sgChoices, ["form" => "form1", "onchange" => " this.dataset.changed = 'changed'; "]);
                    $sgSelectFld->setAttribute("disabled", "disabled");
                    $sgSelectFld->setDefault($item->startgroup_id);
                    $sgHiddenFld = new Hidden("startgroup[" . $item->subject_id . "]", ["value" => $item->startgroup_id]);
                }
                $item->confirm = ($item->subject_id === $subjectId);

                $sgSelectFld->setLabel("Select startgroup");
                $item->startgroup_select = $sgSelectFld;
                $item->hidden_field = $sgHiddenFld;
                $item->link_target = "#row" . max(0, $i - 3);
                $i++;
            }
            unset($i);
        }

        return [
            "study_info"  => $studyInfo,
            "subjects"    => $subjects,
            "search_term" => $searchTerm,
        ];
    }
}