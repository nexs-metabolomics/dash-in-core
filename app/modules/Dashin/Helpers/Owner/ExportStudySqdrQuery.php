<?php

namespace Dashin\Helpers\Owner;

class ExportStudySqdrQuery
{
    public static string $export_sqdr_study_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT study_id
                       , name
                       , title
                       , description
                       , start_date
                       , endpoint
                       , objectives
                       , conclusion
                       , exclusion
                       , inclusion
                       , institute
                       , country_id
                       , consortium_id
                       , published
                       , researchdesign_id
                       , num_treat
                       , num_factor
                       , num_arm
                       , researchdesign_text
                       , num_volun
                       , num_volun_terminated
                       , recruit_start_year
                       , recruit_end_year
                       , blinding
                       , blinding_method
                       , created_at
                       , updated_at
                       , details
                FROM %1$I.study;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_dataset_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , name
                       , description
                       , status
                       , datasettype_id
                       , created_at
                       , updated_at
                       , details
                FROM %1$I.dataset;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_assay_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT assay_id
                       , name
                       , description
                       , researchfield_id
                       , created_at
                       , updated_at
                FROM %1$I.assay;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_datatable_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT datarow_id
                       , datarow_ordinal
                       , studyrow_id
                       , dataset_id
                       , study_id
                       , datasetevent_id
                       , datasetsubevent_id
                       , datasetsamplingevent_id
                       , datasetsamplingtime_id
                       , datasetcenter_id
                       , datasetstartgroup_id
                       , datasetsubject_id
                       , datasetevent_name
                       , datasetsubevent_name
                       , datasetsamplingevent_name
                       , datasetsamplingtime_name
                       , datasetcenter_name
                       , datasetstartgroup_name
                       , datasetsubject_name
                       , datasetevent_status
                       , datasetsubevent_status
                       , datasetsamplingevent_status
                       , datasetsamplingtime_status
                       , datasetcenter_status
                       , datasetstartgroup_status
                       , datasetsubject_status
                       , studyevent_id
                       , studysubevent_id
                       , studysamplingevent_id
                       , studysamplingtime_id
                       , studycenter_id
                       , studystartgroup_id
                       , studysubject_id
                       , datarow
                FROM %1$I.datadesign_datatable;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_center_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_center_id
                       , study_id
                       , study_center_id
                FROM %1$I.datadesign_x_study_center;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_event_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_event_id
                       , study_id
                       , study_event_id
                FROM %1$I.datadesign_x_study_event;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_samplingevent_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_samplingevent_id
                       , study_id
                       , study_samplingevent_id
                FROM %1$I.datadesign_x_study_samplingevent;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_samplingtime_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_samplingtime_id
                       , study_id
                       , study_samplingtime_id
                FROM %1$I.datadesign_x_study_samplingtime;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_startgroup_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_startgroup_id
                       , study_id
                       , study_startgroup_id
                FROM %1$I.datadesign_x_study_startgroup;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_subevent_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_subevent_id
                       , study_id
                       , study_subevent_id
                FROM %1$I.datadesign_x_study_subevent;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_datadesign_x_study_subject_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT dataset_id
                       , datadesign_subject_id
                       , study_id
                       , study_subject_id
                FROM %1$I.datadesign_x_study_subject;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_center_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT center_id
                FROM %1$I.study_center;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_event_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT event_id
                FROM %1$I.study_event;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_samplingevent_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT samplingevent_id
                FROM %1$I.study_samplingevent;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_samplingtime_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT samplingtime_id
                FROM %1$I.study_samplingtime;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_startgroup_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT startgroup_id
                FROM %1$I.study_startgroup;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_subevent_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT subevent_id
                FROM %1$I.study_subevent;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_study_subject_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT subject_id
                FROM %1$I.study_subject;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_studydesign_datatable_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT studyrow_id
                       , study_id
                       , studyevent_id
                       , studysubevent_id
                       , studysamplingevent_id
                       , studysamplingtime_id
                       , studycenter_id
                       , studystartgroup_id
                       , studysubject_id
                       , studyevent_ordinal
                       , studyevent_created_at
                       , studyevent_updated_at
                       , studyevent_name
                       , studyevent_label
                       , studyevent_description
                       , studysubevent_subeventtype_id
                       , studysubevent_ordinal
                       , studysubevent_intervention_type_id
                       , studysubevent_created_at
                       , studysubevent_updated_at
                       , studysubevent_name
                       , studysubevent_label
                       , studysubevent_description
                       , studysubevent_row_comment
                       , studysubevent_additional_data
                       , studysamplingevent_samplingtype_id
                       , studysamplingevent_ordinal
                       , studysamplingevent_created_at
                       , studysamplingevent_updated_at
                       , studysamplingevent_name
                       , studysamplingevent_label
                       , studysamplingevent_description
                       , studysamplingevent_row_comment
                       , studysamplingevent_additional_data
                       , studysamplingtime_ordinal_time
                       , studysamplingtime_created_at
                       , studysamplingtime_updated_at
                       , studysamplingtime_name
                       , studysamplingtime_label
                       , studysamplingtime_description
                       , studycenter_ordinal
                       , studycenter_created_at
                       , studycenter_updated_at
                       , studycenter_name
                       , studycenter_label
                       , studycenter_description
                       , studystartgroup_ordinal
                       , studystartgroup_created_at
                       , studystartgroup_updated_at
                       , studystartgroup_name
                       , studystartgroup_label
                       , studystartgroup_description
                       , studysubject_name
                       , studysubject_label
                       , studysubject_description
                       , studysubject_status
                       , studysubject_created_at
                       , studysubject_updated_at
                       , studysubject_details
                FROM %1$I.studydesign_datatable;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_var_dataset_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT var_dataset_id
                       , dataset_id
                       , name
                       , description
                       , status
                       , created_at
                       , updated_at
                       , details
                FROM %1$I.var_dataset;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_var_datatable_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT variable_id
                       , var_dataset_id
                       , ordinal
                       , status
                       , created_at
                       , updated_at
                       , details
                       , datarow
                FROM %1$I.var_datatable;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_var_variable_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT var_variable_id
                       , var_dataset_id
                       , local_id
                       , name
                       , description
                       , status
                       , search_single_id
                       , search_generic_id
                       , datatype_id
                       , created_at
                       , updated_at
                       , details
                FROM %1$I.var_variable;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;

    public static string $export_sqdr_variable_sql =
        <<<'EOD'
            SELECT format(
              $qry$
                  SELECT variable_id
                       , local_id
                       , dataset_id
                       , name
                       , description
                       , status
                       , variabletype_id
                       , datatype_id
                       , researchfield_id
                       , assay_id
                       , nunitprefix_id
                       , nunit_id
                       , dunitprefix_id
                       , dunit_id
                       , created_at
                       , updated_at
                       , details
                FROM %1$I.variable;
              $qry$, (:schemaname)::REGNAMESPACE) AS sqlquery;
        EOD;
    
    public static string $export_sqdr_supportfile_paths =
        <<<'EOD'
            SELECT a.name
                 , 'organizations/' || c.owner_org::TEXT || '/studies/' || b.study_id::TEXT || '/supportfiles/' || a.filename AS path
            FROM dashin.supportfile                 a
              INNER JOIN dashin.study_x_supportfile b ON a.supportfile_id = b.supportfile_id
              INNER JOIN dashin.study               c ON b.study_id = c.study_id
            WHERE b.study_id = :study_id;
        EOD;

}