<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-14
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Forms\ApplicationFormBase;
use App\Library\Utils\Json;
use App\Library\Utils\JsonException;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\SamplingeventSubformBodyfluidForm;
use Dashin\Forms\Owner\SamplingeventSubformExcretionForm;
use Dashin\Forms\Owner\SamplingeventSubformOrganForm;
use Dashin\Forms\Owner\SamplingeventSubformPhysiologyForm;
use Dashin\Forms\Owner\StudydesignAddCenterForm;
use Dashin\Forms\Owner\StudydesignAddSubeventForm;
use Dashin\Forms\Owner\StudydesignCreateEventForm;
use Dashin\Forms\Owner\StudydesignAddSamplingEventForm;
use Dashin\Forms\Owner\StudydesignAddStartgroupForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Forms\Element\Select;

class StudydesignHelper extends HelperBase
{
    /**
     * @var StudydesignRepository
     */
    private $_repository;

    /**
     * @return StudydesignRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new StudydesignRepository();
        }
        return $this->_repository;
    }

    /**
     * @param $studyId
     *
     * @return array|false
     */
    public function createCenter($studyId)
    {
        $form = new StudydesignAddCenterForm();
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $centerId = $this->_getRepository()->createCenter($studyId);
                if ($centerId) {
                    $this->localsession->setKeyValue("dashin_owner_center_id", $centerId);
                    $this->response->redirect("/dashin/owner/studydesign/editcenter/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    public function editCenter($centerId, $studyId)
    {
        $form = new StudydesignAddCenterForm();
        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $ok = $this->_getRepository()->updateCenter($centerId, $studyId);
                if ($ok && $action === "submit") {
                    $this->response->redirect("dashin/owner/studydesign/listcenters/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }
        $center = $this->_getRepository()->getCenter($centerId,$studyId);
        if ($center) {
            $form->bindValues($center);
            $form->setReadonly("name", $center["n_datasets"] > 0);
        } else {
            $form = false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"        => $form,
            "study_info"  => $studyInfo,
            "center_info" => $center,
        ];
    }

    public function listCenters($studyId)
    {
        $action = $this->Btn->getAction();
        if($action === "edit") {
            $centerId = $this->Btn->getValue();
            if(UniqueId::uuidValidate($centerId)) {
                $this->localsession->setKeyValue("dashin_owner_center_id",$centerId);
                $this->response->redirect("/dashin/owner/studydesign/editcenter/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }
        $data = $this->_getRepository()->getCenters($studyId);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        return [
            "centers"    => $data,
            "study_info" => $studyInfo,
        ];
    }

    /**
     * @param $studyId
     *
     * @return array|false
     */
    public function createStartgroup($studyId)
    {
        $form = new StudydesignAddStartgroupForm();
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $startgroupId = $this->_getRepository()->createStartgroup($studyId);
                if ($startgroupId) {
                    $this->localsession->setKeyValue("dashin_owner_startgroup_id", $startgroupId);
                    $this->response->redirect("/dashin/owner/studydesign/editstartgroup/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    public function editStartgroup($startgroupId, $studyId)
    {
        $form = new StudydesignAddStartgroupForm();
        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $ok = $this->_getRepository()->updateStartgroup($startgroupId, $studyId);
                if ($ok && $action === "submit") {
                    $this->response->redirect("dashin/owner/studydesign/liststartgroups/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }
        $startgroup = $this->_getRepository()->getStartgroup($startgroupId,$studyId);
        if ($startgroup) {
            $form->bindValues($startgroup);
            $form->setReadonly("name", $startgroup["n_datasets"] > 0);
        } else {
            $form = false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"            => $form,
            "study_info"      => $studyInfo,
            "startgroup_info" => $startgroup,
        ];
    }

    public function listStartgroups($studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $startgroupId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->setKeyValue("dashin_owner_delete_startgroup", $startgroupId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_startgroup");
            }
        } elseif ($action === "confirm") {
            $startgroupId = $this->Btn->getValue();
            $startgroupIdSaved = $this->getKeyValue("dashin_owner_delete_startgroup");
            if ($startgroupId === $startgroupIdSaved) {
                $this->_getRepository()->deleteStartgroup($startgroupId);
            }
            $this->removeKeyValue("dashin_owner_delete_startgroup");
            $this->localsession->removeKeyValue("dashin_owner_startgropu_id");
            $startgroupId = false;
        } elseif ($action === "edit") {
            $startgroupId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->localsession->setKeyValue("dashin_owner_startgroup_id", $startgroupId);
                $this->response->redirect("dashin/owner/studydesign/editstartgroup/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
            $this->removeKeyValue("dashin_owner_delete_startgroup");
            $this->localsession->removeKeyValue("dashin_owner_startgropu_id");
            $startgroupId = false;
        } else {
            $startgroupId = false;
        }

        $data = $this->_getRepository()->getStartgroups($studyId);
        if ($data) {
            foreach ($data as &$row) {
                if ($row["startgroup_id"] === $startgroupId) {
                    $row["confirm"] = true;
                } else {
                    $row["confirm"] = false;
                }
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "startgroups" => $data,
            "study_info"  => $studyInfo,
        ];
    }

    public function createEvent($studyId)
    {
        $form = new StudydesignCreateEventForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $eventId = $this->_getRepository()->createEvent($studyId);
                if ($eventId) {
                    $this->localsession->setKeyValue("dashin_owner_event_id", $eventId);
                    $this->response->redirect("/dashin/owner/studydesign/editevent/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    private function _setReadonly(ApplicationFormBase &$form, $fieldName, $criteria)
    {
        if ($criteria && $form->has($fieldName)) {
            $form->get("name")->setAttribute("readonly", "readonly");
        }
    }

    /**
     * Edit an event
     * 
     * @param $eventId
     * @param $studyId
     *
     * @return array
     */
    public function editEvent($eventId, $studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "remove") {
            $subeventId = $this->Btn->getValue();
            $this->_getRepository()->removeSubeventFromEvent($eventId, $studyId, $subeventId);
        } elseif ($action === "add") {
            $subeventId = $this->Btn->getValue();
            $this->_getRepository()->addSubeventToEvent($eventId, $studyId, $subeventId);
        }

        $form = new StudydesignCreateEventForm();
        if ($action === "submit" || $action === "remove" || $action === "add" || $action === "apply") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $ok = $this->_getRepository()->updateEvent($eventId, $studyId);
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $eventData = $this->_getRepository()->getEvent($eventId, $studyId);
        if ($eventData) {
            # don't allow editing name if linked to dataset
            $form->bindValues($eventData);
            $form->setReadonly("name", $eventData["n_datasets"] > 0);
        } else {
            $form = false;
        }

        $subeventsSelected = $this->_getRepository()->getSubeventsInEvent($eventId, $studyId);
        if ($subeventsSelected) {
            foreach ($subeventsSelected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }
        $subeventsUnselected = $this->_getRepository()->getSubeventsNotInEvent($eventId, $studyId);
        if ($subeventsUnselected) {
            foreach ($subeventsUnselected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        return [
            "form"                 => $form,
            "study_info"           => $studyInfo,
            "event_info"           => $eventData,
            "subevents_selected"   => $subeventsSelected,
            "subevents_unselected" => $subeventsUnselected,
        ];
    }

    /**
     * List events
     *
     * @param $studyId
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array|bool
     */
    public function listEvents($studyId, $page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $eventId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($eventId)) {
                $this->localsession->setKeyValue("dashin_owner_event_id", $eventId);
                $this->response->redirect("/dashin/owner/studydesign/editevent/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "delete") {
            $eventId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($eventId)) {
                $this->setKeyValue("dashin_owner_delete_event", $eventId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_event");
            }
        } elseif ($action === "confirm") {
            $eventId = $this->Btn->getValue();
            $eventIdSaved = $this->getKeyValue("dashin_owner_delete_event");
            if ($eventId === $eventIdSaved) {
                $this->_getRepository()->deleteEvent($eventId, $studyId);
            }
            $this->removeKeyValue("dashin_owner_delete_event");
            $this->localsession->removeKeyValue("dashin_owner_event_id");
            $eventId = false;
        } else {
            $eventId = false;
        }

        $study = $this->_getRepository()->getStudyInfo($studyId);
        $searchTerm = $this->getSearchTerm("dashin_owner_list_events_search_term", $reset);
        $data = $this->_getRepository()->getEventPaginated($page, $nrows, $searchTerm);
        if ($data->items) {
            foreach ($data->items as &$item) {
                $item->confirm = ($item->event_id === $eventId);
            }
        }
        return [
            "events"      => $data,
            "study"       => $study,
            "search_term" => $searchTerm,
        ];
    }

    /**
     * Get form options for subevent form
     *
     * @param      $studyId
     * @param      $subeventId
     * @param null $subeventTypeId
     *
     * @return array
     * @throws JsonException
     */
    private function _getSubeventFormOptions($studyId, $subeventId = null, $subeventTypeId = null)
    {
        $subeventTypes = $this->_getRepository()->getSubeventTypes();
        if (is_array($subeventTypes)) {
            $at = array_combine(
                array_column($subeventTypes, "subevent_type_id"),
                array_column($subeventTypes, "description")
            );
        }

        $actionSubtypes = $this->_getRepository()->getActionSubtypes();
        if (is_array($actionSubtypes)) {
            $st = array_combine(
                array_column($actionSubtypes, "intervention_type_id"),
                array_column($actionSubtypes, "name")
            );
        }

        if ($this->request->has("subevent_type")) {
            $subeventTypeId = $this->request->getPost("subevent_type");
        }

        # get template for additional fields for this subevent-type
        $additionalFields = null;
        if ($subeventTypeId) {
            $idx = array_search($subeventTypeId, array_column($subeventTypes, "subevent_type_id"));
            $additionalDataSchemaJson = $subeventTypes[$idx]["additional_data_schema"];
            $tmpFields = Json::jsonDecode($additionalDataSchemaJson, JSON_OBJECT_AS_ARRAY);
            $additionalFields = $tmpFields["fields"];
        }

        $selectionChoices = $this->_getRepository()->getSelectionChoices();

        return [
            "subevent_type"     => $at,
            "action_type"       => $st,
            "subevent_type_id"  => $subeventTypeId,
            "selection_choices" => $selectionChoices,
            "additional_fields" => $additionalFields,
        ];
    }

    /**
     * Add subevent
     *
     * @param $studyId
     *
     * @return array|bool
     * @throws JsonException
     */
    public function createSubevent($studyId)
    {
        $options = $this->_getSubeventFormOptions($studyId);
        $form = new StudydesignAddSubeventForm($options);
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $additionalFieldNames = array_keys($options["additional_fields"]);
                $subeventTypeId = $options["subevent_type_id"];
                $subeventId = $this->_getRepository()->createSubevent($studyId, $additionalFieldNames, $subeventTypeId);
                if ($subeventId) {
                    $this->localsession->setKeyValue("dashin_owner_subevent_id", $subeventId);
                    $this->response->redirect("/dashin/owner/studydesign/editsubevent/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    /**
     * Bind values from dynamic fields to the form
     *
     * @param $form
     * @param $subeventData
     */
    private function _bindAditionalFields(&$form, $subeventData)
    {
        if (!$form) {
            return;
        }
        if (isset($subeventData["additional_data"]) && $subeventData["additional_data"]) {
            if (is_array($subeventData["additional_data"]["fields"])) {
                foreach ($subeventData["additional_data"]["fields"] as $key => $value) {
                    if ($form->has($key)) {
                        if ($form->get($key) instanceof Select) {
                            $form->get($key)->setDefault($value);
                        } else {
                            $form->get($key)->setAttribute("value", $value);
                        }
                    }
                }
            }
        }
        echo "";
    }

    /**
     * Edit subevent
     *
     * @param $subeventId
     * @param $eventId
     * @param $studyId
     *
     * @return array
     * @throws JsonException
     */
    public function editSubevent($subeventId, $eventId, $studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "createsamplingevent") {
            $this->response->redirect("/dashin/owner/studydesign/createsubevent/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } elseif ($action === "remove") {
            $samplingeventId = $this->Btn->getValue();
            $this->_getRepository()->removeSamplingEventFromSubevent($subeventId, $studyId, $samplingeventId);
        } elseif ($action === "add") {
            $samplingeventId = $this->Btn->getValue();
            $this->_getRepository()->addSamplingEventToSubevent($subeventId, $studyId, $samplingeventId);
        }

        $subeventTypeId = $this->_getRepository()->getSubeventTypeIdFromSubeventId($subeventId);
        $options = $this->_getSubeventFormOptions($studyId, $subeventId, $subeventTypeId);
        $form = new StudydesignAddSubeventForm($options);
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $additionalFieldsJson = $this->_getAdditionalFieldsAsJson($options["additional_fields"]);
                $ok = $this->_getRepository()->updateSubevent($subeventId, $studyId, $additionalFieldsJson, $subeventTypeId);
            }
        }

        # pupulate form
        $subevent = $this->_getRepository()->getSubevent($subeventId, $studyId);
        if ($subevent) {
            $form->bindValues($subevent);
            if ($form->has("subevent_type")) {
                $form->get("subevent_type")->setDefault($subevent["subevent_type_id"]);
            }
            if ($form->has("action_type")) {
                $form->get("action_type")->setDefault($subevent["intervention_type_id"]);
            }
            $this->_bindAditionalFields($form, $subevent);
            $form->setReadonly("name", $subevent["n_datasets"] > 0);
        } else {
            $form = false;
        }

        $samplingEventsSelected = $this->_getRepository()->getSamplingEventsBySubevent($subeventId, $studyId);
        if ($samplingEventsSelected) {
            foreach ($samplingEventsSelected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }
        $samplingEventsUnselected = $this->_getRepository()->getSamplingEventsNotInSubevent($subeventId, $studyId);
        if ($samplingEventsUnselected) {
            foreach ($samplingEventsUnselected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $eventInfo = $this->_getRepository()->getEvent($eventId, $studyId);

        return [
            "form"                       => $form,
            "study_info"                 => $studyInfo,
            "event_info"                 => $eventInfo,
            "subevent_info"              => $subevent,
            "sampling_events_selected"   => $samplingEventsSelected,
            "sampling_events_unselected" => $samplingEventsUnselected,
        ];
    }

    public function listSubevents($studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $subeventId = $this->Btn->getValue();
            $this->localsession->setKeyValue("dashin_owner_subevent_id", $subeventId);
            $this->response->redirect("dashin/owner/studydesign/editsubevent/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } elseif ($action === "delete") {
            $subeventId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($subeventId)) {
                $this->setKeyValue("dashin_owner_delete_subevent", $subeventId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_subevent");
            }
        } elseif ($action === "confirm") {
            $subeventId = $this->Btn->getValue();
            $subeventIdSaved = $this->getKeyValue("dashin_owner_delete_subevent");
            if ($subeventId === $subeventIdSaved) {
                $this->_getRepository()->deleteSubevent($subeventId, $studyId);
            }
            $this->removeKeyValue("dashin_owner_delete_subevent");
            $this->localsession->removeKeyValue("dashin_owner_subevent_id");
            $subeventId = false;
        } else {
            $subeventId = false;
        }

        $data = $this->_getRepository()->getSubevents($studyId);
        if ($data) {
            foreach ($data as &$row) {
                $row["confirm"] = ($row["subevent_id"] === $subeventId);
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "subevents"  => $data,
            "study_info" => $studyInfo,
        ];
    }

    /**
     * Get form options for sampling event form
     *
     * @param null $samplingTypeId
     * @param bool $isEdit
     *
     * @return array
     * @throws JsonException
     */
    private function _getSamplingFormOptions($samplingTypeId = null, $isEdit = false)
    {
        $samplingTypes = $this->_getRepository()->getSamplingTypes();
        if (is_array($samplingTypes)) {
            $samplingTypesOut = array_combine(
                array_column($samplingTypes, "sampling_type_id"),
                array_column($samplingTypes, "description")
            );
        }

        if ($this->request->has("sampling_type")) {
            $samplingTypeId = $this->request->getPost("sampling_type");
        }
        $additionalFields = null;
        if ($samplingTypeId !== null) {
            $res = array_search($samplingTypeId, array_column($samplingTypes, "sampling_type_id"));
            $additionalDataSchemaJson = $samplingTypes[$res]["additional_data_schema"];
            $tmpFields = Json::jsonDecode($additionalDataSchemaJson, JSON_OBJECT_AS_ARRAY);
            $additionalFields = $tmpFields["fields"];
        }

        $selectionChoices = $this->_getRepository()->getSelectionChoices();

        return [
            "sampling_type"     => isset($samplingTypesOut) ? $samplingTypesOut : null,
            "sampling_type_id"  => $samplingTypeId,
            "selection_choices" => $selectionChoices,
            "additional_fields" => $additionalFields,
            "is_edit"           => $isEdit,
        ];
    }

    /**
     * Add a startgroup to a subevent
     *
     * @param $eventId
     * @param $subeventId
     * @param $studyId
     *
     * @return array
     * @throws JsonException
     */
    public function addStartgroupToSubevent($eventId, $subeventId, $studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "remove") {
            $startgroupId = $this->Btn->getValue();
            $this->_getRepository()->removeStartgroupFromEventSubevent($eventId, $subeventId, $studyId, $startgroupId);
        } elseif ($action === "add") {
            $startgroupId = $this->Btn->getValue();
            $this->_getRepository()->addStartgroupToEventSubevent($eventId, $subeventId, $studyId, $startgroupId);
        }

        if ($action === "editgroups" || $action === "remove" || $action === "add") {
            $startgroupsSelected = $this->_getRepository()->getStartgroupsByEventSubevent($eventId, $subeventId, $studyId);
            if ($startgroupsSelected) {
                foreach ($startgroupsSelected as &$row) {
                    $row = (object)$row;
                }
                unset($row);
            }
            $startgroupsUnselected = $this->_getRepository()->getStartgroupsNotInEventSubevent($eventId, $subeventId, $studyId);
            if ($startgroupsUnselected) {
                foreach ($startgroupsUnselected as &$row) {
                    $row = (object)$row;
                }
                unset($row);
            }
        } else {
            $startgroupsSelected = false;
            $startgroupsUnselected = false;
            $eventSubeventInfo = false;
        }

        $subevents = $this->_getRepository()->getSubeventsByEvent($eventId, $studyId);
        if ($subevents) {
            foreach ($subevents as &$row) {
                $row["current"] = (($row["subevent_id"] === $subeventId) ? " current" : "");
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $eventInfo = $this->_getRepository()->getEvent($eventId, $studyId);
        $subeventInfo = $this->_getRepository()->getSubevent($subeventId, $studyId);

        return [
            "startgroups_selected"   => $startgroupsSelected,
            "startgroups_unselected" => $startgroupsUnselected,
            "study_info"             => $studyInfo,
            "event_info"             => $eventInfo,
            "subevent_info"          => $subeventInfo,
            "subevents"              => $subevents,
        ];
    }

    /**
     * Add sampling event to action event
     *
     * @param $subeventId
     * @param $eventId
     * @param $studyId
     *
     * @return array
     * @throws JsonException
     */
    public function createSamplingEvent($studyId)
    {
        # get options for form
        $samplingTypeId = $this->request->getPost("sampling_type");
        $options = $this->_getSamplingFormOptions($samplingTypeId);
        $form = new StudydesignAddSamplingEventForm($options);

        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $additionalFieldsJson = $this->_getAdditionalFieldsAsJson($options["additional_fields"]);
                $samplingeventId = $this->_getRepository()->createSamplingEvent($studyId, $samplingTypeId, $additionalFieldsJson);
                if ($samplingeventId) {
                    $this->localsession->setKeyValue("dashin_owner_samplingevent_id", $samplingeventId);
                    $this->response->redirect("/dashin/owner/studydesign/editsampling/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    private function _samplingSubform($samplingeventId)
    {
        $samplingTypeId = $this->request->getPost("sampling_type");
        if ($samplingTypeId === null) {
            $samplingTypeId = $this->_getRepository()->getSamplingTypeIdFromSamplingEventId($samplingeventId);
        }
        $samplingTypeId = (int)$samplingTypeId;
        if (in_array($samplingTypeId, [1, 2, 3, 4])) {
            if ($samplingTypeId === 1) {
                return new SamplingeventSubformPhysiologyForm();
            } elseif ($samplingTypeId === 2) {
                return new SamplingeventSubformBodyfluidForm();
            } elseif ($samplingTypeId === 3) {
                return new SamplingeventSubformExcretionForm();
            } elseif ($samplingTypeId === 4) {
                return new SamplingeventSubformOrganForm();
            }
        }
    }

    public function editSamplingevent($samplingeventId, $studyId)
    {
        $samplingTypeId = $this->request->getPost("sampling_type");
        if ($samplingTypeId === null) {
            $samplingTypeId = $this->_getRepository()->getSamplingTypeIdFromSamplingEventId($samplingeventId);
        }
        $options = $this->_getSamplingFormOptions($samplingTypeId, true);
        $options["sampling_type_id"] = $samplingTypeId;

        $form = new StudydesignAddSamplingEventForm($options);
        $subform = $this->_samplingSubform($samplingeventId);

        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply" || $action === "addtime" || $action === "removetime") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $additionalFieldsJson = $this->_getAdditionalFieldsAsJson($options["additional_fields"]);
                $ok = $this->_getRepository()->updateSamplingEvent($samplingeventId, $studyId, $samplingTypeId, $additionalFieldsJson);
                if ($ok) {
                    $this->_getRepository()->updateSamplingtimes($samplingeventId);
                }
                if ($ok && $action === "submit") {
                    $this->response->redirect("dashin/owner/studydesign/listsamplingevents/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        if ($action === "addtime") {
            $this->_getRepository()->addSamplingtime($samplingeventId);
        } elseif ($action === "removetime") {
            $this->_getRepository()->removeSamplingtime($samplingeventId, $studyId);
        } elseif ($action === "moveup") {
            $samplingtimeId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($samplingtimeId)) {
                $this->_getRepository()->reorderSamplingtimeCategories($samplingtimeId, $samplingeventId, $studyId, "up");
            }
        } elseif ($action === "movedown") {
            $samplingtimeId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($samplingtimeId)) {
                $this->_getRepository()->reorderSamplingtimeCategories($samplingtimeId, $samplingeventId, $studyId, "down");
            }
        }
        # pupulate form
        $samplingEvent = $this->_getRepository()->getSamplingevent($samplingeventId, $studyId);
        if ($samplingEvent) {
            $form->bindValues($samplingEvent);
            if ($form->has("sampling_type")) {
                $form->get("sampling_type")->setDefault($samplingEvent["sampling_type_id"]);
            }
            $this->_bindAditionalFields($subform, $samplingEvent);
            $form->setReadonly("name", $samplingEvent["n_datasets"] > 0);

            $samplingTimes = $this->_getRepository()->getSamplingtimes($samplingeventId, $studyId);
            if ($samplingTimes) {
                $n = count($samplingTimes);
                $i = 0;
                foreach ($samplingTimes as &$row) {
                    $row["index0"] = $i;
                    $row["index"] = $i + 1;
                    $row["first"] = ($i == 0);
                    $row["last"] = ($i == $n - 1);
                    $row = (object)$row;
                    ++$i;
                }
                unset($row, $i, $n);
            }
        } else {
            $form = false;
            $samplingTimes = false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"                => $form,
            "subform"             => $subform,
            "study_info"          => $studyInfo,
            "sampling_event_info" => $samplingEvent,
            "sampling_times"      => $samplingTimes,
        ];
    }

    public function listSamplingevents($studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $samplingeventId = $this->Btn->getValue();
            $this->localsession->setKeyValue("dashin_owner_samplingevent_id", $samplingeventId);
            $this->response->redirect("dashin/owner/studydesign/editsampling/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } elseif ($action === "delete") {
            $samplingeventId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($samplingeventId)) {
                $this->setKeyValue("dashin_owner_delete_samplingevent", $samplingeventId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_samplingevent");
            }
        } elseif ($action === "confirm") {
            $samplingeventId = $this->Btn->getValue();
            $samplingeventIdSaved = $this->getKeyValue("dashin_owner_delete_samplingevent");
            if ($samplingeventId === $samplingeventIdSaved) {
                $this->_getRepository()->deleteSamplingevent($samplingeventId, $studyId);
            }
            $this->removeKeyValue("dashin_owner_delete_samplingevent");
            $this->localsession->removeKeyValue("dashin_owner_samplingevent_id");
            $samplingeventId = false;
        }

        $data = $this->_getRepository()->getSamplingEvents($studyId);
        if ($data) {
            foreach ($data as &$row) {
                $row["confirm"] = ($row["samplingevent_id"] == $samplingeventId);
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "sampling_events" => $data,
            "study_info"      => $studyInfo,
        ];
    }

    public function addSamplingEventToSubevent($subeventId, $eventId, $studyId)
    {
        $action = $this->Btn->getAction();
        if ($action === "remove") {
            $startgroupId = $this->Btn->getValue();
            $this->_getRepository()->removeSamplingEventFromSubevent($subeventId, $studyId, $startgroupId);
        } elseif ($action === "add") {
            $startgroupId = $this->Btn->getValue();
            $this->_getRepository()->addSamplingEventToSubevent($subeventId, $studyId, $startgroupId);
        }

        $samplingEventsSelected = $this->_getRepository()->getSamplingEventsBySubevent($subeventId, $studyId);
        if ($samplingEventsSelected) {
            foreach ($samplingEventsSelected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }
        $samplingEventsUnselected = $this->_getRepository()->getSamplingEventsNotInSubevent($subeventId, $studyId);
        if ($samplingEventsUnselected) {
            foreach ($samplingEventsUnselected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $eventInfo = $this->_getRepository()->getEvent($eventId, $studyId);
        $subeventInfo = $this->_getRepository()->getSubevent($subeventId, $studyId);

        return [
            "sampling_events_selected"   => $samplingEventsSelected,
            "sampling_events_unselected" => $samplingEventsUnselected,
            "study_info"                 => $studyInfo,
            "event_info"                 => $eventInfo,
            "subevent_info"              => $subeventInfo,
        ];
    }

    public function importDatadesign($studyId, $datasetId)
    {
        $action = $this->Btn->getAction();
        $confirmImport = false;
        if ($action === "import") {
            if (UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_studydesign_import_datadesign", [$studyId, $datasetId]);
                $confirmImport = true;
            } else {
                $this->removeKeyValue("owner_studydesign_import_datadesign");
            }
        } elseif ($action === "confirm") {
            $IdsSaved = $this->getKeyValue("owner_studydesign_import_datadesign");
            if ($IdsSaved[0] === $studyId && $IdsSaved[1] === $datasetId) {
                $importResult = $this->_getRepository()->studydesignImportDatadesign($studyId, $datasetId);
                if ($importResult) {
                    $this->flashSession->success("Design successfully imported");
                }
            }
            $this->removeKeyValue("owner_studydesign_import_datadesign");
        }

        $showImportbtn = false;

        $designImported = $this->_getRepository()->studydesignDatadesignIsImported($studyId, $datasetId);
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        $designExists = $this->_getRepository()->datadesignEsists($datasetId);
        if (!$designExists) {
            return [
                "dataset_info"   => $datasetInfo,
                "study_info"     => $studyInfo,
                "design_exists"  => $designExists,
                "show_importbtn" => $showImportbtn,
            ];
        }

        # @formatter:off
        # summary
        $datadesignSummary = $this->_getRepository()->datadesignGetSummaryForImport($datasetId);
        if(!$datadesignSummary) {
            return [
                "dataset_info"  => $datasetInfo,
                "study_info"    => $studyInfo,
                "design_exists" => $designExists,
                "show_importbtn" => $showImportbtn,
            ];
        }

        # extract individual summaries from result
        $summaryType       = array_column($datadesignSummary,"summary_type");

        #--------------------------------------------------------------------------
        $designExistsKeys     = array_keys($summaryType,"design_exists");
        $countDistinctKeys    = array_keys($summaryType,"count_distinct");
        $countFullKeys        = array_keys($summaryType,"count_full");
        #--------------------------------------------------------------------------
        $designExists  = array_values(array_intersect_key($datadesignSummary,array_flip($designExistsKeys)));
        $countDistinct = array_values(array_intersect_key($datadesignSummary,array_flip($countDistinctKeys)));
        $countFull     = array_values(array_intersect_key($datadesignSummary,array_flip($countFullKeys)));
        #--------------------------------------------------------------------------
        $designExists  = $designExists[0]->n;
        $countDistinct = $countDistinct[0]->n;
        $countFull     = $countFull[0]->n;
        #--------------------------------------------------------------------------

        #--------------------------------------------------------------------------
        $countSummaryKeys     = array_keys($summaryType,"count_summary");
        #--------------------------------------------------------------------------
        $designSummary  = array_values(array_intersect_key($datadesignSummary,array_flip($countSummaryKeys)));
        #--------------------------------------------------------------------------

        #--------------------------------------------------------------------------
        $eventKeys            = array_keys($summaryType,"event");
        $subeventKeys         = array_keys($summaryType,"subevent");
        $samplingeventKeys    = array_keys($summaryType,"samplingevent");
        $samplingtimeKeys     = array_keys($summaryType,"samplingtime");
        $startgroupKeys       = array_keys($summaryType,"startgroup");
        $subjectKeys          = array_keys($summaryType,"subject");
        $centerKeys           = array_keys($summaryType,"center");
        #--------------------------------------------------------------------------
        $event         = array_values(array_intersect_key($datadesignSummary,array_flip($eventKeys)));
        $subevent      = array_values(array_intersect_key($datadesignSummary,array_flip($subeventKeys)));
        $samplingevent = array_values(array_intersect_key($datadesignSummary,array_flip($samplingeventKeys)));
        $samplingtime  = array_values(array_intersect_key($datadesignSummary,array_flip($samplingtimeKeys)));
        $startgroup    = array_values(array_intersect_key($datadesignSummary,array_flip($startgroupKeys)));
        $subject       = array_values(array_intersect_key($datadesignSummary,array_flip($subjectKeys)));
        $center        = array_values(array_intersect_key($datadesignSummary,array_flip($centerKeys)));
        #--------------------------------------------------------------------------
        # @formatter:on
        $showImportbtn = ($studyInfo && $designExists);
        return [
            "dataset_info"    => $datasetInfo,
            "study_info"      => $studyInfo,
            "import_result"   => isset($importResult) ? $importResult : false,
            "confirm_import"  => $confirmImport,
            "design_exists"   => $designExists,
            "design_imported" => $designImported,
            "count_distinct"  => $countDistinct,
            "count_full"      => $countFull,
            "design_summary"  => $designSummary,
            "event"           => $event,
            "subevent"        => $subevent,
            "samplingevent"   => $samplingevent,
            "samplingtime"    => $samplingtime,
            "startgroup"      => $startgroup,
            "subject"         => $subject,
            "center"          => $center,
            "show_importbtn"  => $showImportbtn,
        ];
    }

    public function viewStudydesign($studyId)
    {
        # TODO: PARTIALLY implemented delete-studydesign, Awaiting initial tests
        $action = $this->Btn->getAction();
        $this->view->confirm_delete = false;
        if ($action === "delete") {
            if (UniqueId::uuidValidate($studyId)) {
                $this->setKeyValue("owner_studydesign_delete_studydesign", $studyId);
                $this->view->confirm_delete = true;
            }
        } elseif ($action === "confirm") {
            if (UniqueId::uuidValidate($studyId)) {
                $studyIdSaved = $this->getKeyValue("owner_studydesign_delete_studydesign");
                if ($studyId === $studyIdSaved) {
                    $deleteResults = $this->_getRepository()->deleteStudydesign($studyId);
                    if ($deleteResults) {
                        if ($deleteResults) {
                            $this->flashSession->success("Design successfully deleted");
                        }
                    }
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        $event = $this->_getRepository()->studydesignSummaryEvent($studyId);
        $subevent = $this->_getRepository()->studydesignSummarySubevent($studyId);
        $samplingevent = $this->_getRepository()->studydesignSummarySamplingevent($studyId);
        $samplingtime = $this->_getRepository()->studydesignSummarySamplingtime($studyId);
        $startgroup = $this->_getRepository()->studydesignSummaryStartgroup($studyId);
        $subject = $this->_getRepository()->studydesignSummarySubject($studyId);
        $center = $this->_getRepository()->studydesignSummaryCenter($studyId);
        $designExists = ($event || $subevent || $samplingevent || $samplingtime || $startgroup || $subject || $center);

        echo "";

        return [
            "study_info"    => $studyInfo,
            "event"         => $event,
            "subevent"      => $subevent,
            "samplingevent" => $samplingevent,
            "samplingtime"  => $samplingtime,
            "startgroup"    => $startgroup,
            "subject"       => $subject,
            "center"        => $center,
            "design_exists" => $designExists,
        ];

    }
}