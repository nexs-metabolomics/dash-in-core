<?php

namespace Dashin\Helpers\Owner;

use Dashin\Helpers\QueryBase;

class StudyQuery extends QueryBase
{

    static string $get_study_and_dependents_publc_status =
        <<<'EOD'
            WITH
              study_query         AS (
                                       SELECT study_id
                                            , name
                                            , title
                                            , description
                                            , study_is_public
                                            , CASE WHEN study_is_public = 0 THEN 'set_public' ELSE 'set_private' END AS study_toggle_text
                                       FROM dashin.study                                                                            a
                                          , coalesce((jsonb_extract_path_text(a.details, 'permissions', 'public') = 'yes')::INT, 0) b (study_is_public)
                                       WHERE a.owner_org = :owner_org
                                         AND a.study_id = :study_id
                                     )
              , dataset_query     AS (
                                       SELECT study_id
                                            , sum(is_public)                                                                     AS datasets_are_public
                                            , jsonb_object_agg(dataset_id::TEXT, dataset) FILTER ( WHERE dataset_id IS NOT NULL) AS datasets
                                       FROM (
                                              SELECT study_id
                                                   , jsonb_build_object('name', name,
                                                                        'dataset_id', dataset_id,
                                                                        'is_public', is_public,
                                                                        'toggle_text', toggle_text
                                                                        ) AS dataset
                                                   , is_public
                                                   , dataset_id
                                              FROM (
                                                     SELECT a.study_id
                                                          , c.name
                                                          , c.dataset_id
                                                          , d.is_public
                                                          , CASE WHEN is_public = 0 THEN 'set_public' ELSE 'set_private' END AS toggle_text
                                                     FROM study_query                   a
                                                       LEFT JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                                                       LEFT JOIN dashin.dataset         c ON b.dataset_id = c.dataset_id
                                                     , coalesce((jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes')::INT, 0) d (is_public)
                                                   ) x
                                            ) x
                                       GROUP BY study_id
                                     )
              , supportfile_query AS (
                                       SELECT study_id
                                            , sum(is_public)                                                                           AS supportfiles_are_public
                                            , jsonb_object_agg(supportfile_id, supportfile) FILTER ( WHERE supportfile_id IS NOT NULL) AS supportfiles
                                       FROM (
                                              SELECT study_id
                                                   , jsonb_build_object('name', name,
                                                                        'supportfile_id', supportfile_id,
                                                                        'is_public', is_public,
                                                                        'toggle_text', toggle_text
                                                     ) AS supportfile
                                                   , is_public
                                                   , supportfile_id
                                              FROM (
                                                     SELECT a.study_id
                                                          , c.name
                                                          , c.supportfile_id
                                                          , d.is_public
                                                          , CASE WHEN is_public = 0 THEN 'set_public' ELSE 'set_private' END AS toggle_text
                                                    FROM study_query                       a
                                                       LEFT JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                                                       LEFT JOIN dashin.supportfile         c ON b.supportfile_id = c.supportfile_id
                                                     , coalesce((jsonb_extract_path_text(c.details, 'permissions', 'public') = 'yes')::INT, 0) d (is_public)
                                                   ) x
                                            ) x
                                       GROUP BY study_id
                                     )
            SELECT a.study_id
                 , a.name
                 , a.title
                 , a.description
                 , a.study_is_public
                 , a.study_toggle_text
                 , b.datasets_are_public
                 , c.supportfiles_are_public
                 , b.datasets
                 , c.supportfiles
            FROM study_query               a
              INNER JOIN dataset_query     b ON a.study_id = b.study_id
              INNER JOIN supportfile_query c ON a.study_id = c.study_id;
        EOD;

    static string $set_study_public_status_on =
        <<<'EOD'
            UPDATE dashin.study
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"yes"')
            WHERE study_id = :study_id
              AND owner_org = :owner_org
            RETURNING 1;
        EOD;

    static string $set_study_public_status_off =
        <<<'EOD'
            UPDATE dashin.study
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"no"')
            WHERE study_id = :study_id
              AND owner_org = :owner_org;
        EOD;

    static string $set_dataset_public_status_on =
        <<<'EOD'
            UPDATE dashin.dataset
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"yes"')
            WHERE dataset_id = :dataset_id
              AND exists
              (
                SELECT
                FROM dashin.study                   a
                  INNER JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                WHERE (a.owner_org, a.study_id, b.dataset_id) = 
                      (:owner_org, :study_id, :dataset_id)
              );
        EOD;

    static string $set_dataset_public_status_off =
        <<<'EOD'
            UPDATE dashin.dataset
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"no"')
            WHERE dataset_id = :dataset_id
              AND exists
              (
                SELECT
                FROM dashin.study                   a
                  INNER JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                WHERE (a.owner_org, a.study_id, b.dataset_id) = 
                      (:owner_org, :study_id, :dataset_id)
              );
        EOD;

    static string $set_supportfile_public_status_on =
        <<<'EOD'
            UPDATE dashin.supportfile
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"yes"')
            WHERE supportfile_id = :supportfile_id
              AND exists
              (
                SELECT *
                FROM dashin.study                   a
                  INNER JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                WHERE (a.owner_org, a.study_id, b.supportfile_id) =
                      (:owner_org, :study_id, :supportfile_id)
              );
        EOD;

    static string $set_supportfile_public_status_off =
        <<<'EOD'
            UPDATE dashin.supportfile
            SET details = jsonb_set_deep(coalesce(details, '{}'), '{"permissions","public"}', '"no"')
            WHERE supportfile_id = :supportfile_id
              AND exists
              (
                SELECT *
                FROM dashin.study                   a
                  INNER JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                WHERE (a.owner_org, a.study_id, b.supportfile_id) =
                      (:owner_org, :study_id, :supportfile_id)
              );
        EOD;

    static string $set_study_and_all_object_public_status_off =
        <<<'EOD'
            WITH
              study_query               AS (
                                             SELECT study_id
                                                  , jsonb_set_deep(coalesce(a.details, '{}'), '{"permissions","public"}', '"no"') AS study_new_status
                                             FROM dashin.study a
                                             WHERE a.owner_org = :owner_org
                                                AND a.study_id = :study_id
                                           )
              , dataset_query           AS (
                                             UPDATE dashin.dataset tgt SET details = dataset_new_status
                                               FROM (
                                                      SELECT a.study_id
                                                           , c.dataset_id
                                                           , jsonb_set_deep(coalesce(c.details, '{}'), '{"permissions","public"}', '"no"') AS dataset_new_status
                                                      FROM study_query                   a
                                                        LEFT JOIN dashin.dataset_x_study b ON a.study_id = b.study_id
                                                        LEFT JOIN dashin.dataset         c ON b.dataset_id = c.dataset_id
                                                    ) src
                                               WHERE tgt.dataset_id = src.dataset_id
                                               RETURNING 1
                                           )
              , supportfile_query       AS (
                                             UPDATE dashin.supportfile tgt SET details = file_new_status
                                               FROM (
                                                      SELECT a.study_id
                                                           , c.supportfile_id
                                                           , jsonb_set_deep(coalesce(c.details, '{}'), '{"permissions","public"}', '"no"') AS file_new_status
                                                      FROM study_query                       a
                                                        LEFT JOIN dashin.study_x_supportfile b ON a.study_id = b.study_id
                                                        LEFT JOIN dashin.supportfile         c ON b.supportfile_id = c.supportfile_id
                                                    ) src
                                               WHERE tgt.supportfile_id = src.supportfile_id
                                               RETURNING 1
                                           )
              , dataset_count_query     AS (
                                             SELECT count(*) AS num_datasets
                                             FROM dataset_query
                                           )
              , supportfile_count_query AS (
                                             SELECT count(*) AS num_files
                                             FROM supportfile_query
                                           )
            
            SELECT a.study_id
                 , b.num_datasets
                 , c.num_files
            FROM study_query             a
               , dataset_count_query     b
               , supportfile_count_query c;
        EOD;

}