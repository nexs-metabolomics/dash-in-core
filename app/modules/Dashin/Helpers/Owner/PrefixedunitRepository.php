<?php


namespace Dashin\Helpers\Owner;


use Dashin\Helpers\RepositoryBase;

class PrefixedunitRepository extends RepositoryBase
{
    public function createPrefixedunit($inData)
    {
        try {
            $sql = $this->dashin_config->sql->owner_create_prefixedunit;

            $result = $this->db->fetchOne($sql);

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getPrefixedunitsPaginated($page, $nrows, $searchTerm)
    {
        $showvalid = $this->getKeyValue("owner_list_prefixedunits_showvalid");

        $queryParams["showvalid"] = ($showvalid === 2) ? 2 : 0;
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->other->owner_get_prefixedunit_count_search;
            $itemQuery = $this->dashin_config->sql->other->owner_get_prefixedunit_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->other->owner_get_prefixedunit_count;
            $itemQuery = $this->dashin_config->sql->other->owner_get_prefixedunit_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function prefixedunitSetValid($prefixedunitId,$valid)
    {
        $prefixedunitId = (int)$prefixedunitId;
        #------------------------------------------------------------------
        # check for int or non-int values and set to int
        # 
        # 1: invalid, 2: valid, 0: default (=invalid)
        # (unset/0) is used when updating prefixedunit table
        # to determine which values to update (only update valid=0)
        # because other values indicate a local deliberate
        # decision to change the default
        #------------------------------------------------------------------
        if($valid == 2) {
            $valid = 2;
        } elseif ($valid == 1) {
            $valid = 1;
        } else {
            $valid = 0;
        }
        try {
            $sql = $this->dashin_config->sql->other->owner_prefixedunit_set_valid;

            $state = $this->db->execute($sql,[
                "prefixedunit_id" => $prefixedunitId,
                "valid" => $valid,
            ]);
            
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

}