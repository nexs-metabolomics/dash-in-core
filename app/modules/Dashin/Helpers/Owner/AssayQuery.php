<?php

namespace Dashin\Helpers\Owner;

use Dashin\Helpers\QueryBase;

class AssayQuery extends QueryBase
{

    static string $create_assay =
        <<<'EOD'
            INSERT INTO dashin.assay (name, description, researchfield_id)
            VALUES (:name, :description, :researchfield_id)
            RETURNING assay_id;
        EOD;

    static string $get_assay =
        <<<'EOD'
            SELECT assay_id
                 , name
                 , description
                 , researchfield_id
            FROM dashin.assay
            WHERE assay_id = :assay_id;
        EOD;

    static string $update_assay =
        <<<'EOD'
            UPDATE dashin.assay
            SET (name, description, researchfield_id) = (:name, :description, :researchfield_id)
            WHERE assay_id = :assay_id
            AND dashin.organization_id;
        EOD;

    static string $delete_assay =
        <<<'EOD'
            DELETE
                FROM dashin.assay
            WHERE assay_id = :assay_id;
        EOD;

    static string $get_assay_list =
        <<<'EOD'
            SELECT assay_id
                 , name
                 , description
                 , researchfield_id
            FROM dashin.assay;
        EOD;

    static string $get_assay_list_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM dashin.assay;
        EOD;

    static string $get_assay_list_search_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM dashin.assay
            WHERE lower(concat(name, ' ', description))
              LIKE lower(concat('%', (:search_term) :: TEXT, '%'));
        EOD;

    static string $get_assay_list_paginated =
        <<<'EOD'
            SELECT a.assay_id
                 , a.name
                 , a.description
                 , t.researchfield_id
                 , t.name AS researchfield_name
            FROM dashin.assay                a
              LEFT JOIN dashin.researchfield t USING (researchfield_id)
            OFFSET :offset
            LIMIT :limit;
        EOD;

    static string $get_assay_list_paginated_search =
        <<<'EOD'
            SELECT assay_id
                 , name
                 , description
                 , researchfield_id
            FROM dashin.assay
            WHERE lower(concat(name, ' ', description))
              LIKE lower(concat('%', (:search_term) :: TEXT, '%'))
            OFFSET :offset
            LIMIT :limit;
        EOD;

    static string $get_researchfields =
        <<<'EOD'
            SELECT researchfield_id
                 , name
                 , description
            FROM dashin.researchfield
            ORDER BY researchfield_id;
        EOD;

}