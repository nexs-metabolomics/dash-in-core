<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Csv;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\EditDatasetForm;
use Dashin\Forms\Owner\SupportFileEditForm;
use Dashin\Forms\Owner\UploadDatasetForm;
use Dashin\Forms\Owner\UploadSupportFileForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;

/**
 * Handle datasets
 *
 * class ImportHelper
 *
 * @package Dashin\Helpers\Owner
 */
class ImportHelper extends HelperBase
{

    /**
     * @var ImportRepository
     */
    private $_repository;

    /**
     * @return ImportRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new ImportRepository();
        }

        return $this->_repository;
    }

    /**
     * Internal function to handle conversion of data columns
     * - currently handles only numeric and to-numeric conversion
     * - first, check and mark columns as numeric
     * - second, convert if marked
     *
     * @param $datasetId
     *
     * @return bool
     */
    private function _handleDatatype($datasetId)
    {

        # set numeric datatype
        $ok = $this->_getRepository()->setDatasetDatatypeNumeric($datasetId);
        if (!$ok) {
            return false;
        }

        # convert
        # $ok = $this->_getRepository()->convertDatasetToNumeric($datasetId);

    }

    /**
     * Internal function to handle uploading of dataset
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    private function _handleDataUpload()
    {

        if (!$this->request->hasFiles()) {
            return false;
        }
        $files = $this->request->getUploadedFiles(true);
        /** @var \Phalcon\Http\Request\File $file */
        $file = $files[0];

        # move to temp folder
        $name = uniqid("fileup_");
        $moveToName = TEMPDIR_PATH . "/$name";
        if (!$file->moveTo($moveToName)) {
            return false;
        }

        # get seps
        $separators = ["c" => ",", "sc" => ";", "t" => "\t", "sp" => " "];
        $sep = $this->request->getPost("sep");
        # null: automatic
        $sep = isset($separators[$sep]) ? $separators[$sep] : null;

        $file = new \SplFileObject($moveToName, "r");
        $sep = $this->_guessSeparator($file, $sep);
        if (!$sep) {
            return false;
        }
        # header
        $hasHeader = $this->request->getPost("header");
        $datasetId = $this->_getRepository()->createDatasetFromUpload($file, $sep, $hasHeader);

        # convert numeric string input to numeric values
        $this->_handleDatatype($datasetId);

        return $datasetId;
    }

    /**
     * Upload a dataset
     *
     * @return array|bool
     * @throws \App\Library\Utils\JsonException
     */
    public function uploadDataset()
    {

        $form = new UploadDatasetForm();
        if ($form->isRepeatPost()) {
            $this->flashSession->warning("Please select a file");
            # $form->get("file_up")->clear();
        } elseif ($this->request->isPost() && !$this->request->hasFiles()) {
            # no file selected
            $this->flashSession->notice("Please select a file");
        } elseif ($this->request->hasFiles(true)) {
            # file exists - check the other form entries
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $datasetId = $this->_handleDataUpload();
                if ($datasetId) {
                    $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
                    $this->response->redirect("/dashin/owner/import/uploadsummary/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        $form->preventRepeatPost();

        return [
            "form"         => $form,
            "dataset_info" => $datasetInfo,
        ];
    }

    /**
     * View summary report of a dataset
     *
     * @param $datasetId
     *
     * @return array|bool
     */
    public function viewDataset($datasetId)
    {

        $data = $this->_getRepository()->getDatasetForView($datasetId);
        if (!$data) {
            return false;
        }

        return ["dataset" => $data];
    }

    /**
     * Edit name, description of dataset
     *
     * @param $datasetId
     *
     * @return array|bool
     */
    public function editDataset($datasetId)
    {

        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/import/uploadsummary/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $form = new EditDatasetForm();
        if ($action === "submit" || $action === "apply") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $params["name"] = $this->request->getPost("name");
                $params["description"] = $this->request->getPost("description");
                $ok = $this->_getRepository()->updateDataset($datasetId, $params);
                if ($ok) {
                    $this->flashSession->success("Dataset successfully updated");
                }
                if ($action === "submit") {
                    $this->response->redirect("/dashin/owner/import/uploadsummary/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        $data = $this->_getRepository()->getDatasetForEdit($datasetId);
        if (!$data) {
            return false;
        }
        $form->bindValues($data);
        $form->setAction("/dashin/owner/import/editdataset/");

        return ["form" => $form, "data" => $data];
    }

    /**
     * Delete a dataset
     *
     * @return array|false
     */
    public function deleteDataset()
    {

        # get the request object
        $deleteRequestObject = $this->localsession->getKeyValue("dashin_owner_request_delete_dataset");

        # extract individual elements from request object
        $requestObjectId = $deleteRequestObject["object_id"];
        $returnUri = $deleteRequestObject["return_uri"];
        $msgs = $deleteRequestObject["msgs"];

        # info on object to delete
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($requestObjectId);

        # get requested action (from delete page)
        $action = $this->Btn->getAction();
        if ($action === "confirm") {

            # get object_id from delete page
            $datasetId = $this->Btn->getValue();

            # compare to original delete request
            if ($requestObjectId === $datasetId) {
                # confirmation ok

                # check for confirmation messages
                # keep asking the user for confirmation
                # until no more confirmation messages
                if (is_array($msgs) && count($msgs) > 0) {
                    $this->view->confirm_msg = array_shift($msgs);

                    # save delete request with updated confirm message array
                    $this->localsession->setKeyValue("dashin_owner_request_delete_dataset", ["object_id" => $datasetId, "msgs" => $msgs, "return_uri" => $returnUri]);
                } else {
                    $isDeleted = $this->_getRepository()->deleteDataset($datasetId);
                    if ($isDeleted) {
                        $this->flashSession->success("Dataset '$datasetInfo[name]' successfully deleted");
                    }

                    # clean up
                    $this->localsession->removeKeyValue("dashin_owner_request_delete_dataset");
                    $this->response->redirect("$returnUri{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        } elseif ($action === "cancel") {

            # clean up
            $this->localsession->removeKeyValue("dashin_owner_request_delete_dataset");
            $this->response->redirect("$returnUri{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        return [
            "dataset_info" => $datasetInfo,
        ];
    }

    /**
     * List datasets
     * Includes functionality to delete
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listDatasets($page, $nrows, $reset)
    {

        $datasetId = false;

        $action = $this->Btn->getAction();
        // filter readonly user out
        if ($this->SU->allowWrite()) {
            if ($action === "delete") {
                $deletDatasetId = $this->Btn->getValue();
                $msgs = ["Are you sure you want to delete this dataset?"];
                $this->localsession->setKeyValue("dashin_owner_request_delete_dataset", ["object_id" => $deletDatasetId, "msgs" => $msgs, "return_uri" => "/dashin/owner/import/listdatasets/$page"]);
                $this->response->redirect("/dashin/owner/import/deletedataset/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;

            } elseif ($action === "edit") {
                $datasetId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($datasetId)) {
                    $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
                    $this->response->redirect("/dashin/owner/import/editdataset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        if ($action === "view") {
            $datasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($datasetId)) {
                $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
                $this->response->redirect("/dashin/owner/import/uploadsummary/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        }

        $selectDatasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($selectDatasetId);
        $searchTerm = $this->getSearchTerm("dashin_owner_import_list_datasets_search_term", $reset);

        $datasets = $this->_getRepository()->getDatasetPaginated($page, $nrows, $searchTerm);

        if ($datasets) {
            foreach ($datasets->items as &$row) {
                if ($row->dataset_id === $datasetId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
                $row = (object)$row;
            }
        }

        return ["datasets"     => $datasets,
                "search_term"  => $searchTerm,
                "dataset_info" => $datasetInfo,
        ];
    }

    /**
     * Download a copy of the extracted variable information
     *
     * @param $datasetId
     */
    private function _downloadVariableInfoTemplate($datasetId)
    {

        $data = $this->_getRepository()->getVariableTemplateForDownload($datasetId);
        # convert to csv

        $Csv = new Csv();
        $header = array_keys(current($data));
        reset($data);
        $csvContents = $Csv->arrayToCsv($data, ";", $header);
        # push to download
        $this->downloader->send($csvContents, "variable_descriptor.csv", "csv");
    }

    /**
     * Download dataset structure
     *
     * @param $datasetId
     *
     * @return void
     */
    private function _downloadDatasetStructure($datasetId)
    {

        $data = $this->_getRepository()->getDatasetStructure($datasetId);
        # convert to csv

        $Csv = new Csv();
        $header = array_keys(current($data));
        reset($data);
        $csvContents = $Csv->arrayToCsv($data, ";", $header);
        # push to download
        $this->downloader->send($csvContents, "dataset_structure.csv", "csv");
    }

    /**
     * Download all data from a specific dataset
     *
     * @param $datasetId
     *
     * @return void
     */
    private function _downloadDatasetData($datasetId)
    {

        $data = $this->_getRepository()->getDatasetData($datasetId);
        if (isset($data["dataset"])) {
            $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);
            if (isset($datasetInfo["name"])) {
                $fileName = preg_replace('#[^A-Za-z0-9 ,.{}()-]+#', "_", $datasetInfo["name"]) . ".csv";
            } else {
                $fileName = "dataset.csv";
            }

            $contents = $data["dataset"];
            $this->downloader->send($contents, $fileName, "csv");
        } else {
            $this->flashSession->warning("The dataset could not be created");
        }
    }

    /**
     * Show a paginated list of variables in current dataset
     *
     * @param $datasetId
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listVariables($datasetId, $page, $nrows, $varnameMaxLength, $reset)
    {

        $action = $this->Btn->getAction();
        $designvarUpdateAttempt = 0;
        if ($action === "save") {
            $hasDesign = $this->_getRepository()->datasetHasDesign($datasetId);
            if ($hasDesign) {
                $res = $this->_getRepository()->updateVariableDefinitionAllRowsWithDesign($datasetId);
                $designvarUpdateAttempt = array_sum(array_column($res, "designvar_update"));
                if ($designvarUpdateAttempt > 0) {
                    $this->flashSession->warning("Attempt to update design variables");
                }
            } else {
                $ok = $this->_getRepository()->updateVariableDefinitionAllRowsNoDesign($datasetId);
            }
            $this->flashSession->success("Changes successfully saved");
        } elseif ($action === "downloadtemplate") {
            $this->_downloadVariableInfoTemplate($datasetId);
        } elseif ($action === "reset") {
            $page = 1;
            $this->session->remove("dashin_owner_import_listvars_filter_search");
        } elseif ($action === "convert") {
            $this->_handleDatatype($datasetId);
        } elseif ($action === "editvariable") {
            $variableId = $this->Btn->getValue();
            $this->localsession->setKeyValue("dashin_owner_variable_id", $variableId);
            $this->response->redirect("/dashin/owner/import/editvariable/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        # variable types
        $variableTypes = $this->_getRepository()->getVariableTypes();
        $vtVals = array_column($variableTypes, "variabletype_id");
        $vtLabs = array_column($variableTypes, "name");

        # data types
        $dataTypes = $this->_getRepository()->getDataTypes();
        $dtVals = array_column($dataTypes, "datatype_id");
        $dtLabs = array_column($dataTypes, "name");

        # prefixed units
        $prfxUnits = $this->_getRepository()->getValidPrefixedunits();
        $prfxuVals = array_column($prfxUnits, "prefixedunit_id");
        $prfxuLabs = array_column($prfxUnits, "combined_symbol");

        # assays
        $assays = $this->_getRepository()->getAssayList();
        $asVals = array_column($assays, "assay_id");
        $asLabs = array_column($assays, "name");
        $asVals = array_merge([0], $asVals);
        $asLabs = array_merge(["None"], $asLabs);

        #-------------------------------------------------------------------
        # validate search parameters
        #-------------------------------------------------------------------
        if (($action === "search" || $this->session->has("dashin_owner_import_listvars_filter_search"))) {
            # remember search filters when navigating
            if ($action === "search") {
                $params["varname"] = $this->request->getPost("varname");
                if (!$params["varname"]) {
                    unset($params["varname"]);
                }
                $params["method"] = $this->request->getPost("method");
                if (!$params["method"]) {
                    unset($params["method"]);
                }

                $params["datatype_id"] = $this->request->getPost("dt_fi");
                if (!in_array($params["datatype_id"], $dtVals)) {
                    unset($params["datatype_id"]);
                }

                $params["vartype_id"] = $this->request->getPost("vt_fi");
                if (!in_array($params["vartype_id"], $vtVals)) {
                    unset($params["vartype_id"]);
                }

                $this->session->set("dashin_owner_import_listvars_filter_search", $params);
                $page = 1;
            } else {
                $params = $this->session->get("dashin_owner_import_listvars_filter_search");
            }
        } else {
            $params = null;
        }

        $variablesPaginated = $this->_getRepository()->getVariablesPaginated($datasetId, $page, $nrows, $params);
        if ($params) {
            $this->view->show_filter_reset = true;
        }

        if ($variablesPaginated) {
            foreach ($variablesPaginated->items as &$item) {
                # variable name max-length
                if (mb_strlen($item->name) > $varnameMaxLength) {
                    $item->name_short = substr($item->name, 0, $varnameMaxLength - 3) . "...";
                } else {
                    $item->name_short = $item->name;
                }

                # variable type
                $vtFld = new Select("var_type[$item->variable_id]", array_combine($vtVals, $vtLabs), [
                    "onchange" => " vartypeChange(this); ",
                    "id"       => "var_type_" . $item->variable_id,
                    "class"    => "vartype",
                ]);
                $vtFld->setDefault($item->variabletype_id);
                $item->vt = $vtFld;

                # data type
                $dtFld = new Select("datatype[$item->variable_id]", array_combine($dtVals, $dtLabs), ["onchange" => " this.dataset.changed = 'changed'; "]);
                $dtFld->setDefault($item->datatype_id);
                $item->dt = $dtFld;

                # assay
                $asFld = new Select("assay[$item->variable_id]", array_combine($asVals, $asLabs), ["onchange" => " this.dataset.changed = 'changed'; ", "title" => $item->assay]);
                $asFld->setDefault($item->assay_id);
                $item->as = $asFld;

                # unit nominator
                $unFld = new Select("unitnum[$item->variable_id]", array_combine($prfxuVals, $prfxuLabs), ["onchange" => " this.dataset.changed = 'changed'; "]);
                $unFld->setDefault($item->numerator_prefixedunit_id);
                $item->un = $unFld;

                # unit denominator
                $udFld = new Select("unitdenom[$item->variable_id]", array_combine($prfxuVals, $prfxuLabs), ["onchange" => " this.dataset.changed = 'changed'; "]);
                $udFld->setDefault($item->denominator_prefixedunit_id);
                $item->ud = $udFld;
            }
        }

        # action all
        # - add no-action option (must sort before the regular values)
        $vtVals = array_merge([-1], $vtVals);
        $dtVals = array_merge([0], $dtVals);
        $asVals = array_merge([""], $asVals);

        # variable type
        $vtLabs = array_merge(["Variable type"], $vtLabs);
        # Data type
        $dtLabs = array_merge(["Data type"], $dtLabs);
        # assay
        $asLabs = array_merge(["Assay (all)"], $asLabs);

        $vtFi = new Select("vt_fi", array_combine($vtVals, $vtLabs), ["form" => "frm-fi"]);
        $dtFi = new Select("dt_fi", array_combine($dtVals, $dtLabs), ["form" => "frm-fi"]);
        $asFi = new Select("as_fi", array_combine($asVals, $asLabs), ["form" => "frm-fi"]);
        $vnFld = new Text("varname", ["form" => "frm-fi", "placeholder" => "Variable name"]);
        $mtFld = new Text("method", ["form" => "frm-fi", "placeholder" => "Assay"]);

        # set values on filter fields
        if (isset($params)) {
            if ($params["varname"]) {
                $vnFld->setAttribute("value", $params["varname"]);
            }
            if ($params["method"]) {
                $mtFld->setAttribute("value", $params["method"]);
            }
            if (isset($params["datatype_id"])) {
                $dtFi->setDefault((int)$params["datatype_id"]);
            }
            if (isset($params["vartype_id"])) {
                $vtFi->setDefault((int)$params["vartype_id"]);
            }
            if (isset($params["assay_id"])) {
                $asFi->setDefault((string)$params["assay_id"]);
            }
        }
        if ($action === "reset") {
            $vnFld->clear();
            $dtFi->clear();
            $vtFi->clear();
            $asFi->clear();
            $mtFld->clear();
        }
        $dataset = $this->_getRepository()->getDataset($datasetId);

        return [
            "variables"                => $variablesPaginated,
            "select_fi"                => ["vt" => $vtFi, "dt" => $dtFi, "as" => $asFi, "vn" => $vnFld, "mt" => $mtFld],
            "dataset"                  => $dataset,
            "designvar_update_attempt" => $designvarUpdateAttempt > 0,
        ];
    }

    /**
     * Edit a single variable
     *
     * @param $datasetId
     * @param $variableId
     *
     * @return array
     */
    public function editVariable($datasetId, $variableId)
    {

        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);
        $variable = $this->_getRepository()->getVariableForEdit($datasetId, $variableId);

        return [
            "variable"     => $variable,
            "dataset_info" => $datasetInfo,
        ];
    }

    /**
     * Browse data of current dataset
     *
     * @param $datasetId
     * @param $rowStart
     * @param $rowLimit
     * @param $colStart
     * @param $colLimit
     *
     * @return array|bool
     */
    public function browseData($datasetId, $rowStart, $rowLimit, $colStart, $colLimit)
    {

        $action = $this->Btn->getAction();
        if ($action === "downloaddata") {
            $this->_downloadDatasetData($datasetId);
        }
        $data = $this->_getRepository()->getDatarowsPaginated($datasetId, $rowStart, $rowLimit, $colStart, $colLimit);
        $dataset = $this->_getRepository()->getDataset($datasetId);

        return [
            "data"    => $data,
            "dataset" => $dataset,
        ];
    }

    /**
     * View the datadesign for a dataset
     *
     * @param $datasetId
     *
     * @return array
     */
    public function viewDatadesign($datasetId)
    {

        $action = $this->Btn->getAction();
        $confirmDelete = false;
        if ($action === "save") {
            $saveResult = $this->_getRepository()->datadesignSaveDesign($datasetId);
        } elseif ($action === "delete") {
            if (UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_import_delete_datadesign", $datasetId);
                $confirmDelete = true;
            } else {
                $this->removeKeyValue("owner_import_delete_datadesign");
            }
        } elseif ($action === "confirm") {
            $datasetIdSaved = $this->getKeyValue("owner_import_delete_datadesign");
            if ($datasetIdSaved === $datasetId) {
                $ok = $this->_getRepository()->datadesignDeleteDesign($datasetId);
            }
            $this->removeKeyValue("owner_import_delete_datadesign");
        }
        # dataset
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        # @formatter:off
        # summary
        $datadesignSummary = $this->_getRepository()->datadesignGetSummary($datasetId);
        if(!$datadesignSummary) {
            return ["dataset_info" => $datasetInfo];
        }
        
        # extract individual summaries from result
        $summaryType       = array_column($datadesignSummary,"summary_type");
        
        #--------------------------------------------------------------------------
        $designExistsKeys     = array_keys($summaryType,"design_exists");
        $countDistinctKeys    = array_keys($summaryType,"count_distinct");
        $countFullKeys        = array_keys($summaryType,"count_full");
        #--------------------------------------------------------------------------
        $designExists  = array_values(array_intersect_key($datadesignSummary,array_flip($designExistsKeys)));
        $countDistinct = array_values(array_intersect_key($datadesignSummary,array_flip($countDistinctKeys)));
        $countFull     = array_values(array_intersect_key($datadesignSummary,array_flip($countFullKeys)));
        #--------------------------------------------------------------------------
        $designExists  = $designExists[0]->n;
        $countDistinct = $countDistinct[0]->n;
        $countFull     = $countFull[0]->n;
        #--------------------------------------------------------------------------

        #--------------------------------------------------------------------------
        $countSummaryKeys     = array_keys($summaryType,"count_summary");
        #--------------------------------------------------------------------------
        $designSummary  = array_values(array_intersect_key($datadesignSummary,array_flip($countSummaryKeys)));
        #--------------------------------------------------------------------------
        
        #--------------------------------------------------------------------------
        $eventKeys            = array_keys($summaryType,"event");
        $subeventKeys         = array_keys($summaryType,"subevent");
        $samplingeventKeys    = array_keys($summaryType,"samplingevent");
        $samplingtimeKeys     = array_keys($summaryType,"samplingtime");
        $startgroupKeys       = array_keys($summaryType,"startgroup");
        $subjectKeys          = array_keys($summaryType,"subject");
        #--------------------------------------------------------------------------
        $event         = array_values(array_intersect_key($datadesignSummary,array_flip($eventKeys)));
        $subevent      = array_values(array_intersect_key($datadesignSummary,array_flip($subeventKeys)));
        $samplingevent = array_values(array_intersect_key($datadesignSummary,array_flip($samplingeventKeys)));
        $samplingtime  = array_values(array_intersect_key($datadesignSummary,array_flip($samplingtimeKeys)));
        $startgroup    = array_values(array_intersect_key($datadesignSummary,array_flip($startgroupKeys)));
        $subject       = array_values(array_intersect_key($datadesignSummary,array_flip($subjectKeys)));
        #--------------------------------------------------------------------------
        # @formatter:on

        #$subjectEventSequence = $this->_getRepository()->getDatadesignSubjectEventSequence($datasetId);
        #if ($subjectEventSequence) {
        #    foreach ($subjectEventSequence as &$row) {
        #        if (mb_strlen($row["event_sequence"]) > 60) {
        #            $row["event_sequence"] = mb_substr($row["event_sequence"], 0, 57) . " . . .";
        #        }
        #        $row = (object)$row;
        #    }
        #}
        #unset($row);
        #
        #$startgroupEventSequence = $this->_getRepository()->getDatadesignStartgroupEventSequence($datasetId);
        #if ($startgroupEventSequence) {
        #    foreach ($startgroupEventSequence as &$row) {
        #        if (mb_strlen($row["event_sequence"]) > 60) {
        #            $row["event_sequence"] = mb_substr($row["event_sequence"], 0, 57) . " . . .";
        #        }
        #        if (mb_strlen($row["subjects"]) > 60) {
        #            $row["subjects"] = mb_substr($row["subjects"], 0, 57) . " . . .";
        #        }
        #        $row = (object)$row;
        #    }
        #}
        #unset($row);
        #
        #    "subject_event_sequence"    => $subjectEventSequence,
        #    "startgroup_event_sequence" => $startgroupEventSequence,

        return [
            "dataset_info"   => $datasetInfo,
            "save_result"    => isset($saveResult) ? $saveResult : false,
            "confirm_delete" => $confirmDelete,
            "design_exists"  => $designExists,
            "count_distinct" => $countDistinct,
            "count_full"     => $countFull,
            "design_summary" => $designSummary,
            "event"          => $event,
            "subevent"       => $subevent,
            "samplingevent"  => $samplingevent,
            "samplingtime"   => $samplingtime,
            "startgroup"     => $startgroup,
            "subject"        => $subject,
        ];
    }

    /**
     * Handle upload of dataset support file
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function supportFileUpload($datasetId)
    {

        # get info on active dataset
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        # handle possible file upload
        if ($this->supportFiles->handleUpload()) {

            # register uploaded file as a dataset supportfile
            $supportfileId = $this->supportFiles->saveAndRegister(["dataset_id" => $datasetId, "organization_id" => $datasetInfo["organization_id"], "type" => "dataset"]);
            if (UniqueId::uuidValidate($supportfileId)) {

                # save to file-id as active file to localsession
                $this->localsession->setKeyValue("dashin_owner_dataset_supportfile_id", $supportfileId);

                # redirect to edit
                $this->response->redirect("/dashin/owner/import/fileedit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }

        }

        # form for file upload
        $form = new UploadSupportFileForm();

        return [
            "dataset_info" => $datasetInfo,
            "form"         => $form,
        ];

    }

    /**
     * Edit support file description
     *
     * @param $datasetId
     * @param $supportfileId
     *
     * @return array
     */
    public function supportFileEdit($datasetId, $supportfileId)
    {

        $form = new SupportFileEditForm();
        $action = $this->Btn->getAction();

        if ($action === "cancel") {
            $this->response->redirect("dashin/owner/import/filelist/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        } elseif ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $description = $this->request->getPost("description");
                    $ok = $this->supportFiles->updateFileDescription($supportfileId, $description);
                    if ($ok) {
                        $this->flashSession->success("File description updated");
                        if ($action === "submit") {
                            $this->response->redirect("dashin/owner/import/filelist/{$this->localsession->getQuerystring('?')}");
                            $this->response->send();

                            return false;
                        }
                    }
                }
            }
        }

        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        $supportfileInfo = $this->supportFiles->getSupportFileInfo($supportfileId);
        if ($supportfileInfo) {
            $form->bindValues($supportfileInfo);
        }

        return [
            "form"             => $form,
            "dataset_info"     => $datasetInfo,
            "supportfile_info" => $supportfileInfo,
        ];
    }

    /**
     * List support files for the current dataset
     *
     * This dialog is also where support files are
     * deleted from current dataset
     *
     * @param $datasetId
     *
     * @return array
     */
    public function supportFileList($datasetId)
    {

        $supportfileId = false;
        $setConfirm = false;
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $setConfirm = true;
                $this->localsession->setKeyValue("dashin_owner_dataset_delete_support_file", $supportfileId);
            } else {
                $this->localsession->removeKeyValue("dashin_owner_dataset_delete_support_file");
            }
        }

        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);
        if ($action === "confirm") {
            $supportfileId = $this->Btn->getValue();
            $supportfileIdSaved = $this->localsession->getKeyValue("dashin_owner_dataset_delete_support_file");
            if ($supportfileId === $supportfileIdSaved) {
                $organizationId = $datasetInfo["organization_id"] ?? false;
                $ok = $this->supportFiles->deleteFile(["organization_id" => $organizationId, "dataset_id" => $datasetId, "type" => "dataset"], $supportfileId);
                if ($ok) {
                    $this->flashSession->success("File deleted");
                }
            }
            $this->localsession->removeKeyValue("dashin_owner_dataset_delete_support_file");

            # if the deleted file is the active file - remove from being active
            $supportfileId = $this->localsession->getKeyValue("dashin_owner_dataset_supportfile_id");
            if ($supportfileIdSaved === $supportfileId) {
                $this->localsession->removeKeyValue("dashin_owner_dataset_supportfile_id");
            }
        }
        if ($action === "edit") {

            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $this->localsession->setKeyValue("dashin_owner_dataset_supportfile_id", $supportfileId);
                $querystring = $this->localsession->getQuerystring("?");
                $this->response->redirect("/dashin/owner/import/fileedit/$querystring");
                $this->response->send();

                return false;
            }

        }

        if ($action === "download") {

            $supportfileId = $this->Btn->getValue();
            $organizationId = $datasetInfo["organization_id"] ?? false;
            if ($organizationId === false) {
                $this->flashSession->error("Could not find dataset in database");

                return false;
            }
            $this->supportFiles->downloadSupportFile(["dataset_id" => $datasetId, "organization_id" => $organizationId, "type" => "dataset"], $supportfileId);
        }

        $datasetfileList = $this->supportFiles->getFileList(["dataset_id" => $datasetId, "type" => "dataset"]);
        if ($datasetfileList) {
            foreach ($datasetfileList as &$row) {
                if ($setConfirm && $row["supportfile_id"] === $supportfileId) {
                    $row["confirm"] = true;
                } else {
                    $row["confirm"] = false;
                }
                $row = (object)$row;
            }
            unset($row);
        }

        return [
            "dataset_info"     => $datasetInfo,
            "datasetfile_list" => $datasetfileList,
        ];
    }

}