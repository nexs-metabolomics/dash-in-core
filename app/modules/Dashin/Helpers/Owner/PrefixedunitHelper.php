<?php


namespace Dashin\Helpers\Owner;


use Dashin\Helpers\HelperBase;

class PrefixedunitHelper extends HelperBase
{
    /**
     * @var PrefixedunitRepository
     */
    private $_repository;

    /**
     * @return PrefixedunitRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new PrefixedunitRepository();
        }
        return $this->_repository;
    }
    
    
    public function listPrefixedunits($page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        # valid=2: valid
        # valid=1: deliberate invalid
        # valid=0: default invalid
        if($action === "enable") {
            $prefixedunitId = $this->Btn->getValue();
            $this->_getRepository()->prefixedunitSetValid($prefixedunitId,2);
        } elseif ($action === "disable") {
            $prefixedunitId = $this->Btn->getValue();
            $this->_getRepository()->prefixedunitSetValid($prefixedunitId,1);
        } elseif ($action === "showvalid") {
            $this->setKeyValue("owner_list_prefixedunits_showvalid",2);
        } elseif ($action === "showall") {
            $this->setKeyValue("owner_list_prefixedunits_showvalid",0);
        }
        $searchTerm = $this->getSearchTerm("dashin_owner_list_prefixedunits_search_term",$reset);
        $prefixedunits = $this->_getRepository()->getPrefixedunitsPaginated($page,$nrows,$searchTerm);
        return [
            "search_term" => $searchTerm,
            "prefixedunits" => $prefixedunits,
        ];
    }
}