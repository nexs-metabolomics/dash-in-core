<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\CreateAssayForm;
use Dashin\Forms\Owner\SupportFileEditForm;
use Dashin\Forms\Owner\UploadSupportFileForm;
use Dashin\Helpers\HelperBase;

/**
 * Handle assays
 *
 * Class AssayHelper
 *
 * @package Dashin\Helpers\Owner
 */
class AssayHelper extends HelperBase
{

    /**
     * @var AssayRepository
     */
    private $_repository;

    /**
     * @return AssayRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new AssayRepository();
        }

        return $this->_repository;
    }

    /**
     * Create assay
     *
     * @return array|bool
     */
    public function createAssay()
    {

        $researchfields = $this->_getRepository()->getResearchfields();
        $rtIDs = array_column($researchfields, "researchfield_id");
        $rtNames = array_column($researchfields, "name");
        $rtChoices = array_combine($rtIDs, $rtNames);

        $form = new CreateAssayForm(["researchfield" => $rtChoices]);
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $post["name"] = $this->request->getPost("name");
                $post["description"] = $this->request->getPost("description");
                $post["researchfield_id"] = $this->request->getPost("researchfield");
                $assayId = $this->_getRepository()->createAssay($post);
                if ($assayId) {
                    $this->localsession->setKeyValue("dashin_owner_assay_id", $assayId);
                    $this->response->redirect("/dashin/owner/assay/edit/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        return ["form" => $form];
    }

    /**
     * Edit assay
     *
     * @param $assayId
     *
     * @return array|bool
     */
    public function editAssay($assayId)
    {

        $researchfields = $this->_getRepository()->getResearchfields();
        $rtIDs = array_column($researchfields, "researchfield_id");
        $rtNames = array_column($researchfields, "name");
        $rtChoices = array_combine($rtIDs, $rtNames);

        $form = new CreateAssayForm(["researchfield" => $rtChoices]);
        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $state = $this->_getRepository()->updateAssay($assayId);
                $action = $this->Btn->getAction();
                if ($state && $action === "submit") {
                    $this->response->redirect("/dashin/owner/assay/list/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $assay = $this->_getRepository()->getAssay($assayId);
        if ($assay) {
            $form->bindValues($assay);
            if ($form->has("researchfield")) {
                $form->get("researchfield")->setDefault($assay["researchfield_id"]);
            }
        }

        return [
            "form"  => $form,
            "assay" => $assay,
        ];
    }

    /**
     * List available assays
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listAssays($page, $nrows, $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "delete") {
            # delete - 1st run
            $assayId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($assayId)) {
                $this->setKeyValue("dashin_owner_assay_delete", $assayId);
            } else {
                $this->removeKeyValue("dashin_owner_assay_delete");
            }
        } elseif ($action === "confirm") {
            # delete 2nd run
            $assayId = $this->Btn->getValue();
            $assayIdSaved = $this->getKeyValue("dashin_owner_assay_delete");
            # if not equal, user has clicked to delete a different entity
            # don't delete, reset and return
            if ($assayId === $assayIdSaved) {
                $this->_getRepository()->deleteAssay($assayId);
            }
            $this->removeKeyValue("dashin_owner_assay_delete");
            $this->localsession->removeKeyValue("dashin_owner_assay_id");
            # reset buttons in ui even if deletion fails
            $assayId = false;
        } else {
            $assayId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_assays_search_term", $reset);
        $assays = $this->_getRepository()->getAssaysPaginated($page, $nrows, $searchTerm);
        # set flag for showing "confirm" button in ui
        if ($assays) {
            foreach ($assays->items as &$row) {
                if ($row->assay_id === $assayId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
            }
        }

        return ["assays" => $assays, "search_term" => $searchTerm];

    }

    /**
     * Handle upload of assay support file
     *
     * @param $assayId
     *
     * @return array|false
     */
    public function supportFileUpload($assayId)
    {

        # handle possible file upload
        if ($this->supportFiles->handleUpload()) {

            # register uploaded file as a assay supportfile
            $supportfileId = $this->supportFiles->saveAndRegister(["assay_id" => $assayId, "type" => "assay"]);
            if (UniqueId::uuidValidate($supportfileId)) {

                # save to file-id as active file to localsession
                $this->localsession->setKeyValue("dashin_owner_assay_supportfile_id", $supportfileId);

                # redirect to edit
                $this->response->redirect("/dashin/owner/assay/fileedit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }

        }

        # get info on active assay
        $assayInfo = $this->_getRepository()->getAssayInfo($assayId);

        # form for file upload
        $form = new UploadSupportFileForm();

        return [
            "assay_info" => $assayInfo,
            "form"       => $form,
        ];

    }

    /**
     * Edit support file description
     *
     * @param $assayId
     * @param $supportfileId
     *
     * @return array
     */
    public function supportFileEdit($assayId, $supportfileId)
    {

        $form = new SupportFileEditForm();
        $action = $this->Btn->getAction();

        if ($action === "cancel") {
            $this->response->redirect("dashin/owner/assay/filelist/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        } elseif ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $description = $this->request->getPost("description");
                    $ok = $this->supportFiles->updateFileDescription($supportfileId, $description);
                    if ($ok) {
                        $this->flashSession->success("File description updated");
                        if ($action === "submit") {
                            $this->response->redirect("dashin/owner/assay/filelist/{$this->localsession->getQuerystring('?')}");
                            $this->response->send();

                            return false;
                        }
                    }
                }
            }
        }

        $assayInfo = $this->_getRepository()->getAssayInfo($assayId);

        $supportfileInfo = $this->supportFiles->getSupportFileInfo($supportfileId);
        if ($supportfileInfo) {
            $form->bindValues($supportfileInfo);
        }

        return [
            "form"             => $form,
            "assay_info"       => $assayInfo,
            "supportfile_info" => $supportfileInfo,
        ];
    }

    /**
     * List support files for the current assay
     *
     * This dialog is also where support files are
     * deleted from current assay
     *
     * @param $assayId
     *
     * @return array
     */
    public function supportFileList($assayId)
    {

        $supportfileId = false;
        $setConfirm = false;
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $setConfirm = true;
                $this->localsession->setKeyValue("dashin_owner_assay_delete_support_file", $supportfileId);
            } else {
                $this->localsession->removeKeyValue("dashin_owner_assay_delete_support_file");
            }
        } elseif ($action === "confirm") {
            $supportfileId = $this->Btn->getValue();
            $supportfileIdSaved = $this->localsession->getKeyValue("dashin_owner_assay_delete_support_file");
            if ($supportfileId === $supportfileIdSaved) {
                $ok = $this->supportFiles->deleteFile(["assay_id" => $assayId, "type" => "assay"], $supportfileId);
                if ($ok) {
                    $this->flashSession->success("File deleted");
                }
            }
            $this->localsession->removeKeyValue("dashin_owner_assay_delete_support_file");

            # if the deleted file is the active file - remove from being active
            $supportfileId = $this->localsession->getKeyValue("dashin_owner_assay_supportfile_id");
            if ($supportfileIdSaved === $supportfileId) {
                $this->localsession->removeKeyValue("dashin_owner_assay_supportfile_id");
            }
        } elseif ($action === "edit") {
            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $this->localsession->setKeyValue("dashin_owner_assay_supportfile_id", $supportfileId);
                $querystring = $this->localsession->getQuerystring("?");
                $this->response->redirect("/dashin/owner/assay/fileedit/$querystring");
                $this->response->send();

                return false;
            }
        } elseif ($action === "download") {

            $supportfileId = $this->Btn->getValue();
            $this->supportFiles->downloadSupportFile(["assay_id" => $assayId, "type" => "assay"], $supportfileId);
        }

        $assayfileList = $this->supportFiles->getFileList(["assay_id" => $assayId, "type" => "assay"]);
        if ($assayfileList) {
            foreach ($assayfileList as &$row) {
                if ($setConfirm && $row["supportfile_id"] === $supportfileId) {
                    $row["confirm"] = true;
                } else {
                    $row["confirm"] = false;
                }
                $row = (object)$row;
            }
            unset($row);
        }

        $assayInfo = $this->_getRepository()->getAssayInfo($assayId);

        return [
            "assay_info"     => $assayInfo,
            "assayfile_list" => $assayfileList,
        ];
    }

}