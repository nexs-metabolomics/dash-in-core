<?php

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class ImportguideRepository extends RepositoryBase
{

    /**
     * Identify if the data column is numeric and mark it as such
     * in the variable table
     *
     * @param $datasetId
     *
     * @return bool
     */
    public function setDatasetDatatypeNumeric($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = ImportguideQuery::$dataset_set_datatype_numeric;

            $result = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "limit"      => 10,
            ]);

            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create a dataset from uploaded csv file
     *
     * @param \SplFileObject $file
     * @param string         $sep
     * @param bool           $hasHeader
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createDatasetFromUpload(\SplFileObject $file, string $sep = ",", ?bool $hasHeader = true)
    {

        try {

            #----------------------------------------------------
            # open file
            #----------------------------------------------------
            $organizationId = $this->SU->getActiveManagerOrgId();

            $this->db->begin();

            #----------------------------------------------------
            # create  dataset entry
            #----------------------------------------------------
            $sqlDset = $this->dashin_config->sql->owner_import->create_dataset;
            $dataSetResult = $this->db->fetchOne($sqlDset, Enum::FETCH_ASSOC, [
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "owner_org"   => $organizationId,
            ]);

            #----------------------------------------------------
            # was a dataset entry created? - check for valid id
            #----------------------------------------------------
            if (!UniqueId::uuidValidate($dataSetResult["dataset_id"])) {
                $this->db->rollback();

                return false;
            }
            $datasetId = $dataSetResult["dataset_id"];

            #----------------------------------------------------
            # does dataset contain meaningful data?
            #----------------------------------------------------
            $firstRow = $file->fgetcsv($sep);
            if (!is_array($firstRow)) {
                $this->db->rollback();

                return false;
            }
            $ncol = count($firstRow);
            if (!$ncol) {
                $this->db->rollback();

                return false;
            }

            #----------------------------------------------------
            # create variable entries
            #----------------------------------------------------
            $localIds = range(0, $ncol - 1);
            $sqlHdr = $this->dashin_config->sql->owner_import->create_dataset_variables;

            #----------------------------------------------------
            # we use prepared statement:
            # iterate through inserts instead of generating one sql
            #----------------------------------------------------

            # generate varnames if no header
            if (!$hasHeader) {
                for ($i = 0; $i < $ncol; ++$i) {
                    $this->db->execute($sqlHdr, [
                        "local_id"   => $localIds[$i],
                        "dataset_id" => $datasetId,
                        "name"       => "var" . ($i + 1),
                    ]);
                }
            } else {

                # use first line as header
                $firstRow = mb_convert_encoding($firstRow, 'UTF-8', 'UTF-8');
                for ($i = 0; $i < $ncol; ++$i) {
                    $this->db->execute($sqlHdr, [
                        "local_id"   => $localIds[$i],
                        "dataset_id" => $datasetId,
                        "name"       => (string)$firstRow[$i],
                    ]);
                }
            }

            #----------------------------------------------------
            # datarows
            #----------------------------------------------------
            $sqlDatarow = $this->dashin_config->sql->owner_import->create_dataset_datarows;
            $ordinal = 1;
            while (!$file->eof()) {

                $datarow = $file->fgetcsv($sep);
                if (count($datarow) !== $ncol) {
                    continue;
                }

                $datarowJson = Json::jsonEncode(mb_convert_encoding($datarow, 'UTF-8', 'UTF-8'));
                $this->db->execute($sqlDatarow, [
                    "dataset_id" => $datasetId,
                    "ordinal"    => $ordinal,
                    "datarow"    => $datarowJson,
                ]);
                ++$ordinal;
            }

            #----------------------------------------------------
            # set datasettype as regular dataset
            #----------------------------------------------------
            $sqlSetDatatype = ImportguideQuery::$set_datasetype;
            $this->db->execute($sqlSetDatatype, [
                "datasettype_id" => 1,
                "dataset_id" => $datasetId,
                "organization_id" => $organizationId,
            ]);

            $this->db->commit();

            return $datasetId;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * Get dataset summary
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function datasetSummary($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = ImportguideQuery::$dataset_summary;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update dataset name and description
     *
     * @param $datasetId
     *
     * @return bool
     */
    public function updateDatasetNameDescription($datasetId)
    {

        try {
            $sql = ImportguideQuery::$dataset_update_name_description;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $name = $this->request->getPost("name");
            $description = $this->request->getPost("description");

            $state = $this->db->execute($sql, [
                "dataset_id"  => $datasetId,
                "owner_org"   => $organizationId,
                "description" => $description,
                "name"        => $name,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get list of variable types
     *
     * @return array|false
     */
    public function getVariabletypes()
    {

        try {
            $sql = ImportguideQuery::$get_variabletypes;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Variables paginated
     *
     * @param $datasetId
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return void
     */
    public function getVariablesPaginated($datasetId, $page, $nrows, $searchTerm)
    {

        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["dataset_id"] = $datasetId;
        if (strlen($searchTerm) > 0) {
            $countQuery = ImportguideQuery::$get_variables_paginated_count_search;
            $itemQuery = ImportguideQuery::$get_variables_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = ImportguideQuery::$get_variables_paginated_count;
            $itemQuery = ImportguideQuery::$get_variables_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;

    }

    public function saveDesignvariableDefinition($datasetId)
    {

        $varTypes = $this->request->getPost("vartype");
        $varTypes = $this->_handleVartypes($varTypes);

    }

    public function setVariabletypes($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {

            $vartypeArray = $this->request->getPost("vartype");

            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = ImportguideQuery::$set_variabletype;

            $this->db->begin();
            $nUpdates = 0;

            foreach ($vartypeArray as $variableId => $variabletypeId) {

                if (!UniqueId::uuidValidate($variableId)) {
                    continue;
                }

                $nTemp = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                    "dataset_id"              => $datasetId,
                    "owner_org"               => $organizationId,
                    "variabletype_id"         => (int)$variabletypeId,
                    "variable_id"             => $variableId,
                    "default_variabletype_id" => 1,
                ]);
                $nUpdates = $nUpdates + ($nTemp["n"] ?? 0);
            }

            $this->db->commit();

            return isset($nUpdates);

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get summary of dataset before data-design is saved
     *
     * @param $datasetId
     *
     * @return array|false
     */
//    public function preDatadesignSummary($datasetId)
//    {
//
//        if (!UniqueId::uuidValidate($datasetId)) {
//            return false;
//        }
//        try {
//            $sqlQuery = ImportguideQuery::$generate_datatable_query_from_local_ids;
//
//            $organizationId = $this->SU->getActiveManagerOrgId();
//
//            $resultQuery = $this->db->fetchOne($sqlQuery, Enum::FETCH_ASSOC, [
//                "min_n"      => 2,
//                //                "local_ids"  => "{}",
//                "local_ids"  => "{2,3,4,5,6,7,8}",
//                "dataset_id" => $datasetId,
//                "owner_org"  => $organizationId,
//            ]);
//
//            if (!isset($resultQuery["full_query"])) {
//                return false;
//            }
//            $result = $this->db->fetchAll($resultQuery["full_query"], Enum::FETCH_ASSOC);
//
//            if ($result) {
//                return $result;
//            }
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//
//        return false;
//    }

    /**
     * Checks if count over the dataset's design variables
     * leads to duplicate entries.
     * Returns true if there are duplicates, false if not
     *
     * @param $datasetId
     *
     * @return false|mixed
     */
    public function datasetDesignHasDuplicates($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = ImportguideQuery::$datasetdesign_has_duplicates;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if (isset($result["has_duplicates"])) {
                return $result["has_duplicates"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getPreDatasetDesignTable($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = ImportguideQuery::$pre_dataset_design_table;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Save dataset design or update if already saved
     *
     * If 'update' the dataset design is first deleted and then saved
     *
     * @param $datasetId
     *
     * @return false
     */
    public function saveOrUpdateDatadesign($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $organizationId = $this->SU->getActiveManagerOrgId();

            #-------------------------------------------------
            # check if design already exists
            #-------------------------------------------------
            $sql = ImportguideQuery::$datasetdesign_exists;
            $designExistResult = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            #-------------------------------------------------
            # delete design if it already exists
            #-------------------------------------------------
            if (isset($designExistResult["design_exists"]) && $designExistResult["design_exists"]) {
                $deleteDesignResult = $this->deleteDatadesign($datasetId);
            }

            #-------------------------------------------------
            # set variable types
            #-------------------------------------------------
            $this->setVariabletypes($datasetId);

            #-------------------------------------------------
            # update design-variables
            #-------------------------------------------------
            $saveDesignResult = $this->saveDatadesign($datasetId);
            if ($saveDesignResult) {
                return $saveDesignResult;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete dataset design
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function deleteDatadesign($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {

            $sql = ImportguideQuery::$delete_datasetdesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $deleteDesignResult = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($deleteDesignResult) {
                return $deleteDesignResult;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Save dataset design
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function saveDatadesign($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {

            $sql = ImportguideQuery::$save_datasetdesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $saveDesignResult = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($saveDesignResult) {
                return $saveDesignResult;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}