<?php

namespace Dashin\Helpers\Owner;

use Dashin\Helpers\QueryBase;

class ImportguideQuery extends QueryBase
{

    /**
     * Identify numeric variables in a dataset
     * and set to numeric datatype
     *
     * @param: limit
     * @param: dataset_id
     *
     * @var string $dataset_set_datatype_numeric
     */
    static string $dataset_set_datatype_numeric =
        <<<'EOD'
            UPDATE dashin.variable dv
            SET (status, datatype_id) = (3, x.datatype_id)
            FROM
            (
              SELECT
                variable_id
                , local_id
                , is_num
                , CASE WHEN is_num THEN 2
                       ELSE 1
                  END AS datatype_id
              FROM
                dashin.variable dv
                , LATERAL (SELECT
                             sum(is_numeric) = 0 AS is_num
                           FROM
                             (
                               SELECT
                                 (NOT jsonb_text_is_numeric(jsonb_array_element(datarow , dv.local_id)))::INT AS is_numeric
                               FROM
                                 dashin.datatable
                               WHERE dataset_id = dv.dataset_id
                               AND ordinal <= :limit
                             ) x0
                )               l1
              WHERE dataset_id = :dataset_id
            ) x
            WHERE dv.variable_id = x.variable_id;
        EOD;

    /**
     * Summary for a dataset
     *
     * @param: dataset_id
     * @param: owner_org
     *
     * @var string $dataset_summary
     */
    static string $dataset_summary =
        <<<'EOD'
            SELECT a.dataset_id
                 , a.name
                 , a.description
                 , d.datasettype_id
                 , d.name AS datasetype_name
                 , b.n_cols
                 , c.n_rows
            FROM dashin.dataset                                                                        a
                 LEFT JOIN dashin.datasettype                                                          d ON a.datasettype_id = d.datasettype_id
               , LATERAL (SELECT count(*) FROM dashin.variable b0 WHERE b0.dataset_id = a.dataset_id)  b(n_cols)
               , LATERAL (SELECT count(*) FROM dashin.datatable c0 WHERE c0.dataset_id = a.dataset_id) c(n_rows)
            WHERE dataset_id = :dataset_id
              AND owner_org = :owner_org;
        EOD;

    /**
     * Update name and description
     *
     * @param: name
     * @param: description
     * @param: dataset_id
     * @param: owner_org
     *
     * @var string $dataset_update_name_description
     */
    static string $dataset_update_name_description =
        <<<'EOD'
            UPDATE dashin.dataset
                SET (name, description) = (:name, :description)
            WHERE dataset_id = :dataset_id
              AND owner_org = :owner_org;
        EOD;

    /**
     * Get all variable types
     *
     * @var string $get_variabletypes
     */
    static string $get_variabletypes =
        <<<'EOD'
            SELECT variabletype_id
                 , name
                 , description
            FROM dashin.variabletype;
        EOD;

    /**
     * Paginated variables
     *
     * @var string $get_variables_paginated_count_search
     */
    static string $get_variables_paginated_count_search =
        <<<'EOD'
            SELECT count(*) AS n
            FROM dashin.variable        a
              INNER JOIN dashin.dataset b ON a.dataset_id = b.dataset_id
            WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org)
              AND lower(concat(a.name , ' ' , a.description))
              LIKE lower(concat('%' , (:search_term)::TEXT , '%'))
        EOD;

    static string $get_variables_paginated_search =
        <<<'EOD'
            SELECT a.variable_id
                 , a.local_id
                 , a.name
                 , a.description
                 , a.datatype_id
                 , a.variabletype_id
                 , c.name AS datatype
                 , d.name AS variabletype
            FROM dashin.variable             a
              INNER JOIN dashin.dataset      b ON a.dataset_id = b.dataset_id
              INNER JOIN dashin.datatype     c ON a.datatype_id = c.datatype_id
              INNER JOIN dashin.variabletype d ON a.variabletype_id = d.variabletype_id
            WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org)
              AND lower(concat(a.name , ' ' , a.description))
              LIKE lower(concat('%' , (:search_term)::TEXT , '%'))
            ORDER BY a.local_id
            OFFSET :offset
            LIMIT :limit;
        EOD;

    static string $get_variables_paginated_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM dashin.variable        a
              INNER JOIN dashin.dataset b ON a.dataset_id = b.dataset_id
            WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org);
        EOD;

    static string $get_variables_paginated =
        <<<'EOD'
            SELECT a.variable_id
                 , a.local_id
                 , a.name
                 , a.description
                 , a.datatype_id
                 , a.variabletype_id
                 , c.name AS datatype
                 , d.name AS variabletype
            FROM dashin.variable             a
              INNER JOIN dashin.dataset      b ON a.dataset_id = b.dataset_id
              INNER JOIN dashin.datatype     c ON a.datatype_id = c.datatype_id
              INNER JOIN dashin.variabletype d ON a.variabletype_id = d.variabletype_id
            WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org)
            ORDER BY a.local_id
            OFFSET :offset
            LIMIT :limit;
        EOD;

    /**
     * Saves the design for a dataset
     *
     * @param: dataset_id
     * @param: owner_org
     *
     * @var string $save_datasetdesign
     */
    static string $save_datasetdesign =
        <<<'EOD'
            SELECT name
               , affected_rows
               , description
            FROM dashin.datadesign_save_design(:dataset_id , :owner_org);
        EOD;

    static string $datasetdesign_exists =
        <<<'EOD'
            SELECT dashin.datadesign_exists(:dataset_id,:owner_org)::INT AS design_exists;
        EOD;

    static string $delete_datasetdesign =
        <<<'EOD'
            SELECT message
                 , status
            FROM dashin.datadesign_delete_design(:dataset_id,:owner_org);
        EOD;

    static string $set_datasetype = "
        UPDATE dashin.dataset
        SET (datasettype_id, updated_at) = (:datasettype_id, now())
        WHERE (dataset_id, owner_org) = (:dataset_id, :organization_id);";

    static string $set_variabletype =
        <<<'EOD'
            WITH
              design_exists_query        AS (
                                              SELECT dashin.datadesign_exists(:dataset_id, :owner_org) AS design_exists
                                            )
              -- get all variables (remove design-variable if design exists)
              , variable_query           AS (
                                              SELECT a.variable_id
                                                   , a.variabletype_id
                                                   , c.design_exists
                                              FROM dashin.variable        a
                                                INNER JOIN dashin.dataset b ON a.dataset_id = b.dataset_id
                                              , design_exists_query       c
                                              WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org)
                                               AND NOT (
                                                  c.design_exists
                                                  AND a.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
                                                )
                     )
              -- find if there is an existing design-variable of the same type so it can be reset
              , designvar_exists_query   AS (
                                              SELECT a.variable_id
                                              FROM variable_query a
                                              WHERE a.variabletype_id = :variabletype_id
                                                AND :variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
                                                AND NOT a.design_exists
                                                AND a.variable_id != :variable_id
                                            )
              -- reset existing design-variable if setting another variable to same value (design-variables are filtered out in above query if design exists)
              , reset_existing_query     AS (
                                              UPDATE dashin.variable a SET (variabletype_id, updated_at) = (:default_variabletype_id, now())
                                                WHERE a.variable_id IN (
                                                                         SELECT variable_id
                                                                         FROM designvar_exists_query
                                                                       )
                                                RETURNING a.variable_id
                                            )
              -- update to new variabletype
              , update_new_vartype_query AS (
                                              
                                              UPDATE dashin.variable a SET (variabletype_id, updated_at) = (:variabletype_id, now())
                                                FROM design_exists_query b
                                                -- force execution of reset-query before this one
                                                WHERE a.variable_id NOT IN (
                                                                             SELECT variable_id
                                                                             FROM reset_existing_query
                                                                           )
                                                  -- don't update if trying to set it to the same value as it already has
                                                  AND (a.variable_id, a.dataset_id) = (:variable_id, :dataset_id)
                                                  AND variabletype_id != :variabletype_id
                                                  -- don't set new design-variables if design exists
                                                  AND NOT (b.design_exists AND :variabletype_id IN (2, 3, 4, 5, 6, 7, 8))
                                                RETURNING a.variable_id
                                            
                                            )
            SELECT count(*) AS n
            FROM update_new_vartype_query;
        EOD;

    static string $generate_datatable_query_from_local_ids =
        <<<'EOD'
            SELECT dataset_id
                 , E'SELECT *\nFROM (\n' || count_query_top || inner_query || groupby_query || E'\n)x2 WHERE n >= (' || :min_n || ')::INT' AS full_query
                 , variable_ids
            FROM (
                   SELECT dataset_id
                        , E'SELECT count(*) AS n,\n' || string_agg(variable_id_name || ' AS ' || varname , E',\n' ORDER BY local_id) || E'\nFROM (\n'                                                           AS count_query_top
                        , E'SELECT \n' || string_agg(column_extraction || ' AS ' || variable_id_name , E',\n' ORDER BY local_id) || E'\nFROM dashin.datatable WHERE dataset_id = ' || quote_literal(dataset_id) AS inner_query
                        , E'\n)x\nGROUP BY \n' || string_agg(variable_id_name , E',\n' ORDER BY local_id)                                                                                                       AS groupby_query
                        , array_agg(variable_id)                                                                                                                                                                AS variable_ids
                   FROM (
                          SELECT a.dataset_id
                               , a.variable_id
                               , a.local_id
                               , quote_ident(a.name)                                      AS varname
                               , quote_ident(a.variable_id::TEXT)                         AS variable_id_name
                               , 'jsonb_array_element_text(datarow,' || a.local_id || ')' AS column_extraction
                          FROM dashin.variable             a
                            INNER JOIN dashin.dataset      b ON a.dataset_id = b.dataset_id
                            INNER JOIN dashin.variabletype c ON a.variabletype_id = c.variabletype_id
                          WHERE (b.dataset_id, b.owner_org) = (:dataset_id, :owner_org)
                            AND a.variabletype_id IN (
                                                       SELECT unnest((:local_ids)::INT[])
                                                     )
                          ORDER BY a.local_id
                        ) x0
                   GROUP BY dataset_id
                 ) x1;
         EOD;

    static string $datasetdesign_has_duplicates =
        <<<'EOD'
            WITH
              base_query               AS (
                                            SELECT jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'event'))         AS event_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'subevent'))      AS subevent_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'samplingevent')) AS samplingevent_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'samplingtime'))  AS samplingtime_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'center'))        AS center_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'startgroup'))    AS startgroup_name
                                                 , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'subject'))       AS subject_name
                                            FROM dashin.datatable dt
                                            WHERE dt.dataset_id = :dataset_id
                                              AND exists
                                              (
                                                SELECT
                                                FROM dashin.dataset d0
                                                WHERE (d0.dataset_id, d0.owner_org) = (:dataset_id, :owner_org)
                                              )
                                          )
              , count_distinct_query   AS (
                                            SELECT event_name
                                                 , subevent_name
                                                 , samplingevent_name
                                                 , samplingtime_name
                                                 , center_name
                                                 , startgroup_name
                                                 , subject_name
                                                 , count(*) AS n
                                            FROM base_query
                                            GROUP BY event_name
                                                   , subevent_name
                                                   , samplingevent_name
                                                   , samplingtime_name
                                                   , center_name
                                                   , startgroup_name
                                                   , subject_name
                                          )
              , check_duplicates_query AS (
                                            SELECT count(*)>0 AS has_duplicates
                                            FROM count_distinct_query
                                            WHERE n > 1
                                          )
            SELECT has_duplicates
            FROM check_duplicates_query;
        EOD;

    static string $pre_dataset_design_table =
        <<<'EOD'
            WITH
              base_query             AS (
                                          SELECT jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'event'))         AS event_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'subevent'))      AS subevent_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'samplingevent')) AS samplingevent_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'samplingtime'))  AS samplingtime_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'center'))        AS center_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'startgroup'))    AS startgroup_name
                                               , jsonb_array_element_text(datarow, dashin.datadesign_variable_get_local_id(dt.dataset_id, 'subject'))       AS subject_name
                                          FROM dashin.datatable dt
                                            WHERE dt.dataset_id = :dataset_id
                                              AND exists
                                              (
                                                SELECT
                                                FROM dashin.dataset d0
                                                WHERE (d0.dataset_id, d0.owner_org) = (:dataset_id, :owner_org)
                                            )
                                        )
              , count_distinct_query AS (
                                          SELECT event_name
                                               , subevent_name
                                               , samplingevent_name
                                               , samplingtime_name
                                               , center_name
                                               , startgroup_name
                                               , subject_name
                                               , count(*) AS n
                                          FROM base_query
                                          GROUP BY event_name
                                                 , subevent_name
                                                 , samplingevent_name
                                                 , samplingtime_name
                                                 , center_name
                                                 , startgroup_name
                                                 , subject_name
                                        )
            SELECT event_name
                 , subevent_name
                 , samplingevent_name
                 , samplingtime_name
                 , center_name
                 , startgroup_name
                 , subject_name
                 , n
            FROM count_distinct_query;
        EOD;

}