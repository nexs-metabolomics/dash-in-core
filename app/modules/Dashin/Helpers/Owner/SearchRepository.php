<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-09
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class SearchRepository extends RepositoryBase
{
    public function searchMetabolomicsSummary($searchParams)
    {
        if (strlen($searchParams["mz_center"] . $searchParams["mz_range"] . $searchParams["rt_center"] . $searchParams["rt_range"]) === 0) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->metabolomics_summary;

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "mz_center" => $searchParams["mz_center"],
                "mz_range"  => $searchParams["mz_range"],
                "rt_center" => $searchParams["rt_center"],
                "rt_range"  => $searchParams["rt_range"],
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function searchMetabolomicsPaginated($totalItems, $itemParams, $page, $nrows)
    {

        if (!($totalItems && strlen($itemParams["mz_center"].$itemParams["mz_range"].$itemParams["rt_center"].$itemParams["rt_range"]) > 0)) {
            return false;
        }
        $itemQuery = $this->dashin_config->sql->owner_search->metabolomics_items;
        $result = $this->_preparePaginationSearch(
            $this->db,
            $totalItems,
            $itemQuery,
            $itemParams,
            $page,
            $nrows
        );
        return $result;
    }

    public function searchMetabolomicsType02Summary($searchParams,$searchData)
    {
        try {
            if($searchData) {
                $sql = $this->dashin_config->sql->owner_search->metabolomics_type02_summary;
            } else {
                $sql = $this->dashin_config->sql->owner_search->metabolomics_type02_summary_no_data;
            }

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, $searchParams);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function searchMetabolomicsType02Paginated($totalItems, $itemParams, $page, $nrows)
    {
        if (!($totalItems && strlen($itemParams["mz_center"].$itemParams["mz_range"].$itemParams["rt_center"].$itemParams["rt_range"]) > 0)) {
            return false;
        }
        $itemQuery = $this->dashin_config->sql->owner_search->metabolomics_type02_paginated;
        $result = $this->_preparePaginationSearch(
            $this->db,
            $totalItems,
            $itemQuery,
            $itemParams,
            $page,
            $nrows
        );
        return $result;
    }

    public function saveMetabolomicsResultset($searchParams)
    {
        if (strlen($searchParams["mz_center"].$searchParams["mz_range"].$searchParams["rt_center"].$searchParams["rt_range"]) === 0) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_search->save_resultset;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "owner_user"  => $userId,
                "mz_center"   => $searchParams["mz_center"],
                "mz_range"    => $searchParams["mz_range"],
                "rt_center"   => $searchParams["rt_center"],
                "rt_range"    => $searchParams["rt_range"],
            ]);
            if (isset($result["resultset_id"])) {
                return $result["resultset_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function saveMetabolomicsType02Resultset($searchParams)
    {
        if (strlen($searchParams["mz_center"].$searchParams["mz_range"].$searchParams["rt_center"].$searchParams["rt_range"]) === 0) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_search->save_type02_resultset;

            $searchParams["name"] = $this->request->getPost("name");
            $searchParams["description"] = $this->request->getPost("description");
            $searchParams["owner_user"] = $this->SU->getUserId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, $searchParams);
            if (isset($result["resultset_id"])) {
                return $result["resultset_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function saveSearchResults($innerSql, $queryParms, $searchParams)
    {
        if (strlen($searchParams["mz_center"].$searchParams["mz_range"].$searchParams["rt_center"].$searchParams["rt_range"]) === 0) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->variables_save_resultset;

            $userId = $this->SU->getUserId();

            $searchParamsJson = Json::jsonEncode(["search_params" => $searchParams]);

            $this->db->begin();
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "owner_user"  => $userId,
                "details"     => $searchParamsJson,
            ]);
            if (!(isset($result["resultset_id"]) && $result["resultset_id"])) {
                return false;
            }
            $resultsetId = $result["resultset_id"];
            $sql = "INSERT INTO dashin.resultset_x_variable (resultset_id, variable_id)";
            $sql .= "\nSELECT ('$resultsetId')::UUID, variable_id \n$innerSql";
            $state = $this->db->execute($sql, $queryParms);
            $this->db->commit();
            return $resultsetId;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getResultsetsPaginated($page, $nrows)
    {
        $itemQuery = $this->dashin_config->sql->owner_search->list_resultsets_paginated;
        $countQuery = $this->dashin_config->sql->owner_search->list_resultsets_count;
        $userId = $this->SU->getUserId();
        $queryParams = ["owner_user" => $userId];
        $result = $this->_preparePagination2($this->db, $countQuery, $queryParams, $itemQuery, $queryParams, $page, $nrows);

        return $result;
    }

    public function getResultsetDatasetVarsSelectedPaginated($resultsetId, $datasetId, $page, $nrows, $searchTerm)
    {
        $userId = $this->SU->getUserId();
        $queryParams = [
            "owner_user"   => $userId,
            "resultset_id" => $resultsetId,
            "dataset_id"   => $datasetId,
        ];
        if(strlen($searchTerm)>0) {
            $itemQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_selected_paginated_search;
            $countQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_selected_count_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $itemQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_selected_paginated;
            $countQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_selected_count;
        }
        $result = $this->_preparePagination2($this->db, $countQuery, $queryParams, $itemQuery, $queryParams, $page, $nrows);

        return $result;
    }

    public function getResultsetDatasetVarsUnselectedPaginated($resultsetId, $datasetId, $page, $nrows, $searchTerm)
    {
        $userId = $this->SU->getUserId();
        $queryParams = [
            "owner_user"   => $userId,
            "resultset_id" => $resultsetId,
            "dataset_id"   => $datasetId,
        ];
        if(strlen($searchTerm)>0) {
            $itemQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_unselected_paginated_search;
            $countQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_unselected_count_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $itemQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_unselected_paginated;
            $countQuery = $this->dashin_config->sql->owner_search->resultset_dataset_vars_unselected_count;
        }
        $result = $this->_preparePagination2($this->db, $countQuery, $queryParams, $itemQuery, $queryParams, $page, $nrows);

        return $result;
    }

    public function getResultset($resultsetId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->view_resultset;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "resultset_id" => $resultsetId,
                "owner_user"   => $userId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getResultsetMinimal($resultsetId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->get_resultset_minimal;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "resultset_id" => $resultsetId,
                "owner_user"   => $userId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function addResultsetVariable($resultsetId, $variableId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($variableId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->add_resultset_variable;

            $userId = $this->SU->getUserId();

            $state = $this->db->execute($sql, [
                "resultset_id" => $resultsetId,
                "variable_id"  => $variableId,
                "owner_user"   => $userId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function removeResultsetVariable($resultsetId, $variableId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($variableId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_search->remove_resultset_variable;

            $userId = $this->SU->getUserId();

            $state = $this->db->execute($sql, [
                "resultset_id" => $resultsetId,
                "variable_id"  => $variableId,
                "owner_user"   => $userId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getResultsetDatasetIds($resultsetId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_search->resultset_get_dataset_ids;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "resultset_id" => $resultsetId,
                "owner_user"   => $userId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function extractDataset($resultsetId, $datasetId)
    {
        if (!UniqueId::uuidValidate($resultsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_search->resultset_generate_export_query;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "resultset_id" => $resultsetId,
                "dataset_id"   => $datasetId,
                "owner_user"   => $userId,
            ]);

            if (!(isset($result["qry"]) && $result["qry"])) {
                return false;
            }

            $exportQuery = $result["qry"];

            $result = $this->db->fetchAll($exportQuery, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;

    }
}
