<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-24
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use Dashin\Helpers\RepositoryBase;

class ServiceRepository extends RepositoryBase
{
    public function getSeclabelsPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_id"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->fgac_get_seclabels_paginated_count_search;
            $itemQuery = $this->dashin_config->sql->fgac_get_seclabels_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->fgac_get_seclabels_paginated_count;
            $itemQuery = $this->dashin_config->sql->fgac_get_seclabels_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getDatasetPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_datasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_datasets_paginated;
        }
        
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getDatadesignPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_datadesigns_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_datadesigns_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_datadesigns_count;
            $itemQuery = $this->dashin_config->sql->service->get_datadesigns_paginated;
        }
        
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getAssayPaginated($page, $nrows, $searchTerm)
    {
        $queryParams=[];
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_assays_paginated_search_count;
            $itemQuery = $this->dashin_config->sql->service->get_assays_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_assays_paginated_count;
            $itemQuery = $this->dashin_config->sql->service->get_assays_paginated;
        }
        
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getDatacolumntypePaginated($page, $nrows, $searchTerm)
    {
        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_search_count;
            $itemQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_count;
            $itemQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getUpvarDatasetPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_upvar_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_upvar_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_upvar_datasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_upvar_datasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getVarDatasetPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $queryParams["dataset_id"] = $datasetId;
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_var_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_var_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_var_datasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_var_datasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studies_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studies_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studies_count;
            $itemQuery = $this->dashin_config->sql->service->get_studies_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyfilePaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["study_id"] = $this->localsession->getKeyValue("dashin_owner_study_id");
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studyfiles_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studyfiles_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studyfiles_count;
            $itemQuery = $this->dashin_config->sql->service->get_studyfiles_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudydatasetPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studydatasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studydatasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studydatasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_studydatasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getSubjectPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["study_id"] = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_subjects_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_subjects_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_subjects_count;
            $itemQuery = $this->dashin_config->sql->service->get_subjects_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getCenterPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_centers_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_centers_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_centers_count;
            $itemQuery = $this->dashin_config->sql->service->get_centers_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStartgroupPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_startgroups_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_startgroups_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_startgroups_count;
            $itemQuery = $this->dashin_config->sql->service->get_startgroups_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getEventPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_events_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_events_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_events_count;
            $itemQuery = $this->dashin_config->sql->service->get_events_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getSubeventPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_subevents_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_subevents_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_subevents_count;
            $itemQuery = $this->dashin_config->sql->service->get_subevents_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudycontactPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studycontacts_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studycontacts_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studycontacts_count;
            $itemQuery = $this->dashin_config->sql->service->get_studycontacts_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getConsortiumPaginated($page,$nrows,$searchTerm)
    {
        $queryParams = [];
        if(strlen($searchTerm)>0) {
            $countQuery = $this->dashin_config->sql->service->get_consortiums_paginated_search_count;
            $itemQuery = $this->dashin_config->sql->service->get_consortiums_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_consortiums_paginated_count;
            $itemQuery = $this->dashin_config->sql->service->get_consortiums_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getResultsetsPaginated($page,$nrows,$searchTerm)
    {
        $userId = $this->SU->getUserId();
        $queryParams = [
            "owner_user" => $userId
        ];
        if(strlen($searchTerm)>0) {
            $countQuery = $this->dashin_config->sql->service->get_resultsets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_resultsets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_resultsets_count;
            $itemQuery = $this->dashin_config->sql->service->get_resultsets_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getResultsetDatasetsPaginated($page,$nrows,$searchTerm)
    {
        $resultsetId = $this->localsession->getKeyValue("dashin_owner_resultset_id");
        $userId = $this->SU->getUserId();
        $queryParams = [
            "owner_user" => $userId,
            "resultset_id" => $resultsetId
        ];
        if(strlen($searchTerm)>0) {
            $countQuery = $this->dashin_config->sql->service->get_resultset_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_resultset_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_resultset_datasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_resultset_datasets_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getSamplingEventPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_sampling_events_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_sampling_events_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_sampling_events_count;
            $itemQuery = $this->dashin_config->sql->service->get_sampling_events_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getVariablesPaginated($page, $nrows, $searchTerm)
    {
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $queryParams["dataset_id"] = $datasetId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_variables_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_variables_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_variables_count;
            $itemQuery = $this->dashin_config->sql->service->get_variables_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyrowsetPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studyrowset_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studyrowset_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studyrowset_count;
            $itemQuery = $this->dashin_config->sql->service->get_studyrowset_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyvarconditionPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studyvarcondition_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studyvarcondition_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studyvarcondition_count;
            $itemQuery = $this->dashin_config->sql->service->get_studyvarcondition_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyvariablesetPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studyvariableset_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studyvariableset_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studyvariableset_count;
            $itemQuery = $this->dashin_config->sql->service->get_studyvariableset_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function getStudyexportsetPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_studyexportsets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_studyexportsets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_studyexportsets_count;
            $itemQuery = $this->dashin_config->sql->service->get_studyexportsets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

}