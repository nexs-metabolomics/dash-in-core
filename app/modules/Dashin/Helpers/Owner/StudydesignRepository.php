<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-14
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class StudydesignRepository extends RepositoryBase
{
    public function getStudyIdFromEventId($eventId)
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_study_id_from_event_id;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "event_id" => $eventId,
            ]);

            if (isset($result["study_id"])) {
                return $result["study_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSelectionChoices()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_selection_choices;

            $result = $this->db->fetchAll($sql);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getTimeUnits()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_time_units;

            $result = $this->db->fetchAll($sql);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubeventTypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevent_types;

            $result = $this->db->fetchAll($sql);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getActionType($actionTypeId)
    {
        if (!UniqueId::uuidValidate($actionTypeId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevent_type;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "subevent_type_id" => $actionTypeId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getActionSubtypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_action_types;

            $result = $this->db->fetchAll($sql);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function createCenter($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_center;

            # set label to name if not provided
            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "name"        => $this->request->getPost("name"),
                "label"       => $label,
                "description" => $this->request->getPost("description"),
            ]);

            if (isset($result["center_id"])) {
                return $result["center_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateCenter($centerId, $studyId)
    {
        if (!UniqueId::uuidValidate($centerId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_center;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "center_id"   => $centerId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
                "name"        => $this->request->getPost("name"),
                "label"       => $this->request->getPost("label"),
                "description" => $this->request->getPost("description"),
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getCenter($centerId, $studyId)
    {
        if (!UniqueId::uuidValidate($centerId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_center;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "center_id" => $centerId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $studyId
     *
     * @return false
     */
    public function getCenters($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_centers;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function createStartgroup($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_startgroup;

            # set label to name if not provided
            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "name"        => $this->request->getPost("name"),
                "label"       => $label,
                "description" => $this->request->getPost("description"),
            ]);

            if (isset($result["startgroup_id"])) {
                return $result["startgroup_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateStartgroup($startgroupId, $studyId)
    {
        if (!UniqueId::uuidValidate($startgroupId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_startgroup;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "startgroup_id" => $startgroupId,
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
                "name"          => $this->request->getPost("name"),
                "label"         => $this->request->getPost("label"),
                "description"   => $this->request->getPost("description"),
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStartgroups($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_startgroups;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function deleteStartgroup($startgroupId)
    {
        if (!UniqueId::uuidValidate($startgroupId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_startgroup;

            #$organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "startgroup_id" => $startgroupId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStartgroupsByEventSubevent($eventId, $subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_startgroups_by_event_subevent;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "event_id"    => $eventId,
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStartgroupsNotInEventSubevent($event, $subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_startgroups_not_in_event_subevent;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "event_id"    => $event,
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStartgroup($startgroupId, $studyId)
    {
        if (!UniqueId::uuidValidate($startgroupId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_startgroup;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "startgroup_id" => $startgroupId,
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * TODO: Unused method
     * @return false
     */
    public function addStartgroupToSubject()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_startgroup_to_subject;

            $result = $this->db->fetchOne($sql);
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function createEvent($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_event;

            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "name"        => $this->request->getPost("name"),
                "label"       => $label,
                "description" => $this->request->getPost("description"),
            ]);

            if (isset($result["event_id"])) {
                return $result["event_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_event;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "event_id"  => $eventId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_event;
            $organiztionId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "event_id"    => $eventId,
                "study_id"    => $studyId,
                "owner_org"   => $organiztionId,
                "name"        => $this->request->getPost("name"),
                "label"       => $this->request->getPost("label"),
                "description" => $this->request->getPost("description"),
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
        return false;
    }

    public function addSubeventToEvent($eventId, $studyId, $subeventId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_subevent_to_event;

            $state = $this->db->execute($sql, [
                "event_id"    => $eventId,
                "study_id"    => $studyId,
                "subevent_id" => $subeventId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function removeSubeventFromEvent($eventId, $studyId, $subeventId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->remove_subevent_from_event;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "event_id"    => $eventId,
                "study_id"    => $studyId,
                "subevent_id" => $subeventId,
                "owner_org"   => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubeventsInEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevents_in_event;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "event_id"  => $eventId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubeventsNotInEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevents_not_in_event;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "event_id"  => $eventId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getEventPaginated($page, $nrows, $searchTerm)
    {
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studydesign->get_events_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studydesign->get_events_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studydesign->get_events_count;
            $itemQuery = $this->dashin_config->sql->owner_studydesign->get_events_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    public function createSubevent($studyId, $additionalFieldNames, $subeventTypeId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_subevent;

            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $additionalFieldsJson = null;
            if (is_array($additionalFieldNames)) {
                $tmpFieldValues = [];
                foreach ($additionalFieldNames as $fieldName) {
                    $tmpFieldValues[$fieldName] = $this->request->getPost($fieldName);
                }
                if ($tmpFieldValues) {
                    $additionalFieldsJson = Json::jsonEncode(["fields" => $tmpFieldValues]);
                }
            }

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "subevent_type_id"     => (int)$subeventTypeId,
                "study_id"             => $studyId,
                "owner_org"            => $organizationId,
                "intervention_type_id" => $this->request->getPost("action_type"),
                "name"                 => $this->request->getPost("name"),
                "label"                => $label,
                "description"          => $this->request->getPost("description"),
                "row_comment"          => $this->request->getPost("row_comment"),
                "additional_data"      => $additionalFieldsJson,
            ]);

            if (isset($result["subevent_id"])) {
                return $result["subevent_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\Exception $e) {
            $e;
        }
        return false;
    }

    public function deleteSubevent($subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_subevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function deleteEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_event;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "event_id"  => $eventId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubeventsByEvent($eventId, $studyId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevents_by_event;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "event_id"  => $eventId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateSubevent($subeventId, $studyId, $additionalFieldsJson)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_subevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "subevent_id"          => $subeventId,
                "study_id"             => $studyId,
                "owner_org"            => $organizationId,
                "subevent_type_id"     => $this->request->getPost("subevent_type"),
                "intervention_type_id" => $this->request->getPost("action_type"),
                "name"                 => $this->request->getPost("name"),
                "label"                => $this->request->getPost("label"),
                "description"          => $this->request->getPost("description"),
                "row_comment"          => $this->request->getPost("row_comment"),
                "additional_fields"    => $additionalFieldsJson,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubevents($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevents;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubevent($subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevent;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);
            if ($result) {
                if (isset($result["additional_data"]) && $result["additional_data"]) {
                    $result["additional_data"] = Json::jsonDecode($result["additional_data"], JSON_OBJECT_AS_ARRAY);
                }
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSubeventTypeIdFromSubeventId($subeventId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_subevent_type_id_from_subevent_id;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "subevent_id" => $subeventId,
            ]);
            if (isset($result["subevent_type_id"])) {
                return $result["subevent_type_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingTypeIdFromSamplingEventId($samplingeventId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_sampling_type_id_from_samplingevent_id;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "samplingevent_id" => $samplingeventId,
            ]);
            if (isset($result["sampling_type_id"])) {
                return $result["sampling_type_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingTypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_sampling_types;

            $result = $this->db->fetchAll($sql);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function removeStartgroupFromEventSubevent($eventId, $subeventId, $studyId, $startgroupId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($startgroupId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->remove_startgroup_from_event_subevent;

            $state = $this->db->execute($sql, [
                "event_id"      => $eventId,
                "subevent_id"   => $subeventId,
                "startgroup_id" => $startgroupId,
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function addStartgroupToEventSubevent($eventId, $subeventId, $studyId, $startgroupId)
    {
        if (!UniqueId::uuidValidate($eventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($startgroupId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_startgroup_to_event_subevent;

            $organizationId = $this->SU->getActiveManagerOrgId();
            $state = $this->db->execute($sql, [
                "event_id"      => $eventId,
                "subevent_id"   => $subeventId,
                "startgroup_id" => $startgroupId,
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingEvents($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_samplingevents;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingevent($samplingeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_samplingevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);
            if ($result) {
                if (isset($result["additional_data"]) && $result["additional_data"]) {
                    $result["additional_data"] = Json::jsonDecode($result["additional_data"], JSON_OBJECT_AS_ARRAY);
                }
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateSamplingtimes($samplingeventId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_sampling_time;
            $st = $this->request->getPost("st");
            if (is_array($st)) {
                $this->db->begin();
                foreach ($st as $key => $values) {
                    if (!UniqueId::uuidValidate($key)) {
                        return false;
                    }
                    $state = $this->db->execute($sql, [
                        "name"             => $values["name"],
                        "description"      => $values["description"],
                        "samplingtime_id"  => $key,
                        "samplingevent_id" => $samplingeventId,
                    ]);
                }
                $this->db->commit();
            }

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function addSamplingtime($samplingeventId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_sampling_time;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "samplingevent_id" => $samplingeventId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function reorderSamplingtimeCategories($samplingtimeId, $samplingeventId, $studyId, $moveDirection)
    {
        if (!UniqueId::uuidValidate($samplingtimeId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            if ($moveDirection === "up") {
                $sql = $this->dashin_config->sql->owner_studydesign->update_samplingtime_moveup;
            } elseif (($moveDirection === "down")) {
                $sql = $this->dashin_config->sql->owner_studydesign->update_samplingtime_movedown;
            } else {
                return false;
            }

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "samplingtime_id"  => $samplingtimeId,
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function removeSamplingtime($samplingeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_samplingtime;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingtimes($samplingeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_samplingtimes;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function createSamplingEvent($studyId, $samplingTypeId, $additionalFieldsJson)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->create_samplingevent;

            # set label to name if not provided
            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "sampling_type_id" => (int)$samplingTypeId,
                "study_id"         => $studyId,
                "name"             => $this->request->getPost("name"),
                "label"            => $label,
                "description"      => $this->request->getPost("description"),
                "row_comment"      => $this->request->getPost("row_comment"),
                "additional_data"  => $additionalFieldsJson,
            ]);

            if (isset($result["samplingevent_id"])) {
                return $result["samplingevent_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateSamplingEvent($samplingeventId, $studyId, $samplingTypeId, $additionalFieldsJson)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->update_samplingevent;

            $organizationid = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "sampling_type_id"  => (int)$samplingTypeId,
                "samplingevent_id"  => $samplingeventId,
                "study_id"          => $studyId,
                "owner_org"         => $organizationid,
                "name"              => $this->request->getPost("name"),
                "label"             => $this->request->getPost("label"),
                "description"       => $this->request->getPost("description"),
                "additional_fields" => $additionalFieldsJson,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function deleteSamplingevent($samplingeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_samplingevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingEventsBySubevent($subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_sampling_events_by_subevent;

            $organizationId = $this->SU->getActiveManagerOrgId();
            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getSamplingEventsNotInSubevent($subeventId, $studyId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_sampling_events_not_in_subevent;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "subevent_id" => $subeventId,
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function removeSamplingEventFromSubevent($subeventId, $studyId, $samplingeventId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->remove_sampling_event_from_subevent;

            $state = $this->db->execute($sql, [
                "subevent_id"      => $subeventId,
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function addSamplingEventToSubevent($subeventId, $studyId, $samplingeventId)
    {
        if (!UniqueId::uuidValidate($subeventId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($samplingeventId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->add_sampling_event_to_subevent;

            $state = $this->db->execute($sql, [
                "subevent_id"      => $subeventId,
                "samplingevent_id" => $samplingeventId,
                "study_id"         => $studyId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function datadesignEsists($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = $this->dashin_config->sql->owner_studydesign->datadesign_exists;
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            # only return summary of existing design
            if (isset($result["datadesign_exists"]) && $result["datadesign_exists"] === 1) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function datadesignGetSummaryForImport($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = $this->dashin_config->sql->owner_studydesign->get_datadesign_summary_for_import;

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignImportDatadesign($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->datadesign_import;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignDatadesignIsImported($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->datadesign_is_imported;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
            ]);

            if (isset($result["is_imported"])) {
                return $result["is_imported"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummaryEvent($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_event;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummarySubevent($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_subevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummarySamplingevent($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_samplingevent;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummarySamplingtime($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_samplingtime;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummaryStartgroup($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_startgroup;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummarySubject($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_subject;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignSummaryCenter($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->summary_for_view_center;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * TODO: PARTIALLY implemented delete-studydesign, Awaiting initial tests
     *
     * @param $studyId
     *
     * @return array|bool
     */
    public function deleteStudydesign($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->delete_studydesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}