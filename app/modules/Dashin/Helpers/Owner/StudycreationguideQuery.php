<?php

namespace Dashin\Helpers\Owner;

use Dashin\Helpers\QueryBase;

class StudycreationguideQuery extends QueryBase
{

    static string $study_guide_create_study =
        <<<'EOD'
            INSERT INTO dashin.study (name, title, description, owner_org)
            VALUES
                (:name, :title, :description, :owner_org)
            RETURNING study_id;
        EOD;

    static string $study_guide_import_datadesign =
        <<<'EOD'
            SELECT *
            FROM
              dashin.studydesign_import_design_from_data(:study_id,:dataset_id,:owner_org);
        EOD;
    static string $study_guide_validate_datadesign =
        <<<'EOD'
            SELECT *
            FROM
              dashin.studydesign_validate_data_for_import(:study_id,:dataset_id,:owner_org);
        EOD;
    static string $study_guide_add_dataset =
        <<<'EOD'
            SELECT status
                 , status_id
                 , description
            FROM
              dashin.studydata_add_dataset_to_study(:study_id , :dataset_id , :owner_org);
        EOD;

}