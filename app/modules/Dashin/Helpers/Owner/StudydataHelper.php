<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\UniqueId;
use Dashin\Helpers\HelperBase;

class StudydataHelper extends HelperBase
{
    /**
     * @var StudydataRepository
     */
    private $_repository;

    /**
     * @return StudydataRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new StudydataRepository();
        }
        return $this->_repository;
    }

    /**
     * View a dataset in an excel-like table
     * paginated both row- and columnwise
     *
     * @param string $datasetId (uuid) id for current dataset
     * @param string $studyId   (uuid) id for current study
     * @param int    $rowStart  current row-page
     * @param int    $rowLimit  number of rows per row-page
     * @param int    $colStart  current column-page
     * @param int    $colLimit  number columns per column-page
     *
     * @return array acts as DTO, sends all output to the controller
     */
    public function browseData($datasetId, $studyId, $rowStart, $rowLimit, $colStart, $colLimit)
    {
        $data = $this->_getRepository()->getDatarowsPaginated($datasetId, $studyId, $rowStart, $rowLimit, $colStart, $colLimit);
        $dataset = $this->_getRepository()->getStudydataset($datasetId, $studyId);
        $study = $this->_getRepository()->getStudyInfo($studyId);
        return [
            "data"    => $data,
            "dataset" => $dataset,
            "study"   => $study,
        ];
    }

    /**
     * Lists all datasets for the current study (paginated)
     *
     * @param int      $page    current page
     * @param int      $nrows   number of entries per page
     * @param int|bool $reset   wither to reset search form
     * @param string   $studyId (uuid) id for current study
     *
     * @return array|false acts as DTO, sends all output to the controller
     */
    public function listAddedDatasets($page, $nrows, $reset, $studyId)
    {
        $action = $this->Btn->getAction();
        $datasetId = false;
        if ($action === "select") {
            $datasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($datasetId)) {
                $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
                $this->response->redirect("/dashin/owner/studydata/attachdataset/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "nodups") {
            $this->setKeyValue("dashin_owner_list_potentialdatasets_nodups", true);
        } elseif ($action === "showall") {
            $this->removeKeyValue("dashin_owner_list_potentialdatasets_nodups");
        }
        $nodups = false;
        if ($this->getKeyValue("dashin_owner_list_potentialdatasets_nodups")) {
            $nodups = true;
        }
        $studyinfo = $this->_getRepository()->getStudyInfo($studyId);

        $searchTerm = $this->getSearchTerm("dashin_studydata_list_added_datasets_search_term", $reset);
        $data = $this->_getRepository()->getPotentialDatasetsPaginated($page, $nrows, $searchTerm, $nodups);
        if ($data) {
            foreach ($data->items as &$item) {
                if ($item->dataset_id === $datasetId) {
                    $item->confirm = true;
                } else {
                    $item->confirm = false;
                }
            }
        }

        return [
            "search_term" => $searchTerm,
            "nodups"      => $nodups,
            "data"        => $data,
            "study_info"  => $studyinfo,
        ];

    }

    /**
     * Review dataset design for adding to study
     *
     * TODO: Deprecate?
     *
     * @param $datasetId
     *
     * @return array
     */
    public function viewDatadesign($datasetId)
    {
        # dataset
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);

        # @formatter:off
        # summary
        $datadesignSummary = $this->_getRepository()->datadesignGetSummary($datasetId);
        if(!$datadesignSummary) {
            return ["dataset_info" => $datasetInfo];
        }

        # extract individual summaries from result
        $summaryType       = array_column($datadesignSummary,"summary_type");

        #--------------------------------------------------------------------------
        $designExistsKeys     = array_keys($summaryType,"design_exists");
        $countDistinctKeys    = array_keys($summaryType,"count_distinct");
        $countFullKeys        = array_keys($summaryType,"count_full");
        #--------------------------------------------------------------------------
        $designExists  = array_values(array_intersect_key($datadesignSummary,array_flip($designExistsKeys)));
        $countDistinct = array_values(array_intersect_key($datadesignSummary,array_flip($countDistinctKeys)));
        $countFull     = array_values(array_intersect_key($datadesignSummary,array_flip($countFullKeys)));
        #--------------------------------------------------------------------------
        $designExists  = $designExists[0]->n;
        $countDistinct = $countDistinct[0]->n;
        $countFull     = $countFull[0]->n;
        #--------------------------------------------------------------------------

        #--------------------------------------------------------------------------
        $countSummaryKeys     = array_keys($summaryType,"count_summary");
        #--------------------------------------------------------------------------
        $designSummary  = array_values(array_intersect_key($datadesignSummary,array_flip($countSummaryKeys)));
        #--------------------------------------------------------------------------

        #--------------------------------------------------------------------------
        $eventKeys            = array_keys($summaryType,"event");
        $subeventKeys         = array_keys($summaryType,"subevent");
        $samplingeventKeys    = array_keys($summaryType,"samplingevent");
        $samplingtimeKeys     = array_keys($summaryType,"samplingtime");
        $startgroupKeys       = array_keys($summaryType,"startgroup");
        $subjectKeys          = array_keys($summaryType,"subject");
        #--------------------------------------------------------------------------
        $event         = array_values(array_intersect_key($datadesignSummary,array_flip($eventKeys)));
        $subevent      = array_values(array_intersect_key($datadesignSummary,array_flip($subeventKeys)));
        $samplingevent = array_values(array_intersect_key($datadesignSummary,array_flip($samplingeventKeys)));
        $samplingtime  = array_values(array_intersect_key($datadesignSummary,array_flip($samplingtimeKeys)));
        $startgroup    = array_values(array_intersect_key($datadesignSummary,array_flip($startgroupKeys)));
        $subject       = array_values(array_intersect_key($datadesignSummary,array_flip($subjectKeys)));
        #--------------------------------------------------------------------------
        # @formatter:on


        #$subjectEventSequence = $this->_getRepository()->getDatadesignSubjectEventSequence($datasetId);
        #if ($subjectEventSequence) {
        #    foreach ($subjectEventSequence as &$row) {
        #        if (mb_strlen($row["event_sequence"]) > 60) {
        #            $row["event_sequence"] = mb_substr($row["event_sequence"], 0, 57) . " . . .";
        #        }
        #        $row = (object)$row;
        #    }
        #}
        #unset($row);
        #
        #$startgroupEventSequence = $this->_getRepository()->getDatadesignStartgroupEventSequence($datasetId);
        #if ($startgroupEventSequence) {
        #    foreach ($startgroupEventSequence as &$row) {
        #        if (mb_strlen($row["event_sequence"]) > 60) {
        #            $row["event_sequence"] = mb_substr($row["event_sequence"], 0, 57) . " . . .";
        #        }
        #        if (mb_strlen($row["subjects"]) > 60) {
        #            $row["subjects"] = mb_substr($row["subjects"], 0, 57) . " . . .";
        #        }
        #        $row = (object)$row;
        #    }
        #}
        #unset($row);
        #
        #    "subject_event_sequence"    => $subjectEventSequence,
        #    "startgroup_event_sequence" => $startgroupEventSequence,


        return [
            "dataset_info"   => $datasetInfo,
            "design_exists"  => $designExists,
            "count_distinct" => $countDistinct,
            "count_full"     => $countFull,
            "design_summary" => $designSummary,
            "event"          => $event,
            "subevent"       => $subevent,
            "samplingevent"  => $samplingevent,
            "samplingtime"   => $samplingtime,
            "startgroup"     => $startgroup,
            "subject"        => $subject,
        ];
    }

    /**
     * Add a dataset to current study
     *
     * @param string $studyId   (uuid) id of the current study
     * @param string $datasetId (uuid) id of the current dataset
     *
     * @return array acts as DTO, sends all output for the controller
     */
    public function addDataset($studyId, $datasetId)
    {
        $action = $this->Btn->getAction();
        $confirmImport = false;
        $confirmRemove = false;
        if ($action === "save") {

            if (UniqueId::uuidValidate($studyId) && UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_studydesign_study_import_dataset", [$studyId, $datasetId]);
                $confirmImport = true;
            } else {
                $this->removeKeyValue("owner_studydesign_study_import_dataset");
            }

        } elseif ($action === "confirm") {

            $IdsSaved = $this->getKeyValue("owner_studydesign_study_import_dataset");
            if ($IdsSaved[0] === $studyId && $IdsSaved[1] === $datasetId) {
                $this->_getRepository()->studyAddDataset($studyId, $datasetId);
            }
            $this->removeKeyValue("owner_studydesign_study_import_dataset");

        } elseif ($action === "remove") {

            if (UniqueId::uuidValidate($studyId) && UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_studydesign_study_remove_dataset", [$studyId, $datasetId]);
                $confirmRemove = true;
            } else {
                $this->removeKeyValue("owner_studydesign_study_remove_dataset");
            }

        } elseif ($action === "confirmremove") {

            $IdsSaved = $this->getKeyValue("owner_studydesign_study_remove_dataset");
            if ($IdsSaved[0] === $studyId && $IdsSaved[1] === $datasetId) {
                $this->_getRepository()->studyRemoveData($studyId, $datasetId);
            }
            $this->removeKeyValue("owner_studydesign_study_remove_dataset");

        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $datasetInfo = $this->_getRepository()->getDatasetStudyindicatorMinimal($datasetId, $studyId);

        $data = $this->_getRepository()->studydesignValidateData($studyId, $datasetId);
        if (!$data) {
            return [
                "study_info"   => $studyInfo,
                "dataset_info" => $datasetInfo,
            ];
        }

        $keys = array_column($data, "element_name");
        $extractMatch = function ($data, $keys, $keyStub) {
            # @formatter:off
            $event         = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}event"))));
            $subevent      = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}subevent"))));
            $samplingevent = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}samplingevent"))));
            $samplingtime  = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}samplingtime"))));
            $startgroup    = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}startgroup"))));
            $subject       = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}subject"))));
            $center        = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}center"))));
            # @formatter:on
            return (object)[
                "event"         => $event,
                "subevent"      => $subevent,
                "samplingevent" => $samplingevent,
                "samplingtime"  => $samplingtime,
                "startgroup"    => $startgroup,
                "subject"       => $subject,
                "center"        => $center,
            ];
        };

        $allSummary = array_values(array_intersect_key($data, array_flip(array_keys($keys, "all_summary"))));
        if (isset($allSummary[0])) {
            $allSummary = $allSummary[0];
            $designIsValid = ($allSummary->data_study_matched > 0 && $allSummary->data_unmatched === 0) ? 1 : 0;
        } else {
            $allSummary = null;
            $designIsValid = null;
        }
        return [
            "study_info"      => $studyInfo,
            "dataset_info"    => $datasetInfo,
            "data_unmatched"  => $extractMatch($data, $keys, "data_unmatched_"),
            "data_matched"    => $extractMatch($data, $keys, "data_matched_"),
            "study_unmatched" => $extractMatch($data, $keys, "study_unmatched_"),
            "study_matched"   => $extractMatch($data, $keys, "study_matched_"),
            "all_summary"     => $allSummary,
            "design_is_valid" => $designIsValid,
            "confirm_import"  => $confirmImport,
            "confirm_remove"  => $confirmRemove,
        ];
    }

}