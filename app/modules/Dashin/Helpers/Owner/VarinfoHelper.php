<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-13
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\VarDatasetEditForm;
use Dashin\Forms\Owner\UpvarDatasetEditForm;
use Dashin\Forms\Owner\VarinfoUploadForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Forms\Element\Select;

class VarinfoHelper extends HelperBase
{
    /**
     * @var VarinfoRepository
     */
    private $_repository;

    /**
     * @return VarinfoRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new VarinfoRepository();
        }
        return $this->_repository;
    }

    private function _handleUpvarDatatype($upvarDatasetId)
    {
        # set numeric datatype
        $ok = $this->_getRepository()->setUpvarDatatypeNumeric($upvarDatasetId);
        if (!$ok) {
            return false;
        }

        # convert
        # $ok = $this->_getRepository()->convertUpvarToNumeric($upvarDatasetId);
    }

    private function _handleVarVarDatatype($datasetId)
    {
        # set numeric datatype
        $ok = $this->_getRepository()->setVarVarDatatypeNumeric($datasetId);
        if (!$ok) {
            return false;
        }

        # convert
        # $ok = $this->_getRepository()->convertVarVarToNumeric($varDatasetId);
    }

    private function _downloadUpvarData($upvarDatasetId)
    {
        $data = $this->_getRepository()->getUpvarDatasetDataCsv($upvarDatasetId);
        if (isset($data["upvar_dataset"])) {
            $upvarDatasetInfo = $this->_getRepository()->getUpvarDatasetMinimal($upvarDatasetId);
            if (isset($upvarDatasetInfo["name"])) {
                $fileName = preg_replace('#[^A-Za-z0-9 ,.{}()-]+#', "_", $upvarDatasetInfo["name"]) . ".csv";
            } else {
                $fileName = "varinfo_dataset.csv";
            }

            $contents = $data["upvar_dataset"];
            $this->downloader->send($contents, $fileName, "csv");
        } else {
            $this->flashSession->warning("The variableset could not be created");
        }
    }

    /**
     * Upload variable info dataset
     *
     * @return array|bool
     */
    public function uploadUpvarDataset()
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/varinfo/listupvardatasets/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $form = new VarinfoUploadForm();
        if ($form->isRepeatPost()) {
            $this->flashSession->warning("Please select a file");
            $form->get("file_up")->clear();
        } elseif ($this->request->isPost() && !$this->request->hasFiles()) {
            # no file selected
            $this->flashSession->notice("Please select a file");
        } elseif ($this->request->hasFiles(true)) {
            # file exists - check the other form entries
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $variablesetId = $this->_handleUploadVarinfo();
                if ($variablesetId) {
                    $this->localsession->setKeyValue("dashin_owner_upvar_dataset_id", $variablesetId);
                    $this->response->redirect("/dashin/owner/varinfo/upvarreport/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }
        $form->preventRepeatPost();
        return ["form" => $form];
    }

    /**
     * Handle upload and creation of varinfo-dataset
     *
     * @return bool|mixed
     * @throws \App\Library\Utils\JsonException
     */
    private function _handleUploadVarinfo()
    {
        if (!$this->request->hasFiles()) {
            return false;
        }
        $files = $this->request->getUploadedFiles(true);
        /** @var \Phalcon\Http\Request\File $file */
        $file = $files[0];

        # move to temp folder
        $name = uniqid("fileup_");
        $moveToName = TEMPDIR_PATH . "/$name";
        if (!$file->moveTo($moveToName)) {
            return false;
        }

        # get seps
        $separators = ["c" => ",", "sc" => ";", "t" => "\t", "sp" => " "];
        $sep = $this->request->getPost("sep");
        # null: automatic
        $sep = isset($separators[$sep]) ? $separators[$sep] : null;
        # open file
        $file = new \SplFileObject($moveToName, "r");
        $sep = $this->_guessSeparator($file, $sep);
        if (!$sep) {
            return false;
        }

        # varinfo files must have a header
        $variabelsetId = $this->_getRepository()->createUploadVarinfo($file, $sep);

        return $variabelsetId;
    }

    /**
     * Report an uploaded varinfo-dataset
     *
     * @param $upvardatasetId
     *
     * @return array
     */
    public function viewUpvarDataset($upvardatasetId = null)
    {
        $action = $this->Btn->getAction();
        if ($action === "dupvname") {
            $value = $this->Btn->getValue();
            $this->setKeyValue("dashin_owner_browseupvardata_search_term", $value);
            $this->response->redirect("dashin/owner/varinfo/browseupvardata/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } else {
            $this->removeKeyValue("dashin_owner_browseupvardata_search_term");
        }
        $data = $this->_getRepository()->getUpvarDatasetForCheck($upvardatasetId);
        $data["upvar_dataset_for_view"] = $this->_getRepository()->getUpvarDatasetForView($upvardatasetId);

        if (!$data) {
            $data = false;
        }
        return $data;
    }

    public function editUpvarDataset($upvarDatasetId)
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/varinfo/upvarreport/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $form = new UpvarDatasetEditForm();
        if ($action === "submit" || $action === "apply") {
            if ($form->isValid($this->request->getPost())) {
                $params["name"] = $this->request->getPost("name");
                $params["description"] = $this->request->getPost("description");
                $ok = $this->_getRepository()->upvarDatasetUpdate($upvarDatasetId, $params);
                if ($action === "submit") {
                    $this->response->redirect("/dashin/owner/varinfo/upvarreport/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $data = $this->_getRepository()->getUpvarDatasetMinimal($upvarDatasetId);
        if ($data) {
            $form->bindValues($data);
        }
        $form->setAction("/dashin/owner/varinfo/upvaredit/");
        return [
            "form" => $form,
            "data" => $data
        ];
    }

    private function _checkData($data)
    {
        # duplicate names
        $name = array_column($data, "name");
//        $nameCount = array_count_values($name);
//        $nameCountCount = array_count_values($nameCount);
//        $af = array_filter($nameCount,function($x){return $x>1;});
        echo "";
    }

    /**
     * List all variables for a variableset
     *
     * @param $upvarDatasetId
     * @param $page
     * @param $nrows
     *
     * @return array
     */
    public function listUpvarVariables($upvarDatasetId, $page, $nrows)
    {
        $action = $this->Btn->getAction();
        if ($action === "convert") {
            $this->_handleUpvarDatatype($upvarDatasetId);
        }
        $variablesPaginated = $this->_getRepository()->getUpvarVariablesPaginated($upvarDatasetId, $page, $nrows);
        $upvardata = $this->_getRepository()->getUpvarDatasetMinimal($upvarDatasetId);
        return [
            "variables"     => $variablesPaginated,
            "upvar_dataset" => $upvardata,
        ];
    }

    /**
     * Browse the data for a varinfo-dataset
     * Browse both vertical and horizontal
     *
     * @param $upvarDatasetId
     * @param $rowStart
     * @param $rowLimit
     * @param $colStart
     * @param $colLimit
     * @param $filterIdx
     *
     * @return array|bool
     */
    public function browseUpvarData($upvarDatasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx)
    {
        $action = $this->Btn->getAction();
        if ($action === "download") {
            $this->_downloadUpvarData($upvarDatasetId);
        }
        $searchTerm = $this->getKeyValue("dashin_owner_browseupvardata_search_term");
        $columnFilter = $this->getKeyValue("dashin_owner_browseupvardata_column_filter");
        $data = $this->_getRepository()->getVarinforowsPaginated($upvarDatasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx, $searchTerm, $columnFilter);
        $data["upvar_dataset"] = $this->_getRepository()->getUpvarDatasetMinimal($upvarDatasetId);
        return $data;
    }

    /**
     * List all uploaded varinfo-datasets
     * Also allow to delete
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listUpvarDatasets($page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "view") {
            $upvarDatasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($upvarDatasetId)) {
                $this->localsession->setKeyValue("dashin_owner_var_dataset_id", $upvarDatasetId);
                $this->response->redirect("/dashin/owner/varinfo/viewupvarv/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "edit") {
            $upvarDatasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($upvarDatasetId)) {
                $this->localsession->setKeyValue("dashin_owner_var_dataset_id", $upvarDatasetId);
                $this->response->redirect("/dashin/owner/varinfo/upvaredit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "delete") {
            $upvarDatasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($upvarDatasetId)) {
                $this->setKeyValue("dashin_owner_delete_upvar_dataset", $upvarDatasetId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_upvar_dataset");
            }
        } elseif ($action === "confirm") {
            $upvarDatasetId = $this->Btn->getValue();
            $datasetIdSaved = $this->getKeyValue("dashin_owner_delete_upvar_dataset");
            if ($upvarDatasetId === $datasetIdSaved) {
                $this->_getRepository()->deleteUpvarDataset($upvarDatasetId);
            }
            $this->removeKeyValue("dashin_owner_delete_upvar_dataset");
            $activeUpvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");
            # only delete active object id if same as deleted object
            if ($activeUpvarDatasetId === $upvarDatasetId) {
                $this->localsession->removeKeyValue("dashin_owner_upvar_dataset_id");
            }
            $upvarDatasetId = false;
        } else {
            $upvarDatasetId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_upvar_datasets_search_term", $reset);
        $data = $this->_getRepository()->getUpvarDatasetsPaginated($page, $nrows, $searchTerm);
        if ($data->items) {
            foreach ($data->items as &$row) {
                if ($row->upvar_dataset_id === $upvarDatasetId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
            }
        }
        return ["data" => $data, "search_term" => $searchTerm];
    }

    public function matchVarinfo($upvarDatasetId, $datasetId)
    {
        $form = new VarDatasetEditForm();

        # check if dataset already has a meta-dataset
        $varDataset = $this->_getRepository()->getVarDatasetMinimalFromDatasetId($datasetId);
        if (!$varDataset) {
            $action = $this->Btn->getAction();
            if ($action === "match") {
                if (UniqueId::uuidValidate($upvarDatasetId) && UniqueId::uuidValidate($datasetId)) {
                    $this->view->confirm = true;
                    # save both ids
                    $this->setKeyValue("dashin_owner_varinfo_do_match", [
                        "upvar_dataset_id" => $upvarDatasetId,
                        "dataset_id"       => $datasetId,
                    ]);
                } else {
                    $this->removeKeyValue("dashin_owner_varinfo_do_match");
                }
            } elseif ($action === "confirm") {
                $matchIds = $this->getKeyValue("dashin_owner_varinfo_do_match");
                $this->removeKeyValue("dashin_owner_varinfo_do_match");
                if (isset($matchIds["upvar_dataset_id"])
                    && isset($matchIds["dataset_id"])
                    && $matchIds["upvar_dataset_id"] === $upvarDatasetId
                    && $matchIds["dataset_id"] === $datasetId) {
                    $varDatasetId = $this->_getRepository()->createVarinfo($upvarDatasetId, $datasetId);
                    # handle numeric variables

                    if ($varDatasetId) {
                        $this->localsession->setKeyValue("dashin_owner_var_dataset_id", $varDatasetId);
                        $this->response->redirect("/dashin/owner/varinfo/matchsummary/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();
                        return false;
                    }
                }
            }

            #--------------------------------------
            # get upvar-overview
            #--------------------------------------
            # varset name
            # nvars (nrows), ncols/nfeatures/nvars
            $varinfoDataset = $this->_getRepository()->getUpvarDatasetForView($upvarDatasetId);
            if ($upvarDatasetId && !$this->request->isPost()) {
                $form->bindValues($varinfoDataset);
            }

            #--------------------------------------
            # get matching-report
            #--------------------------------------
            # nvars matching
            # nvars in data not in upvar
            # nvars in upvar not in data

            $matchStats = $this->_getRepository()->getMatchStatistics($datasetId, $upvarDatasetId);
        } else {
            $varDatasetId = $varDataset["var_dataset_id"];
            $varinfoDataset = $this->_getRepository()->getVarDatasetForView($varDatasetId);
        }
        #--------------------------------------
        # get data-variables, overview
        #--------------------------------------
        # dataset name
        # nvars, nrows
        $dataset = $this->_getRepository()->getDataset($datasetId);

        return [
            "dataset"       => $dataset,
            "var_dataset"   => $varDataset,
            "upvar_dataset" => isset($varinfoDataset) ? $varinfoDataset : false,
            "match_stats"   => isset($matchStats) ? $matchStats : false,
            "form"          => $form,
        ];
    }

    public function editVarDataset($datasetId)
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/varinfo/listvdvariables/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $form = new VarDatasetEditForm();
        if ($action === "submit" || $action === "apply") {
            if ($form->isValid($this->request->getPost())) {
                $params["name"] = $this->request->getPost("name");
                $params["description"] = $this->request->getPost("description");
                $ok = $this->_getRepository()->varDatasetUpdate($datasetId, $params);
                if ($action === "submit") {
                    $this->response->redirect("/dashin/owner/varinfo/listvdvariables/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        $data = $this->_getRepository()->getVarDatasetMinimal($datasetId);
        if ($data) {
            $form->bindValues($data);
        }
        $form->setAction("/dashin/owner/varinfo/varedit/");
        return [
            "form" => $form,
            "data" => $data
        ];
    }

    public function viewVarDataset($varDatasetId)
    {
        $data = $this->_getRepository()->getVarDatasetForView($varDatasetId);
        if (!$data) {
            return false;
        }
        return [
            "var_dataset" => $data,
        ];
    }

    public function listVarVariables($datasetId, $page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "save") {
            $mapcol = $this->request->getPost("mapcol");
            if (is_array($mapcol)) {
                $del = array_intersect($mapcol, [""]);
                $add = array_diff($mapcol, [""]);
                foreach ($add as &$item) {
                    $item = (int)$item;
                }

                $numAdded = $this->_getRepository()->addSearchColumnMapping($add);
                $numRemoved = $this->_getRepository()->removeSearchColumnMapping($del);
                $msgAdd = "Added $numAdded mapping" . (($numAdded == 1) ? "" : "s") . ".";
                $msgRm = "Removed $numRemoved mapping" . (($numRemoved == 1) ? "" : "s") . ".";
                if ($numAdded + $numRemoved > 0) {
                    $this->flashSession->success("$msgAdd $msgRm");
                } else {
                    $this->flashSession->notice("$msgAdd $msgRm");
                }
            }
        }
//        elseif ($action === "convert") {
//            $this->_handleVarVarDatatype($datasetId);
//        }

        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);
        $varDatasetInfo = $this->_getRepository()->getVarDatasetMinimal($datasetId);
        $data = $this->_getRepository()->getVarVariablesPaginated($datasetId, $page, $nrows);
        if ($data) {
            $searchColumnTypes = $this->_getRepository()->getSearchColumnTypes();
            $optKeys = array_merge([""], array_column($searchColumnTypes, "search_column_type_id"));
            $optVals = array_merge(["Select"], array_column($searchColumnTypes, "name"));
            $mapColOptions = array_combine($optKeys, $optVals);

            foreach ($data->items as &$item) {
                $fld = new Select("mapcol[$item->var_variable_id]", $mapColOptions);
                $fld->setDefault($item->search_column_type_id);
                $item->mapped_column = $fld;
            }
        }
        return [
            "var_variables"    => $data,
            "var_dataset_info" => $varDatasetInfo,
            "dataset_info"     => $datasetInfo,
        ];
    }

    public function listVarDatasets($page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $varDatasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($varDatasetId)) {
                $this->setKeyValue("dashin_owner_delete_var_dataset", $varDatasetId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_var_dataset");
            }
        } elseif ($action === "confirm") {
            $varDatasetId = $this->Btn->getValue();
            $datasetIdSaved = $this->getKeyValue("dashin_owner_delete_var_dataset");
            if ($varDatasetId === $datasetIdSaved) {
                $this->_getRepository()->deleteVarDataset($varDatasetId);
            }
            $this->removeKeyValue("dashin_owner_delete_var_dataset");
            $this->localsession->removeKeyValue("dashin_owner_var_dataset_id");
            $varDatasetId = false;
        } elseif ($action === "view") {
            $varDatasetId = $this->Btn->getValue();
            $this->localsession->setKeyValue("dashin_owner_var_dataset_id", $varDatasetId);
            $this->response->redirect("/dashin/owner/varinfo/matchsummary/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } else {
            $varDatasetId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_var_datasets_search_term", $reset);
        $data = $this->_getRepository()->getVarDatasetsPaginated($page, $nrows, $searchTerm);
        if ($data->items) {
            foreach ($data->items as &$row) {
                if ($row->var_dataset_id === $varDatasetId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
            }
        }
        return ["data" => $data, "search_term" => $searchTerm];
    }

    public function browseVarData($datasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx)
    {
        $data = $this->_getRepository()->getVarDatarowsPaginated($datasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx);
        $data["var_dataset"] = $this->_getRepository()->getVarDatasetMinimal($datasetId);
        return $data;
    }

    public function mapSearchColumns($datasetId, $page, $nrows)
    {
        # return immediately if not var_dataset available
        $varDatasetInfo = $this->_getRepository()->getVarDatasetMinimal($datasetId);
        if (!$varDatasetInfo) {
            return false;
        }

        $action = $this->Btn->getAction();
        if ($action === "unselect") {
            $this->removeKeyValue("dashin_owner_map_search_column_type_id");
            $this->localsession->removeKeyValue("dashin_owner_map_var_variable_id");
        }
        if ($action === "selectvar") {
            $varVariableId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($varVariableId)) {
                $this->localsession->setKeyValue("dashin_owner_map_var_variable_id", $varVariableId);
            }
        } elseif ($action === "selectct") {
            $searchColumnTypeId = $this->Btn->getValue();
            if (ctype_digit($searchColumnTypeId)) {
                $this->setKeyValue("dashin_owner_map_search_column_type_id", $searchColumnTypeId);
            }
        }
        $searchColumnTypeId = $this->getKeyValue("dashin_owner_map_search_column_type_id");
        $varVariableId = $this->localsession->getKeyValue("dashin_owner_map_var_variable_id");

        if ($action === "unmap") {
            $varVariableIdUnmap = $this->Btn->getValue();
            $ok = $this->_getRepository()->setVarVariableColUnmap($varVariableIdUnmap);
        }
        if ($action === "map") {
            $affectedRows = $this->_getRepository()->setVarVariableColMap($varVariableId, $searchColumnTypeId);
            $this->removeKeyValue("dashin_owner_map_search_column_type_id");
            $this->localsession->removeKeyValue("dashin_owner_map_var_variable_id");
        }
        $mappedVarVariables = $this->_getRepository()->getVarVariablesMappedCols($datasetId, $varVariableId, $searchColumnTypeId);

        $varVariables = $this->_getRepository()->getVarVariablesUnmappedColsPaginated($datasetId, $varVariableId, $page, $nrows);
        $searchColumnTypes = $this->_getRepository()->getSearchColumnTypes();
        if ($searchColumnTypes) {
            foreach ($searchColumnTypes as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        return [
            "var_variables"        => $varVariables,
            "mapped_var_variables" => $mappedVarVariables,
            "search_column_types"  => $searchColumnTypes,
            "var_dataset_info"     => $varDatasetInfo,
        ];
    }
}