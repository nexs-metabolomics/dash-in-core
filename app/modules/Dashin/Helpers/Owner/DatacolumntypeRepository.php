<?php


namespace Dashin\Helpers\Owner;


use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class DatacolumntypeRepository extends RepositoryBase
{

    public function getDatacolumntype($datacolumntypeId)
    {
        if(!UniqueId::uuidValidate($datacolumntypeId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->datacolumntype->get_datacolumntype;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "datacolumntype_id" => $datacolumntypeId,
            ]);

            if ($result) {
                return $result;
            }


        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function createDatacolumntype($inData)
    {
        try {
            $sql = $this->dashin_config->sql->datacolumntype->create_datacolumntype;
            
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $inData["shortname"],
                "name"        => $inData["name"],
                "description" => $inData["description"],
                "owner_org" => $organizationId,
            ]);

            if (isset($result["datacolumntype_id"])) {
                return $result["datacolumntype_id"];
            }


        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function updateDatacolumntype($datacolumntypeId, $inData)
    {
        if(!UniqueId::uuidValidate($datacolumntypeId)) {
            return false;
        }
        
        try {
            
            $sql = $this->dashin_config->sql->datacolumntype->update_datacolumntype;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "datacolumntype_id" => $datacolumntypeId,
                "shortname"        => $inData["shortname"],
                "name"        => $inData["name"],
                "description" => $inData["description"],
                "owner_org" => $organizationId,
            ]);
            
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function deleteDatacolumntype($datacolumntypeId)
    {
        if(!UniqueId::uuidValidate($datacolumntypeId)) {
            return false;
        }
        
        try {
            $sql = $this->dashin_config->sql->datacolumntype->delete_datacolumntype;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "datacolumntype_id" => $datacolumntypeId,
                "owner_org" => $organizationId,
            ]);
            
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getDatacolumntypesPaginated($page, $nrows, $searchTerm)
    {
        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_search_count;
            $itemQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_count;
            $itemQuery = $this->dashin_config->sql->datacolumntype->get_datacolumntype_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }
}