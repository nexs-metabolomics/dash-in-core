<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Json;
use App\Library\Utils\JsonException;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class StudyRepository extends RepositoryBase
{

    public function getCountriesMinimal()
    {

        try {
            $sql = $this->dashin_config->sql->owner_study->get_countries_minimal;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getConsortiumsMinimal()
    {

        try {
            $sql = $this->dashin_config->sql->owner_study->get_consortiums_minimal;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getResearchdesignsMinimal()
    {

        try {
            $sql = $this->dashin_config->sql->owner_study->get_researchdesigns_minimal;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudyRoles()
    {

        try {
//            $sql = $this->dashin_config->sql->owner_study->get_selected_persons;
            $sql = "SELECT study_role_id,name FROM dashin.study_role";

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function createStudy($inData)
    {

        try {
            $sql = $this->dashin_config->sql->owner_study->create_study;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $inData["name"],
                "title"       => $inData["title"],
                "description" => $inData["description"],
                "owner_org"   => $organizationId,
            ]);

            if (isset($result["study_id"])) {
                return $result["study_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function deleteStudy($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->delete_study;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function updateGeneral($studyId, $inData)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->update_general;

            $organizationId = $this->SU->getActiveManagerOrgId();

            try {
                $startDate = new \DateTime($inData["start_date"], new \DateTimeZone("UTC"));
                $startDateTimestamp = $startDate->format(\DateTime::ATOM);
            } catch (\Exception $e) {
                $startDateTimestamp = null;
                error_log($e->getMessage());
            }

            $countryId = (isset($inData["country"]) && (int)$inData["country"]) ? $inData["country"] : null;
            $consortiumId = (isset($inData["consortium"]) && UniqueId::uuidValidate($inData["consortium"])) ? $inData["consortium"] : null;

            $state = $this->db->execute($sql, [
                "study_id"             => $studyId,
                "owner_org"            => $organizationId,
                "name"                 => $inData["name"],
                "title"                => $inData["title"],
                "description"          => $inData["description"],
                "start_date"           => $startDateTimestamp,
                "endpoint"             => $inData["endpoint"],
                "objectives"           => $inData["objectives"],
                "conclusion"           => $inData["conclusion"],
                "exclusion"            => $inData["exclusion"],
                "inclusion"            => $inData["inclusion"],
                "institute"            => $inData["institute"],
                "country_id"           => $countryId,
                "consortium_id"        => $consortiumId,
                "published"            => $inData["published"],
                "researchdesign_id"    => strlen($inData["researchdesign"]) ? (int)$inData["researchdesign"] : null,
                "num_treat"            => $inData["numtreat"],
                "num_factor"           => strlen($inData["numfactor"]) ? (int)$inData["numfactor"] : null,
                "num_arm"              => strlen($inData["numarm"]) ? (int)$inData["numarm"] : null,
                "researchdesign_text"  => $inData["researchdesign_text"],
                "num_volun"            => $inData["numvolun"],
                "num_volun_terminated" => $inData["numvolun_term"],
                "recruit_start_year"   => strlen($inData["recruit_start_year"]) ? (int)$inData["recruit_start_year"] : null,
                "recruit_end_year"     => strlen($inData["recruit_end_year"]) ? (int)$inData["recruit_end_year"] : null,
                "blinding"             => isset($inData["blinding"]) ? 1 : 0,
                "blinding_method"      => $inData["blinding_method"],
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getGeneralForForm($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->get_general_for_form;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Gets permissions for the study and all dependent objects
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyPermissinos($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = StudyQuery::$get_study_and_dependents_publc_status;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * Gets the status of whether the study is public or private
     *
     * @param $studyId
     *
     * @return false|mixed
     */
    public function getStudyPermissionStatus($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = StudyQuery::$get_study_and_dependents_publc_status;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                $result["datasets"] = Json::jsonDecode($result["datasets"]);
                $result["supportfiles"] = Json::jsonDecode($result["supportfiles"]);

                return $result;
            }

        } catch (\PDOException|JsonException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    public function setStudyPublicStatus($studyId, $status)
    {

        try {
            if ($status === "on") {
                $sql = StudyQuery::$set_study_public_status_on;
            } elseif ($status === "off") {
                $sql = StudyQuery::$set_study_public_status_off;
            }
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function setDatasetPublicStatus($datasetId, $studyId, $status)
    {

        try {
            if ($status === "on") {
                $sql = StudyQuery::$set_dataset_public_status_on;
            } elseif ($status === "off") {
                $sql = StudyQuery::$set_dataset_public_status_off;
            }
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "study_id"   => $studyId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function setSupportfilePublicStatus($supportfileId, $studyId, $status)
    {

        try {
            if ($status === "on") {
                $sql = StudyQuery::$set_supportfile_public_status_on;
            } elseif ($status === "off") {
                $sql = StudyQuery::$set_supportfile_public_status_off;
            }
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "supportfile_id"  => $supportfileId,
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudiesPaginated($page, $nrows, $searchTerm)
    {

        $organizationId = $this->SU->getActiveManagerOrgId();
        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_study->studies_paginated_search_count;
            $itemQuery = $this->dashin_config->sql->owner_study->studies_paginated_search;
            $queryParams["search_term"] = $searchTerm;
            $queryParams["owner_org"] = $organizationId;
        } else {
            $countQuery = $this->dashin_config->sql->owner_study->studies_paginated_count;
            $itemQuery = $this->dashin_config->sql->owner_study->studies_paginated;
            $queryParams["owner_org"] = $organizationId;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    public function getGeneralForView($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->study_summary_general;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudyDesignForView($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->get_researchdesign_for_view;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudyStructureForDownload($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_study->get_study_structure_for_download;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function verifyStudyDatasetStructure($studyId, $datasetId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->verify_study_dataset_structure;

            $result = $this->db->fetchOne($sql);
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudyDatasetsForView($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_study->get_study_datasets_for_view;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function removeDatasetFromStudy($datasetId, $studyId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydata->remove_dataset_from_study;

            $state = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "study_id"   => $studyId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getAddedDatasetsPaginated($page, $nrows, $searchTerm, $studyId)
    {

        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studydata->added_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studydata->added_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studydata->added_datasets_count;
            $itemQuery = $this->dashin_config->sql->owner_studydata->added_datasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    public function deleteStudycontact($studyId, $studycontactId)
    {

        try {
            $sql = $this->dashin_config->sql->owner_study->delete_studycontact;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"        => $studyId,
                "studycontact_id" => $studycontactId,
                "owner_org"       => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudycontacts($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_study->list_studycontacts;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudycontact($studyId, $studycontactId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studycontactId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_study->get_studycontact;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"        => $studyId,
                "studycontact_id" => $studycontactId,
                "owner_org"       => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function createStudycontact($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = $this->dashin_config->sql->owner_study->create_studycontact;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
                "first_name"    => $this->request->getPost("first_name"),
                "last_name"     => $this->request->getPost("last_name"),
                "email"         => $this->request->getPost("email"),
                "description"   => $this->request->getPost("description"),
                "study_role_id" => $this->request->getPost("study_role"),
            ]);

            if (isset($result["studycontact_id"])) {
                return $result["studycontact_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function updateStudycontact($studycontactId, $studyId)
    {

        if (!UniqueId::uuidValidate($studycontactId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = $this->dashin_config->sql->owner_study->update_studycontact;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studycontact_id" => $studycontactId,
                "study_id"        => $studyId,
                "owner_org"       => $organizationId,
                "first_name"      => $this->request->getPost("first_name"),
                "last_name"       => $this->request->getPost("last_name"),
                "email"           => $this->request->getPost("email"),
                "description"     => $this->request->getPost("description"),
                "study_role_id"   => $this->request->getPost("study_role"),
            ]);

            if (isset($result["studycontact_id"])) {
                return $result["studycontact_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getStudySupportFiles($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->get_;

            $result = $this->db->fetchOne($sql);

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}
