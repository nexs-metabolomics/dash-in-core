<?php

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Csv;
use App\Library\Utils\Json;
use App\Library\Utils\StudyExporterOld;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\StudyexportsetForm;
use Dashin\Forms\Owner\StudyrowsetForm;
use Dashin\Forms\Owner\StudyvariableconditionForm;
use Dashin\Forms\Owner\StudyvariablesetForm;
use Dashin\Helpers\HelperBase;

class StudyexportHelper extends HelperBase
{

    /**
     * @var StudyexportRepository
     */
    private $_repository;

    /**
     * @return StudyexportRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new StudyexportRepository();
        }

        return $this->_repository;
    }

    /**
     * Create a new condition for a single studyvariable
     * select variable from the (paginated) list of
     * variables in the study
     *
     *
     * @param int      $page    current page
     * @param int      $nrows   number entries per page
     * @param int|bool $reset   whether to reset the search form
     * @param string   $studyId (uuid) id of current study
     *
     * @return array|false send all aoutputs to the conttoller (acts as DTO)
     */
    public function createStudyvariablecondition($page, $nrows, $reset, $studyId)
    {

        $action = $this->Btn->getAction();
        if ($action === "create") {
            $studyvariableId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyvariableId)) {
                $studyvariableconditionId = $this->_getRepository()->createStudyvariablecondition($studyvariableId, $studyId);
                if (UniqueId::uuidValidate($studyvariableconditionId)) {
                    $this->localsession->setKeyValue("dashin_owner_studyvariablecondition_id", $studyvariableconditionId);
                    $this->response->redirect("/dashin/owner/studyexport/editcondition/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        $searchTerm = $this->getSearchTerm("dashin_studyexport_list_studyvarconditions_search_term", $reset);
        $studyvarcondsPaginated = $this->_getRepository()->getStudyvariablesPaginated($page, $nrows, $searchTerm);
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_info"             => $studyInfo,
            "studyvarcond_paginated" => $studyvarcondsPaginated,
        ];
    }

    /**
     * List studyvariable conditions (paginated)
     *
     * @param int      $page    current page
     * @param int      $nrows   number entries per page
     * @param int|bool $reset   whether to reset the search form
     * @param string   $studyId (uuid) id of current study
     *
     * @return array|false send all aoutputs to the conttoller (acts as DTO)
     */
    public function listStudyvariableconditions($page, $nrows, $reset, $studyId)
    {

        $studyvariableconditionId = null;

        $action = $this->Btn->getAction();

        if ($this->SU->allowWrite()) {

            if ($action === "edit") {
                $studyvariableconditionId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($studyvariableconditionId)) {
                    $this->localsession->setKeyValue("dashin_owner_studyvariablecondition_id", $studyvariableconditionId);
                    $this->response->redirect("/dashin/owner/studyexport/editcondition/{$this->localsession->getQuerystring('?')}");
                }
            } elseif ($action === "delete") {
                $studyvariableconditionId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($studyvariableconditionId)) {
                    $this->setKeyValue("dashin_owner_studyvariablecondition_delete", $studyvariableconditionId);
                } else {
                    $this->removeKeyValue("dashin_owner_studyvariablecondition_delete");
                }
            } elseif ($action == "confirm") {
                $studyvariableconditionId = $this->Btn->getValue();
                $studyvariableconditionIdSaved = $this->getKeyValue("dashin_owner_studyvariablecondition_delete");
                if ($studyvariableconditionId === $studyvariableconditionIdSaved) {
                    $ok = $this->_getRepository()->deleteStudyvariablecondition($studyvariableconditionId, $studyId);
                }
            }

        }

        $searchTerm = $this->getSearchTerm("dashin_studyexport_list_studyvarconditions_search_term", $reset);
        
        # allowed number of categories to show in output (subject to further shortening of output string)
        $catMaxnum = 50;
        $catstringLength = 50;
        
        # allowed variable name length in output
        $nameLength = 20;
        $studyvarcondsPaginated = $this->_getRepository()->getStudyvariableconditionsPaginated($page, $nrows, $searchTerm, $studyId, $nameLength, $catMaxnum, $catstringLength);
        
        # invalidate result if no items
        if (!($studyvarcondsPaginated && count($studyvarcondsPaginated->items) > 0)) {
            $studyvarcondsPaginated = false;
        } else {
            foreach ($studyvarcondsPaginated->items as &$row) {
                $row->confirm = ($row->studyvariablecondition_id === $studyvariableconditionId);
                $row = (object)$row;
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_info"             => $studyInfo,
            "studyvarcond_paginated" => $studyvarcondsPaginated,
        ];
    }

    /**
     * Handle input for edit studdyvariable condition
     * parse form input and update condition (or reset if requested)
     *
     * @param string $studyvariableconditionId (uuid) id of current studyvariable condition
     * @param string $studyId                  (uuid) id for current study
     *
     * @return void
     */
    private function _handleConditionInput($studyvariableconditionId, $studyId)
    {

        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply" || $action === "addcategory" || $action === "rmcategory") {
            $operatorId = (int)$this->request->getPost("conditionoperator");
            if ($operatorId === 0) {
                # update only name description
                $this->flashSession->notice("You did not select an operator - using default. To reset condtion use 'Reset'");
            }

            $inData["conditionoperator_id"] = $operatorId;
            $inData["value_a"] = (string)$this->request->getPost("value_a");
            $inData["value_b"] = (string)$this->request->getPost("value_b");
            $inData["name"] = (string)$this->request->getPost("conditionname");
            $inData["description"] = (string)$this->request->getPost("description");
            $ok = $this->_getRepository()->updateStudyvariablcondition($studyvariableconditionId, $studyId, $inData);
        } elseif ($action === "reset") {
            $inData["conditionoperator_id"] = 0;
            $inData["value_a"] = "";
            $inData["value_b"] = "";
            $ok = $this->_getRepository()->updateStudyvariablconditionReset($studyvariableconditionId, $studyId, $inData);
            $ok = $this->_getRepository()->removeAllStudyvariableconditionCategories($studyvariableconditionId);
        }
    }

    /**
     * Prepare parts of the ouput
     * in particular:
     *      prepare the form and set the flag for
     *      which scenario will be used in the view
     *
     * @param StudyvariableconditionForm $form
     * @param string                     $studyvariableconditionId (uuid) id of current studyvariable condition
     * @param string                     $studyId                  (uuid) id of current study
     * @param array                      $studyvariableTypeInfo    various info on the studyvariable in the condition
     *
     * @return array|false
     */
    private function _handleConditionOutput(StudyvariableconditionForm &$form, $studyvariableconditionId, $studyId, $studyvariableTypeInfo)
    {

        $studyvariablecondition = $this->_getRepository()->getStudyvariablecondition($studyvariableconditionId, $studyId);
//        $form->clear("conditionoperator");
//        $form->get("conditionoperator")->setAttribute("value",$studyvariablecondition["conditionoperator_id"]);
        $form->get("conditionoperator")->setDefault($studyvariablecondition["conditionoperator_id"]);
        $form->get("value_a")->setAttribute("value", $studyvariablecondition["value_a"]);
        $form->get("value_b")->setAttribute("value", $studyvariablecondition["value_b"]);
        $form->get("conditionname")->setAttribute("value", $studyvariablecondition["name"]);
        $form->get("description")->setAttribute("value", $studyvariablecondition["description"]);

        if (!$studyvariablecondition["conditionoperator_id"]) {

            # undefined, null, 0
            $this->view->condition_scenario = 0;

        } elseif (in_array($studyvariablecondition["conditionoperator_id"], [1, 2, 3, 4, 5, 6])) {

            # numeric single condition operator
            $this->view->condition_scenario = 1;

        } elseif (in_array($studyvariablecondition["conditionoperator_id"], [7, 8, 9, 10])) {

            # numeric multiple condition
            $this->view->condition_scenario = 2;

        } elseif (in_array($studyvariablecondition["conditionoperator_id"], [11, 12])) {

            # categorical condition
            if ($studyvariableTypeInfo["variabletype_id"] === 7) {

                # samplingevent category selected -> condition_scenario 4
                # element2_id is set only when samplingevent category is selected
                $this->view->condition_scenario = 4;

            } else {

                # all other categorical scenarios
                $this->view->condition_scenario = 3;
            }
        }

        return $studyvariablecondition;
    }

    /**
     * Estimante number of decimals in the summaries
     * in the 'edit studyvariable condition' menu
     *
     * @param string $value numeric value as string
     *
     * @return int|mixed
     */
    private function _estimateNumberOfDecimals($value)
    {

        preg_match('#[1-9]#', preg_replace('#\.#', '', $value), $matchnz, PREG_OFFSET_CAPTURE);
        $posnz = 0;
        if (isset($matchnz[0][1])) {
            $posnz = $matchnz[0][1];
        }
        preg_match('#\.#', $value, $matchdot, PREG_OFFSET_CAPTURE);
        $posdot = strpos($value, ".");
        $ndec = 0;
        if ($posdot !== false) {
            $ndec = strlen(substr($value, $posdot));
        }
        if ($posnz > 0 && $ndec > 0) {
            return $posnz + 2;
        }

        return 0;
    }

    /**
     * Edit the specific parameters of the studyvariavble condition
     *
     * @param string $studyvariableconditionId (uuid) id of the current studyvariablecondition
     * @param string $studyId                  (uuid) id for current study
     *
     * @return array
     */
    public function editStudyvariablecondition($studyvariableconditionId, $studyId)
    {

        $this->_handleConditionInput($studyvariableconditionId, $studyId);
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            $this->response->redirect("/dashin/owner/studyexport/listconditions/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }
        #--------------------------------------------------------------
        # object-info
        #--------------------------------------------------------------
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        if ($action === "addcategory") {
            $inputValue = $this->Btn->getValue();
            $elementId = key($inputValue);
            $value = array_values($inputValue)[0];
            $ok = $this->_getRepository()->addStudyvariableconditionCategory($studyvariableconditionId, $elementId, $value);
        } elseif ($action === "rmcategory") {
            $inputValue = $this->Btn->getValue();
            $elementId = key($inputValue);
            $value = array_values($inputValue)[0];
            $this->_getRepository()->removeStudyvariableconditionCategory($studyvariableconditionId, $elementId, $value);
        }

        #--------------------------------------------------------------
        # datatype and variabletype
        #--------------------------------------------------------------
        $limit = 10;
        $studyvariableTypeInfo = $this->_getRepository()->studyvariableGetTypeInfo($studyvariableconditionId, $studyId, $limit);

        #--------------------------------------------------------------
        # initialize summaries
        #--------------------------------------------------------------
        $studyvarSummaryNumeric = null;
        $studyvarSummaryHistogram = null;
        $studyvarSummaryCategorical = null;
        $studyvarSelectedMaincategories = null;
        $studyvarSelectedSubcategories = null;
        if (isset($studyvariableTypeInfo["datatype_id"]) && $studyvariableTypeInfo["datatype_id"] === 2) {
            #--------------------------------------------------------------
            # numeric
            #--------------------------------------------------------------
            # datatypeFlag: used to filter operaters for select-input
            # -> only show operators that are valid for datatype
            $datatypeFlag = 2;

            #--------------------------------------------------------------
            # numeric summary
            #--------------------------------------------------------------
            $studyvarSummaryNumeric = $this->_getRepository()->getStudyvariableSummaryNumeric($studyvariableconditionId, $studyId);
            if (isset($studyvarSummaryNumeric["iqr"]) && $studyvarSummaryNumeric["iqr"] > 0) {
                $breaksize = $studyvarSummaryNumeric["iqr"] / 5;

                #--------------------------------------------------------------
                # histogram
                #--------------------------------------------------------------
                $studyvarSummaryHistogram = $this->_getRepository()->getStudyvariableSummaryHistogram($studyvariableconditionId, $studyId, $breaksize);
                if ($studyvarSummaryHistogram) {
                    $value = $studyvarSummaryHistogram[0]["bin_upper"];
                    $newndec = $this->_estimateNumberOfDecimals($value);

                    if ($newndec > 0) {
                        foreach ($studyvarSummaryHistogram as &$row) {
                            $row["bin_lower"] = number_format($row["bin_lower"], $newndec, ".", "");
                            $row["bin_mid"] = number_format($row["bin_mid"], $newndec);
                            $row["bin_upper"] = number_format($row["bin_upper"], $newndec);
                            $row = (object)$row;
                        }
                        foreach ($studyvarSummaryNumeric as $k => &$item) {
                            if (!($k === "n" || $k === "filtered_n")) {
                                $item = number_format($item, $newndec, ".", "");
                            }
                        }
                    } else {
                        foreach ($studyvarSummaryHistogram as &$row) {
                            $row = (object)$row;
                        }
                    }
                    unset($row);
                }
            }
        } else {
            #--------------------------------------------------------------
            # categorical
            #--------------------------------------------------------------
            $datatypeFlag = 1;

            #--------------------------------------------------------------
            # summary
            #--------------------------------------------------------------
            $studyvarSummaryCategorical = $this->_getRepository()->getStudyvariableSummaryCategorical($studyvariableconditionId, $studyId);
            if ($studyvarSummaryCategorical) {
                foreach ($studyvarSummaryCategorical as &$row) {
                    $row = (object)$row;
                }
                unset($row);
            }
            # differentiate between main categories and subcategories
            $studyvarSelectedCategories = $this->_getRepository()->getStudyvariableconditionSelectedCategories($studyvariableconditionId);

            if ($studyvarSelectedCategories) {
                $categorylevel = array_column($studyvarSelectedCategories, "categorylevel");
                $mainkeys = array_keys($categorylevel, "maincategory");
                $subkeys = array_keys($categorylevel, "subcategory");

                $studyvarSelectedMaincategories = array_intersect_key($studyvarSelectedCategories, array_flip($mainkeys));
                $studyvarSelectedSubcategories = array_intersect_key($studyvarSelectedCategories, array_flip($subkeys));
                if ($studyvarSelectedMaincategories) {
                    foreach ($studyvarSelectedMaincategories as &$row) {
                        $row = (object)$row;
                    }
                    unset($row);
                }
                if ($studyvarSelectedSubcategories) {
                    foreach ($studyvarSelectedSubcategories as &$row) {
                        $row = (object)$row;
                    }
                    unset($row);
                }
            }
        }

        #--------------------------------------------------------------
        # get list of operators
        #--------------------------------------------------------------
        $conditionoperators = $this->_getRepository()->getConditionoperators($datatypeFlag);
        $operatorIds = array_column($conditionoperators, "conditionoperator_id");
        $operatorSymbols = array_column($conditionoperators, "symbol");
        $operatorOptions = array_combine($operatorIds, $operatorSymbols);
        $form = new StudyvariableconditionForm(["conditionoperators" => $operatorOptions]);

        $studyvariablecondition = $this->_handleConditionOutput($form, $studyvariableconditionId, $studyId, $studyvariableTypeInfo);

        return [
            "study_info"                            => $studyInfo,
            "studyvariablecondition_info"           => $studyvariablecondition,
            "studyvariable_summary_categorical"     => $studyvarSummaryCategorical,
            "studyvariable_selected_maincategories" => $studyvarSelectedMaincategories,
            "studyvariable_selected_subcategories"  => $studyvarSelectedSubcategories,
            "studyvariable_summary_numeric"         => $studyvarSummaryNumeric,
            "studyvariable_summary_histogram"       => $studyvarSummaryHistogram,
            "form"                                  => $form,
        ];
    }

    public function createStudyrowset($studyId)
    {

        $form = new StudyrowsetForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $studyrowsetId = $this->_getRepository()->createStudyrowset($studyId);
                if ($studyrowsetId) {
                    $this->localsession->setKeyValue("dashin_owner_studyrowset_id", $studyrowsetId);
                    $querystring = $this->localsession->getQuerystring("?");
                    $this->response->redirect("/dashin/owner/studyexport/editstudyrowset/$querystring");
                    $this->response->send();

                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    public function editStudyrowset($studyrowsetId, $studyId, $studyrowsubsetId)
    {

        $subsetnames = $this->request->getPost("subsetname");
        $action = $this->Btn->getAction();
        if ($action === "subsetapply") {
            $studyrowsubsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyrowsubsetId)) {
                $subsetname = substr($subsetnames[$studyrowsubsetId], 0, 200);
                $this->_getRepository()->updateStudyrowsubsetNames($studyrowsubsetId, $studyrowsetId, $studyId, $subsetname);
            }
        }
        $form = new StudyrowsetForm();
        if ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $ok = $this->_getRepository()->updateStudyrowset($studyrowsetId, $studyId);
                }
            }
        } elseif ($action === "addsubset") {
            $studyrowsubsetId = $this->_getRepository()->createStudyrowsubset($studyrowsetId, $studyId);
            if ($studyrowsubsetId) {
                $this->localsession->setKeyValue("dashin_owner_studyrowsubset_id", $studyrowsubsetId);
            }
        } elseif ($action === "deletesubset") {
            $deleteStudyrowsubsetId = $this->Btn->getValue();
            $ok = $this->_getRepository()->deleteStudyrowsubset($deleteStudyrowsubsetId, $studyId);
            if ($studyrowsubsetId === $deleteStudyrowsubsetId) {
                $this->localsession->removeKeyValue("dashin_owner_studyrowsubset_id");
                $studyrowsubsetId = false;
            }
        } elseif ($action === "setactive") {
            $tempStudyrowsubsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyrowsubsetId)) {
                $studyrowsubsetId = $tempStudyrowsubsetId;
                $this->localsession->setKeyValue("dashin_owner_studyrowsubset_id", $studyrowsubsetId);
            }
        } elseif ($action === "addcondition") {
            $activeStudyrowsubsetId = $this->request->getPost("activesubset");
            if (UniqueId::uuidValidate($activeStudyrowsubsetId)) {
                $studyvariableconditionId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($studyvariableconditionId)) {
                    $ok = $this->_getRepository()->addStudyvariableconditionToSubset($activeStudyrowsubsetId, $studyvariableconditionId, $studyId);
                }
            }
        } elseif ($action === "rmcondition") {
            $rmstr = $this->Btn->getValue();
            parse_str($rmstr, $rmparsed);
            $rmids = explode(",", key($rmparsed["cond"]));
            $rmStudyrowsubsetId = $rmids[0];
            $rmStudyvariableconditionId = $rmids[1];
            $ok = $this->_getRepository()->removeStudyvariableconditionFromSubset($rmStudyrowsubsetId, $rmStudyvariableconditionId, $studyId);
        } elseif ($action === "editcondition") {
            $editStudyvariableconditionId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($editStudyvariableconditionId)) {
                $this->localsession->setKeyValue("dashin_owner_studyvariablecondition_id", $editStudyvariableconditionId);
                $this->response->redirect("/dashin/owner/studyexport/editcondition/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studyrowsetInfo = $this->_getRepository()->getStudyrowsetInfo($studyrowsetId, $studyId);
        if ($studyrowsetInfo) {
            $form->bindValues($studyrowsetInfo);
            $studyrowCount = $this->_getRepository()->studyrowsetStudyrowCount($studyrowsetId, $studyId);
            $studyrowsetInfo["studyrow_count"] = $studyrowCount;
        }

        $studyrowsubsets = $this->_getRepository()->getStudyrowsubsetByStudyrowset($studyrowsetId, $studyId);
        if ($studyrowsubsets) {
            $studyrowsubsetId = $this->localsession->getKeyValue("dashin_owner_studyrowsubset_id");
            if (!$studyrowsubsetId) {
                $studyrowsubsetId = $studyrowsubsets[0]["studyrowsubset_id"];
                if ($studyrowsubsetId) {
                    $this->localsession->setKeyValue("dashin_owner_studyrowsubset_id", $studyrowsubsetId);
                }
            }
            foreach ($studyrowsubsets as &$row) {
                $row["active"] = ($row["studyrowsubset_id"] === $studyrowsubsetId);
                $row["conditions"] = Json::jsonDecode($row["conditions"]);
                $row = (object)$row;
            }
            unset($row);
        }

        $studyvariableconditions = $this->_getRepository()->getStudyvariableconditionsForSubset($studyId);

        return [
            "form"                    => $form,
            "studyrowset_info"        => $studyrowsetInfo,
            "studyrowsubsets"         => $studyrowsubsets,
            "studyvariableconditions" => $studyvariableconditions,
            "study_info"              => $studyInfo,
        ];
    }

    /**
     * @param $studyId
     * @param $page
     * @param $nrow
     * @param $reset
     *
     * @return array|false
     */
    public function listStudyrowsets($studyId, $page, $nrow, $reset)
    {

        $studyrowsetId = false;

        $action = $this->Btn->getAction();

        if ($this->SU->allowWrite()) {

            if ($action === "edit") {
                $studyrowsetId = $this->Btn->getValue();
                if (UniqueId::uuidValidate(($studyrowsetId))) {
                    $this->localsession->setKeyValue("dashin_owner_studyrowset_id", $studyrowsetId);
                    $this->response->redirect("/dashin/owner/studyexport/editstudyrowset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            } elseif ($action === "delete") {
                $studyrowsetId = $this->Btn->getValue();
                if (UniqueId::uuidValidate(($studyrowsetId))) {
                    $this->localsession->setKeyValue("dashin_owner_studyrowset_delete", $studyrowsetId);
                } else {
                    $this->localsession->removeKeyValue("dashin_owner_studyrowset_delete");
                }
            } elseif ($action === "confirm") {
                $studyrowsetId = $this->Btn->getValue();
                $studyrowsetIdSaved = $this->localsession->getKeyValue("dashin_owner_studyrowset_delete");
                $this->localsession->removeKeyValue("dashin_owner_studyrowset_delete");
                if ($studyrowsetId === $studyrowsetIdSaved) {
                    $ok = $this->_getRepository()->deleteStudyrowset($studyrowsetId, $studyId);
                }
            }

        }

        $searchTerm = $this->getSearchTerm("dashin_studyexport_list_studyrowsets_search_term", $reset);
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studyrowsets = $this->_getRepository()->getStudyrowsetsPaginated($page, $nrow, $searchTerm);
        if ($studyrowsets) {
            foreach ($studyrowsets->items as &$row) {
                $row->confirm = ($row->studyrowset_id === $studyrowsetId);
            }
            unset($row);
        }

        return [
            "studyrowsets_paginated" => $studyrowsets,
            "study_info"             => $studyInfo,
        ];
    }

    public function createStudyvariableset($studyId)
    {

        $form = new StudyvariablesetForm();
//        $action = $this->Btn->getAction();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $studyvariablesetId = $this->_getRepository()->createStudyvariableset($studyId);
                if ($studyvariablesetId) {
                    $this->localsession->setKeyValue("dashin_owner_studyvariableset_id", $studyvariablesetId);
                    $this->response->redirect("/dashin/owner/studyexport/editstudyvarset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                }
            }
        }
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_info" => $studyInfo,
            "form"       => $form,
        ];
    }

    public function editStudyvariableset($page1, $page2, $nrows, $reset1, $reset2, $studyId, $studyvariablesetId)
    {

        $action = $this->Btn->getAction();
        if ($action === "addvariable") {
            if (!UniqueId::uuidValidate($studyvariablesetId)) {
                $this->flashSession->warning("You need to select a studyvariableset to add variables");
            }
            $studyvariableId = $this->Btn->getValue();
            $ok = $this->_getRepository()->addSingleStudyvariableToStudyvarset($studyvariableId, $studyvariablesetId, $studyId);
            if ($ok) {
                $this->flashSession->success("Variable successfully added");
            }
        } elseif ($action === "addallonpage") {
            $this->_getRepository()->addMultipleStudyvariableToStudyvarset($studyvariablesetId, $studyId);
        } elseif ($action === "addfiltered") {
            $searchTerm1 = $this->getSearchTerm("dashin_studyexport_edit_datavarset_select_search_term", $reset1, "search_term1");
            if ($searchTerm1) {
                $this->_getRepository()->addFilteredStudyvariableToStudyvarset($studyvariablesetId, $studyId, $searchTerm1);
            }
        } elseif ($action === "addall") {
            $this->_getRepository()->addAllStudyvariableToStudyvarset($studyvariablesetId, $studyId);
        } elseif ($action === "removevariable") {
            $studyvariableId = $this->Btn->getValue();
            $ok = $this->_getRepository()->removeStudyvariableFromStudyvarset($studyvariableId, $studyId, $studyvariablesetId);
            if ($ok) {
                $this->flashSession->success("Variable successfully removed");
            }
        } elseif ($action === "removeallonpage") {
            $this->_getRepository()->removeMultipleStudyvariableFromStudyvarset($studyvariablesetId, $studyId);
        } elseif ($action === "removefiltered") {
            $searchTerm2 = $this->getSearchTerm("dashin_studyexport_edit_datavarset_unselect_search_term", $reset2, "search_term2");
            if ($searchTerm2) {
                $this->_getRepository()->removeFilteredStudyvariableFromStudyvarset($studyvariablesetId, $studyId, $searchTerm2);
            }
        } elseif ($action === "removeall") {
            $this->_getRepository()->removeAllStudyvariableFromStudyvarset($studyvariablesetId, $studyId);
        }

        # validate that studyrowset belongs to current study (or remove from session)
        $hasStudyvarset = $this->_getRepository()->studyHasStudyvarset($studyId, $studyvariablesetId);
        if (!$hasStudyvarset) {
            $this->localsession->removeKeyValue("dashin_owner_studyvariableset_id");
            $studyvariablesetId = null;
        }

        $searchTerm1 = $this->getSearchTerm("dashin_studyexport_edit_datavarset_select_search_term", $reset1, "search_term1");
        $unselectedStudyvariablesPaginated = $this->_getRepository()->getStudyvarsetUnselectedVarsPaginated($page1, $nrows, $searchTerm1);

        # ensure correct page-count in ui
        if ($unselectedStudyvariablesPaginated->last < $page1) {
            $page1 = $unselectedStudyvariablesPaginated->last;
            $this->localsession->setKeyValue("dashin_owner_editstudyvarsetvars_page1", $page1);
        }

        $searchTerm2 = $this->getSearchTerm("dashin_studyexport_edit_datavarset_unselect_search_term", $reset2, "search_term2");
        $selectedStudyvariablesPaginated = $this->_getRepository()->getStudyvarsetSelectedVarsPaginated($page2, $nrows, $searchTerm2);

        # ensure correct page-count in ui
        if ($selectedStudyvariablesPaginated->last < $page2) {
            $page2 = $selectedStudyvariablesPaginated->last;
            $this->localsession->setKeyValue("dashin_owner_editstudyvarsetvars_page2", $page2);
        }

        $studyvariablesetInfo = $this->_getRepository()->getStudyvariablesetMinimal($studyvariablesetId, $studyId);
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        $studydatasets = $this->_getRepository()->getStudyDatasets($studyId);
        if ($studydatasets) {
            foreach ($studydatasets as &$row) {
                $row = (object)$row;
            }
        }

        return [
            "studyvariableset_info"     => $studyvariablesetInfo,
            "study_info"                => $studyInfo,
            "studydatasets"             => $studydatasets,
            "unselected_studyvariables" => $unselectedStudyvariablesPaginated,
            "selected_studyvariables"   => $selectedStudyvariablesPaginated,
        ];
    }

    public function listStudyvariablesets($page, $nrows, $reset, $studyId)
    {

        $studyvariablesetId = false;

        $action = $this->Btn->getAction();

        if ($this->SU->allowWrite()) {

            if ($action === "edit") {

                $studyvariablesetId = $this->Btn->getValue();

                if (UniqueId::uuidValidate(($studyvariablesetId))) {
                    $this->localsession->setKeyValue("dashin_owner_studyvariableset_id", $studyvariablesetId);
                    $this->response->redirect("/dashin/owner/studyexport/editstudyvarset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }

            } elseif ($action === "delete") {

                $studyvariablesetId = $this->Btn->getValue();

                if (UniqueId::uuidValidate($studyvariablesetId)) {
                    $this->setKeyValue("dashin_owner_delete_studyvariableset", $studyvariablesetId);
                } else {
                    $this->removeKeyValue("dashin_owner_delete_studyvariableset");
                }

            } elseif ($action === "confirm") {

                $studyvariablesetId = $this->Btn->getValue();

                $studyvariablesetIdSaved = $this->getKeyValue("dashin_owner_delete_studyvariableset");

                if ($studyvariablesetId === $studyvariablesetIdSaved) {
                    $this->_getRepository()->deleteStudyvariableset($studyvariablesetId, $studyId);
                }

                $this->localsession->removeKeyValue("dashin_owner_studyvariableset_id");
                $this->removeKeyValue("dashin_owner_delete_studyvariableset");

                $studyvariablesetId = false;
            }

        }

        $searchTerm = $this->getSearchTerm("dashin_studyexport_list_studyvariablesets_search_term", $reset);

        $studyvariablesetsPaginated = $this->_getRepository()->getStudyvariablesetsPaginated($page, $nrows, $searchTerm);
        if ($studyvariablesetsPaginated) {
            foreach ($studyvariablesetsPaginated->items as &$row) {
                $row->confirm = ($row->studyvariableset_id === $studyvariablesetId);
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "studyvariablesets_paginated" => $studyvariablesetsPaginated,
            "study_info"                  => $studyInfo,
        ];
    }

    public function createStudyexportset($studyId)
    {

        $form = new StudyexportsetForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $studyexportsetId = $this->_getRepository()->createStudyexportset($studyId);
                if ($studyexportsetId) {
                    $this->localsession->setKeyValue("dashin_owner_studyexportset_id", $studyexportsetId);
                    $this->response->redirect("/dashin/owner/studyexport/editstudyexportset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "form"       => $form,
            "study_info" => $studyInfo,
        ];
    }

    public function editStudyexportset($page1, $page2, $nrows, $reset1, $reset2, $studyexportsetId, $studyId)
    {

        error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);
        $action = $this->Btn->getAction();
//        $form = new StudyexportsetForm();
//        if ($this->request->isPost()) {
//            if (!$form->isValid($this->request->getPost())) {
//                foreach ($form->getMessages() as $message) {
//                    $this->flashSession->error($message);
//                }
//            } else {
//                $ok = $this->_getRepository()->updateStudyexportset($studyexportsetId, $studyId);
//            }
//        }
        if ($action === 'setstudyrowset') {
            $studyrowsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyrowsetId)) {
                $ok = $this->_getRepository()->setStudyrowsetOnStudyexportset($studyexportsetId, $studyrowsetId, $studyId);
                if ($ok) {
                    $this->flashSession->success("Rowset successfully updated on exportset");
                }
            }
        } elseif ($action === "rmstudyrowset") {
            $ok = $this->_getRepository()->removeStudyrowsetFromStudyexportset($studyexportsetId, $studyId);
            if ($ok) {
                $this->flashSession->success("Rowset successfully removed from exportset");
            }
        } elseif ($action === "setstudyvarset") {
            $studyrowsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyrowsetId)) {
                $ok = $this->_getRepository()->setStudyvariuablesetOnStudyexportset($studyexportsetId, $studyrowsetId, $studyId);
                if ($ok) {
                    $this->flashSession->success("Variableset successfully updated on exportset");
                }
            }
        } elseif ($action === "rmstudyvarset") {
            $ok = $this->_getRepository()->removeStudyvariuablesetFromStudyexportset($studyexportsetId, $studyId);
            if ($ok) {
                $this->flashSession->success("Variableset successfully removed from exportset");
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studyexportsetInfo = $this->_getRepository()->getStudyexportsetInfo($studyexportsetId, $studyId);
        $studyrowsetInfo = $this->_getRepository()->getStudyrowsetInfo($studyexportsetInfo["studyrowset_id"], $studyId);
        $studyvariablesetInfo = $this->_getRepository()->getStudyvariablesetMinimal($studyexportsetInfo["studyvariableset_id"], $studyId);

        if ($action === "exportmultiple") {
            if ($studyexportsetInfo["n_rows"] == 0) {
                $this->flashSession->warning("Exportset has no rows");
            } elseif ($studyexportsetInfo["n_cols"] == 0) {
                $this->flashSession->warning("Exportset has no variables");
            } else {
                $exportOk = $this->exportStudyexportset($studyexportsetId, $studyId);
                if (!$exportOk) {
                    $this->flashSession->warning("Data could not be exported");
                }
            }
        }
        # FIXME: --------------------------------------------------------------------------
        # FIXME: commented because of bug
        # FIXME: --------------------------------------------------------------------------
        # elseif ($action === "exportsingle") {
        #     if ($studyexportsetInfo["n_rows"] == 0) {
        #         $this->flashSession->warning("Exportset has no rows");
        #     } elseif ($studyexportsetInfo["n_cols"] == 0) {
        #         $this->flashSession->warning("Exportset has no variables");
        #     } else {
        #         $exportOk = $this->exportStudyexportsetSingle($studyexportsetId, $studyId);
        #         if (!$exportOk) {
        #             $this->flashSession->warning("Data could not be exported");
        #         }
        #     }
        # }

        # get paginated studyrowsets
        $searchTerm1 = $this->getSearchTerm("dashin_studyexport_define_studyexportset_search_term1", $reset1, "search_term1");
        $studyrowsetPaginated = $this->_getRepository()->getStudyrowsetsForStudyexportsetPaginated($page1, $nrows, $searchTerm1);
        # get paginated studyvariablesets
        $searchTerm2 = $this->getSearchTerm("dashin_studyexport_define_studyexportset_search_term2", $reset2, "search_term2");
        $studyvariablesetPaginated = $this->_getRepository()->getStudyvariablesetsForStudyexportsetPaginated($page2, $nrows, $searchTerm2);

//        if ($studyexportsetInfo) {
//            $form->bindValues($studyexportsetInfo);
//        }

//            "form"                => $form,
        return [
            "study_info"                 => $studyInfo,
            "studyexportset_info"        => $studyexportsetInfo,
            "studyrowset_info"           => $studyrowsetInfo,
            "studyvariableset_info"      => $studyvariablesetInfo,
            "studyrowset_paginated"      => $studyrowsetPaginated,
            "studyvariableset_paginated" => $studyvariablesetPaginated,
        ];
    }

    public
    function viewStudyexportset($studyexportsetId, $studyId)
    {

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studyexportsetViewInfo = $this->_getRepository()->getStudyexportsetViewInfo($studyexportsetId, $studyId);
        $studyrowsetInfo = $this->_getRepository()->getStudyrowsetInfo($studyexportsetViewInfo["studyrowset_id"], $studyId);
        $studyvariablesetInfo = $this->_getRepository()->getStudyvariablesetMinimal($studyexportsetViewInfo["studyvariableset_id"], $studyId);

        $action = $this->Btn->getAction();
        if ($action === "export") {
            $this->exportStudyexportset($studyexportsetId, $studyId);
        }

        return [
            "studyexportset_info"   => $studyexportsetViewInfo,
            "studyrowset_info"      => $studyrowsetInfo,
            "study_info"            => $studyInfo,
            "studyvariableset_info" => $studyvariablesetInfo,
        ];
    }

    public
    function listStudyexportsets($page, $nrows, $reset, $studyId)
    {

        $studyexportsetId = false;

        $action = $this->Btn->getAction();

        if ($this->SU->allowWrite()) {

            if ($action === "edit" || $action === "view") {

                $studyexportsetId = $this->Btn->getValue();

                if (UniqueId::uuidValidate(($studyexportsetId))) {
                    $this->localsession->setKeyValue("dashin_owner_studyexportset_id", $studyexportsetId);
                    $this->response->redirect("/dashin/owner/studyexport/editstudyexportset/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }

            } elseif ($action === "delete") {

                $studyexportsetId = $this->Btn->getValue();

                if (UniqueId::uuidValidate($studyexportsetId)) {
                    $this->setKeyValue("dashin_owner_delete_studyexportset", $studyexportsetId);
                } else {
                    $this->removeKeyValue("dashin_owner_delete_studyexportset");
                }

            } elseif ($action === "confirm") {

                $studyexportsetId = $this->Btn->getValue();

                $studyexportsetIdSaved = $this->getKeyValue("dashin_owner_delete_studyexportset");

                if ($studyexportsetId === $studyexportsetIdSaved) {
                    $this->_getRepository()->deleteStudyexportset($studyexportsetId, $studyId);
                }

                $this->localsession->removeKeyValue("dashin_owner_studyexportset_id");
                $this->removeKeyValue("dashin_owner_delete_studyexportset");

                $studyexportsetId = false;
            }

        }

        $searchTerm = $this->getSearchTerm("dashin_studyexport_list_studyexportsets_search_term", $reset);

        $studyexportsetsPaginated = $this->_getRepository()->getStudyexportsetsPaginated($page, $nrows, $searchTerm);
        if ($studyexportsetsPaginated) {
            foreach ($studyexportsetsPaginated->items as &$row) {
                $row->confirm = ($row->studyexportset_id === $studyexportsetId);
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "studyexportsets_paginated" => $studyexportsetsPaginated,
            "study_info"                => $studyInfo,
        ];

    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return false|void
     */
    public function exportStudyexportset($studyexportsetId, $studyId)
    {

        $dataQueries = $this->_getRepository()->getStudyexportsetDataQueries($studyexportsetId, $studyId);
        if (!$dataQueries) {
            $this->flashSession->warning("No dataqueries");

            return false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        if (!$studyInfo) {
            $this->flashSession->warning("No study info");

            return false;
        }

        $studyexportsetInfo = $this->_getRepository()->getStudyexportsetInfo($studyexportsetId, $studyId);
        if (!$studyexportsetInfo) {
            $this->flashSession->warning("No export info");

            return false;
        }

        #---------------------
        $datetime = new \DateTime();
        $datetimeStr = $datetime->format('YmdHi');

        #---------------------
//        $prefix = substr(md5($studyexportsetId), 0, 10);
        #---------------------
        $nameStr = substr(preg_replace('# +#', '_', preg_replace('#[^a-zA-Z0-9_ -]#', "", $studyexportsetInfo["name"])), 0, 50);

        #---------------------
        $exportsetName = $nameStr . "_" . $datetimeStr;
        $zipFileName = $exportsetName . ".zip";

        #---------------------
        try {
            $zip = new \ZipArchive();

            $tmpdir = $this->storage->getTempdirLocation();
            $tempZipFilePath = $tmpdir . "/" . UniqueId::uuid4();

//            $tempZipFile = tmpfile();
//            error_log("DEBUG_STUDYEXPORT: ".__FILE__.":".__LINE__);
//            $tempZipFilePath = stream_get_meta_data($tempZipFile)["uri"];
            #---------------------
            $zip->open($tempZipFilePath, \ZipArchive::CREATE);
            $CSV = new Csv();

            foreach ($dataQueries as $dataQuery) {
                $data = $this->_getRepository()->getStudyexportsetDataSingle($dataQuery["data_query"]);
                if ($data) {
                    $datasetName = substr(preg_replace('# +#', '_', preg_replace('#[^a-zA-Z0-9_ -]#', "", $dataQuery["dataset_name"])), 0, 50);
                    $hdr = array_keys($data[0]);
                    $x = $CSV->arrayToCsv($data, ";", $hdr);
                    $zip->addFromString("$exportsetName/$datasetName.csv", $x);
                }
            }
            $zip->close();
            $zipfile = file_get_contents($tempZipFilePath);
            $this->downloader->send($zipfile, $zipFileName, "zip");
        } catch (\Exception $e) {
            $this->flashSession->warning($e->getMessage());
        }
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return false|void
     */
    public function exportStudyexportsetSingle($studyexportsetId, $studyId)
    {

        $dataQuery = $this->_getRepository()->getStudyexportsetSingleDataQuery($studyexportsetId, $studyId);
        if (!$dataQuery) {
            $this->flashSession->warning("No dataqueries");

            return false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        if (!$studyInfo) {
            $this->flashSession->warning("No study info");

            return false;
        }

        $studyexportsetInfo = $this->_getRepository()->getStudyexportsetInfo($studyexportsetId, $studyId);
        if (!$studyexportsetInfo) {
            $this->flashSession->warning("No export info");

            return false;
        }

        #---------------------
        $datetime = new \DateTime();
        $datetimeStr = $datetime->format('YmdHi');

        #---------------------
//        $prefix = substr(md5($studyexportsetId), 0, 10);
        #---------------------
        $nameStr = substr(preg_replace('# +#', '_', preg_replace('#[^a-zA-Z0-9_ -]#', "", $studyexportsetInfo["name"])), 0, 50);

        #---------------------
        $exportsetName = $nameStr . "_" . $datetimeStr;
        $zipFileName = $exportsetName . ".zip";

        #---------------------
        try {
            $zip = new \ZipArchive();

            $tmpdir = $this->storage->getTempdirLocation();
            $tempZipFilePath = $tmpdir . "/" . UniqueId::uuid4();

            #---------------------
            $zip->open($tempZipFilePath, \ZipArchive::CREATE);
            $CSV = new Csv();

            $data = $this->_getRepository()->getStudyexportsetData($dataQuery["data_query"]);
            if ($data) {
                $datasetName = substr(preg_replace('# +#', '_', preg_replace('#[^a-zA-Z0-9_ -]#', "", $dataQuery["dataset_name"])), 0, 50);
                $hdr = json_decode($dataQuery["header_array"], JSON_OBJECT_AS_ARRAY);
                $x = $CSV->arrayToCsv($data, ";", $hdr);
                $zip->addFromString("$exportsetName/$datasetName.csv", $x);
            }
            $zip->close();
            if (!file_exists($tempZipFilePath)) {
                $this->flashSession->error("There was an error exporting data (tempfile deleted before export complete)");

                return false;
            }
            $zipfile = file_get_contents($tempZipFilePath);
            $this->downloader->send($zipfile, $zipFileName, "zip");
        } catch (\Exception $e) {
            $this->flashSession->warning($e->getMessage());
        }
    }

    /**
     * @param $studyId
     *
     * @return array
     */
    public function wholestudyexportOld($studyId)
    {

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        $action = $this->Btn->getAction();
        if ($action == "export") {
            $Exporter = new StudyExporterOld(":memory:");
            $Exporter->addWholestudyData($studyId);
            $Exporter->close();
            $zipContents = $Exporter->getContents();
            $this->downloader->send($zipContents, "ouit_zip_name", "zip");
            $outName = mb_ereg_replace('[^\p{L}\p{N} _-]+', "_", $studyInfo["name"]);

        } elseif ($action === "go_permissions") {
            $this->response->redirect("/dashin/owner/study/editpermissions/{$this->localsession->getQuerystring('?')}");
        }
        $designvarSummary = $this->_getRepository()->exportDesignvarSummary($studyId);
        if ($designvarSummary) {
            $tempArr = [];
            foreach ($designvarSummary as $row) {
                $tempArr[$row["objectname"]] = (object)Json::jsonDecode($row["obj"]);

            }
            unset($row);
            $designvarSummary = $tempArr;
        }

        $studyDatasets = $this->_getRepository()->getStudyDatasets($studyId);
        if ($studyDatasets) {
            foreach ($studyDatasets as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }
        $studySupportFiles = $this->_getRepository()->getStudySupportFiles($studyId);
        if ($studySupportFiles) {
            foreach ($studySupportFiles as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

//        $action = $this->Btn->getAction();
//        if ($action === "exprotstudy") {
//            $studydatatable = $this->_getRepository()->wholestudyexportGetStudydatatable($studyId);
//        }

        return [
            "study_info"         => $studyInfo,
            "designvar_summary"  => $designvarSummary,
            "study_datasets"     => $studyDatasets,
            "study_supportfiles" => $studySupportFiles,
        ];
    }

//    private function _getHeader($array)
//    {
//
//        $header = array_keys($array[0]);
//
//        return $header;
//    }

//    private function _addTableToZip($queryName, $internalPath, $schemaname, &$zip, $CSV)
//    {
//
//        $data = $this->_getRepository()->exportSqdr($schemaname, $queryName);
//        if ($data) {
//            $header = $this->_getHeader($data);
//            $dataCsv = $CSV->arrayToCsv($data, ",", $header);
//            $zip->addFromString($internalPath, $dataCsv);
//        }
//
//    }

    public function exportTempSupportfiles($studyId)
    {

        $storageRoot = $this->storage->getStorageRoot();
        $result = $this->_getRepository()->exportSqdrSupportfiles($studyId);
        foreach ($result as $finfo) {
            $path = "$storageRoot/$finfo[path]";
            $internalPath = "supportfiles/$finfo[name]";
            echo "";
        }
    }

//    private function _addSupportfilesToZip($studyId, \ZipArchive &$zip)
//    {
//
//        $storageRoot = $this->storage->getStorageRoot();
//        $result = $this->_getRepository()->exportSqdrSupportfiles($studyId);
//        foreach ($result as $finfo) {
//            $path = "$storageRoot/$finfo[path]";
//            $internalPath = "supportfiles/$finfo[name]";
//            $zip->addFile($path, $internalPath);
//        }
//
//    }

//    public function exportStudyAsSqdr($studyId)
//    {
//
////        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
////        $action = $this->Btn->getAction();
////        if ($action == "export") {
////        }
//        $schemaname = "studyexport_0a831053630d4b2585788986ea859307";
//        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
//        $zipOutName = substr($studyInfo["name"], 0, 100);
//        $zipOutNameClean = mb_ereg_replace('[^\p{L}\p{N}_-]', '_', $zipOutName) . ".sqdr";
//        try {
//            $zip = new \ZipArchive();
//
//            $tmpdir = $this->storage->getTempdirLocation();
//            $tempZipFilePath = $tmpdir . "/" . UniqueId::uuid4();
//
//            $zip->open($tempZipFilePath, \ZipArchive::CREATE);
//            $CSV = new Csv();
//
//            #-----------------------------------------------------------------------
//            # go through all tables
//            #-----------------------------------------------------------------------
//            $this->_addTableToZip("study", "data/study.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("dataset", "data/dataset.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("assay", "data/assay.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_datatable", "data/datadesign_datatable.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_center", "data/datadesign_x_study_center.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_event", "data/datadesign_x_study_event.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_samplingevent", "data/datadesign_x_study_samplingevent.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_samplingtime", "data/datadesign_x_study_samplingtime.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_startgroup", "data/datadesign_x_study_startgroup.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_subevent", "data/datadesign_x_study_subevent.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("datadesign_x_study_subject", "data/datadesign_x_study_subject.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_center", "data/study_center.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_event", "data/study_event.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_samplingevent", "data/study_samplingevent.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_samplingtime", "data/study_samplingtime.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_startgroup", "data/study_startgroup.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_subevent", "data/study_subevent.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("study_subject", "data/study_subject.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("studydesign_datatable", "data/studydesign_datatable.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("var_dataset", "data/var_dataset.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("var_datatable", "data/var_datatable.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("var_variable", "data/var_variable.csv", $schemaname, $zip, $CSV);
//            $this->_addTableToZip("variable", "data/variable.csv", $schemaname, $zip, $CSV);
//            $this->_addSupportfilesToZip($studyId, $zip);
//
//            $zip->close();
//            if (!file_exists($tempZipFilePath)) {
//                $this->flashSession->error("There was an error exporting data (tempfile deleted before export complete)");
//
//                return false;
//            }
//            $zipfile = file_get_contents($tempZipFilePath);
//            $this->downloader->send($zipfile, $zipOutNameClean, "zip");
//
//        } catch (Exception $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//    }

    public function sqdrImport()
    {
        # https://dashin.local.3q.dk/dashin/owner/studyexport/exportsqdr/?ls=e2u56GALQvy0Mg4_eBtvcv5rTHNQdQvY6m5RDlAydcFmQ4r4dvz2GQV81cECycfM7VxrBxNjcf82GfAJWCtj-s25exXpx4yXfw0ks4ahJYaWIQldIP6pzpiwh-lcyFO1VjMqv4m-d4N-F-ES-S3bg1bUnUyDpriPNqo-UyEU-j7KsCIHtJ31LMxik7u1kIxoRmb_NGCMpLCyjJcShFCcSTIi7VKhU-_yznahH8Gc3p5meWl3mruUuXyGq6d8YX8Apg
        $inPath = "/var/www/squidr_storage/tmp/Scientists_enhance_cell-based_therapy_to_destroy_solid_tumors-2.sqdr";
        echo "";
        $zip = new \ZipArchive();
        $ok = $zip->open($inPath, \ZipArchive::RDONLY);
        if ($ok === true) {
            $nFiles = $zip->numFiles;
            $nFiles2 = $zip->count();
            for($i=0;$i<$nFiles;++$i){
                $fname = $zip->getNameIndex($i);
            }
        }
        $zip->close();

    }

}
