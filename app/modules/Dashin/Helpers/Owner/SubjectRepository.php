<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

/**
 * Class SubjectRepository
 * @package Dashin\Helpers\Owner
 */
class SubjectRepository extends RepositoryBase
{
    /**
     * @param $studyId
     * @return array|false|string[]
     */
    public function createStudysubject($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_subject->create_subject;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $label = ($this->request->getPost("label") ? $this->request->getPost("label") : $this->request->getPost("name"));

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"     => $studyId,
                "owner_org"    => $organizationId,
                "name" => $this->request->getPost("name"),
                "label"        => $label,
                "description"  => $this->request->getPost("description"),
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            if ($e->getCode() === "23505") {
                $subjectName = $this->request->getPost("name");
                return ["error" => "Subject code '$subjectName' already in use in this study"];
            }
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $subjectId
     * @param $studyId
     * @return bool
     */
    public function updateStudysubject($subjectId, $studyId)
    {
        if (!UniqueId::uuidValidate($subjectId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_subject->update_subject;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $startgroupId = (UniqueId::uuidValidate($this->request->getPost("startgroup"))) ? $this->request->getPost("startgroup") : null;
            $centerId = (UniqueId::uuidValidate($this->request->getPost("center"))) ? $this->request->getPost("center") : null;

            $state = $this->db->execute($sql, [
                "subject_id"    => $subjectId,
                "startgroup_id" => $startgroupId,
                "center_id"     => $centerId,
                "study_id"      => $studyId,
                "owner_org"     => $organizationId,
                "name"  => $this->request->getPost("name"),
                "label"         => $this->request->getPost("label"),
                "description"   => $this->request->getPost("description"),
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $subjectId
     * @param $studyId
     * @return bool
     */
    public function updateStudysubjectStartgroup($subjectId, $studyId)
    {
        if (!UniqueId::uuidValidate($subjectId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_subject->update_subject_startgroup;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $startgroupId = (UniqueId::uuidValidate($this->request->getPost("startgroup")[$subjectId])) ? $this->request->getPost("startgroup")[$subjectId] : null;

            $state = $this->db->execute($sql, [
                "subject_id"    => $subjectId,
                "owner_org"     => $organizationId,
                "study_id"      => $studyId,
                "startgroup_id" => $startgroupId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $studyId
     * @return bool
     */
    public function updateallStudysubjectStartgroup($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_subject->update_subject_startgroup;

            $organizationId = $this->SU->getActiveManagerOrgId();
            $startgroups = $this->request->getPost("startgroup");
            if (is_array($startgroups)) {
                $this->db->begin();
                foreach ($startgroups as $subjectId => $startgroupId) {
                    if (UniqueId::uuidValidate($subjectId)) {
                        if (!UniqueId::uuidValidate($startgroupId)) {
                            $startgroupId = null;
                        }
                        $state = $this->db->execute($sql, [
                            "subject_id"    => $subjectId,
                            "owner_org"     => $organizationId,
                            "study_id"      => $studyId,
                            "startgroup_id" => $startgroupId,
                        ]);
                    }
                }
                $this->db->commit();
            }

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $subjectId
     * @param $studyId
     * @return array|false
     */
    public function getStudysubjectMinimal($subjectId, $studyId)
    {
        if (!UniqueId::uuidValidate($subjectId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_subject->get_studysubject;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "subject_id" => $subjectId,
                "study_id"   => $studyId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * delete subject
     * 
     * @param $subjectId
     * @param $studyId
     * @return false
     */
    public function deleteSubject($subjectId, $studyId)
    {
        if (!UniqueId::uuidValidate($subjectId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_subject->delete_studysubject;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "subject_id" => $subjectId,
                "study_id"   => $studyId,
                "owner_org"  => $organizationId,
            ]);
            $ok = $this->db->affectedRows()>0;
            
            return $ok;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $studyId
     * @return false
     */
    public function getCenters($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_centers;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $studyId
     * @return array|false
     */
    public function getStartgroups($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studydesign->get_startgroups;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     * @param $studyId
     * @return false|object
     */
    public function getStudysubjectsPaginated($page, $nrows, $searchTerm, $studyId)
    {
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_subject->get_studysubjects_count_search;
            $itemQuery = $this->dashin_config->sql->owner_subject->get_studysubjects_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_subject->get_studysubjects_count;
            $itemQuery = $this->dashin_config->sql->owner_subject->get_studysubjects_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }
}