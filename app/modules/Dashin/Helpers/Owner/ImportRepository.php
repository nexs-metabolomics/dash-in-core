<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class ImportRepository extends RepositoryBase
{
    /**
     * Create a dataset from uploaded csv file
     *
     * @param $file
     * @param string $sep
     * @param bool $hasHeader
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createDatasetFromUpload(\SplFileObject $file, $sep = ",", $hasHeader = true)
    {
        try {

            #----------------------------------------------------
            # open file
            #----------------------------------------------------
            $organizationId = $this->SU->getActiveManagerOrgId();

            $this->db->begin();

            #----------------------------------------------------
            # create  dataset entry
            #----------------------------------------------------
            $sqlDset = $this->dashin_config->sql->owner_import->create_dataset;
            $dataSetResult = $this->db->fetchOne($sqlDset, Enum::FETCH_ASSOC, [
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "owner_org"   => $organizationId,
            ]);

            #----------------------------------------------------
            # was a dataset entry created? - check for valid id
            #----------------------------------------------------
            if (!UniqueId::uuidValidate($dataSetResult["dataset_id"])) {
                $this->db->rollback();
                return false;
            }
            $datasetId = $dataSetResult["dataset_id"];

            #----------------------------------------------------
            # does dataset contain meaningful data?
            #----------------------------------------------------
            $firstRow = $file->fgetcsv($sep);
            if (!is_array($firstRow)) {
                $this->db->rollback();
                return false;
            }
            $ncol = count($firstRow);
            if (!$ncol) {
                $this->db->rollback();
                return false;
            }

            #----------------------------------------------------
            # create variable entries
            #----------------------------------------------------
            $localIds = range(0, $ncol - 1);
            $sqlHdr = $this->dashin_config->sql->owner_import->create_dataset_variables;

            #----------------------------------------------------
            # we use prepared statement:
            # iterate through inserts instead of generating one sql
            #----------------------------------------------------
            # generate varnames if no header
            if (!$hasHeader) {
                for ($i = 0; $i < $ncol; ++$i) {
                    $this->db->execute($sqlHdr, [
                        "local_id"   => $localIds[$i],
                        "dataset_id" => $datasetId,
                        "name"       => "var" . ($i + 1),
                    ]);
                }
            } else {
                $firstRow = mb_convert_encoding($firstRow, 'UTF-8', 'UTF-8');
                # use first line as header
                for ($i = 0; $i < $ncol; ++$i) {
                    $this->db->execute($sqlHdr, [
                        "local_id"   => $localIds[$i],
                        "dataset_id" => $datasetId,
                        "name"       => (string)$firstRow[$i],
                    ]);
                }
            }

            #----------------------------------------------------
            # datarows
            #----------------------------------------------------
            $sqlDrow = $this->dashin_config->sql->owner_import->create_dataset_datarows;
            $ordinal = 1;
            while (!$file->eof()) {
                $drow = $file->fgetcsv($sep);
                if (count($drow) !== $ncol) {
                    continue;
                }
                $drowJson = Json::jsonEncode(mb_convert_encoding($drow, 'UTF-8', 'UTF-8'));
                $this->db->execute($sqlDrow, [
                    "dataset_id" => $datasetId,
                    "ordinal"    => $ordinal,
                    "datarow"    => $drowJson,
                ]);
                ++$ordinal;
            }

            $this->db->commit();
            return $datasetId;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\LogicException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }
        return false;
    }

    /**
     * Validate that data column is numeric and mark it as such
     *
     * @param $datasetId
     * @return bool
     */
    public function setDatasetDatatypeNumeric($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->dataset_set_datatype_numeric;

            $result = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "limit"      => 10,
            ]);
            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Convert marked columns to numeric
     *
     * @param $datasetId
     * @return bool
     */
//    public function convertDatasetToNumeric($datasetId)
//    {
//        if(!UniqueId::uuidValidate($datasetId)) {
//            return false;
//        }
//        try {
//            $sql = $this->dashin_config->sql->owner_import->dataset_convert_to_numeric;
//
//            $result = $this->db->execute($sql,[
//                "dataset_id" => $datasetId,
//            ]);
//            return $result;
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//        return false;
//    }

    /**
     * Get full dataset info
     *
     * @param $datasetId
     * @return array|bool
     */
    public function getDatasetForView($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_dataset_for_view;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get minimal dataset info
     *
     * @param $datasetId
     * @return array|bool
     */
    public function getDataset($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {

            $sql = $this->dashin_config->sql->owner_import->get_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get variable data for a single variable for edit
     * 
     * @param $datasetId
     * @param $variableId
     *
     * @return array|false
     */
    public function getVariableForEdit($datasetId, $variableId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($variableId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_import->get_variable_for_edit;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_OBJ, [
                "variable_id" => $variableId,
                "dataset_id"  => $datasetId,
                "owner_org"   => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get database info for editing dataset info and metadata
     *
     * @param $datasetId
     * @return array|bool
     */
    public function getDatasetForEdit($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {

            $sql = $this->dashin_config->sql->owner_import->get_dataset_for_edit;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get select options for study-design
     * used in dataset metadata
     *
     * @return array|bool
     */
    public function getStudyDesignOptions()
    {
        try {
//            $sql = $this->dashin_config_config->sql->get_meta_study_design_options;
            $sql = "SELECT researchdesign_id,name FROM dashin.researchdesign";

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Update/edit dataset info and dataset metadata
     *
     * @param $datasetId
     * @param $inData
     * @return bool
     */
    public function updateDataset($datasetId, $inData)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_import->update_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "name"        => $inData["name"],
                "description" => $inData["description"],
                "dataset_id"  => $datasetId,
                "owner_org"   => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get list of datasets
     *
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getDatasetPaginated($page, $nrows, $searchTerm)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->service->get_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->service->get_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->service->get_datasets_count;
            $itemQuery = $this->dashin_config->sql->service->get_datasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }


    /**
     * Delete a dataset
     *
     * @param $datasetId
     * @return bool
     */
    public function deleteDataset($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_import->delete_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * @param $varTypes
     *
     * @return array
     */
    private function _handleVartypes($varTypes)
    {

        $varB = array_intersect($varTypes, [2, 3, 4, 5, 6, 7]);
        $varC = array_diff_key($varTypes, $varB);
        $varD = array_unique($varB);
        if (count($varB) != count($varD)) {
            $varE = array_diff($varB, $varD);
            $outArray = array_merge($varE, $varC, $varD);
        } else {
            $outArray = array_merge($varC, $varB);
        }
        return $outArray;
    }

    /**
     * Check if dataset has a saved design
     * 
     * @param $datasetId
     *
     * @return bool
     */
    public function datasetHasDesign($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->dataset_has_design;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
            ]);

            if (isset($result["n"])) {
                return ($result["n"] === 1);
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Update design variables - in old edit design menu ('listVariables')
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function updateVariableDefinitionAllRowsWithDesign($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        $varTypes = $this->request->getPost("var_type");
        $varTypes = $this->_handleVartypes($varTypes);
        $assays = $this->request->getPost("assay");
        $dataTypes = $this->request->getPost("datatype");
        $unitnum = $this->request->getPost("unitnum");
        $unitdenom = $this->request->getPost("unitdenom");

        try {
            $sql = $this->dashin_config->sql->owner_import->variable_definition_single_with_design;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $variableids = array_keys($varTypes);
            $result = [];
            foreach ($variableids as $variableid) {
                if (UniqueId::uuidValidate($variableid)) {
                    $result[] = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                        "dataset_id"                  => $datasetId,
                        "datatype_id"                 => (isset($dataTypes[$variableid]) ? $dataTypes[$variableid] : 1),
                        "variabletype_id"            => (isset($varTypes[$variableid]) ? $varTypes[$variableid] : 1),
                        "assay_id"                    => (isset($assays[$variableid]) && $assays[$variableid] ? $assays[$variableid] : null),
                        "numerator_prefixedunit_id"   => (isset($unitnum[$variableid]) ? $unitnum[$variableid] : null),
                        "denominator_prefixedunit_id" => (isset($unitdenom[$variableid]) ? $unitdenom[$variableid] : null),
                        "variable_id"                 => $variableid,
                        "owner_org"                   => $organizationId,
                    ]);
                }
            }
            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Update all rows
     *
     * @return array|bool
     */
    public function updateVariableDefinitionAllRowsNoDesign($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $varTypes = $this->request->getPost("var_type");
        $varTypes = $this->_handleVartypes($varTypes);
        $assays = $this->request->getPost("assay");
        $dataTypes = $this->request->getPost("datatype");
        $unitnum = $this->request->getPost("unitnum");
        $unitdenom = $this->request->getPost("unitdenom");
        try {
            $sql = $this->dashin_config->sql->owner_import->variable_definition_single_no_design;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $variableids = array_keys($varTypes);
            foreach ($variableids as $variableid) {
                if (UniqueId::uuidValidate($variableid)) {
                    $state[] = $this->db->execute($sql, [
                        "dataset_id"                  => $datasetId,
                        "datatype_id"                 => (isset($dataTypes[$variableid]) ? $dataTypes[$variableid] : 1),
                        "variabletype_id"            => (isset($varTypes[$variableid]) ? $varTypes[$variableid] : 1),
                        "assay_id"                    => (isset($assays[$variableid]) && $assays[$variableid] ? $assays[$variableid] : null),
                        "numerator_prefixedunit_id"   => (isset($unitnum[$variableid]) ? $unitnum[$variableid] : null),
                        "denominator_prefixedunit_id" => (isset($unitdenom[$variableid]) ? $unitdenom[$variableid] : null),
                        "variable_id"                 => $variableid,
                        "owner_org"                   => $organizationId,
                    ]);
                }
            }
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Generate a version of the variable info table for download
     * (essentially a copy of the current variable info table with content)
     *
     * @param $datasetId
     * @return array|bool
     */
    public function getVariableTemplateForDownload($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_import->variable_template_for_download;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get the structure of a dataset for download
     * To download a template for variable-info
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatasetStructure($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_import->variable_get_dataset_structure;

            #$organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get all data from a single dataset for download
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatasetData($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            # get the dataset query
            $sqlGetQuery = $this->dashin_config->sql->owner_import->variable_get_dataset_full_data_as_string;

            #$organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sqlGetQuery, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                #"owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }


            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get paginated variable info table
     * filtered one or more of the available filter criteria
     *
     * @param $datasetId
     * @param $params
     * @param $page
     * @param $nrows
     * @return array|bool|object
     */
    public function getVariablesByDatasetPaginatorSearch($datasetId, &$params, $page, $nrows)
    {
        #$sqlMethod = "\nAND method_id = :method_id";


        $countQuery = $this->dashin_config->sql->owner_import->variables_by_dataset_search;
        $itemQuery = $this->dashin_config->sql->owner_import->get_variables_paginated_search;

        # OBS: This relies on select-box values == 0 or == "0" means "no action"!

        # delete params from session if all terms are empty
        $keepParams = false;

        $queryParams = ["dataset_id" => $datasetId];
        if (isset($params["varname"]) && $params["varname"]) {
            $countQuery .= "\nAND LOWER(name) LIKE concat('%',LOWER(:varname)::TEXT,'%')";
            $itemQuery .= "\nAND LOWER(v.name) LIKE concat('%',LOWER(:varname)::TEXT,'%')";
            $queryParams["varname"] = str_replace("*", "%", $params["varname"]);
            $keepParams = true;
        }
        if (isset($params["datatype_id"]) && $params["datatype_id"]) {
            $sqlAdd = "\nAND datatype_id = :datatype_id";
            $countQuery .= $sqlAdd;
            $itemQuery .= $sqlAdd;
            $queryParams["datatype_id"] = $params["datatype_id"];
            unset($sqlAdd);
            $keepParams = true;
        }
        if (isset($params["vartype_id"]) && $params["vartype_id"]) {
            $sqlAdd = "\nAND variabletype_id = :vartype_id";
            $countQuery .= $sqlAdd;
            $itemQuery .= $sqlAdd;
            $queryParams["vartype_id"] = $params["vartype_id"];
            unset($sqlAdd);
            $keepParams = true;
        }
        if (isset($params["researchfield_id"]) && $params["researchfield_id"]) {
            $sqlAdd = "\nAND researchfield_id = :researchfield_id";
            $countQuery .= $sqlAdd;
            $itemQuery .= $sqlAdd;
            $queryParams["researchfield_id"] = $params["researchfield_id"];
            unset($sqlAdd);
            $keepParams = true;
        }
        # delete 
        if (!$keepParams) {
            $this->session->remove("dashin_owner_import_listvars_filter_search");
            unset($params);
        }
        $itemQuery .= "\nORDER BY v.local_id LIMIT :limit OFFSET :offset";


        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $outParams = $this->_preparePagination($countQuery, $queryParams, $page, $nrows);

        try {

            $queryParams["offset"] = $outParams["offset"];
            $queryParams["limit"] = $outParams["limit"];

            $outRows = $this->db->fetchAll($itemQuery, Enum::FETCH_OBJ, $queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get list of variables for a dataset - paginated
     * 
     * @param $datasetId
     * @param $page
     * @param $nrows
     * @param $params
     *
     * @return false|object
     */
    public function getVariablesPaginated($datasetId, $page, $nrows, &$params)
    {
        $keepParams = false;
        $queryParams["dataset_id"] = $datasetId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (is_array($params) && count($params) > 0) {

            $countQuery = $this->dashin_config->sql->owner_import->get_variables_paginated_count_search;
            $itemQuery = $this->dashin_config->sql->owner_import->get_variables_paginated_search;

            # OBS: This relies on select-box values == 0 or == "0" means "no action"!

            # delete params from session if all terms are empty

            if (isset($params["varname"]) && $params["varname"]) {
                $countQuery .= "\nAND LOWER(name) LIKE concat('%',LOWER(:varname)::TEXT,'%')";
                $itemQuery .= "\nAND LOWER(v.name) LIKE concat('%',LOWER(:varname)::TEXT,'%')";
                $queryParams["varname"] = str_replace("*", "%", $params["varname"]);
                $keepParams = true;
            }
            if (isset($params["method"]) && $params["method"]) {
                $sqlAdd = "\nAND assay_id IN (SELECT assay_id FROM dashin.assay\n" .
                    "WHERE lower(name || ' ' || description) LIKE lower('%' || (:method)::TEXT || '%'))";
                $countQuery .= $sqlAdd;
                $itemQuery .= $sqlAdd;
                $queryParams["method"] = str_replace("*", "%", $params["method"]);
                unset($sqlAdd);
                $keepParams = true;
            }
            if (isset($params["datatype_id"]) && $params["datatype_id"]) {
                $sqlAdd = "\nAND datatype_id = :datatype_id";
                $countQuery .= $sqlAdd;
                $itemQuery .= $sqlAdd;
                $queryParams["datatype_id"] = $params["datatype_id"];
                unset($sqlAdd);
                $keepParams = true;
            }
            if (isset($params["vartype_id"]) && $params["vartype_id"]) {
                $sqlAdd = "\nAND variabletype_id = :vartype_id";
                $countQuery .= $sqlAdd;
                $itemQuery .= $sqlAdd;
                $queryParams["vartype_id"] = $params["vartype_id"];
                unset($sqlAdd);
                $keepParams = true;
            }
            if (isset($params["researchfield_id"]) && $params["researchfield_id"]) {
                $sqlAdd = "\nAND researchfield_id = :researchfield_id";
                $countQuery .= $sqlAdd;
                $itemQuery .= $sqlAdd;
                $queryParams["researchfield_id"] = $params["researchfield_id"];
                unset($sqlAdd);
                $keepParams = true;
            }
            if ($keepParams) {
                $itemQuery .= "\nORDER BY v.local_id LIMIT :limit OFFSET :offset";
            }
        }

        if (!$keepParams) {
            $this->session->remove("dashin_owner_import_listvars_filter_search");
            unset($params);
            $countQuery = $this->dashin_config->sql->owner_import->get_variables_paginated_count;
            $itemQuery = $this->dashin_config->sql->owner_import->get_variables_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    /**
     * Get paginated variable info table
     * (unfiltered)
     *
     * @param $datasetId
     * @param $page
     * @param $nrows
     * @return array|bool|object
     */
    public function getVariablesByDatasetPaginator($datasetId, $page, $nrows)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $countQuery = $this->dashin_config->sql->owner_import->count_variables_by_dataset;
        $itemQuery = $this->dashin_config->sql->owner_import->get_variables_paginated;

        $queryParams = [];
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["dataset_id"] = $datasetId;
        $outParams = $this->_preparePagination($countQuery, $queryParams, $page, $nrows);

        try {

            $queryParams["offset"] = $outParams["offset"];
            $queryParams["limit"] = $outParams["limit"];


            $outRows = $this->db->fetchAll($itemQuery, Enum::FETCH_OBJ, $queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get select options for variable type selector
     * (used in variable info table)
     *
     * @return array|bool
     */
    public function getVariableTypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_import->get_variabletypes;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get select options for data type selector
     * (used in variable info table)
     *
     * @return array|bool
     */
    public function getDataTypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datatypes;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get select options for assay selector
     * (used in variable info table)
     *
     * @return array|bool
     */
    public function getAssayList()
    {
        try {
            $sql = AssayQuery::$get_assay_list;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get list of verifed valid prefixed units
     * 
     * @return array|false
     */
    public function getValidPrefixedunits()
    {
        try {
            $sql = $this->dashin_config->sql->other->owner_get_valid_prefixedunits;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get paginated data rows
     *
     * @param $datasetId
     * @param $rowStart
     * @param $rowLimit
     * @param $colStart
     * @param $colLimit
     * @return array|bool
     */
    public function getDatarowsPaginated($datasetId, $rowStart, $rowLimit, $colStart, $colLimit)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        # query params
        $organizationId = $this->SU->getActiveManagerOrgId();
        $countParms = ["dataset_id" => $datasetId];
        $countParms["owner_org"] = $organizationId;

        # columns
        $countColsSql = $this->dashin_config->sql->owner_import->count_variables_by_dataset;
        $colParams = $this->_preparePagination($countColsSql, $countParms, $colStart, $colLimit);

        # rows
        $countRowsSql = $this->dashin_config->sql->owner_import->count_datarows_by_dataset;
        $rowParams = $this->_preparePagination($countRowsSql, $countParms, $rowStart, $rowLimit);

        try {

            # column items
            $sql = $this->dashin_config->sql->owner_import->get_variables_paginated_minimal;
            $datacols = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "offset"     => $colParams["offset"],
                "limit"      => $colParams["limit"],
                "owner_org"  => $organizationId,
            ]);

            # alignment
            $datatypes = array_column($datacols, "datatype_id");

            # row query
            $queryLines = implode(",\n", array_column($datacols, "query_line"));
            unset($datacols["query_line"]);
            $datarowQuery =
                "SELECT datarow_id, ordinal,\n" .
                "json_build_array($queryLines) AS datarow\n" .
                "FROM dashin.datatable dt\n" .
                "WHERE dataset_id = :dataset_id\n" .
                "AND EXISTS (SELECT 1 FROM dashin.dataset d WHERE (d.dataset_id,d.owner_org) = (:dataset_id,:owner_org))\n" .
                "ORDER BY ordinal LIMIT :limit OFFSET :offset";

            # finalize column pagination object
            $colParams["items"] = $datacols;
            $colParams = (object)$colParams;

            # row items
            $datarows = $this->db->fetchAll($datarowQuery, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "offset"     => $rowParams["offset"],
                "limit"      => $rowParams["limit"],
                "owner_org"  => $organizationId,
            ]);

            foreach ($datarows as &$row) {
                $tmp = json_decode($row->datarow);
                $tmp2 = [];
                for ($i = 0, $imax = count($tmp); $i < $imax; ++$i) {
                    $tmp2[$i] = (object)["v" => $tmp[$i], "t" => $datatypes[$i]];
                }
                $row->datarow = (object)$tmp2;
            }
            unset($row, $tmp, $tmp2);

            # row pagination object
            $rowParams["items"] = $datarows;
            $rowParams = (object)$rowParams;

            return [
                "datarows"  => $rowParams,
                "datacols"  => $colParams,
                "datatypes" => $datatypes,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get list of distinct values for each design variable
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatadesignDesignvariablesDistinct($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datadesign_design_variable_distinct;

            $organizationId = $this->SU->getActiveManagerOrgId();
            $event = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"    => $datasetId,
                "owner_org"     => $organizationId,
                "variabletype" => "event"
            ]);
            $subevent = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"    => $datasetId,
                "owner_org"     => $organizationId,
                "variabletype" => "subevent"
            ]);
            $startgroup = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"    => $datasetId,
                "owner_org"     => $organizationId,
                "variabletype" => "startgroup"
            ]);
            $subject = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"    => $datasetId,
                "owner_org"     => $organizationId,
                "variabletype" => "subject"
            ]);
            $samplingevent = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"    => $datasetId,
                "owner_org"     => $organizationId,
                "variabletype" => "samplingevent"
            ]);

            return [
                "event"         => $event,
                "subevent"      => $subevent,
                "samplingevent" => $samplingevent,
                "startgroup"    => $startgroup,
                "subject"       => $subject,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get names of design variables in a dataset
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatadesignVariableNames($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datadesign_variable_names;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Save a dataset design
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function datadesignSaveDesign($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->save_datadesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Delete a dataset design
     * 
     * @param $datasetId
     *
     * @return bool
     */
    public function datadesignDeleteDesign($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->delete_datadesign;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Compute all possible sequences of events for individual subjects
     * based on the design variables
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatadesignSubjectEventSequence($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datadesign_subject_event_sequence;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Compute all possible sequences of events for startgroups
     * based on the design variables
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatadesignStartgroupEventSequence($datasetId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_import->get_datadesign_startgroup_event_sequence;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}