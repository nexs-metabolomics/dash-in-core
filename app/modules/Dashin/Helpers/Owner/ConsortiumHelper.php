<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\CreateConsortiumForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Paginator\Adapter\NativeArray as Paginator;

/**
 * Handle consortiums
 * A consortium is a group responsible for a study
 *
 * Class ConsortiumHelper
 *
 * @package Dashin\Helpers\Owner
 */
class ConsortiumHelper extends HelperBase
{
    /**
     * @var ConsortiumRepository
     */
    private $_repository;

    /**
     * @return ConsortiumRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new ConsortiumRepository();
        }
        return $this->_repository;
    }

    /**
     * Add consortium to database
     *
     * @return array|bool
     */
    public function createConsortium()
    {
        $form = new CreateConsortiumForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $inData["name"] = $this->request->getPost("name");
                $inData["description"] = $this->request->getPost("description");
                $consortiumId = $this->_getRepository()->createConsortium($inData);
                if ($consortiumId) {
                    $this->localsession->setKeyValue("dashin_owner_consortium_id", $consortiumId);
                    $this->response->redirect("/dashin/owner/consort/edit/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();
                    return false;
                }
            }
        }

        return [
            "form" => $form,
        ];
    }

    /**
     * Edit consortium
     *
     * @param $consortiumId
     *
     * @return array
     */
    public function editConsortium($consortiumId)
    {
        $form = new CreateConsortiumForm();
        $action = $this->Btn->getAction();
        if ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $inData["name"] = $this->request->getPost("name");
                    $inData["description"] = $this->request->getPost("description");
                    $ok = $this->_getRepository()->updateConsortium($consortiumId, $inData);
                    if($ok && $action === "submit") {
                        $this->response->redirect("/dashin/owner/consort/list/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();
                        return false;
                    }
                }
            }
        }
        $consortium = $this->_getRepository()->getConsortium($consortiumId);
        if ($consortium) {
            $form->bindValues($consortium);
        } else {
            $consortium = false;
        }

        return [
            "form"       => $form,
            "consortium" => $consortium,
        ];
    }

    /**
     * List consoritums
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listConsortiums($page, $nrows, $reset)
    {
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $consortiumId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($consortiumId)) {
                $this->setKeyValue("dashin_owner_delete_consortium", $consortiumId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_consortium");
            }
        } elseif ($action === "confirm") {
            $consortiumId = $this->Btn->getValue();
            $datasetIdSaved = $this->getKeyValue("dashin_owner_delete_consortium");
            if ($consortiumId === $datasetIdSaved) {
                $this->_getRepository()->deleteConsortium($consortiumId);
            }
            $this->removeKeyValue("dashin_owner_delete_consortium");
            $this->localsession->removeKeyValue("dashin_owner_consortium_id");
            $consortiumId = false;
        } else {
            $consortiumId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_list_consortiums_search_term", $reset);
        $data = $this->_getRepository()->getConsortiums($searchTerm);
        if ($data) {
            foreach ($data as &$row) {
                if ($row["consortium_id"] === $consortiumId) {
                    $row["confirm"] = true;
                } else {
                    $row["confirm"] = false;
                }
                $row = (object)$row;
            }
            unset($row);
            $paginator = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $paginator->paginate();
        } else {
            $paginate = false;
        }

        return [
            "consortiums" => $paginate,
            "search_term" => $searchTerm,
        ];
    }
}