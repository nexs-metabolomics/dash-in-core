<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-24
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use Dashin\Helpers\HelperBase;

/**
 * Select objects for controllers under 'owner'
 *
 * Class ServiceHelper
 * @package Dashin\Helpers\Owner
 */
class ServiceHelper extends HelperBase
{
    /**
     * @var ServiceRepository
     */
    private $_repository;

    /**
     * @return ServiceRepository
     */
    private function _getRepository()
    {
        if (!$this->_repository) {
            $this->_repository = new ServiceRepository();
        }
        return $this->_repository;
    }

    /**
     * Select objects
     * This is the entry point for both the initial select call
     * and the return call after object is selected (or cancelled)
     *
     * call parameters
     * 'select' is called with a parameter variable containing at least
     *  1) a return uri pointing to the calling context
     *  2) a key for the object-id
     *  3) the service name
     * It may also contain object ids necessary for selecting object of interest
     * (fe. to select a study-subject the study-id must be known)
     *
     * @param int $page page number for pagination of the selectable objects
     * @param int $nrows number of items per page
     * @param int $reset reset the search filter
     * @param array $params call parameters
     * @param string $serviceName name of the service (the select-action)
     * @return array|false
     */
    public function select($page, $nrows, $reset, $params, $serviceName)
    {
        $action = $this->Btn->getAction();
        if ($action === "select") {
            $this->removeKeyValue("dashin_owner_service_select_{$serviceName}");
            $itemId = $this->Btn->getValue();
            $returnUri = $params["return_uri"];
            $key = $params["key"];
            $this->serviceSelectReturn($returnUri, $key, $itemId);
            if ($this->response->isSent()) {
                return false;
            }
        } elseif ($action === "cancel") {
            $this->removeKeyValue("dashin_owner_service_select_{$serviceName}");
            $returnUri = $params["return_uri"];
            $querystring = $this->localsession->getQuerystring("?");
            $this->response->redirect("$returnUri/$querystring");
            $this->response->send();
            return false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_service_select_{$serviceName}_search_term", $reset);

        if ($serviceName === "assay") {
            $data = $this->_getRepository()->getAssayPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_assay");

        } elseif ($serviceName === "consortium") {
            $data = $this->_getRepository()->getConsortiumPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_consortium");

        } elseif ($serviceName === "datacolumntype") {
            $data = $this->_getRepository()->getDatacolumntypePaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_datacolumntype");

        } elseif ($serviceName === "dataset") {
            $data = $this->_getRepository()->getDatasetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_dataset");

        } elseif ($serviceName === "datadesign") {
            $data = $this->_getRepository()->getDatadesignPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_datadesign");

        } elseif ($serviceName === "center") {
            $data = $this->_getRepository()->getCenterPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_center");

        } elseif ($serviceName === "startgroup") {
            $data = $this->_getRepository()->getStartgroupPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_startgroup");

        } elseif ($serviceName === "samplingevent") {
            $data = $this->_getRepository()->getSamplingEventPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_samplingevent");

        } elseif ($serviceName === "event") {
            $data = $this->_getRepository()->getEventPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_event");

        } elseif ($serviceName === "subevent") {
            $data = $this->_getRepository()->getSubeventPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_subevent");

        } elseif ($serviceName === "study") {
            $data = $this->_getRepository()->getStudyPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_study");

        } elseif ($serviceName === "studyfile") {
            $data = $this->_getRepository()->getStudyfilePaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studyfile");

        } elseif ($serviceName === "studycontact") {
            $data = $this->_getRepository()->getStudycontactPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studycontact");

        } elseif ($serviceName === "studydataset") {
            $data = $this->_getRepository()->getStudydatasetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studydataset");

        } elseif ($serviceName === "subject") {
            $data = $this->_getRepository()->getSubjectPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_subject");

        } elseif ($serviceName === "upvardataset") {
            $data = $this->_getRepository()->getUpvarDatasetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_upvardataset");

        } elseif ($serviceName === "vardataset") {
            $data = $this->_getRepository()->getVarDatasetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_vardataset");

        } elseif ($serviceName === "searchresultset") {
            $data = $this->_getRepository()->getResultsetsPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_resultset");

        } elseif ($serviceName === "resultsetdataset") {
            $data = $this->_getRepository()->getResultsetDatasetsPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_resultsetdataset");

        } elseif ($serviceName === "variable") {
            $data = $this->_getRepository()->getVariablesPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_variable");

        } elseif ($serviceName === "studyrowset") {
            $data = $this->_getRepository()->getStudyrowsetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studyrowset");

        } elseif ($serviceName === "studyvarcondition") {
            $data = $this->_getRepository()->getStudyvarconditionPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studyvarcondition");

        } elseif ($serviceName === "studyvarset") {
            $data = $this->_getRepository()->getStudyvariablesetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studyvarset");

        } elseif ($serviceName === "studyexportset") {
            $data = $this->_getRepository()->getStudyexportsetPaginated($page, $nrows, $searchTerm);
            $this->view->pick("modules/dashin/owner/service/select_studyexportset");

        }
        #--------------------------------------------------------------
        # dynamic menu - show caller's menu context
        #--------------------------------------------------------------
//        $menuParams = $this->getKeyValue("dashin_owner_service_selectx_menu");
//        $submenu = $menuParams["submenu"];
//        $current = $menuParams["current"];
//        $menuPartial = $menuParams["menu_partial"];
//        $this->view->setVar("show_submenu_{$submenu}", 1);
//        $this->view->setVar("current", $current);
//        $this->view->setVar("menu_partial", $menuPartial);

        return [
            "data"        => $data,
            "search_term" => $searchTerm,
        ];
    }

    /**
     * Obsolete function todo: remove this function
     * @param $page
     * @param $nrow
     * @param $reset
     * @param $params
     * @return array|false
     */
    public function selectSeclabel($page, $nrow, $reset, $params)
    {
        $action = $this->Btn->getAction();
        if ($action === "select") {

            $this->removeKeyValue("dashin_owner_service_select_seclabel");
            $seclabelId = $this->Btn->getValue();
            $returnUri = $params["return_uri"];
            $key = $params["key"];
            $this->serviceSelectSeclabel($returnUri, $key, true, $seclabelId);
        } elseif ($action === "cancel") {
            $this->removeKeyValue("dashin_owner_service_select_seclabel");
            $returnUri = $params["return_uri"];
            $this->response->redirect($returnUri);
            $this->response->send();
            return false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_service_select_seclabel_search_term", $reset);
        $data = $this->_getRepository()->getSeclabelsPaginated($page, $nrow, $searchTerm);
        return [
            "data"        => $data,
            "search_term" => $searchTerm,
        ];
    }

}