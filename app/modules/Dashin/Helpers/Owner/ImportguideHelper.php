<?php

namespace Dashin\Helpers\Owner;

use App\Library\Paginator\CsvHeaderPaginator;
use App\Library\Utils\Csv;
use App\Library\Utils\CsvFile;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\ImportguideHasQualityControlDataForm;
use Dashin\Forms\Owner\ImportguideImportDatafileForm;
use Dashin\Forms\Owner\ImportguideImportSummaryForm;
use Dashin\Forms\Owner\ImportguideUploadDatafileForm;
use Dashin\Helpers\HelperBase;
use Phalcon\Forms\Element\Select;

class ImportguideHelper extends HelperBase
{

    /**
     * @var ImportguideRepository
     */
    private $_repository;

    private $_csv;

    private $_uploadedFile;

    private function _getCsv()
    {

        if (!$this->_csv) {
            $this->_csv = new Csv();
        }

        return $this->_csv;
    }

    /**
     * @return ImportguideRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new ImportguideRepository();
        }

        return $this->_repository;
    }

    /**
     * Identify numeric columns
     * and mark them as such in the variable table
     *
     * @param $datasetId
     *
     * @return bool
     */
    private function _handleDatatype($datasetId)
    {

        $ok = $this->_getRepository()->setDatasetDatatypeNumeric($datasetId);
        if (!$ok) {
            return false;
        }
    }

    /**
     * Internal function to handle uploading of dataset
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    private function _handleDataImport($filepath)
    {

        if (!file_exists($filepath)) {
            return false;
        }

        # hardcoded list of recognized separators
        $separators = ["c" => ",", "sc" => ";", "t" => "\t", "sp" => " "];
        $sep = $this->request->getPost("sep");

        # null: automatic
        $sep = $separators[$sep] ?? null;

        $file = new \SplFileObject($filepath, "r");
        $sep = $this->_guessSeparator($file, $sep);
        if (!$sep) {
            return false;
        }

        # data file must always have a header
        $datasetId = $this->_getRepository()->createDatasetFromUpload($file, $sep);

        # Identify numeric columns, mark as such in variable table
        $this->_handleDatatype($datasetId);

        return $datasetId;
    }

    /**
     * Remove old data files from temp dir
     *
     * @return void
     */
    private function _uploadedFilesCleanup()
    {

        $now = microtime(true);
        $tempdir = $this->config->tempdir;
        $files = scandir($tempdir);
        foreach ($files as $file) {
            if (!($file === '.' || $file == '..')) {
                $filepath = $tempdir . "/" . $file;
                $filestat = stat($filepath);

                $timediff = ($now - $filestat["atime"]) / 3600;
                # more than 6 hours
                if ($timediff > 6) {
                    unlink($filepath);
                }
            }
        }
        echo "";
    }

    /**
     * Prepare data file for import
     *
     * @param                               $filepath
     * @param ImportguideImportDatafileForm $form
     *
     * @return array|false|null
     */
    private function _prepareImport($filepath, ImportguideImportDatafileForm &$form)
    {

        # guess separator - get header
        $file = new \SplFileObject($filepath, "r");
        $sep = $this->_getCsv()->guessSeparator($file);
        $header = $this->_getCsv()->getHeader($file, $sep);

        if ($form->has("sep")) {
            $separators = ["," => "c", ";" => "sc", "\t" => "t", " " => "sp"];
            $sepidx = $separators[$sep];
            $form->get("sep")->setDefault($sepidx);
        }
        if ($form->has("header") && $header) {
            $form->get("header")->setAttributes(["checked" => "yes", "value" => "yes"]);
        }

        return $header;
    }

    /**
     * Move uploaded file to temporary folder
     *
     * Returns array
     *      'session_filename': name of temporary saved file,
     *      'original_filename': original name without extension
     *
     * @return array|false|void
     */
    private function _moveUploadedFile()
    {

        if ($this->request->hasFiles()) {
            # get tempdir
            $tempdir = $this->config->tempdir;

            # TODO: Should we allow multiple files?
            # rewtrieve file object
            $file = $this->request->getUploadedFiles(true)[0];
            $fileName = $file->getName();

            # remove extension (we don't need it)
            $extlen = mb_strlen($file->getExtension());
            if ($extlen > 0) {
                $fileName = mb_strcut($fileName, 0, mb_strlen($fileName) - $extlen);
            }

            # sanitize filename for later use
            # remove extension, replace non-word (utf8) characters 
            $maxlen = 200;
            $fileNameClean = mb_strcut(mb_ereg_replace('[^\w.-]+', "_", $fileName), 0, $maxlen);

            # create temporary name
            $uploadedFileName = UniqueId::uuid4();

            # move uploaded file to tempdir
            $uploadedFilePath = $tempdir . DIRECTORY_SEPARATOR . $uploadedFileName;
            $uploadOk = move_uploaded_file($file->getTempName(), $uploadedFilePath);

            if ($uploadOk) {

                return [
                    "session_filename"  => $uploadedFileName,
                    "original_filename" => $fileNameClean,
                ];
            }

            $this->flashSession->warning("Could not upload file");

            return false;
        }
    }

    /**
     * Upload a data file to temporary folder
     * in order to handle it for import in the import function
     *
     * @return ImportguideUploadDatafileForm[]|false
     */
    public function uploadDatafile()
    {

        $form = new ImportguideUploadDatafileForm();
        $action = $this->Btn->getAction();
        if ($action === "submit") {

            $uploadedFileInfo = $this->_moveUploadedFile();
            if ($uploadedFileInfo) {
                $this->localsession->setKeyValue("dashin_owner_importguide_uploaded_file", $uploadedFileInfo);
                $this->response->redirect("/dashin/owner/importguide/import/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        }

        $this->_uploadedFilesCleanup();

        return [
            "form" => $form,
        ];
    }

    /**
     * Import a recently uploaded data-file inot the database
     * 
     * @return ImportguideImportDatafileForm[]|false
     * @throws \App\Library\Utils\JsonException
     */
    public function importDatafile($page, $nrows, $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "gotoupload") {
            $this->response->redirect("/dashin/owner/importguide/upload/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }
        #---------------------------------------------------------------------
        # get saved file-info
        #---------------------------------------------------------------------
        $uploadedFileInfo = $this->localsession->getKeyValue("dashin_owner_importguide_uploaded_file");
        if (isset($uploadedFileInfo["session_filename"]) && isset($uploadedFileInfo["original_filename"])) {

            # create file path
            $tmpdir = $this->config->tempdir;
            $filepath = $tmpdir . "/" . $uploadedFileInfo["session_filename"];

            if (file_exists($filepath)) {

                # update file atime - to prevent premature deleting 
                touch($filepath);
                $this->_uploadedFile = new CsvFile($filepath);
            }
        }

        #---------------------------------------------------------------------
        # remove stale uploaded data files
        #---------------------------------------------------------------------
        $this->_uploadedFilesCleanup();

        #---------------------------------------------------------------------
        # abort if no file
        #---------------------------------------------------------------------
        if (!$this->_uploadedFile) {

            $this->flashSession->warning("No uploaded file could be found. Please upload again.");

            # remove session reference to non-existing file
            $this->localsession->removeKeyValue("dashin_owner_importguide_uploaded_file");

            return false;
        }

        if ($action === "back") {
            # remove has_quality_control_data entry
            $uploadedFileInfo = [
                "session_filename"  => $uploadedFileInfo["session_filename"],
                "original_filename" => $uploadedFileInfo["original_filename"],
            ];
            $this->localsession->setKeyValue("dashin_owner_importguide_uploaded_file", $uploadedFileInfo);
            $this->response->redirect("/dashin/owner/importguide/import/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $this->localsession->setKeyValue("dashin_owner_importguide_uploaded_file", $uploadedFileInfo);

        $searchTerm = $this->getSearchTerm("dashin_owner_import_list_datasets_search_term", $reset);
        $headerPagination = $this->_uploadedFile->paginate($page, $nrows, $searchTerm);
        if (!$headerPagination) {
            return false;
        }

        $form = new ImportguideImportDatafileForm();
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $datasetId = $this->_handleDataImport($filepath);
                if ($datasetId) {
                    # TODO: remove file-info
                    # TODO: delete file
                    # TODO: general cleanup

                    #--------------------------------------------------------------------------
                    # OBS: save dataset-id both relative to the import guide and in general
                    #
                    # The import guide is only valid for newly imported dataset, but newly
                    # imported datasets should also be the current dataset in other contexts
                    #--------------------------------------------------------------------------
                    $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
                    $this->localsession->setKeyValue("dashin_owner_importguide_dataset_id", $datasetId);
                    $this->response->redirect("/dashin/owner/importguide/importsummary/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        $this->_prepareImport($filepath, $form);
        if ($form->has("name")) {
            $form->get("name")->setAttribute("value", $uploadedFileInfo["original_filename"]);
        }

        return [
            "form"              => ($form ?? false),
            "header_pagination" => ($headerPagination ?? false),
        ];
    }

    /**
     * Show summary of relevant info after the dataset import
     * with option to update name and description
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function datasetImportSummary($datasetId)
    {

        $action = $this->Btn->getAction();
        if ($action === "gotoupload") {
            $this->response->redirect("/dashin/owner/importguide/upload/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $form = new ImportguideImportSummaryForm();
        if ($action === "update" || $action === "continue") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                # update name, description
                $ok = $this->_getRepository()->updateDatasetNameDescription($datasetId);
                if ($ok && $action == "continue") {
                    $this->setKeyValue("dashin_owner_importguide_dataset_id", $datasetId);
                    $this->response->redirect("/dashin/owner/importguide/designedit/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }

            }
        }
        $summary = $this->_getRepository()->datasetSummary($datasetId);
        if ($form->has("name")) {
            $form->get("name")->setAttribute("value", $summary["name"]);
        }
        if ($form->has("description")) {
            $form->get("description")->setAttribute("value", $summary["description"]);
        }

        return [
            "import_summary" => $summary,
            "form"           => $form,
        ];
    }

    /**
     * Edit design variables for a dataset
     * and save the design
     * 
     * @param          $datasetId
     * @param int|null $page
     * @param int|null $nrows
     * @param int|null $reset
     *
     * @return array|false
     */
    public function editDatasetDesign($datasetId, ?int $page, ?int $nrows, ?int $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "update" || $action === "continue") {
            $saveOk = $this->_getRepository()->saveOrUpdateDatadesign($datasetId);
            if ($saveOk && $action === "continue") {
                $this->response->redirect("/dashin/owner/importguide/complete{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        } elseif ($action === "viewdesign") {
            $this->response->redirect("/dashin/owner/importguide/designview{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        } elseif ($action === "nosave") {
            $this->response->redirect("/dashin/owner/importguide/complete{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $searchTerm = $this->getSearchTerm("dashin_importguide_dataset_design_search_term", $reset);
        $variablesPaginated = $this->_getRepository()->getVariablesPaginated($datasetId, $page, $nrows, $searchTerm);
        if ($variablesPaginated) {

            # variabltype select
            $variabletypes = $this->_getRepository()->getVariabletypes();
            $variabltypeValues = array_column($variabletypes, "variabletype_id");
            $variabltypeNames = array_column($variabletypes, "name");
            $variabletypeSelectList = array_combine($variabltypeValues, $variabltypeNames);

            foreach ($variablesPaginated->items as &$item) {
                $variabletypeField = new Select("vartype[$item->variable_id]", $variabletypeSelectList, [
                    "id"       => "vartype_{$item->variable_id}",
                    "class"    => "vartype",
                    "form"     => "form1",
                    "onchange" => " vartypeChange(this); ",
                ]);
                $variabletypeField->setDefault($item->variabletype_id);
                $item->vartype = $variabletypeField;
            }
        }

        $datadesignHasDuplicates = $this->_getRepository()->datasetDesignHasDuplicates($datasetId);

        $datasetInfo = $this->_getRepository()->datasetSummary($datasetId);

        return [
            "variables_paginated"   => $variablesPaginated,
            "dataset_info"          => $datasetInfo,
            "design_has_duplicates" => $datadesignHasDuplicates,
        ];
    }

    /**
     * View summary of dataset design
     *
     * @param $datasetId
     * @param $page      int|null current page of paginated variables
     * @param $nrows     int|null number of rows to show on one page
     * @param $reset     int|null reset search  (1=reset, 0|null=do nothing)
     *
     * @return array
     */
    public function viewDatasetDesign($datasetId, ?int $page, ?int $nrows, ?int $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "update" || $action === "continue") {
            $result = $this->_getRepository()->saveOrUpdateDatadesign($datasetId);
            if ($action === "continue") {
//                $this->response->redirect();
            }

        }

        $searchTerm = $this->getSearchTerm("dashin_importguide_dataset_design_search_term", $reset);
        $variablesPaginated = $this->_getRepository()->getVariablesPaginated($datasetId, $page, $nrows, $searchTerm);
        if ($variablesPaginated) {

            # variabltype select
            $variabletypes = $this->_getRepository()->getVariabletypes();
            $variabltypeValues = array_column($variabletypes, "variabletype_id");
            $variabltypeNames = array_column($variabletypes, "name");
            $variabletypeSelectList = array_combine($variabltypeValues, $variabltypeNames);

            foreach ($variablesPaginated->items as &$item) {
                $variabletypeField = new Select("vartype[$item->variable_id]", $variabletypeSelectList, [
                    "id"       => "vartype_{$item->variable_id}",
                    "class"    => "vartype",
                    "form"     => "form1",
                    "onchange" => " vartypeChange(this); ",
                ]);
                $variabletypeField->setDefault($item->variabletype_id);
                $item->vartype = $variabletypeField;
            }
        }

        $datasetInfo = $this->_getRepository()->datasetSummary($datasetId);
        $preDatasetDesign = $this->_getRepository()->getPreDatasetDesignTable($datasetId);
        if ($preDatasetDesign) {
            foreach ($preDatasetDesign as &$row) {
                $row = (object)$row;
            }
        }

        return [
            "variables_paginated" => $variablesPaginated,
            "dataset_info"        => $datasetInfo,
            "pre_dataset_design"  => $preDatasetDesign,
        ];

    }

    /**
     * Final menu shown after dataset import is complete
     * 
     * @param $datasetId
     *
     * @return array|false
     */
    public function importguideComplete($datasetId)
    {

        $action = $this->Btn->getAction();
        if ($action === "newstudy") {
            $this->setKeyValue("dashin_owner_studycreationguide_dataset_id", $datasetId);
            $this->response->redirect("/dashin/owner/studycreate/createfromdata/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
            
        }
        
        $datasetInfo = $this->_getRepository()->datasetSummary($datasetId);

        return [
            "dataset_info" => $datasetInfo,
        ];
    }

}

