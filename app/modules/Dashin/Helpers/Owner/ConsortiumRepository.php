<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

/**
 * Database layer for Consortium handler
 *
 * Class ConsortiumRepository
 * @package Dashin\Helpers\Owner
 */
class ConsortiumRepository extends RepositoryBase
{
    /**
     * Create a consortium
     *
     * @param $inData
     * @return bool
     */
    public function createConsortium($inData)
    {
        try {
            $sql = $this->dashin_config->sql->other->owner_create_consortium;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $inData["name"],
                "description" => $inData["description"],
            ]);

            if ((isset($result["consortium_id"]))) {
                return $result["consortium_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Update name and description of consortium
     *
     * @param $consortiumId
     * @param $inData
     * @return array|bool
     */
    public function updateConsortium($consortiumId, $inData)
    {
        if (!UniqueId::uuidValidate($consortiumId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->other->owner_update_consortium;

            $state = $this->db->execute($sql,  [
                "consortium_id" => $consortiumId,
                "name"          => $inData["name"],
                "description"   => $inData["description"],
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get consortium
     *
     * @param $consortiumId
     * @return array|bool
     */
    public function getConsortium($consortiumId)
    {
        if (!UniqueId::uuidValidate($consortiumId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->other->owner_get_consortium;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "consortium_id" => $consortiumId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get a list of consortiums
     *
     * @return bool
     */
    public function getConsortiums($searchTerm)
    {
        try {

            if (strlen($searchTerm)) {
                $sql = $this->dashin_config->sql->other->owner_get_consortiums_search;
                $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "search_term" => $searchTerm,
                ]);
            } else {
                $sql = $this->dashin_config->sql->other->owner_get_consortiums;
                $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            }

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Delete consortium
     *
     * @param $consortiumId
     * @return bool
     */
    public function deleteConsortium($consortiumId)
    {
        if (!UniqueId::uuidValidate($consortiumId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->other->owner_delete_consortium;

            $result = $this->db->execute($sql, [
                "consortium_id" => $consortiumId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}