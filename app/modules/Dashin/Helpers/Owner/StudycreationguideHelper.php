<?php

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\HelperBase;

class StudycreationguideHelper extends HelperBase
{

    /**
     * @var StudycreationguideRepository
     */
    private $_repository;

    /**
     * @return StudycreationguideRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new StudycreationguideRepository();
        }

        return $this->_repository;
    }

    public function createStudyFromDataset($datasetId)
    {

//        $name = $this->request->getPost("name");
//        $title = $this->request->getPost("title");
//        $description = $this->request->getPost("description");

        #----------------------------------------------------------------
        # generate study parameters
        #----------------------------------------------------------------
        $datasetInfo = $this->_getRepository()->getDatasetMinimal($datasetId);
        if (!$datasetInfo) {
            return false;
        }
        $datasetName = $datasetInfo["name"];
        $datasetDescription = $datasetInfo["description"];
        $studyParams["name"] = "Study name auto-created from '$datasetName'";
        $studyParams["title"] = "Study title auto-created from '$datasetName'";
        $studyParams["description"] = "Study description auto-created from '$datasetDescription'";

        #----------------------------------------------------------------
        # create study
        #----------------------------------------------------------------
        $studyId = $this->_getRepository()->createStudy($studyParams);

        #----------------------------------------------------------------
        # import data-design
        #----------------------------------------------------------------
        $result = $this->_getRepository()->importDatadesign($studyId, $datasetId);

        # create study
//        $form;
    }

    /**
     * Add a new dataset to an existing study
     * 
     * @param $studyId
     * @param $datasetId
     *
     * @return array
     */
    public function guideAddDataset($studyId, $datasetId)
    {

        $confirmImport = false;
        $confirmRemove = false;
        
        $action = $this->Btn->getAction();
        if ($action === "save") {

            if (UniqueId::uuidValidate($studyId) && UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_study_guide_add_dataset", [$studyId, $datasetId]);
                $confirmImport = true;
            } else {
                $this->removeKeyValue("owner_study_guide_add_dataset");
            }

        } elseif ($action === "confirm") {

            $IdsSaved = $this->getKeyValue("owner_study_guide_add_dataset");
            if ($IdsSaved[0] === $studyId && $IdsSaved[1] === $datasetId) {
                # $this->_getRepository()->studyAddDataset($studyId, $datasetId);
            }
            $this->removeKeyValue("owner_study_guide_add_dataset");

        } elseif ($action === "remove") {

            if (UniqueId::uuidValidate($studyId) && UniqueId::uuidValidate($datasetId)) {
                $this->setKeyValue("owner_study_guide_remove_dataset", [$studyId, $datasetId]);
                $confirmRemove = true;
            } else {
                $this->removeKeyValue("owner_study_guide_remove_dataset");
            }

        } elseif ($action === "confirmremove") {

            $IdsSaved = $this->getKeyValue("owner_studydesign_study_remove_dataset");
            if ($IdsSaved[0] === $studyId && $IdsSaved[1] === $datasetId) {
                # $this->_getRepository()->studyRemoveData($studyId, $datasetId);
            }
            $this->removeKeyValue("owner_study_guide_remove_dataset");

        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $datasetInfo = $this->_getRepository()->getDatasetStudyindicatorMinimal($datasetId, $studyId);

        $data = $this->_getRepository()->guideStudydesignValidateData($studyId, $datasetId);
        $keys = array_column($data, "element_name");
        $extractMatch = function ($data, $keys, $keyStub) {
            # @formatter:off
            $event         = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}event"))));
            $subevent      = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}subevent"))));
            $samplingevent = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}samplingevent"))));
            $samplingtime  = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}samplingtime"))));
            $startgroup    = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}startgroup"))));
            $subject       = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}subject"))));
            $center        = array_values(array_intersect_key($data,array_flip(array_keys($keys,"{$keyStub}center"))));
            # @formatter:on
            return (object)[
                "event"         => $event,
                "subevent"      => $subevent,
                "samplingevent" => $samplingevent,
                "samplingtime"  => $samplingtime,
                "startgroup"    => $startgroup,
                "subject"       => $subject,
                "center"        => $center,
            ];
        };
        $allSummary = array_values(array_intersect_key($data, array_flip(array_keys($keys, "all_summary"))));
        if (isset($allSummary[0])) {
            $allSummary = $allSummary[0];
            $designIsValid = ($allSummary->data_study_matched > 0 && $allSummary->data_unmatched === 0) ? 1 : 0;
        } else {
            $allSummary = null;
            $designIsValid = null;
        }
        return [
            "study_info"      => $studyInfo,
            "dataset_info"    => $datasetInfo,
            "data_unmatched"  => $extractMatch($data, $keys, "data_unmatched_"),
            "data_matched"    => $extractMatch($data, $keys, "data_matched_"),
            "study_unmatched" => $extractMatch($data, $keys, "study_unmatched_"),
            "study_matched"   => $extractMatch($data, $keys, "study_matched_"),
            "all_summary"     => $allSummary,
            "design_is_valid" => $designIsValid,
            "confirm_import"  => $confirmImport,
            "confirm_remove"  => $confirmRemove,
        ];

    }

}