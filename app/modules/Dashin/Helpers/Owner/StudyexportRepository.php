<?php

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class StudyexportRepository extends RepositoryBase
{

    /**
     * Create condition based on single studyvariable
     *
     * @param $studyvariableId - uuid
     * @param $studyId         - uuid
     *
     * @return false|mixed
     */
    public function createStudyvariablecondition($studyvariableId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->create_studyvariablecondition;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariable_id" => $studyvariableId,
                "study_id"         => $studyId,
                "owner_org"        => $organizationId,
            ]);
            if (isset($result["studyvariablecondition_id"])) {
                return $result["studyvariablecondition_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete studyvariable condition
     *
     * @param $studyvariableconditionId - uuid
     * @param $studyId                  - uuid
     *
     * @return array|false
     */
    public function deleteStudyvariablecondition($studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->delete_studyvariablecondition;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $page            - int
     * @param $nrows           - int
     * @param $searchTerm      - string
     * @param $studyId         - uuid
     * @param $nameLength      - limit for name
     * @param $catMaxnum       - int - maximum number of categories
     * @param $catstringLength - int - max-length of category name shown
     *
     * @return false|object
     */
    public function getStudyvariableconditionsPaginated($page, $nrows, $searchTerm, $studyId, int $nameLength = 20, int $catMaxnum = 50, int $catstringLength = 50)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $countQueryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $countQueryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["study_id"] = $studyId;
        $queryParams["name_length"] = $nameLength;
        $queryParams["cat_maxnum"] = $catMaxnum;
        $queryParams["catstring_length"] = $catstringLength;
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition_paginated_search;
            $countQueryParams["search_term"] = $searchTerm;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $countQueryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * Update studyvariablecondition (including name and description)
     *
     * @param $studyvariableconditionId - uuid
     * @param $studyId                  - uuid
     * @param $inData                   - array
     *
     * @return bool
     */
    public function updateStudyvariablcondition($studyvariableconditionId, $studyId, $inData)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->update_studyvariablecondition;

            $state = $this->db->execute($sql, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "conditionoperator_id"      => $inData["conditionoperator_id"],
                "txtval1"                   => $inData["value_a"],
                "txtval2"                   => $inData["value_b"],
                "name"                      => $inData["name"],
                "description"               => $inData["description"],
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update studyvariablecondition to default values
     * (don't change name and description)
     *
     * @param $studyvariableconditionId
     * @param $studyId
     * @param $inData
     *
     * @return bool
     */
    public function updateStudyvariablconditionReset($studyvariableconditionId, $studyId, $inData)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->update_studyvariablecondition_reset;

            $state = $this->db->execute($sql, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "conditionoperator_id"      => $inData["conditionoperator_id"],
                "txtval1"                   => $inData["value_a"],
                "txtval2"                   => $inData["value_b"],
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     *
     * @return array|false
     */
    public function removeAllStudyvariableconditionCategories($studyvariableconditionId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_all_studyvariablecondition_categories;

            $state = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $elementId
     * @param $value
     *
     * @return bool
     */
    public function addStudyvariableconditionCategory($studyvariableconditionId, $elementId, $value)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($elementId)) {
            $elementId = null;
        }
        $value = (string)$value;

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_studyvariablecondition_category;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "element_id"                => $elementId,
                "value_column"              => $value,
            ]);

            if (isset($result["added_category"]) && $result["added_category"] > 0) {
                return true;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $elementId
     * @param $value
     *
     * @return array|false
     */
    public function removeStudyvariableconditionCategory($studyvariableconditionId, $elementId, $value)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($elementId)) {
            $elementId = null;
        }
        $value = (string)$value;

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_studyvariablecondition_category;

            $state = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "element_id"                => $elementId,
                "value_column"              => $value,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $studyId
     * @param $limit
     *
     * @return array|false
     */
    public function studyvariableGetTypeInfo($studyvariableconditionId, $studyId, $limit)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->studyvariable_get_datatype;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyvariableSummaryNumeric($studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariable_summary_numeric;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "owner_org"                 => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $studyId
     * @param $breaksize
     *
     * @return array|false
     */
    public function getStudyvariableSummaryHistogram($studyvariableconditionId, $studyId, $breaksize)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        $breaksize = (float)$breaksize;
        if (!($breaksize > 0)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariable_summary_histogram;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
                "breaksize"                 => $breaksize,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyvariableSummaryCategorical($studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariable_summary_categrical;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     *
     * @return array|false
     */
    public function getStudyvariableconditionSelectedCategories($studyvariableconditionId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition_selected_categories;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $datatypeFlag
     *
     * @return array|false
     */
    public function getConditionoperators($datatypeFlag)
    {

        $datatypeFlag = (int)$datatypeFlag;
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_conditionoperators;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "datatype_flag" => $datatypeFlag,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableconditionId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyvariablecondition($studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariablecondition;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyId
     *
     * @return false|mixed
     */
    public function createStudyrowset($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->create_studyrowset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
            ]);

            if (isset($result["studyrowset_id"])) {
                return $result["studyrowset_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyrowsetInfo($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyrowset_info;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return bool
     */
    public function updateStudyrowset($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->update_studyrowset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
                "name"           => $this->request->getPost("name"),
                "description"    => $this->request->getPost("description"),
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return false|mixed
     */
    public function createStudyrowsubset($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->create_studyrowsubset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if (isset($result["studyrowsubset_id"])) {
                return $result["studyrowsubset_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsubsetId
     * @param $studyrowsetId
     * @param $studyId
     * @param $subsetname
     *
     * @return bool
     */
    public function updateStudyrowsubsetNames($studyrowsubsetId, $studyrowsetId, $studyId, $subsetname)
    {

        if (!UniqueId::uuidValidate($studyrowsubsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        echo "";

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->update_studyrowsubset_name;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "name"              => $subsetname,
                "studyrowsubset_id" => $studyrowsubsetId,
                "studyrowset_id"    => $studyrowsetId,
                "study_id"          => $studyId,
                "owner_org"         => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsubsetId
     * @param $studyId
     *
     * @return bool
     */
    public function deleteStudyrowsubset($studyrowsubsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsubsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->delete_studyrowsubset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyrowsubset_id" => $studyrowsubsetId,
                "study_id"          => $studyId,
                "owner_org"         => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyrowsubsetByStudyrowset($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyrowsubsets_by_studyrowset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyvariableconditionsForSubset($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyvariableconditions_for_subset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsubsetId
     * @param $studyvariableconditionId
     * @param $studyId
     *
     * @return bool
     */
    public function addStudyvariableconditionToSubset($studyrowsubsetId, $studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsubsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_studyvariableconditions_to_subset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyrowsubset_id"         => $studyrowsubsetId,
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsubsetId
     * @param $studyvariableconditionId
     * @param $studyId
     *
     * @return bool
     */
    public function removeStudyvariableconditionFromSubset($studyrowsubsetId, $studyvariableconditionId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsubsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_studyvariableconditions_from_subset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyrowsubset_id"         => $studyrowsubsetId,
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "owner_org"                 => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return false|mixed
     */
    public function studyrowsetStudyrowCount($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->studyrowset_studyrow_count;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if (isset($result["n"])) {
                return $result["n"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function listStudyrowsets($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->list_studyrowsets;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyrowsetsPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return bool
     */
    public function deleteStudyrowset($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->delete_studyrowset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyId
     *
     * @return false|mixed
     */
    public function createStudyvariableset($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->create_studyvariableset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
                "name"        => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
            ]);
            if (isset($result["studyvariableset_id"])) {
                return $result["studyvariableset_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return bool
     */
    public function deleteStudyvariableset($studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->delete_studyvariableset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableId
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return false|int
     */
    public function addSingleStudyvariableToStudyvarset($studyvariableId, $studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariableId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_single_studyvariable_to_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $this->db->execute($sql, [
                "studyvariable_id"    => $studyvariableId,
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);
            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return bool
     */
    public function addMultipleStudyvariableToStudyvarset($studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_multiple_studyvariable_to_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $studyvariableIds = $this->request->getPost("varid");
            $studyvariableIdArray = "{" . join(",", $studyvariableIds) . "}";

            $state = $this->db->execute($sql, [
                "studyvariable_id_array" => $studyvariableIdArray,
                "studyvariableset_id"    => $studyvariablesetId,
                "study_id"               => $studyId,
                "owner_org"              => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     * @param $searchTerm
     *
     * @return bool
     */
    public function addFilteredStudyvariableToStudyvarset($studyvariablesetId, $studyId, $searchTerm)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_filtered_studyvariable_to_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "search_term"         => $searchTerm,
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return false|int
     */
    public function addAllStudyvariableToStudyvarset($studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->add_all_studyvariable_to_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $this->db->execute($sql, [
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);
            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariableId
     * @param $studyId
     * @param $studyvariablesetId
     *
     * @return false|int
     */
    public function removeStudyvariableFromStudyvarset($studyvariableId, $studyId, $studyvariablesetId)
    {

        if (!UniqueId::uuidValidate($studyvariableId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_single_studyvariable_from_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $this->db->execute($sql, [
                "studyvariable_id"    => $studyvariableId,
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);
            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return bool
     */
    public function removeMultipleStudyvariableFromStudyvarset($studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_multiple_studyvariable_from_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $studyvariableIds = $this->request->getPost("varid");
            $studyvariableIdArray = "{" . join(",", $studyvariableIds) . "}";

            $state = $this->db->execute($sql, [
                "studyvariable_id_array" => $studyvariableIdArray,
                "studyvariableset_id"    => $studyvariablesetId,
                "study_id"               => $studyId,
                "owner_org"              => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     * @param $searchTerm
     *
     * @return bool
     */
    public function removeFilteredStudyvariableFromStudyvarset($studyvariablesetId, $studyId, $searchTerm)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_filtered_studyvariable_from_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "search_term"         => $searchTerm,
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return bool
     */
    public function removeAllStudyvariableFromStudyvarset($studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_all_studyvariable_from_studyvarset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyvariableset_id" => $studyvariablesetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        if ($this->db->isUnderTransaction()) {
            $this->db->rollback();
        }

        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyvariablesetsPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $studyId
     * @param $studyvariablesetId
     *
     * @return false|mixed
     */
    public function studyHasStudyvarset($studyId, $studyvariablesetId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->study_has_studyvariableset;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"            => $studyId,
                "studyvariableset_id" => $studyvariablesetId
            ]);
            if (isset($result["has_studyvariableset"])) {
                return $result["has_studyvariableset"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyvarsetUnselectedVarsPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyvariablesetId = $this->localsession->getKeyValue("dashin_owner_studyvariableset_id");
        $queryParams["studyvariableset_id"] = $studyvariablesetId;
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_unselected_vars_search_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_unselected_vars_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_unselected_vars_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_unselected_vars_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyvarsetSelectedVarsPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyvariablesetId = $this->localsession->getKeyValue("dashin_owner_studyvariableset_id");
        $queryParams["studyvariableset_id"] = $studyvariablesetId;
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_selected_vars_search_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_selected_vars_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_selected_vars_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvarset_selected_vars_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyexportsetInfo($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyexportset_info;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);

            if ($result) {

                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyId
     *
     * @return false|mixed
     */
    public function createStudyexportset($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->create_studyexportset;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "owner_org"   => $organizationId,
                "name"        => substr($this->request->getPost("name"), 0, 500),
                "description" => substr($this->request->getPost("description"), 0, 5000),
            ]);

            if (isset($result["studyexportset_id"])) {
                return $result["studyexportset_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return bool
     */
    public function updateStudyexportset($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->update_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "owner_org"         => $organizationId,
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "name"              => substr($this->request->getPost("name"), 0, 500),
                "description"       => substr($this->request->getPost("description"), 0, 5000),
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return false|int
     */
    public function setStudyrowsetOnStudyexportset($studyexportsetId, $studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->set_studyrowset_on_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"          => $studyId,
                "studyrowset_id"    => $studyrowsetId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);

            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyvariablesetId
     * @param $studyId
     *
     * @return false|int
     */
    public function setStudyvariuablesetOnStudyexportset($studyexportsetId, $studyvariablesetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyvariablesetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->set_studyvariableset_on_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"            => $studyId,
                "studyvariableset_id" => $studyvariablesetId,
                "studyexportset_id"   => $studyexportsetId,
                "owner_org"           => $organizationId,
            ]);

            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return false|int
     */
    public function removeStudyrowsetFromStudyexportset($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_studyrowset_from_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);

            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return false|int
     */
    public function removeStudyvariuablesetFromStudyexportset($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->remove_studyvariableset_from_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);

            $ok = $this->db->affectedRows();

            return $ok;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyrowsetsForStudyexportsetPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyexportsetId = $this->localsession->getKeyValue("dashin_owner_studyexportset_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["studyexportset_id"] = $studyexportsetId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_for_studyexportset_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_for_studyexportset_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_for_studyexportset_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyrowsets_for_studyexportset_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyvariablesetsForStudyexportsetPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyexportsetId = $this->localsession->getKeyValue("dashin_owner_studyexportset_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["studyexportset_id"] = $studyexportsetId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_for_studyexportset_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_for_studyexportset_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_for_studyexportset_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyvariablesets_for_studyexportset_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyexportsetViewInfo($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyexportset_info;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyrowsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyrowsetMinimal($studyrowsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyrowsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyrowset_info;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "studyrowset_id" => $studyrowsetId,
                "study_id"       => $studyId,
                "owner_org"      => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getStudyexportsetsPaginated($page, $nrows, $searchTerm)
    {

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $queryParams["study_id"] = $studyId;
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyexportsets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyexportsets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studyexport->get_studyexportsets_count;
            $itemQuery = $this->dashin_config->sql->owner_studyexport->get_studyexportsets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return bool
     */
    public function deleteStudyexportset($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studyexport->delete_studyexportset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "studyvariableset_id" => $studyexportsetId,
                "study_id"            => $studyId,
                "owner_org"           => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     * @param $sep
     *
     * @return array|false
     */
    public function getStudyexportsetDataQueries($studyexportsetId, $studyId, $sep = ",")
    {

        error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);
        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);
        try {
            $sql = $this->dashin_config->sql->owner_studyexport->get_studyexportset_data_queries;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
                "sep"               => $sep,
            ]);
            error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);

            if ($result) {
                error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);

                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
            error_log("DEBUG_STUDYEXPORT: " . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $studyexportsetId
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyexportsetSingleDataQuery($studyexportsetId, $studyId)
    {

        if (!UniqueId::uuidValidate($studyexportsetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);
            $sql = StudyexportQuery::$get_studyexportset_single_data_query;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"          => $studyId,
                "studyexportset_id" => $studyexportsetId,
                "owner_org"         => $organizationId,
            ]);
            error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);

            if ($result) {
                error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__);

                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
            error_log("DEBUG_STUDYEXPORT: " . __FILE__ . ":" . __LINE__ . " error: " . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $query
     *
     * @return array|false
     */
    public function getStudyexportsetData($query)
    {

        try {
            $result = $this->db->fetchAll($query);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $query
     *
     * @return array|false
     */
    public function getStudyexportsetDataSingle($query)
    {

        try {
            $result = $this->db->fetchAll($query, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get studydatatable for wholestudy export
     * Creates a table of studydatatable where each row is in a single jsonb format
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function wholestudyexportGetStudydatatable($studyId)
    {

        try {
            $sql = StudyexportQuery::$wholestudyexport_get_studydatatable_query;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"    => $studyId,
                "ownerorg_id" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get summary of all design variables
     * 
     * @param $studyId
     *
     * @return array|false
     */
    public function exportDesignvarSummary($studyId)
    {

        try {
            $sql = StudyexportQuery::$export_designvar_summary;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Generic function to return named sql-queries for wholestudy export
     * 
     * @param $queryname
     * @param $schemaname
     *
     * @return false|mixed
     */
    private function _returnExportTableSql($queryname, $schemaname)
    {

        try {
            $sql = ExportStudySqdrQuery::${"export_sqdr_{$queryname}_sql"};
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "schemaname" => $schemaname
            ]);

            if (isset($result["sqlquery"])) {
                return $result["sqlquery"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

//    /**
//     * wholestudy export to zip-file
//     * 
//     * @param $schemaname
//     * @param $queryname
//     *
//     * @return array|false
//     */
//    public function exportSqdr($schemaname, $queryname)
//    {
//
//        try {
//
//            $sql = $this->_returnExportTableSql($queryname, $schemaname);
//
//            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
//
//            if ($result) {
//                return $result;
//            }
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//
//        return false;
//    }
    
    public function exportSqdrSupportfiles($studyId)
    {

        try {
            $sql = ExportStudySqdrQuery::$export_sqdr_supportfile_paths;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}