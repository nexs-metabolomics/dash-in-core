<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class StudydataRepository extends RepositoryBase
{
    public function getStudydataset($datasetId, $studyId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydata->get_studydataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetId,
                "study_id"   => $studyId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    #--------------------------------------------------------------------------------
    public function removeDatasetFromStudy($datasetId, $studyId)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_studydata->remove_dataset_from_study;

            $state = $this->db->execute($sql, [
                "dataset_id" => $datasetId,
                "study_id"   => $studyId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getPotentialDatasetsPaginated($page, $nrows, $searchTerm, $nodups)
    {
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studydata->potential_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_studydata->potential_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studydata->potential_datasets_count;
            $itemQuery = $this->dashin_config->sql->owner_studydata->potential_datasets_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;

    }

    /**
     * Get paginated data rows
     *
     * @param $datasetId
     * @param $rowStart
     * @param $rowLimit
     * @param $colStart
     * @param $colLimit
     * @return array|bool
     */
    public function getDatarowsPaginated($datasetId, $studyId, $rowStart, $rowLimit, $colStart, $colLimit)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        # query params
        $organizationId = $this->SU->getActiveManagerOrgId();
        $countParms = ["dataset_id" => $datasetId];
        $countParms["owner_org"] = $organizationId;
        $countParms["study_id"] = $studyId;

        # columns
        $countColsSql = $this->dashin_config->sql->owner_studydata->columns_paginated_count;
        $colParams = $this->_preparePagination($countColsSql, $countParms, $colStart, $colLimit);

        # rows
        $countRowsSql = $this->dashin_config->sql->owner_studydata->rows_paginated_count;
        $rowParams = $this->_preparePagination($countRowsSql, $countParms, $rowStart, $rowLimit);

        try {

            # column items
            $sql = $this->dashin_config->sql->owner_studydata->column_items_paginated;
            $datacols = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "study_id"   => $studyId,
                "offset"     => $colParams["offset"],
                "limit"      => $colParams["limit"],
                "owner_org"  => $organizationId,
            ]);

            # alignment
            $datatypes = array_column($datacols, "datatype_id");

            # row query
            $queryLines = implode(",\n", array_column($datacols, "query_line"));
            unset($datacols["query_line"]);
            $datarowQuery =
                "SELECT datarow_id, ordinal,\n" .
                "json_build_array($queryLines) AS datarow\n" .
                "FROM dashin.datatable dt\n" .
                "WHERE dataset_id = :dataset_id\n" .
                "AND EXISTS (SELECT 1 FROM dashin.dataset d WHERE (d.dataset_id,d.owner_org) = (:dataset_id,:owner_org))\n" .
                "ORDER BY ordinal LIMIT :limit OFFSET :offset";

            # finalize column pagination object
            $colParams["items"] = $datacols;
            $colParams = (object)$colParams;

            # row items
            $datarows = $this->db->fetchAll($datarowQuery, Enum::FETCH_OBJ, [
                "dataset_id" => $datasetId,
                "offset"     => $rowParams["offset"],
                "limit"      => $rowParams["limit"],
                "owner_org"  => $organizationId,
            ]);

            foreach ($datarows as &$row) {
                $tmp = json_decode($row->datarow);
                $tmp2 = [];
                for ($i = 0, $imax = count($tmp); $i < $imax; ++$i) {
                    $tmp2[$i] = (object)["v" => $tmp[$i], "t" => $datatypes[$i]];
                }
                $row->datarow = (object)$tmp2;
            }
            unset($row);

            # row pagination object
            $rowParams["items"] = $datarows;
            $rowParams = (object)$rowParams;

            return ["datarows" => $rowParams, "datacols" => $colParams];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studydesignValidateData($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->validate_data_for_import;

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studyAddDataset($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->studydata_add_dataset;

            $result = $this->db->fetchAll($sql, Enum::FETCH_OBJ, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function studyRemoveData($studyId, $datasetId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = $this->dashin_config->sql->owner_studydesign->studydata_remove_dataset;

            $state = $this->db->execute($sql, [
                "study_id"   => $studyId,
                "dataset_id" => $datasetId,
                "owner_org"  => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getStudyrowsetsPaginated($page, $nrows, $searchTerm)
    {
        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = $this->dashin_config->sql->owner_studydata->get_studyrowset_search_count;
            $itemQuery = $this->dashin_config->sql->owner_studydata->get_studyrowset_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_studydata->get_studyrowset_count;
            $itemQuery = $this->dashin_config->sql->owner_studydata->get_studyrowset_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );
        return $outParams;
    }

    /**
     * Update studyvariablecondition (only name and description)
     *
     * When no condition is present
     *
     * @param $studyvariableconditionId
     * @param $studyId
     * @param $inData
     * @return bool
     */
    public function updateStudyvariablconditionNameDescription($studyvariableconditionId, $studyId, $inData)
    {
        if (!UniqueId::uuidValidate($studyvariableconditionId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_studydata->update_studyvariablecondition_name_description;

            $state = $this->db->execute($sql, [
                "studyvariablecondition_id" => $studyvariableconditionId,
                "study_id"                  => $studyId,
                "name"                      => $inData["name"],
                "description"               => $inData["description"],
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

}