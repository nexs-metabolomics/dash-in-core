<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\Csv;
use App\Library\Utils\StudyExporter;
use App\Library\Utils\UniqueId;
use Dashin\Forms\Owner\StudycontactForm;
use Dashin\Forms\Owner\StudyCreateForm;
use Dashin\Forms\Owner\StudyEditGeneralForm;
use Dashin\Forms\Owner\StudyEditNameDescription;
use Dashin\Forms\Owner\StudyEditResearchdesignForm;
use Dashin\Forms\Owner\StudyImportSqdrForm;
use Dashin\Forms\Owner\SupportFileEditForm;
use Dashin\Forms\Owner\UploadSupportFileForm;
use Dashin\Helpers\HelperBase;

class StudyHelper extends HelperBase
{

    /**
     * @var StudyRepository
     */
    private $_repository;

    /**
     * @return StudyRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new StudyRepository();
        }

        return $this->_repository;
    }

    /**
     * Get list of countries in a format suitable for Select form element
     *
     * @param $noopHeader
     *
     * @return array
     */
    private function _getCountryChoices($noopHeader = null)
    {

        $data = $this->_getRepository()->getCountriesMinimal();
        $keys = array_column($data, "country_id");
        $values = array_column($data, "short_name");
        if ($noopHeader) {
            array_unshift($keys, "");
            array_unshift($values, $noopHeader);
        }
        $out = array_combine($keys, $values);

        return $out;
    }

    /**
     * Get list of research designs in format suitable for Select form element
     *
     * @param $noopHeader
     *
     * @return array
     */
    private function _getResearchdesignChoices($noopHeader = null)
    {

        $data = $this->_getRepository()->getResearchdesignsMinimal();
        $keys = array_column($data, "researchdesign_id");
        $values = array_column($data, "name");
        if ($noopHeader) {
            array_unshift($keys, "");
            array_unshift($values, $noopHeader);
        }
        $out = array_combine($keys, $values);

        return $out;
    }

    /**
     * Get list of consortia in a format suitable for Select form element
     *
     * @param $noopHeader
     *
     * @return array
     */
    private function _getConsortiumChoices($noopHeader = null)
    {

        $data = $this->_getRepository()->getConsortiumsMinimal();
        $keys = array_column($data, "consortium_id");
        $values = array_column($data, "name");
        if ($noopHeader) {
            array_unshift($keys, "");
            array_unshift($values, $noopHeader);
        }
        $out = array_combine($keys, $values);

        return $out;
    }

    /**
     * Create a new study
     * Only the basic is created
     *
     * @return StudyCreateForm[]|false
     */
    public function createStudy()
    {

        $form = new StudyCreateForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $inData = $this->request->getPost();
                $studyId = $this->_getRepository()->createStudy($inData);
                if ($studyId) {
                    $this->localsession->setKeyValue("dashin_owner_study_id", $studyId);
                    $querystring = $this->localsession->getQuerystring("?");
                    $this->response->redirect("/dashin/owner/study/editgeneral/$querystring");
                    $this->response->send();

                    return false;
                }
            }
        }

        return [
            "form" => $form,
        ];
    }

    private function _moveUploadedFile()
    {
        if ($this->request->hasFiles()) {
            # get tempdir
            $tempdir = $this->config->tempdir;

            $file = $this->request->getUploadedFiles(true)[0];
            $uploadedFileName = UniqueId::uuid4();
            $uploadedFilePath = $tempdir . DIRECTORY_SEPARATOR . $uploadedFileName;
            $uploadOk = move_uploaded_file($file->getTempName(), $uploadedFilePath);
            $studyImporter = new StudyExporter();

            $studyImporter->importStudy($uploadedFilePath);

        }
    }

    public function importSqdr()
    {
        $form = new StudyImportSqdrForm();
        $action = $this->Btn->getAction();
        if ($action === "submit") {

            $uploadedFileInfo = $this->_moveUploadedFile();
            if ($uploadedFileInfo) {
                $this->localsession->setKeyValue("dashin_owner_importsqdr_uploaded_file", $uploadedFileInfo);
                $this->response->redirect("/dashin/owner/study/unpacksqdr/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        }

        return [
            "form" => $form,
        ];
    }

    /**
     * Edit/update study -  the main menu
     *
     * @param $studyId
     *
     * @return array
     */
    public function editGeneral($studyId)
    {

        $option["country"] = $this->_getCountryChoices("Select country");
        $option["consortium"] = $this->_getConsortiumChoices("Select consortium");
        $option["researchdesign"] = $this->_getResearchdesignChoices("Select study design");

        $formNamedesc = new StudyEditNameDescription();
        $formGeneral = new StudyEditGeneralForm($option);
        $formStudydesign = new StudyEditResearchdesignForm($option);

        if ($this->request->isPost()) {
            $formNamedescOk = false;
            $formGeneralOk = false;
            $formStudydesignOk = true;

            $post = $this->request->getPost();

            if (!$formNamedesc->isValid($post)) {
                foreach ($formNamedesc->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $formNamedescOk = true;
            }

            if (!$formGeneral->isValid($this->request->getPost())) {
                foreach ($formGeneral->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $formGeneralOk = true;
            }

            if (!$formStudydesign->isValid($post)) {
                foreach ($formStudydesign->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $formStudydesignOk = true;
            }

            if ($formNamedescOk && $formGeneralOk && $formStudydesignOk) {
                $ok = $this->_getRepository()->updateGeneral($studyId, $post);
            }
        }
        $action = $this->Btn->getAction();
        if ($action === "submit") {
            $this->response->redirect("/dashin/owner/study/summary/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        }

        $data = $this->_getRepository()->getGeneralForForm($studyId);
        if ($data) {
            $formNamedesc->bindValues($data);

            $formGeneral->bindValues($data);
            if ($formGeneral->has("researchdesign")) {
                $formGeneral->get("researchdesign")->setDefault($data["researchdesign"]);
            }
            if ($formGeneral->has("blinding")) {
                if ($data["blinding"]) {
                    $formGeneral->get("blinding")->setDefault("checked");
                    # $formGeneral->get("blinding")->setAttribute("checked",true);
                }
            }
            if ($formGeneral->has("consortium")) {
                if ($data["consortium_id"]) {
                    $formGeneral->get("consortium")->setDefault($data["consortium_id"]);
                    # $formGeneral->get("blinding")->setAttribute("checked",true);
                }
            }
            if ($formGeneral->has("country")) {
                if ($data["country_id"]) {
                    $formGeneral->get("country")->setDefault($data["country_id"]);
                    # $formGeneral->get("blinding")->setAttribute("checked",true);
                }
            }
            $formStudydesign->bindValues($data);
            if ($formStudydesign->has("researchdesign")) {
                $formStudydesign->get("researchdesign")->setDefault($data["researchdesign_id"]);
            }
            if ($formStudydesign->has("blinding")) {
                if ($data["blinding"]) {

                    #-------------------------------------------------------------------
                    # phalcon-bug?:
                    # HACK to make phalcon render checkbox correctly:
                    # both value=checked and checked=checked must be set
                    #-------------------------------------------------------------------
                    $formStudydesign->get("blinding")->setAttribute("value", "checked");
                    $formStudydesign->get("blinding")->setAttribute("checked", "checked");
                }
            }
        } else {
            $formNamedesc = false;
            $formGeneral = false;
            $formStudydesign = false;
            $data = false;
        }

        return [
            "form_namedesc"    => $formNamedesc,
            "form_general"     => $formGeneral,
            "form_studydesign" => $formStudydesign,
            "study_info"       => $data,
        ];
    }

    private function _studyToggle($studyId, &$studyStatus)
    {

        $msg = [];
        if ($studyStatus["study_is_public"] === 1) {
            $this->setKeyValue("dashin_owner_study_toggle_public_off", $studyId);
            $studyStatus["study_toggle"] = "off";

            # check dependent objects
            if ($studyStatus["datasets_are_public"]) {
                $msg[] = "Some datasets are public, they will be set to private";
            }

            if ($studyStatus["supportfiles_are_public"]) {
                $msg[] = "Some files are public, they will be set to private";
            }

        } elseif ($studyStatus["study_is_public"] === 0) {

            $this->setKeyValue("dashin_owner_study_toggle_public_on", $studyId);
            $studyStatus["study_toggle"] = "on";
        }
        $studyStatus["toggle_msg"] = $msg;
    }

    private function _studySetAllPublic($studyId, $studyStatus)
    {
    }

    /**
     * Set permission on study
     *
     * @param $studyId
     * @param $studyStatus
     *
     * @return array|mixed
     */
    public function setStudyPermissionStatus($studyId, $studyStatus)
    {

        $action = $this->Btn->getAction();

        #---------------------------------------------------------
        # study public
        #---------------------------------------------------------
        if ($action === "study_set_public") {

            $this->setKeyValue("dashin_owner_study_set_public", $studyId);
            $studyStatus["public_confirm"] = 1;

        }
        elseif ($action === "study_set_public_cancel")
        {

            $this->removeKeyValue("dashin_owner_study_set_public");

        }
        elseif ($action === "study_public_confirm")
        {

            $savedStudyId = $this->getKeyValue("dashin_owner_study_set_public");
            if ($studyId === $savedStudyId) {
                $this->_getRepository()->setStudyPublicStatus($studyId, "on");

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_study_set_public");

        }

        #---------------------------------------------------------
        # study private (= study all private)
        #---------------------------------------------------------
        elseif ($action === "study_set_private")
        {

            $this->setKeyValue("dashin_owner_study_set_private", $studyId);
            $studyStatus["private_confirm"] = 1;

        }
        elseif ($action === "study_private_confirm")
        {

            $savedStudyId = $this->getKeyValue("dashin_owner_study_set_private");
            if ($studyId === $savedStudyId) {
                $this->_getRepository()->setStudyPublicStatus($studyId, "off");

                if (isset($studyStatus["datasets"])) {
                    foreach ($studyStatus["datasets"] as $datasetId => $dataset) {
                        $this->_getRepository()->setDatasetPublicStatus($datasetId, $studyId, "off");
                    }
                }
                if (isset($studyStatus["supportfiles"])) {
                    foreach ($studyStatus["supportfiles"] as $supportfileId => $supportfile) {
                        $this->_getRepository()->setSupportfilePublicStatus($supportfileId, $studyId, "off");
                    }
                }

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_study_set_private");
        }

        #---------------------------------------------------------
        # study all public
        #---------------------------------------------------------
        elseif  ($action === "study_all_public")
        {

            $this->setKeyValue("dashin_owner_study_all_public", $studyId);
            $studyStatus["study_all_public_confirm"] = 1;

        }
        elseif  ($action === "study_all_public_cancel")
        {

            $this->removeKeyValue("dashin_owner_study_all_public");

        }
        elseif ($action === "study_all_public_confirm")
        {

            $savedStudyId = $this->getKeyValue("dashin_owner_study_all_public");
            if ($studyId === $savedStudyId) {
                $this->_getRepository()->setStudyPublicStatus($studyId, "on");

                if (isset($studyStatus["datasets"])) {
                    foreach ($studyStatus["datasets"] as $datasetId => $dataset) {
                        $this->_getRepository()->setDatasetPublicStatus($datasetId, $studyId, "on");
                    }
                }
                if (isset($studyStatus["supportfiles"])) {
                    foreach ($studyStatus["supportfiles"] as $supportfileId => $supportfile) {
                        $this->_getRepository()->setSupportfilePublicStatus($supportfileId, $studyId, "on");
                    }
                }

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_study_all_public");
        }

        return $studyStatus;
    }

    /**
     * @param $studyId
     * @param $studyStatus
     *
     * @return false|mixed
     */
    public function setDatasetPermissionStatus($studyId, $studyStatus)
    {
        if ($studyStatus["study_is_public"] !== 1) {
            return $studyStatus;
        }

        $action = $this->Btn->getAction();
        if ($action === "dataset_set_public") {
            $datasetId = $this->Btn->getValue();

            $this->setKeyValue("dashin_owner_dataset_set_public", $datasetId);

            $studyStatus["datasets"]->{$datasetId}->public_confirm = 1;

        } elseif ($action === "dataset_set_public_cancel") {

            $this->removeKeyValue("dashin_owner_dataset_set_public");

        } elseif ($action === "dataset_public_confirm") {
            $datasetId = $this->Btn->getValue();

            $savedDatasetId = $this->getKeyValue("dashin_owner_dataset_set_public");
            if ($datasetId === $savedDatasetId) {
                $this->_getRepository()->setDatasetPublicStatus($datasetId, $studyId, "on");

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_dataset_set_public");

        } elseif ($action === "dataset_set_private") {
            $datasetId = $this->Btn->getValue();

            $this->setKeyValue("dashin_owner_dataset_set_private", $datasetId);

            $studyStatus["datasets"]->{$datasetId}->private_confirm = 1;

        } elseif ($action === "dataset_set_private_cancel") {
            $datasetId = $this->Btn->getValue();

            $this->removeKeyValue("dashin_owner_dataset_set_private");

        } elseif ($action === "dataset_private_confirm") {
            $datasetId = $this->Btn->getValue();

            $savedDatasetId = $this->getKeyValue("dashin_owner_dataset_set_private");
            if ($datasetId === $savedDatasetId) {
                $this->_getRepository()->setDatasetPublicStatus($datasetId, $studyId, "off");

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_dataset_set_private");
        }

        return $studyStatus;
    }

    public function setSupportfilePermissionStatus($studyId, $studyStatus)
    {

        if ($studyStatus["study_is_public"] !== 1) {
            return $studyStatus;
        }

        $action = $this->Btn->getAction();
        if ($action === "supportfile_set_public") {
            $supportfileId = $this->Btn->getValue();

            $this->setKeyValue("dashin_owner_supportfile_set_public", $supportfileId);
            $studyStatus["supportfiles"]->{$supportfileId}->public_confirm = 1;

        } elseif ($action === "supportfile_public_confirm") {
            $supportfileId = $this->Btn->getValue();

            $savedsupportfileId = $this->getKeyValue("dashin_owner_supportfile_set_public");
            if ($supportfileId === $savedsupportfileId) {
                $this->_getRepository()->setSupportfilePublicStatus($supportfileId, $studyId, "on");

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_supportfile_set_public");

        } elseif ($action === "supportfile_public_cancel") {

            $this->removeKeyValue("dashin_owner_supportfile_set_public");

        } elseif ($action === "supportfile_set_private") {
            $supportfileId = $this->Btn->getValue();

            $this->setKeyValue("dashin_owner_supportfile_set_private", $supportfileId);
            $studyStatus["supportfiles"]->{$supportfileId}->private_confirm = 1;

        } elseif ($action === "supportfile_private_confirm") {
            $supportfileId = $this->Btn->getValue();

            $savedSupportfileId = $this->getKeyValue("dashin_owner_supportfile_set_private");
            if ($supportfileId === $savedSupportfileId) {
                $this->_getRepository()->setSupportfilePublicStatus($supportfileId, $studyId, "off");

                $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
            }
            $this->removeKeyValue("dashin_owner_supportfile_set_private");

        } elseif ($action === "supportfile_private_cancel") {

            $this->removeKeyValue("dashin_owner_supportfile_set_private");
        }

        return $studyStatus;
    }

    public function editPermissions($studyId)
    {

        $studyStatus = $this->_getRepository()->getStudyPermissionStatus($studyId);
        $studyStatus = $this->setStudyPermissionStatus($studyId, $studyStatus);
        $studyStatus = $this->setDatasetPermissionStatus($studyId, $studyStatus);
        $studyStatus = $this->setSupportfilePermissionStatus($studyId, $studyStatus);

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_status" => $studyStatus,
            "study_info"   => $studyInfo,
        ];
    }

    /**
     * List all studies within an organization
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array|false
     */
    public function listStudies($page, $nrows, $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->response->redirect("/dashin/owner/study/list#$studyId/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
            }
            $this->removeKeyValue("dashin_owner_delete_study");
            $this->localsession->removeKeyValue("dashin_owner_study_id");

            return false;
        } elseif ($action === "delete") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->setKeyValue("dashin_owner_delete_study", $studyId);
            } else {
                $this->removeKeyValue("dashin_owner_delete_study");
            }
        } elseif ($action === "confirm") {
            $studyId = $this->Btn->getValue();
            $studyIdSaved = $this->getKeyValue("dashin_owner_delete_study");
            if ($studyId === $studyIdSaved) {
                $this->_getRepository()->deleteStudy($studyId);
            }
            $this->removeKeyValue("dashin_owner_delete_study");
            $this->localsession->removeKeyValue("dashin_owner_study_id");
            $studyId = false;
        } elseif ($action === "edit") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->localsession->setKeyValue("dashin_owner_study_id", $studyId);
                $this->response->redirect("/dashin/owner/study/editgeneral/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
            $this->removeKeyValue("dashin_owner_delete_study");
            $this->localsession->removeKeyValue("dashin_owner_study_id");
            $studyId = false;
        } elseif ($action === "view") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->localsession->setKeyValue("dashin_owner_study_id", $studyId);
                $this->response->redirect("/dashin/owner/study/summary/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
            $this->removeKeyValue("dashin_owner_delete_study");
            $this->localsession->removeKeyValue("dashin_owner_study_id");
            $studyId = false;
        } elseif ($action === "downloadstructure") {
            $studyId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($studyId)) {
                $this->_downloadStudyStructure($studyId);
            }
            $this->removeKeyValue("dashin_owner_delete_study");
            $this->localsession->removeKeyValue("dashin_owner_study_id");
            $studyId = false;
        } else {
            $studyId = false;
        }

        $searchTerm = $this->getSearchTerm("dashin_owner_study_list_studies_search_term", $reset);
        $data = $this->_getRepository()->getStudiesPaginated($page, $nrows, $searchTerm);
        if ($data) {
            foreach ($data->items as &$item) {
                if ($item->study_id === $studyId) {
                    $item->confirm = true;
                } else {
                    $item->confirm = false;
                }
            }
        }

        return [
            "studies"     => $data,
            "search_term" => $searchTerm,
        ];
    }

    /**
     * Download the structure of a study as csv-file
     *
     * @param $studyId
     *
     * @return false|void
     */
    private function _downloadStudyStructure($studyId)
    {

        $data = $this->_getRepository()->getStudyStructureForDownload($studyId);
        if (!$data) {
            $this->flashSession->warning("There is no structure configured for this study");

            return false;
        }
        $Csv = new Csv();
        $header = array_keys(current($data));
        reset($data);
        $csvContents = $Csv->arrayToCsv($data, ";", $header);
        # push to download
        $this->downloader->send($csvContents, "study_structure.csv", "csv");
    }

    /**
     * Overview of a single study
     *
     * @param $studyId
     * @param $type
     *
     * @return array
     */
    public function studySummary($studyId, $type = null)
    {

        $action = $this->Btn->getAction();
        if ($action === "downloadstructure") {
            $this->_downloadStudyStructure($studyId);
        }
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studyGeneral = false;
        $studyDesign = false;
        $studycontacts = false;
        $studyDatasets = false;
        if ($type === "general") {
            $studyGeneral = $this->_getRepository()->getGeneralForView($studyId);
        } elseif ($type === "design") {
            $studyDesign = $this->_getRepository()->getStudyDesignForView($studyId);
        } elseif ($type === "contacts") {
            $studycontacts = $this->_getRepository()->getStudycontacts($studyId);
            if ($studycontacts) {
                foreach ($studycontacts as &$row) {
                    $row = (object)$row;
                }
                unset($row);
            }
        } elseif ($type === "datasets") {
            $studyDatasets = $this->_getRepository()->getStudyDatasetsForView($studyId);
            if ($studyDatasets) {
                foreach ($studyDatasets as &$row) {
                    $row = (object)$row;
                }
                unset($row);
            }
        }

        return [
            "study_info"     => $studyInfo,
            "study_general"  => $studyGeneral,
            "researchdesign" => $studyDesign,
            "studycontacts"  => $studycontacts,
            "study_datasets" => $studyDatasets,
        ];
    }

    /**
     * View the part of the study meta-data that relates the design of the sutdy
     *
     * @param $studyId
     *
     * @return array
     */
    public function viewStudyDesign($studyId)
    {

        $studyMinimal = $this->_getRepository()->getStudyInfo($studyId);
        $studyDesign = $this->_getRepository()->getStudyDesignForView($studyId);

        return [
            "study_minimal"  => $studyMinimal,
            "researchdesign" => $studyDesign,
        ];
    }

    /**
     * View list of datasets within a study
     *
     * @param $page
     * @param $nrows
     * @param $reset
     * @param $studyId
     *
     * @return array|false
     */
    public function viewDatasets($page, $nrows, $reset, $studyId)
    {

        $datasetId = false;
        $action = $this->Btn->getAction();
        if ($this->SU->allowWrite()) {
            if ($action === "remove") {
                $datasetId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($datasetId)) {
                    $this->setKeyValue("dashin_owner_delete_dataset", $datasetId);
                } else {
                    $this->removeKeyValue("dashin_owner_delete_dataset");
                }
            } elseif ($action === "confirm") {
                $datasetId = $this->Btn->getValue();
                $datasetIdSaved = $this->getKeyValue("dashin_owner_delete_dataset");
                if ($datasetId === $datasetIdSaved) {
                    $this->_getRepository()->removeDatasetFromStudy($datasetId, $studyId);
                }
                $this->removeKeyValue("dashin_owner_delete_dataset");
                $this->localsession->removeKeyValue("dashin_owner_dataset_id");
                $datasetId = false;
            }
        }
        if ($action === "cancel") {
            $datasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($datasetId)) {
                $this->response->redirect("/dashin/owner/study/viewdatasets/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
            }
            $this->removeKeyValue("dashin_owner_delete_dataset");
            $this->localsession->removeKeyValue("dashin_owner_dataset_id");

            return false;
        } elseif ($action === "browse") {
            $this->response->redirect("/dashin/owner/studydata/browsedata/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }
        $studyinfo = $this->_getRepository()->getStudyInfo($studyId);

        $searchTerm = $this->getSearchTerm("dashin_studydata_list_added_datasets_search_term", $reset);
        $data = $this->_getRepository()->getAddedDatasetsPaginated($page, $nrows, $searchTerm, $studyId);
        if ($data) {
            foreach ($data->items as &$item) {
                if ($item->dataset_id === $datasetId) {
                    $item->confirm = true;
                } else {
                    $item->confirm = false;
                }
            }
        }

        return [
            "search_term" => $searchTerm,
            "data"        => $data,
            "study_info"  => $studyinfo,
        ];
    }

    /**
     * Remove datasets from a study
     *
     * @param $page
     * @param $nrows
     * @param $reset
     * @param $studyId
     *
     * @return array|false
     */
    public function removeDatasets($page, $nrows, $reset, $studyId)
    {

        $datasetId = false;
        $action = $this->Btn->getAction();
        if ($this->SU->allowWrite()) {
            if ($action === "remove") {
                $datasetId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($datasetId)) {
                    $this->setKeyValue("dashin_owner_delete_dataset", $datasetId);
                } else {
                    $this->removeKeyValue("dashin_owner_delete_dataset");
                }
            } elseif ($action === "confirm") {
                $datasetId = $this->Btn->getValue();
                $datasetIdSaved = $this->getKeyValue("dashin_owner_delete_dataset");
                if ($datasetId === $datasetIdSaved) {
                    $this->_getRepository()->removeDatasetFromStudy($datasetId, $studyId);
                }
                $this->removeKeyValue("dashin_owner_delete_dataset");
                $this->localsession->removeKeyValue("dashin_owner_dataset_id");
                $datasetId = false;
            }
        }
        if ($action === "cancel") {
            $datasetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($datasetId)) {
                $this->response->redirect("/dashin/owner/study/removedatasets/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
            }
            $this->removeKeyValue("dashin_owner_delete_dataset");
            $this->localsession->removeKeyValue("dashin_owner_dataset_id");

            return false;
        } elseif ($action === "browse") {
            $this->response->redirect("/dashin/owner/studydata/browsedata/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }
        $studyinfo = $this->_getRepository()->getStudyInfo($studyId);

        $searchTerm = $this->getSearchTerm("dashin_studydata_list_added_datasets_search_term", $reset);
        $data = $this->_getRepository()->getAddedDatasetsPaginated($page, $nrows, $searchTerm, $studyId);
        if ($data) {
            foreach ($data->items as &$item) {
                if ($item->dataset_id === $datasetId) {
                    $item->confirm = true;
                } else {
                    $item->confirm = false;
                }
            }
        }

        return [
            "search_term" => $searchTerm,
            "data"        => $data,
            "study_info"  => $studyinfo,
        ];
    }

    /**
     * Create a new study contact
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function createStudcontact($studyId)
    {

        $action = $this->Btn->getAction();
        $studyRoles = $this->_getRepository()->getStudyRoles();
        $studyRoleOptions = array_combine(array_column($studyRoles, "study_role_id"), array_column($studyRoles, "name"));
        $form = new StudycontactForm(["study_role_options" => $studyRoleOptions]);
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $studycontactId = $this->_getRepository()->createStudycontact($studyId);
                if ($studycontactId) {
                    $this->localsession->setKeyValue("dashin_owner_studycontact_id", $studycontactId);
                    $this->response->redirect("/dashin/owner/study/liststudycontacts/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        return [
            "study_info" => $studyInfo,
            "form"       => $form,
        ];
    }

    /**
     * Edit a study contact
     *
     * @param $studyId
     * @param $studycontactId
     *
     * @return array
     */
    public function editStudcontact($studyId, $studycontactId)
    {

        $action = $this->Btn->getAction();
        $studyRoles = $this->_getRepository()->getStudyRoles();
        $studyRoleOptions = array_combine(array_column($studyRoles, "study_role_id"), array_column($studyRoles, "name"));
        $form = new StudycontactForm(["study_role_options" => $studyRoleOptions]);
        if ($action === "submit") {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $ok = $this->_getRepository()->updateStudycontact($studycontactId, $studyId);
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $studycontact = $this->_getRepository()->getStudycontact($studyId, $studycontactId);
        if ($studycontact) {
            $form->bindValues($studycontact);
            if ($form->has("study_role")) {
                $form->get("study_role")->setDefault($studycontact["study_role_id"]);
            }
        }

        return [
            "study_info"        => $studyInfo,
            "studycontact_info" => $studycontact,
            "form"              => $form,
        ];
    }

    /**
     * Return a list of study contacts within a study
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function listStudycontacts($studyId)
    {

        $studycontactId = null;
        $action = $this->Btn->getAction();
        if ($this->SU->allowWrite()) {
            if ($action === "delete") {
                $studycontactId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($studycontactId)) {
                    $this->setKeyValue("dashin_owner_delete_studycontact", $studycontactId);
                } else {
                    $this->removeKeyValue("dashin_owner_delete_studycontact");
                }
            } elseif ($action === "confirm") {
                $studycontactId = $this->Btn->getValue();
                $studycontactIdSaved = $this->getKeyValue("dashin_owner_delete_studycontact");
                if ($studycontactId === $studycontactIdSaved) {
                    $this->_getRepository()->deleteStudycontact($studyId, $studycontactId);
                }
                $this->removeKeyValue("dashin_owner_delete_studycontact");
                $this->localsession->removeKeyValue("dashin_owner_studycontact_id");
                $studycontactId = false;
            } elseif ($action === "edit") {
                $studycontactId = $this->Btn->getValue();
                if (UniqueId::uuidValidate($studycontactId)) {
                    $this->localsession->setKeyValue("dashin_owner_studycontact_id", $studycontactId);
                    $this->response->redirect("/dashin/owner/study/editstudycontact/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $data = $this->_getRepository()->getStudycontacts($studyId);
        if ($data) {
            foreach ($data as &$row) {
                if ($row->studycontact_id === $studycontactId) {
                    $row->confirm = true;
                } else {
                    $row->confirm = false;
                }
            }
            unset($row);
        }

        return [
            "studycontacts" => $data,
            "study_info"    => $studyInfo,
        ];
    }

    /**
     * Handle upload of study support file
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function supportFileUpload($studyId)
    {

        # get info on active study
        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        # handle possible file upload
        if ($this->supportFiles->handleUpload()) {

            $organizationId = $studyInfo["organization_id"] ?? false;

            # register uploaded file as a study supportfile
            $supportfileId = $this->supportFiles->saveAndRegister(["study_id" => $studyId, "organization_id" => $organizationId, "type" => "study"]);
            if (UniqueId::uuidValidate($supportfileId)) {

                # save to file-id as active file to localsession
                $this->localsession->setKeyValue("dashin_owner_study_supportfile_id", $supportfileId);

                # redirect to edit
                $this->response->redirect("/dashin/owner/study/fileedit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }

        }

        # form for file upload
        $form = new UploadSupportFileForm();

        return [
            "study_info" => $studyInfo,
            "form"       => $form,
        ];

    }

    /**
     * Edit support file description
     *
     * @param $studyId
     * @param $supportfileId
     *
     * @return array
     */
    public function supportFileEdit($studyId, $supportfileId)
    {

        $form = new SupportFileEditForm();

        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("dashin/owner/study/filelist/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        } elseif ($action === "submit" || $action === "apply") {
            if ($this->request->isPost()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                } else {
                    $description = $this->request->getPost("description");
                    $ok = $this->supportFiles->updateFileDescription($supportfileId, $description);
                    if ($ok) {
                        $this->flashSession->success("File description updated");
                        if ($action === "submit") {
                            $this->response->redirect("dashin/owner/study/filelist/{$this->localsession->getQuerystring('?')}");
                            $this->response->send();

                            return false;
                        }
                    }

                }
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);

        $supportfileInfo = $this->supportFiles->getSupportFileInfo($supportfileId);
        if ($supportfileInfo) {
            $form->bindValues($supportfileInfo);
        }

        return [
            "form"             => $form,
            "study_info"       => $studyInfo,
            "supportfile_info" => $supportfileInfo,
        ];
    }

    /**
     * List support files for the current study
     *
     * This dialog is also where support files are
     * deleted from current study
     *
     * @param $studyId
     *
     * @return array
     */
    public function supportFileList($studyId)
    {

        $supportfileId = false;
        $setConfirm = false;
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $setConfirm = true;
                $this->localsession->setKeyValue("dashin_owner_study_delete_support_file", $supportfileId);
            } else {
                $this->localsession->removeKeyValue("dashin_owner_study_delete_support_file");
            }
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        if ($action === "confirm") {
            $supportfileId = $this->Btn->getValue();
            $supportfileIdSaved = $this->localsession->getKeyValue("dashin_owner_study_delete_support_file");
            if ($supportfileId === $supportfileIdSaved) {
                $organizationId = $studyInfo["organization_id"] ?? false;
                $ok = $this->supportFiles->deleteFile(["organization_id" => $organizationId, "study_id" => $studyId, "type" => "study"], $supportfileId);
                if ($ok) {
                    $this->flashSession->success("File deleted");
                }
            }
            $this->localsession->removeKeyValue("dashin_owner_study_delete_support_file");

            # if the deleted file is the active file - remove from being active
            $supportfileId = $this->localsession->getKeyValue("dashin_owner_study_supportfile_id");
            if ($supportfileIdSaved === $supportfileId) {
                $this->localsession->removeKeyValue("dashin_owner_study_supportfile_id");
            }
        }
        if ($action === "edit") {

            $supportfileId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($supportfileId)) {
                $this->localsession->setKeyValue("dashin_owner_study_supportfile_id", $supportfileId);
                $querystring = $this->localsession->getQuerystring("?");
                $this->response->redirect("/dashin/owner/study/fileedit/$querystring");
                $this->response->send();

                return false;
            }

        }

        if ($action === "download") {

            $supportfileId = $this->Btn->getValue();
            $organizationId = $studyInfo["organization_id"] ?? false;
            $this->supportFiles->downloadSupportFile(["organization_id" => $organizationId, "study_id" => $studyId, "type" => "study"], $supportfileId);
        }

        $studyfileList = $this->supportFiles->getFileList(["study_id" => $studyId, "type" => "study"]);
        if ($studyfileList) {
            foreach ($studyfileList as &$row) {
                if ($setConfirm && $row["supportfile_id"] === $supportfileId) {
                    $row["confirm"] = true;
                } else {
                    $row["confirm"] = false;
                }
                $row = (object)$row;
            }
            unset($row);
        }

        return [
            "study_info"     => $studyInfo,
            "studyfile_list" => $studyfileList,
        ];
    }

    /**
     * Export a study to sqdr-format
     * 
     * @param $studyId
     *
     * @return void
     * @throws \App\Library\Utils\StorageException
     */
    public function studyExport($studyId)
    {
        
        $StudyExporter = new StudyExporter();
        $dbVersion = $StudyExporter->checkDbversion();
        if($dbVersion) {
            if(!$dbVersion["dbversion_ok"]) {
                $minVersion = $dbVersion["minimum_version"];
                $currentVersion = $dbVersion["version_string"];
                $this->flashSession->warning("This functionality is not available with your current database version ('{$currentVersion}').\nYou must upgrade to version '{$minVersion}' or greater.");
            }
        }
        $StudyExporter->exportStudy($studyId);
    }

}