<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Helpers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

/**
 * Handle assays
 *
 * Class AssayRepository
 *
 * @package Dashin\Helpers\Owner
 */
class AssayRepository extends RepositoryBase
{

    /**
     * Get list of research types
     *
     * @return array|false
     */
    public function getResearchfields()
    {

        try {
            $sql = AssayQuery::$get_researchfields;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create new assay
     *
     * @param $inData
     *
     * @return bool
     */
    public function createAssay($inData)
    {

        try {
            $sql = AssayQuery::$create_assay;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, $inData);
            if (isset($result["assay_id"])) {
                return $result["assay_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update assay
     *
     * @param $assayId
     *
     * @return bool
     */
    public function updateAssay($assayId)
    {

        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }

        try {
            $sql = AssayQuery::$update_assay;

            $result = $this->db->execute($sql, [
                "assay_id"         => $assayId,
                "name"             => $this->request->getPost("name"),
                "description"      => $this->request->getPost("description"),
                "researchfield_id" => $this->request->getPost("researchfield"),
            ]);

            return $result;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete assay
     *
     * @param $assayId
     *
     * @return bool
     */
    public function deleteAssay($assayId)
    {

        try {
            $sql = AssayQuery::$delete_assay;

            $result = $this->db->execute($sql, [
                "assay_id" => $assayId,
            ]);

            return $result;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get an assay
     *
     * @param $assayId
     *
     * @return array|bool
     */
    public function getAssay($assayId)
    {

        try {
            $sql = AssayQuery::$get_assay;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "assay_id" => $assayId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Alias for getAssay
     * 
     * @param $assayId
     *
     * @return array|bool
     */
    public function getAssayInfo($assayId)
    {

        return $this->getAssay($assayId);
    }

    /**
     * Get assay paginated
     *
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return bool|object
     */
    public function getAssaysPaginated($page, $nrows, $searchTerm)
    {

        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = AssayQuery::$get_assay_list_search_count;
            $itemQuery = AssayQuery::$get_assay_list_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = AssayQuery::$get_assay_list_count;
            $itemQuery = AssayQuery::$get_assay_list_paginated;
        }

        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

}