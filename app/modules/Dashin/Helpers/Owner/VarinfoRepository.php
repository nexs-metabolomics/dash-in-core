<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-13
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Helpers\Owner;


use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Dashin\Helpers\RepositoryBase;
use Phalcon\Db\Enum;

class VarinfoRepository extends RepositoryBase
{
    public function setUpvarDatatypeNumeric($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }
        
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->upvar_set_datatype_numeric;

            $result = $this->db->execute($sql,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
//    public function convertUpvarToNumeric($upvarDatasetId)
//    {
//        if(!UniqueId::uuidValidate($upvarDatasetId)) {
//            return false;
//        }
//
//        try {
//            $sql = $this->dashin_config->sql->owner_varinfo->upvar_convert_to_numeric;
//
//            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
//                "upvar_dataset_id" => $upvarDatasetId,
//            ]);
//            if($result) {
//                return $result;
//            }
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//        return false;
//    }
    
    public function setVarVarDatatypeNumeric($datasetId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->var_set_datatype_numeric;

            $result = $this->db->execute($sql,[
                "dataset_id" => $datasetId,
            ]);
            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
//    public function convertVarVarToNumeric($varDatasetId)
//    {
//        if(!UniqueId::uuidValidate($varDatasetId)) {
//            return false;
//        }
//
//        try {
//            $sql = $this->dashin_config->sql->owner_varinfo->var_convert_to_numeric;
//
//            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
//                "var_dataset_id" => $varDatasetId,
//            ]);
//            if($result) {
//                return $result;
//            }
//
//        } catch (\PDOException $e) {
//            $this->flashSession->error($e->getMessage());
//        }
//        return false;
//    }
    
    /**
     * @param $filename
     * @param $sep
     * @return mixed
     * @throws \App\Library\Utils\JsonException
     */
    public function createUploadVarinfo(\SplFileObject $file, $sep)
    {
        try {

            $this->db->begin();
            
            $userId = $this->SU->getUserId();
            $organizationId = $this->SU->getActiveManagerOrgId();

            #----------------------------------------------------
            # create  upload_set
            #----------------------------------------------------
            $sqlUpvarDataset = $this->dashin_config->sql->owner_varinfo->create_upvar_dataset;
            $upvarDatasetResult = $this->db->fetchOne($sqlUpvarDataset, Enum::FETCH_ASSOC, [
                "name" => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "owner_org" => $organizationId,
                "owner_user" => $userId,
            ]);
            
            if (!UniqueId::uuidValidate($upvarDatasetResult["upvar_dataset_id"])) {
                $this->db->rollback();
                return false;
            }
            $upvarDatasetId = $upvarDatasetResult["upvar_dataset_id"];
            
            #----------------------------------------------------
            # first row must be header
            #----------------------------------------------------
            
            $firstRow = $file->fgetcsv($sep);
            if (!is_array($firstRow)) {
                $this->db->rollback();
                return false;
            }
            $ncol = count($firstRow);
            if (!$ncol) {
                $this->db->rollback();
                return false;
            }

            #----------------------------------------------------
            # check for valid header row (check first 20 columns)
            #----------------------------------------------------
            $header = array_values($firstRow);
            $firstHdrs = array_slice($header, 0, 20);

            # check if column varname or variable_id is present
            $standardColumnNames = [
                "variable_id",
                "varname",
            ];

            # identify prerequisite standard columns
            $standardColIdx = [];
            foreach ($standardColumnNames as $columnName) {
                $viMatch = array_keys(preg_grep("#^" . $columnName . "$#i", $firstHdrs));
                if (count($viMatch) === 1) {
                    $standardColIdx[$columnName] = $viMatch[0];
                }
            }
            
            # check that required columns present
            if (!count($standardColIdx)) {
                $this->flashSession->error("You must include at least one of the columns 'varname' or 'variable_id'. Did you choose the correct separator?");
                $this->db->rollback();
                return false;
            }
            
            # identify other standard columns
            $otherColumnNames = [
                "description",
            ];
            foreach ($otherColumnNames as $columnName) {
                $viMatch = array_keys(preg_grep("#^" . $columnName . "$#i", $firstHdrs));
                if (count($viMatch) === 1) {
                    $standardColIdx[$columnName] = $viMatch[0];
                }
            }

            # merge standard names
            $standardColumnNames = array_merge($standardColumnNames, $otherColumnNames);
            
            #----------------------------------------------------
            # remove standard columns from header
            #----------------------------------------------------
            foreach ($standardColumnNames as $columnName) {
                if (isset($standardColIdx[$columnName])) {
                    unset($header[$standardColIdx[$columnName]]);
                }
            }
            # check if any headers left
            $nVars = count($header);
            if (!count($header)) {
                $this->flashSession->notice("No extended variable info");
                $this->db->rollback();
                return false;
            }
            # reindex header after remove
            $header = array_values($header);
            
            #----------------------------------------------------
            # insert into varinfo
            #----------------------------------------------------
            # generate local ids
            $localIds = range(0, count($header) - 1);
            $sqlHdr = $this->dashin_config->sql->owner_varinfo->create_upvar_variable;
            for ($i = 0; $i < $nVars; ++$i) {
                $this->db->execute($sqlHdr, [
                    "local_id" => $localIds[$i],
                    "upvar_dataset_id" => $upvarDatasetId,
                    "name" => (string)$header[$i],
                ]);
            }

            #----------------------------------------------------
            # variable inforow(s)
            #----------------------------------------------------
            $sqlDrow = $this->dashin_config->sql->owner_varinfo->create_upvar_datatable;

            # initialize params array
            $params = [
                "upvar_dataset_id" => $upvarDatasetId,

                "variable_id" => "",
                "varname" => "",
                "description" => "",

                "ordinal" => "",
                "datarow" => "",
            ];
            $ordinal = 1;
            while (!$file->eof()) {
                $drow = $file->fgetcsv($sep);

                # fill columns that exist in uploaded data
                foreach ($standardColumnNames as $columnName) {
                    if (isset($standardColIdx[$columnName])) {
                        $params[$columnName] = $drow[$standardColIdx[$columnName]];
                        unset($drow[$standardColIdx[$columnName]]);
                    } else {
                        $params[$columnName] = "";
                    }
                }
                # remove rows that don't have either varname or variable_id
                if (strlen($params["variable_id"] . $params["varname"]) == 0) {
                    continue;
                } elseif (!UniqueId::uuidValidate($params["variable_id"])) {
                    $params["variable_id"] = null;
                }
                $params["datarow"] = Json::jsonEncode(array_values($drow));
                $params["ordinal"] = $ordinal;

                $this->db->execute($sqlDrow, $params);
                ++$ordinal;
            }

            $this->db->commit();
        } catch (\Exception $e) {
            $this->flashSession->error($e->getMessage());
        }
        return $upvarDatasetId;
    }

    public function getUpvarDatasetDataCsv($upvarDatasetId)
    {
        if (!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_upvar_dataset_data_full_csv;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "upvar_dataset_id" => $upvarDatasetId,
                "owner_org"        => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get uploaded varinfo-dataset for view
     * 
     * @param $upvarDatasetId
     * @return array|bool
     */
    public function getUpvarDatasetForView($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_upvar_dataset_for_view;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            if($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }


    public function getUpvarDatasetForCheck($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }

        $userId = $this->SU->getUserId();
        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            # duplicate varnames
//            $upvarDatasetId = "3bb69318-cbda-4ab8-8a51-d590831ebfdd";
            $sql = $this->dashin_config->sql->owner_varinfo->upvar_duplicate_varnames;
            $dupNames = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            $sql = $this->dashin_config->sql->owner_varinfo->upvar_empty_varnames;
            $emptyNames = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            $sql = $this->dashin_config->sql->owner_varinfo->upvar_duplicate_upvarnames;
            $dupUpvarNames = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            $sql = $this->dashin_config->sql->owner_varinfo->upvar_empty_upvarnames;
            $emptyUpvarNames = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            
            return [
                "duplicate_varnames" => $dupNames,
                "empty_varnames" => $emptyNames,
                "duplicate_upvarnames" => $dupUpvarNames,
                "empty_upvarnames" => $emptyUpvarNames,
            ];
            # empty varnames
            # duplicate upvar_names
            # empty upvar_names
            # $sql = "SELECT * FROM dashin.upvar_datatable WHERE upvar_dataset_id = :upvar_dataset_id";
            # $sql = $this->dashin_config->sql->owner_varinfo->upvar_check_dup_names;

            
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get uploaded varinfo-dataset for view
     * 
     * @param $upvarDatasetId
     * @return array|bool
     */
    public function getUpvarDatasetMinimal($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_upvar_dataset_minimal;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            if($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function upvarDatasetUpdate($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->update_upvar_dataset;

            $state = $this->db->execute($sql,[
                "name" => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "upvar_dataset_id" => $upvarDatasetId,
            ]);
            
            return $state;
            
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get variables from a single variableset
     * paginated at the database level
     *
     * @param $upvarDatasetId
     * @param $page
     * @param $nrows
     * @return array|bool|object
     */
    public function getUpvarVariablesPaginated($upvarDatasetId, $page, $nrows)
    {
        if (!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }

        $countQuery = $this->dashin_config->sql->owner_varinfo->upvar_variable_by_upvar_dataset_count;
        $itemQuery = $this->dashin_config->sql->owner_varinfo->upvar_variable_by_upvar_dataset_paginated;
        $organizationId = $this->SU->getActiveManagerOrgId();

        $queryParams=[
            "upvar_dataset_id" => $upvarDatasetId,
            "owner_org" => $organizationId,
        ];
        $outParams = $this->_preparePagination($countQuery,$queryParams,$page,$nrows);

        try {

            $queryParams["offset"] = $outParams["offset"];
            $queryParams["limit"] = $outParams["limit"];

            $outRows = $this->db->fetchAll($itemQuery,Enum::FETCH_OBJ,$queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;

    }

    /**
     * Get paginated data rows
     *
     * @param $upvarDatasetId
     * @param $rowStart
     * @param $rowLimit
     * @param $colStart
     * @param $colLimit
     * @return array|bool
     */
    public function getVarinforowsPaginated($upvarDatasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx, $searchTerm, $columnFilter)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)){
            return false;
        }
        # query params
        $organizationId = $this->SU->getActiveManagerOrgId();
        $countParms = ["upvar_dataset_id" => $upvarDatasetId];
        $countParms["owner_org"] = $organizationId;
        $countParmsRow = $countParms;
        

        # columns
        $countColsSql = $this->dashin_config->sql->owner_varinfo->upvar_variable_by_upvar_dataset_count;
        $colParams = $this->_preparePagination($countColsSql,$countParms,$colStart,$colLimit);

        # rows
        if(strlen($searchTerm)) {
            $countRowsSql = $this->dashin_config->sql->owner_varinfo->upvar_datarows_by_upvar_dataset_count_search;
            $countParmsRow["search_term"] = $searchTerm;
            $searchFilter = "AND name = :search_term\n";
        } else {
            $countRowsSql = $this->dashin_config->sql->owner_varinfo->upvar_datarows_by_upvar_dataset_count;
            $searchFilter = "";
        }
        $rowParams = $this->_preparePagination($countRowsSql,$countParmsRow,$rowStart,$rowLimit);

        try {

            # column items
            $sql = $this->dashin_config->sql->owner_varinfo->get_upvar_datarows_paginated;
            $datacols = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "upvar_dataset_id" => $upvarDatasetId,
                "offset" => $colParams["offset"],
                "limit" => $colParams["limit"],
                "owner_org" => $organizationId,
            ]);
            
            if(null !== $filterIdx) {
                $filterIdx = (int)$filterIdx;
                $filter = "AND length(jsonb_extract_path_text(datarow,'$filterIdx'))>0\n";
            } else {
                $filter = "";
            }

            # row query
            $queryLines = implode(",\n",array_column($datacols,"query_line"));
            unset($datacols["query_line"]);
            $datarowQuery =
                "SELECT upvar_datarow_id, name, ordinal,\n".
                "json_build_array($queryLines) AS datarow\n".
                "FROM dashin.upvar_datatable dt\n".
                "WHERE upvar_dataset_id = :upvar_dataset_id\n".
                $searchFilter.
                $filter.
                "AND EXISTS (SELECT 1 FROM dashin.upvar_dataset uv WHERE (uv.upvar_dataset_id,uv.owner_org) = (:upvar_dataset_id,:owner_org))\n".
                "ORDER BY ordinal LIMIT :limit OFFSET :offset";

            # finalize column pagination object
            $colParams["items"] = $datacols;
            $colParams = (object)$colParams;

            # row items
            if(strlen($searchTerm)) {
                $datarows = $this->db->fetchAll($datarowQuery,Enum::FETCH_OBJ,[
                    "upvar_dataset_id" => $upvarDatasetId,
                    "offset" => $rowParams["offset"],
                    "limit" => $rowParams["limit"],
                    "owner_org" => $organizationId,
                    "search_term" => $searchTerm,
                ]);
            } else {
                $datarows = $this->db->fetchAll($datarowQuery,Enum::FETCH_OBJ,[
                    "upvar_dataset_id" => $upvarDatasetId,
                    "offset" => $rowParams["offset"],
                    "limit" => $rowParams["limit"],
                    "owner_org" => $organizationId,
                ]);
            }

            foreach ($datarows as &$row) {
                $row->datarow = (object)json_decode($row->datarow);
            }
            unset($row);

            # row pagination object
            $rowParams["items"] = $datarows;
            $rowParams = (object)$rowParams;

            return ["datarows" => $rowParams,"datacols" => $colParams];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Delete an uploaded varinfo-dataset
     * 
     * @param $upvarDatasetId
     * @return bool
     */
    public function deleteUpvarDataset($upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->delete_upvar_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql,[
                "upvar_dataset_id" => $upvarDatasetId,
                "owner_org" => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;

    }

    /**
     * Get a list of uploadet varinfo-datasets, paginated
     * 
     * @param $page
     * @param $nrows
     * @param $searchTerm
     * @return array|bool|object
     */
    public function getUpvarDatasetsPaginated($page,$nrows,$searchTerm)
    {
        $organizationId = $this->SU->getActiveManagerOrgId();
        $queryParams["owner_org"] = $organizationId;
        if(strlen($searchTerm)) {
            $countQuery = $this->dashin_config->sql->owner_varinfo->list_upvar_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_varinfo->list_upvar_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_varinfo->list_upvar_datasets_count;
            $itemQuery = $this->dashin_config->sql->owner_varinfo->list_upvar_datasets_paginated;
        }
        $outParams = $this->_preparePagination($countQuery,$queryParams,$page,$nrows);
        $queryParams["offset"] = $outParams["offset"];
        $queryParams["limit"] = $outParams["limit"];
        try {

            $outRows = $this->db->fetchAll($itemQuery,Enum::FETCH_OBJ,$queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;
            
            return $outParams;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function createVarinfo($upvarDatasetId,$datasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->match_create_var_dataset;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "name" => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "upvar_dataset_id" => $upvarDatasetId,
                "dataset_id" => $datasetId,
            ]);
            if(isset($result["var_dataset_id"])) {
                return $result["var_dataset_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getVarDatasetMinimalFromDatasetId($datasetId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_var_dataset_minimal_from_dataset_id;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "dataset_id" => $datasetId,
            ]);
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get dataset, name, description and data dimensions
     * 
     * @param $datasetId
     * @return array|bool
     */
    public function getDataset($datasetId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_dataset_for_view;
            
            $organizationId = $this->SU->getActiveManagerOrgId();
            
            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "dataset_id" => $datasetId,
                "owner_org" => $organizationId,
            ]);
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getMatchStatistics($datasetId,$upvarDatasetId)
    {
        if(!UniqueId::uuidValidate($upvarDatasetId)) {
            return false;
        }
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->match_nvars_matched;
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "upvar_dataset_id" => $upvarDatasetId,
                "dataset_id" => $datasetId,
            ]);
            $out["matched"] = isset($result["n"]) ? $result["n"] : 0;

            $sql = $this->dashin_config->sql->owner_varinfo->match_nvars_data_unmatched;
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "upvar_dataset_id" => $upvarDatasetId,
                "dataset_id" => $datasetId,
            ]);
            $out["dvar_unmatched"] = isset($result["n"]) ? $result["n"] : 0;

            $sql = $this->dashin_config->sql->owner_varinfo->match_nvars_upvar_unmatched;
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "upvar_dataset_id" => $upvarDatasetId,
                "dataset_id" => $datasetId,
            ]);
            $out["upvar_unmatched"] = isset($result["n"]) ? $result["n"] : 0;

            return $out;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function varDatasetUpdate($datasetId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->update_var_dataset;

            $state = $this->db->execute($sql,[
                "name" => $this->request->getPost("name"),
                "description" => $this->request->getPost("description"),
                "dataset_id" => $datasetId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function deleteVarDataset($varDatasetId)
    {
        if(!UniqueId::uuidValidate($varDatasetId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->delete_var_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql,[
                "var_dataset_id" => $varDatasetId,
//                "owner_org" => $organizationId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getVarDatasetsPaginated($page,$nrows,$searchTerm)
    {
        $organizationId = $this->SU->getActiveManagerOrgId();
        $queryParams["owner_org"] = $organizationId;
        if(strlen($searchTerm)) {
            $countQuery = $this->dashin_config->sql->owner_varinfo->list_var_datasets_count_search;
            $itemQuery = $this->dashin_config->sql->owner_varinfo->list_var_datasets_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = $this->dashin_config->sql->owner_varinfo->list_var_datasets_count;
            $itemQuery = $this->dashin_config->sql->owner_varinfo->list_var_datasets_paginated;
//            $queryParams = [];
        }
        $outParams = $this->_preparePagination($countQuery,$queryParams,$page,$nrows);
        $queryParams["offset"] = $outParams["offset"];
        $queryParams["limit"] = $outParams["limit"];

        try {
            $outRows = $this->db->fetchAll($itemQuery,Enum::FETCH_OBJ,$queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;

    }

    public function addSearchColumnMapping($mappings)
    {
        if (!is_array($mappings)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->insert_search_column_mapping;

            $state = 0;
            $this->db->begin();
            foreach ($mappings as $varVariableId => $searchColumnTypeId) {
                if (UniqueId::uuidValidate($varVariableId)) {
                    $this->db->execute($sql, [
                        "search_column_type_id" => $searchColumnTypeId,
                        "var_variable_id"       => $varVariableId,
                        "owner_org"             => $organizationId,
                    ]);
                    $state += $this->db->affectedRows();

                }
            }
            $this->db->commit();
            return $state;


        } catch (\PDOException $e) {
            $this->db->rollback();
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function removeSearchColumnMapping($mappings)
    {
        if (!is_array($mappings)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->delete_search_column_mapping;

            $state = 0;
            $this->db->begin();
            foreach (array_keys($mappings) as $varVariableid) {
                if (UniqueId::uuidValidate($varVariableid)) {
                    $this->db->execute($sql, [
                        "var_variable_id" => $varVariableid,
                        "owner_org"       => $organizationId,
                    ]);
                    $state += $this->db->affectedRows();
                }
            }
            $this->db->commit();
            return $state;


        } catch (\PDOException $e) {
            $this->db->rollback();
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getVarDatasetMinimal($datasetId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_var_dataset_minimal;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "dataset_id" => $datasetId,
            ]);
            if($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getVarDatarowsPaginated($datasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx)
    {
        if(!UniqueId::uuidValidate($datasetId)){
            return false;
        }
        # query params
        $organizationId = $this->SU->getActiveManagerOrgId();
        $countParms = ["dataset_id" => $datasetId];
//        $countParms["owner_org"] = $organizationId;

        # columns
        $countColsSql = $this->dashin_config->sql->owner_varinfo->var_dataset_cols_count;
        $colParams = $this->_preparePagination($countColsSql,$countParms,$colStart,$colLimit);

        # rows
        $countRowsSql = $this->dashin_config->sql->owner_varinfo->var_dataset_rows_count;
        $rowParams = $this->_preparePagination($countRowsSql,$countParms,$rowStart,$rowLimit);

        try {

            # column items
            $sql = $this->dashin_config->sql->owner_varinfo->var_dataset_get_rows_paginated;
            $datacols = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "dataset_id" => $datasetId,
                "offset" => $colParams["offset"],
                "limit" => $colParams["limit"],
//                "owner_org" => $organizationId,
            ]);

            if(null !== $filterIdx) {
                $filterIdx = (int)$filterIdx;
                $filter = "AND length(jsonb_extract_path_text(datarow,'$filterIdx'))>0\n";
            } else {
                $filter = "";
            }

            # row query
            $queryLines = implode(",\n",array_column($datacols,"query_line"));
            unset($datacols["query_line"]);
            $datarowQuery =
                "SELECT dt.variable_id, v.name , dt.ordinal,\n".
                "json_build_array($queryLines) AS datarow\n".
                "FROM dashin.var_datatable dt\n".
                "LEFT JOIN dashin.variable v USING (variable_id)".
                "WHERE dt.var_dataset_id = (SELECT var_dataset_id FROM dashin.var_dataset WHERE dataset_id = :dataset_id)\n".
                $filter.
//                "AND EXISTS (SELECT 1 FROM dashin.upvar_dataset uv WHERE (uv.upvar_dataset_id,uv.owner_org) = (:upvar_dataset_id,:owner_org))\n".
                "ORDER BY ordinal LIMIT :limit OFFSET :offset";

            # finalize column pagination object
            $colParams["items"] = $datacols;
            $colParams = (object)$colParams;

            # row items
            $datarows = $this->db->fetchAll($datarowQuery,Enum::FETCH_OBJ,[
                "dataset_id" => $datasetId,
                "offset" => $rowParams["offset"],
                "limit" => $rowParams["limit"],
//                "owner_org" => $organizationId,
            ]);

            foreach ($datarows as &$row) {
                $row->datarow = (object)json_decode($row->datarow);
            }
            unset($row);

            # row pagination object
            $rowParams["items"] = $datarows;
            $rowParams = (object)$rowParams;

            return ["datarows" => $rowParams,"datacols" => $colParams];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getVarDatasetForView($varDatasetId)
    {
        if(!UniqueId::uuidValidate($varDatasetId)) {
            return false;
        }
        
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->var_dataset_for_view;
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC,[
                "var_dataset_id" => $varDatasetId,
                "owner_org" => $organizationId,
            ]);
            
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getVarVariablesPaginated($datasetId, $page, $nrows)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        $countQuery = $this->dashin_config->sql->owner_varinfo->var_variable_count;
        $itemQuery = $this->dashin_config->sql->owner_varinfo->var_variable_paginated;

        $queryParams = [];
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["dataset_id"] = $datasetId;
        $outParams = $this->_preparePagination($countQuery,$queryParams,$page,$nrows);

        try {

            $queryParams["offset"] = $outParams["offset"];
            $queryParams["limit"] = $outParams["limit"];


            $outRows = $this->db->fetchAll($itemQuery,Enum::FETCH_OBJ,$queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getVarVariablesUnmappedColsPaginated($datasetId, $varVariableId, $page, $nrows)
    {
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($varVariableId)) {
            $varVariableId = null;
        }

        $countQuery = $this->dashin_config->sql->owner_varinfo->var_variable_unmapped_cols_count;
        $itemQuery = $this->dashin_config->sql->owner_varinfo->var_variable_unmapped_cols_paginated;

        $queryParams = [];
        $queryParams["owner_org"] = $this->SU->getActiveManagerOrgId();
        $queryParams["dataset_id"] = $datasetId;
        $queryParams["var_variable_id"] = $varVariableId;
        $outParams = $this->_preparePagination($countQuery,$queryParams,$page,$nrows);

        try {

            $queryParams["offset"] = $outParams["offset"];
            $queryParams["limit"] = $outParams["limit"];


            $outRows = $this->db->fetchAll($itemQuery,Enum::FETCH_OBJ,$queryParams);
            $outParams["items"] = $outRows;
            $outParams = (object)$outParams;

            return $outParams;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getVarVariablesMappedCols($datasetId, $varVariableId, $searchColumnTypeId)
    {
        if(!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if(!UniqueId::uuidValidate($varVariableId)) {
            $varVariableId = null;
        }
        if(!(ctype_digit($searchColumnTypeId) || is_int($searchColumnTypeId))) {
            $searchColumnTypeId = null;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->var_variable_mapped_search_cols;

            $result = $this->db->fetchAll($sql,Enum::FETCH_OBJ,[
                "dataset_id" => $datasetId,
                "var_variable_id" => $varVariableId,
                "search_column_type_id" => $searchColumnTypeId,
            ]);
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function setVarVariableColMap($varVariableId,$searchColumnTypeId)
    {
        if(!UniqueId::uuidValidate($varVariableId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->var_variable_search_col_map_single;

            $result = $this->db->execute($sql,[
                "var_variable_id" => $varVariableId,
                "search_column_type_id" => $searchColumnTypeId
            ]);
            $nrows = $this->db->affectedRows();
            return $nrows;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function setVarVariableColUnmap($varVariableId)
    {
        if(!UniqueId::uuidValidate($varVariableId)) {
            return false;
        }
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->var_variable_search_col_unmap;

            $result = $this->db->execute($sql,[
                "var_variable_id" => $varVariableId,
            ]);
            return $result;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function getSearchColumnTypes()
    {
        try {
            $sql = $this->dashin_config->sql->owner_varinfo->get_search_column_types;
            
            $result = $this->db->fetchAll($sql,Enum::FETCH_ASSOC);
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}