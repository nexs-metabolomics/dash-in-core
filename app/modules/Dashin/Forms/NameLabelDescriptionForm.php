<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms;


use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class NameLabelDescriptionForm extends FormBase
{
    public function initialize()
    {
        $nameFld = new Text("name",[
            "placeholder" => "Name (required)",
            "required" => true,
        ]);
        $nameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Name is required",
            ])
        ]);
        $nameFld->setLabel("Name (required)");
        $this->add($nameFld);
        
        $labelFld = new Text("label",[
            "placeholder" => "Label",
        ]);
        $labelFld->setLabel("Label");
        $this->add($labelFld);
        
        $descriptionFld = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
    }
    
}