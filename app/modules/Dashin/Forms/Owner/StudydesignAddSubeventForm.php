<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use App\Library\FormElements\PlainText;
use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class StudydesignAddSubeventForm extends FormBase
{
    protected $_options = [];

    public function __construct(?array $options = null)
    {
        $this->options["subevent_type"] = isset($options["subevent_type"]) ? $options["subevent_type"] : null;
        $this->options["action_type"] = isset($options["action_type"]) ? $options["action_type"] : null;
        $this->options["selection_choices"] = isset($options["selection_choices"]) ? $options["selection_choices"] : null;
        $this->options["additional_fields"] = isset($options["additional_fields"]) ? $options["additional_fields"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
        # name
        $nameFld = new Text("name", [
            "placeholder" => "Name",
        ]);
        $nameFld->setLabel("Name");
        $this->add($nameFld);

        # label
        $labelFld = new Text("label", [
            "placeholder" => "Label",
        ]);
        $labelFld->setLabel("Label");
        $this->add($labelFld);

        # description
        $descFld = new TextArea("description", [
            "placeholder" => "Description",
        ]);
        $descFld->setLabel("Description");
        $this->add($descFld);

        # subevent_type_id -> Select
        $actionTypeChoices = $this->options["subevent_type"];
        if (!is_array($actionTypeChoices)) {
            $actionTypeChoices = ["No option available"];
        }
        $actionTypeFld = new Select("subevent_type", $actionTypeChoices, [
            "onchange" => "this.form.submit()"
        ]);
        $actionTypeFld->setLabel("Subevent type");
        $this->add($actionTypeFld);

        # intervention_type_id -> Select
        $actionSubtypeChoices = $this->options["action_type"];
        if (!is_array($actionSubtypeChoices)) {
            $actionSubtypeChoices = ["No option available"];
        }
        $actionSubtypeFld = new Select("action_type", $actionSubtypeChoices);
        $actionSubtypeFld->setLabel("Type");
        $this->add($actionSubtypeFld);

        # additional
        $this->_additionalFields();
        
        # row_comment
        $commentFld = new TextArea("row_comment", [
            "placeholder" => "Comment",
        ]);
        $commentFld->setLabel("Comment");
        $this->add($commentFld);

    }
    
    private function _additionalFields()
    {
        if ($this->options["additional_fields"]) {
            $fields = $this->options["additional_fields"];
            $ordinal = array_column($fields,"ordinal");
            array_multisort($ordinal,$fields);
            if (is_array($fields)) {
                foreach ($fields as $fieldName => $field) {
                    if ($field["type"] === "select") {
                        $sourceChoices = $this->options["selection_choices"];
                        $choicesIn = array_intersect_key($sourceChoices,
                            array_flip(array_keys(array_column($sourceChoices, "selection_type_id"), $field["sourceid"])));
                        $tmpChoices = [];
                        if (is_array($choicesIn)) {
                            $tmpChoices = array_combine(
                                array_column($choicesIn, "selection_choice_id"),
                                array_column($choicesIn, "name")
                            );
                        }
                        $tmpFld = new Select($fieldName, $tmpChoices);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    } elseif ($field["type"] === "text") {
                        $tmpFld = new Text($fieldName, [
                            "placeholder" => $field["label"]
                        ]);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    } elseif ($field["type"] === "plaintext") {
                        $tmpFld = new PlainText($fieldName, [
                            "content" => $field["content"] ?? null
                        ]);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    }
                }

            }
        }
    }
}