<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-13
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class StudyCreateForm extends FormBase
{
    public function initialize()
    {
        #--------------------------------------------------------
        # general information
        #--------------------------------------------------------
        # study short name
        $shortNameFld = new Text("name",[
            "placeholder" => "Study short name",
            "required" => true,
        ]);
        $shortNameFld->setLabel("Study short name");
        $shortNameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Short name is required",
            ]),
        ]);
//        $shortNameFld->setUserOption("required",true);
        $this->add($shortNameFld);
        
        # title
        $titleFld = new TextArea("title",[
            "placeholder" => "Title",
            "required" => true,
        ]);
        $titleFld->setLabel("Title");
        $titleFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Title is required",
            ]),
        ]);
        $this->add($titleFld);
        
        # description
        $descriptionFld = new TextArea("description",[
            "placeholder" => "Description",
            "required" => true,
        ]);
        $descriptionFld->setLabel("Description");
        $descriptionFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Description is required",
            ]),
        ]);
        $this->add($descriptionFld);
    }
}