<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;

class SamplingeventSubformPhysiologyForm extends FormBase
{
    public function initialize()
    {
        $timeUnitChoices = [
            "Select time unit"
            , "Seconds"
            , "Minutes"
            , "Hours"
            , "Days"
            , "Weeks"
            , "Years"
        ];
        $timeUnitFld = new Select("time_unit", $timeUnitChoices);
        $timeUnitFld->setLabel("Time unit");
        $this->add($timeUnitFld);
    }
}