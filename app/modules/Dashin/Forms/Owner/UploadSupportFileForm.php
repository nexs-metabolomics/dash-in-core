<?php

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Text;

class UploadSupportFileForm extends FormBase
{
    public function initialize()
    {
        $fileFld = new File("fileup");
        $fileFld->setLabel("Support file");
        $this->add($fileFld);
        
//        $descFld = new Text("description",[
//            "placeholder" => "Description",
//        ]);
//        $descFld->setLabel("Description");
//        $this->add($descFld);
    }
}