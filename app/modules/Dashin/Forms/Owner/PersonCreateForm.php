<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use App\Library\Validator\Email;
use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class PersonCreateForm extends FormBase
{
    public function initialize()
    {
        # first name
        $firstNameFld = new Text("first_name",[
            "placeholder" => "First name",
            "required" => true,
        ]);
        $firstNameFld->setLabel("First name");
        $firstNameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "First name required",
            ]),
        ]);
        $this->add($firstNameFld);

        # last name
        $lastNameFld = new Text("last_name",[
            "placeholder" => "Last name",
            "required" => true,
        ]);
        $lastNameFld->setLabel("Last name");
        $lastNameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Last name required",
            ]),
        ]);
        $this->add($lastNameFld);

        # email
        $emailFld = new Text("email",[
            "placeholder" => "Email",
        ]);
        $emailFld->setLabel("Email");
        $emailFld->addValidators([
            new Email(),
        ]);
        $this->add($emailFld);
        
        # description
        $descriptionFld = new TextArea("description",[
            "placeholder" => "Description"
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
    }
}