<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Numeric;

class MetaSearchForm extends FormBase
{
    public function initialize()
    {
        $mzLow = new Numeric("mz_low",[
            "placeholder" => "Mz-value",
            "step" => "any",
            "class" => "num-left",
        ]);
        $this->add($mzLow);
        $mzHigh = new Numeric("mz_high",[
            "placeholder" => "Mz-range",
            "step" => "any",
            "class" => "num-right",
        ]);
        $this->add($mzHigh);
        
        $rtLow = new Numeric("rt_low",[
            "placeholder" => "rt-value",
            "step" => "0.0001",
            "class" => "num-left",
        ]);
        $this->add($rtLow);
        $rtHigh = new Numeric("rt_high",[
            "placeholder" => "rt-range",
            "step" => "0.0001",
            "class" => "num-right",
        ]);
        $this->add($rtHigh);
        
    }
}