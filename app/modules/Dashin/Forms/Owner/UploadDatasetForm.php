<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class UploadDatasetForm extends FormBase
{
    public function initialize()
    {
        # file
        $fileFld = new File("file_up");
        $fileFld->setLabel("Select file");
        $this->add($fileFld);

        # dataset name
        $nameFld = new Text("name",[
            "placeholder" => "Name",
            "required" => true,
        ]);
        $nameFld->setLabel("Name (required)");
        $nameFld->addValidators([
            new StringLength([
                "min" => 1,
                "minMessage" => "Name is required",
            ]),
        ]);
        $this->add($nameFld);

        # dataset description
        $nameFld = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $nameFld->setLabel("Description");
        $this->add($nameFld);

        # has header
        $hasHeaderFld = new Check("header",["value"=>"on","checked"=>"on"]);
        $hasHeaderFld->setLabel("Header");
        $this->add($hasHeaderFld);

        # separator
        $seps = ["a" => "Auto", "c" => ",", "sc" => ";", "t" => "{tab}", "sp" => "{space}"];
        $sepFld = new Select("sep",$seps);
        $sepFld->setLabel("Separator");
        $this->add($sepFld);
    }
}