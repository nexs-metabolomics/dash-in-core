<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-24
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class EditDatasetForm extends FormBase
{
    public function initialize()
    {
        # name
        $nameFld = new Text("name", [
            "placeholder" => "Name",
            "required"    => true,
        ]);
        $nameFld->setLabel("Name");
        $this->add($nameFld);

        # description
        $descriptionFld = new TextArea("description", [
            "plachelder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
    }
}