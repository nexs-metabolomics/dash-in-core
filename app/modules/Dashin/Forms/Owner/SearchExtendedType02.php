<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;

class SearchExtendedType02 extends FormBase
{
    public function initialize()
    {
        # name
        $nameFld = new Text("studyname",[
            "placeholder" => "Study name",
        ]);
        $nameFld->setLabel("Study name");
        $this->add($nameFld);
        
        # title
        $titleFld = new Text("studytitle",[
            "placeholder" => "Study title",
        ]);
        $titleFld->setLabel("Study title");
        $this->add($titleFld);
        
        # description
        $descFld = new Text("studydescription",[
            "placeholder" => "Study description",
        ]);
        $descFld->setLabel("Study description");
        $this->add($descFld);

        # endpoint
        $endpointFld = new Text("studyendpoint",[
            "placeholder" => "Study endpoint",
        ]);
        $endpointFld->setLabel("Study endpoint");
        $this->add($endpointFld);

        # objective
        $objectivesFld = new Text("studyobjectives",[
            "placeholder" => "Study objectives",
        ]);
        $objectivesFld->setLabel("Study objectives");
        $this->add($objectivesFld);

        # conclusion
        $conclusionFld = new Text("studyconclusion",[
            "placeholder" => "Study conclusion",
        ]);
        $conclusionFld->setLabel("Study conclusion");
        $this->add($conclusionFld);

        # exclusion
        $exclusionFld = new Text("studyexclusion",[
            "placeholder" => "Study exclusion",
        ]);
        $exclusionFld->setLabel("Study exclusion");
        $this->add($exclusionFld);

        # research design
        $researchdesignFld = new Text("studyresearchdesign",[
            "placeholder" => "Study research design",
        ]);
        $researchdesignFld->setLabel("Study research design");
        $this->add($researchdesignFld);

    }
}