<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;

class CreatePrefixedunitForm extends FormBase
{
    public function __construct(?array $options = null)
    {
        $this->options["prefix_options"] = isset($options["prefix_options"]) ? $options["prefix_options"] : null;
        $this->options["unit_options"] = isset($options["unit_options"]) ? $options["unit_options"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
        $fldName = new Text("name",[
            "placeholder" => "Name",
        ]);
        $fldName->setLabel("Name");
        $this->add($fldName);
        
        $fldSymbol = new Text("symbol",[
            "placeholder" => "Symbol",
        ]);
        $fldSymbol->setLabel("Symbol");
        $this->add($fldSymbol);
        
        $fldDescription = new Text("description",[
            "placeholder" => "Description",
        ]);
        $fldDescription->setLabel("Description");
        $this->add($fldDescription);
        
        $pOpt = $this->options["prefix_options"];
        $fldPrefix = new Select("prefix",$pOpt);
        $fldPrefix->setLabel("Prefix");
        $this->add($fldPrefix);
        
        $uOpt = $this->options["unit_options"];
        $fldUnit = new Select("unt",$uOpt);
        $fldUnit->setLabel("Unit");
        $this->add($fldUnit);
        
    }

}