<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;

class SamplingeventSubformExcretionForm extends FormBase
{
    public function initialize()
    {
        $timeUnitChoices = [
            "Select time unit"
            , "Seconds"
            , "Minutes"
            , "Hours"
            , "Days"
            , "Weeks"
            , "Years"
        ];
        $timeUnitFld = new Select("time_unit", $timeUnitChoices);
        $timeUnitFld->setLabel("Time unit");
        $this->add($timeUnitFld);

        $samplingMethodChoices = [
            "Not defined"
            , "Direct collection"
            , "Swap (absorbent)"
            , "Other"
        ];
        $smplMethodFld = new Select("sampling_method", $samplingMethodChoices);
        $smplMethodFld->setLabel("Sampling method");
        $this->add($smplMethodFld);

        $escrtionChoices = [
            "Not defined"
            , "Urine"
            , "Feces"
            , "Sweat"
            , "Menstruation blood"
            , "Sebacceous matter"
            , "Breath"
            , "Other"
        ];
        $escrFld = new Select("escretion_type",$escrtionChoices);
        $escrFld->setLabel("Escretion sample");
        $this->add($escrFld);

    }

}
