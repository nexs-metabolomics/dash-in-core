<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class StudyEditNameDescription extends FormBase
{
    public function initialize()
    {
        #--------------------------------------------------------
        # general information
        #--------------------------------------------------------
        # study short name
        $shortNameFld = new Text("name",[
            "placeholder" => "Study short name",
            "required" => true,
        ]);
        $shortNameFld->setLabel("Study short name");
        $shortNameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Short name is required",
            ]),
        ]);
        $this->add($shortNameFld);

        # title
        $titleFld = new TextArea("title",[
            "placeholder" => "Title",
            "required" => true,
        ]);
        $titleFld->setLabel("Title");
        $titleFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Title is required",
            ]),
        ]);
        $this->add($titleFld);

        # description
        $descriptionFld = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
        
    }
}