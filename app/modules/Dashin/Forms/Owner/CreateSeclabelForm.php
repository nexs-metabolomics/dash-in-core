<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-05-01
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Filter\Validation\Validator\Regex;

class CreateSeclabelForm extends FormBase
{
    public function initialize()
    {
        $keyFld = new Text("key",[
            "placeholder" => "Key",
            "title" => "Only letters, digits and underscore, no spaces. Must start with a letter and end with a letter or a digit",
            "required" => true,
        ]);
        $keyFld->addValidators(array(
            new Regex(array(
                'pattern' => '#^[A-Za-z][A-Za-z0-9_]*[A-Za-z0-9]$#',
                "message" => "Only letters, digits and underscore '_', no spaces, Must start with a letter and end with a letter or a digit"
            ))
        ));
        $keyFld->setLabel("Key (Only letters, digits and underscore, no spaces. Must start with a letter and end with a letter or a digit)");
        $this->add($keyFld);

        $nameFld = new Text("name",[
            "placeholder" => "Name",
        ]);
        $nameFld->setLabel("Name");
        $this->add($nameFld);

        $descriptionFld = new Text("description",[
            "placeholder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
        
    }
}