<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;

class StudyvariableconditionForm extends FormBase
{
    public function __construct(?array $options = null)
    {
        $this->options["conditionoperators"] = isset($options["conditionoperators"]) ? $options["conditionoperators"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
     
        # condition name
        $nameFld = new Text("conditionname",[
            "placeholder" => "Condition name",
        ]);
        $nameFld->setLabel("Condition name");
        $this->add($nameFld);
        
        $descFld = new Text("description",[
            "placeholder" => "Description",
        ]);
        $descFld->setLabel("Description");
        $this->add($descFld);
        
        # select condition operator
        $conditionoperators = $this->options["conditionoperators"];
        $condopFld = new Select("conditionoperator", $conditionoperators);
        $condopFld->setLabel("Operator");
        $this->add($condopFld);

        $valueAFld = new Text("value_a", [
            "placeholder" => "Value A"
        ]);
        $valueAFld->setLabel("Value A");
        $this->add($valueAFld);

        $valueBFld = new Text("value_b", [
            "placeholder" => "Value B"
        ]);
        $valueBFld->setLabel("Value B");
        $this->add($valueBFld);
        
        # number of input values/fields
    }
}