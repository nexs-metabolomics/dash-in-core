<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class DatacolumntypeForm extends FormBase
{
    public function initialize()
    {
        $shnameFld = new Text("shortname",[
            "placeholder" => "Shortame (required)",
            "required" => true,
        ]);
        $shnameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Shortname is required",
            ])
        ]);
        $shnameFld->setLabel("Shortname (required)");
        $this->add($shnameFld);

        $nameFld = new Text("name",[
            "placeholder" => "Name (required)",
            "required" => true,
        ]);
        $nameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Name is required",
            ])
        ]);
        $nameFld->setLabel("Name (required)");
        $this->add($nameFld);

        $descriptionFld = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);
    }
}