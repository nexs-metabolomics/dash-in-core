<?php

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\File;

class StudyImportSqdrForm extends FormBase
{
    public function initialize()
    {
        # file
//        $fileFld = new File("file_up",[
//            "accept"=>".sqdr",
//        ]);
        $fileFld = new File("file_up",[
            "accept"=>".sqdr",
        ]);
        $fileFld->setLabel("Select file");
        $this->add($fileFld);
    }

}