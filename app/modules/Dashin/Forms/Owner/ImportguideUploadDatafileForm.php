<?php

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\File;

class ImportguideUploadDatafileForm extends FormBase
{
    public function initialize()
    {
        # file
        $fileFld = new File("file_up");
        $fileFld->setLabel("Select file");
        $this->add($fileFld);

    }
}