<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;

class SamplingeventSubformBodyfluidForm extends FormBase
{
    public function initialize()
    {
        $timeUnitChoices = [
            "Select time unit"
            , "Seconds"
            , "Minutes"
            , "Hours"
            , "Days"
            , "Weeks"
            , "Years"
        ];
        $timeUnitFld = new Select("time_unit", $timeUnitChoices);
        $timeUnitFld->setLabel("Time unit");
        $this->add($timeUnitFld);

        $bodyfldSamplingChoices = [
            "Not defined"
            , "Direct sampling"
            , "Needle"
            , "Swap"
            , "Scraping"
            , "Biopsy"
            , "Fractionation"
            , "Other"
        ];
        $smplMethodFld = new Select("sampling_method", $bodyfldSamplingChoices);
        $smplMethodFld->setLabel("Sampling method");
        $this->add($smplMethodFld);

        $fluidTypeChoices = [
            "Not defined"
            , "Whole blood"
            , "Plasma"
            , "Serum"
            , "White blood cells"
            , "Red blood cells"
            , "Saliva"
            , "Semen"
            , "Lymph"
            , "Cerebrospinal fluid"
            , "Other"
        ];
        $fluidTypeFld = new Select("fluid_type",$fluidTypeChoices);
        $fluidTypeFld->setLabel("Body fluid");
        $this->add($fluidTypeFld);

    }
}
