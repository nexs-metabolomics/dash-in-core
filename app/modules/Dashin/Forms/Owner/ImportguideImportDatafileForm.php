<?php

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Filter\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class ImportguideImportDatafileForm extends FormBase
{
    public function initialize()
    {
        # dataset name
        $nameFld = new Text("name", [
            "placeholder" => "Name",
            "required"    => true,
        ]);
        $nameFld->setLabel("Name (required)");
        $nameFld->addValidators([
            new StringLength([
                "min"        => 1,
                "minMessage" => "Name is required",
            ]),
        ]);
        $this->add($nameFld);

        # dataset description
        $nameFld = new TextArea("description", [
            "placeholder" => "Description",
        ]);
        $nameFld->setLabel("Description");
        $this->add($nameFld);

        # has header
        $hasHeaderFld = new Check("header");
        $hasHeaderFld->setLabel("Header");
        $this->add($hasHeaderFld);

        # separator
        $seps = ["a" => "Auto", "c" => ",", "sc" => ";", "t" => "{tab}", "sp" => "{space}"];
        $sepFld = new Select("sep", $seps);
        $sepFld->setLabel("Separator");
        $this->add($sepFld);
    }
}