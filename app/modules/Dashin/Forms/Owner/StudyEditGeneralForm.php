<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-13
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class StudyEditGeneralForm extends FormBase
{
    public function __construct(?array $options = null)
    {
        $this->options["consortium"] = isset($options["consortium"]) ? $options["consortium"] : null;
        $this->options["country"] = isset($options["country"]) ? $options["country"] : ["No option available"];
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {

        # start date
        $startDateFld = new Date("start_date");
        $startDateFld->setLabel("Start date");
        $this->add($startDateFld);
        
        # endpoint
        $primaryEndpointFld = new TextArea("endpoint",[
            "placeholder" => "Primary endpoint"
        ]);
        $primaryEndpointFld->setLabel("Primary endpoint");
        $this->add($primaryEndpointFld);
        
        # objectives
        $objectivesFld = new TextArea("objectives",[
            "placeholder" => "Objectives"
        ]);
        $objectivesFld->setLabel("Objectives");
        $this->add($objectivesFld);
        
        # central conclusion
        $conclusionFld = new TextArea("conclusion",[
            "placeholder" => "Central conclusion"
        ]);
        $conclusionFld->setLabel("Central conclusion");
        $this->add($conclusionFld);

        # exclusion
        $exclusionFld = new TextArea("exclusion",[
            "placeholder" => "Exclusion criteria"
        ]);
        $exclusionFld->setLabel("Exclusion criteria");
        $this->add($exclusionFld);

        # inclusion
        $inclusionFld = new TextArea("inclusion",[
            "placeholder" => "Inclusion criteria"
        ]);
        $inclusionFld->setLabel("Inclusion criteria");
        $this->add($inclusionFld);
        
        # institute
        $instituteFld = new Text("institute",[
            "placeholder" => "Institute",
        ]);
        $instituteFld->setLabel("Institute");
        $this->add($instituteFld);
        
        # country
        $countryChoices = $this->options["country"];
        if(!is_array($countryChoices)) {
            $countryChoices = ["No option available"];
        }
        $countryFld = new Select("country",$countryChoices);
        $countryFld->setLabel("Country");
        $this->add($countryFld);

        #consortium
        $consortiumChoices = $this->options["consortium"];
        if(!is_array($consortiumChoices)) {
            $consortiumChoices = ["No option available"];
        }
        $consortiumFld = new Select("consortium",$consortiumChoices);
        $consortiumFld->setLabel("Consortium");
        $this->add($consortiumFld);
        
        # published
        $publishedFld = new Text("published",[
            "placeholder" => "Published (PubMedID)",
        ]);
        $publishedFld->setLabel("Published (PubMedID)");
        $this->add($publishedFld);
    }
}