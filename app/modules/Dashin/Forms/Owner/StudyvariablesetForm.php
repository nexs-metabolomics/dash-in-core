<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Text;

class StudyvariablesetForm extends FormBase
{
    public function initialize()
    {
        $nameFld = new Text("name", [
            "placeholder" => "Name",
        ]);
        $nameFld->setLabel("Name");
        $this->add($nameFld);

        $descriptionFld = new Text("description", [
            "placeholder" => "Description",
        ]);
        $descriptionFld->setLabel("Description");
        $this->add($descriptionFld);

    }
}