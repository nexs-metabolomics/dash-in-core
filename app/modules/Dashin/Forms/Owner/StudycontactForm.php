<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class StudycontactForm extends FormBase
{
    public function __construct(?array $options = null)
    {
        $this->options["study_role_options"] = isset($options["study_role_options"]) ? $options["study_role_options"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
        $fnameFld = new Text("first_name", [
            "placeholder" => "First name",
        ]);
        $fnameFld->setLabel("First name");
        $this->add($fnameFld);

        $lnameFld = new Text("last_name", [
            "placeholder" => "Last name",
        ]);
        $lnameFld->setLabel("Last name");
        $this->add($lnameFld);

        $emailFld = new Text("email", [
            "placeholder" => "Email",
        ]);
        $emailFld->setLabel("Email");
        $this->add($emailFld);

        $descFld = new TextArea("description", [
            "placeholder" => "Description",
        ]);
        $descFld->setLabel("Description");
        $this->add($descFld);

        $studyRoleOpt = $this->options["study_role_options"];
        $studyRoleFld = new Select("study_role", $studyRoleOpt);
        $studyRoleFld->setLabel("Study role");
        $this->add($studyRoleFld);

    }
}