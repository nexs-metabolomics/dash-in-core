<?php

namespace Dashin\Forms\Owner;

use App\Library\FormElements\PlainText;
use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\TextArea;

class SupportFileEditForm extends FormBase
{

    public function initialize()
    {
        $pltxtFld = new PlainText("name",[
            "tag" =>"div",
            "class" => "card-column",
        ]);
        $pltxtFld->setLabel("Name");
        $this->add($pltxtFld);
        
        $descFld = new TextArea("description",[
            "placeholder" => "Description"
        ]);
        $descFld->setLabel("Description");
        $this->add($descFld);
        

    }
}