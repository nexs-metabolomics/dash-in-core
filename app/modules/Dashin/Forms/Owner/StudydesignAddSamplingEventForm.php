<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use App\Library\FormElements\PlainText;
use Dashin\Forms\Owner\SamplingeventSubformExcretionForm;
use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\StringLength;

class StudydesignAddSamplingEventForm extends FormBase
{
    protected $_options;

    public function __construct(?array $options = null)
    {
//        $this->options["is_edit"] = isset($options["is_edit"]) ? $options["is_edit"] : false;
//        $this->options["sampling_type_id"] = isset($options["sampling_type_id"]) ? $options["sampling_type_id"] : false;
        $this->options["sampling_type"] = isset($options["sampling_type"]) ? $options["sampling_type"] : null;
//        $this->options["selection_choices"] = isset($options["selection_choices"]) ? $options["selection_choices"] : null;
//        $this->options["additional_fields"] = isset($options["additional_fields"]) ? $options["additional_fields"] : null;
        parent::__construct(null, $this->options);
    }
    
    public function initialize()
    {
        # name
        $nameFld = new Text("name", [
            "placeholder" => "Name",
            "required" => true,
        ]);
        $nameFld->setLabel("Name");
        $nameFld->addValidators([
            new StringLength([
                "min" => 1,
                "messageMinimum" => "Name is required",
            ]),
        ]);
        $this->add($nameFld);

        # label
        $labelFld = new Text("label", [
            "placeholder" => "Label",
        ]);
        $labelFld->setLabel("Label");
        $this->add($labelFld);

        # description
        $descFld = new TextArea("description", [
            "placeholder" => "Description",
        ]);
        $descFld->setLabel("Description");
        $this->add($descFld);

        
        # 
        # TODO: Change sampling type after creation - does this cause problems?
        # 
        # if($this->options["is_edit"] && $this->options["sampling_type_id"] !== false) {
        #     $stName = $this->options["sampling_type"][$this->options["sampling_type_id"]];
        #     $samplingTypeFld = new PlainText("sampling_type",["content" => $stName]);
        # } else {
            $samplingTypeChoices = $this->options["sampling_type"];
            if (!is_array($samplingTypeChoices)) {
                $samplingTypeChoices = ["No option available"];
            }
            $samplingTypeFld = new Select("sampling_type", $samplingTypeChoices, [
                "onchange" => "this.form.submit()"
            ]);
            $samplingTypeFld->setLabel("Sampling type");
        # }
        $this->add($samplingTypeFld);

//        # time_unit_id -> Select (default = 2)
//        $timeUnitChoices = $this->options["time_unit"];
//        if (!is_array($timeUnitChoices)) {
//            $timeUnitChoices = ["No time unit available"];
//        }
//        $timeUnitFld = new Select("time_unit", $timeUnitChoices);
//        $timeUnitFld->setLabel("Time unit");
//        $timeUnitFld->setDefault(2);
//        $this->add($timeUnitFld);
//
//        # start_time
//        $startTimeFld = new Numeric("start_time", [
//            "placeholder" => "Start time"
//        ]);
//        $startTimeFld->setLabel("Start time");
//        $this->add($startTimeFld);
//
//        # duration
//        $durationFld = new Numeric("duration", [
//            "placeholder" => "Duration"
//        ]);
//        $durationFld->setLabel("Duration");
//        $this->add($durationFld);

        # row_comment
        $commentFld = new TextArea("row_comment", [
            "placeholder" => "Comment",
        ]);
        $descFld->setLabel("Comment");
        $this->add($commentFld);

        # additional
//        $this->_additionalFields();
//        $this->_addForm();

    }
    
    private function _addForm()
    {
        if(in_array($this->options["sampling_type_id"],[1,2,3,4])) {
            if($this->options["sampling_type_id"] === "1") {
                $subform = new SamplingeventSubformExcretionForm();
                foreach ($subform as $e) {
                    $this->add($e);
                }
            }
        }
    }
    
    private function _additionalFields()
    {
        if ($this->options["additional_fields"]) {
            $fields = $this->options["additional_fields"];
            $ordinal = array_column($fields,"ordinal");
            array_multisort($ordinal,$fields);
            if (is_array($fields)) {
                foreach ($fields as $fieldName => $field) {
                    if ($field["type"] === "select") {
                        $sourceChoices = $this->options["selection_choices"];
                        $choicesIn = array_intersect_key($sourceChoices,
                            array_flip(array_keys(array_column($sourceChoices, "selection_type_id"), $field["sourceid"])));
                        $tmpChoices = [];
                        if (is_array($choicesIn)) {
                            $tmpChoices = array_combine(
                                array_column($choicesIn, "selection_choice_id"),
                                array_column($choicesIn, "name")
                            );
                        }
                        $tmpFld = new Select($fieldName, $tmpChoices);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    } elseif ($field["type"] === "text") {
                        $tmpFld = new Text($fieldName, [
                            "placeholder" => $field["label"]
                        ]);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    } elseif ($field["type"] === "plaintext") {
                        $tmpFld = new PlainText($fieldName, [
                            "content" => $field["content"] ?? null
                        ]);
                        $tmpFld->setLabel($field["label"]);
                        $this->add($tmpFld);
                    }
                }

            }
        }
    }
}