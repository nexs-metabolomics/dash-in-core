<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\NameDescriptionForm;
use Phalcon\Forms\Element\Select;

class CreateAssayForm extends NameDescriptionForm
{
    public function __construct(?array $options = null)
    {
        $this->options["researchfield"] = isset($options["researchfield"]) ? $options["researchfield"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
        parent::initialize();

        $researchfieldChoices = $this->options["researchfield"];
        if(!is_array($researchfieldChoices)) {
            $researchfieldChoices = ["No option available"];
        }
        $researchfieldFld = new Select("researchfield",$researchfieldChoices);
        $researchfieldFld->setLabel("Research type");
        $this->add($researchfieldFld);
    }
}