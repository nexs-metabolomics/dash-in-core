<?php

namespace Dashin\Forms\Owner;

use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\File;

class ImportSqdrForm extends FormBase
{
    public function initialize()
    {
        # file
        $fileFld = new File("file_up");
        $fileFld->setLabel("Select file");
        $fileFld->addFilter("sqdr");
        $this->add($fileFld);

    }
}