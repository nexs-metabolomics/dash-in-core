<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-16
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

class StudyEditResearchdesignForm extends FormBase
{
    public function __construct($options = null)
    {
        $this->options["researchdesign"] = isset($options["researchdesign"]) ? $options["researchdesign"] : null;
        parent::__construct(null, $this->options);
    }

    public function initialize()
    {
        # study design
        $studyDesignChoices = $this->options["researchdesign"];
        if(!is_array($studyDesignChoices)) {
            $studyDesignChoices = ["No option available"];
        }
        $studyDesignFld = new Select("researchdesign",$studyDesignChoices);
        $studyDesignFld->setLabel("Study design");
        $this->add($studyDesignFld);
        
        # number of treatments
        $numTreatFld = new Text("numtreat",[
            "placeholder" => "Number of treatments",
        ]);
        $numTreatFld->setLabel("Number of treatments");
        $this->add($numTreatFld);
        
        # number of factors
        $numFacFld = new Numeric("numfactor",[
            "placeholder" => "Number of factors",
        ]);
        $numFacFld->setLabel("Number of factors");
        $this->add($numFacFld);
        
        # number of arms
        $numArmFld = new Numeric("numarm",[
            "placeholder" => "Number of arms",
        ]);
        $numArmFld->setLabel("Number of arms");
        $this->add($numArmFld);
        
        # design description
        $studyDesignTxtFld = new TextArea("researchdesign_text",[
            "placeholder" => "Explanatory text (Give a textual account of the overall design leading to the total number of groups)",
        ]);
        $studyDesignTxtFld->setLabel("Study design (explanatory text)");
        $this->add($studyDesignTxtFld);
        
        # number of volunteers
        $numVolunFld = new Numeric("numvolun",[
            "placeholder" => "Number of volunteers",
        ]);
        $numVolunFld->setLabel("Number of volunteers");
        $this->add($numVolunFld);
        
        # number of volunteers terminating
        $numVolunTermFld = new Numeric("numvolun_term",[
            "placeholder" => "Number of volunteers terminating the study",
        ]);
        $numVolunTermFld->setLabel("Number of volunteers terminating the study");
        $this->add($numVolunTermFld);
        
        # recruitment start year
        $recruitStartYearFld = new Numeric("recruit_start_year",[
            "placeholder" => "Recruitment start year",
        ]);
        $recruitStartYearFld->setLabel("Recruitment start year");
        $this->add($recruitStartYearFld);
        
        # recruitment end year
        $recruitEndYearFld = new Numeric("recruit_end_year",[
            "placeholder" => "Recruitment end year",
        ]);
        $recruitEndYearFld->setLabel("Recruitment end year");
        $this->add($recruitEndYearFld);
        
        # blinding
        $blindFld = new Check("blinding");
        $blindFld->setLabel("Blinding");
        $this->add($blindFld);
        
        # blinding method
        $blindMethodFld = new Text("blinding_method",[
            "placeholder" => "Blinding method",
        ]);
        $blindMethodFld->setLabel("Blinding method");
        $this->add($blindMethodFld);
    }
}