<?php


namespace Dashin\Forms\Owner;


use Dashin\Forms\FormBase;
use Phalcon\Forms\Element\Select;

class SamplingeventSubformOrganForm extends FormBase
{
    public function initialize()
    {
        $timeUnitChoices = [
            "Select time unit"
            , "Seconds"
            , "Minutes"
            , "Hours"
            , "Days"
            , "Weeks"
            , "Years"
        ];
        $timeUnitFld = new Select("time_unit", $timeUnitChoices);
        $timeUnitFld->setLabel("Time unit");
        $this->add($timeUnitFld);

        $organSamplingChoices = [
            "Not defined"
            , "Needle biopsy"
            , "Incision"
            , "Other"
        ];
        $smplMethodFld = new Select("sampling_method", $organSamplingChoices);
        $smplMethodFld->setLabel("Sampling method");
        $this->add($smplMethodFld);
        
        $organChoices = [
            "Not defined"
            ,"Colon"
            ,"Adipose tissue"
            ,"Muscle"
            ,"Skin"
            ,"Bones"
            ,"Other"            
        ];
        $organFld = new Select("organ",$organChoices);
        $organFld->setLabel("Organ sample");
        $this->add($organFld);


    }
}
