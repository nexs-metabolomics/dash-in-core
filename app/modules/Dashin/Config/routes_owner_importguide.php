<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2023 dash-in team NEXS Copenhage University
 *
 * Created 2013-04-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$importguideRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Importguide",
        "view_base"  => "/modules/dashin/owner/importguide",
        "params"     => 1,
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$importguideRouterGroup->setPrefix("/dashin/owner/importguide");

$importguideRouterGroup->add(
    "/index(/)?",
    [
        "action" => "index",
    ]
);
$importguideRouterGroup->add(
    "/upload(/)?",
    [
        "action" => "uploadDatafile",
    ]
);
$importguideRouterGroup->add(
    "/import/:params",
    [
        "action" => "importData",
    ]
);
$importguideRouterGroup->add(
    "/importsummary/:params",
    [
        "action" => "importSummary",
    ]
);
$importguideRouterGroup->add(
    "/designedit/:params",
    [
        "action" => "editDatasetDesign",
    ]
);
$importguideRouterGroup->add(
    "/designview/:params",
    [
        "action" => "viewDatasetDesign",
    ]
);
$importguideRouterGroup->add(
    "/complete/:params",
    [
        "action" => "importguideComplete",
    ]
);

$router->mount($importguideRouterGroup);
