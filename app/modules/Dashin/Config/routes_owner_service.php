<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-16
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
#------------------------------
# dashin/owner/service
#------------------------------
$router->add(
    "/dashin/owner/service/select/([a-z]+)/:params",
    [
        "namespace" => 'Dashin\Controllers\Owner',
        "controller" => "Service",
        "action" => "select",
        "params" => 1,
        "seclab_id"     => SECLAB_ORG_READONLY,
    ]
);
#------------------------------
# dashin/owner/service - localsession
#------------------------------
$router->add(
    "/dashin/owner/service/select2/([a-z0-9]+)/:params",
    [
        "namespace" => 'Dashin\Controllers\Owner',
        "controller" => "Service",
        "action" => "select2",
        "params" => 1,
    ]
);


#-----------------------------------------------------------------------------------
$router->add(
    "/dashin/owner/service/selectseclab/:params",
    [
        "namespace" => 'Dashin\Controllers\Owner',
        "controller" => "Service",
        "action" => "selectSeclabel",
        "params" => 1,
    ]
);

