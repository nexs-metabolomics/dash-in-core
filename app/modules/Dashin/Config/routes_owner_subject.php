<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studydataRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Subject",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/subject",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);

$studydataRouterGroup->setPrefix("/dashin/owner/subject");
$studydataRouterGroup->add(
    "/index/:params",
    [
        "action"    => "index",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studydataRouterGroup->add(
    "/createstudysubject/:params",
    [
        "action" => "createStudysubject",
    ]
);
$studydataRouterGroup->add(
    "/liststudysubjects/:params",
    [
        "action"    => "listStudysubjects",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studydataRouterGroup->add(
    "/editstudysubject/:params",
    [
        "action" => "editStudysubject",
    ]
);

$router->mount($studydataRouterGroup);