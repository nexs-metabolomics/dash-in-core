<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

# admin
$adminRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Admin",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/admin",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$adminRouterGroup->setPrefix("/dashin/owner/admin");
$adminRouterGroup->add(
    "/index/:params",
    [
        "action"    => "index",
        "view_base" => "/modules/dashin/owner/admin",
    ]
);
$router->mount($adminRouterGroup);

#-------------------------------------------------------
# consortium
#-------------------------------------------------------
$consortiumRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Consortium",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/consortium",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$consortiumRouterGroup->setPrefix("/dashin/owner/consort");

$consortiumRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$consortiumRouterGroup->add(
    "/create/:params",
    [
        "action" => "create",
    ]
);
$consortiumRouterGroup->add(
    "/edit/:params",
    [
        "action" => "edit",
    ]
);
$consortiumRouterGroup->add(
    "/list/:params",
    [
        "action" => "list",
    ]
);

$router->mount($consortiumRouterGroup);
#-------------------------------------------------------
# assay
#-------------------------------------------------------
$assayRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Assay",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/assay",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$assayRouterGroup->setPrefix("/dashin/owner/assay");

$assayRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$assayRouterGroup->add(
    "/create/:params",
    [
        "action" => "create",
    ]
);
//$assayRouterGroup->add(
//    "/view/:params",
//    [
//        "action" => "view",
//    ]
//);
$assayRouterGroup->add(
    "/list/:params",
    [
        "action" => "list",
    ]
);
$assayRouterGroup->add(
    "/edit/:params",
    [
        "action" => "edit",
    ]
);
$assayRouterGroup->add(
    "/fileupload/:params",
    [
        "action" => "supportFileUpload",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$assayRouterGroup->add(
    "/fileedit/:params",
    [
        "action" => "supportFileEdit",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$assayRouterGroup->add(
    "/filelist/:params",
    [
        "action" => "supportFileList",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$router->mount($assayRouterGroup);
#--------------------------------------------------------------------------
# datacolumntype
#--------------------------------------------------------------------------
$columntypeRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Datacolumntype",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/datacolumntype",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$columntypeRouterGroup->setPrefix("/dashin/owner/datacolumntype");

$columntypeRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$columntypeRouterGroup->add(
    "/create/:params",
    [
        "action" => "create",
    ]
);
$columntypeRouterGroup->add(
    "/edit/:params",
    [
        "action" => "edit",
    ]
);
$columntypeRouterGroup->add(
    "/view/:params",
    [
        "action" => "view",
    ]
);
$columntypeRouterGroup->add(
    "/list/:params",
    [
        "action" => "list",
    ]
);

$router->mount($columntypeRouterGroup);

#--------------------------------------------------------------------------
# prefixed units
#--------------------------------------------------------------------------
$router->add(
    "/dashin/owner/prfxunit/listprfxunits/:params",
    [
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Prefixedunit",
        "action"     => "listPrefixedunits",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/prefixedunit",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
