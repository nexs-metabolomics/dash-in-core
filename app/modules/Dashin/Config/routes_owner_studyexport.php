<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studyexportRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Studyexport",
        "view_base"  => "/modules/dashin/owner/studyexport",
        "params"     => 1,
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyexportRouterGroup->setPrefix("/dashin/owner/studyexport");

$studyexportRouterGroup->add(
    "/listconditions/:params",
    [
        "action"    => "listStudyvariableconditions",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studyexportRouterGroup->add(
    "/createcondition/:params",
    [
        "action" => "createStudyvariablecondition",
    ]
);
$studyexportRouterGroup->add(
    "/editcondition(/)?",
    [
        "action" => "editStudyvariablecondition",
    ]
);
$studyexportRouterGroup->add(
    "/createstudyrowset(/)?",
    [
        "action" => "createStudyrowset",
    ]
);
$studyexportRouterGroup->add(
    "/editstudyrowset(/)?",
    [
        "action" => "editStudyrowset",
    ]
);
$studyexportRouterGroup->add(
    "/viewstudyrowset(/)?",
    [
        "action"    => "viewStudyrowset",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studyexportRouterGroup->add(
    "/liststudyrowsets/:params",
    [
        "action"    => "listStudyrowsets",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studyexportRouterGroup->add(
    "/createstudyvarset(/)?",
    [
        "action" => "createStudyvariableset",
    ]
);
$studyexportRouterGroup->add(
    "/editstudyvarset/:params",
    [
        "action" => "editStudyvariableset",
    ]
);
$studyexportRouterGroup->add(
    "/liststudyvarsets/:params",
    [
        "action"    => "listStudyvariablesets",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studyexportRouterGroup->add(
    "/createstudyexportset(/)?",
    [
        "action" => "createStudyexportset",
    ]
);
$studyexportRouterGroup->add(
    "/editstudyexportset/:params",
    [
        "action" => "editStudyexportset",
    ]
);
$studyexportRouterGroup->add(
    "/viewstudyexportset(/)?",
    [
        "action"    => "viewStudyexportset",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$studyexportRouterGroup->add(
    "/liststudyexportsets(/)?",
    [
        "action"    => "listStudyexportsets",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
//$studyexportRouterGroup->add(
//    "/exportstudy(/)?",
//    [
//        "action"    => "wholestudyexportOld",
//        "seclab_id" => SECLAB_ORG_READONLY,
//    ]
//);
$studyexportRouterGroup->add(
    "/exportsqdr(/)?",
    [
        "action"    => "exportStudyAsSqdr",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);

$router->mount($studyexportRouterGroup);