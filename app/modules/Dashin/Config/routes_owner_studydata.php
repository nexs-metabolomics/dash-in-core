<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studydataRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Studydata",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/studydata",
        "seclab_id"  => SECLAB_ORG_READONLY,

    ]
);

$studydataRouterGroup->setPrefix("/dashin/owner/studydata");
$studydataRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$studydataRouterGroup->add(
    "/attachdataset/:params",
    [
        "action"    => "addDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$studydataRouterGroup->add(
    "/listdatasets/:params",
    [
        "action" => "listAddedDatasets",
    ]
);
$studydataRouterGroup->add(
    "/browsedata/:params",
    [
        "action" => "browseData",
    ]
);

$studydataRouterGroup->add(
    "/viewdatadesign/:params",
    [
        "action" => "viewDatadesign",
    ]
);
$router->mount($studydataRouterGroup);
