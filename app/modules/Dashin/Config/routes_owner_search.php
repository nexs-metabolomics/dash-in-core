<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-03-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$searchRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Search",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/search",
        "seclab_id"     => SECLAB_ORG_READONLY,
    ]
);
$searchRouterGroup->setPrefix("/dashin/owner/search");

#------------------------------
# dashin/owner/search
#------------------------------
$searchRouterGroup->add(
    "/default/:params",
    [
        "action" => "searchFrontPage",
    ]
);
$searchRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$searchRouterGroup->add(
    "/metabolomics/:params",
    [
        "action" => "searchMetabolomics", #
    ]
);
$searchRouterGroup->add(
    "/metabolomicstype02/:params",
    [
        "action" => "searchMetabolomicsType02", #
    ]
);
$searchRouterGroup->add(
    "/savemetabolomicstype02/:params",
    [
        "action" => "saveMetabolomicsType02",
    ]
);
$searchRouterGroup->add(
    "/savemetabolomics/:params",
    [
        "action" => "saveMetabolomics",
    ]
);
$searchRouterGroup->add(
    "/listresults/:params",
    [
        "action" => "listResultsets", #
    ]
);
$searchRouterGroup->add(
    "/viewresultset/:params",
    [
        "action" => "viewResultset", #
    ]
);
$searchRouterGroup->add(
    "/editresultdataset/:params",
    [
        "action" => "editResultsetDataset", #
    ]
);
$searchRouterGroup->add(
    "/anthropometry(/)?",
    [
        "action" => "searchAnthropometry",
    ]
);

$router->mount($searchRouterGroup);