<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-14
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studydesignRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Studydesign",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/studydesign",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);

$studydesignRouterGroup->setPrefix("/dashin/owner/studydesign");
/** @var $router */
$studydesignRouterGroup->add(
    "/index(/)?",
    [
        "action" => "viewStudydesign",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$studydesignRouterGroup->add(
    "/createcenter(/)?",
    [
        "action" => "createCenter",
    ]
);
$studydesignRouterGroup->add(
    "/editcenter(/)?",
    [
        "action" => "editCenter",
    ]
);
$studydesignRouterGroup->add(
    "/listcenters/:params",
    [
        "action" => "listCenters",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$studydesignRouterGroup->add(
    "/createstartgroup(/)?",
    [
        "action" => "createStartgroup",
    ]
);
$studydesignRouterGroup->add(
    "/editstartgroup(/)?",
    [
        "action" => "editStartgroup",
    ]
);
$studydesignRouterGroup->add(
    "/liststartgroups/:params",
    [
        "action" => "listStartgroups",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$studydesignRouterGroup->add(
    "/addstartgrouptosubject(/)?",
    [
        "action" => "addStartgroupToSubject",
    ]
);
$studydesignRouterGroup->add(
    "/listevents/:params",
    [
        "action" => "listEvents",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$studydesignRouterGroup->add(
    "/createevent(/)?",
    [
        "action" => "createEvent",
    ]
);
$studydesignRouterGroup->add(
    "/editevent(/)?",
    [
        "action" => "editEvent",
    ]
);
$studydesignRouterGroup->add(
    "/createsubevent(/)?",
    [
        "action" => "createSubevent",
    ]
);
$studydesignRouterGroup->add(
    "/editsubevent(/)?",
    [
        "action" => "editSubevent",
    ]
);
$studydesignRouterGroup->add(
    "/listsubevents/:params",
    [
        "action" => "listSubevents",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$studydesignRouterGroup->add(
    "/addgrouptosubevent(/)?",
    [
        "action" => "addStartgroupToSubevent",
    ]
);
$studydesignRouterGroup->add(
    "/createsamplingevent(/)?",
    [
        "action" => "createSamplingEvent",
    ]
);
$studydesignRouterGroup->add(
    "/editsampling(/)?",
    [
        "action" => "editSamplingevent",
    ]
);
$studydesignRouterGroup->add(
    "/listsamplingevents/:params",
    [
        "action" => "listSamplingevents",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
//$studydesignRouterGroup->add(
//    "/addsamplingtosubevent(/)?",
//    [
//        "action" => "addSamplingEventToSubevent",
//    ]
//);
$studydesignRouterGroup->add(
    "/importdatadesign(/)?",
    [
        "action" => "importDatadesign",
    ]
);
$studydesignRouterGroup->add(
    "/viewstudydesign(/)?",
    [
        "action" => "viewStudydesign",
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);

$router->mount($studydesignRouterGroup);