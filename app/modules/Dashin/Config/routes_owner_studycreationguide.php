<?php
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studyguideRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Studycreationguide",
        "view_base"  => "/modules/dashin/owner/studycreationguide",
        "params"     => 1,
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyguideRouterGroup->setPrefix("/dashin/owner/studycreate");

$studyguideRouterGroup->add(
    "/createfromdata/:params",
    [
        "action"    => "createStudyFromDataset",
    ]
);
$studyguideRouterGroup->add(
    "/createnew/:params",
    [
        "action"    => "createStudy",
    ]
);
$router->mount($studyguideRouterGroup);