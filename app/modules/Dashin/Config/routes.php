<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
$router->add(
    "/dashin/index/:params",
    [
        "namespace" => 'Dashin\Controllers',
        "controller" => "Dashin",
        "action" => "index",
        "params" => 1,
        "seclab_id"     => SECLAB_ALL,
    ]
);
#-------------------------------------------------------
# dashin/owner/import
#-------------------------------------------------------
include __DIR__ . "/routes_owner_import.php";
include __DIR__ . "/routes_owner_importguide.php";
#-------------------------------------------------------
# dashin/owner/varinfo
#-------------------------------------------------------
include __DIR__ . "/routes_owner_varinfo.php";

#------------------------------
# dashin/owner/study
#------------------------------
include __DIR__ . "/routes_owner_study.php";
include __DIR__ . "/routes_owner_studydesign.php";
#------------------------------
# study related
#------------------------------
include __DIR__ . "/routes_owner_studydata.php";
#------------------------------
include __DIR__ . "/routes_owner_subject.php";
#------------------------------
include __DIR__ . "/routes_owner_studyexport.php";
include __DIR__ . "/routes_owner_studycreationguide.php";

#------------------------------
# dashin/owner -> admin
#------------------------------
include __DIR__ . "/routes_owner_admin.php";

#------------------------------
# dashin/owner/service
#------------------------------
include __DIR__ . "/routes_owner_service.php";

#------------------------------
# dashin/owner/search
#------------------------------
include __DIR__ . "/routes_owner_search.php";
