<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$varinfoRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Varinfo",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/varinfo",
        "seclab_id"  => SECLAB_ORG_READONLY,

    ]
);
$varinfoRouterGroup->setPrefix("/dashin/owner/varinfo");
$varinfoRouterGroup->add(
    "/index(/)?",
    [
        "action" => "index",
    ]
);
$varinfoRouterGroup->add(
    "/matchindex(/)?",
    [
        "action" => "matchIndex",
    ]
);
$varinfoRouterGroup->add(
    "/uploadvarinfo(/)?",
    [
        "action"    => "uploadUpvarDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$varinfoRouterGroup->add(
    "/viewupvarv(/)?",
    [
        "action" => "listUpvarVariables",
    ]
);
$varinfoRouterGroup->add(
    "/upvarreport(/)?",
    [
        "action" => "viewUpvarDataset",
    ]
);
$varinfoRouterGroup->add(
    "/upvaredit(/)?",
    [
        "action"    => "editUpvarDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$varinfoRouterGroup->add(
    "/browseupvardata/:params",
    [
        "action" => "browseUpvarData",
    ]
);
$varinfoRouterGroup->add(
    "/listupvardatasets/:params",
    [
        "action" => "listUpvarDatasets",
    ]
);
$varinfoRouterGroup->add(
    "/match(/)?",
    [
        "action"    => "matchVarinfo",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$varinfoRouterGroup->add(
    "/matchsummary(/)?",
    [
        "action" => "viewVarDataset",
    ]
);
$varinfoRouterGroup->add(
    "/varedit(/)?",
    [
        "action"    => "editVarDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$varinfoRouterGroup->add(
    "/listvardatasets/:params",
    [
        "action" => "listVarDatasets",
    ]
);
$varinfoRouterGroup->add(
    "/listvdvariables/:params",
    [
        "action"    => "listVarVariables",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$varinfoRouterGroup->add(
    "/browsevardata(/)?",
    [
        "action" => "browseVarData",
    ]
);
$varinfoRouterGroup->add(
    "/mapsearchcols/:params",
    [
        "action"    => "mapSearchColumns",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$router->mount($varinfoRouterGroup);