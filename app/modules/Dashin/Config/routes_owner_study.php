<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$studyRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Study",
        "params"     => 1,
        "view_base"  => "/modules/dashin/owner/study",
        "seclab_id"  => SECLAB_ORG_READONLY,

    ]
);

$studyRouterGroup->setPrefix("/dashin/owner/study");
$studyRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$studyRouterGroup->add(
    "/studyindex/:params",
    [
        "action" => "studyIndex",
    ]
);
$studyRouterGroup->add(
    "/create/:params",
    [
        "action" => "create",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
//$studyRouterGroup->add(
//    "/editname/:params",
//    [
//        "action" => "editName",
//    ]
//);
$studyRouterGroup->add(
    "/editgeneral/:params",
    [
        "action" => "editGeneral",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/import/:params",
    [
        "action" => "importSqdr",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/editpermissions/:params",
    [
        "action" => "editPermissions",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
//$studyRouterGroup->add(
//    "/editstudydesign/:params",
//    [
//        "action"     => "editResearchdesign",
//    ]
//);
$studyRouterGroup->add(
    "/list/:params",
    [
        "action" => "list",
    ]
);
$studyRouterGroup->add(
    "/viewgeneral/:params",
    [
        "action" => "studySummary",
    ]
);
$studyRouterGroup->add(
    "/summary/:params",
    [
        "action" => "studySummary",
    ]
);
$studyRouterGroup->add(
    "/summarygeneral/:params",
    [
        "action" => "studySummaryGeneral",
    ]
);
$studyRouterGroup->add(
    "/summarydesign/:params",
    [
        "action" => "studySummaryDesign",
    ]
);
$studyRouterGroup->add(
    "/summarycontacts/:params",
    [
        "action" => "studySummaryContacts",
    ]
);
$studyRouterGroup->add(
    "/summarydatasets/:params",
    [
        "action" => "studySummaryDatasets",
    ]
);
$studyRouterGroup->add(
    "/viewstudydesign/:params",
    [
        "action" => "viewStudyDesign",
    ]
);
$studyRouterGroup->add(
    "/viewdatasets/:params",
    [
        "action" => "viewDatasets",
    ]
);
$studyRouterGroup->add(
    "/removedatasets/:params",
    [
        "action" => "removeDatasets",
    ]
);
$studyRouterGroup->add(
    "/createstudycontact/:params",
    [
        "action" => "createStudycontact",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/editstudycontact/:params",
    [
        "action" => "editStudycontact",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/liststudycontacts/:params",
    [
        "action" => "listStudycontacts",
    ]
);
$studyRouterGroup->add(
    "/fileupload/:params",
    [
        "action" => "supportFileUpload",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/fileedit/:params",
    [
        "action" => "supportFileEdit",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$studyRouterGroup->add(
    "/filelist/:params",
    [
        "action" => "supportFileList",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);

$router->mount($studyRouterGroup);