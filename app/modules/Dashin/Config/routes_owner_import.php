<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

$importRouterGroup = new Group([
        "namespace"  => 'Dashin\Controllers\Owner',
        "controller" => "Import",
        "view_base"  => "/modules/dashin/owner/import",
        "params"     => 1,
        "seclab_id"  => SECLAB_ORG_READONLY,
    ]
);
$importRouterGroup->setPrefix("/dashin/owner/import");
#-------------------------------------------------------
# import
#-------------------------------------------------------
$importRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
//$importRouterGroup->add(
//    "/dataset(/)?",
//    [
//        "action" => "datasetIndex",
//    ]
//);
$importRouterGroup->add(
    "/uploadsummary(/)?",
    [
        "action" => "viewDataset",
    ]
);
$importRouterGroup->add(
    "/listdatasets/:params",
    [
        "action"    => "listDatasets",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$importRouterGroup->add(
    "/deletedataset(/)?",
    [
        "action"    => "deleteDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/uploaddata(/)?",
    [
        "action"    => "uploadDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/editdataset(/)?",
    [
        "action"    => "editDataset",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/listvariables/:params",
    [
        "action"    => "listVariables",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/editvariable(/)?",
    [
        "action"    => "editVariable",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/browsedata/:params",
    [
        "action"    => "browseData",
        "seclab_id" => SECLAB_ORG_READONLY,
    ]
);
$importRouterGroup->add(
    "/viewdatadesign/:params",
    [
        "action"    => "viewDatadesign",
        "seclab_id" => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/fileupload/:params",
    [
        "action" => "supportFileUpload",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/fileedit/:params",
    [
        "action" => "supportFileEdit",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
$importRouterGroup->add(
    "/filelist/:params",
    [
        "action" => "supportFileList",
        "seclab_id"  => SECLAB_ORG_USER,
    ]
);
# MOUNT IMPORT GROUP
#-------------------------------------------------------
$router->mount($importRouterGroup);
#-------------------------------------------------------
