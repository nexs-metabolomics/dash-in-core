<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\StudyHelper;

class StudyController extends OwnerControllerBase
{

    /**
     * @var StudyHelper
     */
    private $_helper;

    /**
     * @return StudyHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new StudyHelper();
        }

        return $this->_helper;
    }

    /**
     * Main menu for study submenu
     */
    public function indexAction()
    {

        $this->toggleSubmenu();
    }

    /**
     * Main menu for study submenu
     */
    public function studyIndexAction()
    {
    }

    /**
     * Create new study
     */
    public function createAction()
    {

        $results = $this->_getHelper()->createStudy();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Edit study metadata
     * Edit general properties of study
     *
     * @return bool
     */
    public function editGeneralAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/editgeneral/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $togglestateGeneral = $this->request->getPost("togglestate_general", null, "closed");
        $togglestateDesign = $this->request->getPost("togglestate_design", null, "closed");

        if ($action === "togglegeneral") {
            $btnStudy = $this->Btn->getValue();
            if ($btnStudy === "open") {
                $togglestateGeneral = "open";
            } else {
                $togglestateGeneral = "closed";
            }
        } elseif ($action === "toggledesign") {
            $btnMetbol = $this->Btn->getValue();
            if ($btnMetbol === "open") {
                $togglestateDesign = "open";
            } else {
                $togglestateDesign = "closed";
            }
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->editGeneral($studyId);
        if (isset($results["form_namedesc"]) && $results["form_namedesc"]) {
            $this->view->form_namedesc = $results["form_namedesc"];
        }
        if (isset($results["form_general"]) && $results["form_general"]) {
            $this->view->form_general = $results["form_general"];
        }
        if (isset($results["form_studydesign"]) && $results["form_studydesign"]) {
            $this->view->form_studydesign = $results["form_studydesign"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if ($togglestateGeneral === "open") {
            $this->view->details_general = "open";
        } else {
            $this->view->details_general = "closed";
        }
        if ($togglestateDesign === "open") {
            $this->view->details_design = "open";
        } else {
            $this->view->details_design = "closed";
        }
    }

    public function importSqdrAction()
    {

        return;
        $results = $this->_getHelper()->importSqdr();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }
    
    /**
     * Edit permissions for study and study elements
     * 
     * @return false|void
     */
    public function editPermissionsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/editpermissions/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        } 

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        
        if ($action === "export_study") {
            $this->_getHelper()->studyExport($studyId);
        }
        
        $results = $this->_getHelper()->editPermissions($studyId);
        if (isset($results["study_status"]) && $results["study_status"]) {
            $this->view->study_status = (object)$results["study_status"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["datasets"]) && $results["datasets"]) {
            $this->view->datasets = $results["datasets"];
        }
        if (isset($results["supportfiles"]) && $results["supportfiles"]) {
            $this->view->supportfiles = $results["supportfiles"];
        }

    }

    /**
     * List availabe studies
     *
     * @param int  $page
     * @param null $reset
     *
     * @return bool
     */
    public function listAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listStudies($page, $nrows, $reset);
        if ($this->response->isSent()) {
            return false;
        }
        if (isset($results["studies"]) && $results["studies"]) {
            $this->view->pagination = $results["studies"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * View study metadata
     * study-generate information
     *
     * @return bool
     */
    public function studySummaryAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/summary/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->studySummary($studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["study_general"]) && $results["study_general"]) {
            $this->view->study_general = (object)$results["study_general"];
        }
        if (isset($results["researchdesign"]) && $results["researchdesign"]) {
            $this->view->researchdesign = (object)$results["researchdesign"];
        }
        if (isset($results["studycontacts"]) && $results["studycontacts"]) {
            $this->view->studycontacts = $results["studycontacts"];
        }
        if (isset($results["study_datasets"]) && $results["study_datasets"]) {
            $this->view->study_datasets = $results["study_datasets"];
        }
    }

    /**
     * View study metadata
     * study-generate information
     *
     * @return bool
     */
    public function studySummaryGeneralAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/summarygeneral/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->studySummary($studyId, "general");
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["study_general"]) && $results["study_general"]) {
            $this->view->study_general = (object)$results["study_general"];
        }
    }

    /**
     * View study metadata
     * study-generate information
     *
     * @return bool
     */
    public function studySummaryDesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/summarydesign/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->studySummary($studyId, "design");
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["researchdesign"]) && $results["researchdesign"]) {
            $this->view->researchdesign = (object)$results["researchdesign"];
        }
    }

    /**
     * View study metadata
     * study-generate information
     *
     * @return bool
     */
    public function studySummaryContactsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/summarycontacts/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->studySummary($studyId, "contacts");
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studycontacts"]) && $results["studycontacts"]) {
            $this->view->studycontacts = $results["studycontacts"];
        }
    }

    /**
     * View study metadata
     * study-generate information
     *
     * @return bool
     */
    public function studySummaryDatasetsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/summarydatasets/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->studySummary($studyId, "datasets");
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["study_datasets"]) && $results["study_datasets"]) {
            $this->view->study_datasets = $results["study_datasets"];
        }
    }

    /**
     * View study metadata
     * study design information
     *
     * @return bool
     */
    public function viewStudyDesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/viewstudydesign/", "dashin_owner_study_id", "study");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->viewStudyDesign($studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["researchdesign"]) && $results["researchdesign"]) {
            $this->view->researchdesign = (object)$results["researchdesign"];
        }
    }

    /**
     * List the datasets added to the study
     *
     * @param int  $page
     * @param null $reset
     *
     * @return bool
     */
    public function viewDatasetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/viewdatasets/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrows = 20;

        $results = $this->_getHelper()->viewDatasets($page, $nrows, $reset, $studyId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    /**
     * Remove a dataset from a study
     *
     * @return false|void
     */
    public function removeDatasetsAction()
    {

        $page = (int)$this->dispatcher->getParam(0, null, 1);
        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/viewdatasets/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $nrows = 20;

        $results = $this->_getHelper()->removeDatasets($page, $nrows, $reset, $studyId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    /**
     * Create a new study contact
     *
     * @return false|void
     */
    public function createStudycontactAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/createstudycontact/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->createStudcontact($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    /**
     * edit a study contact
     *
     * @return false|void
     */
    public function editStudycontactAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/editstudycontact/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studycontact") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/editstudycontact/", "dashin_owner_studycontact_id", "studycontact");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studycontactId = $this->localsession->getKeyValue("dashin_owner_studycontact_id");

        $results = $this->_getHelper()->editStudcontact($studyId, $studycontactId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studycontact_info"]) && $results["studycontact_info"]) {
            $this->view->studycontact_info = (object)$results["studycontact_info"];
        }
    }

    /**
     * Show a list of study contacts withn a study
     *
     * @return false|void
     */
    public function listStudycontactsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/liststudycontacts/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->listStudycontacts($studyId);
        if ($this->response->isSent()) {
            return false;
        }
        if (isset($results["studycontacts"]) && $results["studycontacts"]) {
            $this->view->studycontacts = $results["studycontacts"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }

        # initialize with null if not already set (used in the delete procedure)
        if (!isset($this->view->prevent_delete_token)) {
            $this->view->prevent_delete_token = null;
        }
    }

    /**
     * Upload a file related to a study
     *
     * @return false|void
     */
    public function supportFileUploadAction()
    {

        # Check for select-study action
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/fileupload/", "dashin_owner_study_id", "study");
        }

        # get active study_id from localsession
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        # results from helper functions
        $results = $this->_getHelper()->supportFileUpload($studyId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }

    }

    /**
     * Edit the description of a support file in a study
     *
     * @return void
     */
    public function supportFileEditAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/fileedit/", "dashin_owner_study_id", "study");
        }
        if ($action === "select_file") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/fileedit/", "dashin_owner_study_supportfile_id", "studyfile");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $supportfileId = $this->localsession->getKeyValue("dashin_owner_study_supportfile_id");

        $results = $this->_getHelper()->supportFileEdit($studyId, $supportfileId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["supportfile_info"]) && $results["supportfile_info"]) {
            $this->view->supportfile_info = (object)$results["supportfile_info"];
        }
    }

    /**
     * Show a list of support files in a study
     *
     * @return false|void
     */
    public function supportfileListAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/study/filelist/", "dashin_owner_study_id", "study");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->supportFileList($studyId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyfile_list"]) && $results["studyfile_list"]) {
            $this->view->studyfile_list = $results["studyfile_list"];
        }
    }

}