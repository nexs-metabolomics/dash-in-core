<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-18
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\StudydataHelper;

/**
 * StudydataController
 *
 * Entry points operations relating to data handling and
 * datasets belonging to a study
 *
 */
class StudydataController extends OwnerControllerBase
{

    /**
     * @var StudydataHelper
     */
    private $_helper;

    /**
     * @return StudydataHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new StudydataHelper();
        }

        return $this->_helper;
    }

    /**
     * Main menu for study-data
     */
    public function indexAction()
    {
    }

    /**
     * Browse the data of the added dataset (studydata)
     *
     * @param int $rowStart
     * @param int $colStart
     *
     * @return bool
     */
    public function browseDataAction()
    {

        $rowStart = (int)$this->dispatcher->getParam(0, null, 1);
        $colStart = (int)$this->dispatcher->getParam(1, null, 1);

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/browsedata/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/browsedata/", "dashin_owner_dataset_id", "studydataset");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $this->view->dataset_id = null;

        $rowStart = (int)$rowStart;
        $colStart = (int)$colStart;
        $rowLimit = 20;
        $colLimit = 7;

        $this->view->has_results = false;
        $results = $this->_getHelper()->browseData($datasetId, $studyId, $rowStart, $rowLimit, $colStart, $colLimit);
        if (isset($results["data"]["datarows"]) && $results["data"]["datarows"]) {
            $this->view->pagination_rows = $results["data"]["datarows"];
            $this->view->pagination_cols = $results["data"]["datacols"];
            $this->view->has_results = true;
        }
        if (isset($results["study"]) && $results["study"]) {
            $this->view->study_info = (object)$results["study"];
        }
        if (isset($results["dataset"]) && $results["dataset"]) {
            $this->view->dataset_info = (object)$results["dataset"];
        }
    }

    /**
     * List the datasets added to the study
     *
     * @param int  $page
     * @param null $reset
     *
     * @return bool
     */
    public function listAddedDatasetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/listdatasets/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listAddedDatasets($page, $nrows, $reset, $studyId);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        if (isset($results["nodups"]) && $results["nodups"]) {
            $this->view->nodups = $results["nodups"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    /**
     * Review the design of dataset that is to be added to a study
     * TODO: Deprecate?
     *
     * @return false|void
     */
    public function viewDatadesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/viewdatadesign/", "dashin_owner_dataset_id", "dataset");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $results = $this->_getHelper()->viewDatadesign($datasetId);
        if (isset($results["design_summary"]) && $results["design_summary"]) {
            $this->view->design_summary = $results["design_summary"];
        }
        if (isset($results["event"]) && $results["event"]) {
            $this->view->event = $results["event"];
        }
        if (isset($results["subevent"]) && $results["subevent"]) {
            $this->view->subevent = $results["subevent"];
        }
        if (isset($results["samplingevent"]) && $results["samplingevent"]) {
            $this->view->samplingevent = $results["samplingevent"];
        }
        if (isset($results["count_full"]) && $results["count_full"]) {
            $this->view->count_full = $results["count_full"];
        }
        if (isset($results["count_distinct"]) && $results["count_distinct"]) {
            $this->view->count_distinct = $results["count_distinct"];
        }
        if (isset($results["samplingtime"]) && $results["samplingtime"]) {
            $this->view->samplingtime = $results["samplingtime"];
        }
        if (isset($results["startgroup"]) && $results["startgroup"]) {
            $this->view->startgroup = $results["startgroup"];
        }
        if (isset($results["subject"]) && $results["subject"]) {
            $this->view->subject = $results["subject"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["design_exists"]) && $results["design_exists"]) {
            $this->view->design_exists = $results["design_exists"];
        }
        if (isset($results["confirm_delete"]) && $results["confirm_delete"]) {
            $this->view->confirm_delete = $results["confirm_delete"];
        }
        if (isset($results["save_result"]) && $results["save_result"]) {
            $this->view->save_result = $results["save_result"];
        }
        if (isset($results["design_variable_names"]) && $results["design_variable_names"]) {
            $this->view->design_variable_names = $results["design_variable_names"];
        }
    }

    /**
     * Add a dataset to a study
     *
     * @return false|void
     */
    public function addDatasetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/attachdataset/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydata/attachdataset/", "dashin_owner_dataset_id", "dataset");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->addDataset($studyId, $datasetId);
        # @formatter:off
        if(isset($results["study_info"     ]) && $results["study_info"     ]){$this->view->study_info      = (object)$results["study_info"  ];}
        if(isset($results["dataset_info"   ]) && $results["dataset_info"   ]){$this->view->dataset_info    = (object)$results["dataset_info"];}
        if(isset($results["all_summary"    ]) && $results["all_summary"    ]){$this->view->all_summary     = $results["all_summary"         ];}
        if(isset($results["design_is_valid"]) && $results["design_is_valid"]){$this->view->design_is_valid = $results["design_is_valid"     ];}
        if(isset($results["study_matched"  ]) && $results["study_matched"  ]){$this->view->study_matched   = $results["study_matched"       ];}
        if(isset($results["study_unmatched"]) && $results["study_unmatched"]){$this->view->study_unmatched = $results["study_unmatched"     ];}
        if(isset($results["data_unmatched" ]) && $results["data_unmatched" ]){$this->view->data_unmatched  = $results["data_unmatched"      ];}
        if(isset($results["data_matched"   ]) && $results["data_matched"   ]){$this->view->data_matched    = $results["data_matched"        ];}
        if(isset($results["confirm_import" ]) && $results["confirm_import" ]){$this->view->confirm_import  = $results["confirm_import"      ];}
        if(isset($results["confirm_remove" ]) && $results["confirm_remove" ]){$this->view->confirm_remove  = $results["confirm_remove"      ];}
        # @formatter:on
    }

}