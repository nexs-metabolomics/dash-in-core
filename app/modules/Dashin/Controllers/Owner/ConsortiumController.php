<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-12-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\ConsortiumHelper;
use phpDocumentor\Reflection\Types\False_;

/**
 * Class ConsortiumController
 *
 * Define consortium (resposible participans in study)
 *
 * @package Dashin\Controllers\Owner
 */
class ConsortiumController extends OwnerControllerBase
{
    /**
     * @var ConsortiumHelper
     */
    private $_helper;

    /**
     * @return ConsortiumHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new ConsortiumHelper();
        }
        return $this->_helper;
    }

    /**
     * Main consortium menu
     */
    public function indexAction()
    {
    }

    /**
     * Create a new consortium
     */
    public function createAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/consort/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $results = $this->_getHelper()->createConsortium();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Edit name, description of consortium
     *
     * @return bool
     */
    public function editAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "select_consort") {
            $this->_getHelper()->serviceSelect("/dashin/owner/consort/edit/", "dashin_owner_consortium_id", "consortium");
        } elseif ($action === "cancel") {
            $this->response->redirect("/dashin/owner/consort/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $consortiumId = $this->localsession->getKeyValue("dashin_owner_consortium_id");

        $results = $this->_getHelper()->editConsortium($consortiumId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["consortium"]) && $results["consortium"]) {
            $this->view->consortium_info = (object)$results["consortium"];
        }
    }

    /**
     * List availabe consortiums
     * Delete consortium
     *
     * @param int  $page
     * @param null $reset
     */
    public function listAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $consortiumId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($consortiumId)) {
                $this->localsession->setKeyValue("dashin_owner_consortium_id", $consortiumId);
                $this->response->redirect("/dashin/owner/consort/edit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listConsortiums($page, $nrows, $reset);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["consortiums"]) && $results["consortiums"]) {
            $this->view->pagination = $results["consortiums"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }
}