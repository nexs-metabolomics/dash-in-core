<?php

namespace Dashin\Controllers\Owner;

use Dashin\Controllers\ControllerBase;
use Dashin\Helpers\Owner\StudycreationguideHelper;

class StudycreationguideController extends ControllerBase
{

    /**
     * @var StudycreationguideHelper
     */
    private $_helper;

    /**
     * @return StudycreationguideHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new StudycreationguideHelper();
        }

        return $this->_helper;
    }

    public function indexAction()
    {
    }

    public function guideCreateStudyFromDatasetAction()
    {
        $datasetId = $this->localsession->getKeyValue("dashin_owner_study_guide_dataset_id");
        $results = $this->_getHelper()->createStudyFromDataset($datasetId);
    }

    public function guideAddDatasetAction()
    {
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        
    }

}