<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\AssayHelper;

/**
 * Class AssayController
 *
 * Define assays
 *
 * @package Dashin\Controllers\Owner
 */
class AssayController extends OwnerControllerBase
{
    /**
     * @var AssayHelper
     */
    private $_helper;

    /**
     * @return AssayHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new AssayHelper();
        }
        return $this->_helper;
    }

    /**
     * Main assay menu
     */
    public function indexAction()
    {
    }

    /**
     * Create a new assay
     */
    public function createAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/assay/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        
        $results = $this->_getHelper()->createAssay();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Edit name description and researchfield of an assay
     */
    public function editAction()
    {
        # validate or select seclabel
        $action = $this->Btn->getAction();

        # select seclabel
        if ($action === "select_assay") {
            $this->_getHelper()->serviceSelect("/dashin/owner/assay/edit/", "dashin_owner_assay_id", "assay");
            if ($this->response->isSent()) {
                return;
            }
        } elseif ($action === "cancel") {
            $this->response->redirect("/dashin/owner/assay/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $assayId = $this->localsession->getKeyValue("dashin_owner_assay_id");

        $results = $this->_getHelper()->editAssay($assayId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
            $this->view->seclabel_id = $assayId;
            $this->localsession->setKeyValue("dashin_owner_assay_id", $assayId);
        }
        if (isset($results["assay"]) && $results["assay"]) {
            $this->view->assay_info = (object)$results["assay"];
        }
    }

    /**
     * List available assays
     * Delete assay
     *
     * @param int  $page
     * @param null $reset
     */
    public function listAction()
    {
        $page = (int)$this->dispatcher->getParam(0,null,1);
        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $assayId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($assayId)) {
                $this->localsession->setKeyValue("dashin_owner_assay_id", $assayId);
                $this->response->redirect("/dashin/owner/assay/edit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }
        $nrows = 20;

        $results = $this->_getHelper()->listAssays($page, $nrows, $reset);
        # FIXME: temporary use count(items) to identify valid/empty results
        $n = count($results["assays"]->items);

        if (isset($results["assays"]) && $n>0) {
            $this->view->pagination = $results["assays"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        $this->view->current_page = $page;
    }


    /**
     * Upload a file related to a assay
     *
     * @return false|void
     */
    public function supportFileUploadAction()
    {

        # Check for select-assay action
        $action = $this->Btn->getAction();
        if ($action === "select_assay") {
            $this->_getHelper()->serviceSelect("/dashin/owner/assay/fileupload/", "dashin_owner_assay_id", "assay");
        }

        # get active assay_id from localsession
        $assayId = $this->localsession->getKeyValue("dashin_owner_assay_id");

        # results from helper functions
        $results = $this->_getHelper()->supportFileUpload($assayId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["assay_info"]) && $results["assay_info"]) {
            $this->view->assay_info = (object)$results["assay_info"];
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }

    }

    /**
     * Edit the description of a support file in a assay
     *
     * @return void
     */
    public function supportFileEditAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_assay") {
            $this->_getHelper()->serviceSelect("/dashin/owner/assay/fileedit/", "dashin_owner_assay_id", "assay");
        }
        if ($action === "select_file") {
            $this->_getHelper()->serviceSelect("/dashin/owner/assay/fileedit/", "dashin_owner_assay_supportfile_id", "assayfile");
        }

        $assayId = $this->localsession->getKeyValue("dashin_owner_assay_id");
        $supportfileId = $this->localsession->getKeyValue("dashin_owner_assay_supportfile_id");

        $results = $this->_getHelper()->supportFileEdit($assayId, $supportfileId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["assay_info"]) && $results["assay_info"]) {
            $this->view->assay_info = (object)$results["assay_info"];
        }
        if (isset($results["supportfile_info"]) && $results["supportfile_info"]) {
            $this->view->supportfile_info = (object)$results["supportfile_info"];
        }
    }


    /**
     * Show a list of support files in a assay
     *
     * @return false|void
     */
    public function supportfileListAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_assay") {
            $this->_getHelper()->serviceSelect("/dashin/owner/assay/filelist/", "dashin_owner_assay_id", "assay");
        }

        $assayId = $this->localsession->getKeyValue("dashin_owner_assay_id");

        $results = $this->_getHelper()->supportFileList($assayId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["assay_info"]) && $results["assay_info"]) {
            $this->view->assay_info = (object)$results["assay_info"];
        }
        if (isset($results["assayfile_list"]) && $results["assayfile_list"]) {
            $this->view->assayfile_list = $results["assayfile_list"];
        }
    }
}