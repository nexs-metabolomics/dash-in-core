<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-22
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\ImportHelper;

class ImportController extends OwnerControllerBase
{

    public function initialize()
    {

        parent::initialize();
    }

    /**
     * @var ImportHelper
     */
    private $_helper;

    /**
     * @return ImportHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new ImportHelper();
        }

        return $this->_helper;
    }

    /**
     * Main menu for owner/import submenu
     */
    public function indexAction()
    {

        $this->toggleSubmenu();
    }

//    /**
//     * main menu for dataset submenu
//     */
//    public function datasetIndexAction()
//    {}

    /**
     * Upload a new dataset
     */
    public function uploadDatasetAction()
    {

        $results = $this->_getHelper()->uploadDataset();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
    }

    /**
     * View summary properties of dataset
     *
     * @param null $datasetId
     */
    public function viewDatasetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/uploadsummary/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->viewDataset($datasetId);
        if (isset($results["dataset"]) && $results["dataset"]) {
            $this->view->dataset = (object)$results["dataset"];
            # use "_info" to keep the standard info-format in view-files
            $this->view->dataset_info = (object)$results["dataset"];
            $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
        }
    }

    /**
     * Edit name and description of dataset
     *
     * @param null $datasetId
     *
     * @return bool
     */
    public function editDatasetAction()
    {

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/editdataset/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->editDataset($datasetId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
            $this->view->dataset_id = $datasetId;
            $this->view->dataset = (object)$results["data"];
            $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
        }
    }

    /**
     * Delete dataset
     * 
     * @return void
     */
    public function deleteDatasetAction()
    {

        $results = $this->_getHelper()->deleteDataset();
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }

    }

    /**
     * List available datasets
     *
     * @param int  $page
     * @param null $reset
     */
    public function listDatasetsAction($page = 1)
    {
        
        $rparams = $this->router->getParams();
        $dparams = $this->dispatcher->getParams();

        $reset = $this->dispatcher->getParam(1);
        $p0 = $this->dispatcher->getParam(2);

        $page = (int)$page;
        $this->view->current_page = $page;
        $nrows = 4;
        $results = $this->_getHelper()->listDatasets($page, $nrows, $reset);
        if (isset($results["datasets"]) && $results["datasets"]) {
            $this->view->pagination = $results["datasets"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * View variables in dataset and edit their standard properties
     *
     * @param null $datasetId
     * @param int  $page
     * @param null $reset
     *
     * @return bool
     */
    public function listVariablesAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/listvariables/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $page = (int)$page;
        $nrows = 15;
        $varnameMaxLength = 20;
        $results = $this->_getHelper()->listVariables($datasetId, $page, $nrows, $varnameMaxLength, $reset);
        if (isset($results["variables"]) && $results["variables"]) {
            if ($results["variables"]->items) {
                $this->view->pagination = $results["variables"];
            }
            $this->view->dataset_info = $results["dataset"];
            $this->view->current_page = $page;
            $this->localsession->setKeyValue("dashin_owner_dataset_id", $datasetId);
            $action = $this->Btn->getAction();
            if ($action === "allp") {
                $this->view->select = " checked ";
            } else {
                $this->view->select = "";
            }
//            $this->view->select_all = (object)$results["select_all"];
            $this->view->select_fi = (object)$results["select_fi"];
            $this->view->designvar_update_attempt = $results["designvar_update_attempt"];
        }
    }

    /**
     * Edit a single variable
     * TODO: menu not visible
     *
     * @return false|void
     */
    public function editVariableAction()
    {

        # select variable
        # select dataset
        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/editvariable/", "dashin_owner_dataset_id", "dataset");
        } elseif ($action === "select_variable") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/editvariable/", "dashin_owner_variable_id", "variable");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $variableId = $this->localsession->getKeyValue("dashin_owner_variable_id");
//        $datasetId = "ea3b6da8-0b14-4c1f-9d3b-914ceca7243b";
//        $variableId = "b279ceb7-deb1-4544-b228-62be0cbe157d";

        $results = $this->_getHelper()->editVariable($datasetId, $variableId);
        if (isset($results["variable"]) && $results["variable"]) {
            $this->view->variable = (object)$results["variable"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
    }

    /**
     * Browse the data of a dataset
     *
     * @return bool
     */
    public function browseDataAction()
    {

        $rowStart = $this->dispatcher->getParam(0, null, 1);
        $colStart = $this->dispatcher->getParam(1, null, 1);

//        $this->flashMessage->error("Some message");
        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/browsedata/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $this->view->dataset_id = null;

        $rowStart = (int)$rowStart;
        $colStart = (int)$colStart;
        $rowLimit = 20;
        $colLimit = 7;

        $this->view->has_results = false;
        $results = $this->_getHelper()->browseData($datasetId, $rowStart, $rowLimit, $colStart, $colLimit);
        if (isset($results["data"]["datarows"]) && $results["data"]["datarows"]) {
            $this->view->pagination_rows = $results["data"]["datarows"];
            $this->view->pagination_cols = $results["data"]["datacols"];
            $this->view->dataset = (object)$results["dataset"];
            $this->view->datatypes = $results["data"]["datatypes"];
            $this->view->has_results = true;
        }
    }

    /**
     * View the datadesign for a dataset
     *
     * @return false|void
     */
    public function viewDatadesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/viewdatadesign/", "dashin_owner_dataset_id", "dataset");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $results = $this->_getHelper()->viewDatadesign($datasetId);
        if (isset($results["design_summary"]) && $results["design_summary"]) {
            $this->view->design_summary = $results["design_summary"];
        }
        if (isset($results["event"]) && $results["event"]) {
            $this->view->event = $results["event"];
        }
        if (isset($results["subevent"]) && $results["subevent"]) {
            $this->view->subevent = $results["subevent"];
        }
        if (isset($results["samplingevent"]) && $results["samplingevent"]) {
            $this->view->samplingevent = $results["samplingevent"];
        }
        if (isset($results["count_full"]) && $results["count_full"]) {
            $this->view->count_full = $results["count_full"];
        }
        if (isset($results["count_distinct"]) && $results["count_distinct"]) {
            $this->view->count_distinct = $results["count_distinct"];
        }
        if (isset($results["samplingtime"]) && $results["samplingtime"]) {
            $this->view->samplingtime = $results["samplingtime"];
        }
        if (isset($results["startgroup"]) && $results["startgroup"]) {
            $this->view->startgroup = $results["startgroup"];
        }
        if (isset($results["subject"]) && $results["subject"]) {
            $this->view->subject = $results["subject"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["design_exists"]) && $results["design_exists"]) {
            $this->view->design_exists = $results["design_exists"];
        }
        if (isset($results["confirm_delete"]) && $results["confirm_delete"]) {
            $this->view->confirm_delete = $results["confirm_delete"];
        }
        if (isset($results["save_result"]) && $results["save_result"]) {
            $this->view->save_result = $results["save_result"];
        }
        if (isset($results["design_variable_names"]) && $results["design_variable_names"]) {
            $this->view->design_variable_names = $results["design_variable_names"];
        }
    }

    /**
     * Upload a file related to a dataset
     *
     * @return false|void
     */
    public function supportFileUploadAction()
    {

        # Check for select-dataset action
        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/fileupload/", "dashin_owner_dataset_id", "dataset");
        }

        # get active dataset_id from localsession
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        # results from helper functions
        $results = $this->_getHelper()->supportFileUpload($datasetId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }

    }

    /**
     * Edit the description of a support file in a dataset
     *
     * @return void
     */
    public function supportFileEditAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/fileedit/", "dashin_owner_dataset_id", "dataset");
        }
        if ($action === "select_file") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/fileedit/", "dashin_owner_dataset_supportfile_id", "datasetfile");
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
        $supportfileId = $this->localsession->getKeyValue("dashin_owner_dataset_supportfile_id");

        $results = $this->_getHelper()->supportFileEdit($datasetId, $supportfileId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["supportfile_info"]) && $results["supportfile_info"]) {
            $this->view->supportfile_info = (object)$results["supportfile_info"];
        }
    }

    /**
     * Show a list of support files in a dataset
     *
     * @return false|void
     */
    public function supportfileListAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/import/filelist/", "dashin_owner_dataset_id", "dataset");
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->supportFileList($datasetId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["datasetfile_list"]) && $results["datasetfile_list"]) {
            $this->view->datasetfile_list = $results["datasetfile_list"];
        }
    }

}