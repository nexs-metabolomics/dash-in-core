<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-09-14
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\StudydesignHelper;

class StudydesignController extends OwnerControllerBase
{

    /**
     * @var StudydesignHelper
     */
    private $_helper;

    /**
     * @return StudydesignHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new StudydesignHelper();
        }

        return $this->_helper;
    }

    public function indexAction()
    {
    }

    public function createCenterAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/createcenter/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->createCenter($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editCenterAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editcenter/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectcenter") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editcenter/", "dashin_owner_center_id", "center");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $centerId = $this->localsession->getKeyValue("dashin_owner_center_id");

        $results = $this->_getHelper()->editCenter($centerId, $studyId);
        if (isset($results["form"]) && $results['form']) {
            $this->view->form = $results['form'];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["center_info"]) && $results["center_info"]) {
            $this->view->center_info = (object)$results["center_info"];
        }
    }

    public function listCentersAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/liststartgroups/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->listCenters($studyId);
        if (isset($results["centers"]) && $results["centers"]) {
            $this->view->centers = $results["centers"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function createStartgroupAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/createstartgroup/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->createStartgroup($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editStartgroupAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editstartgroup/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectstartgroup") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editstartgroup/", "dashin_owner_startgroup_id", "startgroup");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $startgroupId = $this->localsession->getKeyValue("dashin_owner_startgroup_id");

        $results = $this->_getHelper()->editStartgroup($startgroupId, $studyId);
        if (isset($results["form"]) && $results['form']) {
            $this->view->form = $results['form'];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["startgroup_info"]) && $results["startgroup_info"]) {
            $this->view->startgroup_info = (object)$results["startgroup_info"];
        }
    }

    public function listStartgroupsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/liststartgroups/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $this->view->has_results = false;
        $results = $this->_getHelper()->listStartgroups($studyId);
        if (isset($results["startgroups"]) && $results["startgroups"]) {
            $this->view->has_results = true;
            $this->view->startgroups = $results["startgroups"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function addStartgroupToSubjectAction()
    {

        /**
         * TODO: Add missing elements: View + helper are missing (repository exists)
         */
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
    }

    public function createEventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/createevent/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->createEvent($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editEventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectevent") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editevent/", "dashin_owner_event_id", "event");
        } elseif ($action === "edit_subevent") {
            $subeventId = $this->Btn->getValue();
            $this->localsession->setKeyValue("owner_subevent_id", $subeventId);
            $querystring = $this->localsession->getQuerystring("?");
            $this->response->redirect("/dashin/owner/studydesign/editsubevent/$querystring");
            $this->response->send();

            return false;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $eventId = $this->localsession->getKeyValue("dashin_owner_event_id");

        $results = $this->_getHelper()->editEvent($eventId, $studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["event_info"]) && $results["event_info"]) {
            $this->view->event_info = (object)$results["event_info"];
        }
        if (isset($results["subevents_selected"]) && $results["subevents_selected"]) {
            $this->view->subevents_selected = $results["subevents_selected"];
        }
        if (isset($results["subevents_unselected"]) && $results["subevents_unselected"]) {
            $this->view->subevents_unselected = $results["subevents_unselected"];
        }
    }

    public function listEventsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/listevents/", "dashin_owner_study_id", "study");
        }

        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $this->view->has_results = false;
        $results = $this->_getHelper()->listEvents($studyId, $page, $nrows, $reset);
        if (isset($results["events"]) && $results["events"]) {
            $this->view->has_results = true;
            $this->view->pagination = $results["events"];
        }
        if (isset($results["study"]) && $results["study"]) {
            $this->view->study_info = (object)$results["study"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    public function createSubeventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_event") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editevent/", "dashin_owner_event_id", "event");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $eventId = $this->localsession->getKeyValue("dashin_owner_event_id");
        $results = $this->_getHelper()->createSubevent($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editSubeventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editsubevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectevent") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editsubevent/", "dashin_owner_event_id", "event");
        } elseif ($action === "selectsubevent") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editsubevent/", "dashin_owner_subevent_id", "subevent");
        } elseif ($action === "addsampling") {
            $this->response->redirect("/dashin/owner/studydesign/createsamplingevent/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $eventId = $this->localsession->getKeyValue("dashin_owner_event_id");
        $subeventId = $this->localsession->getKeyValue("dashin_owner_subevent_id");

        $results = $this->_getHelper()->editSubevent($subeventId, $eventId, $studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["event_info"]) && $results["event_info"]) {
            $this->view->event_info = (object)$results["event_info"];
        }
        if (isset($results["subevent_info"]) && $results["subevent_info"]) {
            $this->view->subevent_info = (object)$results["subevent_info"];
        }
        if (isset($results["sampling_events_selected"]) && $results["sampling_events_selected"]) {
            $this->view->sampling_events_selected = (object)$results["sampling_events_selected"];
        }
        if (isset($results["sampling_events_unselected"]) && $results["sampling_events_unselected"]) {
            $this->view->sampling_events_unselected = (object)$results["sampling_events_unselected"];
        }
    }

    public function listSubeventsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/listsubevents/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $this->view->has_results = false;
        $results = $this->_getHelper()->listSubevents($studyId);
        if (isset($results["subevents"]) && $results["subevents"]) {
            $this->view->has_results = true;
            $this->view->subevents = $results["subevents"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function addStartgroupToSubeventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/addgrouptosubevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectevent") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/addgrouptosubevent/", "dashin_owner_event_id", "event");
        } elseif ($action === "editgroups") {
            $subeventId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($subeventId)) {
                $this->localsession->setKeyValue("dashin_owner_subevent_id", $subeventId);
            }
        }
        if ($this->response->isSent()) {
            return false;
        }

        $eventId = $this->localsession->getKeyValue("dashin_owner_event_id");
        $subeventId = $this->localsession->getKeyValue("dashin_owner_subevent_id");
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->addStartgroupToSubevent($eventId, $subeventId, $studyId);

        if (isset($results["startgroups_selected"]) && $results["startgroups_selected"]) {
            $this->view->startgroups_selected = $results["startgroups_selected"];
        }
        if (isset($results["startgroups_unselected"]) && $results["startgroups_unselected"]) {
            $this->view->startgroups_unselected = $results["startgroups_unselected"];
        }
        if (isset($results["subevents"]) && $results["subevents"]) {
            $this->view->subevents = $results["subevents"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["event_info"]) && $results["event_info"]) {
            $this->view->event_info = (object)$results["event_info"];
        }
        if (isset($results["subevent_info"]) && $results["subevent_info"]) {
            $this->view->subevent_info = (object)$results["subevent_info"];
        }
        # only set curent_subevent_id if we are editing startgroups
        # curent_subevent_id is used to determine if we are editing mode
        if (UniqueId::uuidValidate($subeventId) && ($action === "editgroups" || $action === "add" || $action === "remove")) {
            $this->view->current_subevent_id = $subeventId;
        }
    }

    /**
     * Add sampling event to action event
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createSamplingEventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/createsamplingevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "cancel") {
            $this->response->redirect("/dashin/owner/studydesign/listsamplingevents/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->createSamplingEvent($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editSamplingeventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editsampling/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectsampling") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/editsampling/", "dashin_owner_samplingevent_id", "samplingevent");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $samplingeventId = $this->localsession->getKeyValue("dashin_owner_samplingevent_id");

        $results = $this->_getHelper()->editSamplingevent($samplingeventId, $studyId);
        if (isset($results["form"]) && $results['form']) {
            $this->view->form = $results['form'];
        }
        if (isset($results["subform"]) && $results['subform']) {
            $this->view->subform = $results['subform'];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["sampling_event_info"]) && $results["sampling_event_info"]) {
            $this->view->sampling_event_info = (object)$results["sampling_event_info"];
        }
        if (isset($results["sampling_times"]) && $results["sampling_times"]) {
            $this->view->sampling_times = (object)$results["sampling_times"];
        }
    }

    public function listSamplingeventsAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/listsamplingevents/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $results = $this->_getHelper()->listSamplingevents($studyId);
        if (isset($results["sampling_events"]) && $results["sampling_events"]) {
            $this->view->sampling_events = $results["sampling_events"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function addSamplingEventToSubeventAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/addsamplingtosubevent/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_event") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/addsamplingtosubevent/", "dashin_owner_event_id", "event");
        } elseif ($action === "select_subevent") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/addsamplingtosubevent/", "dashin_owner_subevent_id", "subevent");
        }

        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $eventId = $this->localsession->getKeyValue("dashin_owner_event_id");
        $subeventId = $this->localsession->getKeyValue("dashin_owner_subevent_id");

        $results = $this->_getHelper()->addSamplingEventToSubevent($subeventId, $eventId, $studyId);

        if (isset($results["sampling_events_selected"]) && $results["sampling_events_selected"]) {
            $this->view->sampling_events_selected = $results["sampling_events_selected"];
        }
        if (isset($results["sampling_events_unselected"]) && $results["sampling_events_unselected"]) {
            $this->view->sampling_events_unselected = $results["sampling_events_unselected"];
        }

        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["event_info"]) && $results["event_info"]) {
            $this->view->event_info = (object)$results["event_info"];
        }
        if (isset($results["subevent_info"]) && $results["subevent_info"]) {
            $this->view->subevent_info = (object)$results["subevent_info"];
        }
    }

    public function importDatadesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/importdatadesign/", "dashin_owner_study_id", "study");
        } elseif ($action === "selectdatadesign") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/importdatadesign/", "dashin_owner_dataset_id", "datadesign");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->importDatadesign($studyId, $datasetId);
        if (isset($results["design_summary"]) && $results["design_summary"]) {
            $this->view->design_summary = $results["design_summary"];
        }
        if (isset($results["event"]) && $results["event"]) {
            $this->view->event = $results["event"];
        }
        if (isset($results["subevent"]) && $results["subevent"]) {
            $this->view->subevent = $results["subevent"];
        }
        if (isset($results["samplingevent"]) && $results["samplingevent"]) {
            $this->view->samplingevent = $results["samplingevent"];
        }
        if (isset($results["count_full"]) && $results["count_full"]) {
            $this->view->count_full = $results["count_full"];
        }
        if (isset($results["count_distinct"]) && $results["count_distinct"]) {
            $this->view->count_distinct = $results["count_distinct"];
        }
        if (isset($results["samplingtime"]) && $results["samplingtime"]) {
            $this->view->samplingtime = $results["samplingtime"];
        }
        if (isset($results["startgroup"]) && $results["startgroup"]) {
            $this->view->startgroup = $results["startgroup"];
        }
        if (isset($results["subject"]) && $results["subject"]) {
            $this->view->subject = $results["subject"];
        }
        if(isset($results["center"]) && $results["center"]) {
            $this->view->center = $results["center"];
        }
        if(isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["design_exists"]) && $results["design_exists"]) {
            $this->view->design_exists = $results["design_exists"];
        }
        if (isset($results["show_importbtn"]) && $results["show_importbtn"]) {
            $this->view->show_importbtn = $results["show_importbtn"];
        }
        if (isset($results["design_imported"]) && $results["design_imported"]) {
            $this->view->design_imported = $results["design_imported"];
        }
        if (isset($results["confirm_import"]) && $results["confirm_import"]) {
            $this->view->confirm_import = $results["confirm_import"];
        }
        if (isset($results["import_result"]) && $results["import_result"]) {
            $this->view->import_result = $results["import_result"];
        }
        if (isset($results["design_variable_names"]) && $results["design_variable_names"]) {
            $this->view->design_variable_names = $results["design_variable_names"];
        }
    }

    public function viewStudydesignAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studydesign/viewstudydesign/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->viewStudydesign($studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["event"]) && $results["event"]) {
            $this->view->event = $results["event"];
        }
        if (isset($results["subevent"]) && $results["subevent"]) {
            $this->view->subevent = $results["subevent"];
        }
        if (isset($results["samplingevent"]) && $results["samplingevent"]) {
            $this->view->samplingevent = $results["samplingevent"];
        }
        if (isset($results["samplingtime"]) && $results["samplingtime"]) {
            $this->view->samplingtime = $results["samplingtime"];
        }
        if (isset($results["startgroup"]) && $results["startgroup"]) {
            $this->view->startgroup = $results["startgroup"];
        }
        if (isset($results["subject"]) && $results["subject"]) {
            $this->view->subject = $results["subject"];
        }
        if(isset($results["center"]) && $results["center"]) {
            $this->view->center = $results["center"];
        }
        $this->view->design_exists = $results["design_exists"];
    }

}