<?php

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\DatacolumntypeHelper;

class DatacolumntypeController extends OwnerControllerBase
{

    /**
     * @var DatacolumntypeHelper
     */
    private $_helper;

    /**
     * @return DatacolumntypeHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new DatacolumntypeHelper();
        }

        return $this->_helper;
    }

    /**
     * Main datacolumn type menu
     */
    public function indexAction()
    {
    }

    /**
     * Create data column type
     *
     * A column type is a data type relevant for searching or
     * comparing measurements of the same type.
     */
    public function createAction()
    {

        $results = $this->_getHelper()->createDatacolumntype();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Edit column type
     */
    public function editAction()
    {

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_datacolumntype") {
            $this->_getHelper()->serviceSelect("/dashin/owner/datacolumntype/edit/", "dashin_owner_datacolumntype_id", "datacolumntype");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $datacolumntypeId = $this->localsession->getKeyValue("dashin_owner_datacolumntype_id");

        $results = $this->_getHelper()->editDatacolumntype($datacolumntypeId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * View column type
     */
    public function viewAction()
    {

        $datacolumntypeId = $this->localsession->getKeyValue("dashin_owner_datacolumntype_id");
        $results = $this->_getHelper()->viewDatacolumntype($datacolumntypeId);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->datacolumntype = (object)$results["data"];
        }

    }

    /**
     * List existing column types
     *
     * @param int  $page
     * @param null $reset
     */
    public function listAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listDatacolumntypes($page, $nrows, $reset);
        if (isset($results["datacolumntypes"]) && $results["datacolumntypes"]) {
            $this->view->pagination = $results["datacolumntypes"];
        }
        $this->view->current_page = $page;
    }

}