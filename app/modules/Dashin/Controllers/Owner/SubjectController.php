<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-01-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\SubjectHelper;

class SubjectController extends OwnerControllerBase
{

    /**
     * @var SubjectHelper
     */
    private $_helper;

    /**
     * @return SubjectHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new SubjectHelper();
        }

        return $this->_helper;
    }

    /**
     * Main menu for subject submenu
     */
    public function indexAction()
    {
    }

    /**
     * Extract subjects from dataset
     * based on structure defined by appropriate variables (ID)
     *
     * @return bool
     */
    public function createStudysubjectAction()
    {

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/subject/createstudysubject/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->createStudysubject($studyId);
        if (isset($results["form"]) && $results["form"] && $studyId) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }

    }

    public function editStudysubjectAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "selectsubject") {
            $this->_getHelper()->serviceSelect("/dashin/owner/subject/editstudysubject/", "dashin_owner_subject_id", "subject");
        } elseif ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/subject/editstudysubject/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $subjectId = $this->localsession->getKeyValue("dashin_owner_subject_id");

        $results = $this->_getHelper()->editStudysubject($subjectId, $studyId);
        if (isset($results["subject_info"]) && $results["subject_info"] && $studyId) {
            $this->view->subject_info = (object)$results["subject_info"];
            if (isset($results["form"]) && $results["form"]) {
                $this->view->form = $results["form"];
            }
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function listStudysubjectsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/subject/liststudysubjects/", "dashin_owner_study_id", "study");
        } elseif ($action === "edit") {
            $subjectId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($subjectId)) {
                $this->localsession->setKeyValue("dashin_owner_subject_id", $subjectId);
            }
            $this->response->redirect("/dashin/owner/subject/editstudysubject/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        }
        if ($this->response->isSent()) {
            return false;
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrows = 20;

        $this->view->has_results = false;
        $results = $this->_getHelper()->listStudysubjects($page, $nrows, $reset, $studyId);
        if (isset($results["subjects"]) && $results["subjects"] && $studyId) {
            $this->view->has_results = true;
            $this->view->pagination = $results["subjects"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        $this->view->current_page = $page;
    }

}