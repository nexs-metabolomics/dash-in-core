<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-13
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\VarinfoHelper;

class VarinfoController extends OwnerControllerBase
{

    /**
     * @var VarinfoHelper
     */
    private $_helper;

    /**
     * @return VarinfoHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new VarinfoHelper();
        }

        return $this->_helper;
    }

    /**
     * Main entry for uploading variable info
     */
    public function indexAction()
    {
    }

    /**
     * Main entry for match variable info
     */
    public function matchIndexAction()
    {
    }

    /**
     * Upload a variable info dataset
     *
     * A second-order dataset related to variables
     * - to be matched to a set of variables
     *
     * OBS: A dataset generally consists of three tables:
     * *_dataset
     * (identifying the dataset by name and id)
     * *_variable
     * (identifying the individual columns of the dataset)
     * *_datatable
     * (holding the actual data one entry per row as a jsonb-array
     * where each element corresponds to one entry in the *_variable table)
     *
     * For uploaded (ie. not matched) second-order datasets the table names are prefixed:
     * upvar_*
     *
     * Uploaded second-order datasets are called "upvar_*"
     */
    public function uploadUpvarDatasetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvs") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/browseupvardata/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $results = $this->_getHelper()->uploadUpvarDataset();
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Overview of uploaded variable info
     *
     * @param null $upvarDatasetId
     */
    public function viewUpvarDatasetAction($upvarDatasetId = null)
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvs") {

            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/upvarreport/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return;
            }
        }
        if (!UniqueId::uuidValidate($upvarDatasetId)) {
            $upvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");
            # for testing:
            # if (!UniqueId::uuidValidate($upvarDatasetId)) {
            #     $upvarDatasetId = "3bb69318-cbda-4ab8-8a51-d590831ebfdd";
            # }
        }

        $results = $this->_getHelper()->viewUpvarDataset($upvarDatasetId);
        if (isset($results["duplicate_varnames"]) && $results["duplicate_varnames"]) {
            $this->view->duplicate_varnames = $results["duplicate_varnames"];
            $this->view->upvar_dataset_id = $upvarDatasetId;
            $this->localsession->setKeyValue("dashin_owner_upvar_dataset_id", $upvarDatasetId);
        }
        if (isset($results["empty_varnames"]) && $results["empty_varnames"]) {
            $this->view->empty_varnames = (object)$results["empty_varnames"];
        }
        if (isset($results["duplicate_upvarnames"]) && $results["duplicate_upvarnames"]) {
            $this->view->duplicate_upvarnames = (object)$results["duplicate_upvarnames"];
        }
        if (isset($results["empty_upvarnames"]) && $results["empty_upvarnames"]) {
            $this->view->empty_upvarnames = (object)$results["empty_upvarnames"];
        }
        if (isset($results["upvar_dataset_for_view"]) && $results["upvar_dataset_for_view"]) {
            $this->view->upvar_dataset = (object)$results["upvar_dataset_for_view"];
        }
    }

    /**
     * Edit name and description of uploaded variable info dataset
     *
     * @return bool
     */
    public function editUpvarDatasetAction()
    {

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "selectvs") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/upvaredit/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $upvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");

        $results = $this->_getHelper()->editUpvarDataset($upvarDatasetId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["data"]) && $results["data"]) {
            $this->view->upvar_dataset = (object)$results["data"];
        }

    }

    /**
     * List all variables for a single uploaded variable info dataset
     *
     * @param int  $page
     * @param null $reset
     *
     * @return bool
     */
    public function listUpvarVariablesAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "selectvds") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/viewupvarv/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $upvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listUpvarVariables($upvarDatasetId, $page, $nrows, $reset);
        if (isset($results["variables"]) & !empty($results["variables"])) {
            $this->view->pagination = $results["variables"];
            $this->view->upvar_dataset = (object)$results["upvar_dataset"];
        }
    }

    /**
     * Browse uploaded variable info data
     *
     * @param int $rowStart
     * @param int $colStart
     */
    public function browseUpvarDataAction($rowStart = 1, $colStart = 1)
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvs") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/browseupvardata/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $upvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");

        $rowStart = (int)$rowStart;
        $colStart = (int)$colStart;
        $rowLimit = 20;
        $colLimit = 7;
        $this->view->upvar_dataset_id = null;

        $action = $this->Btn->getAction();
        if ($action === "fi" || $action === "fi_x") {
            # filter button was pressed
            $valueString = $this->Btn->getValue();
            $values = explode("/", $valueString);
            $rowStart = (int)$values[2];
            $colStart = (int)$values[3];

            if ($action === "fi") {
                # add filter
                # filter index for sql-query
                $filterIdx = abs((int)$values[0] + $values[1] - 1);
                # index to mark button in ui
                # (idx within page view always 0 to limit)
                $this->view->fi_idx = (int)$values[0];
            } else {
                # remove filter
                $filterIdx = null;
                $this->view->fi_idx = null;
            }
        } else {
            $this->view->fi_idx = null;
            $filterIdx = null;
        }

        $this->view->has_results = false;
        $results = $this->_getHelper()->browseUpvarData($upvarDatasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx);
        if (isset($results["datarows"]) && $results["datarows"]) {
            $this->view->pagination_rows = $results["datarows"];
            $this->view->upvar_dataset = (object)$results["upvar_dataset"];
            $this->view->pagination_cols = $results["datacols"];
            $this->view->has_results = true;
        }
    }

    /**
     * List uploaded variable-info datasets
     * This function also allows to delete a varinfo dataset
     *
     * @param int  $page
     * @param null $reset
     */
    public function listUpvarDatasetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listUpvarDatasets($page, $nrows, $reset);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * Match a variable-info dataset
     * to a set of variables from a dataset
     *
     * @param null $upvarDatasetId
     * @param null $datasetId
     */
    public function matchVarinfoAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "selectup") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/match/", "dashin_owner_upvar_dataset_id", "upvardataset");
            if ($this->response->isSent()) {
                return;
            }
        } elseif ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/match/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        # get select objects
        $upvarDatasetId = $this->localsession->getKeyValue("dashin_owner_upvar_dataset_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->matchVarinfo($upvarDatasetId, $datasetId);
        if (isset($results["dataset"]) && $results["dataset"]) {
            $this->view->dataset = (object)$results["dataset"];
        }
        if (isset($results["upvar_dataset"]) && $results["upvar_dataset"]) {
            $this->view->upvar_dataset = (object)$results["upvar_dataset"];
        }
        if (isset($results["var_dataset"]) && $results["var_dataset"]) {
            $this->view->var_dataset = (object)$results["var_dataset"];
        }
        if (isset($results["match_stats"]) && $results["match_stats"]) {
            $this->view->match_stats = (object)$results["match_stats"];
        }
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = (object)$results["form"];
        }
    }

    /**
     * Edit name and description of matched variable info dataset
     *
     * @return bool
     */
    public function editVarDatasetAction()
    {

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "selectvardataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/varedit/", "dashin_owner_dataset_id", "vardataset");
            if ($this->response->isSent()) {
                return false;
            }
        }
        $varDatasetId = $this->localsession->getKeyValue("dashin_owner_var_dataset_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $results = $this->_getHelper()->editVarDataset($datasetId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["data"]) && $results["data"]) {
            $this->view->var_dataset = (object)$results["data"];
        }

    }

    /**
     * View summary report for variable dataset
     *
     */
    public function viewVarDatasetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvds") {

            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/matchsummary/", "dashin_owner_var_dataset_id", "vardataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $varDatasetId = $this->localsession->getKeyValue("dashin_owner_var_dataset_id");

        $results = $this->_getHelper()->viewVarDataset($varDatasetId);
        if (isset($results["var_dataset"]) && $results["var_dataset"]) {
            $this->view->var_dataset = (object)$results["var_dataset"];
        }
    }

    /**
     * List variables for variable dataset
     *
     * @param int  $page
     * @param null $reset
     * @param null $varDatasetId
     *
     * @return bool
     */
    public function listVarVariablesAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        # validate or select seclabel
        $action = $this->Btn->getAction();
        if ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/listvdvariables/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return false;
            }
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listVarVariables($datasetId, $page, $nrows, $reset);
        if (isset($results["var_variables"]) && $results["var_variables"]) {
            $this->view->pagination = $results["var_variables"];
        }
        if (isset($results["var_dataset_info"]) && $results["var_dataset_info"]) {
            $this->view->var_dataset_info = (object)$results["var_dataset_info"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }

    }

    /**
     * List matched variable-info datasets
     *
     * @param int  $page
     * @param null $reset
     */
    public function listVarDatasetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listVarDatasets($page, $nrows, $reset);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * Browse data of variable dataset
     *
     * @param null $varDatasetId
     * @param int  $rowStart
     * @param int  $colStart
     */
    public function browseVarDataAction($rowStart = 1, $colStart = 1)
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvds") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/browsevardata/", "dashin_owner_var_dataset_id", "vardataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $varDatasetId = $this->localsession->getKeyValue("dashin_owner_var_dataset_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $rowStart = (int)$rowStart;
        $colStart = (int)$colStart;
        $rowLimit = 20;
        $colLimit = 7;
        $this->view->var_dataset_id = null;

        $action = $this->Btn->getAction();
        if ($action === "fi" || $action === "fi_x") {
            # filter button was pressed
            $valueString = $this->Btn->getValue();
            $values = explode("/", $valueString);
            $rowStart = (int)$values[2];
            $colStart = (int)$values[3];

            if ($action === "fi") {
                # add filter
                # filter index for sql-query
                $filterIdx = abs((int)$values[0] + $values[1] - 1);
                # index to mark button in ui
                # (idx within page view always 0 to limit)
                $this->view->fi_idx = (int)$values[0];
            } else {
                # remove filter
                $filterIdx = null;
                $this->view->fi_idx = null;
            }
        } else {
            $this->view->fi_idx = null;
            $filterIdx = null;
        }

        $this->view->var_dataset_name = "";

        $results = $this->_getHelper()->browseVarData($datasetId, $rowStart, $rowLimit, $colStart, $colLimit, $filterIdx);
        if (isset($results["datarows"]) && $results["datarows"]) {
            $this->view->pagination_rows = $results["datarows"];
            $this->view->pagination_cols = $results["datacols"];
            $this->view->var_dataset_id = $varDatasetId;
            $this->view->var_dataset_name = " (" . $results["var_dataset"]["name"] . ")";
            $this->localsession->setKeyValue("dashin_owner_var_dataset_id", $varDatasetId);
        }
        if (isset($results["var_dataset"]) && $results["var_dataset"]) {
            $this->view->var_dataset_info = (object)$results["var_dataset"];
        }

    }

    /**
     * Map data columns to column-types
     * Used to determine how search is performed
     *
     * @param int  $page
     * @param null $varDatasetId
     */
    public function mapSearchColumnsAction($page = 1)
    {

        $action = $this->Btn->getAction();
        if ($action === "selectvds") {
            $this->_getHelper()->serviceSelect("/dashin/owner/varinfo/mapsearchcols/", "dashin_owner_dataset_id", "dataset");
            if ($this->response->isSent()) {
                return;
            }
        }

        $varDatasetId = $this->localsession->getKeyValue("dashin_owner_var_dataset_id");
        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->mapSearchColumns($datasetId, $page, $nrows);
        if (isset($results["var_variables"]) && $results["var_variables"]) {
            $this->view->pagination = $results["var_variables"];
        }
        if (isset($results["var_dataset_info"]) && $results["var_dataset_info"]) {
            $this->view->var_dataset_info = (object)$results["var_dataset_info"];
        }
        if (isset($results["mapped_var_variables"]) && $results["mapped_var_variables"]) {
            $this->view->mappedvars = $results["mapped_var_variables"];
        }
        if (isset($results["search_column_types"]) && $results["search_column_types"]) {
            $this->view->search_column_types = $results["search_column_types"];
        }
    }

}