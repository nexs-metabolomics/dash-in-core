<?php

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\ImportguideHelper;

class ImportguideController extends OwnerControllerBase
{

    /**
     * @var ImportguideHelper
     */
    private $_helper;

    /**
     * @return ImportguideHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new ImportguideHelper();
        }

        return $this->_helper;
    }

    /**
     * Default entry point
     * 
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * Upload a data file to a temporary directory
     *
     * If successful, redirect to import-data menu
     *
     * @return false|void
     */
    public function uploadDatafileAction()
    {

        $results = $this->_getHelper()->uploadDatafile();
        if ($this->response->isSent()) {
            return false;
        }
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * Import dataset from uploaded file
     * 
     * @param $page
     *
     * @return true
     * @throws \App\Library\Utils\JsonException
     */
    public function importDataAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->importDatafile($page, $nrows, $reset);

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["header_pagination"]) && $results["header_pagination"]) {
            $this->view->header_pagination = $results["header_pagination"];
        }
        if (isset($results["has_quality_control_data"])) {
            $this->view->has_quality_control_data = $results["has_quality_control_data"];
        }

        return true;
    }

    /**
     * Show summary of relevant info after the dataset import
     * with option to update name and description
     *
     * @return void|false
     */
    public function importSummaryAction()
    {

        $datasetId = $this->localsession->getKeyValue("dashin_owner_importguide_dataset_id");

        $results = $this->_getHelper()->datasetImportSummary($datasetId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["import_summary"]) && $results["import_summary"]) {
            $this->view->import_summary = (object)$results["import_summary"];
        }
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }
    

    /**
     * Edit data design variables
     * 
     * @param $page
     *
     * @return void
     */
    public function editDatasetDesignAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $datasetId = $this->localsession->getKeyValue("dashin_owner_importguide_dataset_id");

        $page = (int)$page;
        $nrows = 10;
        $results = $this->_getHelper()->editDatasetDesign($datasetId, $page, $nrows, $reset);
        if($this->response->isSent()) {
            return;
        }

        
        if (isset($results["variables_paginated"]) && $results["variables_paginated"]) {
            $this->view->pagination = $results["variables_paginated"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
        if (isset($results["design_has_duplicates"]) && $results["design_has_duplicates"]) {
            $this->view->design_has_duplicates = $results["design_has_duplicates"];
        }

    }

    /**
     * View defined dataset design
     * 
     * @param $page
     *
     * @return void
     */
    public function viewDatasetDesignAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);
        $page = (int)$page;
        $nrows = 10;

        $datasetId = $this->localsession->getKeyValue("dashin_owner_importguide_dataset_id");

        $results = $this->_getHelper()->viewDatasetDesign($datasetId, $page, $nrows, $reset);
        if($this->response->isSent()) {
            return;
        }
        
        if(isset($results["pre_dataset_design"]) && $results["pre_dataset_design"]) {
            $this->view->pre_dataset_design = $results["pre_dataset_design"];
        }

    }

    /**
     * Final "success" page in import guide
     * 
     * @return void
     */
    public function importguideCompleteAction()
    {
        $datasetId = $this->localsession->getKeyValue("dashin_owner_importguide_dataset_id");

        $results = $this->_getHelper()->importguideComplete($datasetId);
        if(isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
    }

}