<?php


namespace Dashin\Controllers\Owner;


use Dashin\Helpers\Owner\PrefixedunitHelper;

class PrefixedunitController extends OwnerControllerBase
{
    /**
     * @var PrefixedunitHelper
     */
    private $_helper;

    /**
     * @return PrefixedunitHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new PrefixedunitHelper();
        }
        return $this->_helper;
    }

    public function indexAction()
    {
    }
    
    public function createPrefixedunitAction()
    {
        
    }
    
    public function listPrefixedunitsAction($page=1, $reset=null)
    {
        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listPrefixedunits($page,$nrows,$reset);
        
        if(isset($results["prefixedunits"]) && $results["prefixedunits"]) {
            $this->view->pagination = $results["prefixedunits"];
        }
//        if(isset($results["search_term"]) && $results["search_term"]) {
//            $this->view->search_term = $results["search_term"];
//        }
        $showvalid = (int)$this->_getHelper()->getKeyValue("owner_list_prefixedunits_showvalid");
        $showvalid = ($showvalid === 2) ? 2 : 0;
        $this->view->showvalid = $showvalid;
    }

}