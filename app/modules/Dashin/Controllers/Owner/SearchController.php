<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-11-09
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace Dashin\Controllers\Owner;

use App\Library\Utils\UniqueId;
use Dashin\Helpers\Owner\SearchHelper;

class SearchController extends OwnerControllerBase
{
    /**
     * @var SearchHelper
     */
    private $_helper;

    /**
     * @return SearchHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new SearchHelper();
        }
        return $this->_helper;
    }

    /**
     * TODO: the search pages are work in process
     * placeholder
     */
    public function searchFrontPageAction()
    {
    }

    public function indexAction()
    {
        $this->toggleSubmenu();
    }

    public function saveMetabolomicsAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/search/metabolomics/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        $searchParams = $this->_getHelper()->getKeyValue("dashin_owner_metabolomics_search_params");
        if (!is_array($searchParams)) {
            return true;
        }

        $results = $this->_getHelper()->saveMetabolomics($searchParams);
        if ($this->response->isSent()) {
            return false;
        }
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["summary"]) && $results["summary"]) {
            $this->view->summary = $results["summary"];
        }
    }

    public function saveMetabolomicsType02Action()
    {
        $action = $this->Btn->getAction();
        if ($action === "cancel") {
            $this->response->redirect("/dashin/owner/search/metabolomicstype02/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        $searchParams = $this->_getHelper()->getKeyValue("dashin_owner_metabolomics_type02_search_params");
        if (!is_array($searchParams)) {
            return true;
        }

        $results = $this->_getHelper()->saveMetabolomicsType02($searchParams);
        if ($this->response->isSent()) {
            return false;
        }
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["summary"]) && $results["summary"]) {
            $this->view->summary = $results["summary"];
        }
    }

    /**
     * Search metabolimics data by Rt and mz
     *
     * @param int  $page
     * @param null $reset
     */
    public function searchMetabolomicsAction($page = 1)
    {
        $pagex = ($this->dispatcher->getParam(0));
        $reset = $this->dispatcher->hasParam(1);

        $action = $this->Btn->getAction();
        if ($action === "save") {
            $this->response->redirect("/dashin/owner/search/savemetabolomics/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->searchMetabolomics($page, $nrows, $reset);
        if (isset($results["results_paginated"]) && $results["results_paginated"]) {
            $this->view->pagination = $results["results_paginated"];
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }

        if (isset($results["summary"]) && $results["summary"]) {
            $this->view->summary = (object)$results["summary"];
        }
    }

    public function searchMetabolomicsType02Action($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $togglestateStudy = $this->request->getPost("togglestate_study", null, "closed");
        $togglestateMetabol = $this->request->getPost("togglestate_metabol", null, "open");

        $action = $this->Btn->getAction();
        if ($action === "togglestudy") {
            $btnStudy = $this->Btn->getValue();
            if ($btnStudy === "open") {
                $togglestateStudy = "open";
            } else {
                $togglestateStudy = "closed";
            }
        } elseif ($action === "togglemetabol") {
            $btnMetbol = $this->Btn->getValue();
            if ($btnMetbol === "open") {
                $togglestateMetabol = "open";
            } else {
                $togglestateMetabol = "closed";
            }
        }

        if ($action === "save") {
            $this->response->redirect("/dashin/owner/search/savemetabolomicstype02/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->searchMetabolomicsType02($page, $nrows, $reset);
        if (isset($results["results_paginated"]) && $results["results_paginated"]) {
            $this->view->pagination = $results["results_paginated"];
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }

        if (isset($results["form_study"]) && $results["form_study"]) {
            $this->view->form_study = $results["form_study"];
        }

        if (isset($results["summary"]) && $results["summary"]) {
            $this->view->summary = (object)$results["summary"];
        }
        if ($togglestateStudy === "open") {
            $this->view->details_study = "open";
        } else {
            $this->view->details_study = "closed";
        }
        if ($togglestateMetabol === "open") {
            $this->view->details_metabol = "open";
        } else {
            $this->view->details_metabol = "closed";
        }
    }

    public function listResultsetsAction($page = 1)
    {
        $action = $this->Btn->getAction();
        if ($action === "editresultset") {
            $resultsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($resultsetId)) {
                $this->localsession->setKeyValue("dashin_owner_resultset_id", $resultsetId);
                $this->response->redirect("/dashin/owner/search/editresultdataset/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "download") {
            $resultsetId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($resultsetId)) {
                $this->localsession->setKeyValue("dashin_owner_resultset_id", $resultsetId);
                $this->response->redirect("/dashin/owner/search/viewresultset/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }
        $reset = $this->dispatcher->getParam(1);

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listResultsets($page, $nrows, $reset);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
    }

    public function viewResultsetAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_resultset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/search/viewresultset/", "dashin_owner_resultset_id", "searchresultset");
        } elseif ($action === "edit") {
            $datasetId = $this->Btn->getValue();
            $this->localsession->setKeyValue("dashin_owner_search_dataset_id", $datasetId);
            $this->response->redirect("/dashin/owner/search/editresultdataset/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $resultsetId = $this->localsession->getKeyValue("dashin_owner_resultset_id");

        if ($action === "export") {
            $this->_getHelper()->exportResultset($resultsetId);
        }

        $page = (int)$page;
        $nrows = 20;
        $this->view->has_results = false;
        $results = $this->_getHelper()->viewResultset($resultsetId);
        if (isset($results["data"]) && $results["data"]) {
            $this->view->resultset = $results["data"];
            $this->view->has_results = true;
        }
        if (isset($results["resultset_info"]) && $results["resultset_info"]) {
            $this->view->resultset_info = (object)$results["resultset_info"];
        }
    }

    /**
     * TODO: the search pages are work in process
     * placeholder
     */
    public function searchAnthropometryAction()
    {
    }

    public function editResultsetDatasetAction()
    {
        $selectedPage = $this->dispatcher->getParam(0, null, 1);
        $unselectedPage = $this->dispatcher->getParam(1, null, 1);
        $reset1 = $this->dispatcher->getParam(2);
        $reset2 = $this->dispatcher->getParam(2);

        $action = $this->Btn->getAction();
        if ($action === "select_resultset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/search/editresultdataset/", "dashin_owner_resultset_id", "searchresultset");
        } elseif ($action === "select_dataset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/search/editresultdataset/", "dashin_owner_search_dataset_id", "resultsetdataset");
        } elseif ($action === "finish") {
            $this->response->redirect("/dashin/owner/search/viewresultset/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        }
        if ($this->response->isSent()) {
            return false;
        }

        if ($action === "reset1") {
            $reset1 = 1;
        }
        if ($action === "reset2") {
            $reset2 = 1;
        }

        $datasetId = $this->localsession->getKeyValue("dashin_owner_search_dataset_id");
        $resultsetId = $this->localsession->getKeyValue("dashin_owner_resultset_id");

        $selectedPage = (int)$selectedPage;
        $selectedNrows = 10;
        $unselectedNrows = 10;
        $this->view->has_results = false;
        $this->view->has_sresults = false;
        $this->view->has_uresults = false;

        $results = $this->_getHelper()->editResultsetDataset(
            $resultsetId,
            $datasetId,
            $selectedPage,
            $selectedNrows,
            $unselectedPage,
            $unselectedNrows,
            $reset1,
            $reset2
        );

        if (isset($results["selected"]) && $results["selected"]) {
            $this->view->selected = $results["selected"];
            $this->view->has_results = true;
            $this->view->has_sresults = true;
        }
        if (isset($results["unselected"]) && $results["unselected"]) {
            $this->view->unselected = $results["unselected"];
            $this->view->has_results = true;
            $this->view->has_uresults = true;
        }
        if (isset($results["resultset_info"]) && $results["resultset_info"]) {
            $this->view->resultset_info = (object)$results["resultset_info"];
        }
        if (isset($results["dataset_info"]) && $results["dataset_info"]) {
            $this->view->dataset_info = (object)$results["dataset_info"];
        }
    }
}