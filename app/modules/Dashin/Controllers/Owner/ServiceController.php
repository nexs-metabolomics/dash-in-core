<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-08-24
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Dashin\Controllers\Owner;


use Dashin\Helpers\Owner\ServiceHelper;

class ServiceController extends OwnerControllerBase
{
    /**
     * @var ServiceHelper
     */
    private $_helper;

    /**
     * @return ServiceHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new ServiceHelper();
        }
        return $this->_helper;
    }
    
    /***
     * Get the name of the specific service requested
     * This determined from the route-pattern as the
     * last slug between the last two slashes
     *
     * @return mixed
     */
    private function _getServiceName()
    {
        $routeParams = $this->router->getParams();
        $serviceName = isset($routeParams[0]) ? $routeParams[0] : "";
        return $serviceName;
    }
    
    private function _getSeviceParams()
    {
        $page = 1;
        $reset = null;
        
        $params = $this->router->getMatches();
        if(isset($params[2])) {
            $str = substr($params[2],1);
            $params = explode("/",$str);
            $page = (int)$params[0];
            if(isset($params[1]) && $params[1]) {
                $reset = 1;
            }
        }
        
        return [
            "page" => $page,
            "reset" => $reset,
        ];
    }
    
    /**
     * Select an object to be the "current" object
     * within the context of dashin
     *
     * @param int $page (first page of the pagination)
     * @param null|int $reset (null,0,1) boolean: reset search_term
     * @return bool
     */
    public function selectAction()
    {
        $params = $this->_getSeviceParams();
        $page = $params["page"];
        $reset = $params["reset"];

        $serviceName = $this->_getServiceName();
        $params = $this->_getHelper()->getKeyValue("dashin_owner_service_select_{$serviceName}");
        if(!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return false;
        }

        $nrows = 10;
        $results = $this->_getHelper()->select($page,$nrows,$reset,$params,$serviceName);
        if($this->response->isSent()) {
            return false;
        }
        if(isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if(isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    public function select2Action()
    {
        $params = $this->_getSeviceParams();
        $page = $params["page"];
        $reset = $params["reset"];

        $serviceName = $this->_getServiceName();
        $params = $this->_getHelper()->getKeyValue("dashin_owner_service_select_{$serviceName}");
        if(!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return false;
        }

        $nrows = 10;
        $results = $this->_getHelper()->select($page,$nrows,$reset,$params,$serviceName);
        if($this->response->isSent()) {
            return false;
        }
        if(isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if(isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * Select security label to be "current seclabel"
     * within the context of dashin
     * 
     * This is an early version and poc of what will be 
     * a set of service classes providing services
     * that are generally needed by other classes.
     * 
     * The reasoning: When a class has certain requirements 
     * (eg. setting seclab permissions on a dataset requires
     *  both seclab and dataset to be selected before the 
     *  permission ui can be functional) that require user interaction
     * we need to do a redirect to the decision page and after 
     * the decision redirect back to the originating page. 
     * This service will make these two redirects out and 
     * home transparent to the calling method and therefore 
     * we can use it to generalize the decision.
     *
     * @param int $page (first page of the pagination)
     * @param null|int $reset (null,0,1) boolean: reset search_term
     * @return bool
     */
    public function selectSeclabelAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);
        $params = $this->_getHelper()->getKeyValue("dashin_owner_service_select_seclabel");
        if(!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return;
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->selectSeclabel($page,$nrows,$reset,$params);
        if(isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
            $this->view->search_term = $results["search_term"];
        }
    }

}