<?php

namespace Dashin\Controllers\Owner;

use Dashin\Helpers\Owner\StudyexportHelper;

class StudyexportController extends OwnerControllerBase
{

    /**
     * @var StudyexportHelper
     */
    private $_helper;

    /**
     * @return StudyexportHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new StudyexportHelper();
        }

        return $this->_helper;
    }

    /**
     * Create a single-variable condition
     * Single-variable conditions are combined to sets of condition
     * used for filtering data to export
     *
     * @param $page
     * @param $reset
     *
     * @return false|void
     */
    public function createStudyvariableconditionAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/createcondition/", "dashin_owner_study_id", "study");
        } elseif ($action === "goto_adddataset") {
            $this->response->redirect("/dashin/owner/studydata/attachdataset/" . $this->localsession->getQuerystring("?"));
            $this->response->send();

            return false;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->createStudyvariablecondition($page, $nrows, $reset, $studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyvarcond_paginated"]) && $results["studyvarcond_paginated"]) {
            $this->view->pagination = $results["studyvarcond_paginated"];
        }
    }

    /**
     * Paginated list of studyvariable conditions
     *
     * @param $page  int current page
     * @param $reset int|bool - reset the search form
     *
     * @return false|void
     */
    public function listStudyvariableconditionsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/listconditions/", "dashin_owner_study_id", "study");
        } elseif ($action === "goto_createcondition") {
            $this->response->redirect("/dashin/owner/studyexport/createcondition/" . $this->localsession->getQuerystring("?"));
            $this->response->send();

            return false;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $page = (int)$page;
        $this->view->current_page = $page;
        $nrows = 20;
        $results = $this->_getHelper()->listStudyvariableconditions($page, $nrows, $reset, $studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyvarcond_paginated"]) && $results["studyvarcond_paginated"]) {
            $this->view->pagination = $results["studyvarcond_paginated"];
        }
    }

    /**
     * Edit studyvariable condition
     *
     *
     * @return false|void
     */
    public function editStudyvariableconditionAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editcondition/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studyvarcondition") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editcondition/", "dashin_owner_studyvariablecondition_id", "studyvarcondition");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyvariablesconditionId = $this->localsession->getKeyValue("dashin_owner_studyvariablecondition_id");

        $results = $this->_getHelper()->editStudyvariablecondition($studyvariablesconditionId, $studyId);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = (object)$results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyrowset_info"]) && $results["studyrowset_info"]) {
            $this->view->studyrowset_info = (object)$results["studyrowset_info"];
        }
        if (isset($results["studyvariablecondition_info"]) && $results["studyvariablecondition_info"]) {
            $this->view->studyvariablecondition_info = (object)$results["studyvariablecondition_info"];
        }
        if (isset($results["studyvariable_summary_categorical"]) && $results["studyvariable_summary_categorical"]) {
            $this->view->studyvariable_summary_categorical = $results["studyvariable_summary_categorical"];
        }
        if (isset($results["studyvariable_selected_maincategories"]) && $results["studyvariable_selected_maincategories"]) {
            $this->view->studyvariable_selected_maincategories = $results["studyvariable_selected_maincategories"];
        }
        if (isset($results["studyvariable_selected_subcategories"]) && $results["studyvariable_selected_subcategories"]) {
            $this->view->studyvariable_selected_subcategories = $results["studyvariable_selected_subcategories"];
        }
        if (isset($results["studyvariable_summary_numeric"]) && $results["studyvariable_summary_numeric"]) {
            $this->view->studyvariable_summary_numeric = (object)$results["studyvariable_summary_numeric"];
        }
        if (isset($results["studyvariable_summary_histogram"]) && $results["studyvariable_summary_histogram"]) {
            $this->view->studyvariable_summary_histogram = $results["studyvariable_summary_histogram"];
        }
    }

    public function createStudyrowsetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/createstudyvarset/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->createStudyrowset($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function createStudyvariablesetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/createstudyvarset/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->createStudyvariableset($studyId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editStudyrowsetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyrowset/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studyrowset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyrowset/", "dashin_owner_studyrowset_id", "studyrowset");
        }
        if ($this->response->isSent()) {
            return false;
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyrowsetId = $this->localsession->getKeyValue("dashin_owner_studyrowset_id");
        $studyrowsubsetId = $this->localsession->getKeyValue("dashin_owner_studyrowsubset_id");

        $results = $this->_getHelper()->editStudyrowset($studyrowsetId, $studyId, $studyrowsubsetId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyrowset_info"]) && $results["studyrowset_info"]) {
            $this->view->studyrowset_info = (object)$results["studyrowset_info"];
        }
        if (isset($results["studyrowsubsets"]) && $results["studyrowsubsets"]) {
            $this->view->studyrowsubsets = $results["studyrowsubsets"];
        }
        if (isset($results["studyvariableconditions"]) && $results["studyvariableconditions"]) {
            $this->view->studyvariableconditions = $results["studyvariableconditions"];
        }

    }

    /**
     * @param $page
     * @param $reset
     *
     * @return void
     */
    public function listStudyrowsetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/liststudyrowsets/", "dashin_owner_study_id", "study");
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrow = 20;
        $results = $this->_getHelper()->listStudyrowsets($studyId, $page, $nrow, $reset);
        if ($this->response->isSent()) {
            return false;
        }

        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyrowsets_paginated"]) && $results["studyrowsets_paginated"]) {
            $this->view->pagination = $results["studyrowsets_paginated"];
        }
    }

    public function viewStudyrowsetAction()
    {
    }

    /**
     * Edit a studyvariable set for export
     * (add/remove variables from the set)
     *
     * TODO: Redesign interface: maybe separate "select-from" list from
     *       "selected" list into two pages (similar to studyvariablecondition)
     *
     * @param $page1
     * @param $page2
     * @param $reset1
     * @param $reset2
     *
     * @return false|void
     */
    public function editStudyvariablesetAction($page1 = 1, $page2 = 1)
    {

        $reset1 = 0;
        $reset2 = 0;
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyvarset/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studyvarset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyvarset/", "dashin_owner_studyvariableset_id", "studyvarset");
        } elseif ($action === "createstudyvarset") {
            $this->response->redirect("/dashin/owner/studyexport/createstudyvarset/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
        } elseif ($action === "reset1") {
            $reset1 = 1;
        } elseif ($action === "reset2") {
            $reset2 = 1;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyvariablesetId = $this->localsession->getKeyValue("dashin_owner_studyvariableset_id");

        if ($action === "addvariable" || $action === "rmvariable") {
            $page1 = $this->localsession->getKeyValue("dashin_owner_editvariableset_page1");
            $page2 = $this->localsession->getKeyValue("dashin_owner_editvariableset_page2");
        } else {
            $page1 = (int)$page1;
            $page2 = (int)$page2;
            $this->localsession->setKeyValue("dashin_owner_editvariableset_page1", $page1);
            $this->localsession->setKeyValue("dashin_owner_editvariableset_page2", $page2);
        }
        $nrows = 10;
        $results = $this->_getHelper()->editStudyvariableset($page1, $page2, $nrows, $reset1, $reset2, $studyId, $studyvariablesetId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["studyvariableset_info"]) && $results["studyvariableset_info"]) {
            $this->view->studyvariableset_info = (object)$results["studyvariableset_info"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studydatasets"]) && $results["studydatasets"]) {
            $this->view->studydatasets = $results["studydatasets"];
        }
        if (isset($results["unselected_studyvariables"]) && $results["unselected_studyvariables"]) {
            $this->view->pagination1 = $results["unselected_studyvariables"];
        }
        if (isset($results["selected_studyvariables"]) && $results["selected_studyvariables"]) {
            $this->view->pagination2 = $results["selected_studyvariables"];
        }
    }

    public function listStudyvariablesetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/liststudyvarsets/", "dashin_owner_study_id", "study");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrows = 20;

        $results = $this->_getHelper()->listStudyvariablesets($page, $nrows, $reset, $studyId);
        if (isset($results["studyvariablesets_paginated"]) && $results["studyvariablesets_paginated"]) {
            $this->view->pagination = $results["studyvariablesets_paginated"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }

    }

    public function createStudyexportsetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/createstudyexportset/", "dashin_owner_study_id", "study");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $results = $this->_getHelper()->createStudyexportset($studyId);
        if (isset($results["form"])) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"])) {
            $this->view->study_info = (object)$results["study_info"];
        }
    }

    public function editStudyexportsetAction($page1 = 1, $page2 = 1)
    {

        $reset1 = 0;
        $reset2 = 0;
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyexportset/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studyexportset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/editstudyexportset/", "dashin_owner_studyexportset_id", "studyexportset");
        } elseif ($action === "reset1") {
            $reset1 = 1;
        } elseif ($action === "reset2") {
            $reset2 = 1;
        }
        if ($this->response->isSent()) {
            return false;
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyexportsetId = $this->localsession->getKeyValue("dashin_owner_studyexportset_id");

        $nrows = 20;
        if ($this->request->isPost()) {
            $page1 = $this->localsession->getKeyValue("dashin_owner_editstudyvarsetvars_page1");
            $page2 = $this->localsession->getKeyValue("dashin_owner_editstudyvarsetvars_page2");
        } else {
            $page1 = (int)$page1;
            $page2 = (int)$page2;
            $this->localsession->setKeyValue("dashin_owner_editstudyvarsetvars_page1", $page1);
            $this->localsession->setKeyValue("dashin_owner_editstudyvarsetvars_page2", $page2);
        }

        $results = $this->_getHelper()->editStudyexportset($page1, $page2, $nrows, $reset1, $reset2, $studyexportsetId, $studyId);
        if (isset($results["form"])) {
            $this->view->form = $results["form"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyexportset_info"]) && $results["studyexportset_info"]) {
            $this->view->studyexportset_info = (object)$results["studyexportset_info"];
        }
        if (isset($results["studyvariableset_info"]) && $results["studyvariableset_info"]) {
            $this->view->studyvariableset_info = (object)$results["studyvariableset_info"];
        }
        if (isset($results["studyrowset_paginated"]) && $results["studyrowset_paginated"]) {
            $this->view->pagination1 = $results["studyrowset_paginated"];
        }
        if (isset($results["studyvariableset_paginated"]) && $results["studyvariableset_paginated"]) {
            $this->view->pagination2 = $results["studyvariableset_paginated"];
        }
    }

    /**
     * View exportset and export
     *
     * TODO: Page shows nothing execpt name of study and exportset
     *       Must show relevant statistics and/or data
     *
     * @return void
     */
    public function viewStudyexportsetAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/viewstudyexportset/", "dashin_owner_study_id", "study");
        } elseif ($action === "select_studyexportset") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/viewstudyexportset/", "dashin_owner_study_id", "studyexportset");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        $studyexportsetId = $this->localsession->getKeyValue("dashin_owner_studyexportset_id");

        $results = $this->_getHelper()->viewStudyexportset($studyexportsetId, $studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["studyrowset_info"]) && $results["studyrowset_info"]) {
            $this->view->studyrowset_info = (object)$results["studyrowset_info"];
        }
        if (isset($results["studyvariableset_info"]) && $results["studyvariableset_info"]) {
            $this->view->studyvariableset_info = (object)$results["studyvariableset_info"];
        }
        if (isset($results["studyexportset_info"]) && $results["studyexportset_info"]) {
            $this->view->studyexportset_info = (object)$results["studyexportset_info"];
        }
    }

    public function listStudyexportsetsAction($page = 1)
    {

        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/liststudyexportsets/", "dashin_owner_study_id", "study");
        }

        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");

        $page = (int)$page;
        $nrows = 20;

        $results = $this->_getHelper()->listStudyexportsets($page, $nrows, $reset, $studyId);
        if (isset($results["studyexportsets_paginated"]) && $results["studyexportsets_paginated"]) {
            $this->view->pagination = $results["studyexportsets_paginated"];
        }
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }

    }

    /**
     * @return void
     */
    public function wholestudyexportAction()
    {

        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/exportstudy/", "dashin_owner_study_id", "study");
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
        
        $results = $this->_getHelper()->wholestudyexportOld($studyId);
        if (isset($results["study_info"]) && $results["study_info"]) {
            $this->view->study_info = (object)$results["study_info"];
        }
        if (isset($results["designvar_summary"]) && $results["designvar_summary"]) {
            $this->view->designvar_summary = (object)$results["designvar_summary"];
        }
        if (isset($results["study_datasets"]) && $results["study_datasets"]) {
            $this->view->study_datasets = (object)$results["study_datasets"];
        }
        if (isset($results["study_supportfiles"]) && $results["study_supportfiles"]) {
            $this->view->study_supportfiles = (object)$results["study_supportfiles"];
        }
    }
    
    public function exportStudyAsSqdrAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "select_study") {
            $this->_getHelper()->serviceSelect("/dashin/owner/studyexport/exportsqdr/", "dashin_owner_study_id", "study");
        }
        $studyId = $this->localsession->getKeyValue("dashin_owner_study_id");
//        $this->_getHelper()->exportStudyAsSqdr($studyId);
//        $this->_getHelper()->exportTempSupportfiles($studyId);
        $this->_getHelper()->sqdrImport();
    }

}