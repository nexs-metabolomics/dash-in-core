<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-02-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
$router->add(
    "/system/service/select/([a-z]+)/:params",
    [
        "namespace" => 'System\Controllers\Service',
        "controller" => "Service",
        "action" => "select",
        "params" => 1,
        "seclab_id"     => SECLAB_APP_ADMIN,
    ]
);
//$router->add(
//    "/system/service/selectorg/:params",
//    [
//        "namespace" => 'System\Controllers\Service',
//        "controller" => "Service",
//        "action" => "selectOrganization",
//        "params" => 1,
//    ]
//);
//$router->add(
//    "/system/service/selectconsort/:params",
//    [
//        "namespace" => 'System\Controllers\Service',
//        "controller" => "Service",
//        "action" => "selectConsortium",
//        "params" => 1,
//    ]
//);
//
//
