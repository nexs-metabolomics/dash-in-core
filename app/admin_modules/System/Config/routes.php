<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/** @var Router $router */

#-----------------------------------------
# default system
#-----------------------------------------
//$router->add(
//    "/(system|sysadmin|admin)(.*)",
//    [
//        "namespace"  => 'System\Controllers',
//        "controller" => "System",
//        "action"     => "index",
//        "params"     => 1,
//        "seclab_id"     => [1, 2, 3],
//    ]
//);
#-----------------------------------------
# service
#-----------------------------------------
include __DIR__ . "/routes_service.php";

#-----------------------------------------
# sysadm - uri
#-----------------------------------------
//$router->add(
//    "/updateuris",
//    [
//        "namespace" => 'System\Controllers\Sysadmin',
//        "controller" => "Sysadm",
//        "action" => "updateUris",
//        "params" => 1,
//    ]
//);
//$router->add(
//    "/listuris",
//    [
//        "namespace" => 'System\Controllers\Sysadmin',
//        "controller" => "Sysadm",
//        "action" => "listUris",
//        "params" => 1,
//    ]
//);
#-----------------------
# permission / login
#-----------------------
$router->add(
    "/login/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "login",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/logout/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "logout",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);
#---------------------------------------
# new password / reset password
#---------------------------------------
$router->add(
    "/resetpw/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "requestReset",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/confirm/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "confirmReset",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);

$router->add(
    "/confirmreset/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "confirmReset",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/confirmnew/:params",
    [
        "namespace"  => 'System\Controllers\Login',
        "controller" => "Login",
        "action"     => "confirmNewAccount",
        "params"     => 1,
        "view_base"  => "admin_modules/system/login/login",
        "seclab_id"  => SECLAB_ALL,
    ]
);

#-----------------------------------------
# sysadmin
#-----------------------------------------
$router->add(
    "/sysadmin/index/:params",
    [
        "namespace"  => 'System\Controllers',
        "controller" => "Sysadmin",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
#-----------------------
# access_rule
#-----------------------
$router->add(
    "/sysadmin/accessrule/create/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "createAccessRule",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/accessrule/list/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "listAccessRules",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/accessrule/view/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "viewAccessRule",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
#-----------------------
# role
#-----------------------
$router->add(
    "/sysadmin/role/index/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/role/list/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "listRoles",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/role/create/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "createRole",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/role/view/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "viewRole",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/role/edit/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Role",
        "action"     => "edit",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/role",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
#-----------------------
# roleacl
#-----------------------
$router->add(
    "/sysadmin/roleacl/index/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Roleacl",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/roleacl",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/roleacl/create/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Roleacl",
        "action"     => "create",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/roleacl",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/roleacl/edit/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Roleacl",
        "action"     => "edit",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/roleacl",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/roleacl/delete/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Roleacl",
        "action"     => "delete",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/roleacl",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);
$router->add(
    "/sysadmin/roleacl/list/:params",
    [
        "namespace"  => 'System\Controllers\Sysadmin',
        "controller" => "Roleacl",
        "action"     => "list",
        "params"     => 1,
        "view_base"  => "admin_modules/system/sysadmin/roleacl",
        "seclab_id"  => SECLAB_SYS_ADMIN,
    ]
);

#-----------------------------------------
# admin
#-----------------------------------------
$router->add(
    "/admin/index/:params",
    [
        "namespace"  => 'System\Controllers',
        "controller" => "Admin",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
#-----------------------
# organization
#-----------------------
$orgRouterGroup = new Group(
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Organization",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/organization",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$orgRouterGroup->setPrefix("/admin/organization");
$orgRouterGroup->add(
    "/index/:params",
    [
        "action" => "list"
    ]
);
$orgRouterGroup->add(
    "/list/:params",
    [
        "action" => "list"
    ]
);
$orgRouterGroup->add(
    "/create/:params",
    [
        "action"     => "create",
    ]
);
$orgRouterGroup->add(
    "/view/:params",
    [
        "action"     => "view",
    ]
);
$orgRouterGroup->add(
    "/edit/:params",
    [
        "action"     => "edit",
    ]
);
$orgRouterGroup->add(
    "/editroles/:params",
    [
        "action"     => "editRoles",
    ]
);
$router->mount($orgRouterGroup);
#-----------------------
# user
#-----------------------
$router->add(
    "/admin/user/index/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/user/create/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "create",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/user/createwithorg/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "createWithOrganization",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);

$router->add(
    "/admin/user/edit/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "edit",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/user/view/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "view",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/user/list/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "User",
        "action"     => "list",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/user",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
//$router->add(
//    "/admin/user/addmanager/:params",
//    [
//        "namespace"  => 'System\Controllers\Admin',
//        "controller" => "User",
//        "action"     => "addToOrganization",
//        "params"     => 1,
//        "view_base"  => "admin_modules/system/admin/user",
//        "seclab_id"  => SECLAB_APP_ADMIN,
//    ]
//);
#-----------------------
# manager
#-----------------------
$router->add(
    "/admin/manager/index/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Manager",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/manager",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/manager/create/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Manager",
        "action"     => "create",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/manager",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/manager/edit/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Manager",
        "action"     => "edit",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/manager",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/manager/list/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Manager",
        "action"     => "list",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/manager",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
$router->add(
    "/admin/manager/view/:params",
    [
        "namespace"  => 'System\Controllers\Admin',
        "controller" => "Manager",
        "action"     => "view",
        "params"     => 1,
        "view_base"  => "admin_modules/system/admin/manager",
        "seclab_id"  => SECLAB_APP_ADMIN,
    ]
);
#----------------------------------------------
# user-account
#----------------------------------------------
$userAccountRouterGroup = new Group([
        "namespace"  => 'System\Controllers\User',
        "controller" => "Home",
        "params"     => 1,
        "view_base"  => "/admin_modules/system/user/home",
        "seclab_id"  => SECLAB_ALL,

    ]
);

$userAccountRouterGroup->setPrefix("/user");
$userAccountRouterGroup->add(
    "/:params",
    [
        "action" => "index",
    ]
);
$userAccountRouterGroup->add(
    "/index/:params",
    [
        "action" => "index",
    ]
);
$userAccountRouterGroup->add(
    "/view(/)?",
    [
        "action" => "view",
    ]
);
$userAccountRouterGroup->add(
    "/edit(/)?",
    [
        "action" => "edit",
    ]
);

$router->mount($userAccountRouterGroup);