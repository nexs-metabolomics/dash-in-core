<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Sysadmin;


use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\Regex;
use System\Forms\FormBase;

class AccessRuleForm extends FormBase
{

    public function initialize()
    {
        #--------------------------------------------------------
        # key
        #--------------------------------------------------------
        $ruleKeyField = new Text("key",[
            "placeholder" => "Access Rule Key",
            "required" => true,
        ]);
        $ruleKeyField->setLabel("Access Rule Key");
        $ruleKeyField->addValidators([
            new Regex([
                'pattern' => '#^[A-Z][A-Za-z0-9_]*[A-Za-z0-9]$#',
                "message" => "Access rule key must begin with uppercase letter, only ascii, underscore and digits, no spaces"
            ])
        ]);
        $this->add($ruleKeyField);

        #--------------------------------------------------------
        # name
        #--------------------------------------------------------
        $ruleNameField = new Text("name",[
            "placeholder" => "Rule Name",
            "required" => true,
        ]);
        $ruleNameField->setLabel("Rule Name");
        $ruleNameField->addValidators([
            new Regex([
                'pattern' => $this->_serversideLegalChars,
                "message" => "Illegal characters for role name"
            ])
        ]);
        $this->add($ruleNameField);

        #--------------------------------------------------------
        # description
        #--------------------------------------------------------
        $ruleDescriptionField = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $ruleDescriptionField->setLabel("Description");
        $ruleDescriptionField->addValidators([
            new Regex([
                'pattern' => $this->_serversideLegalChars,
                "message" => "Illegal characters for role description"
            ])
        ]);
        $this->add($ruleDescriptionField);

        #--------------------------------------------------------
        # namespace
        #--------------------------------------------------------
        $namespaceField = new Text("namespace",[
            "placeholder" => "Namespace",
            "required" => true,
        ]);
        $namespaceField->setLabel("Namespace");
        $namespaceField->addValidators([
            new Regex([
                'pattern' => '#^([a-z][a-z0-9/]*[a-z0-9])|(\*)$#',
                "message" => "Namespace: either an asterisk (*), or: must begin with a letter, only ascii lowercase and digits, no spaces"
            ])
        ]);
        $this->add($namespaceField);

        #--------------------------------------------------------
        # class
        #--------------------------------------------------------
        $classField = new Text("class",[
            "placeholder" => "Class name",
            "required" => true,
        ]);
        $classField->setLabel("Class name");
        $classField->addValidators([
            new Regex([
                'pattern' => '#^([a-z][a-z0-9/]*[a-z0-9]|\*)$#',
                "message" => "Class name: either an asterisk (*), or: must begin with a letter, only ascii lowercase and digits, no spaces"
            ])
        ]);
        $this->add($classField);

        #--------------------------------------------------------
        # actions allow
        #--------------------------------------------------------
        $actyesField = new Text("actallow",[
            "placeholder" => "Allow actions",
        ]);
        $actyesField->setLabel("Allow actions");
        $actyesField->addValidators([
            new Regex([
                'pattern' => '#^([a-z][a-z0-9/,]*[a-z0-9]|\*)$#',
                "message" => "Controller: either an asterisk (*), or: must begin with a letter, only ascii lowercase and digits, no spaces"
            ])
        ]);
        $this->add($actyesField);
        #--------------------------------------------------------
        # actions deny
        #--------------------------------------------------------
        $actnoField = new Text("actdeny",[
            "placeholder" => "Deny actions",
        ]);
        $actnoField->setLabel("Deny actions");
        $actnoField->addValidators([
            new Regex([
                'pattern' => '#^([a-z][a-z0-9/,]*[a-z0-9]|\*)$#',
                "message" => "Controller: either an asterisk (*), or: must begin with a letter, only ascii lowercase and digits, no spaces"
            ])
        ]);
        $this->add($actnoField);
    }
    
}