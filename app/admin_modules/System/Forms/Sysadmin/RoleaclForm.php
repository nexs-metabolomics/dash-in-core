<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Sysadmin;


use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use System\Forms\FormBase;

class RoleaclForm extends FormBase
{
    public function initialize()
    {
        $nameField = new Text("name",[
            "placeholder" => "Name",
        ]);
        $nameField->setLabel("Name");
        $this->add($nameField);
        
        $descField = new TextArea("description",[
            "placeholder" => "Description",
        ]);
        $descField->setLabel("Description");
        $this->add($descField);
        
        $roleOptions = $this->getUserOption("role_option");
        if(!$roleOptions) {
            $roleOptions = ["","No roles"];
        }
        $roleIdField = new Select("role_id",$roleOptions);
        $roleIdField->setLabel("Role ID");
        $this->add($roleIdField);
        
        $namespaceField = new Text("namespace",[
            "placeholder" => "Namespace",
        ]);
        $namespaceField->setLabel("Namespace");
        $this->add($namespaceField);
        
        $classAllowField = new Text("class_allow",[
            "placeholder" => "Class allow",
        ]);
        $classAllowField->setLabel("Class allow");
        $this->add($classAllowField);
        
        $classDenyField = new Text("class_deny",[
            "placeholder" => "Class deny",
        ]);
        $classDenyField->setLabel("Class deny");
        $this->add($classDenyField);
        
        $actionAllowField = new Text("action_allow",[
            "placeholder" => "Action allow",
        ]);
        $actionAllowField->setLabel("Action allow");
        $this->add($actionAllowField);
        
        $actionDenyField = new Text("action_deny",[
            "placeholder" => "Action deny",
        ]);
        $actionDenyField->setLabel("Action deny");
        $this->add($actionDenyField);
        
    }
}