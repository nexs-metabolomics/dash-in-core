<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Sysadmin;


use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Filter\Validation\Validator\Regex;
use System\Forms\FormBase;

class RoleForm extends FormBase
{

    public function initialize()
    {
        #--------------------------------------------------------
        # ID
        #--------------------------------------------------------
        $roleIdField = new Numeric("role_id",[
            "placeholder" => "Role ID",
        ]);
        $roleIdField->setLabel("Role ID");
        $this->add($roleIdField);

        #--------------------------------------------------------
        # name
        #--------------------------------------------------------
        $roleNameField = new Text("name",[
            "placeholder" => "Role Name",
        ]);
        $roleNameField->setLabel("Role Name");
        $roleNameField->addValidators([
            new Regex([
                'pattern' => '#^[A-Z][A-Za-z0-9_]*[A-Za-z0-9]$#',
                "message" => "Role Name must begin with uppercase letter, only ascii, underscore and digits, no spaces"
            ])
        ]);
        $this->add($roleNameField);

        #--------------------------------------------------------
        # description
        #--------------------------------------------------------
        $roleDescriptionField = new Text("description",[
            "placeholder" => "Description",
        ]);
        $roleDescriptionField->setLabel("Description");
        $roleDescriptionField->addValidators([
            new Regex([
                'pattern' => $this->_serversideLegalChars,
                "message" => "Illegal characters for role description"
            ])
        ]);
        $this->add($roleDescriptionField);
        
        #--------------------------------------------------------
        # label
        #--------------------------------------------------------
        $labelField = new Text("rolelab",[
            "placeholder" => "Label",
        ]);
        $labelField->setLabel("Label");
        $this->add($labelField);
        
        #--------------------------------------------------------
        # long description
        #--------------------------------------------------------
        $longDescription = new TextArea("longdesc",[
            "placeholder" => "Long description",
        ]);
        $longDescription->setLabel("Long description");
        $this->add($longDescription);
    }
    
}