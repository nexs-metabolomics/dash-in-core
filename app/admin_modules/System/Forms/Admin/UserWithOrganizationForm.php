<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-03-06
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Admin;


use App\Library\Validator\Email as EmailValidator;
use App\Library\Validator\Email;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Text;
use System\Forms\FormBase;

class UserWithOrganizationForm extends FormBase
{
    public function initialize()
    {
        $fnameField = new Text("first_name", [
            "placeholder" => "First name",
        ]);
        $fnameField->setLabel("First name");

        $lnameField = new Text("last_name", [
            "placeholder" => "Last name",
        ]);
        $lnameField->setLabel("Last name");

        $userEmailField = new Text("user_email", [
            "placeholder" => "User email",
        ]);
        $userEmailField->addValidators([
            new Email([
                "message" => "Not a valid email (user email)"
            ]),
        ]);
        $userEmailField->setLabel("User email");
        
        $confirmField = new Check("confirm");
        $confirmField->setLabel("Confirm that you want to create an organization for this user");


        # organization

        # name
        $orgNamefield = new Text("name",[
            "placeholder" => "Organization name",
        ]);
        $orgNamefield->setLabel("Organization name");

        # description
        $orgDescField = new Text("description",[
            "placeholder" => "Organization description",
        ]);
        $orgDescField->setLabel("Organization description");

        # email
        $orgEmailField = new Text("org_email",[
            "placeholder" => "Organization email",
        ]);
        $orgEmailField->addValidators([
            new EmailValidator([
                "message" => "Not a valid email (organization email)"
            ]),
        ]);
        $orgEmailField->setLabel("Organization email");

        $this->add($fnameField);
        $this->add($lnameField);
        $this->add($userEmailField);
        $this->add($confirmField);
//        $this->add($orgNamefield);
//        $this->add($orgDescField);
//        $this->add($orgEmailField);

    }
}
