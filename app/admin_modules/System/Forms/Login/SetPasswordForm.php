<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Login;


use Phalcon\Forms\Element\Password;
use Phalcon\Filter\Validation\Validator\Confirmation;
use Phalcon\Filter\Validation\Validator\StringLength;
use System\Forms\FormBase;

class SetPasswordForm extends FormBase
{
    public function initialize()
    {
        $pwFld = new Password("password",[
            "placeholder" => "Password",
            "required" => true,
        ]);
        $pwFld->setLabel("Password");
        $pwFld->addValidators([
            new StringLength([
                "min" => 8,
                "messageMinimum" => "Password must be at least 8 characters long",
            ]),
            new Confirmation([
                "message" => "Passwords don't match",
                "with" => "password2",
            ]),
        ]);
        $this->add($pwFld);

        $pwFld2 = new Password("password2",[
            "placeholder" => "Confirm password",
            "required" => true,
        ]);
        $pwFld2->setLabel("Confirm password");
        $this->add($pwFld2);
    }
}