<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-28
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Forms\Login;


use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use System\Forms\FormBase;

class LoginForm extends FormBase
{
    public function initialize()
    {
        # name
        $nameField = new Text("username",[
            "placeholder" => "USR-ABCD-1234",
            "autofocus" => "1",
        ]);
        $nameField->setLabel("User code");
        $this->add($nameField);
        
        # password
        $passwordField = new Password("password",[
            "placeholder" => "Password",
        ]);
        $passwordField->setLabel("Password");
        $this->add($passwordField);
    }
}