<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-01
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Library\Permission;


use Phalcon\Db\Enum;
use System\Library\ComponentBase;

class PermissionRepository extends ComponentBase
{
    public function actionAllow($roleIds, $namespace, $class, $action)
    {
        try{
            $sql = PermissionQuery::$system_action_allow;
            
            $result = $this->db->fetchOne($sql,Enum::FETCH_NUM,[
                "role_id" => $roleIds,
                "namespace" => $namespace,
                "class" => $class,
                "action" => $action,
            ]);
            
            if(isset($result["allow"])) {
                return $result["allow"];
            }
            
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

}