<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-25
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Library\SessionUser;

use App\Library\Security\RolePermission;
use System\Library\ComponentBase;

/**
 * Class SessionUser
 *
 * @package System\Library\SessionUser
 */
class SessionUser extends ComponentBase
{

    /**
     * @var array
     */
    private $_sessionUserData;

    /**
     * @var bool
     */
    private $_isLoggedIn = false;

    /**
     * @var RedirectContext
     */
    private $_preloginContext;

    /**
     * @var RedirectContext
     */
    private $_preallowContext;

    /**
     * @var SessionUserRepository
     */
    private $_repository;

    /**
     * @var  RolePermission
     */
    private $_permission;

    /**
     * @return SessionUserRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new SessionUserRepository();
        }

        return $this->_repository;
    }

    public function __construct()
    {

        $this->_init();
    }

    /**
     * Initialiae SessionUser
     *
     * @return bool
     */
    private function _init()
    {

        $userTemp = $this->session->get("session_user");
        if (!$userTemp) {
            $this->_isLoggedIn = false;

            return false;
        }

        # get the user from db
        $userId = $userTemp["user_id"];
        $data = $this->_setupUserData($userId);
        if (!$data) {
            $this->_isLoggedIn = false;
            unset($this->_sessionUserData);

            return false;
        }
        $this->_isLoggedIn = true;
        $this->_sessionUserData = $data;

        # todo ----------------------------------------------------
        # TODO:set activeManager here?
        # todo ----------------------------------------------------

        $this->_permission = new RolePermission($userId);

        return true;
    }

    private function _setupUserData($userId)
    {

        $userData = $this->_getRepository()->getUser($userId);
        if (!$userData) {
            return false;
        }

        #------------------------------------------
        # show explicitly - work in progress
        #------------------------------------------
        # userData will become more complex
        # maybe make it into a dedicated object
        #------------------------------------------
        $data["user_id"] = $userData["user_id"];
        $data["first_name"] = $userData["first_name"];
        $data["last_name"] = $userData["last_name"];
        $data["email"] = $userData["email"];
        $data["status"] = $userData["status"];
        $data["active_manager"]["organization_id"] = $userData["active_org_id"];
        $data["active_manager"]["organization_name"] = $userData["organization_name"];
        $data["active_manager"]["roles"] = $userData["manager_roles"];
        $data["active_manager"]["roles_json"] = $userData["manager_roles_json"];
        $data["details"] = json_decode($userData["details"], JSON_OBJECT_AS_ARRAY);

        return $data;
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function register($userId)
    {

        $data = $this->_setupUserData($userId);
        if (!$data) {
            return false;
        }

        $this->_sessionUserData = $data;

        $this->session->set("session_user", $data);
        $this->_isLoggedIn = true;
    }

    public function unregister()
    {

        $this->_isLoggedIn = false;
        $this->_sessionUserData = null;
        $this->session->remove("session_user");
    }

    public function get($name, $default = null)
    {

        if (isset($this->_sessionUserData[$name])) {
            return $this->_sessionUserData[$name];
        }

        return $default;
    }

    public function getUserId()
    {

        $userId = $this->get("user_id");

        return $userId;
    }

    public function getUserFullName()
    {

        $fname = $this->get("first_name");
        $lname = $this->get("last_name");

        return $fname . " " . $lname;
    }

    public function setActiveManager($organizationId)
    {

        $ok = $this->_getRepository()->setActiveManager($organizationId);
        if ($ok) {
            $organization = $this->_getRepository()->getOrganization($organizationId);
            if ($organization) {
                $this->_sessionUserData["active_manager"]["organization_id"] = $organizationId;
                $this->_sessionUserData["active_manager"]["organization_name"] = $organization["name"];
            }
        }
    }

    public function unsetActiveManager()
    {

        $this->_getRepository()->unsetActiveManager();
        $this->_sessionUserData["active_manager"]["organization_id"] = "";
        $this->_sessionUserData["active_manager"]["organization_name"] = "";
    }

    public function getActiveManagerOrgId()
    {

        if (!isset($this->_sessionUserData["active_manager"]["organization_id"])) {
            $orgId = $this->_getRepository()->getActiveManagerOrgId();
            $this->_sessionUserData["active_manager"]["organization_id"] = $orgId;
        }

        return $this->_sessionUserData["active_manager"]["organization_id"];
    }

    public function getActiveManagerOrgName($default = "")
    {

        if (isset($this->_sessionUserData["active_manager"]["organization_name"])) {
            return $this->_sessionUserData["active_manager"]["organization_name"];
        }

        return $default;
    }

    public function getManagerRoles()
    {

        if (isset($this->_sessionUserData["active_manager"]["roles"])) {
            return $this->_sessionUserData["active_manager"]["roles"];
        }

        return null;
    }

    public function getManagerRolesJson()
    {

        if (isset($this->_sessionUserData["active_manager"]["roles_json"])) {
            return $this->_sessionUserData["active_manager"]["roles_json"];
        }

        return null;
    }

    public function setPreloginContext()
    {

        if (!$this->_preloginContext) {
            $this->_preloginContext = new RedirectContext("prelogin_context");
        }
        $this->_preloginContext->set();
    }

    public function getPreloginContext()
    {

        if (!$this->_preloginContext) {
            $this->_preloginContext = new RedirectContext("prelogin_context");
        }

        return $this->_preloginContext->get();
    }

    public function removePreloginContext()
    {

        if ($this->_preloginContext) {
            $this->_preloginContext->remove();
        }
    }

    public function getPreallowContext()
    {

        if (!$this->_preallowContext) {
            $this->_preallowContext = new RedirectContext("preallow_context", true);
        }

        return $this->_preallowContext;
    }

    public function isLoggedIn($validate = true)
    {

        if ($this->_isLoggedIn) {
            # if logged in, check status
            $status = $this->get("status");
            if ($status > 0) {
                return true;
            }
            if (!$validate) {
                return false;
            }
            # invalid status, force logout
            # use internal forward instead of redirect
            $this->dispatcher->forward([
                "namespace"  => 'System\Controllers\Permission',
                "controller" => "login",
                "action"     => "logout",
            ]);
            $this->response->redirect("/");
            $this->response->send();
        }

        return false;
    }

    public function securedLogggedIn()
    {

        if (!$this->isLoggedIn()) {
            $this->setPreloginContext();

            return false;
        }
        $this->auditLogger->auditlog($this);

        $this->removePreloginContext();

        return true;
    }

    public function allowWrite()
    {

        return $this->_permission->ROLE_ORG_ALLOW_WRITE;
    }

    public function isAllowed()
    {

        return $this->_permission->ALLOW_ROUTE;

//        $managerRoles = $this->getManagerRolesJson();
//        if (!$managerRoles) {
//            return false;
//        }
//        $uri = $this->_getCurrentUri();
//        $allow = $this->_getRepository()->actionAllow($managerRoles, $uri["n"], $uri["c"], $uri["a"]);
//        if ($allow) {
//            # $this->view->DEBUG_PERM = "YES: ".implode("/",$uri);
//            return true;
//        }
//        # $this->view->DEBUG_PERM = "NO: ".implode("/",$uri);
//        return false;
    }

    private function _getCurrentUri()
    {

        $uri["n"] = strtolower(str_replace("\\", "/", $this->router->getNamespaceName()));
        $uri["c"] = strtolower($this->router->getControllerName());
        $uri["a"] = strtolower($this->router->getActionName());

        return $uri;
    }

    public function counter($name, $change = 1, $reset = false)
    {

        if ($reset) {
            $this->session->remove($name);
        } else {
            $count = (int)$this->session->get($name);
            if ($change) {
                $count = $count + $change;
                $this->session->set($name, $count);
            }

            return $count;
        }
    }

    public function saveToSession()
    {

        if ($this->_sessionUserData) {
            $this->session->set("session_user", $this->_sessionUserData);
        }
    }

    public function __destruct()
    {

        $this->saveToSession();
    }

}