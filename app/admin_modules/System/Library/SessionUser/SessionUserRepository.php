<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-29
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Library\SessionUser;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use System\Helpers\HelperBase;

class SessionUserRepository extends HelperBase
{

    /**
     * Get user data for the current user
     *
     * @param $userId
     *
     * @return array|bool
     */
    public function getUser($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = SessionUserQuery::$system_get_login_user;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                $data["manager_roles"] = Json::jsonDecode($data["manager_roles_json"], JSON_OBJECT_AS_ARRAY);

                return $data;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Set active manager
     * (set active_org_id = $organizationId)
     *
     * @param $organizationId
     *
     * @return bool
     */
    public function setActiveManager($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = SessionUserQuery::$session_user_set_active_manager;

            $userId = $this->SU->getUserId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Unset the active manager
     * (set active_org_id = NULL)
     *
     * @return bool
     */
    public function unsetActiveManager()
    {

        try {
            $sql = SessionUserQuery::$user_unset_active_manager;

            $userId = $this->SU->getUserId();

            $state = $this->db->execute($sql, [
                "user_id" => $userId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get the organization_id for active manager
     *
     * @return bool
     */
    public function getActiveManagerOrgId()
    {

        try {
            $sql = SessionUserQuery::$session_user_get_active_manager_org_id;

            $userId = $this->SU->getUserId();

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if (isset($data["active_org_id"])) {
                return $data["active_org_id"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getOrganization($organizationId)
    {

        if (!UniqueId::accountname($organizationId)) {
            return false;
        }

        try {
            $sql = SessionUserQuery::$system_get_organization;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);
            if ($data) {
                return $data;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getManagerRoles()
    {

        try {
            $sql = SessionUserQuery::$session_user_get_active_manager_roles;

            $userId = $this->SU->getUserId();
            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id"         => $userId,
                "organization_id" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function actionAllow($roleIds, $namespace, $class, $action)
    {

        try {
            $sql = SessionUserQuery::$system_action_allow;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "role_id"   => $roleIds,
                "namespace" => $namespace,
                "class"     => $class,
                "action"    => $action,
            ]);

            if (isset($result["allow"])) {
                return $result["allow"];
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}