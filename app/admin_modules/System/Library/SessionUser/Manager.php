<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-01
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Library\SessionUser;

use System\Library\ComponentBase;

class Manager extends ComponentBase
{

    /**
     * @var string uuid
     */
    private $_userId;

    /**
     * @var string uuid
     */
    private $_organizationId;

    /**
     * @var ManagerRepository
     */
    private $_repository;

    /**
     * @return ManagerRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new ManagerRepository();
        }

        return $this->_repository;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {

        $this->_userId = $userId;
    }

    /**
     * @return string uuid
     */
    public function getUserId()
    {

        return $this->_userId;
    }

    /**
     * @param $organizationId
     */
    public function setOrganizationId($organizationId)
    {

        $this->_organizationId = $organizationId;
    }

    /**
     * @return string uuid
     */
    public function getOrganizationId()
    {

        return $this->_organizationId;
    }

}