<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-29
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Library\SessionUser;

use System\Library\ComponentBase;

class RedirectContext extends ComponentBase
{

    /**
     * @var string
     */
    private $_name;

    /**
     * Prevent repeated redirects
     *
     * @var bool
     */
    private $_preventRepeated;

    private $_data;

    public function __construct($name, $preventRepeatedRedirects = false)
    {

        $this->_name = $name;

        $this->_preventRepeated = $preventRepeatedRedirects;

    }

    public function set()
    {

        $data = (array)$this->session->get($this->_name);
        $data["target"] = $this->request->getURI();

        if ($this->_preventRepeated) {
            if (isset($data["redirect"])) {
                $data["redirect"] = false;
            } else {
                $data["redirect"] = true;
            }
        }

        $this->session->set($this->_name, $data);
        $this->_data = $data;
    }

    public function get()
    {

        if (!$this->_data) {
            $this->_data = $this->session->get($this->_name);
        }

        return $this->_data;
    }

    public function remove()
    {

        $this->session->remove($this->_name);
    }

    public function getUri()
    {

        $this->get();
        if (isset($this->_data["target"])) {
            return $this->_data["target"];
        }
    }

    public function isSet()
    {

        $this->get();

        return isset($this->_data);
    }

    public function redirect()
    {

        $redirectUri = $this->getUri();
        $this->remove();
        $this->response->redirect($redirectUri);
        $this->response->send();
    }

}