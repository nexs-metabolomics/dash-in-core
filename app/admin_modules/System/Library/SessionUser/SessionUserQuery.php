<?php

namespace System\Library\SessionUser;

use System\Helpers\QueryBase;

class SessionUserQuery extends QueryBase
{

    static string $session_user_set_active_manager =
        <<<'EOD'
            UPDATE admin.users
            SET active_org_id = :organization_id
            WHERE user_id = :user_id
              AND exists
              (
                SELECT
                FROM admin.manager
                WHERE (user_id, organization_id) = (:user_id, :organization_id)
              )
            RETURNING 1 AS ok;
        EOD;

    static string $user_unset_active_manager =
        <<<'EOD'
            UPDATE admin.users
            SET active_org_id = NULL
            WHERE user_id = :user_id;
        EOD;

    static string $session_user_get_active_manager_org_id =
        <<<'EOD'
            SELECT active_org_id
            FROM admin.users
            WHERE user_id = :user_id
        EOD;

    static string $session_user_get_active_manager_roles =
        <<<'EOD'
            SELECT admin.get_manager_roles(:organization_id, :user_id) AS roles;
        EOD;

}