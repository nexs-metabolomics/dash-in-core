<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-25
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Library\Session\Adapter;


use Phalcon\Db\Enum;
use \Exception;
use SessionHandlerInterface;

/**
 * Class Database
 *
 * @package System\Library\Session\Adapter
 */
class Database implements SessionHandlerInterface
{
    protected $isDestroyed = false;

    private $_options;

    public function __construct($options = null)
    {
        if (!isset($options['db'])) {
            throw new Exception("The parameter 'db' is required");
        }

        @ini_set("session.gc_probability", 1);
        $this->_options = $options;
        #parent::__construct($options);

        session_set_save_handler(
            array($this, 'open'),
            array($this, 'close'),
            array($this, 'read'),
            array($this, 'write'),
            array($this, 'destroy'),
            array($this, 'gc')
        );
    }

    /**
     * {@inheritdoc}
     * @return boolean
     */
    public function open($path, $name):bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     * @return boolean
     */
    public function close():bool
    {
        return false;
    }

    /**
     * @param $sessionId
     *
     * @return string
     */
    public function read($sessionId):string|false
    {
        $maxlifetime = (int)ini_get('session.gc_maxlifetime');
        try {
            /** @var \Phalcon\Db\Adapter\Pdo\Postgresql $db */
            $db = $this->_options['db'];
            $sql =
                "SELECT data FROM admin.session\n" .
                "  WHERE session_id = :session_id\n" .
                "  AND COALESCE(updated_at,created_at) + (:maxlifetime || ' seconds')::INTERVAL >= NOW();";

            $sessionData = $db->fetchOne($sql, Enum::FETCH_NUM, [
                "session_id"  => $sessionId,
                "maxlifetime" => $maxlifetime,
            ]);

        } catch (\PDOException $e) {
            error_log("session_read error: $sessionId");
            error_log($e->getMessage());
            return false;
        }

        if (isset($sessionData[0])) {
            return $sessionData[0];
        }

        return '';
    }

    /**
     * @param $sessionId
     * @param $data
     *
     * @return bool
     */
    public function write($sessionId, $data):bool
    {
        if ($this->isDestroyed) {
            return false;
        }

        try {
            /** @var \Phalcon\Db\Adapter\Pdo\Postgresql $db */
            $db = $this->_options['db'];

            $sql =
                "INSERT INTO admin.session (session_id, data) VALUES\n" .
                "    (:session_id,:data)\n" .
                "  ON CONFLICT (session_id)\n" .
                "  DO UPDATE SET (data,updated_at) = (:data,NOW());";

            $state = $db->execute($sql, [
                "session_id" => $sessionId,
                "data"       => $data,
            ]);
            return $state;

        } catch (\PDOException $e) {
            error_log("session_write error: $sessionId");
            error_log($e->getMessage());
        }
        return false;
    }

    /**
     * {@inheritdoc}
     * @return boolean
     */
    public function destroy($sessionId = null):bool
    {
        if (session_status() !== PHP_SESSION_ACTIVE || $this->isDestroyed) {
            return true;
        }

        if (is_null($sessionId)) {
            $sessionId = $this->getId();
        }

        try {
            /** @var \Phalcon\Db\Adapter\Pdo\Postgresql $db */
            $db = $this->_options['db'];
            $sql = "DELETE FROM admin.session WHERE session_id = :session_id";

            $state = $db->execute($sql, [
                "session_id" => $sessionId
            ]);
            if ($state) {
                $this->isDestroyed = true;
                session_regenerate_id();
            }
            return $state;

        } catch (\PDOException $e) {
            error_log("session_destroy error: $sessionId");
            error_log($e->getMessage());
        }
        return false;
    }

    /**
     * @return bool
     */
    public function refresh()
    {
        $oldSessionId = $this->getId();
        session_regenerate_id();
        $newSessionId = $this->getId();

        try {
            /** @var \Phalcon\Db\Adapter\Pdo\Postgresql $db */
            $db = $this->_options['db'];
            $sql = "UPDATE admin.session SET session_id = :new_id,updated_at = now() WHERE session_id = :old_id";

            $state = $db->execute($sql, [
                "old_id" => $oldSessionId,
                "new_id" => $newSessionId,
            ]);
            if ($state) {
                $this->isDestroyed = true;
            }
            return $state;

        } catch (\PDOException $e) {
            error_log("session_refresh error: $oldSessionId / $newSessionId");
            error_log($e->getMessage());
        }
        return false;
    }

    /**
     * @param $maxLifetime
     *
     * @return bool
     */
    public function gc($maxLifetime):int|false
    {
        try {
            /** @var \Phalcon\Db\Adapter\Pdo\Postgresql $db */
            $db = $this->_options['db'];
            $sql = "DELETE FROM admin.session WHERE updated_at + (:max_lifetime || ' seconds')::INTERVAL < now()";

            $state = $db->execute($sql, [
                "max_lifetime" => $maxLifetime,
            ]);
            return $state;

        } catch (\PDOException $e) {
            error_log("session_gc: error");
            error_log($e->getMessage());
        }
        return false;
    }
}
