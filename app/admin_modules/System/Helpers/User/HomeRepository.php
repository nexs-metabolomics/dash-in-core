<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\User;

use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use System\Helpers\HelperBase;

class HomeRepository extends HelperBase
{

    /**
     * View current user (SU)
     *
     * @return array|bool
     */
    public function getUserForView()
    {

        try {
            $sql = HomeQuery::$user_get_user_for_view;
            $userId = $this->SU->get("user_id");

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);

            if ($data) {
                return $data;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get all managers atteched to current user (SU)
     *
     * @return array|bool
     */
    public function getManagersByUser()
    {

        try {
            $sql = HomeQuery::$user_get_managers_by_user;
            $userId = $this->SU->get("user_id");

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);

            if ($data) {
                return $data;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getManagerInfo()
    {

        $organizationId = $this->SU->getActiveManagerOrgId();
        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = HomeQuery::$admin_manager_get_manager_info;

            $userId = $this->SU->getUserId();

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
            ]);

            if ($data) {
                return $data;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function updateUser($userId, $params)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = HomeQuery::$user_update;
            $emailCanonical = mb_strtolower(trim($params["email"]));

            $params["is_public"] = (int)(isset($params["is_public"]) && $params["is_public"]);
            $state = $this->db->execute($sql, [
                "user_id"         => $userId,
                "first_name"      => $params["first_name"],
                "last_name"       => $params["last_name"],
                "email"           => $params["email"],
                "email_canonical" => $emailCanonical,
                "is_public"       => $params["is_public"],
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    public function getUser($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = HomeQuery::$user_get_user;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                # ensure is_public set to false if null
                $data["is_public"] = isset($data["is_public"]) && $data["is_public"];

                return $data;
            }
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

}