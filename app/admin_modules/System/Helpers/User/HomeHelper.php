<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\User;

use System\Forms\User\UserEditForm;
use System\Helpers\HelperBase;

class HomeHelper extends HelperBase
{

    /**
     * @var HomeRepository
     */
    private $_repository;

    /**
     * @return HomeRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new HomeRepository();
        }

        return $this->_repository;
    }

    public function view()
    {

        $changed = false;
        $action = $this->Btn->getAction();
        if ($action === "select") {
            $organizationId = $this->Btn->getValue();
            $this->SU->setActiveManager($organizationId);
            $changed = true;
        } elseif ($action === "deselect") {
            $this->SU->unsetActiveManager();
            $changed = true;
        }
        $userData = $this->_getRepository()->getUserForView();
        if (!$userData) {
            return ["user" => false, "managers" => false];
        }
        $managers = $this->_getRepository()->getManagersByUser();
        if (!$managers) {
            return ["user" => $userData, "managers" => false];
        }
        foreach ($managers as &$manager) {
            $manager = (object)$manager;
        }
        unset($manager);

        $managerInfo = $this->_getRepository()->getManagerInfo();

        return [
            "user"         => $userData,
            "managers"     => $managers,
            "changed"      => $changed,
            "manager_info" => $managerInfo
        ];
    }

    public function setManager()
    {

        $action = $this->Btn->getAction();
        if ($action === "select") {
            $organizationId = $this->request->getPost("organization_id");
        }
    }

    public function editUser($userId)
    {

        $form = new UserEditForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $this->_getRepository()->updateUser($userId, $post);
                $action = $this->Btn->getAction();
                if ($action === "submit") {
                    $this->response->redirect("user/view/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $user = $this->_getRepository()->getUser($userId);
        if (!$user) {
            $this->response->redirect("/");
            $this->response->send();

            return false;
        }
        $form->bindValues($user);
//        if($form->has("is_public")) {
//            if(isset($user["is_public"]) && $user["is_public"]) {
//                $form->get("is_public")->setDefault("checked");
//            } else {
//                $form->get("is_public")->setAttribute("checked",false);
//                $form->get("is_public")->clear();
//                $form->get("is_public")->setAttributes([]);
//            }
//        }
        return [
            "form" => $form,
            "user" => $user,
        ];
    }

}