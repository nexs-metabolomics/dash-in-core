<?php

namespace System\Helpers\User;

use System\Helpers\QueryBase;

class HomeQuery extends QueryBase
{

    static string $user_get_user_for_view =
        <<<'EOD'
            SELECT u.user_id
                 , u.first_name
                 , u.last_name
                 , u.label
                 , u.email
                 , u.code
                 , coalesce(jsonb_extract_path_text(details, 'is_public')::BOOL::INT, 0) AS is_public
            FROM admin.users u
            WHERE u.user_id = :user_id;
        EOD;

    static string $user_get_managers_by_user =
        <<<'EOD'
            WITH
              manager_query AS (
                                 SELECT a.organization_id
                                      , a.label
                                      , b.name
                                      , c.user_id IS NOT NULL AS active
                                      , d.role_id
                                 FROM admin.manager                a
                                      LEFT JOIN admin.organization b USING (organization_id)
                                      LEFT JOIN admin.users        c ON (c.active_org_id, c.user_id) = (a.organization_id, a.user_id)
                                    , LATERAL (SELECT min(d0.role_id)
                                               FROM (
                                                      SELECT jsonb_array_elements_text(role_ids)::INT AS role_id
                                                      FROM admin.get_manager_roles(a.organization_id, a.user_id) z (role_ids)
                                                    ) d0
                                   )                               d (role_id)
                                 WHERE a.user_id = :user_id
                                 ORDER BY a.organization_id
                               )
              , role_query  AS (
                                 SELECT role_name
                                      , role_id
                                 FROM (
                                        VALUES ('ROLE_SYS_ADMIN', 1)
                                             , ('ROLE_APP_ADMIN', 10)
                                             , ('ROLE_ORG_ADMIN', 500)
                                             , ('ROLE_ORG_USER', 510)
                                             , ('ROLE_ORG_READONLY', 590)
                                             , ('ROLE_ALL', 1000)
                                      ) a(role_name, role_id)
                               )
            SELECT a.organization_id
                 , a.label
                 , a.name
                 , a.active
                 , a.role_id
                 , b.role_name
            FROM manager_query     a
              LEFT JOIN role_query b ON a.role_id = b.role_id
            ORDER BY a.organization_id;
        EOD;

    static string $admin_manager_get_manager_info =
        <<<'EOD'
            WITH
              manager_query AS (
                                 SELECT a.label AS manager_label
                                      , b.name  AS organization_name
                                      , b.description
                                      , a.organization_id
                                      , a.user_id
                                      , c.role_id
                                 FROM admin.manager                 a
                                      INNER JOIN admin.organization b ON a.organization_id = b.organization_id
                                    , LATERAL (SELECT min(c0.role_id)
                                               FROM (
                                                      SELECT jsonb_array_elements_text(role_ids)::INT AS role_id
                                                      FROM admin.get_manager_roles(a.organization_id, a.user_id) z (role_ids)
                                                    ) c0
                                   )                                c (role_id)
                                 WHERE (a.user_id, a.organization_id) = (:user_id, :organization_id)
                               )
              , role_query  AS (
                                 SELECT role_name
                                      , role_id
                                 FROM (
                                        VALUES ('ROLE_SYS_ADMIN', 1)
                                             , ('ROLE_APP_ADMIN', 10)
                                             , ('ROLE_ORG_ADMIN', 500)
                                             , ('ROLE_ORG_USER', 510)
                                             , ('ROLE_ORG_READONLY', 590)
                                             , ('ROLE_ALL', 1000)
                                      ) a(role_name, role_id)
                               )
            SELECT a.manager_label
                 , a.organization_name
                 , a.description
                 , a.organization_id
                 , a.user_id
                 , a.role_id
                 , b.role_name
            FROM manager_query     a
              LEFT JOIN role_query b ON a.role_id = b.role_id;
        EOD;

    static string $user_update =
        <<<'EOD'
            UPDATE admin.users
            SET (first_name, last_name, email, email_canonical, updated_at) =
              (:first_name, :last_name, :email, :email_canonical, now())
              , details = jsonb_set_deep(coalesce(details, '{}'), '{is_public}', to_jsonb((:is_public)::TEXT))
            WHERE user_id = :user_id;
        EOD;

    static string $user_get_user =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
                 , coalesce(jsonb_extract_path_text(details, 'is_public')::BOOL::INT, 0) AS is_public
            FROM admin.users
            WHERE user_id = :user_id;
        EOD;

}