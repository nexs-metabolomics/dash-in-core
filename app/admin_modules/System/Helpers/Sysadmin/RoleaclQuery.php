<?php

namespace System\Helpers\Sysadmin;

class RoleaclQuery extends SysadminQueryBase
{

    static string $sysadmin_roleacl_create =
        <<<'EOD'
            INSERT INTO admin.roleacl (name, description, role_id, namespace, class_allow, class_deny, action_allow, action_deny)
            VALUES (:name, :description, :role_id, :namespace, :class_allow, :class_deny, :action_allow, :action_deny)
            RETURNING roleacl_id;
        EOD;

    static string $sysadmin_roleacl_update =
        <<<'EOD'
            UPDATE admin.roleacl
            SET (name, description, namespace, class_allow, class_deny, action_allow, action_deny, updated_at)
              = (:name, :description, :namespace, :class_allow, :class_deny, :action_allow, :action_deny, now())
            WHERE roleacl_id = :roleacl_id;
        EOD;

    static string $sysadmin_roleacls_get =
        <<<'EOD'
            SELECT roleacl_id
                 , ra.name        AS roleacl_name
                 , ra.description AS roleacl_description
                 , ra.role_id
                 , ra.namespace
                 , ra.class_allow
                 , ra.class_deny
                 , ra.action_allow
                 , ra.action_deny
                 , r.name         AS role_name
            FROM admin.roleacl     ra
              LEFT JOIN admin.role r USING (role_id)
        EOD;

    static string $sysadmin_roleacl_get_for_edit =
        <<<'EOD'
            SELECT roleacl_id
                 , ra.name
                 , ra.description
                 , ra.role_id
                 , ra.namespace
                 , ra.class_allow
                 , ra.class_deny
                 , ra.action_allow
                 , ra.action_deny
                 , r.name AS role_name
            FROM admin.roleacl     ra
              LEFT JOIN admin.role r USING (role_id)
            WHERE roleacl_id = :roleacl_id
        EOD;

    static string $sysadmin_roleacl_delete =
        <<<'EOD'
            DELETE
            FROM admin.roleacl
            WHERE roleacl_id = :roleacl_id;
        EOD;

}