<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use App\Library\Utils\Json;
use Phalcon\Paginator\Adapter\NativeArray as Paginator;
use System\Forms\Sysadmin\RoleaclForm;
use System\Library\ComponentBase;

class RoleaclHelper extends ComponentBase
{

    /**
     * @var RoleaclRepository
     */
    private $_repository;

    /**
     * @return RoleaclRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new RoleaclRepository();
        }

        return $this->_repository;
    }

    /**
     * Create a new role based access rule
     *
     * @return RoleaclForm
     */
    public function createRoleacl()
    {

        # get available roles
        $roles = $this->_getRepository()->getRoles();
        if ($roles) {
            $tmp = [];
            foreach ($roles as $role) {
                $tmp[$role["role_id"]] = $role["name"];
            }
            $roleOptions["role_option"] = $tmp;
        } else {
            $roleOptions = null;
        }

        $form = new RoleaclForm(null, $roleOptions);

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $pattern = "#[^a-zA-Z0-9\*]#";
                $classAllowArr = preg_split($pattern, $post["class_allow"]);
                $classDenyArr = preg_split($pattern, $post["class_deny"]);
                $actionAllowArr = preg_split($pattern, $post["action_allow"]);
                $actionDenyArr = preg_split($pattern, $post["action_deny"]);

                $post["class_allow"] = Json::jsonEncode($classAllowArr);
                $post["class_deny"] = Json::jsonEncode($classDenyArr);
                $post["action_allow"] = Json::jsonEncode($actionAllowArr);
                $post["action_deny"] = Json::jsonEncode($actionDenyArr);

                $roleaclId = $this->_getRepository()->createRoleacl($post);
                if ($roleaclId) {
                    # redirect to?
                }
            }
        }

        return $form;
    }

    /**
     * Edit role based access rule
     *
     * @param $roleacldi
     *
     * @return RoleaclForm
     */
    public function editRoleacl($roleacldi)
    {

        $form = new RoleaclForm();
        if ($form->has("role_id")) {
            $form->remove("role_id");
        }
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $pattern = "#[^a-zA-Z0-9\*]#";
                $classAllowArr = preg_split($pattern, $post["class_allow"]);
                $classDenyArr = preg_split($pattern, $post["class_deny"]);
                $actionAllowArr = preg_split($pattern, $post["action_allow"]);
                $actionDenyArr = preg_split($pattern, $post["action_deny"]);

                $post["class_allow"] = Json::jsonEncode($classAllowArr);
                $post["class_deny"] = Json::jsonEncode($classDenyArr);
                $post["action_allow"] = Json::jsonEncode($actionAllowArr);
                $post["action_deny"] = Json::jsonEncode($actionDenyArr);

                $result = $this->_getRepository()->updateRoleacl($roleacldi, $post);
                # redirect?
            }
        }
        $roleacl = $this->_getRepository()->getRoleacl($roleacldi);
        if ($roleacl) {
            $form->bindValues($roleacl);
        }

        return ["form" => $form, "roleacl" => $roleacl];
    }

    /**
     * List role based access rules
     *
     * @param $page
     * @param $nrows
     *
     * @return bool|\stdClass
     */
    public function listRoleacls($page, $nrows)
    {

        $data = $this->_getRepository()->getRoleacls();
        if (!$data) {
            return false;
        }
        foreach ($data as &$row) {
            $row["class_allow"] = implode(",", Json::jsonDecode($row["class_allow"], JSON_OBJECT_AS_ARRAY));
            $row["class_deny"] = implode(",", Json::jsonDecode($row["class_deny"], JSON_OBJECT_AS_ARRAY));
            $row["action_allow"] = implode(",", Json::jsonDecode($row["action_allow"], JSON_OBJECT_AS_ARRAY));
            $row["action_deny"] = implode(",", Json::jsonDecode($row["action_deny"], JSON_OBJECT_AS_ARRAY));
            $row = (object)$row;
        }
        unset($row);
        $paginator = new Paginator([
            "data"  => $data,
            "limit" => $nrows,
            "page"  => $page,
        ]);
        $pagination = $paginator->paginate();

        return $pagination;
    }

    /**
     * Delete role based access rule
     *
     * @param $roleaclId
     *
     * @return array|bool
     */
    public function deleteRoleacl($roleaclId)
    {

        $confirm = false;
        $action = $this->Btn->getAction();
        if ($action === "delete") {
            $savedRoleaclId = $this->session->get("sysadmin_delete_roleacl");
            if ($savedRoleaclId === $roleaclId) {
                # 2nd time -> delete
                $this->session->remove("sysadmin_delete_roleacl");
                # $state = $this->_getRepository()->deleteRoleacl($roleaclId);
                $state = true;
                if ($state) {
                    # redirect to list
                    $this->response->redirect("/sysadmin/roleacl/list/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            } else {
                $confirm = true;
                $this->session->set("sysadmin_delete_roleacl", $roleaclId);
            }
        }
        $data = $this->_getRepository()->getRoleacl($roleaclId);
        if (!$data) {
            $data = false;
        }

        return ["roleacl" => $data, "confirm" => $confirm];
    }

}