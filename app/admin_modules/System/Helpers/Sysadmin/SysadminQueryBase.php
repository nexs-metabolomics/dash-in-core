<?php

namespace System\Helpers\Sysadmin;

use System\Helpers\QueryBase;

class SysadminQueryBase extends QueryBase
{

    static string $sysadmin_get_roles =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role
            WHERE role_id > 0
            ORDER BY role_id;
        EOD;

    static string $sysadmin_update_uri =
        <<<'EOD'
            INSERT INTO admin.route
              (route_key, namespace, controller, action, pattern, uri, updated_at)
            VALUES (:route_key, :namespace, :controller, :action, :pattern, :uri, now())
            ON CONFLICT (route_key) DO UPDATE SET (pattern, uri, updated_at) = (:pattern, :uri, now());
        EOD;

    static string $sysadmin_get_uris =
        <<<'EDI'
            SELECT namespace
                 , controller
                 , action
                 , pattern
                 , uri
                 , n_lab
                 , c_lab
                 , a_lab
                 , n_ord
                 , c_ord
                 , a_ord
            FROM admin.route
            ORDER BY n_ord
                   , namespace
                   , c_ord
                   , controller
                   , a_ord
                   , action
        EDI;

    static string $sysadmin_get_uri_namespaces =
        <<<'EOD'
            SELECT DISTINCT namespace
                          , n_ord
            FROM admin.route
            ORDER BY n_ord
                   , namespace
        EOD;

    static string $sysadmin_get_uri_controllers =
        <<<'EOD'
            SELECT namespace
                 , controller
                 , action
                 , pattern
                 , uri
                 , n_lab
                 , c_lab
                 , a_lab
                 , n_ord
                 , c_ord
                 , a_ord
            FROM admin.route
            ORDER BY n_ord
                   , namespace
                   , c_ord
                   , controller
                   , a_ord
                   , action;
        EOD;

}