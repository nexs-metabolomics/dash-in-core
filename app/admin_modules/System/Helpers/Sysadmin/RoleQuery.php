<?php

namespace System\Helpers\Sysadmin;

class RoleQuery extends SysadminQueryBase
{

    static string $sysadmin_get_role =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role
            WHERE role_id = :role_id
        EOD;

    static string $sysadmin_role_view_get_rolacl_by_role =
        <<<'EOD'
            SELECT role_id
                 , r.name                                           AS role_name
                 , ra.name
                 , ra.description
                 , namespace
                 , jsonb_array_a_minus_b(class_allow, class_deny)   AS class_allow
                 , jsonb_array_a_minus_b(action_allow, action_deny) AS action_allow
            FROM (
                   SELECT DISTINCT jsonb_array_elements_text(admin.get_role_with_children(:role_id::INT)) :: INT AS role_id
                 )                     x
              INNER JOIN admin.role    r USING (role_id)
              LEFT JOIN  admin.roleacl ra USING (role_id);
        EOD;

    static string $sysadmin_insert_role =
        <<<'EOD'
            INSERT INTO admin.role
              (role_id, name, description, label, long_desc)
            VALUES (:role_id, :name, :description, :label, :long_desc)
        EOD;

    static string $sysadmin_access_rule_create =
        <<<'EOD'
            INSERT INTO admin.access_rule (access_rule_key, name, description, namespace, class, allow, deny)
            VALUES (:access_rule_key, :name, :description, :namespace, :class, :allow, :deny)
            RETURNING access_rule_id
        EOD;

    static string $sysadmin_access_rule_update =
        <<<'EOD'
            UPDATE admin.access_rule
            SET (access_rule_key, name, description, namespace, class, allow, deny) =
              (:access_rule_key, :name, :description, :namespace, :class, :allow, :deny)
            WHERE access_rule_id = :access_rule_id;
        EOD;

    static string $sysadmin_get_access_rules =
        <<<'EOD'
            SELECT roleacl_id
                 , name
                 , description
                 , created_at
                 , updated_at
                 , role_id
                 , namespace
                 , class_allow
                 , class_deny
                 , action_allow
                 , action_deny
            FROM admin.roleacl;
        EOD;

    static string $sysadmin_get_access_rule =
        <<<'EOD'
            SELECT roleacl_id
                 , name
                 , description
                 , created_at
                 , updated_at
                 , role_id
                 , namespace
                 , class_allow
                 , class_deny
                 , action_allow
                 , action_deny
            FROM admin.roleacl
            WHERE roleacl_id = :access_rule_id
        EOD;

    static string $sysadmin_role_hierarchy_include_add =
        <<<'EOD'
            UPDATE admin.role
            SET roles_include =
              CASE
                WHEN :change_role = 0 THEN ('["*"]') :: JSONB
                ELSE
                  jsonb_array_a_union_b
                    (
                    jsonb_array_a_minus_b(roles_include, ('["*"]') :: JSONB)
                    , to_jsonb('{}'::INT[] || :change_role)
                    )
              END
              , updated_at = now()
            WHERE role_id = :role_id;
        EOD;

    static string $sysadmin_role_hierarchy_include_remove =
        <<<'EOD'
            UPDATE
              admin.role
            SET roles_include = coalesce(
              CASE
                WHEN :change_role = 0 THEN jsonb_array_a_minus_b(roles_include, ('["*"]') :: JSONB)
                ELSE jsonb_array_a_minus_b(roles_include, (:change_role)::TEXT::JSONB)
              END, '[]')
              , updated_at = now()
            WHERE role_id = :role_id
              AND role_id != 0;
        EOD;

    static string $sysadmin_role_hierarchy_exclude_add =
        <<<'EOD'
            UPDATE
              admin.role
            SET roles_exclude = coalesce(
              CASE
                WHEN :change_role = 0 THEN ('["*"]') :: JSONB
                ELSE coalesce(jsonb_array_a_minus_b(roles_exclude, ('["*"]') :: JSONB), '[]') || (:change_role):: TEXT :: JSONB
              END, '[]')
              , updated_at = now()
            WHERE role_id = :role_id
              AND role_id != 0;
        EOD;

    static string $sysadmin_role_hierarchy_exclude_remove =
        <<<'EOD'
            UPDATE
              admin.role
            SET roles_exclude = coalesce(
              CASE
                WHEN :change_role = 0 THEN jsonb_array_a_minus_b(roles_exclude, ('["*"]') :: JSONB)
                ELSE jsonb_array_a_minus_b(roles_exclude, (:change_role)::TEXT::JSONB)
              END, '[]')
              , updated_at = now()
            WHERE role_id = :role_id
              AND role_id != 0;
        EOD;

    static string $sysadmin_role_hierarchy_get_selected_incl =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role r1
            WHERE (to_jsonb(r1.role_id) <@ (
                                             SELECT r2.roles_include
                                             FROM admin.role r2
                                             WHERE r2.role_id = :role_id
                                           ))
            ORDER BY role_id;
        EOD;

    static string $sysadmin_role_hierarchy_get_selected_excl =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role r1
            WHERE (to_jsonb(r1.role_id) <@ (
                                             SELECT r2.roles_exclude
                                             FROM admin.role r2
                                             WHERE r2.role_id = :role_id
                                           ))
            ORDER BY role_id;
        EOD;

    static string $sysadmin_role_hierarchy_get_unselected =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role r1
            WHERE NOT (to_jsonb(r1.role_id) <@ (
                                                 SELECT jsonb_array_a_union_b(r2.roles_include || r2.roles_exclude, to_jsonb('{}'::INT[] || r2.role_id))
                                                 FROM admin.role r2
                                                 WHERE r2.role_id = :role_id
                                               ))
            ORDER BY role_id;
        EOD;

}