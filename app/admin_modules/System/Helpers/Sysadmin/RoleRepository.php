<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use App\Library\Utils\Json;
use Phalcon\Db\Enum;
use \Exception;
use System\Helpers\RepositoryBase;

class RoleRepository extends RepositoryBase
{

    /**
     * Get all roles
     *
     * @return array|bool
     */
    public function getRoles()
    {

        try {
            $sql = RoleQuery::$sysadmin_get_roles;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Get a single role
     *
     * @param $roleId
     *
     * @return array|bool
     */
    public function getRole($roleId)
    {

        try {
            $sql = RoleQuery::$sysadmin_get_role;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "role_id" => $roleId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    public function getRoleaclFromRole($roleId)
    {

        $roleId = (int)$roleId;
        try {
            $sql = RoleQuery::$sysadmin_role_view_get_rolacl_by_role;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "role_id" => $roleId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create a new role
     *
     * @param $data
     *
     * @return bool
     */
    public function createRole($data)
    {

        try {
            $sql = RoleQuery::$sysadmin_insert_role;
            $state = $this->db->execute($sql, [
                "role_id"     => $data["role_id"],
                "name"        => $data["name"],
                "description" => $data["description"],
                "long_desc"   => $data["long_desc"],
                "label"       => $data["label"],
            ]);
            if ($state) {
                return $data["role_id"];
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createAccessRule($data)
    {

        try {
            $sql = RoleQuery::$sysadmin_access_rule_create;

            # actions allow
            $actAllow = $this->request->getPost("actallow");
            $actAllowArr = preg_split("#\s*,\s*#", $actAllow);
            $actAllowJson = Json::jsonEncode($actAllowArr);

            # actions deny
            $actDeny = $this->request->getPost("actallow");
            $actDenyArr = preg_split("#\s*,\s*#", $actDeny);
            $actDenyJson = Json::jsonEncode($actDenyArr);

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "access_rule_key" => $data["key"],
                "name"            => $data["name"],
                "description"     => $data["description"],
                "namespace"       => $data["namespace"],
                "class"           => $data["class"],
                "allow"           => $actAllowJson,
                "deny"            => $actDenyJson,
            ]);
            if (isset($data["access_rule_id"])) {
                return $data["access_rule_id"];
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update an access role
     *
     * @param $roleAceId
     * @param $data
     *
     * @return bool
     */
    public function updateAccessRule($roleAceId, $data)
    {

        $this->flashSession->error("TODO: Not implemented");

        return false;

        try {
            $sql = RoleQuery::$sysadmin_access_rule_update;
            $state = $this->db->execute($sql, [
                "role_id"     => $data["role_id"],
                "name"        => $data["name"],
                "description" => $data["description"],
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get all access roles
     *
     * @return array|bool
     */
    public function getAccessRules()
    {

        try {
            $sql = RoleQuery::$sysadmin_get_access_rules;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Get a single access rule
     *
     * @param $accessRuleId
     *
     * @return array|bool
     */
    public function getAccessRule($accessRuleId)
    {

        try {
            $sql = RoleQuery::$sysadmin_get_access_rule;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "access_rule_id" => $accessRuleId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Change hierarchy
     * add or remove include-roles and exclude-roles
     *
     * @param $roleId
     * @param $changeRoleId
     * @param $type
     *
     * @return bool
     */
    public function changeRoleHierarchy($roleId, $changeRoleId, $type)
    {

        try {
            if ($type === "incladd") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_include_add;
            } elseif ($type === "inclrm") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_include_remove;
            } elseif ($type === "excladd") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_exclude_add;
            } elseif ($type === "exclrm") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_exclude_remove;
            } else {
                return false;
            }

            $state = $this->db->execute($sql, [
                "role_id"     => $roleId,
                "change_role" => $changeRoleId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get selected or unselected roles
     *
     * @param        $roleId
     * @param string $type
     *
     * @return bool
     */
    public function getRolesForHierarchy($roleId, $type)
    {

        try {
            if ($type === "included") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_get_selected_incl;
            } elseif ($type === "excluded") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_get_selected_excl;
            } elseif ($type === "unselected") {
                $sql = RoleQuery::$sysadmin_role_hierarchy_get_unselected;
            }

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "role_id" => $roleId,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}