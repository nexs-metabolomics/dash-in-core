<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use Phalcon\Db\Enum;
use System\Helpers\RepositoryBase;

class SysadmRepository extends RepositoryBase
{

    public function updateUris($inData)
    {

        try {
            $sql = SysadminQueryBase::$sysadmin_update_uri;

            $this->db->begin();
            foreach ($inData as $data) {
                $routeKey = $data["namespace"] . "/" . $data["controller"] . "/" . $data["action"];
                $this->db->execute($sql, [
                    "route_key"  => $routeKey,
                    "namespace"  => $data["namespace"],
                    "controller" => $data["controller"],
                    "action"     => $data["action"],
                    "pattern"    => $data["pattern"],
                    "uri"        => $data["pattern"],
                ]);
            }
            $this->db->commit();

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUris()
    {

        try {
            $sql = SysadminQueryBase::$sysadmin_get_uris;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUriNamespaces()
    {

        try {
//            $sql = $this->system_config->sql->sysadmin_get_uris;
            $sql = SysadminQueryBase::$sysadmin_get_uri_namespaces;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUriControllers()
    {

        try {
            $sql = SysadminQueryBase::$sysadmin_get_uri_controllers;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}