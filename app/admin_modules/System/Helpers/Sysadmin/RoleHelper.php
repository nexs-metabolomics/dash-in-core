<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use Phalcon\Paginator\Adapter\NativeArray as Paginator;
use System\Forms\Sysadmin\AccessRuleForm;
use System\Forms\Sysadmin\RoleForm;
use System\Helpers\HelperBase;

class RoleHelper extends HelperBase
{

    /**
     * @var RoleRepository
     */
    private $_repository;

    /**
     * @return RoleRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new RoleRepository();
        }

        return $this->_repository;
    }

    /**
     * List roles
     *
     * @param $page
     * @param $nrows
     *
     * @return bool|\stdClass
     */
    public function listRoles($page, $nrows)
    {

        $data = $this->_getRepository()->getRoles();
        if (!$data) {
            return false;
        }
        foreach ($data as &$row) {
            $row = (object)$row;
        }
        $paginator = new Paginator([
            "data"  => $data,
            "limit" => $nrows,
            "page"  => $page,
        ]);
        $pagination = $paginator->paginate();

        return $pagination;
    }

    /**
     * Create a new role
     *
     * @return bool|RoleForm
     */
    public function createRole()
    {

        $form = new RoleForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $roleId = $this->_getRepository()->createRole($post);
                if ($roleId) {
                    $this->response->redirect("/sysadmin/role/view/$roleId");
                    $this->response->send();

                    return false;
                }
            }
        }

        return $form;
    }

    /**
     * View a role
     *
     * @param $roleId
     *
     * @return array|bool
     */
    public function viewRole($roleId)
    {

        $data = $this->_getRepository()->getRole($roleId);
        if (!$data) {
            return false;
        }
        $roleacl = $this->_getRepository()->getRoleaclFromRole($roleId);
        foreach ($roleacl as &$row) {
            $row = (object)$row;
        }

        return ["role" => $data, "roleacl" => $roleacl];
    }

    /**
     * Create an access rule
     *
     * @return bool|AccessRuleForm
     */
    public function createAccessRule()
    {

        $form = new AccessRuleForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $ruleId = $this->_getRepository()->createAccessRule($post);
                if ($ruleId !== false) {
                    $this->response->redirect("/sysadmin/accessrule/view/$ruleId");
                    $this->response->send();

                    return false;
                }
            }
        }

        return $form;
    }

    /**
     * List access rules
     *
     * @param $page
     * @param $nrows
     *
     * @return bool|\stdClass
     */
    public function listAccessRules($page, $nrows)
    {

        $data = $this->_getRepository()->getAccessRules();
        if (!$data) {
            return false;
        }
        foreach ($data as &$row) {
            $row = (object)$row;
        }
        $paginator = new Paginator([
            "data"  => $data,
            "limit" => $nrows,
            "page"  => $page,
        ]);
        $pagination = $paginator->paginate();

        return $pagination;
    }

    /**
     * View an access rule
     *
     * @param $accessRuleId
     *
     * @return array|bool
     */
    public function viewAccessRule($accessRuleId)
    {

        $data = $this->_getRepository()->getAccessRule($accessRuleId);
        if (!$data) {
            return false;
        }

        return $data;
    }

    /**
     * Edite role
     *
     * @param $roleId
     *
     * @return array
     */
    public function editRole($roleId)
    {

        $role = $this->_getRepository()->getRole($roleId);
        if (!$role) {
            return ["included" => false, "excluded" => false, "unselected" => false, "role" => false];
        }
        $action = $this->Btn->getAction();
        $addRoleId = $this->Btn->getValue();
        if ($action === "incladd") {
            $this->_getRepository()->changeRoleHierarchy($roleId, $addRoleId, "incladd");
        } elseif ($action === "inclrm") {
            $this->_getRepository()->changeRoleHierarchy($roleId, $addRoleId, "inclrm");
        } elseif ($action === "excladd") {
            $this->_getRepository()->changeRoleHierarchy($roleId, $addRoleId, "excladd");
        } elseif ($action === "exclrm") {
            $this->_getRepository()->changeRoleHierarchy($roleId, $addRoleId, "exclrm");
        }

        # get selected
        $inclSelected = $this->_getRepository()->getRolesForHierarchy($roleId, "included");
        if ($inclSelected) {
            foreach ($inclSelected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }
        $exclSelected = $this->_getRepository()->getRolesForHierarchy($roleId, "excluded");
        if ($exclSelected) {
            foreach ($exclSelected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        # get unselected
        $unselected = $this->_getRepository()->getRolesForHierarchy($roleId, "unselected");
        if ($unselected) {
            foreach ($unselected as &$row) {
                $row = (object)$row;
            }
            unset($row);
        }

        return ["included" => $inclSelected, "excluded" => $exclSelected, "unselected" => $unselected, "role" => $role];
    }

}