<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use System\Helpers\HelperBase;

class SysadmHelper extends HelperBase
{

    /**
     * @var SysadmRepository
     */
    private $_repository;

    /**
     * @return SysadmRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new SysadmRepository();
        }

        return $this->_repository;
    }

    public function updateUris()
    {

        $routes = $this->router->getRoutes();
        $paths = [];
        $i = 0;
        foreach ($routes as $route) {
            $tmp = (array)$route;
            $tmpKeys = array_keys($tmp);
            $pattern = preg_replace("#:.*$#", "", $tmp[$tmpKeys[0]]);
            if ($pattern !== "/updateuris") {
                $paths[$i] = $tmp[$tmpKeys[2]];
                $paths[$i]["pattern"] = $pattern;
                $i++;
            }
        }

        $this->_getRepository()->updateUris($paths);
    }

    public function listUris()
    {

        $data = $this->_getRepository()->getUris();
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
        }

        return ["links" => $data];
    }

}