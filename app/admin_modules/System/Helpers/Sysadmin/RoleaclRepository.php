<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Sysadmin;

use App\Library\Utils\Json;
use App\Library\Utils\JsonException;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use System\Helpers\RepositoryBase;

class RoleaclRepository extends RepositoryBase
{

    /**
     * Create a new role based access rule
     *
     * @param $params
     *
     * @return bool
     */
    public function createRoleacl($params)
    {

        try {
            $sql = RoleaclQuery::$sysadmin_roleacl_create;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"         => $params["name"],
                "description"  => $params["description"],
                "role_id"      => $params["role_id"],
                "namespace"    => $params["namespace"],
                "class_allow"  => $params["class_allow"],
                "class_deny"   => $params["class_deny"],
                "action_allow" => $params["action_allow"],
                "action_deny"  => $params["action_deny"],
            ]);

            if (isset($data["roleacl_id"])) {
                return $data["roleacl_id"];
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $roleaclId
     * @param $params
     *
     * @return bool
     */
    public function updateRoleacl($roleaclId, $params)
    {

        try {
            $sql = RoleaclQuery::$sysadmin_roleacl_update;

            $data = $this->db->execute($sql, [
                "roleacl_id"   => $roleaclId,
                "name"         => $params["name"],
                "description"  => $params["description"],
                "namespace"    => $params["namespace"],
                "class_allow"  => $params["class_allow"],
                "class_deny"   => $params["class_deny"],
                "action_allow" => $params["action_allow"],
                "action_deny"  => $params["action_deny"],
            ]);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get list of roles
     * (role_id,name,description)
     *
     * @return array|bool
     */
    public function getRoles()
    {

        try {
            $sql = RoleaclQuery::$sysadmin_get_roles;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get list of role based access rules
     *
     * @return array|bool
     */
    public function getRoleacls()
    {

        try {
            $sql = RoleaclQuery::$sysadmin_roleacls_get;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get role based access rule for edit
     *
     * @param $roleaclId
     *
     * @return array|bool
     */
    public function getRoleaclForEdit($roleaclId)
    {
        if(!UniqueId::uuidValidate($roleaclId)) {
            return false;
        }

        try {
            $sql = RoleaclQuery::$sysadmin_roleacl_get_for_edit;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "roleacl_id" => $roleaclId
            ]);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get role based access rule for view
     *
     * @param $roleaclId
     *
     * @return array|bool
     */
    public function getRoleacl($roleaclId)
    {

        if (!UniqueId::uuidValidate($roleaclId)) {
            return false;
        }
        try {
            $sql = RoleaclQuery::$sysadmin_roleacl_get_for_edit;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "roleacl_id" => $roleaclId
            ]);

            if ($data) {
                $keyList = ["class_allow", "class_deny", "action_allow", "action_deny"];
                foreach ($keyList as $key) {
                    try {
                        $data[$key] = implode(",", Json::jsonDecode($data[$key]));
                    } catch (JsonException $e) {
                        $this->flashSession->error($e->getMessage());
                    }
                }

                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete a role based access rule
     *
     * @param $roleaclId
     *
     * @return bool
     */
    public function deleteRoleacl($roleaclId)
    {

        $roleaclId = (int)$roleaclId;
        try {
            $sql = RoleaclQuery::$sysadmin_roleacl_delete;

            $state = $this->db->execute($sql, [
                "roleacl_id" => $roleaclId
            ]);

            if (isset($state)) {
                return $state;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

}