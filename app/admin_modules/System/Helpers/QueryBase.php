<?php

namespace System\Helpers;

use App\Library\ApplicationBase\ApplicationQueryBase;

class QueryBase extends ApplicationQueryBase
{

    static string $system_create_login_reset_token =
        <<<'EOD'
            INSERT INTO admin.password_reset_token 
                (token, user_id) VALUES 
                (uuid_generate_v4(),:user_id)
              RETURNING token;        
        EOD;

    static string $system_get_user =
        <<<'EOD'
            SELECT
                user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users
            WHERE user_id = :user_id;
        EOD;

    static string $system_get_organization =
        <<<'EOD'
            SELECT organization_id
                 , name
                 , description
                 , email
            FROM admin.organization
            WHERE organization_id = :organization_id;
        EOD;

    static string $system_get_organizations_search =
        <<<'EOD'
            SELECT organization_id
                 , name
                 , description
            FROM admin.organization
            WHERE lower(concat(name, ' ', description))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'));
        EOD;

    static string $system_get_organizations =
        <<<'EOD'
            SELECT organization_id
                 , name
                 , description
                 , email
            FROM admin.organization;
        EOD;

    static string $system_get_login_user =
        <<<'EOD'
            SELECT u.user_id
                 , u.code
                 , u.first_name
                 , u.last_name
                 , u.email
                 , u.status
                 , u.active_org_id
                 , u.details
                 , o.name                                          AS organization_name
                 , admin.get_manager_roles(active_org_id, user_id) AS manager_roles_json
            FROM admin.users               u
              LEFT JOIN admin.organization o ON o.organization_id = u.active_org_id
            WHERE user_id = :user_id;
        EOD;

    static string $system_action_allow =
        <<<'EOD'
            SELECT count(1) > 0 AS allow
            FROM admin.roleacl
            WHERE (role_id::TEXT::JSONB <@ (:role_id)::JSONB OR role_id::TEXT::JSONB <@ '100')
              AND asterisk_match(:namespace, namespace)
              AND asterisk_match(to_jsonb(:class::TEXT), jsonb_array_a_minus_b(class_allow, class_deny))
              AND asterisk_match(to_jsonb(:action::TEXT), jsonb_array_a_minus_b(action_allow, action_deny));
        EOD;

}