<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers;

use App\Library\ApplicationBase\ApplicationHelperBase;

class HelperBase extends ApplicationHelperBase
{

    public function serviceSelect($returnUri, $key, $serviceName, $menuPartial = null, $submenu = null, $current = null)
    {

        $this->setKeyValue("system_service_select_{$serviceName}", [
            "key"        => $key,
            "return_uri" => $returnUri,
        ]);
        $this->setKeyValue("system_service_selectx_menu", ["submenu" => $submenu, "current" => $current, "menu_partial" => $menuPartial]);
        $querystring = $this->localsession->getQuerystring("?");
        $redirectUri = "/system/service/select/{$serviceName}/{$querystring}";
        $this->response->redirect($redirectUri);
        $this->response->send();

        return false;
    }

    public function serviceSelectUser($returnUri, $key, $return = false, $userId = null)
    {

        if ($return) {

            $this->setKeyValue($key, $userId);
            $this->response->redirect($returnUri);
            $this->response->send();

            return false;

        } else {
            $this->setKeyValue("system_service_select_user", [
                "key"        => $key,
                "return_uri" => $returnUri,
            ]);
            $this->response->redirect("/system/service/selectuser/");
            $this->response->send();

            return false;
        }
    }

    public function serviceSelectOrganization($returnUri, $key, $return = false, $organizationId = null)
    {

        if ($return) {

            $this->setKeyValue($key, $organizationId);
            $this->response->redirect($returnUri);
            $this->response->send();

            return false;

        } else {
            $this->setKeyValue("system_service_select_organization", [
                "key"        => $key,
                "return_uri" => $returnUri,
            ]);
            $this->response->redirect("/system/service/selectorg/");
            $this->response->send();

            return false;
        }
    }

}