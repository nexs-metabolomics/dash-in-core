<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-28
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Login;

use System\Forms\Login\RequestResetForm;
use System\Forms\Login\SetPasswordForm;
use System\Forms\Login\LoginForm;
use System\Helpers\HelperBase;

class LoginHelper extends HelperBase
{

    /**
     * @var LoginRepository
     */
    private $_repository;

    /**
     * @return LoginRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new LoginRepository();
        }

        return $this->_repository;
    }

    /**
     * Check user password, return user or FALSE
     *
     * @return bool|array
     */
    private function _checkLogin()
    {

        $username = $this->request->getPost("username");
        $password = $this->request->getPost("password");

        $userMinimal = $this->_getRepository()->getUserByIdent($username);
        if (!isset($userMinimal["password"])) {
            return false;
        }

        if (!(isset($userMinimal["status"]) && $userMinimal["status"] > 0)) {
            return false;
        }
        $passwordHash = $userMinimal["password"];
        $pwOk = password_verify($password, $passwordHash);
        unset($password, $passwordHash, $userMinimal["password"]);
        if (!$pwOk) {
            return false;
        }

        return $userMinimal;
    }

    public function login()
    {

        $form = new LoginForm();
        if ($this->request->isPost()) {
            $userMinimal = $this->_checkLogin();
            if (!$userMinimal) {
                $this->flashSession->error("Password or username did not match, please try again");
            } else {
                # login ok
                $preloginContext = $this->SU->getPreloginContext();
                # SessionUser SU
                $this->SU->unregister();
                $this->SU->register($userMinimal["user_id"]);
                if (isset($preloginContext["target"])) {
                    $this->response->redirect($preloginContext["target"]);
                    $this->SU->removePreloginContext();
                } else {
                    $querystring = $this->localsession->getQuerystring('?');
                    $this->response->redirect("/dashin/index/$querystring");
                }
                $this->response->send();

                return false;
            }
        }
        $form->get("password")->clear();

        return $form;
    }

    public function requestReset()
    {

        $form = new RequestResetForm();

        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {

                $userIdent = $this->request->getPost("usercode");

                $user = $this->_getRepository()->getUserByIdent($userIdent);
                if (isset($user["user_id"])) {
                    $token = $this->_getRepository()->createResetToken($user["user_id"]);
                    if ($token) {
                        $user = $this->_getRepository()->getUser($user["user_id"]);

                        $email = false;
                        if (isset($user["email"])) {
                            $email = filter_var($user["email"], FILTER_VALIDATE_EMAIL);
                        }
                        if ($email) {
                            $emailTemplate = $this->_getRepository()->getEmailTemplate("email_reset_pw", "en");
                            if ($emailTemplate) {
                                $emailBody = json_decode($emailTemplate["body"]);
                                $emailSubject = json_decode($emailTemplate["subject"]);

                                $h = $this->request->getHttpHost();
                                $s = $this->request->getScheme();
                                $link = "{$s}://{$h}/confirm/$token";

                                $search = [
                                    "[[LINK_URL]]",
                                    "[[LINK_TEXT]]",
                                ];
                                $replace = [
                                    $link,
                                    "reset password",
                                ];
                                $emailBody = str_replace($search, $replace, $emailBody);
                                if ($emailBody) {
                                    $this->mailer->simpleMailer($email, "kdlab user", $emailSubject, $emailBody);
                                }
                            }
                        }
                    }
                }

            }
        }

        return $form;
    }

    public function confirmReset($token)
    {

        $form = new SetPasswordForm();
        if ($this->request->isPost()) {

            # check form is fresh
            if ($form->isRepeatPost()) {
                $this->flashSession->error("Something went wrong");

                return false;
            }

            # compare token/user_id
            $token = $this->session->get("permission_login_confirm_user_account");
            $this->session->remove("permission_login_confirm_user_account");
            $user = $this->_getRepository()->getUserByResetToken($token);
            if (!isset($user["user_id"])) {
                return false;
            }

            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                # set password
                $password = $this->request->getPost("password");
                $passwordHash = password_hash($password, PASSWORD_BCRYPT);
                $ok = $this->_getRepository()->setUserPassword($user["user_id"], $passwordHash);
                if ($ok) {
                    # revoke token
                    $this->_getRepository()->revokeResetToken($token);
                    # redirect
                    $this->response->redirect("/login/");
                    $this->response->send();

                    return false;
                }

            }
        }
        $data = $this->_getRepository()->getUserByResetToken($token);
        if ($data) {
            # TODO: check user status

            # save token to session
            $this->session->set("permission_login_confirm_user_account", $token);
        }

        $form->preventRepeatPost();

        return $form;
    }

    public function confirmUserAccount($token)
    {

        $form = new SetPasswordForm();
        if ($this->request->isPost()) {

            # check form is fresh
            if ($form->isRepeatPost()) {
                $this->flashSession->error("Something went wrong");

                return false;
            }

            # compare token/user_id
            $token = $this->session->get("permission_login_confirm_user_account");
            $this->session->remove("permission_login_confirm_user_account");
            $user = $this->_getRepository()->getUserByResetToken($token);
            if (!isset($user["user_id"])) {
                return false;
            }

            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                # expire token
                $this->_getRepository()->revokeResetToken($token);
                # set password
                $password = $this->request->getPost("password");
                $passwordHash = password_hash($password, PASSWORD_BCRYPT);
                $ok = $this->_getRepository()->setUserPassword($user["user_id"], $passwordHash);
                if ($ok) {
                    # redirect
                    $this->response->redirect("/login/");
                    $this->response->send();

                    return false;
                }

            }
        }

        $data = $this->_getRepository()->getUserByResetToken($token);
        if ($data) {
            # TODO: check user status

            # save token to session
            $this->session->set("permission_login_confirm_user_account", $token);
        }

        $form->preventRepeatPost();

        return ["form" => $form];
    }

}

