<?php

namespace System\Helpers\Login;

use System\Helpers\QueryBase;

class LoginQuery extends QueryBase
{

    static string $system_login_get_email_template =
        <<<'EOD'
            SELECT jsonb_extract_path(details, 'lang', (:lang)::TEXT, 'subject') AS subject
                 , jsonb_extract_path(details, 'lang', (:lang)::TEXT, 'body')    AS body
            FROM admin.template
            WHERE (template_type_id, template_key) = (1, :template_key);
        EOD;

    static string $permission_login_get_user_by_user_id =
        <<<'EOD'
            SELECT user_id
                 , status
                 , password
            FROM admin.users
            WHERE user_id = :user_id
        EOD;

    static string $permission_login_get_user_by_code =
        <<<'EOD'
            SELECT user_id
                 , status
                 , password
            FROM admin.users
            WHERE code_canonical = :code
        EOD;

    static string $permission_login_get_user_by_email =
        <<<'EOD'
            SELECT user_id
                 , status
                 , password
            FROM admin.users
            WHERE email_canonical = :email
        EOD;

    static string $permission_login_revoke_reset_token =
        <<<'EOD'
            UPDATE admin.password_reset_token SET status = 0 WHERE token = :token;
        EOD;

    static string $permission_login_get_user_by_reset_token =
        <<<'EOD'
            SELECT user_id
                 , status
            FROM admin.users
            WHERE user_id IN (
                               SELECT prt.user_id
                               FROM admin.password_reset_token prt
                               WHERE token = :token
                             );
        EOD;

    static string $permission_login_set_user_password =
        <<<'EOD'
            UPDATE admin.users
            SET (password, updated_at) = (:password_hash, now())
            WHERE user_id = :user_id;
        EOD;

}