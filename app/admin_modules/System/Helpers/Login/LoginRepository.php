<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-28
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Login;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use System\Helpers\RepositoryBase;

class LoginRepository extends RepositoryBase
{

    public function debugLog($userId)
    {

        try {
            $sql = "INSERT INTO admin.log (user_id,route,uri,params) VALUES (:user_id,:route,:uri,:params)";

            if (!null === $userId && !UniqueId::uuidValidate($userId)) {
                $userId = null;
            }
            $route = $this->router->getControllerName() . "/" . $this->router->getControllerName() . "::" . $this->router->getActionName();
            $uri = $this->request->getURI();

            $post = $this->request->getPost();
            $jsonPost = Json::jsonEncode($post);

            $this->db->execute($sql, [
                "user_id" => $userId,
                "route"   => $route,
                "uri"     => $uri,
                "params"  => $jsonPost,
            ]);

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
    }

    public function getEmailTemplate($templateKey, $lang)
    {

        try {
            $sql = LoginQuery::$system_login_get_email_template;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "template_key" => $templateKey,
                "lang"         => $lang,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUser($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = LoginQuery::$system_get_login_user;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get user by user_id,user-code,emal
     * Returns user_id,status,password-hash
     *
     * @param $userIdent
     *
     * @return bool
     */
    public function getUserByIdent($userIdent)
    {

        if (!$userIdent) {
            return false;
        }

        try {
            # user_id
            if (UniqueId::uuidValidate($userIdent)) {
                $sql = LoginQuery::$permission_login_get_user_by_user_id;
                $user = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                    "user_id" => $userIdent,
                ]);
                if ($user) {
                    return $user;
                }
            }

            # code
            if (!preg_match('#[^A-Za-z0-9_-]#', $userIdent)) {
                $sql = LoginQuery::$permission_login_get_user_by_code;
                $codeCanonical = mb_strtolower(str_replace("-", "", trim($userIdent)));
                $user = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                    "code" => $codeCanonical,
                ]);
                if ($user) {
                    return $user;
                }
            }

            # email
            $emailCanonical = filter_var(mb_strtolower(trim($userIdent)), FILTER_VALIDATE_EMAIL);
            if ($emailCanonical) {
                $sql = LoginQuery::$permission_login_get_user_by_email;
                $userAll = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "email" => $emailCanonical,
                ]);

                if (count($userAll) == 1) {
                    return $userAll[0];
                } elseif (count($userAll) > 1) {
                    # if more than one account with the same email, use another identification
                    $this->flashSession->error("Please use your user code to login.");
                }
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
    }

    public function createResetToken($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = LoginQuery::$system_create_login_reset_token;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);

            if (isset($data["token"])) {
                return $data["token"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function revokeResetToken($token)
    {

        if (!UniqueId::uuidValidate($token)) {
            return false;
        }
        try {
            $sql = LoginQuery::$permission_login_revoke_reset_token;

            $state = $this->db->execute($sql, [
                "token" => $token,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUserByResetToken($token)
    {

        if (!UniqueId::uuidValidate($token)) {
            return false;
        }

        try {
            $sql = LoginQuery::$permission_login_get_user_by_reset_token;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "token" => $token,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function setUserPassword($userId, $passwordHash)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = LoginQuery::$permission_login_set_user_password;

            $state = $this->db->execute($sql, [
                "user_id"       => $userId,
                "password_hash" => $passwordHash,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

}