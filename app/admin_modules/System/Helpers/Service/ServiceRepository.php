<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-02-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Service;

use System\Helpers\RepositoryBase;

/**
 * Class ServiceRepository
 *
 * @package System\Helpers\Service
 */
class ServiceRepository extends RepositoryBase
{

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getPaginatedUser($page, $nrows, $searchTerm)
    {

        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = ServiceQuery::$admin_get_users_paginated_search_count;
            $itemQuery = ServiceQuery::$admin_get_users_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = ServiceQuery::$admin_get_users_paginated_count;
            $itemQuery = ServiceQuery::$admin_get_users_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

    /**
     * @param $page
     * @param $nrows
     * @param $searchTerm
     *
     * @return false|object
     */
    public function getPaginatedOrganization($page, $nrows, $searchTerm)
    {

        $queryParams = [];
        if (strlen($searchTerm) > 0) {
            $countQuery = ServiceQuery::$admin_get_organizations_paginated_search_count;
            $itemQuery = ServiceQuery::$admin_get_organizations_paginated_search;
            $queryParams["search_term"] = $searchTerm;
        } else {
            $countQuery = ServiceQuery::$admin_get_organizations_paginated_count;
            $itemQuery = ServiceQuery::$admin_get_organizations_paginated;
        }
        $outParams = $this->_preparePagination2(
            $this->db,
            $countQuery,
            $queryParams,
            $itemQuery,
            $queryParams,
            $page,
            $nrows
        );

        return $outParams;
    }

}