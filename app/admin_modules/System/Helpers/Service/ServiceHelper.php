<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-02-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Service;

use System\Helpers\HelperBase;

/**
 * Class ServiceHelper
 *
 * @package System\Helpers\Service
 */
class ServiceHelper extends HelperBase
{

    /**
     * @var ServiceRepository
     */
    private $_repository;

    /**
     * @return ServiceRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new ServiceRepository();
        }

        return $this->_repository;
    }

    private function _serviceSelectReturn($returnUri, $key, $itemId)
    {

        $returnUri = rtrim($returnUri, "/");
        $this->localsession->setKeyValue($key, $itemId);
        $querystring = $this->localsession->getQuerystring("?");
        $returnUri2 = "$returnUri/$querystring";
        $this->response->redirect($returnUri2);
        $this->response->send();
        $this->removeKeyValue("system_service_selectx_menu");

        return false;
    }

    public function select($page, $nrows, $reset, $params, $serviceName)
    {

        $action = $this->Btn->getAction();
        if ($action === "select") {
            $this->removeKeyValue("system_service_select_{$serviceName}");
            $itemId = $this->Btn->getValue();
            $returnUri = $params["return_uri"];
            $key = $params["key"];
            $this->_serviceSelectReturn($returnUri, $key, $itemId);
            if ($this->response->isSent()) {
                return false;
            }
        } elseif ($action === "cancel") {
            $this->removeKeyValue("system_service_select_{$serviceName}");
            $returnUri = $params["return_uri"];
            $querystring = $this->localsession->getQuerystring("?");
            $this->response->redirect("$returnUri/$querystring");
            $this->response->send();

            return false;
        }

        $data = [];
        $searchTerm = $this->getSearchTerm("system_service_select_{$serviceName}_search_term", $reset);
        $methodName = "getPaginated" . ucfirst($serviceName);
        if (method_exists('System\Helpers\Service\ServiceRepository', $methodName)) {
            $data = $this->_getRepository()->{$methodName}($page, $nrows, $searchTerm);
            $this->view->pick("admin_modules/system/service/select_{$serviceName}");
        }

        return [
            "data"        => $data,
            "search_term" => $searchTerm,
        ];
    }

    /**
     * @param $page
     * @param $nrows
     * @param $reset
     * @param $params
     *
     * @return array|false
     */
    public function selectOrganization($page, $nrows, $reset, $params)
    {

        $action = $this->Btn->getAction();
        if ($action === "select") {

            $this->removeKeyValue("system_service_select_organization");
            $userId = $this->Btn->getValue();
            $returnUri = $params["return_uri"];
            $key = $params["key"];
            $this->serviceSelectOrganization($returnUri, $key, true, $userId);
        } elseif ($action === "cancel") {
            $this->removeKeyValue("system_service_select_organization");
            $returnUri = $params["return_uri"];
            $this->response->redirect($returnUri);
            $this->response->send();

            return false;
        }

        $searchTerm = $this->getSearchTerm("system_service_select_organization_search_term", $reset);
        $organizations = $this->_getRepository()->getOrganizationsPaginated($page, $nrows, $searchTerm);

        return ["organizations" => $organizations];
    }

}