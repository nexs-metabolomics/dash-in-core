<?php

namespace System\Helpers\Service;

use System\Helpers\QueryBase;

class ServiceQuery extends QueryBase
{

    static string $admin_get_users_paginated_search_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM admin.users
            WHERE lower(concat(first_name, ' ', last_name, ' ', email))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'))
        EOD;

    static string $admin_get_users_paginated_search =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users
            WHERE lower(concat(first_name, ' ', last_name, ' ', email))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'))
            OFFSET :offset LIMIT :limit
        EOD;

    static string $admin_get_users_paginated_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM admin.users
        EOD;

    static string $admin_get_users_paginated =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users
            OFFSET :offset LIMIT :limit
        EOD;

    static string $admin_get_organizations_paginated_search_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM admin.organization
            WHERE lower(concat(name, ' ', description))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'));
        EOD;

    static string $admin_get_organizations_paginated_search =
        <<<'EOD'
            SELECT organization_id
                 , name
                 , description
            FROM admin.organization
            WHERE lower(concat(name, ' ', description))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'))
            OFFSET :offset LIMIT :limit;
        EOD;

    static string $admin_get_organizations_paginated_count =
        <<<'EOD'
            SELECT count(*) AS n
            FROM admin.organization
        EOD;

    static string $admin_get_organizations_paginated =
        <<<'EOD'
            SELECT organization_id
                 , name
                 , description
                 , email
            FROM admin.organization
            OFFSET :offset LIMIT :limit;
        EOD;

}