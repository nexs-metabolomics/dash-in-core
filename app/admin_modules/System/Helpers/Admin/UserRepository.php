<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;

class UserRepository extends AdminRepositoryBase
{

    /**
     * Get managers by user
     *
     * @param $userId
     *
     * @return array|bool
     */
    public function getManagersByUserId($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = UserQuery::$admin_user_get_managers_by_user_id;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create reset token
     *
     * @param $userId
     *
     * @return bool
     */
    public function createResetToken($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = UserQuery::$system_create_login_reset_token;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);

            if (isset($data["token"])) {
                return $data["token"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create new user
     *
     * @param $params
     *
     * @return bool
     */
    public function createUser($params)
    {

        try {
            $sql = UserQuery::$admin_user_create;
            $userCode = UniqueId::usercode2("USR", "-", 2, 4, 4);
            $userCodeCanonical = mb_strtolower(str_replace("-", "", $userCode));
            $emailCanonical = mb_strtolower(trim($params["email"]));

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "first_name"      => $params["first_name"],
                "last_name"       => $params["last_name"],
                "email"           => $params["email"],
                "email_canonical" => $emailCanonical,
                "code"            => $userCode,
                "code_canonical"  => $userCodeCanonical,
            ]);
            if (isset($data["user_id"])) {
                return $data["user_id"];
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    public function createUserWithOrganization($params)
    {

        try {
            $sql = UserQuery::$admin_user_create_with_organization;
            $userCode = UniqueId::usercode2("USR", "-", 2, 4, 4);
            $userCodeCanonical = mb_strtolower(str_replace("-", "", $userCode));
            $emailCanonical = mb_strtolower(trim($params["user_email"]));

            $orgRoles = Json::jsonEncode([4000]);

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "first_name"      => $params["first_name"],
                "last_name"       => $params["last_name"],
                "email"           => $params["email"],
                "email_canonical" => $emailCanonical,
                "code"            => $userCode,
                "code_canonical"  => $userCodeCanonical,
                "roles"           => $orgRoles,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update user
     *
     * @param $userId
     * @param $params
     *
     * @return bool
     */
    public function updateUser($userId, $params)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = UserQuery::$admin_user_update;
            $emailCanonical = mb_strtolower(trim($params["email"]));

            $state = $this->db->execute($sql, [
                "user_id"         => $userId,
                "first_name"      => $params["first_name"],
                "last_name"       => $params["last_name"],
                "email"           => $params["email"],
                "email_canonical" => $emailCanonical,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get user
     *
     * @param $userId
     *
     * @return array|bool
     */
    public function getUser($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = UserQuery::$system_get_user;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    public function getResetToken($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = UserQuery::$admin_user_get_reset_token;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get list of users
     *
     * @return array|bool
     */
    public function getUsers()
    {

        try {
            $sql = UserQuery::$admin_user_list;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

}