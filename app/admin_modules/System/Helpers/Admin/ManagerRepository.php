<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;

class ManagerRepository extends AdminRepositoryBase
{

    /**
     * Get all roles for an organization
     * as a list of id,name,description
     * suited for table lists and Select
     *
     * @param $organizationId
     *
     * @return array|bool
     */
    public function getOrganizationRoles($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = ManagerQuery::$admin_manager_get_organization_roles;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $organizationId
     * @param $userId
     *
     * @return array|bool
     */
    public function getManager($organizationId, $userId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = ManagerQuery::$admin_manager_get_manager;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
            ]);

            if ($data) {
                $data["organization_id"] = $organizationId;
                $data["user_id"] = $userId;

                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get all roles for a manager
     * as a list of id,name,description
     * suited for table lists and Select
     *
     * @param $organizationId
     * @param $userId
     *
     * @return array|bool
     */
    public function getManagerRoles($organizationId, $userId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = ManagerQuery::$admin_manager_get_manager_roles_table;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getOrganization($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        try {
            $sql = ManagerQuery::$system_get_organization;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get organizations
     *
     * @param string $searchTerm
     *
     * @return array|bool
     */
    public function getOrganizations($searchTerm = null)
    {

        try {
            if (strlen($searchTerm)) {
                $sql = ManagerQuery::$system_get_organizations_search;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "search_term" => $searchTerm,
                ]);
            } else {
                $sql = ManagerQuery::$system_get_organizations;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            }

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    public function getUser($userId)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = ManagerQuery::$system_get_user;

            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "user_id" => $userId,
            ]);
            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function getUsers($searchTerm)
    {

        try {
            if (strlen($searchTerm)) {
                $sql = ManagerQuery::$admin_manager_get_users_search;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "search_term" => $searchTerm,
                ]);
            } else {
                $sql = ManagerQuery::$admin_manager_get_users;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            }

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get list of managers
     *
     * @return array|bool
     */
    public function getManagers($searchTerm)
    {

        try {
            if (strlen($searchTerm)) {
                $sql = ManagerQuery::$admin_manager_list_managers_search;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "search_term" => $searchTerm,
                ]);
            } else {
                $sql = ManagerQuery::$admin_manager_list_managers;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            }

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $organizationId
     * @param $userId
     * @param $params
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createManager($organizationId, $userId, $params)
    {

        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        if (isset($params["roles"])) {
            if (!is_array($params["roles"])) {
                $params["roles"] = (array)$params["roles"];
            }
            $roles = array_values($params["roles"]);
            $roles = array_map("intval", $roles);
        } else {
            $roles = [];
        }

        $rolesJson = Json::jsonEncode($roles);

        try {
            $sql = ManagerQuery::$admin_manager_create_manager;

            $state = $this->db->execute($sql, [
                "user_id"         => $userId,
                "organization_id" => $organizationId,
                "roles"           => $rolesJson,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update manager
     *
     * @param $organizationId
     * @param $userId
     * @param $params
     *
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function updateManager($organizationId, $userId, $params)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }

        try {
            $sql = ManagerQuery::$admin_manager_update_manager;

            if (isset($params["roles"])) {
                if (!is_array($params["roles"])) {
                    $params["roles"] = (array)$params["roles"];
                }
                $roles = array_values($params["roles"]);
                $roles = array_map("intval", $roles);
            } else {
                $roles = [];
            }

            $rolesJson = Json::jsonEncode($roles);

            $state = $this->db->execute($sql, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
                "label"           => $params["label"],
                "roles"           => $rolesJson,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

}