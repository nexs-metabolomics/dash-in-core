<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use App\Library\Utils\Json;
use App\Library\Utils\UniqueId;
use Phalcon\Paginator\Adapter\NativeArray as Paginator;
use System\Forms\Admin\UserForm;
use System\Forms\Admin\UserWithOrganizationForm;

class UserHelper extends AdminHelperBase
{

    /**
     * @var UserRepository
     */
    private $_repository;

    /**
     * @return UserRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new UserRepository();
        }

        return $this->_repository;
    }

    /**
     * Create new user
     *
     * @return bool|UserForm
     */
    public function createUser()
    {

        $form = new UserForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $userId = $this->_getRepository()->createUser($post);
                if ($userId) {
                    $this->localsession->setKeyValue("system_user_id", $userId);
                    $querystring = $this->localsession->getQuerystring("?");
                    $this->response->redirect("admin/user/view/$querystring");
                    $this->response->send();

                    return false;
                }
            }
        }

        return [
            "form" => $form
        ];
    }

    public function createUserWithOrganization()
    {

        $form = new UserWithOrganizationForm();
        if ($this->request->isPost() && $this->request->getPost("confirm")) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $result = $this->_getRepository()->createUserWithOrganization($post);
                if (isset($result["user_id"]) && $result["user_id"]) {
                    $this->localsession->setKeyValue("system_user_id", $result["user_id"]);
                    $this->localsession->setKeyValue("system_organization_id", $result["organization_id"]);
                    $querystring = $this->localsession->getQuerystring("?");
                    $this->response->redirect("admin/user/view/$querystring");
                    $this->response->send();

                    return false;
                }
            }
        }

        return $form;
    }

    /**
     * Edit user
     *
     * @param $userId
     *
     * @return bool|UserForm
     */
    public function editUser($userId)
    {

        $action = $this->Btn->getAction();
        $form = new UserForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $this->_getRepository()->updateUser($userId, $post);
                if ($userId && $action === "submit") {
                    $this->response->redirect("admin/user/view/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $userInfo = $this->_getRepository()->getUser($userId);
        if ($userInfo) {
            $form->bindValues($userInfo);
        }

        return [
            "form"      => $form,
            "user_info" => $userInfo
        ];
    }

    /**
     * View user
     *
     * @param $userId
     *
     * @return array|bool
     */
    public function viewUser($userId)
    {

        $user = $this->_getRepository()->getUser($userId);
        if (!$user) {
            return false;
        }

        $action = $this->Btn->getAction();
        if ($action === "viewmanager") {
            $organizationId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($organizationId)) {
                $this->localsession->setKeyValue("system_organization_id", $organizationId);
                $this->localsession->setKeyValue("system_user_id", $userId);

                $this->response->redirect("/admin/manager/view/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        } elseif ($action === "editmanager") {
            $organizationId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($organizationId)) {
                $this->localsession->setKeyValue("system_organization_id", $organizationId);
                $this->localsession->setKeyValue("system_user_id", $userId);

                $this->response->redirect("/admin/manager/edit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();

                return false;
            }
        } elseif ($action === "pwlink") {
            $this->_getRepository()->createResetToken($userId);
        }

        $resetTokenResult = $this->_getRepository()->getResetToken($userId);

        if ($resetTokenResult) {
            $resetToken = $resetTokenResult["token"];
            $expireAt = $resetTokenResult["expires_in"];
            $s = $this->request->getScheme();
            $h = $this->request->getHttpHost();
            $resetLink = "$s://$h/confirm/$resetToken";
        } else {
            $resetLink = false;
            $expireAt = false;
        }

        # debug/test mail
        $sendMail = false;
        if ($action === "sendpw" && $resetToken) {
            $subject = "User account at Dash-In - Create or reset password";
            $content =
                "<h1>An account at Dash-In is ready for you</h1>\n" .
                "<h3>Create or reset password</h3>\n" .
                "<p>You should have received your user code in a separete email. Use that code together with your new password to login to your KdLab account</p>\n" .
                "<p>Use the below link to create or reset your password</p>\n" .
                "<p><a href=\"$resetLink\">$resetLink</a></p>";
            $email = $user["email"];
            $name = $user["first_name"] . " " . $user["last_name"];
            $sendMail = true;
        } elseif ($action === "sendcode") {
            $subject = "User account at Dash-In - User code";
            $content =
                "<h1>An account at Dash-In is ready for you</h1>\n" .
                "<p>Ths is your user code:" . $user["code"] . "</p>\n" .
                "<p>You will receive a \"Create or reset password\" link in a separate mail.</p>\n" .
                "<p>Follow the link and create your new password.</p>\n" .
                "<p>After you have created your password use this user code to login.</p>\n" .
                "";
            $email = $user["email"];
            $name = $user["first_name"] . " " . $user["last_name"];
            $sendMail = true;
        }
        if ($sendMail) {
            try {
                $mailResult = $this->mailer->simpleMailer($email, $name, $subject, $content);
            } catch (\Exception $e) {
                $this->flashSession->error($e->getMessage());
            }
        }

        $managers = $this->_getRepository()->getManagersByUserId($userId);
        if ($managers) {
            foreach ($managers as &$row) {
                $tmp = Json::jsonDecode($row["m_roles"]);
                $row["m_roles"] = "<div>" . implode("</div><div>", $tmp) . "</div>";
                $row = (object)$row;
            }
        }

        return [
            "user"       => $user,
            "managers"   => $managers,
            "reset_link" => $resetLink,
            "expires_in" => $expireAt,
        ];
    }

    /**
     * List users
     *
     * @param $page
     * @param $nrows
     *
     * @return bool|\stdClass
     */
    public function listUsers($page, $nrows)
    {

        $data = $this->_getRepository()->getUsers();
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
            $paginator = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $paginator->paginate();
        } else {
            $paginate = false;
        }

        return $paginate;
    }

}