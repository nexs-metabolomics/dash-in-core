<?php

namespace System\Helpers\Admin;

use System\Helpers\QueryBase;

class ManagerQuery extends QueryBase
{

    /**
     * Get roles of the manager's organization
     *
     * @var string $admin_manager_get_organization_roles
     */
    static string $admin_manager_get_organization_roles =
        <<<'EOD'
            SELECT
                  o.role_id
                , r.name
                , r.description
              FROM (
                SELECT
                  jsonb_array_elements_text(admin.get_roles(roles)) :: INT AS role_id
                FROM admin.organization o
                WHERE organization_id = :organization_id
              ) o
              LEFT JOIN admin.role r USING (role_id);
            EOD;

    /**
     * Get a single manager
     *
     * @var string $admin_manager_get_manager
     */
    static string $admin_manager_get_manager =
        <<<'EOD'
            SELECT
                  m.label                                                                      AS m_label
                , m.status                                                                     AS m_status
                , jsonb_array_a_intersect_b(m.roles,admin.get_roles(o.roles))                  AS m_roles
                , jsonb_array_a_intersect_b(admin.get_roles(m.roles),admin.get_roles(o.roles)) AS mo_roles
                , o.name                                                                       AS o_name
                , o.description                                                                AS o_description
                , o.status                                                                     AS o_status
                , admin.get_roles(o.roles)                                                     AS o_roles
                , u.first_name
                , u.last_name
                , u.email
                , u.status                                                                     AS u_status
              FROM admin.manager m
              LEFT JOIN admin.users u USING (user_id)
              LEFT JOIN admin.organization o USING (organization_id)
              WHERE organization_id = :organization_id
              AND user_id = :user_id;
        EOD;

    /**
     * Get a list of the manager's roles
     *
     * @var string $admin_manager_get_manager_roles_table
     */
    static string $admin_manager_get_manager_roles_table =
        <<<'EOD'
            SELECT
                m.role_id
                , name
                , description
              FROM (
                     SELECT
                       jsonb_array_elements_text(admin.get_manager_roles(:organization_id, :user_id)) :: INT AS role_id
                   ) m
              LEFT JOIN admin.role r USING (role_id)
              ORDER BY role_id;

        EOD;

    /**
     * Create a list of managers
     *
     * For each manager create a list of roles
     * TODO: Figure out how to handle inherited roles vs explicit roles
     *       Option 1) If explicit role is removed all inherited roles are lost
     *       Option 2) Inherited roles are automatically added as explicit, when parent role is added
     *                 If explicit role is removed, inherited roles are kept
     *
     * @var string $admin_manager_list_managers
     */
    static string $admin_manager_list_managers =
        <<<'EOD'
                WITH
                  base_table      AS (
                                       SELECT b.first_name
                                            , b.last_name
                                            , b.email
                                            , b.status      AS u_status
                                            , a.user_id
                                            , a.organization_id
                                            , a.status      AS m_status
                                            , c.name        AS o_name
                                            , c.description AS o_description
                                            , c.status      AS o_status
                                       FROM admin.manager             a
                                         LEFT JOIN admin.users        b ON a.user_id = b.user_id
                                         LEFT JOIN admin.organization c ON a.organization_id = c.organization_id
                                     )
                  , role_table    AS (
                                       SELECT a.organization_id
                                            , a.user_id
                                            , jsonb_agg(a.role_id ORDER BY a.role_id)     AS arr_role_ids
                                            , jsonb_agg(b.name ORDER BY a.role_id)        AS arr_role_names
                                            , jsonb_agg(b.description ORDER BY a.role_id) AS arr_role_descriptions
                                       FROM (
                                              SELECT a0.organization_id
                                                   , a0.user_id
                                                   , a2.role_id::INT AS role_id
                                              FROM base_table                                              a0
                                                 , admin.get_manager_roles(a0.organization_id, a0.user_id) a1(role_ids)
                                                 , jsonb_array_elements_text(a1.role_ids)                  a2(role_id)
                                            )                  a
                                         INNER JOIN admin.role b ON a.role_id = b.role_id
                                       GROUP BY a.organization_id
                                              , a.user_id

                                     )
                  , combine_table AS (
                                       SELECT a.first_name
                                            , a.last_name
                                            , a.email
                                            , a.u_status
                                            , a.user_id
                                            , a.organization_id
                                            , a.m_status
                                            , a.o_name
                                            , a.o_description
                                            , a.o_status
                                            , b.arr_role_ids
                                            , b.arr_role_names
                                            , b.arr_role_descriptions
                                       FROM base_table        a
                                         LEFT JOIN role_table b ON (a.organization_id, a.user_id) = (b.organization_id, b.user_id)
                                     )
                SELECT first_name
                     , last_name
                     , email
                     , u_status
                     , user_id
                     , organization_id
                     , m_status
                     , o_name
                     , o_description
                     , o_status
                     , arr_role_ids
                     , arr_role_names
                     , arr_role_descriptions
                FROM combine_table;
        EOD;

    /**
     * List managers with an option to filter by search
     *
     * @var string $admin_manager_list_managers_search
     */
    static string $admin_manager_list_managers_search =
        <<<'EOD'
            WITH
              base_table      AS (
                                   SELECT b.first_name
                                        , b.last_name
                                        , b.email
                                        , b.status      AS u_status
                                        , a.user_id
                                        , a.organization_id
                                        , a.status      AS m_status
                                        , c.name        AS o_name
                                        , c.description AS o_description
                                        , c.status      AS o_status
                                   FROM admin.manager             a
                                     LEFT JOIN admin.users        b ON a.user_id = b.user_id
                                     LEFT JOIN admin.organization c ON a.organization_id = c.organization_id
                                   WHERE
                                           LOWER(CONCAT(b.first_name,' ',b.last_name,' ',b.email,' ',c.name))
                                           LIKE LOWER(CONCAT('%',(:search_term)::TEXT,'%'))
                                 )
              , role_table    AS (
                                   SELECT a.organization_id
                                        , a.user_id
                                        , jsonb_agg(a.role_id ORDER BY a.role_id)     AS arr_role_ids
                                        , jsonb_agg(b.name ORDER BY a.role_id)        AS arr_role_names
                                        , jsonb_agg(b.description ORDER BY a.role_id) AS arr_role_descriptions
                                   FROM (
                                          SELECT a0.organization_id
                                               , a0.user_id
                                               , a2.role_id::INT AS role_id
                                          FROM base_table                                              a0
                                             , admin.get_manager_roles(a0.organization_id, a0.user_id) a1(role_ids)
                                             , jsonb_array_elements_text(a1.role_ids)                  a2(role_id)
                                        )                  a
                                     INNER JOIN admin.role b ON a.role_id = b.role_id
                                   GROUP BY a.organization_id
                                          , a.user_id
                                 
                                 )
              , combine_table AS (
                                   SELECT a.first_name
                                        , a.last_name
                                        , a.email
                                        , a.u_status
                                        , a.user_id
                                        , a.organization_id
                                        , a.m_status
                                        , a.o_name
                                        , a.o_description
                                        , a.o_status
                                        , b.arr_role_ids
                                        , b.arr_role_names
                                        , b.arr_role_descriptions
                                   FROM base_table        a
                                     LEFT JOIN role_table b ON (a.organization_id, a.user_id) = (b.organization_id, b.user_id)
                                 )
            SELECT first_name
                 , last_name
                 , email
                 , u_status
                 , user_id
                 , organization_id
                 , m_status
                 , o_name
                 , o_description
                 , o_status
                 , arr_role_ids
                 , arr_role_names
                 , arr_role_descriptions
            FROM combine_table;
        EOD;

    static string $admin_manager_create_manager =
        <<<'EOD'
            INSERT INTO admin.manager
              (user_id, organization_id, roles)
              VALUES (:user_id, :organization_id, :roles)
              ON CONFLICT (user_id,organization_id) DO UPDATE SET (roles,updated_at) = (:roles,now());
        EOD;

    static string $admin_manager_get_users_search =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users
            WHERE lower(concat(first_name, ' ', last_name, ' ', email))
              LIKE lower(concat('%', (:search_term)::TEXT, '%'));
        EOD;

    static string $admin_manager_get_users =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users;
        EOD;

    static string $admin_manager_update_manager =
        <<<'EOD'
            UPDATE admin.manager SET (label,roles,updated_at) = (:label,:roles,now())
            WHERE (organization_id,user_id) = (:organization_id,:user_id)
        EOD;

}