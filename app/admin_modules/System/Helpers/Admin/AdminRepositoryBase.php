<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-06
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;
use System\Helpers\RepositoryBase;

class AdminRepositoryBase extends RepositoryBase
{

    /**
     * Get minmal user-info (user_id,status)
     * User identified by any menthod supported
     *
     * @param $userIdent
     *
     * @return bool
     */
    public function getUserMinimalByIdent($userIdent)
    {

        if (!$userIdent) {
            return false;
        }

        try {
            # user_id
            if (UniqueId::uuidValidate($userIdent)) {
                $sql = AdminQueryBase::$admin_get_user_minimal_by_user_id;
                $user = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                    "user_id" => $userIdent,
                ]);
                if ($user) {
                    return $user;
                }
            }

            # code
            if (!preg_match('#[^A-Za-z0-9-]#', $userIdent)) {
                $sql = AdminQueryBase::$admin_get_user_minimal_by_code;
                $codeCanonical = mb_strtolower(str_replace("-", "", trim($userIdent)));
                $user = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                    "code_canonical" => $codeCanonical,
                ]);
                if ($user) {
                    return $user;
                }
            }

            # email
            $emailCanonical = filter_var(mb_strtolower(trim($userIdent)), FILTER_VALIDATE_EMAIL);
            if ($emailCanonical) {
                $sql = AdminQueryBase::$admin_get_user_minimal_by_email;
                $userAll = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "email_canonical" => $emailCanonical,
                ]);

                if (count($userAll) == 1) {
                    return $userAll[0];
                } elseif (count($userAll) > 1) {
                    # if more than one account with the same email, use another identification
                    $this->flashSession->error("Please use your user-code or user-id.");
                }
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
    }

}