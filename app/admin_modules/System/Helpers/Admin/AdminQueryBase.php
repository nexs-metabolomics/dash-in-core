<?php

namespace System\Helpers\Admin;

use System\Helpers\QueryBase;

class AdminQueryBase extends QueryBase
{

    static string $admin_get_user_minimal_by_user_id =
        <<<'EOD'
            SELECT user_id
                 , status
            FROM admin.users
            WHERE user_id = :user_id
        EOD;

    static string $admin_get_user_minimal_by_code =
        <<<'EOD'
            SELECT user_id
                 , status
            FROM admin.users
            WHERE code_canonical = :code_canonical
        EOD;

    static string $admin_get_user_minimal_by_email =
        <<<'EOD'
            SELECT user_id
                 , status
            FROM admin.users
            WHERE email_canonical = :email_canonical
        EOD;




}