<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use Phalcon\Paginator\Adapter\NativeArray as Paginator;
use System\Forms\Admin\AddManagerForm;
use System\Forms\Admin\EditManagerForm;

class ManagerHelper extends AdminHelperBase
{

    /**
     * @var ManagerRepository
     */
    private $_repository;

    /**
     * @return ManagerRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new ManagerRepository();
        }

        return $this->_repository;
    }

    public function getOrganizations($page, $nrows, $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "select") {
            $organizationId = $this->Btn->getValue();
            $this->localsession->setKeyValue("admin_manager_create_organization_id", $organizationId);
            # redirect to create participant
            $this->response->redirect("/admin/manager/create/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $searchTerm = $this->getSearchTerm("admin_manager_select_organization_search_term", $reset);

        $data = $this->_getRepository()->getOrganizations($searchTerm);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
            $pagination = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $pagination->paginate();

            return ["organizations" => $paginate, "search_term" => $searchTerm];
        }
    }

    public function getUsers($page, $nrows, $reset)
    {

        $action = $this->Btn->getAction();
        if ($action === "select") {
            $userId = $this->Btn->getValue();
            $this->localsession->setKeyValue("admin_manager_create_user_id", $userId);
            # redirect to create participant
            $this->response->redirect("/admin/manager/create/{$this->localsession->getQuerystring('?')}");
            $this->response->send();

            return false;
        }

        $organizationId = $this->localsession->getKeyValue("admin_manager_create_organization_id");
        if ($organizationId) {
            $organization = $this->_getRepository()->getOrganization($organizationId);
        } else {
            $organization = false;
        }

        $searchTerm = $this->getSearchTerm("admin_manager_select_user_search_term", $reset);

        $data = $this->_getRepository()->getUsers($searchTerm);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
            $pagination = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $pagination->paginate();
        } else {
            $paginate = false;
        }

        return ["users" => $paginate, "search_term" => $searchTerm, "organization" => $organization];
    }

    /**
     * @param $organizationId
     * @param $userId
     *
     * @return array|bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createManager($organizationId, $userId)
    {

        $roles = $this->_getRepository()->getOrganizationRoles($organizationId);
        if ($roles) {
            $options = [];
            foreach ($roles as $role) {
                $options[$role["role_id"]] = $role["name"];
            }
        }

        if (isset($options) && is_array($options)) {
            $form = new AddManagerForm(null, ["role_options" => $options]);
        } else {
            $form = new AddManagerForm();
        }
        if ($form->has("usercode")) {
            $form->remove("usercode");
        }
        if ($form->has("label")) {
            $form->remove("label");
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                # verify user exists
                $user = $this->_getRepository()->getUserMinimalByIdent($userId);
                if ($user) {

                    # TODO: validate user status $user["status"]

                    # verify user
                    $ok = $this->_getRepository()->createManager($organizationId, $user["user_id"], $post);
                    if ($ok) {
                        $this->localsession->setKeyValue("system_user_id", $userId);
                        $this->localsession->setKeyValue("system_organization_id", $organizationId);
                        $this->response->redirect("admin/manager/view/{$this->localsession->getQuerystring('?')}");
                        $this->response->send();

                        return false;
                    }
                }

            }
        }
        $user = $this->_getRepository()->getUser($userId);
        if ($user) {
            $user["name"] = ($user["first_name"] . $user["last_name"]) ? $user["first_name"] . " " . $user["last_name"] : "No name";
        }
        $organization = $this->_getRepository()->getOrganization($organizationId);

        return ["form" => $form, "user" => $user, "organization" => $organization];
    }

    /**
     * List managers
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return array
     */
    public function listManagers($page, $nrows, $reset)
    {

        $searchTerm = $this->getSearchTerm("prj01_admin_list_managers_search_term", $reset);
        $data = $this->_getRepository()->getManagers($searchTerm);
        if ($data) {
            foreach ($data as &$row) {
                $r_id = json_decode($row["arr_role_ids"]);
                $r_name = json_decode($row["arr_role_names"]);
                $r_desc = json_decode($row["arr_role_descriptions"]);
                $roles = [];
                if(!is_array($r_id)) {
                    $r_id = [$r_id];
                }
                for ($i = 0, $max_i = count($r_id); $i < $max_i; ++$i) {
                    $roles[$i] = (object)[
                        "id"          => $r_id[$i],
                        "name"        => $r_name[$i],
                        "description" => $r_desc[$i],
                    ];
                }
                unset($row["arr_role_ids"]);
                unset($row["arr_role_names"]);
                unset($row["arr_role_descriptions"]);

                $row["roles"] = (object)$roles;
                $row = (object)$row;
            }
            unset($roles);
            unset($row);
            $pagination = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $pagination->paginate();
        } else {
            $paginate = false;
        }

        return ["managers" => $paginate, "search_term" => $searchTerm];
    }

    /**
     * View a manager
     *
     * @param $organizationId
     * @param $userId
     *
     * @return array|bool
     */
    public function viewManager($organizationId, $userId)
    {

        $data = $this->_getRepository()->getManager($organizationId, $userId);
        if (!$data) {
            $data = false;
            $roles = false;
        } else {
            $roles = $this->_getRepository()->getManagerRoles($organizationId, $userId);
            if ($roles) {
                foreach ($roles as &$role) {
                    $role = (object)$role;
                }
            }
        }
        $user = $this->_getRepository()->getUser($userId);
        $organization = $this->_getRepository()->getOrganization($organizationId);

        return [
            "manager"      => $data,
            "roles"        => $roles,
            "user"         => $user,
            "organization" => $organization,
        ];
    }

    public function editManager($organizationId, $userId)
    {

        # get org + roles
        $roles = $this->_getRepository()->getOrganizationRoles($organizationId);
        if ($roles) {
            $options = [];
            foreach ($roles as $role) {
                $options[$role["role_id"]] = $role["name"];
            }
        }
        if (isset($options)) {
            $form = new EditManagerForm(null, ["role_options" => $options]);
        } else {
            $form = new EditManagerForm();
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {

                $this->_getRepository()->updateManager($organizationId, $userId, $post);
                $action = $this->Btn->getAction();
                if ($action === "ok") {
                    $this->response->redirect("/admin/manager/view/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }
        $user = $this->_getRepository()->getUser($userId);
        $organization = $this->_getRepository()->getOrganization($organizationId);
        $manager = $this->_getRepository()->getManager($organizationId, $userId);
        if ($manager) {
            if ($form->has("label")) {
                $form->get("label")->setAttribute("value", $manager["m_label"]);
            }
            $rolesSelected = json_decode($manager["m_roles"], JSON_OBJECT_AS_ARRAY);
            if ($form->has("roles")) {
                $form->get("roles")->setDefault($rolesSelected);
            }
        } else {
            $manager = false;
        }

        return [
            "form"         => $form,
            "manager"      => $manager,
            "user"         => $user,
            "organization" => $organization,
        ];
    }

}