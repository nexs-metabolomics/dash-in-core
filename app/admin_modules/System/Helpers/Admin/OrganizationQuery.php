<?php

namespace System\Helpers\Admin;

use System\Helpers\QueryBase;

class OrganizationQuery extends QueryBase
{

    static string $admin_get_managers_by_organization =
        <<<'EOD'
        SELECT m.label       AS m_label
             , m.status      AS m_status
             , o.name        AS o_name
             , o.description AS o_description
             , o.status      AS o_status
             , u.first_name
             , u.last_name
             , u.email
             , u.status      AS u_status
        FROM admin.manager             m
          LEFT JOIN admin.users        u USING (user_id)
          LEFT JOIN admin.organization o USING (organization_id)
        WHERE organization_id = :organization_id
    EOD;

    static string $admin_organization_create =
        <<<'EOD'
            INSERT INTO admin.organization (organization_id, name, description, email)
            VALUES (uuid_generate_v4(), :name, :description, :email)
            RETURNING organization_id;
        EOD;

    static string $admin_update_organization =
        <<<'EOD'
            UPDATE admin.organization
            SET (name, description, email, updated_at) = (:name, :description, :email, now())
            WHERE organization_id = :organization_id;
        EOD;

    static string $admin_organization_get_selected_roles =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role
            WHERE (role_id :: TEXT :: JSONB <@ (
                                                 SELECT roles
                                                 FROM admin.organization
                                                 WHERE organization_id = :organization_id
                                               ))
            ORDER BY role_id DESC;
        EOD;

    static string $admin_organization_get_unselected_roles =
        <<<'EOD'
            SELECT role_id
                 , name
                 , description
            FROM admin.role
            WHERE NOT (role_id :: TEXT :: JSONB <@ (
                                                     SELECT roles
                                                     FROM admin.organization
                                                     WHERE organization_id = :organization_id
                                                   ))
              AND role_id != 0
            ORDER BY role_id DESC;
        EOD;

    static string $admin_organization_add_role =
        <<<'EOD'
            UPDATE admin.organization
            SET roles = jsonb_array_a_minus_b(roles || (:role_id)::JSONB, '[0]')
              , updated_at = now()
            WHERE organization_id = :organization_id;
        EOD;

    static string $admin_organization_remove_role =
        <<<'EOD'
            UPDATE admin.organization
            SET roles = jsonb_array_a_minus_b(roles, (:role_id) :: JSONB)
              , updated_at = now()
            WHERE organization_id = :organization_id;
        EOD;

    static string $admin_organization_get_roles =
        <<<'EOD'
            SELECT o.role_id
                 , r.name
                 , r.description
            FROM (
                   SELECT jsonb_array_elements_text(admin.get_roles(roles)) :: INT AS role_id
                   FROM admin.organization o
                   WHERE organization_id = :organization_id
                 )                 o
              LEFT JOIN admin.role r USING (role_id);
        EOD;

    static string $admin_organization_get_manager =
        <<<'EOD'
            SELECT m.label                  AS m_label
                 , m.status                 AS m_status
                 , admin.get_roles(m.roles) AS m_roles
                 , o.name                   AS o_name
                 , o.description            AS o_description
                 , o.status                 AS o_status
                 , admin.get_roles(o.roles) AS o_roles
                 , u.first_name
                 , u.last_name
                 , u.email
                 , u.status                 AS u_status
            FROM admin.manager             m
              LEFT JOIN admin.users        u USING (user_id)
              LEFT JOIN admin.organization o USING (organization_id)
            WHERE organization_id = :organization_id
              AND user_id = :user_id
        EOD;

}