<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use Phalcon\Paginator\Adapter\NativeArray as Paginator;
use System\Forms\Admin\OrganizationForm;

class OrganizationHelper extends AdminHelperBase
{

    /**
     * @var OrganizationRepository
     */
    private $_repository;

    /**
     * @return OrganizationRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new OrganizationRepository();
        }

        return $this->_repository;
    }

    public function getManagersByOrganization($organizationId)
    {

        $data = $this->_getRepository()->getManagersByOrganization($organizationId);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
        } else {
            $data = false;
        }

        return $data;
    }

    /**
     * Create a new organization
     *
     * @return bool|OrganizationForm
     */
    public function createOrganization()
    {

        $form = new OrganizationForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!$form->isValid($post)) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $organizationId = $this->_getRepository()->createOrganization($post);
                if ($organizationId) {
                    # redirect to edit
                    $this->localsession->setKeyValue("system_organization_id", $organizationId);
                    $this->response->redirect("admin/organization/edit/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return false;
                }
            }
        }

        return $form;
    }

    public function editOrganization($organizationId)
    {

        $form = new OrganizationForm();
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
            } else {
                $this->_getRepository()->updateOrganization($organizationId);
                $action = $this->Btn->getAction();
                if ($action !== "update") {
                    $this->response->redirect("admin/organization/view/{$this->localsession->getQuerystring('?')}");
                    $this->response->send();

                    return;
                }

            }
        }
        # get organization
        $organization = $this->_getRepository()->getOrganization($organizationId);
        if ($organization) {
            $form->bindValues($organization);
        } else {
            $organizationId = false;
        }

        return ["form" => $form, "organization_id" => $organizationId];
    }

    /**
     * View an organization
     *
     * @param $organizationId
     *
     * @return array
     */
    public function viewOrganization($organizationId)
    {

        $organization = $this->_getRepository()->getOrganization($organizationId);
        $roles = $this->_getRepository()->getSelectedRoles($organizationId);
        if ($roles) {
            foreach ($roles as &$role) {
                $role = (object)$role;
            }
            unset($role);
        }
        $managers = $this->_getRepository()->getManagersByOrganization($organizationId);
        if ($managers) {
            foreach ($managers as &$row) {
                $row = (object)$row;
            }
            unset($row);
        } else {
            $managers = false;
        }

        return ["organization" => $organization, "roles" => $roles, "managers" => $managers];
    }

    /**
     * List organizations
     *
     * @param $page
     * @param $nrows
     * @param $reset
     *
     * @return \stdClass
     */
    public function listOrganizations($page, $nrows, $reset)
    {

        $searchTerm = $this->getSearchTerm("admin_list_organizations_search_term", $reset);

        $data = $this->_getRepository()->getOrganizations($searchTerm);
        if ($data) {
            foreach ($data as &$row) {
                $row = (object)$row;
            }
            unset($row);
            $pagination = new Paginator([
                "data"  => $data,
                "limit" => $nrows,
                "page"  => $page,
            ]);
            $paginate = $pagination->paginate();
        } else {
            $paginate = false;
        }

        return ["organizations" => $paginate, "search_term" => $searchTerm];
    }

    /**
     * Add or remove roles
     *
     * @param $organizationId
     *
     * @return array
     */
    public function editRoles($organizationId)
    {

        # check for add
        $action = $this->Btn->getAction();
        if ($action === "addrole") {
            $roleId = $this->Btn->getValue();
            $this->_getRepository()->addRole($organizationId, $roleId);
        } elseif ($action === "rmrole") {
            $roleId = $this->Btn->getValue();
            $this->_getRepository()->removeRole($organizationId, $roleId);
        }

        # get selected
        $selectedRoles = $this->_getRepository()->getSelectedRoles($organizationId);
        if ($selectedRoles) {
            foreach ($selectedRoles as &$role) {
                $role = (object)$role;
            }
            unset($role);
        } else {
            $selectedRoles = null;
        }

        # get unselected
        $unselectedRoles = $this->_getRepository()->getUnselectedRoles($organizationId);
        if ($unselectedRoles) {
            foreach ($unselectedRoles as &$role) {
                $role = (object)$role;
            }
            unset($role);
        } else {
            $unselectedRoles = null;
        }

        $organization = $this->_getRepository()->getOrganization($organizationId);

        return ["selected" => $selectedRoles, "unselected" => $unselectedRoles, "organization" => $organization];
    }

}