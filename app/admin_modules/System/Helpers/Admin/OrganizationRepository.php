<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Helpers\Admin;

use App\Library\Utils\Json;
use App\Library\Utils\JsonException;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;

class OrganizationRepository extends AdminRepositoryBase
{

    public function getManagersByOrganization($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_get_managers_by_organization;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Create a new organization
     *
     * @param $params
     *
     * @return bool
     */
    public function createOrganization($params)
    {

        try {
            $sql = OrganizationQuery::$admin_organization_create;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "name"        => $params["name"],
                "description" => $params["description"],
                "email"       => $params["email"],
            ]);

            if (isset($data["organization_id"])) {
                return $data["organization_id"];
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    public function updateOrganization($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = OrganizationQuery::$admin_update_organization;

            $state = $this->db->execute($sql, [
                "organization_id" => $organizationId,
                "name"            => $this->request->getPost("name"),
                "description"     => $this->request->getPost("description"),
                "email"           => $this->request->getPost("email"),
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $organizationId
     *
     * @return array|bool
     */
    public function getOrganization($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = OrganizationQuery::$system_get_organization;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Get organizations
     *
     * @param string $searchTerm
     *
     * @return array|bool
     */
    public function getOrganizations($searchTerm = null)
    {

        try {
            if (strlen($searchTerm)) {
                $sql = OrganizationQuery::$system_get_organizations_search;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                    "search_term" => $searchTerm,
                ]);
            } else {
                $sql = OrganizationQuery::$system_get_organizations;
                $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);
            }

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get roles attached organization
     *
     * @param $organizationId
     *
     * @return array|bool
     */
    public function getSelectedRoles($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_organization_get_selected_roles;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Get roles not attached to organization
     *
     * @param $organizationId
     *
     * @return array|bool
     */
    public function getUnselectedRoles($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_organization_get_unselected_roles;
            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if (isset($data)) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Add a role to organization
     *
     * @param $organizationId
     * @param $roleId
     *
     * @return bool
     */
    public function addRole($organizationId, $roleId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        $roleId = (int)$roleId;
        # TODO: don't allow role_id == 0?
        if (!$roleId) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_organization_add_role;
            $state = $this->db->execute($sql, [
                "organization_id" => $organizationId,
                "role_id"         => $roleId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;

    }

    /**
     * Remove a role from organization
     *
     * @param $organizationId
     * @param $roleId
     *
     * @return bool
     */
    public function removeRole($organizationId, $roleId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        $roleId = (int)$roleId;
        # TODO: don't allow role_id == 0?
        if (!$roleId) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_organization_remove_role;
            $state = $this->db->execute($sql, [
                "organization_id" => $organizationId,
                "role_id"         => $roleId,
            ]);

            return $state;
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get roles for organization (id,name,description)
     * suitable for table-view, Select.
     *
     * @param $organizationId
     *
     * @return bool
     */
    public function getOrganizationRoles($organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        try {
            $sql = OrganizationQuery::$admin_organization_get_roles;

            $data = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
            ]);

            if ($data) {
                return $data;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get manager
     *
     * @param $organizationId
     * @param $userId
     *
     * @return array|bool
     */
    public function getManager($organizationId, $userId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = OrganizationQuery::$admin_organization_get_manager;
            $data = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "user_id"         => $userId,
            ]);
            if ($data) {
                try {
                    $data["m_roles"] = (array)Json::jsonDecode($data["m_roles"]);
                    $data["o_roles"] = (array)Json::jsonDecode($data["o_roles"]);
                } catch (JsonException $e) {
                    $this->flashSession->error($e->getMessage());
                }
                $data["organization_id"] = $organizationId;
                $data["user_id"] = $userId;

                return $data;
            }
        } catch (\PDOException $e) {
            $this->flash->error($e->getMessage());
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        return false;
    }

}