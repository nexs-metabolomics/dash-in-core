<?php

namespace System\Helpers\Admin;

use System\Helpers\QueryBase;

class UserQuery extends QueryBase
{

    static string $admin_user_get_managers_by_user_id =
        <<<'EOD'
            WITH roles AS (
                              SELECT
                                  user_id
                                   , json_agg(r.name) role_names
                                   , json_agg(r.role_id) role_ids
                              FROM
                                  (
                                      SELECT
                                          m.user_id
                                           , jsonb_array_elements_text(jsonb_array_a_intersect_b(admin.get_roles(m.roles) , admin.get_roles(o.roles)))::INT AS m_roles
                                      FROM admin.manager m
                                          LEFT JOIN admin.organization o USING (organization_id)
                                      WHERE m.user_id = :user_id
                                  )x0
                                      LEFT JOIN admin.role r ON  role_id = m_roles
                              GROUP BY user_id
                          )
            SELECT
                m.user_id
                 , r.role_names AS m_roles
                 , r.role_ids
                 , m.organization_id
                 , m.status           AS m_status
                 , m.label            AS m_label
                 , o.name             AS o_name
                 , o.description      AS o_description
                 , o.status           AS o_status
            FROM admin.manager m
                LEFT JOIN admin.organization o USING (organization_id)
                LEFT JOIN roles r USING (user_id)
            WHERE m.user_id = :user_id    

    EOD;

    static string $admin_user_create =
        <<<'EOD'
            INSERT INTO admin.users (user_id, code, code_canonical, first_name, last_name, email,email_canonical) 
            VALUES (uuid_generate_v4(),:code,:code_canonical,:first_name,:last_name,:email,:email_canonical)
            RETURNING user_id;
        EOD;

    static string $admin_user_create_with_organization =
        <<<'EOD'
            WITH
              create_user           AS (
                                         INSERT INTO admin.users (user_id, code, code_canonical, first_name, last_name, email, email_canonical)
                                           VALUES (uuid_generate_v4(), :code, :code_canonical, :first_name, :last_name, :email, :email_canonical)
                                           ON CONFLICT (user_id) DO UPDATE SET updated_at = now()
                                           RETURNING user_id, code, code_canonical, first_name, last_name, email
                                       )
              , create_organization AS (
                                         INSERT INTO admin.organization (organization_id, name, description, email, roles)
                                           SELECT uuid_generate_v4()
                                                , concat('Organization for ', first_name, ' ', last_name)
                                                , concat('Auto-generated organization for ', first_name, ' ', last_name)
                                                , email
                                                , :roles
                                           FROM create_user
                                           ON CONFLICT (organization_id) DO UPDATE SET updated_at = now()
                                           RETURNING organization_id,roles,name
                                       )
              , create_manager      AS (
                                         INSERT INTO admin.manager (user_id, organization_id, roles, label)
                                           SELECT u.user_id
                                                , o.organization_id
                                                , roles
                                                , concat('Manager, ', u.first_name, ' (', o.name, ')')
                                           FROM create_user         u
                                              , create_organization o
                                           ON CONFLICT (user_id,organization_id) DO UPDATE SET updated_at = now()
                                           RETURNING 1
                                       )
            SELECT u.user_id
                 , o.organization_id
            FROM create_user         u
               , create_organization o
               , create_manager      m
            
        EOD;

    static string $admin_user_update =
        <<<'EOD'
            UPDATE admin.users
            SET (first_name, last_name, email, email_canonical,updated_at) = (:first_name,:last_name,:email,:email_canonical,now())
            WHERE user_id = :user_id;
        EOD;

    static string $admin_user_get_reset_token =
        <<<'EOD'
            SELECT token
                 , to_char(created_at + INTERVAL '87000 seconds' - now(), 'dd "days" HH24 "hours" MI "minutes" SS "seconds"') AS expires_in
            FROM admin.password_reset_token
            WHERE user_id = :user_id
              AND status = 1
              AND EXTRACT (EPOCH FROM (now() - created_at)) < 87000;
        EOD;

    static string $admin_user_list =
        <<<'EOD'
            SELECT user_id
                 , first_name
                 , last_name
                 , code
                 , email
            FROM admin.users;
        EOD;

}