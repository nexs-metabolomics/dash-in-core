<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Controllers\Admin;

use App\Library\Utils\UniqueId;
use System\Helpers\Admin\OrganizationHelper;

class OrganizationController extends AdminControllerBase
{
    /**
     * @var OrganizationHelper
     */
    private $_helper;

    /**
     * @return OrganizationHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new OrganizationHelper();
        }
        return $this->_helper;
    }

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
    }

    /**
     * Create new organization
     *
     * @return bool
     */
    public function createAction()
    {
        $form = $this->_getHelper()->createOrganization();
        if ($form) {
            $this->view->form = $form;
        } elseif ($this->response->isSent()) {
            return false;
        }
    }

    /**
     * View organization
     *
     * @return bool
     */
    public function viewAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/organization/view/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $action = $this->Btn->getAction();
        if ($action === "addmgr" && UniqueId::uuidValidate($organizationId)) {
            $this->_getHelper()->setKeyValue("admin_manager_create_originator", ["key" => "orgview", "id" => $organizationId]);
            $this->localsession->setKeyValue("admin_manager_create_organization_id", $organizationId);
            $this->response->redirect("/admin/manager/create/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        $results = $this->_getHelper()->viewOrganization($organizationId);
        if (isset($results["organization"]) && $results["organization"]) {
            $this->view->organization = (object)$results["organization"];
        }
        if (isset($results["roles"]) && $results["roles"]) {
            $this->view->roles = (object)$results["roles"];
        }
        if (isset($results["managers"]) && $results["managers"]) {
            $this->view->managers = (object)$results["managers"];
        }
    }

    /**
     * Add or remove roles to/from organization
     *
     * @return bool
     */
    public function editRolesAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/organization/editroles/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $results = $this->_getHelper()->editRoles($organizationId);

        if (isset($results["selected"]) && $results["selected"]) {
            $this->view->selected_roles = $results["selected"];
        }
        if (isset($results["unselected"]) && $results["unselected"]) {
            $this->view->unselected_roles = $results["unselected"];
        }
        if (isset($results["organization"]) && $results["organization"]) {
            $this->view->organization = (object)$results["organization"];
        }
    }

    /**
     * Edit name and description of an organization
     *
     * @return bool
     */
    public function editAction()
    {
        $action = $this->Btn->getAction();
        if ($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/organization/edit/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $results = $this->_getHelper()->editOrganization($organizationId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["organization_id"]) && $results["organization_id"]) {
            $this->view->organization_id = $organizationId;
        }
    }

    /**
     * List organizations
     *
     * @param int $page
     */
    public function listAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $action = $this->Btn->getAction();
        if ($action === "edit") {
            $organizationId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($organizationId)) {
                $this->localsession->setKeyValue("system_organization_id", $organizationId);
                $this->response->redirect("/admin/organization/edit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "view") {
            $organizationId = $this->Btn->getValue();
            if (UniqueId::uuidValidate($organizationId)) {
                $this->localsession->setKeyValue("system_organization_id", $organizationId);
                $this->response->redirect("/admin/organization/view/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }
        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listOrganizations($page, $nrows, $reset);
        if (isset($results["organizations"]) && $results["organizations"]) {
            $this->view->pagination = $results["organizations"];
        }
        if (isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        $organizationId = $this->localsession->getKeyValue("admin_view_organization_organization_id");
        if ($organizationId) {
            $this->view->organization_id = $organizationId;
        }
    }
}