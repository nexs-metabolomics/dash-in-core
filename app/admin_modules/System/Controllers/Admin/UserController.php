<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Admin;


use App\Library\Utils\UniqueId;
use System\Helpers\Admin\UserHelper;

class UserController extends AdminControllerBase
{
    private $_helper;
    
    private function _getHelper()
    {
        if(!$this->_helper) {
            $this->_helper = new UserHelper();
        }
        return $this->_helper;
    }
    
    public function initialize()
    {
        parent::initialize();
    }
    
    public function indexAction()
    {}

    /**
     * Create new user
     */
    public function createAction()
    {
        $results = $this->_getHelper()->createUser();
        if(isset($results) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }

    /**
     * 
     */
    public function createWithOrganizationAction()
    {
        $form = $this->_getHelper()->createUserWithOrganization();
        if($form) {
            $this->view->form = $form;
        }
    }

    /**
     * Edit user
     * 
     * @return bool
     */
    public function editAction()
    {
        $action = $this->Btn->getAction();
        if($action === "select_user") {
            $this->_getHelper()->serviceSelect(
                "/admin/user/edit/",
                "system_user_id",
                "user"
            );
        }
        if($this->response->isSent()) {
            return false;
        }

        $userId = $this->localsession->getKeyValue("system_user_id");
        
        $results = $this->_getHelper()->editUser($userId);
        if(isset($results["form"]) && $results["form"]) {
            $results["form"]->setAction("admin/user/edit/");
            $this->view->form = $results["form"];
        }
        if(isset($results["user_info"]) && $results["user_info"]) {
            $results["form"]->setAction("admin/user/edit/");
            $this->view->user_info = (object)$results["user_info"];
        }
    }

    /**
     * View user
     * 
     * @param null $userId
     * @return bool
     */
    public function viewAction()
    {
        $action = $this->Btn->getAction();
        if($action === "select_user") {
            $this->_getHelper()->serviceSelect(
                "/admin/user/view/",
                "system_user_id",
                "user"
            );
        }
        if($this->response->isSent()) {
            return false;
        }

        $userId = $this->localsession->getKeyValue("system_user_id");

        if($action === "addmgr" && UniqueId::uuidValidate($userId)) {
            $this->_getHelper()->setKeyValue("admin_manager_create_originator",["key"=>"userview","id"=>$userId]);
            $this->localsession->setKeyValue("admin_manager_create_user_id",$userId);
            $this->response->redirect("/admin/manager/create/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $results = $this->_getHelper()->viewUser($userId);
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user_info = (object)$results["user"];
        }
        if(isset($results["reset_link"]) && $results["reset_link"]) {
            $this->view->reset_link = $results["reset_link"];
            $this->view->expires_in = $results["expires_in"];
        }
        if(isset($results["managers"]) && $results["managers"]) {
            $this->view->managers = (object)$results["managers"];
        }
    }
    
    public function delete($userId)
    {
        
    }

    /**
     * List users
     * 
     * @param int $page
     */
    public function listAction($page = 1)
    {
        $action = $this->Btn->getAction();
        if($action === "edit") {
            $userId = $this->Btn->getValue();
            $this->localsession->setKeyValue("system_user_id",$userId);
            $this->response->redirect("/admin/user/edit/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        } elseif ($action === "view") {
            $userId = $this->Btn->getValue();
            $this->localsession->setKeyValue("system_user_id",$userId);
            $this->response->redirect("/admin/user/view/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }

        $page = (int)$page;
        $nrows = 20;
        $users = $this->_getHelper()->listUsers($page,$nrows);
        if($users) {
            $this->view->pagination = $users;
        }
    }

    /**
     * Attach user to organization as manager
     * 
     * @param null $userId
     * @return bool
     */
    public function addToOrganizationAction($userId = null)
    {
        if(!UniqueId::uuidValidate($userId)) {
            $this->flashSession->error("Invalid user id");
            $this->response->redirect("admin/user/list/{$this->localsession->getQuerystring('?')}");
            $this->response->send();
            return false;
        }
        
        $this->session->set("admin_select_organization_for_add_user_id",$userId);
        $this->response->redirect("admin/manager/selectorg/{$this->localsession->getQuerystring('?')}");
        $this->response->send();
        return false;
    }
}