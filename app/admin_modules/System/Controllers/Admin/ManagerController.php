<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Admin;


use System\Helpers\Admin\ManagerHelper;

class ManagerController extends AdminControllerBase
{
    /**
     * @var ManagerHelper
     */
    private $_helper;

    /**
     * @return ManagerHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new ManagerHelper();
        }
        return $this->_helper;
    }

    
    public function initialize()
    {
        parent::initialize();
    }
    
    public function indexAction()
    {}

    /**
     * Create new manager 
     * Redirect to select-user and/or select-organization
     * as needed
     * 
     * @return bool
     * @throws \App\Library\Utils\JsonException
     */
    public function createAction()
    {
        $action = $this->Btn->getAction();
        if($action === "select_user") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/create/",
                "system_user_id",
                "user"
            );
        } elseif($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/create/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $userId = $this->localsession->getKeyValue("system_user_id");
        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $results = $this->_getHelper()->createManager($organizationId,$userId);
        # if successful create, return immediately
        if ($this->response->isSent()) {
            return false;
        }
        if(isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user = (object)$results["user"];
        }
        if(isset($results["organization"]) && $results["organization"]) {
            $this->view->organization = (object)$results["organization"];
        }

    }
    
    /**
     * List managers
     * 
     * @param int $reset
     * @param int $page
     */
    public function listAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);
        $action = $this->Btn->getAction();
        if($action === "edit") {
            $values = $this->Btn->getValue();
            $values = explode("/",$values);
            if(count($values) === 2) {
                $organizationId = $values[0];
                $userId = $values[1];
                $this->localsession->setKeyValue("system_organization_id",$organizationId);
                $this->localsession->setKeyValue("system_user_id",$userId);
                $this->response->redirect("/admin/manager/edit/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        } elseif ($action === "view") {
            $values = $this->Btn->getValue();
            $values = explode("/",$values);
            if(count($values) === 2) {
                $organizationId = $values[0];
                $userId = $values[1];
                $this->localsession->setKeyValue("system_organization_id",$organizationId);
                $this->localsession->setKeyValue("system_user_id",$userId);
                $this->response->redirect("/admin/manager/view/{$this->localsession->getQuerystring('?')}");
                $this->response->send();
                return false;
            }
        }
        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->listManagers($page,$nrows,$reset);

        if(isset($results["managers"]) && $results["managers"]) {
            $this->view->pagination = $results["managers"];
        }
        if(isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
        $manager = $this->_getHelper()->getKeyValue("admin_view_manager_manager_arr");
        if($manager) {
            $this->view->manager = (object)$manager;
        }
    }

    /**
     * View manager
     * 
     * @param null $organizationId
     * @param null $userId
     */
    public function viewAction()
    {
        $action = $this->Btn->getAction();
        if($action === "select_user") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/view/",
                "system_user_id",
                "user"
            );
        } elseif($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/view/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $userId = $this->localsession->getKeyValue("system_user_id");
        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $results = $this->_getHelper()->viewManager($organizationId,$userId);
        if(isset($results["manager"]) && $results["manager"]) {
            $this->view->manager = (object)$results["manager"];
            $this->_getHelper()->setKeyValue("admin_view_manager_manager_arr",$results["manager"]);
        }
        if(isset($results["roles"]) && $results["roles"]) {
            $this->view->roles = (object)$results["roles"];
        }
        if(isset($results["organization"]) && $results["organization"]) {
            $this->view->organization = (object)$results["organization"];
        }
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user = (object)$results["user"];
        }
    }

    public function editAction()
    {
        $action = $this->Btn->getAction();
        if($action === "select_user") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/edit/",
                "system_user_id",
                "user"
            );
        } elseif($action === "select_org") {
            $this->_getHelper()->serviceSelect(
                "/admin/manager/edit/",
                "system_organization_id",
                "organization"
            );
        }
        if ($this->response->isSent()) {
            return false;
        }

        $userId = $this->localsession->getKeyValue("system_user_id");
        $organizationId = $this->localsession->getKeyValue("system_organization_id");

        $results = $this->_getHelper()->editManager($organizationId,$userId);
        if(isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if(isset($results["manager"]) && $results["manager"]) {
            $this->view->manager = (object)$results["manager"];
        }
        if(isset($results["organization"]) && $results["organization"]) {
            $this->view->organization = (object)$results["organization"];
        }
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user = (object)$results["user"];
        }
    }
}