<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2019-02-06
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Service;


use System\Helpers\Service\ServiceHelper;

/**
 * Class ServiceController
 * @package System\Controllers\Service
 */
class ServiceController extends ServiceControllerBase
{
    /**
     * @var ServiceHelper
     */
    private $_helper;

    /**
     * @return ServiceHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new ServiceHelper();
        }
        return $this->_helper;
    }

    private function _getServiceName()
    {
        $routeParams = $this->router->getParams();
        $serviceName = isset($routeParams[0]) ? $routeParams[0] : "";
        return $serviceName;
    }
    
    private function _getServiceParams()
    {
        $page = 1;
        $reset = null;
        $params = $this->router->getMatches();
        if(isset($params[2])) {
            $str = substr($params[2],1);
            $params = explode("/",$str);
            $page = (int)$params[0];
            if(isset($params[1]) && $params[1]) {
                $reset = 1;
            }
        }
        return [
            "page" => $page,
            "reset" => $reset,
        ];
    }

    public function selectAction()
    {
        $params = $this->_getServiceParams();
        $page = $params["page"];
        $reset = $params["reset"];
        
        $serviceName = $this->_getServiceName();
        $params = $this->_getHelper()->getKeyValue("system_service_select_{$serviceName}");
        if(!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return false;
        }
        
        $nrows = 20;
        $results = $this->_getHelper()->select($page,$nrows,$reset,$params,$serviceName);
        if($this->response->isSent()) {
            return false;
        }
        if(isset($results["data"]) && $results["data"]) {
            $this->view->pagination = $results["data"];
        }
        if(isset($results["search_term"]) && $results["search_term"]) {
            $this->view->search_term = $results["search_term"];
        }
    }

    /**
     * @param int $page
     * @param null $reset
     * @return false
     */
    public function selectUserAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $params = $this->_getHelper()->getKeyValue("system_service_select_user");
        if (!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return false;
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->selectUser($page, $nrows, $reset, $params);
        if (isset($results["users"]) && $results["users"]) {
            $this->view->pagination = $results["users"];
        }
    }

    /**
     * @param int $page
     * @param null $reset
     * @return false
     */
    public function selectOrganizationAction($page = 1)
    {
        $reset = $this->dispatcher->getParam(1);

        $params = $this->_getHelper()->getKeyValue("system_service_select_organization");
        if (!$params) {
            $this->response->redirect("/");
            $this->response->send();
            return false;
        }

        $page = (int)$page;
        $nrows = 20;
        $results = $this->_getHelper()->selectOrganization($page, $nrows, $reset, $params);
        if (isset($results["organizations"]) && $results["organizations"]) {
            $this->view->pagination = $results["organizations"];
        }
    }

}