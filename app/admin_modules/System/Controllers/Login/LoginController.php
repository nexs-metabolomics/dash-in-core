<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-28
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Login;


use System\Helpers\Login\LoginHelper;

class LoginController extends LoginControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @var LoginHelper
     */
    private $_helper;

    /**
     * @return LoginHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new LoginHelper();
        }
        return $this->_helper;
    }

    /**
     * Login
     * 
     * @return bool
     */
    public function loginAction()
    {
        $form = $this->_getHelper()->login();
        if($form) {
            $this->view->form = $form;
        } elseif($this->response->isSent()) {
            # redirect
            return null;
        }
    }
    
    public function logoutAction()
    {
        $this->SU->unregister();
        $this->session->destroy();
        if(!$this->response->isSent()) {
            $this->response->redirect("");
            $this->response->send();
        }
        return null;
    }
    
    public function requestResetAction()
    {
        $form = $this->_getHelper()->requestReset();
        if($form) {
            $this->view->form = $form;
        }
    }
    
    public function confirmResetAction($token=null)
    {
        $form = $this->_getHelper()->confirmReset($token);
        if($form) {
            $this->view->form = $form;
        }
    }
    
    public function confirmNewAccountAction($token=null)
    {
        $results = $this->_getHelper()->confirmUserAccount($token);
        
        if(isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
    }
}