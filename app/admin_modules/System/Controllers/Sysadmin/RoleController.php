<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace System\Controllers\Sysadmin;

use System\Helpers\Sysadmin\RoleHelper;

class RoleController extends SysadminControllerBase
{

    /**
     * @var RoleHelper
     */
    private $_helper;

    /**
     * @return RoleHelper
     */
    private function _getHelper()
    {

        if (!$this->_helper) {
            $this->_helper = new RoleHelper();
        }

        return $this->_helper;
    }

    public function initialize()
    {

        parent::initialize();
    }

    public function indexAction()
    {
    }

    /**
     * List roles
     *
     * @param int $page
     */
    public function listRolesAction()
    {

        $page = (int)$this->dispatcher->getParam(0, null, 1);
        $nrows = 20;
        $roles = $this->_getHelper()->listRoles($page, $nrows);
        if ($roles) {
            $this->view->pagination = $roles;
        }
    }

    /**
     * Create a new role
     */
    public function createRoleAction()
    {

        $form = $this->_getHelper()->createRole();
        if ($form) {
            $this->view->form = $form;
        }
    }

    /**
     * View a role
     *
     * @param null $roleId
     */
    public function viewRoleAction($roleId = null)
    {

        $results = $this->_getHelper()->viewRole($roleId);
        if (isset($results["role"]) && $results["role"]) {
            $this->view->role = (object)$results["role"];
        }
        if (isset($results["roleacl"]) && $results["roleacl"]) {
            $this->view->roleacl = (object)$results["roleacl"];
        }

    }

    /**
     * Create an access rule
     */
    public function createAccessRuleAction()
    {

        $form = $this->_getHelper()->createAccessRule();
        if ($form) {
            $this->view->form = $form;
        }
    }

    /**
     * List access rules
     *
     * @param int $page
     */
    public function listAccessRulesAction($page = 1)
    {

        $nrows = 20;
        $accessRules = $this->_getHelper()->listAccessRules($page, $nrows);
        if ($accessRules) {
            $this->view->pagination = $accessRules;
        }
    }

    /**
     * View an access rule
     *
     * @param null $accessRuleId
     */
    public function viewAccessRuleAction($accessRuleId = null)
    {

        $accessRule = $this->_getHelper()->viewAccessRule($accessRuleId);
        if ($accessRule) {
            $this->view->access_rule = (object)$accessRule;
        }
    }

    /**
     * Edit role hierarachy
     *
     * @param null $roleId
     */
    public function editAction($roleId = null)
    {

        $results = $this->_getHelper()->editRole($roleId);
        if (isset($results["form"]) && $results["form"]) {
            $this->view->form = $results["form"];
        }
        if (isset($results["included"]) && $results["included"]) {
            $this->view->included = $results["included"];
        }
        if (isset($results["excluded"]) && $results["excluded"]) {
            $this->view->excluded = $results["excluded"];
        }
        if (isset($results["unselected"]) && $results["unselected"]) {
            $this->view->unselected = $results["unselected"];
        }
        if (isset($results["role"]) && $results["role"]) {
            $this->view->role = (object)$results["role"];
        }
        $this->view->role_id = $roleId;
    }

}