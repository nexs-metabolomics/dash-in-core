<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Sysadmin;


use System\Forms\Sysadmin\RoleaclForm;
use System\Helpers\Sysadmin\RoleaclHelper;

class RoleaclController extends SysadminControllerBase
{
    /**
     * @var RoleaclHelper
     */
    private $_helper;
    
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @return RoleaclHelper
     */
    private function _getHelper()
    {
        if(!$this->_helper) {
            $this->_helper = new RoleaclHelper();
        }
        return $this->_helper;
    }

    public function indexAction()
    {}
    
    /**
     * 
     */
    public function createAction()
    {
        $form = $this->_getHelper()->createRoleacl();
        if($form) {
            $this->view->form = $form;
        }
    }

    public function editAction($roleaclId=null)
    {
        $result = $this->_getHelper()->editRoleacl($roleaclId);
        if(isset($result["form"]) && $result["form"]) {
            /** @var RoleaclForm $form */
            $form = $result["form"];
            $form->setAction("sysadmin/roleacl/edit/$roleaclId");
             $this->view->form = $form;
        }
        if(isset($result["roleacl"]) && $result["roleacl"]) {
             $this->view->roleacl = (object)$result["roleacl"];
             $this->view->roleacl_id = $roleaclId;
        }
        
    }

    public function listAction($page=1)
    {
        $page = (int)$page;
        $nrows = 20;
        $roleacls = $this->_getHelper()->listRoleacls($page,$nrows);
        if($roleacls) {
            $this->view->pagination = $roleacls;
        }
    }
    
    public function deleteAction($roleaclId=null)
    {
        $results = $this->_getHelper()->deleteRoleacl($roleaclId);
        if(isset($results["roleacl"]) && $results["roleacl"]) {
            $this->view->roleacl = (object)$results["roleacl"];
        }
        if(isset($results["confirm"]) && $results["confirm"]) {
            $this->view->confirm = $results["confirm"];
        }
        
    }
}