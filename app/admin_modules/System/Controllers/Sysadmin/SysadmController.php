<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-09-07
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\Sysadmin;


use System\Helpers\Sysadmin\SysadmHelper;

class SysadmController extends SysadminControllerBase
{
    /**
     * @var SysadmHelper
     */
    private $_helper;

    /**
     * @return SysadmHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new SysadmHelper();
        }
        return $this->_helper;
    }
    
    public function updateUrisAction()
    {
        $this->_getHelper()->updateUris();
    }
    
    public function listUrisAction()
    {
        $results = $this->_getHelper()->listUris();
        if(isset($results["links"]) && $results["links"]) {
            $this->view->links = $results["links"];
        }
    }

}