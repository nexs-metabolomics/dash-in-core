<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\User;


use System\Forms\User\UserEditForm;
use System\Helpers\User\HomeHelper;

class HomeController extends UserControllerBase
{
    /**
     * @var HomeHelper
     */
    private $_helper;

    /**
     * @return HomeHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new HomeHelper();
        }
        return $this->_helper;
    }

    public function initialize()
    {
        parent::initialize();
        $this->toggleSubmenu();
    }
    
    public function indexAction()
    {
    }
    
    public function editAction()
    {
        $userId = $this->SU->getUserId();
        $results = $this->_getHelper()->editUser($userId);
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user = (object)$results["user"];
        }
        if(isset($results["form"]) && $results["form"]) {
            /** @var UserEditForm $form */
            $form = $results["form"];
            $form->setAction("user/edit/");
            $this->view->form = $form;
        }
    }
    
    public function viewAction()
    {
        $results = $this->_getHelper()->view();
        if(isset($results["user"]) && $results["user"]) {
            $this->view->user = (object)$results["user"];
        }
        if(isset($results["managers"]) && $results["managers"]) {
            $this->view->managers = $results["managers"];
        }
        if(isset($results["manager_info"]) && $results["manager_info"]) {
            $this->view->manager_info = (object)$results["manager_info"];
        }
        if($results["changed"]) {
            $this->localsession->reset();
        }
    }
    
    public function setManagerAction()
    {
        
    }
}