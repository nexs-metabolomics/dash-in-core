<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-26
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace System\Controllers\User;


use System\Controllers\ControllerBase;

class UserControllerBase extends ControllerBase
{
    public function initialize()
    {
        $this->_viewBasePath .= "/user";
        parent::initialize();
    }

    public function beforeExecuteRoute()
    {
        if(!$this->SU->securedLogggedIn()) {
            $this->flashSession->error("You need to login");
            $this->response->redirect("/login/")->send();
            return false;
        }
        $this->view->IS_LOGGED_IN = true;

        $this->view->LOGGED_IN_USER_FULL_NAME = $this->SU->getUserFullName();
        $this->view->ACTIVE_MANAGER_ORG_NAME = $this->SU->getActiveManagerOrgName();
        $this->_pickView($this->_viewBasePath);

//        parent::beforeExecuteRoute();

    }

}