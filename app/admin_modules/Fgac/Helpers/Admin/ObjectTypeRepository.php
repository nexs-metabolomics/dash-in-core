<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-15
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Fgac\Helpers\Admin;


use Phalcon\Db\Enum;
use Fgac\Helpers\RepositoryBase;

class ObjectTypeRepository extends RepositoryBase
{
    public function createObjectType()
    {
        try {
            $sql = $this->security_config->sql->admin_create_object_type;

            $result = $this->db->fetchOne($sql,Enum::FETCH_ASSOC);
            if(isset($result["otid"])) {
                return $result["otid"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function updateObjectType($otid)
    {
        $otid = (int)$otid;
        try {
            $sql = $this->security_config->sql->admin_update_object_type;

            $state = $this->db->execute($sql,[
                "otid" => $otid,
            ]);

            
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    public function getObjectType($otid)
    {
        $otid = (int)$otid;
        try {
            $sql = $this->security_config->sql->admin_get_object_type;

            $state = $this->db->execute($sql,Enum::FETCH_ASSOC,[
                "otid" => $otid,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    public function getObjectTypes()
    {
        try {
            $sql = $this->security_config->sql->admin_get_object_types;

            $result = $this->db->fetchAll($sql,Enum::FETCH_ASSOC);
            if($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
    
    public function deleteObjectType($otid)
    {
        $otid = (int)$otid;
        try {
            $sql = $this->security_config->sql->admin_delete_object_type;

            $state = $this->db->execute($sql,[
                "otid" => $otid,
            ]);
            
            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}