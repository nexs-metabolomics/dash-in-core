<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-04-05
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace Fgac\Controllers\Client;


use Fgac\Helpers\Client\FgacHelper;

class FgacController extends ClientControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @var FgacHelper
     */
    private $_helper;

    /**
     * @return FgacHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new FgacHelper();
        }
        return $this->_helper;
    }

    
}