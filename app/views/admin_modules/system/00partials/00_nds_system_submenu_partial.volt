<ul>

  {% if show_submenu_sysadmin is defined and show_submenu_sysadmin %}
    <li>
      <a href="/sysadmin/index/{{ _ls_querystring_ }}">Sysadmin</a>
    </li>
    {{ partial("admin_modules/system/sysadmin/00partials/00_nds_sysadmin_submenu_partial") }}
  {% endif %}

  {% if show_submenu_admin is defined and show_submenu_admin %}
    <li>
      <a href="/admin/index/{{ _ls_querystring_ }}">Admin</a>
    </li>
    {{ partial("admin_modules/system/admin/00partials/00_nds_admin_submenu_partial") }}
  {% endif %}

{#  {% if show_submenu_sysadmin is defined %}#}
{#    <li class="main current">#}
{#      <a class="nds-menu-entry" href="/admin/index/{{ show_submenu_admin }}/{{ _ls_querystring_ }}">#}
{#        {{ partial("modules/dashin/00img_partials/icons/admin/ic_settings_24px.svg") }}#}
{#        System admin#}
{#      </a>#}
{#    </li>#}
{#    {% if show_submenu_sysadmin === 1 %}#}
{#      {{ partial("admin_modules/system/00partials/00_nds_admin_submenu_partial") }}#}
{#    {% endif %}#}
{#  {% else %}#}
{#    <li class="main">#}
{#      <a class="nds-menu-entry" href="/admin/index/{{ _ls_querystring_ }}">#}
{#        {{ partial("modules/dashin/00img_partials/icons/admin/ic_settings_24px.svg") }}#}
{#        System admin#}
{#      </a>#}
{#    </li>#}
{#  {% endif %}#}
</ul>
