<ul class="module-header">
  <li>
    <div class="module-header-inner{% if current is defined and current == "index_page" %} current{% endif %}">
      <a href="/dashin/index/{{ _ls_querystring_ }}">
        <div class="link-inner">
          {{ partial("00img_partials/v1/icons/home-icon.svg") }}
          Home
        </div>
      </a>
    </div>
  </li>
</ul>

<ul class="module-submenu">

  {{ m_menu_entry("System admin-x", "sysadmin", submenu_name, "/admin/index", "admin_modules/system/admin/00partials/00_nds_admin_submenu_partial", "00img_partials/v1/icons/dataset-icon.svg"       , _ls_querystring_ ,show_submenu) }}

</ul>
