{% extends "layouts/base-selectpage.volt" %}
  {% block set_params %}
    {{ set_title("Select Organization") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/system/service/select/organization/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[search]" class="button hidden-default" style="position: absolute;left: -90000px">Cancel</button>
    <button form="form1" class="button selectpage cancel" name="btn[cancel]">cancel</button>
  {% endblock %}

  {% block nav_header %}
    {{ partial("00element_partials/nds_table_nav_search_form",[
      "nav_link":"system/service/select/organization",
      "act_link":"system/service/select/organization",
      "reset_link":"system/service/select/organization/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}

  {% block page_content %}



    <div class="table-container fixed-header-select">
      
      <table>
        <thead>
          <tr>
            <th>Actions</th>
            <th>Name</th>
            <th>Description</th>
          </tr>
        </thead>

        <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>
                  <button form="form1" name="btn[select]" value="{{ row.organization_id }}" class="button selectpage select">Select</button>
                </td>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}

  {% block page_subfooter %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"system/service/select/organization",
        "reset_link":"system/service/select/organization/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}