{% extends "layouts/base-selectpage.volt" %}
  {% block set_params %}
    {{ set_title("Select user") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/system/service/select/user/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[search]" class="button hidden-default" style="position: absolute;left: -90000px">Cancel</button>
    <button form="form1" class="button selectpage cancel" name="btn[cancel]">cancel</button>
  {% endblock %}

  {% block page_content %}


    <div class="table-container fixed-header-select">

      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Code/Id</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td class="actions">
                <button form="form1" name="btn[select]" value="{{ row.user_id }}" class="button selectpage select">Select</button>
              </td>
              <td>
                <a href="/system/user/view/{{ row.user_id }}">{{ row.first_name }} {{ row.last_name }}</a>
              </td>
              <td>{{ row.email }}</td>
              <td>{{ row.code }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}

  {% block page_subfooter %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"system/service/select/user",
        "reset_link":"system/service/select/user/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}