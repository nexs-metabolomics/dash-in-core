{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Select Organization") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
  {% endblock %}
  {% block content2 %}

    <form id="form1" action="/system/service/selectorg/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

    <button form="form1" class="nds-button" name="btn[cancel]">cancel</button>

    <div class="nds-table-container">
      {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

      {{ partial("00element_partials/nds_table_nav_search_form",[
        "nav_link":"system/service/selectorg",
        "act_link":"system/service/selectorg",
        "reset_link":"system/service/selectorg/1/1",
        "has_results":has_results
      ]) }}

      <form action="/system/service/selectorg/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
        <table>

          <thead>
          <tr>
            <th>Actions</th>
            <th>Name</th>
            <th>Description</th>
          </tr>
          </thead>

          <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>
                  <button name="btn[select]" value="{{ row.organization_id }}" class="nds-button nds-tiny">Select</button>
                </td>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>

        </table>
      </form>
    </div>
  {% endblock %}
