{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Select userx1") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
  {% endblock %}
  {% block tab_menu %}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/system/service/selectuser/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="nds-button" name="btn[cancel]">cancel</button>
  {% endblock %}
  {% block nav_header %}
    {{ partial("00element_partials/nds_table_nav_search_form",[
      "nav_link":"system/service/selectuser",
      "act_link":"system/service/selectuser",
      "reset_link":"system/service/selectuser/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}
  {% block content2 %}


    <div class="nds-table-container">

      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Code/Id</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td class="actions">
                <button form="form1" name="btn[select]" value="{{ row.user_id }}" class="nds-button nds-tiny">Select</button>
              </td>
              <td>
                <a href="/system/user/view/{{ row.user_id }}">{{ row.first_name }} {{ row.last_name }}</a>
              </td>
              <td>{{ row.email }}</td>
              <td>{{ row.code }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}
