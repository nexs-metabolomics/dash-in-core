{% extends "layouts/basepage-index.volt" %}
  {% block set_params %}
    {{ set_title("System") }}
  {% endblock %}

  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"current":"systemindex"]) }}
  {% endblock %}
  {% block content2 %}

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/sysadmin/index/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Sysadmin
              </header>
              <p>Sysadmin</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/index/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Admin
              </header>
              <p>Admin</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
