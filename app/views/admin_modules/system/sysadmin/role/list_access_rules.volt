{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("List access rules") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_role":1,"current":"listrules"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <div class="nds-table-container">
      {% if pagination is defined %}
        <table>
          <thead>
          <tr>
            <th>Action</th>
            <th>ID</th>
            <th>Key</th>
            <th>Name</th>
            <th>Description</th>
            <th>Rule</th>
          </tr>
          </thead>
          <tbody>
          {% for row in pagination.items %}
            <tr>
              <td><a href="/sysadmin/accessrule/view/{{ row.access_rule_id }}" class="nds-button nds-tiny">View</a></td>
              <td>{{ row.access_rule_id }}</td>
              <td>{{ row.access_rule_key }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
              <td>{{ row.namespace }}/{{ row.class }}/{{ row.allow }}|{{ row.deny }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  {% endblock %}
