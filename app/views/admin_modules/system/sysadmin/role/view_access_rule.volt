{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("View access rules") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_role":1,"current":"viewrule"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <div class="nds-view-container view-dataset">
      {% if access_rule is defined %}
        <div class="nds-view-area">

          <div class="nds-view-block">
            <div class="columns small-1">
              Key
            </div>
            <div class="columns">
              {{ access_rule.access_rule_key }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="columns small-1">
              Name
            </div>
            <div class="columns">
              {{ access_rule.name }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="columns small-1">
              Description
            </div>
            <div class="columns">
              {{ access_rule.description }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="nds-column">
              Namespace
              <div>{{ access_rule.namespace }}</div>
            </div>
            <div class="nds-column">
              Class
              <div>{{ access_rule.class }}</div>
            </div>
            <div class="nds-column">
              Allow
              <div>{{ access_rule.allow }}</div>
            </div>
            <div class="nds-column">
              Deny
              <div>{{ access_rule.allow }}</div>
            </div>
          </div>
        </div>
      {% endif %}
    </div>
  {% endblock %}
