{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("List Roles") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_role":1,"current":"list"]) }}
{#    {{ partial("admin_modules/system/sysadmin/00partials/nds_role_menu_partial",["current":"list"]) }}#}
{#    {{ partial("admin_modules/system/sysadmin/00partials/00_nds_sysadmin_submenu_partial") }}#}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <div class="nds-table-container">
      {% if pagination is defined %}
        <table>
          <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {% for row in pagination.items %}
            <tr>
              <td>{{ row.role_id }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
              <td class="actions">
                <a href="/sysadmin/role/view/{{ row.role_id }}" class="nds-button nds-small nds-circle">V</a>
                <a href="/sysadmin/role/edit/{{ row.role_id }}" class="nds-button nds-small nds-circle">E</a>
              </td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  {% endblock %}
