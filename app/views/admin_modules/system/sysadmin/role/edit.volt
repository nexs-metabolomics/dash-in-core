{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Edit Role") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_role":1,"current":"edit"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <div class="nds-role-edit-container">
      <div class="nds-table-container">
        <form accept-charset="utf-8" action="/sysadmin/role/edit/{{ _ls_querystring_ }}" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
          <div class="nds-row">
            <div class="nds-column">
              <table>
                <thead>
                <tr>
                  <th>Allow</th>
                </tr>
                </thead>
                <tbody>
                {% if included is defined %}
                  {% for row in included %}
                    <tr>
                      <td>
                        <button name="btn[inclrm]" value="{{ row.role_id }}" class="button small">{{ row.name }}</button>
                      </td>
                    </tr>
                  {% endfor %}
                {% endif %}
                </tbody>
              </table>
            </div>

            <div class="nds-column">
              <table>
                <thead>
                <tr>
                  <th>Deny</th>
                </tr>
                </thead>
                <tbody>
                {% if excluded is defined %}
                  {% for row in excluded %}
                    <tr>
                      <td>
                        <button name="btn[exclrm]" value="{{ row.role_id }}" class="button small">{{ row.name }}</button>
                      </td>
                    </tr>
                  {% endfor %}
                {% endif %}
                </tbody>
              </table>
            </div>
 
            <div class="nds-column">
              <table>
                <thead>
                <tr>
                  <th colspan="4">
                    Available roles
                  </th>
                </tr>
                <tr>
                  <th class="idcol">ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {% if unselected is defined %}
                  {% for row in unselected %}
                    <tr>
                      <td class="idcol">{{ row.role_id }}</td>
                      <td>{{ row.name }}</td>
                      <td>{{ row.description }}</td>
                      <td class="actions">
                        <div class="row" style="text-align: right;justify-content: flex-end">
                          <button name="btn[incladd]" value="{{ row.role_id }}" class="nds-button nds-small nds-circle">+</button>
                          <button name="btn[excladd]" value="{{ row.role_id }}" class="nds-button nds-small nds-circle">-</button>
                        </div>
                      </td>
                    </tr>
                  {% endfor %}
                {% endif %}
                </tbody>
              </table>
            </div>
          </div>
        </form>
      </div>
    </div>
  {% endblock %}
