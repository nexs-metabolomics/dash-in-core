{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Create Access Rule") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_role":1,"current":"createrule"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
      <div class="nds-form-container">
        {% if form is defined %}
          {{ form([form.getAction(),"accept-charset":"utf-8","class":"small-12 "]) }}
          <div class="nds-fieldset">

            {% set e = form.get("key") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>

            {% set e = form.get("name") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>

            {% set e = form.get("description") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label><br>
              {{ e.render() }}
            </div>
          </div>
          <div class="nds-fieldset">

            {% set e = form.get("namespace") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>
            {% set e = form.get("class") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>
            {% set e = form.get("actallow") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>
            {% set e = form.get("actdeny") %}
            <div class="">
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {{ e.render() }}
            </div>
          </div>
          <div class="nds-fieldset">
            <div class="nds-btn-group">
              <button id="btn[submit]" name="btn[submit]" class="nds-button nds-expanded nds-small"><span
                    class="btn-txt">Ok</span></button>
              <a href="/sysadmin/accessrule/list/{{ _ls_querystring_ }}" class="nds-button nds-expanded nds-small">Cancel</a>
            </div>
          </div>
          {{ endForm() }}
        {% endif %}
      </div>
  {% endblock %}
