{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("System Administration") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"current":"sysadminindex"]) }}
  {% endblock %}
  {% block content2 %}
    <header>
      {{ get_title(false) }}
    </header>
    <nav class="nds-box-menus">

      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/sysadmin/role/index/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Roles
              </header>
              <p>Roles</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/sysadmin/roleacl/list/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                ACL
              </header>
              <p>Acl</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
