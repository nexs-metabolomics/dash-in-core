{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Delete access rule") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_acl":1,"current":"delete"]) }}
  {% endblock %}
  {% block header2 %}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    {% if roleacl is defined %}
      {% if confirm is defined and confirm === true %}
        {% set warn_style = ";background-color: rgba(255,14,54,0.46)" %}
      {% else %}
        {% set warn_style = "" %}
      {% endif %}
      <div class="nds-view-container view-dataset" style="{{ warn_style }}">
        <div class="nds-view-area">

          <div class="nds-view-block">
            <div class="nds-view-header">
              Name
            </div>
            <div class="nds-view-value">
              {{ roleacl.name }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="nds-view-header">
              Description
            </div>
            <div class="nds-view-value">
              {{ roleacl.description }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="nds-view-header">
              Namespace
            </div>
            <div class="nds-view-value">
              {{ roleacl.namespace }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="nds-view-header">
              Allowed classes
            </div>
            <div class="nds-view-value">
              {{ roleacl.class_allow }}
            </div>
          </div>

          <div class="nds-view-block">
            <div class="nds-view-header">
              Denied classes
            </div>
            <div class="nds-view-value">
              {{ roleacl.class_deny }}
            </div>
          </div>

        </div>
        <div class="nds-edit-container">
          <div class="nds-form-container">
            <form action="/sysadmin/roleacl/delete/{{ _ls_querystring_ }}" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
              <div class="nds-fieldset">
                <div class="nds-btn-group">
                  {% if confirm is defined and confirm === true %}
                    <button id="btn[submit]" name="btn[confirm]" class="nds-button nds-small nds-expanded nds-alert">
                      <span class="btn-txt">Confirm</span>
                    </button>
                  {% else %}
                    <button id="btn[submit]" name="btn[delete]" class="nds-button nds-small nds-expanded">
                      Delete
                    </button>
                  {% endif %}
                  <a href="/sysadmin/roleacl/delete/{{ _ls_querystring_ }}" class="nds-button nds-small nds-expanded">Cancel</a>
                </div>
              </div>
            </form>
          </div>

        </div>
      </div>
    {% endif %}
  {% endblock %}
