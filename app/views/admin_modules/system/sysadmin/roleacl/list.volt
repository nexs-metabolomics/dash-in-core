{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("List access rules") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_acl":1,"current":"list"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <div class="nds-table-container">
      <table>
        <thead>
        <tr>
          {#<th>Id</th>#}
          <th>Actions</th>
          <th>Name</th>
          <th>Role</th>
          <th>Role_id</th>
          <th>Namespace</th>
          <th>Allow</th>
          <th>Deny</th>
          <th>Allow</th>
          <th>Deny</th>
        </tr>
        </thead>
        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td class="actions">
                <a href="/sysadmin/roleacl/edit/{{ row.roleacl_id }}" class="nds-button nds-small nds-circle">E</a>
                <a href="/sysadmin/roleacl/delete/{{ row.roleacl_id }}" class="nds-button nds-small nds-circle nds-alert">X</a>
              </td>
              {#<td>{{ row.roleacl_id }}</td>#}
              <td>{{ row.roleacl_name }}</td>
              <td class="format-c">{{ row.role_id }}</td>
              <td>{{ row.role_name }}</td>
              <td>{{ row.namespace }}</td>
              <td class="format-c">{{ row.class_allow }}</td>
              <td class="format-c">{{ row.class_deny }}</td>
              <td class="format-c">{{ row.action_allow }}</td>
              <td class="format-c">{{ row.action_deny }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}
