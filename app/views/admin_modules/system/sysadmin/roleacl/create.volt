{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("System Administration") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_acl":1,"current":"create"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
      <div class="nds-form-container">
        {% if form is defined %}
          {{ form([form.getAction(),"accept-charset":"utf-8"]) }}
          {% for e in form %}

            {% if e.getAttribute('required') %}
              {% set required_asterisk = " * " %}
              {% set required_popup = ' title="Required field"' %}
            {% else %}
              {% set required_asterisk = "" %}
              {% set required_popup = "" %}
            {% endif %}

            <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
              {% if e.getLabel()!="" %}
                {% if e.getUserOption("type")=="select" %}
                  <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                {% else %}
                  <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                {% endif %}
              {% endif %}
              {{ e.render() }}
            </div>
          {% endfor %}
          <div class="nds-fieldset">
            <div class="nds-btn-group">
              <button id="btn[submit]" name="btn[submit]" class="nds-button nds-small nds-expanded">
                Ok
              </button>
              <a href="/sysadmin/roleacl/list/" class="nds-button nds-small nds-expanded">Cancel</a>
            </div>
          </div>
          {{ endForm() }}
        {% endif %}
      </div>
  {% endblock %}
