{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("System Administration") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_sysadmin":1,"show_submenu_sysadmin_acl":1,"current":"aclindex"]) }}
  {% endblock %}
  {% block content2 %}
    <header>
      {{ get_title(false) }}
    </header>
    <nav class="nds-box-menus">

      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/sysadmin/roleacl/create/">
            <div class="info-group-inner">
              <header>
                Create
              </header>
              <p>Create access rule</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/sysadmin/roleacl/edit/">
            <div class="info-group-inner">
              <header>
                Edit
              </header>
              <p>Edit access rule</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/sysadmin/roleacl/list/">
            <div class="info-group-inner">
              <header>
                List
              </header>
              <p>List access rules</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
