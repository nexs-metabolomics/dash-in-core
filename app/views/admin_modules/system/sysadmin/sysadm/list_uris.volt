{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Upload Dataset") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block content2 %}
    <header class="nds-page-header">{{ get_title() }}</header>
    <div class="nds-edit-container">
      <table>
        <thead>
        <tr>
          <th>Path</th>
          <th>Titles</th>
          <th>Link</th>
        </tr>
        </thead>
        <tbody>
        {% if links is defined %}
          {% for link in links %}
            <tr>
              <td style="font-size: 70%;">
                {{ link.namespace }}<br>
                {{ link.controller }}
              </td>
              <td style="border: #a9c4ff solid 1px;font-weight: bold;">
                {{ link.n_lab }}<br>
                {{ link.c_lab }}<br>
                {{ link.a_lab }}
              </td>
              <td style="border: #a9c4ff solid 1px">
                <a href="{{ link.uri }}" target="_jensen">{{ link.uri }}</a>
              </td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}
