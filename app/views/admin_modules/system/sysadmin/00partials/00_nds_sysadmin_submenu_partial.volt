<ul class="nds-submenu">


  <li class="main{% if current is defined and current === "roleindex" %} current{% endif %}">
    <a href="/sysadmin/role/index/{{ _ls_querystring_ }}">Roles</a>
  </li>

  {% if show_submenu_sysadmin_role is defined and show_submenu_sysadmin_role %}
    <li class="sub{% if current is defined and current === "create" %} current{% endif %}">
      <a href="/sysadmin/role/create/{{ _ls_querystring_ }}">Create role</a>
    </li>
    <li class="sub{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/sysadmin/role/edit/{{ _ls_querystring_ }}">Edit role</a>
    </li>
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/sysadmin/role/list/{{ _ls_querystring_ }}">List roles</a>
    </li>
    <li class="sub{% if current is defined and current === "view" %} current{% endif %}">
      <a href="/sysadmin/role/view/1/{{ _ls_querystring_ }}">View role</a>
    </li>
    <li class="sub{% if current is defined and current === "listrules" %} current{% endif %}">
      <a href="/sysadmin/accessrule/list/{{ _ls_querystring_ }}">List rule</a>
    </li>
    <li class="sub{% if current is defined and current === "createrule" %} current{% endif %}">
      <a href="/sysadmin/accessrule/create/{{ _ls_querystring_ }}">Create rule</a>
    </li>
    <li class="sub{% if current is defined and current === "viewrule" %} current{% endif %}">
      <a href="/sysadmin/accessrule/view/{{ _ls_querystring_ }}">Create rule</a>
    </li>
  {% endif %}

  <li class="main{% if current is defined and current === "aclindex" %} current{% endif %}">
    <a href="/sysadmin/roleacl/index/{{ _ls_querystring_ }}">Access rules</a>
  </li>

  {% if show_submenu_sysadmin_acl is defined and show_submenu_sysadmin_acl %}
    <li class="sub{% if current is defined and current === "create" %} current{% endif %}">
      <a href="/sysadmin/roleacl/create/{{ _ls_querystring_ }}">Create</a>
    </li>
    <li class="sub{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/sysadmin/roleacl/edit/{{ _ls_querystring_ }}">Edit</a>
    </li>
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/sysadmin/roleacl/list/{{ _ls_querystring_ }}">List</a>
    </li>
  {% endif %}

</ul>

