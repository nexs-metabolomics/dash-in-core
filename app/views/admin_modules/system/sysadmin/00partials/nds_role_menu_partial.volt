<ul>
  <li class="img-list{% if current is defined and current === "list" %} current{% endif %}">
    <a href="/sysadmin/role/list/{{ _ls_querystring_ }}"><span class="tab-text">List Roles</span></a>
  </li>
  <li class="img-create{% if current is defined and current === "create" %} current{% endif %}">
    <a href="/sysadmin/role/create/{{ _ls_querystring_ }}"><span class="tab-text">Add Role</span></a>
  </li>
  {% if role_id is defined %}
    <li class="img-edit{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/sysadmin/role/edit/{{ role_id }}"><span class="tab-text">Edit Role</span></a>
    </li>
  {% endif %}
  {#<li class="columns small-1{% if current is defined and current === "create" %} current{% endif %}">#}
    {#<a href="/sysadmin/accessrule/create/{{ _ls_querystring_ }}">Add Rule</a>#}
  {#</li>#}
  {#<li class="columns small-1">#}
    {#<a href="/sysadmin/accessrule/list/{{ _ls_querystring_ }}">List Rules</a>#}
  {#</li>#}
</ul>
