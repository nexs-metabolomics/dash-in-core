<nav class="columns small-12 product-menu-level-2">
  <ul class="row">
    <li class="nav-arrow-left" style="width: 3em">
      <a href="/sysadmin/index/">
        {{ partial("00img_partials/arrow-up-svg") }}
      </a>
    </li>
    <li class="columns medium-expand menu-tab img-list large-1{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/sysadmin/roleacl/list/"><span class="tab-text">List Rules</span></a>
    </li>
    <li class="columns medium-expand menu-tab img-create large-1{% if current is defined and current === "create" %} current{% endif %}">
      <a href="/sysadmin/roleacl/create/"><span class="tab-text">Add Rule</span></a>
    </li>
    {% if roleacl_id is defined %}
      <li class="columns medium-expand menu-tab img-edit large-1{% if current is defined and current === "edit" %} current{% endif %}">
        <a href="/sysadmin/roleacl/edit/{{ roleacl_id }}"><span class="tab-text">Edit Rule</span></a>
      </li>
    {% endif %}
  </ul>
</nav>