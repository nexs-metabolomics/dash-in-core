<nav class="columns small-12 product-menu-level-2">
  <ul class="row">
    <li class="nav-arrow-left" style="width: 3em">
      <a href="/sysadmin/index/">
        {{ partial("00img_partials/arrow-up-svg") }}
      </a>
    </li>


    <li class="columns medium-expand menu-tab img-list large-1{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/sysadmin/role/list/"><span class="tab-text">List Roles</span></a>
    </li>
    <li class="columns medium-expand menu-tab img-create large-1{% if current is defined and current === "create" %} current{% endif %}">
      <a href="/sysadmin/role/create/"><span class="tab-text">Add Role</span></a>
    </li>
    {% if role_id is defined %}
      <li class="columns medium-expand menu-tab img-edit large-1{% if current is defined and current === "edit" %} current{% endif %}">
        <a href="/sysadmin/role/edit/{{ role_id }}"><span class="tab-text">Edit Role</span></a>
      </li>
    {% endif %}
    {#<li class="columns small-1{% if current is defined and current === "create" %} current{% endif %}">#}
      {#<a href="/sysadmin/accessrule/create/">Add Rule</a>#}
    {#</li>#}
    {#<li class="columns small-1">#}
      {#<a href="/sysadmin/accessrule/list/">List Rules</a>#}
    {#</li>#}
  </ul>
</nav>