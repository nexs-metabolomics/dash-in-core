<ul>
  <li class="img-list{% if current is defined and current === "list" %} current{% endif %}">
    <a href="/sysadmin/roleacl/list/{{ _ls_querystring_ }}">
      <span class="tab-text">
        List Access Rules
      </span>
    </a>
  </li>
  <li class="img-create{% if current is defined and current === "create" %} current{% endif %}">
    <a href="/sysadmin/roleacl/create/{{ _ls_querystring_ }}">
      <span class="tab-text">
        Add Access Rule
      </span>
    </a>
  </li>
  {% if roleacl_id is defined %}
    <li class="img-edit{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/sysadmin/roleacl/edit/{{ roleacl_id }}">
        <span class="tab-text">
          Edit Acess Rule
        </span>
      </a>
    </li>
  {% endif %}
</ul>
