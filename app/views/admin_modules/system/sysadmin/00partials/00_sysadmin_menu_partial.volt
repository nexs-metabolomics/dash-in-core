<nav class="columns medium-12 large-9 menu-nav">

  <ul class="row menu-box-container">

    <li class="columns small-12 large-1 menu-box">
      <a href="/system/index/">
        <div class="info-group-inner">
          {{ partial("00img_partials/home-house-svg") }}
        </div>
      </a>
    </li>

    <li class="columns small-12 medium-6 large-2 menu-box">
      <a href="/sysadmin/role/list/">
        <div class="info-group-inner">
          <p style="font-size: 150%;font-weight: bold">Roles</p>
          <p>Edit roles</p>
        </div>
      </a>
    </li>

    <li class="columns small-12 medium-6 large-2 menu-box">
      <a href="/sysadmin/roleacl/list/">
        <div class="info-group-inner">
          <p style="font-size: 150%;font-weight: bold">Actions</p>
          <p>Edit allowed and disallowed actions</p>

        </div>
      </a>
    </li>

  </ul>

</nav>
