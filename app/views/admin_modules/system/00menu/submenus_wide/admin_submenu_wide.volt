<ul class="submenu sysadmin">

  {{ m_menu_submenuentry("Organizations" ,"/admin/organization/list", _ls_querystring_ ,current , "orgindex") }}
  {{ m_menu_submenuentry("Users" ,"/admin/user/list", _ls_querystring_ ,current , "userindex") }}
  {{ m_menu_submenuentry("Managers" ,"/admin/manager/list", _ls_querystring_ ,current , "mngrindex") }}

</ul>
