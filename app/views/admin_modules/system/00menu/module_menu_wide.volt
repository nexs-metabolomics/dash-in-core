<ul class="module-header">
  <li>
    <div class="module-header-inner{% if current is defined and current == "index_page" %} current{% endif %}">
      <a href="/dashin/index/{{ _ls_querystring_ }}">
        <div class="link-inner">
          {{ partial("00img_partials/v1/icons/home-icon.svg") }}
          Home
        </div>
      </a>
    </div>
  </li>
</ul>

<ul class="module-submenu">

  {{ m_menu_entry("System admin", "sysadmin", submenu_name, "/admin/index", "admin_modules/system/00menu/submenus_wide/admin_submenu_wide", "00img_partials/v1/icons/dataset-icon.svg"       , _ls_querystring_ ,show_submenu, ROLE_APP_ADMIN) }}

</ul>
