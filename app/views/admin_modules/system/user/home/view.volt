{% extends "admin_modules/system/user/home/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Select Manager Role") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/user/home/00_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}


    {% if user is defined %}
      <div style="display: flex;flex-flow: row">
        <div class="card-container">
          <header>
            Profile
          </header>
          <div class="card">
            <div class="card-fieldset-header">
              {{ user.first_name }} {{ user.last_name }}
            </div>
            <div class="card-fieldset-header">
              {{ user.email }}
            </div>
            <div class="card-fieldset-header">
              {{ user.code }}
            </div>
{#            <div class="card-fieldset-header">#}
{#              {{ user.user_id }}#}
{#            </div>#}
          </div>
        </div>

        <div class="card-container">
          <header>
            Manager role
          </header>
          <div class="card" style="flex: 1 0">
            <div class="card-fieldset-header">
              {{ manager_info.manager_label }}
            </div>
            <div class="card-fieldset-header">
              {{ manager_info.organization_name }}
            </div>
            <div class="card-fieldset-header">
              {{ manager_info.description }}
            </div>
            <div class="card-fieldset-header">
              {{ manager_info.role_name }} ({{ manager_info.role_id }})
            </div>
          </div>
        </div>

      </div>

      <div class="card-container">
        <header>
          {{ get_title() }}
        </header>
        <div class="card">

          {% if managers is defined %}

            <div class="card-column">
              {% for mgr in managers %}

                {% if mgr.active %}
                  <button form="form1" name="btn[deselect]" value="{{ mgr.organization_id }}" class="button manager selected">
                    {{ mgr.name }} | {{ mgr.label }} <div style="margin-left: auto">{{ mgr.role_name }} ({{ mgr.role_id }})</div>
                  </button>
                {% else %}
                  <button form="form1" name="btn[select]" value="{{ mgr.organization_id }}" class="button manager unselected">
                    {{ mgr.name }} | {{ mgr.label }} <div style="margin-left: auto">{{ mgr.role_name }} ({{ mgr.role_id }})</div>
                  </button>
                {% endif %}

              {% endfor %}
            </div>
          {% endif %}

        </div>
      </div>
    {% endif %}



  {% endblock %}

