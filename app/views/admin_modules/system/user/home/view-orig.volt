{% extends "layouts/base-page-subnav.volt" %}
  {% block set_params %}
    {{ set_title("Select Manager Role") }}
    {% if pagination is defined %}
      {% set has_results = true %}
      {% set current_page = pagination.current %}
    {% else %}
      {% set has_results = false %}
      {% set current_page = 1 %}
    {% endif %}
  {% endblock %}

  {% block menu_wide_module_header %}
    {{ partial("modules/dashin/00partials/menu_wide_module_header_partial") }}
    {#    {{ partial("admin_modules/system/user/00partials/menu_wide_module_header_partial") }} #}
  {% endblock %}

  {% block menu_wide_module_submenus %}
    {{ partial("admin_modules/system/user/00partials/menu_wide_module_submenus_partial",["show_submenu":1,"current":"selectrole"]) }}
  {% endblock %}

  {% block menu_narrow_module_header %}
    {#    {{ partial("modules/dashin/00partials/menu_narrow_module_header_partial") }} #}
  {% endblock %}

  {% block menu_narrow_module_submenus %}
    {#    {{ partial("modules/dashin/00partials/menu_narrow_module_submenus_partial",["show_submenu_study":1,"current":"datafilters_index"]) }} #}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/user/home/00_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_subnav %}
  {% endblock %}

  {% block subheader_action %}
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}


    {% if user is defined %}
      <div class="nds-view-container view-dataset">
        <div class="nds-view-block">
          <div class="nds-view-header">
            <header>{{ user.first_name }} {{ user.last_name }}</header>
          </div>
        </div>
        {#        <div class="nds-row"> #}
        {#          {% if user.is_public %} #}
        {#            <span class="pub-indicator">Public</span> #}
        {#          {% else %} #}
        {#            <span class="priv-indicator">Private</span> #}
        {#          {% endif %} #}
        {#        </div> #}
        <div class="nds-view-block">
          <div class="nds-view-value">
            {{ user.email }}
          </div>
        </div>
        <div class="nds-view-block">
          <div class="nds-view-value">
            {{ user.code }}
          </div>
        </div>
        <div class="nds-view-block">
          <div class="nds-view-value">
            {{ user.user_id }}
          </div>
        </div>
      </div>
      <p></p>

      {% if managers is defined %}

        <form action="/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

          <div class="mgrselect-outer">
            <div class="mgrselect-header">
              <div class="mgrselect-inner">
                <div class="head">
                  Organization
                </div>
                <div class="content">
                  Manager label
                </div>
              </div>
            </div>
          </div>
          {% for mgr in managers %}
            <div class="mgrselect-outer">
              {% if mgr.active %}
                <button name="btn[deselect]" value="{{ mgr.organization_id }}" class="mgrselect-button active">
                  <div class="mgrselect-inner">
                    <div class="head">
                      {{ mgr.name }}
                    </div>
                    <div class="content">
                      {{ mgr.label }}
                    </div>
                  </div>
                </button>
              {% else %}
                <button name="btn[select]" value="{{ mgr.organization_id }}" class="mgrselect-button">
                  <div class="mgrselect-inner">
                    <div class="head">
                      {{ mgr.name }}
                    </div>
                    <div class="content">
                      {{ mgr.label }}
                    </div>
                  </div>
                </button>
              {% endif %}
            </div>
          {% endfor %}
        </form>

      {% endif %}

    {% endif %}
  {% endblock %}

