{% extends "admin_modules/system/user/home/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit profile") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/user/home/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/user/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}


    <header>
      {% if user is defined %}
        {{ user.first_name }} {{ user.last_name }} -
      {% endif %}
      {{ get_title(false) }}
    </header>

    {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

{#    #}{#    <div class="nds-edit-container"> #}
{#    <div class="nds-form-container">#}
{#      {% if form is defined %}#}
{#        {{ form([form.getAction(),"accept-charset":"utf-8","class":"columns"]) }}#}
{#        {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","apply":"Apply","cancel":"Cancel"]]) }}#}
{#        #}{#          {% for e in form %} #}
{#        #}{#            <div class="nds-fieldset"> #}
{#        #}{#              {% if e.getLabel()!="" %} #}
{#        #}{#                {% if e.getUserOption("type")=="select" %} #}
{#        #}{#                  <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label> #}
{#        #}{#                {% else %} #}
{#        #}{#                  <label for="{{ e.getName() }}">{{ e.getLabel() }}</label> #}
{#        #}{#                {% endif %} #}
{#        #}{#              {% endif %} #}
{#        #}{#              {{ e.render() }} #}
{#        #}{#              #}{##}{# <span class="chk"></span> #}
{#        #}{#            </div> #}
{#        #}{#          {% endfor %} #}
{#        #}{#          <div class="nds-fieldset"> #}
{#        #}{#            <div class="nds-btn-group"> #}
{#        #}{#            <button id="btn[submit]" name="btn[submit]" class="nds-button nds-small nds-expanded"> #}
{#        #}{#              Ok #}
{#        #}{#            </button> #}
{#        #}{#            <button id="btn[apply]" name="btn[apply]" class="nds-button nds-small nds-expanded"> #}
{#        #}{#              Apply #}
{#        #}{#            </button> #}
{#        #}{#            <a href="/user/view/{{ _ls_querystring_ }}" class="nds-button nds-small nds-expanded">Cancel</a> #}
{#        #}{#            </div> #}
{#        #}{#          </div> #}
{#        {{ endForm() }}#}
{#      {% endif %}#}
{#    </div>#}


    {#    </div> #}

  {% endblock %}
