{% extends "admin_modules/system/user/home/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("My account") }}

  {% endblock %}
  {% block index_header %}
    <header>{{ get_title(false) }}</header>
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-small">

      <ul>

        {{ m_indexpage_menu_entry("Select role", "section-account","/user/view","00img_partials/v1/icons/select-manager-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Edit profile", "section-account","/user/edit","00img_partials/v1/icons/edit-profile-icon.svg", _ls_querystring_) }}

      </ul>
    </nav>

  {% endblock %}
