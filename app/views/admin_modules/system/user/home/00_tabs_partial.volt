<div class="tabs-row" style="margin-bottom: 1rem">

  <div class="tab{% if current_tab is defined and current_tab === "view" %} current{% endif %}">
    <a href="/user/view/{{ _ls_querystring_ }}">
      Select role
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "edit" %} current{% endif %}">
    <a href="/user/edit/{{ _ls_querystring_ }}">
      Edit profile
    </a>
  </div>

</div>
