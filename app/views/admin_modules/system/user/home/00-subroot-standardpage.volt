{% extends "layouts/root-standardpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'useraccount',"show_submenu":1,"current":"useraccount_index"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'useraccount',"show_submenu":1,"current":"useraccount_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-account">
        {{ partial("00img_partials/v1/icons/home-icon.svg") }}
      </div>
      <a href="/user/{{ _ls_querystring_ }}">Profile</a>
    </div>
  {% endblock %}
