<ul class="module-submenu">

  {# ============================================================================== #}
  {% if show_submenu is defined %}
    <li class="main current">
      <a class="menu-entry" href="/user/index/{{ show_submenu }}/{{ _ls_querystring_ }}">
{#        {{ partial("modules/dashin/00img_partials/icons/upload/ic_cloud_upload_24px.svg") }}#}
        My account
      </a>
    </li>
    {% if show_submenu %}
      {{ partial("admin_modules/system/user/00partials/menu_wide_user_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/user/index/{{ _ls_querystring_ }}">
{#        {{ partial("modules/dashin/00img_partials/icons/upload/ic_cloud_upload_24px.svg") }}#}
        My account
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
</ul>
