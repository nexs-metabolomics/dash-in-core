<ul class="submenu useraccount">

  <li class="head{% if current is defined and current === "selectrole" %} current{% endif %}">
    <a class="menu-entry" href="/user/view/{{ show_submenu }}/{{ _ls_querystring_ }}">
      {#      {{ partial("modules/dashin/00img_partials/icons/upload/ic_cloud_upload_24px.svg") }} #}
      Select role
    </a>
  </li>

  <li class="head{% if current is defined and current === "editprofile" %} current{% endif %}">
    <a class="menu-entry" href="/user/edit/{{ show_submenu }}/{{ _ls_querystring_ }}">
      {#      {{ partial("modules/dashin/00img_partials/icons/upload/ic_cloud_upload_24px.svg") }} #}
      Edit profile
    </a>
  </li>

</ul>
