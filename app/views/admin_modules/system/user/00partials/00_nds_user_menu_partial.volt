<ul class="nds-submenu">
  <li class="head{% if current is defined and current === "user" %} current{% endif %}">
    <a href="/user/index/{{ _ls_querystring_ }}">My account</a>
  </li>
  {% if SU.getUserId() %}
    <li class="head{% if current is defined and current === "view" %} current{% endif %}">
      <a href="/user/view/{{ _ls_querystring_ }}">Select role</a>
    </li>
    <li class="head{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/user/edit/{{ _ls_querystring_ }}">Edit profile</a>
    </li>
  {% endif %}
</ul>
