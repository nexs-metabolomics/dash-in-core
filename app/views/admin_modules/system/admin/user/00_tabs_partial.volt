<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/admin/user/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create", "/admin/user/create", "create", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create with organization", "/admin/user/createwithorg", "createwithorg", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/admin/user/edit", "edit", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("View", "/admin/user/view", "view", current_tab, _ls_querystring_) }}
  
</div>
