{% extends "layouts/basepage-index.volt" %}
  {% block set_params %}
    {{ set_title("Users") }}
  {% endblock %}
  {% block index_header %}
    <header>{{ get_title(false) }}</header>
  {% endblock %}

  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_user":1,"current":"userindex"]) }}
  {% endblock %}
  {% block content2 %}
    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/admin/user/create/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Create
              </header>
              <p>Add new user</p>
            </div>
          </a>
        </li>
        
        <li class="nds-card">
          <a href="/admin/user/edit/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit
              </header>
              <p>Edit user</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/user/view/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                View
              </header>
              <p>View user</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/user/list/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                View
              </header>
              <p>View user</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
