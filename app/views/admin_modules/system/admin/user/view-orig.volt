{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("View User") }}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_user":1,"current":"userindex"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    {# <form id="form1" action="/dashin/owner/study/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form> #}
    {# <button form="ex" class="nds-button nds-hollow" name="btn[select_study]">Ex. selected</button> #}
    {# <button form="ex" class="nds-button" name="btn[select_study]">Ex. select</button> #}
    <form id="form1" action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_user]" class="nds-button">Select user</button>
  {% endblock %}
  {% block content2 %}



    <div class="nds-table-container view-dataset">
    {% if user is not defined %}
      <div class="nds-view-block">
        <div class="nds-view-header">
          <header>
            Please select a user
          </header>
        </div>
      </div>
    {% else %}
      <div class="nds-view-block">
        <div class="nds-view-header">
          <header>
            {{ user.first_name }} {{ user.last_name }}
          </header>
        </div>
      </div>

      <div class="nds-view-block">
        <div class="nds-view-header">
          Email
        </div>
        <div class="nds-view-value">
          {{ user.email }}
        </div>
      </div>

      <div class="nds-view-block">
        <div class="nds-view-header">
          User code
        </div>
        <div class="nds-view-value">
          {{ user.code }}
        </div>
      </div>

      <div class="nds-row">
        <div class="nds-description">
          <form action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
            <button name="btn[addmgr]" class="nds-button nds-small">Add to organization</button>
          </form>
        </div>
      </div>

      {% if reset_link is defined %}
        <div class="nds-view-block">
          <div class="nds-view-header">
            Password link: (Expires in: {{ expires_in }})
          </div>
        </div>
        <div class="nds-view-block">
          <div class="nds-view-value" style="width: 100%">
            <input type="text" style="cursor: pointer;height: 1.5em;font-size:110%;font-weight: bold;width: 100%"
                   onclick="this.focus();
                       this.select()"
                   readonly="readonly"
                   value="{{ reset_link }}" class="output-placeholder">
          </div>
        </div>

        <div class="nds-row">
          <form action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()" style="padding: 0">
            <button name="btn[sendpw]" class="nds-button nds-small">Send password link</button>
            <button name="btn[sendcode]" class="nds-button nds-small">Send user code</button>
          </form>
        </div>

      {% else %}
        <div class="nds-row">
          <div class="nds-description">
            <form action="/admin/user/view/{{ _ls_querystring_ }}" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
              <button name="btn[pwlink]" class="nds-button nds-small">Generate password link</button>
            </form>
          </div>
        </div>
      {% endif %}
      </div>
    {% endif %}

    <div class="nds-table-container">
      <header>
        Manager roles
      </header>
      <table>
        <thead>
        <tr>
          <th>Actions</th>
          <th>Name</th>
          <th>Description</th>
          <th>Manager roles</th>
          <th>Label</th>
        </tr>
        </thead>
        <tbody>
        {% if managers is defined %}
          {% for row in managers %}
            <tr>
              <td style="vertical-align: top">
                <a href="/admin/manager/view/{{ _ls_querystring_ }}" class="nds-button nds-tiny">View</a>
                <a href="/admin/manager/edit/{{ _ls_querystring_ }}" class="nds-button nds-tiny">Edit</a>
              </td>
              <td style="vertical-align: top">{{ row.o_name }}</td>
              <td style="vertical-align: top">{{ row.o_description }}</td>
              <td style="vertical-align: top">{{ row.m_roles }}</td>
              <td style="vertical-align: top">{{ row.m_label }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}
