{% extends "admin_modules/system/admin/user/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List Users") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/user/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      {% if pagination is defined %}
        {% for row in pagination.items %}
          <div class="card hover-hide-action">
            <div class="card-column">
              <div class="card-fieldset-header">
                {{ row.first_name }} {{ row.last_name }}
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.code }}
              </div>
              <div class="card-field-content">
                {{ row.email }}
              </div>
            </div>

            <form action="/admin/user/list/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ m_card_button_box_confirm(
                row.user_id,
                row.confirm|default(null),
                [
                  ["edit","edit-icon48-filled","button edit icon24","Edit user",ROLE_ORG_ALLOW_WRITE],
                  ["view","view-icon48-filled","button view icon24","View user"]
                ],
                [
                  ["confirm","Confirm","button confirm","Confirm delete user",ROLE_ORG_ALLOW_WRITE],
                  ["cancel", "Cancel", "button cancel","Cancel delete user"]
                ],
                "Do you really want to delete this user"
              ) }}
              {#                  ,["delete","delete-icon48-outline","button delete icon24","Delete organization"]#}


            </form>

          </div>
        {% endfor %}
      {% endif %}
    </div>

  {% endblock %}
  {% block subfooter_content %}
    {{ partial("00element_partials/table_nav_filter",[
      "nav_link":"admin/user/list",
      "reset_link":"admin/user/list/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
