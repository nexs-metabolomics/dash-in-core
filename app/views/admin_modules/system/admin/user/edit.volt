{% extends "admin_modules/system/admin/user/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit User") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/user/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
{#    <button form="form1" name="btn[select_user]" class="button">Select user</button>#}
    {{ m_conditional_selectbutton("Select another user","select_user","form1",user_info|default(null)) }}
  {% endblock %}

  {% block page_content %}

    {% if user_info is not defined %}

      {{ m_emptypage_filler("Please select a user", "Select user", "select_user", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["apply","Apply","button apply"]],"form1") }}

    {% endif %}

  {% endblock %}
