{% extends "admin_modules/system/admin/user/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View User") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_action %}
    {# <form id="form1" action="/dashin/owner/study/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form> #}
    {# <button form="ex" class="nds-button nds-hollow" name="btn[select_study]">Ex. selected</button> #}
    {# <button form="ex" class="nds-button" name="btn[select_study]">Ex. select</button> #}
    <form id="form1" action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another user","select_user","form1",user_info|default(null)) }}
  {% endblock %}

  {% block page_content %}



    <div class="table-container card-container">

      {% if user_info is not defined %}

        {{ m_emptypage_filler("Please select another user", "Select user", "select_user", "form1") }}

      {% else %}

        <div class="card">

          <div class="card-column">
            <div class="card-field-header">
              {{ user_info.first_name }} {{ user_info.last_name }}
            </div>
          </div>

          <div class="card-row">
            <div class="card-field-header" style="margin-right: 16px">
              Email
            </div>
            <div class="card-field-content">
              {{ user_info.email }}
            </div>
          </div>

          <div class="card-row">
            <div class="card-field-header" style="margin-right: 16px">
              User code
            </div>
            <div class="card-field-content">
              {{ user_info.code }}
            </div>
          </div>

          <div class="card-row">
            <form action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
              <button name="btn[addmgr]" class="button">Add to organization</button>
            </form>
          </div>
        </div>

        <div class="card">
          {% if reset_link is defined %}
            <div class="card-row">
              <div class="card-field-header">
                Password link: (Expires in: {{ expires_in }})
              </div>
            </div>
            <div class="card-row">
              <div class="card-field-content" style="width: 100%">
                <input type="text" style="cursor: pointer;height: 1.5em;font-size:110%;font-weight: bold;width: 100%"
                       onclick="this.focus();
                       this.select()"
                       readonly="readonly"
                       value="{{ reset_link }}" class="output-placeholder">
              </div>
            </div>

            <div class="nds-row">
              <form action="/admin/user/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()" style="padding: 0">
                <button name="btn[sendpw]" class="nds-button nds-small">Send password link</button>
                <button name="btn[sendcode]" class="nds-button nds-small">Send user code</button>
              </form>
            </div>

          {% else %}
            <div class="nds-row">
              <div class="nds-description">
                <form action="/admin/user/view/{{ _ls_querystring_ }}" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
                  <button name="btn[pwlink]" class="button">Generate password link</button>
                </form>
              </div>
            </div>
          {% endif %}
        </div>
      {% endif %}

      <header>
        Manager roles
      </header>
      <div class="card">
        <div class="table-container">
          <table>
            <thead>
              <tr>
                <th>Actions</th>
                <th>Name</th>
                <th>Description</th>
                <th>Manager roles</th>
                <th>Label</th>
              </tr>
            </thead>
            <tbody>
              {% if managers is defined %}
                {% for row in managers %}
                  <tr>
                    <td style="vertical-align: top">
                      <div style="display: flex;flex-flow: row">
                        <button form="form1" name="btn[viewmanager]" value="{{ row.organization_id }}" class="button small">
                          View
                        </button>
                        <button form="form1" name="btn[editmanager]" value="{{ row.organization_id }}" class="button small">
                          Edit
                        </button>
{#                        <a href="/admin/manager/view/{{ _ls_querystring_ }}" class="button">View</a>#}
{#                        <a href="/admin/manager/edit/{{ _ls_querystring_ }}" class="button">Edit</a>#}
                      </div>
                    </td>
                    <td style="vertical-align: top">{{ row.o_name }}</td>
                    <td style="vertical-align: top">{{ row.o_description }}</td>
                    <td style="vertical-align: top">
                      <details closed style="cursor: pointer">
                        <summary>
                          Roles
                        </summary>
                        {{ row.m_roles }}
                      </details>
                    </td>
                    <td style="vertical-align: top">{{ row.m_label }}</td>
                  </tr>
                {% endfor %}
              {% endif %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  {% endblock %}
