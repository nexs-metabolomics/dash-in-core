{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Edit User") }}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_user":1,"current":"userindex"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/admin/user/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_user]" class="nds-button">Select user</button>
  {% endblock %}
  {% block content2 %}


    <div class="nds-card-container">
      <div class="nds-card">
        <div class="nds-form-container">
          {% if form is not defined %}
            <header>Please select user</header>
          {% else %}
            {{ form([form.getAction(),"accept-charset":"utf-8"]) }}

            {% for e in form %}

              {% if e.getAttribute('required') %}
                {% set required_asterisk = " * " %}
                {% set required_popup = ' title="Required field"' %}
              {% else %}
                {% set required_asterisk = "" %}
                {% set required_popup = "" %}
              {% endif %}

              <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
                {% if e.getLabel()!="" %}
                  {% if e.getUserOption("type")=="select" %}
                    <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                  {% else %}
                    <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                  {% endif %}
                {% endif %}
                {{ e.render() }}
              </div>
            {% endfor %}

            {# <div class="nds-fieldset"> #}
            {# <div class="nds-btn-group"> #}
            {# <button id="btn[submit]" name="btn[submit]" class="nds-button nds-small nds-expanded"> #}
            {# Ok #}
            {# </button> #}
            {# </div> #}
            {# </div> #}
            <div class="nds-card-button-row">
              <button name="btn[submit]" class="nds-button nds-hollow">
                Ok
              </button>
              <button name="btn[apply]" class="nds-button nds-hollow">
                Apply
              </button>
            </div>

            {{ endForm() }}
          {% endif %}
        </div>
      </div>
    </div>
  {% endblock %}
