{% extends "admin_modules/system/admin/user/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Create user with organization") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/user/00_tabs_partial",["current_tab":"createwithorg"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/user/createwithorg/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["apply","Apply","button apply"]],"form1") }}

    </div>
  {% endblock %}
