{% extends "admin_modules/system/admin/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Sysadmin module") }}
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-home">
      <ul>
        {{ m_indexpage_menu_entry("Organizations", "section-sysadmin","/admin/organization/list","00img_partials/v1/icons/sysadmin-organization-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Users", "section-sysadmin","/admin/user/list","00img_partials/v1/icons/admin-consortium.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Managers", "section-sysadmin","/admin/manager/list","00img_partials/v1/icons/admin-managers-icon.svg", _ls_querystring_) }}

      </ul>

    </nav>

  {% endblock %}
