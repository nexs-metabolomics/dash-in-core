{% extends "admin_modules/system/admin/manager/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View Manager") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/manager/00_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/manager/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_user]" class="button select">Select user</button>
    <button form="form1" name="btn[select_org]" class="button select">Select organization</button>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <div class="card">

        {% if manager is not defined %}
          <header>Please select manager</header>
        {% else %}
          <div class="view-container view-dataset">

            <div class="view-area">
              <div class="view-block">
                <div class="view-header">
                  User name
                </div>
                <div class="view-value">
                  {{ manager.first_name }} {{ manager.last_name }}
                </div>
              </div>
              <div class="view-block">
                <div class="view-header">
                  Organization
                </div>
                <div class="view-value">
                  {{ manager.o_name }}
                </div>
              </div>
              <div class="view-block">
                <div class="view-header">
                  Description
                </div>
                <div class="view-value">
                  {{ manager.o_description }}
                </div>
              </div>

            </div>
          </div>

          <div class="table-container">
            <header>Permissions</header>
            <table>
              <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
              </tr>
              </thead>
              <tbody>
              {% if roles is defined %}
                {% for row in roles %}
                  <tr>
                    <td>{{ row.role_id }}</td>
                    <td>{{ row.name }}</td>
                    <td>{{ row.description }}</td>
                  </tr>
                {% endfor %}
              {% endif %}
              </tbody>
            </table>
          </div>
        {% endif %}
      </div>
    </div>
  {% endblock %}
