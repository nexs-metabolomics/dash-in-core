{% extends "00index.volt" %}
  {% block set_params %}
    {{ set_title("Select Organizations") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("admin_modules/system/admin/00partials/nds_manager_menu_partial",["current":"create"]) }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/admin/00partials/00_nds_admin_menu_partial") }}
  {% endblock %}
  {% block header2 %}
    <header>System / Administration / {{ get_title(false) }}</header>
  {% endblock %}
  {% block content %}
    <div class="nds-form-container">
      {% if user is defined %}
        <form action="/admin/manager/selectorg/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
          <div class="nds-view-area">
            <div class="nds-description">
              Selected user
            </div>
            <div class="nds-description">
              {{ user.first_name }} {{ user.last_name }}
            </div>
            <div class="nds-fieldset">
              <button name="btn[resetuser]" class="nds-button nds-small">Reset user</button>
            </div>
          </div>
        </form>
      {% endif %}
    </div>
    <div class="nds-table-container">
      {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

      {{ partial("00element_partials/nds_table_nav_search_form",[
        "nav_link":"admin/manager/selectorg",
        "act_link":"admin/manager/selectorg",
        "reset_link":"admin/manager/selectorg/1/1",
        "has_results":has_results
      ]) }}

      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {% if pagination is defined %}
          <form action="/admin/manager/selectorg/{{ _ls_querystring_ }}" accept-charset="utf-8" class="search-form" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
            {% for row in pagination.items %}
              <tr>
                <td>
                  <a href="/admin/organization/view/{{ row.organization_id }}">{{ row.name }}</a>
                </td>
                <td>{{ row.description }}</td>
                <td style="text-align: right">
                  <button name="btn[select]" value="{{ row.organization_id }}" class="nds-button mds-small">Select</button>
                </td>
              </tr>
            {% endfor %}
          </form>
        {% endif %}
        </tbody>
      </table>
    </div>

  {% endblock %}
