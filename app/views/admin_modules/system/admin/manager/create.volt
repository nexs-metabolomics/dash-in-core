{% extends "admin_modules/system/admin/manager/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Create Manager") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/manager/00_tabs_partial",["current_tab":"create"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/manager/create/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another user","select_user","form1",user_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another organization","select_organization","form1",organization_info|default(null)) }}

    <button form="form1" name="btn[select_user]" class="button select">Select user</button>
    <button form="form1" name="btn[select_org]" class="button select">Select organization</button>
  {% endblock %}

  {% block page_content %}

    <div class="table-container card-container form-container">

      
      <div class="card">
          {% if user is not defined %}

            <div class="nds-row">
              <div class="nds-column">
                You must select a user
              </div>
            </div>

          {% else %}

            <div class="card-column">
              <div class="card-field-header">
                User name
              </div>
              <div class="card-field-content">
                {{ user.name }}
              </div>
            </div>

          {% endif %}

          {% if organization is not defined %}

            <div class="nds-row">
              <div class="nds-column">
              </div>
              <div class="nds-column" style="margin-left: 2rem">
              </div>
            </div>

          {% else %}

            <div class="card-column">
              <div class="card-field-header">
                Organization
              </div>
              <div class="card-field-content">
                {{ organization.name }}
              </div>
              <div class="card-field-content">
                {{ organization.description }}
              </div>
            </div>

          {% endif %}

          <header>
            Select manager permissions
          </header>

          {% if form is defined and organization is defined and user is defined %}
            {{ form([form.getAction(),"accept-charset":"utf-8"]) }}
            {% for e in form %}

              {% if e.getAttribute('required') %}
                {% set required_asterisk = " * " %}
                {% set required_popup = ' title="Required field"' %}
              {% else %}
                {% set required_asterisk = "" %}
                {% set required_popup = "" %}
              {% endif %}

              <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
                {% if e.getLabel()!="" %}
                  {% if e.getUserOption("type")=="select" %}
                    <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                  {% else %}
                    <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                  {% endif %}
                {% endif %}
                {{ e.render() }}
              </div>
            {% endfor %}

            <div class="form-buttonset">
              <button id="btn[submit]" name="btn[submit]" class="button submit">
                Create
              </button>
              <button id="btn[cancel]" name="btn[cancel]" class="button cancel">
                Cancel
              </button>
            </div>
            {{ endForm() }}
          {% endif %}
      </div>
    </div>
  {% endblock %}
