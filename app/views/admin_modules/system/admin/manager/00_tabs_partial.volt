<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/admin/manager/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create", "/admin/manager/create", "create", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/admin/manager/edit", "edit", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("View", "/admin/manager/view", "view", current_tab, _ls_querystring_) }}
  
</div>
