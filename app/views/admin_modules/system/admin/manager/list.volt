{% extends "admin_modules/system/admin/manager/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List Managers") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/manager/00_tabs_partial",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/manager/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}


    <div class="card-container">
      {% if pagination is defined %}
        {% for row in pagination.items %}
          <div class="card hover-hide-action">
            <div class="flex-row">

              <div class="card-column">
                <div class="card-field-content">
                  User
                </div>
                <div class="card-fieldset-header">
                  {{ row.first_name }} {{ row.last_name }}
                </div>
              </div>
              <div class="card-column">
                <div class="card-field-content">
                  Organization
                </div>
                <div class="card-fieldset-header">
                  {{ row.o_name }}
                </div>
              </div>
            </div>

            <form action="/admin/manager/list/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ m_card_button_box_confirm(
                row.organization_id ~ "/" ~ row.user_id,
                row.confirm|default(null),
                [
                  ["edit","edit-icon48-filled","button edit icon24","Edit user",ROLE_ORG_ALLOW_WRITE],
                  ["view","view-icon48-filled","button view icon24","View user"]
                ],
                [
                  ["confirm","Confirm","button confirm","Confirm delete user",ROLE_ORG_ALLOW_WRITE],
                  ["cancel", "Cancel", "button cancel","Cancel delete user"]
                ],
                "Do you really want to delete this user"
              ) }}
              {#                  ,["delete","delete-icon48-outline","button delete icon24","Delete organization"] #}

            </form>
            {% if row.roles %}
              <table>
                <thead>
                  <tr>
                    <th style="text-align: right;padding-right: 8px">role_id</th>
                    <th style="text-align: left">name</th>
                    <th style="text-align: left">description</th>
                  </tr>
                </thead>
                <tbody>
                  {% for role in row.roles %}
                    <tr>
                      <td style="text-align: right;padding-right: 8px">
                        {{ role.id }}
                      </td>
                      <td>
                        {{ role.name }}
                      </td>
                      <td>
                        {{ role.description }}
                      </td>
                    </tr>
                  {% endfor %}
                </tbody>
              </table>
            {% endif %}

          </div>
        {% endfor %}
      {% endif %}
    </div>


    <div class="table-container">

      <table>
        <thead>
          <tr>
            <th>Actions</th>
            <th>Name</th>
            <th>Email</th>
            <th>Organization</th>
          </tr>
        </thead>
        <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>
                  <div class="nds-row">
                    <button form="form1" name="btn[edit]" value="{{ row.organization_id }}/{{ row.user_id }}" class="nds-button nds-small">Edit</button>
                    <button form="form1" name="btn[view]" value="{{ row.organization_id }}/{{ row.user_id }}" class="nds-button nds-small">View</button>
                  </div>
                </td>
                <td><a href="/admin/user/view/{{ localsession.getQuerystring('?') }}">{{ row.first_name }} {{ row.last_name }}</a></td>
                <td>{{ row.email }}</td>
                <td><a href="/admin/organization/view/{{ localsession.getQuerystring('?') }}">{{ row.o_name }}</a></td>
              </tr>
            {% endfor %}
          {% endif %}
        </tbody>
      </table>

    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"admin/manager/list",
        "reset_link":"admin/manager/list/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
