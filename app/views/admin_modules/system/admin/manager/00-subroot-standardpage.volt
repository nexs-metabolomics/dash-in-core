{% extends "layouts/root-standardpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("admin_modules/system/00menu/module_menu_wide",['submenu_name':'sysadmin',"show_submenu":1,"current":"mngrindex"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("admin_modules/system/00menu/module_menu_narrow",['submenu_name':'sysadmin',"show_submenu":1,"current":"mngrindex"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-sysadmin">
        {{ partial("00img_partials/v1/icons/sysadmin-logo.svg") }}
      </div>
      <a href="/admin/index/{{ _ls_querystring_ }}">Sysadmin</a>
    </div>
  {% endblock %}

