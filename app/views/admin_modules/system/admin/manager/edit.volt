{% extends "admin_modules/system/admin/manager/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit Manager") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/manager/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/manager/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_user]" class="button select">Select user</button>
    <button form="form1" name="btn[select_org]" class="button select">Select organization</button>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <div class="card">

        <div class="nds-row">
          {% if user is defined %}User: {{ user.first_name }} {{ user.last_name }}{% endif %}
        </div>
        <div class="nds-row">
          {% if organization is defined %}Organization: {{ organization.name }}{% endif %}
        </div>

        <div class="nds-form-container">
          {% if manager is defined %}

            <header>
              Add or remove manager permissions
            </header>

            {% if form is defined %}
              {{ form([form.getAction(),"accept-charset":"utf-8","class":"columns"]) }}

              {% for e in form %}

                {% if e.getAttribute('required') %}
                  {% set required_asterisk = " * " %}
                  {% set required_popup = ' title="Required field"' %}
                {% else %}
                  {% set required_asterisk = "" %}
                  {% set required_popup = "" %}
                {% endif %}

                <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
                  {% if e.getLabel()!="" %}
                    {% if e.getUserOption("type")=="select" %}
                      <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                    {% else %}
                      <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                    {% endif %}
                  {% endif %}
                  {{ e.render() }}
                </div>
              {% endfor %}

              <div class="nds-card-button-row">
                <button id="btn[ok]" name="btn[ok]" class="nds-button nds-hollow">
                  Ok
                </button>
                <button id="btn[submit]" name="btn[submit]" class="nds-button nds-hollow">
                  Apply
                </button>
                <a href="/admin/manager/view/{{ manager.organization_id }}/{{ manager.user_id }}" class="nds-button nds-hollow">Cancel</a>
              </div>

              {{ endForm() }}
            {% endif %}
          {% endif %}
        </div>
      </div>
    </div>
  {% endblock %}
