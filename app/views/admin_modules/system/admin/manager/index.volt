{% extends "layouts/basepage-index.volt" %}
  {% block set_params %}
    {{ set_title("Managers") }}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_mngr":1,"current":"mngrindex"]) }}
  {% endblock %}
  {% block content2 %}
    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/admin/manager/create/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Create
              </header>
              <p>Create manager</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/manager/edit/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit
              </header>
              <p>Edit manager</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/manager/view/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                View
              </header>
              <p>View manager</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/admin/manager/list/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List
              </header>
              <p>List managers</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
