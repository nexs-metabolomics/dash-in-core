{% extends "00index.volt" %}
  {% block set_params %}
    {{ set_title("Select User") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("admin_modules/system/admin/00partials/nds_manager_menu_partial",["current":"create"]) }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/admin/00partials/00_nds_admin_menu_partial") }}
  {% endblock %}
  {% block header2 %}
    <header>System / Administration / {{ get_title(false) }}</header>
  {% endblock %}
  {% block content %}
    <div class="nds-form-container">
      {% if organization is defined %}
        <form action="/admin/manager/selectuser/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
          <div class="nds-view-area">
            <div class="nds-description">
              Selected orgainzation
            </div>
            <div class="nds-description">
              {{ organization.name }}
            </div>
            <div class="nds-description">
              {{ organization.description }}
            </div>
            <div class="nds-fieldset">
              <button name="btn[resetorg]" class="nds-button nds-small">Reset organization</button>
            </div>
          </div>
        </form>
      {% endif %}
    </div>
    <div class="nds-table-container">
      {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

      {{ partial("00element_partials/nds_table_nav_search_form",[
        "nav_link":"admin/manager/selectorg",
        "act_link":"admin/manager/selectorg",
        "reset_link":"admin/manager/selectorg/1/1",
        "has_results":has_results
      ]) }}

      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Code/Id</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {% if pagination is defined %}
          <form action="/admin/manager/selectuser/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
            {% for row in pagination.items %}
              <tr>
                <td>
                  <a href="/admin/user/view/{{ row.user_id }}">{{ row.first_name }} {{ row.last_name }}</a>
                </td>
                <td>{{ row.email }}</td>
                <td>{{ row.code }}</td>
                <td class="actions">
                  <button name="btn[select]" value="{{ row.user_id }}" class="nds-button nds-small">Select</button>
                </td>
              </tr>
            {% endfor %}
          </form>
        {% endif %}
        </tbody>
      </table>

    </div>
  {% endblock %}
