{% extends "admin_modules/system/admin/organization/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View Organization") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/organization/00_tabs_partial_new",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/organization/view/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another organization","select_org","form1",organization|default(null)) }}
  {% endblock %}

  {% block page_content %}

    {#    {{ m_info_element("Organization", organization|default(false),"name") }} #}

    {% if organization is not defined %}

      {{ m_emptypage_filler("Please select an organization", "Select organization", "select_org", "form1") }}

    {% else %}

      <div class="table-container card-container">
        <div class="card">

          <div class="card-column">
            <div class="card-field-header">
              {{ organization.name }}
            </div>
            <div class="card-field-content">
              {{ organization.description }}
            </div>
          </div>

          <div class="card-column">
            <div class="card-field-header">
              Email
            </div>
            <div class="card-field-content">
              {{ organization.email }}
            </div>
          </div>

        </div>

        <div class="table-container card-container">

          {% if roles is defined %}

            <div class="card">
              {#              <input type="checkbox" class="view-element" checked id="view-roles"> #}
              {#              <label for="view-roles">Organization roles</label> #}
              <header>Organization permissions</header>
              <table class="element-to-hide">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  {% for row in roles %}
                    <tr>
                      <td>{{ row.role_id }}</td>
                      <td>{{ row.name }}</td>
                      <td>{{ row.description }}</td>
                    </tr>
                  {% endfor %}
                </tbody>
              </table>
            </div>
          {% endif %}
          {% if managers is defined %}
            <div class="card">
              {#              <input type="checkbox" class="view-element" id="view-managers"> #}
              {#              <label for="view-managers">Managers</label> #}
              <header>Managers</header>
              <div class="row">
                <button form="form1" name="btn[addmgr]" class="button">Add manager</button>
              </div>
              <table class="element-to-hide">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  {% for row in managers %}
                    <tr>
                      <td>{{ row.first_name }} {{ row.last_name }}</td>
                      <td>{{ row.email }}</td>
                    </tr>
                  {% endfor %}
                </tbody>
              </table>
            </div>
          {% endif %}
        </div>

      </div>
    {% endif %}
  {% endblock %}
