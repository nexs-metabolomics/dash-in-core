{% extends "admin_modules/system/admin/organization/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Create Organization") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/organization/00_tabs_partial_new",["current_tab":"create"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/organization/create/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}
    
    {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

  {% endblock %}

  {% block footer %}
  {% endblock %}
