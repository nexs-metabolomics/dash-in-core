{% extends "admin_modules/system/admin/organization/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit Organization Permissions") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/organization/00_tabs_partial_new",["current_tab":"editroles"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/organization/editroles/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another organization","select_org","form1",organization|default(null)) }}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <div class="card">
        <header>{{ organization.name }}</header>
        <header>{{ organization.description }}</header>

      </div>
    </div>

    {% if organization is not defined %}

      {{ m_emptypage_filler("Please select an organization", "Select organization", "select_org", "form1") }}

    {% else %}

      <form id="form1" action="/admin/organization/editroles/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
      <div style="display: flex;flex-flow: column">
        <div class="card-container">
          <header>Assigned permissions</header>
          <div class="card">
            <div class="table-container">
              <table class="striped">

                <thead>
                  <tr>
                    <th style="width: 5rem">Action</th>
                    <th>Name</th>
                    <th>Description</th>
                  </tr>
                </thead>

                <tbody>
                  {% if selected_roles is defined %}
                    {% for row in selected_roles %}
                      <tr>
                        <td>
                          <button form="form1" name="btn[rmrole]" value="{{ row.role_id }}" class="button table-action icon alert" title="Remove permission">
                            {{ partial("00img_partials/v1/icons/minus-icon.svg") }}
                          </button>
                        </td>
                        <td>
                          {{ row.name }}
                        </td>
                        <td>
                          {{ row.description }}
                        </td>
                      </tr>
                    {% endfor %}
                  {% endif %}
                </tbody>

              </table>
            </div>
          </div>

          <header>Available permissions</header>
          <div class="card">
            <div class="table-container">
              {% if unselected_roles is defined %}
                <table class="striped">

                  <thead>
                    <tr>
                      <th style="width: 5rem">Action</th>
                      <th>Name</th>
                      <th>Description</th>
                    </tr>
                  </thead>

                  <tbody>
                    {% for row in unselected_roles %}
                      <tr>
                        <td>
                          <button form="form1" name="btn[addrole]" value="{{ row.role_id }}" class="button table-action icon" title="Add permission">
                            {{ partial("00img_partials/v1/icons/plus-icon.svg") }}
                          </button>
                        </td>
                        <td>{{ row.name }}</td>
                        <td>{{ row.description }}</td>
                      </tr>
                    {% endfor %}
                  </tbody>

                </table>
              {% endif %}
            </div>
          </div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
