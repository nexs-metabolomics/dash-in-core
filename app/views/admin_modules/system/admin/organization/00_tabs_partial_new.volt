<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/admin/organization/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create", "/admin/organization/create", "create", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/admin/organization/edit", "edit", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Permissions", "/admin/organization/editroles", "editroles", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("View", "/admin/organization/view", "view", current_tab, _ls_querystring_) }}
  
</div>
