<div class="nds-tabs-row" style="margin-bottom: 1rem">
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "list" %} current{% endif %}">
    <a href="/admin/organization/list/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "create" %} current{% endif %}">
    <a href="/admin/organization/create/{{ _ls_querystring_ }}">
      Create
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "edit" %} current{% endif %}">
    <a href="/admin/organization/edit/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "editroles" %} current{% endif %}">
    <a href="/admin/organization/editroles/{{ _ls_querystring_ }}">
      Permissions
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "view" %} current{% endif %}">
    <a href="/admin/organization/view/{{ _ls_querystring_ }}">
      View
    </a>
  </div>

</div>
