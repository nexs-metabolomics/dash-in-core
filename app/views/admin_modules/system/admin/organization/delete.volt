{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Delete Organization") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("admin_modules/system/00partials/00_nds_system_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_org":1,"current":"orgindex"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <header>
      "Delete" not implemented - coming soon
    </header>
  {% endblock %}
