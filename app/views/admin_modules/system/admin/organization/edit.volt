{% extends "admin_modules/system/admin/organization/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit Organization") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/organization/00_tabs_partial_new",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/organization/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_org]" class="button">Select organization</button>
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

  {% endblock %}
