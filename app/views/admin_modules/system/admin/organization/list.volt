{% extends "admin_modules/system/admin/organization/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List Organizations") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("admin_modules/system/admin/organization/00_tabs_partial_new",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/admin/organization/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      {% if pagination is defined %}
        {% for row in pagination.items %}

          {% if row.confirm is defined and row.confirm === true %}
            {% set hover_action = " warning" %}
          {% else %}
            {% set hover_action = " hover-hide-action" %}
          {% endif %}

          <div class="card{{ hover_action }}" id="rowno{{ loop.index }}">

            <div class="card-column">
              <div class="card-fieldset-header">
                {{ row.name }}
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <form action="/admin/organization/list/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ m_card_button_box_confirm(
                row.organization_id,
                row.confirm|default(null),
                [
                  ["edit","edit-icon48-filled","button edit icon24","Edit organization",ROLE_ORG_ALLOW_WRITE],
                  ["view","view-icon48-filled","button delete icon24","Delete organization"]
                ],
                [
                  ["confirm","Confirm","button confirm","Confirm delete organization",ROLE_ORG_ALLOW_WRITE],
                  ["cancel", "Cancel", "button cancel","Cancel delete organization"]
                ],
                "Do you really want to delete this organization?"
              ) }}
{#                  ,["delete","delete-icon48-outline","button delete icon24","Delete organization"]#}


            </form>
          </div>

        {% endfor %}
      {% endif %}
    </div>


  {% endblock %}

  {% block subfooter_content %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"admin/organization/list",
        "reset_link":"admin/organization/list/1/1",
        "has_results":has_results
      ]) }}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
