<ul class="nds-submenu">

  <li class="main{% if current is defined and current === "orgindex" %} current{% endif %}">
    <a href="/admin/organization/index/{{ _ls_querystring_ }}">Organizations</a>
  </li>
  {% if show_submenu_admin_org is defined and show_submenu_admin_org %}
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/admin/organization/list/{{ _ls_querystring_ }}">List</a>
    </li>
  {% endif %}

  <li class="main{% if current is defined and current === "userindex" %} current{% endif %}">
    <a href="/admin/user/index/{{ _ls_querystring_ }}">Users</a>
  </li>
  {% if show_submenu_admin_user is defined and show_submenu_admin_user %}
    <li class="sub{% if current is defined and current === "create" %} current{% endif %}">
      <a href="/admin/user/create/{{ _ls_querystring_ }}">Create</a>
    </li>
    <li class="sub{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/admin/user/edit/{{ _ls_querystring_ }}">Editxxxxxxx</a>
    </li>
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/admin/user/list/{{ _ls_querystring_ }}">List</a>
    </li>
  {% endif %}

  <li class="main{% if current is defined and current === "mgrindex" %} current{% endif %}">
    <a href="/admin/manager/index/{{ _ls_querystring_ }}">Managers</a>
  </li>
  {% if show_submenu_admin_mngr is defined and show_submenu_admin_mngr %}
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/admin/manager/list/{{ _ls_querystring_ }}">List</a>
    </li>
  {% endif %}
</ul>
