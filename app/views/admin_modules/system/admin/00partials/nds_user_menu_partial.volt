<ul>
  <li class="img-list{% if current is defined and current === "list" %} current{% endif %}" aria-label="List users">
    <a href="/admin/user/list/{{ _ls_querystring_ }}"><span class="tab-text">List</span></a>
  </li>
  <li class="img-create{% if current is defined and current === "create" %} current{% endif %}" aria-label="Add new user">
    <a href="/admin/user/create/{{ _ls_querystring_ }}"><span class="tab-text">Add</span></a>
  </li>
  {% if user_id is defined %}
    <li class="img-edit{% if current is defined and current === "edit" %} current{% endif %}" aria-label="Edit user">
      <a href="/admin/user/edit/{{ user_id }}"><span class="tab-text">Edit</span></a>
    </li>
    <li class="img-view{% if current is defined and current === "view" %} current{% endif %}" aria-label="View user">
      <a href="/admin/user/view/{{ user_id }}"><span class="tab-text">View</span></a>
    </li>
  {% else %}
    <li class="nds-disabled" aria-label="Edit user">
      <span class="">Edit user</span>
    </li>
    <li class="nds-disabled" aria-label="View user">
      <span class="">View user</span>
    </li>
  {% endif %}
</ul>
