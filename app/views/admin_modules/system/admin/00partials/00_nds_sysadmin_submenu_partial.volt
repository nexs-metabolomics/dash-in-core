<ul class="nds-submenu">


  <li class="head{% if current is defined and current === "adminindex" %} current{% endif %}">
    <a href="/admin/index/{{ _ls_querystring_ }}">Admin</a>
  </li>

  {% if show_submenu_admin_role is defined and show_submenu_admin_role %}
    <li class="sub{% if current is defined and current === "list" %} current{% endif %}">
      <a href="/sysadmin/role/list/{{ _ls_querystring_ }}">List roles</a>
    </li>
    <li class="sub{% if current is defined and current === "view" %} current{% endif %}">
      <a href="/sysadmin/role/view/1/{{ _ls_querystring_ }}">View role</a>
    </li>
    <li class="sub{% if current is defined and current === "editrules" %} current{% endif %}">
      <a href="/sysadmin/roleacl/list/{{ _ls_querystring_ }}">Rules</a>
    </li>
  {% endif %}

</ul>

