<nav class="columns medium-12 large-9 menu-nav">

  <ul class="row menu-box-container">

    <li class="columns small-12 large-1 menu-box">
      <a href="/system/index/{{ _ls_querystring_ }}">
        <div class="info-group-inner">
          {{ partial("00img_partials/home-house-svg") }}
        </div>
      </a>
    </li>

    <li class="columns small-12 medium-6 large-2 menu-box">
      <a href="/admin/organization/list/{{ _ls_querystring_ }}">
        <div class="info-group-inner">
          <p style="font-size: 150%;font-weight: bold">Organizations</p>
          <p>Administrate organizations</p>
        </div>
      </a>
    </li>

    <li class="columns small-12 medium-6 large-2 menu-box">
      <a href="/admin/user/list/{{ _ls_querystring_ }}">
        <div class="info-group-inner">
          <p style="font-size: 150%;font-weight: bold">Users</p>
          <p>User account administration</p>

        </div>
      </a>
    </li>

    <li class="columns small-12 medium-6 large-2 menu-box">
      <a href="/admin/manager/list/{{ _ls_querystring_ }}">
        <div class="info-group-inner">
          <p style="font-size: 150%;font-weight: bold">Managers</p>
          <p>Add, remove managers</p>

        </div>
      </a>
    </li>

  </ul>

</nav>

