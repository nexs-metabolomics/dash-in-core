<ul>

  <li class="img-list{% if current is defined and current === "list" %} current{% endif %}" aria-label="List managers">
    <a href="/admin/manager/list/{{ _ls_querystring_ }}"><span class="tab-text">List</span></a>
  </li>
  <li class="img-create{% if current is defined and current === "create" %} current{% endif %}" aria-label="Create a new managers">
    <a href="/admin/manager/create/{{ _ls_querystring_ }}"><span class="tab-text">Create</span></a>
  </li>

  {% if manager is defined %}
    <li class="½img-edit{% if current is defined and current === "edit" %} current{% endif %}">
      <a href="/admin/manager/edit/{{ manager.organization_id }}/{{ manager.user_id }}"><span class="tab-text">Edit</span></a>
    </li>
    <li class="img-view{% if current is defined and current === "view" %} current{% endif %}">
      <a href="/admin/manager/view/{{ manager.organization_id }}/{{ manager.user_id }}"><span class="tab-text">View</span></a>
    </li>
  {% else %}
    <li class="nds-disabled">
      <span class="">Edit</span>
    </li>
    <li class="nds-disabled">
      <span class="">View</span>
    </li>
  {% endif %}

</ul>
