<ul class="nds-submenu">

  <li class="head{% if current is defined and current === "orgindex" %} current{% endif %}">
    <a href="/admin/organization/list/{{ _ls_querystring_ }}">Organizations</a>
  </li>

  <li class="head{% if current is defined and current === "userindex" %} current{% endif %}">
    <a href="/admin/user/list/{{ _ls_querystring_ }}">Users</a>
  </li>

  <li class="head{% if current is defined and current === "mngrindex" %} current{% endif %}">
    <a href="/admin/manager/list/{{ _ls_querystring_ }}">Managers</a>
  </li>
</ul>
