{% extends "00index.volt" %}
  {% block set_params %}
    {{ set_title("Confirm new account") }}
  {% endblock %}


    {% block header %}
    {% endblock %}
    {% block menu_start %}
    {% endblock %}
    {% block menu1 %}
    {% endblock %}
    {% block menu_separator %}{% endblock %}
    {% block menu2 %}{% endblock %}
    {% block menu_end %}{% endblock %}

  {% block header2 %}
  {% endblock %}
  {% block content %}
    {% if form is defined %}
      <div class="nds-login-container">
        <form action="/confirmnew/" accept-charset="utf-8" method="post">
          <div class="nds-fieldset">
            <header>Confirm new password</header>
          </div>
          {% for e in form %}

            {% if e.getAttribute('required') %}
              {% set required_asterisk = " * " %}
              {% set required_popup = ' title="Required field"' %}
            {% else %}
              {% set required_asterisk = "" %}
              {% set required_popup = "" %}
            {% endif %}

            <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
              {% if e.getLabel()!="" %}
                {% if e.getUserOption("type")=="select" %}
                  <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                {% else %}
                  <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                {% endif %}
              {% endif %}
              {{ e.render() }}
            </div>
          {% endfor %}
          <div class="nds-fieldset">
            <button id="btn[submit]" name="btn[submit]" class="nds-button nds-expanded nds-small">
              Ok
            </button>
          </div>
          <div class="nds-fieldset">
            <a href="/" class="nds-button nds-expanded nds-small">Cancel</a>
          </div>
        </form>
      </div>
    {% endif %}
  {% endblock %}
{# <div class="page-content"> #}
  {# <main class="row"> #}
    {# <section class="columns"> #}
      {# {{ set_title("Confirm new account") }} #}

      {# <article class="columns small-12"> #}
        {# <div class="columns"> #}
          {# {% if form is defined %} #}
            {# {{ form([form.getAction(),"accept-charset":"utf-8","class":"small-12 columns"]) }} #}
            {# {% for e in form %} #}
              {# <div class="fieldset row"> #}
                {# <div class="columns"> #}
                  {# {% if e.getLabel()!="" %} #}
                    {# {% if e.getUserOption("type")=="select" %} #}
                      {# <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label> #}
                    {# {% else %} #}
                      {# <label for="{{ e.getName() }}">{{ e.getLabel() }}</label> #}
                    {# {% endif %} #}
                  {# {% endif %} #}
                  {# {{ e.render() }} #}
                {# </div> #}
              {# </div> #}
            {# {% endfor %} #}
            {# <div class="fieldset row"> #}
              {# <div class="columns small-2"> #}
                {# <button id="btn[submit]" name="btn[submit]" class="button expanded"><span #}
                      {# class="btn-txt">Ok</span></button> #}
              {# </div> #}
              {# <div class="columns small-2"> #}
                {# <a href="/sysadmin/role/list/{{ _ls_querystring_ }}">Cancel</a> #}
              {# </div> #}
            {# </div> #}
            {# {{ endForm() }} #}
          {# {% endif %} #}

        {# </div> #}
      {# </article> #}

    {# </section> #}
  {# </main> #}
{# </div> #}
