{% extends "layouts/base-loginpage.volt" %}
  {% block set_params %}
    {{ set_title("Login") }}
  {% endblock %}



  {% block page_content %}
    {% if form is defined %}
      <div class="login-container">
        <div class="login-container-inner">

          <div class="logo-container">
            <div class="logo-img">
{#              {{ partial("00img_partials/v1/icons/site-icon-color.svg") }}#}
              {{ partial("00img_partials/v1/icons/squidr-logo-text-black.svg") }}
            </div>
          </div>
          <header>Login</header>

          <form id="form1" accept-charset="utf-8" method="post"></form>

          <label for="username">User code</label>
          <div class="form-fieldset">
            <div class="form-field">
              <input form="form1" type="text" id="username" name="username" placeholder="User code" autofocus="1"/>
            </div>
          </div>

          <label for="password">Password</label>
          <div class="form-fieldset">
            <div class="form-field">
              <input form="form1" type="password" id="password" name="password" placeholder="Password"/>
            </div>
          </div>

          <div class="form-fieldset">
            <div class="form-field">
              <button form="form1" id="btn[submit]" name="btn[submit]" class="button login">
                Continue
              </button>
            </div>
          </div>

          <div class="form-fieldset">
            <div class="form-field">
              <a href="/resetpw/" class="login reset-pw">Forgot your password?</a>
            </div>
          </div>

        </div>
      </div>
    {% endif %}
  {% endblock %}
