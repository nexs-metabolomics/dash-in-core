{% extends "00index.volt" %}
  {% block set_params %}
    {{ set_title("Login") }}
  {% endblock %}

  {% block header %}{% endblock %}
  {% block menu_start %}{% endblock %}
  {% block menu1 %}{% endblock %}
  {% block menu_separator %}{% endblock %}
  {% block menu2 %}{% endblock %}
  {% block menu_end %}{% endblock %}
  {% block header2 %}{% endblock %}

  {% block content %}
    {% if form is defined %}
      <div class="nds-login-container">
        <div class="nds-fieldset">
          <header>Create password</header>
        </div>
        {{ form([form.getAction(),"accept-charset":"utf-8"]) }}
        {% for e in form %}
          <div class="nds-fieldset">
            {{ e.render() }}
          </div>
        {% endfor %}
        <div class="nds-fieldset">
          <button id="btn[submit]" name="btn[submit]" class="nds-button nds-expanded nds-small">
            Ok
          </button>
        </div>
        <div class="nds-fieldset">
          <a href="/login/" class="nds-button nds-expanded nds-small">Cancel</a>
        </div>
        {{ endForm() }}
      </div>
    {% endif %}
  {% endblock %}
{#<div class="page-content login">#}
  {#<main class="row">#}
    {#<section class="columns">#}
      {#{{ set_title("Confirm account") }}#}
      {#<article class="columns small-12 logo">#}
        {#<div class="columns">#}
          {#<a href="/">#}
            {#<img src="/img/dsp3-100x100.png">#}
          {#</a>#}
        {#</div>#}
      {#</article>#}

      {#<article class="columns small-12 login-container">#}
        {#<div class="columns small-12 medium-6 large-4">#}
          {#Create password#}
          {#{% if form is defined %}#}
            {#{{ form([form.getAction(),"accept-charset":"utf-8"]) }}#}
            {#{% for e in form %}#}
              {#<div class="fieldset row">#}
                {#<div class="columns">#}
                  {#{{ e.render() }}#}
                {#</div>#}
              {#</div>#}
            {#{% endfor %}#}
            {#<div class="fieldset row">#}
              {#<div class="columns">#}
                {#<button id="btn[submit]" name="btn[submit]" class="button expanded">#}
                  {#<span class="btn-txt">Ok</span>#}
                {#</button>#}
              {#</div>#}
            {#</div>#}
            {#<div class="row fieldset">#}
              {#<div class="columns">#}
                {#<a href="/login/" class="button expanded">Cancel</a>#}
              {#</div>#}
            {#</div>#}
            {#{{ endForm() }}#}
          {#{% endif %}#}

        {#</div>#}
      {#</article>#}

    {#</section>#}
  {#</main>#}
{#</div> #}
