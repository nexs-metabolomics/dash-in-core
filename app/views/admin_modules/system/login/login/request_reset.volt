{% extends "layouts/base-loginpage.volt" %}
  {% block set_params %}
    {{ set_title("Reset password") }}
  {% endblock %}

  {% block page_content %}
    {% if form is defined %}
      <div class="login-container">
        <div class="login-container-inner">
          <div class="logo-container">
            <div class="logo-img">
              {{ partial("00img_partials/v1/icons/squidr-logo-text-black.svg") }}
            </div>
          </div>
          <header>Reset password</header>

          <form id="form1" accept-charset="utf-8" method="post"></form>
          {{ form_innerloop_standard(form,[],"form1") }}
          <div class="form-fieldset">
            <div class="form-field">
              <button form="form1" id="btn[submit]" name="btn[submit]" class="button login">
                Reset
              </button>
              <a href="/login/" class="button login">
                Cancel
              </a>
            </div>
          </div>

        </div>
      </div>
    {% endif %}

  {% endblock %}
