{%- macro m_studydesign_menu_tabentry(tab_text, uri, tab_name, imgsrc, current_tab, _ls_querystring_, show=true) %}

  {% if show %}
    {#    <div style="min-width: 10px;min-height: 10px;max-height: 10px;border-radius: 5PX;background-color: #ff2d3e"></div> #}
    {#  {% else %} #}
    <a href="{{ uri }}/{{ _ls_querystring_ }}"
       title="{{ tab_text }}"
       class="{% if current_tab is defined and tab_name === current_tab %} current{% endif %}">
      <img src="{{ imgsrc }}">
    </a>
  {% endif %}

{%- endmacro %}

