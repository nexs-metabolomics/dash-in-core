{# ====================================================================================== #}
{# macro: create a menu entry - no associated submenu (and no "active"-marking            #}
{# ====================================================================================== #}
{# @param menu_text: the link text shown in the menu                                      #}
{# @param uri: the link                                                                   #}
{# @param icon_path: path to partial containing the icon svg code                         #}
{# @param _ls_querystring_: the variable for "local_session"                              #}
{# ====================================================================================== #}
{%- macro m_menu_entry_no_submenu(menu_text, uri, icon_path, _ls_querystring_, show=true) %}

  <li class="main">
    {% if show %}
      {#      <div style="min-width: 10px;max-width: 10px;min-height: 10px;max-height: 10px;border-radius: 5px;background-color: #ff2d3e"></div> #}
      <a class="menu-entry" href={{ uri }}/{{ _ls_querystring_ }}">
        {{ partial(icon_path) }}
        {{ menu_text }}
      </a>
    {% endif %}
  </li>

{%- endmacro %}
