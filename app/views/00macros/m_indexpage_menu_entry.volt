{# ====================================================================================== #}
{# macro: create menu entry on index page                                                 #}
{# ====================================================================================== #}
{%- macro m_indexpage_menu_entry(main_text, section_class, uri, icon_path, _ls_querystring_, a_id="") %}

  <li>
    <a id="{{ a_id }}" href="{{ uri }}/{{ _ls_querystring_ }}">

      <div class="box-menu-item-icon {{ section_class }}">
        {{ partial(icon_path) }}
      </div>

      <div class="box-menu-item-caption">
        <header>
          {{ main_text }}
        </header>
      </div>

    </a>
  </li>

{%- endmacro %}

