{# ====================================================================================== #}
{# macro: create a submenu entry                                                          #}
{# ====================================================================================== #}
{# @param menu_text: the link text shown in the menu                                      #}
{# @param uri: the link                                                                   #}
{# @param current: indicator whether to mark entry as in focus                            #}
{# @param _ls_querystring_: the variable for "local_session"                              #}
{# @param current_text: the text to match against "current"                               #}
{# ====================================================================================== #}
{%- macro m_menu_submenuentry(menu_text, uri, _ls_querystring_, current="" ,current_text="",show=true) %}
  {% if show %}
    {#    <div style="min-width: 10px;max-width: 10px;min-height: 10px;max-height: 10px;border-radius: 5px;background-color: #ff2d3e"></div> #}
    <li class="head{% if current is defined and current === current_text %} current{% endif %}">
      <a href="{{ uri }}/{{ _ls_querystring_ }}">{{ menu_text }}</a>
    </li>
  {% endif %}
{%- endmacro %}
