{%- macro m_conditional_genericbutton(btn_text, btn_name, form_id, class, condition_obj=null, title=null) %}

  {% if condition_obj != null %}
    {% if title != null %}
      <button form="{{ form_id }}" class="{{ class }}" name="btn[{{ btn_name }}]" title="{{ title }}">{{ btn_text }}</button>
    {% else %}
      <button form="{{ form_id }}" class="{{ class }}" name="btn[{{ btn_name }}]">{{ btn_text }}</button>
    {% endif %}
  {% endif %}

{%- endmacro %}
