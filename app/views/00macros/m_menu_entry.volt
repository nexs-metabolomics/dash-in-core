{# ====================================================================================== #}
{# macro: create a menu entry including the submenus related to that entry                #}
{# ====================================================================================== #}
{# @param menu_text: the link text shown in the menu                                      #}
{# @param name_key: name of the entry                                                     #}
{# @param submenu_name: name of the entry from link                                       #}
{#                    show submenu when name_key and submenu_name match                   #}
{# @param uri: the link                                                                   #}
{# @param submenu_path: path to partial containing the submenus                           #}
{# @param icon_path: path to partial containing the icon svg code                         #}
{# @param _ls_querystring_: the variable for "local_session"                              #}
{# @param show_submenu (default: null). Toggle submenus when user clicks active menu      #}
{#        Either null:     don't mark entry                                               #}
{#               false(0): mark entry don't show submenus                                 #}
{#               true(1):  mark entry, show submenus                                      #}
{# ====================================================================================== #}
{%- macro m_menu_entry(menu_text, name_key, submenu_name, uri, submenu_path, icon_path, _ls_querystring_, show_submenu=null,show=true) %}


{#  {% if _ls_querystring_ == "" %}{% set _ls_querystring_ = null %}{% endif %}#}
  {% if name_key == submenu_name %}


    <li class="main current">
      {% if show %}
        {#  <div style="min-width: 10px;max-width: 10px;min-height: 10px;max-height: 10px;border-radius: 5px;background-color: #ff2d3e"></div> #}
        <a class="menu-entry" href="{{ uri }}/{{ show_submenu }}/{{ _ls_querystring_ }}">
          {{ partial(icon_path) }}
          {{ menu_text }}
        </a>
      {% endif %}
    </li>

    {% if show_submenu==1 %}
      {{ partial(submenu_path) }}
    {% endif %}

  {% else %}

    <li class="main">
      {% if show %}
        {#  <div style="min-width: 10px;max-width: 10px;min-height: 10px;max-height: 10px;border-radius: 5px;background-color: #ff2d3e"></div> #}
        <a class="menu-entry" href={{ uri }}/{{ _ls_querystring_ }}">
          {{ partial(icon_path) }}
          {{ menu_text }}
        </a>
      {% endif %}
    </li>

  {% endif %}

{%- endmacro %}
