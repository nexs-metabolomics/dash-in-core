{%- macro m_card_button_box_confirm(row_id=null ,confirm=null ,button_set_default=null ,button_set_confirm=null ,confirm_message=null) %}

  <div class="card-button-box-hide-outer">
    <div class="card-button-box-hide-inner">
      {{ partial("00img_partials/v1/icon48/dotsvert-icon48-filled.svg") }}
    </div>
  </div>

  <div class="card-button-box">
    {% if confirm is defined and confirm === true %}

      {% if button_set_confirm is defined and button_set_confirm %}
        <div class="confirm-delete-msg">
          {{ confirm_message }}
        </div>
        {% for btn in button_set_confirm %}
          {% if (btn[4] is defined and not btn[4]) %}
{#            <div style="height: 10px;min-height: 10px;min-width: 10px;max-width: 10px;background-color: #ff2d3e"></div>#}
          {% else %}
            <button name="btn[{{ btn[0] }}]" value="{{ row_id }}" class="{{ btn[2] }}"{% if btn[3] is defined %} title="{{ btn[3] }}"{% endif %}>
              {{ btn[1] }}
            </button>
          {% endif %}
        {% endfor %}
      {% endif %}

    {% else %}

      {% if button_set_default is defined and button_set_default %}
        {% for btn in button_set_default %}
          {% if (btn[4] is defined and not btn[4]) %}
{#            <div style="height: 10px;min-height: 10px;min-width: 10px;max-width: 10px;background-color: #ff2d3e"></div>#}
          {% else %}
            <button name="btn[{{ btn[0] }}]" value="{{ row_id }}" class="{{ btn[2] }}"{% if btn[3] is defined %} title="{{ btn[3] }}"{% endif %}>
              {{ partial("00img_partials/v1/icon48/" ~ btn[1] ~ ".svg") }}
            </button>
          {% endif %}
        {% endfor %}
      {% endif %}

    {% endif %}
  </div>

{%- endmacro %}
