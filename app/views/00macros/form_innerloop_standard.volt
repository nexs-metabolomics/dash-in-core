{#
================================================================== 
   Renders fields in a form object, adds buttons at the end
   @param form: form object
   @param button_set: array of arrays: [name, formid, css-class string]
==================================================================
#}
{%- macro form_innerloop_standard(form,button_set=null,form_id=null) %}

  <div class="form-container">
    {% for e in form %}

      {% if e.getAttribute('required') %}
        {% set required_asterisk = " * " %}
        {% set required_popup = ' title="Required field"' %}
      {% else %}
        {% set required_asterisk = "" %}
        {% set required_popup = "" %}
      {% endif %}

      <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
        {{ renderMessages(e) }}

        {% if e.getLabel()!="" %}
          {% if e.getUserOption("type")=="select" %}
            <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
          {% else %}
            <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
          {% endif %}
        {% endif %}

        <div class="form-field">

          {% if form_id !== null %}
            {{ e.render(["form":form_id]) }}
          {% else %}
            {{ e.render() }}
          {% endif %}

        </div>
      </div>

    {% endfor %}

    {% if button_set is defined and button_set %}
      <div class="form-buttonset">
        {% if form_id !== null %}

          {% for btn in button_set %}
            <button form="{{ form_id }}" name="btn[{{ btn[0] }}]" class="{{ btn[2] }}">
              {{ btn[1] }}
            </button>
            {% if contains(btn[2],"has-spinner") %}

              <div class="spinner-container">
                <div class="spinner"></div>
              </div>

            {% endif %}
          {% endfor %}

        {% else %}

          {% for btn in button_set %}
            <button name="btn[{{ btn[0] }}]" class="{{ btn[2] }}">
              {{ btn[1] }}
            </button>
          {% endfor %}

        {% endif %}
      </div>
    {% endif %}
  </div>

{%- endmacro %}
