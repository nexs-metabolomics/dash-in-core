{%- macro m_study_description(info_object, description_length=0,dot_url="") %}

  <div class="card-container">

    <div class="card">

      <div class="card-column">
        <div class="card-field-header">
          {{ info_object.name }}
        </div>
      </div>

      <div class="card-column">
        <div class="card-fieldset-header">
          {{ info_object.title }}
        </div>
      </div>

      <div class="card-column">
        <div class="card-field-content">
          {% if description_length>0 and info_object.description|length > description_length %}
            {% if dot_url %}
            {{ info_object.description|slice(0,description_length) }} <a href="{{ dot_url }}">...</a>
            {% else %}
            {{ info_object.description|slice(0,description_length) }}...
            {% endif %}
          {% else %}
            {{ info_object.description }}
          {% endif %}
        </div>
      </div>

    </div>
  </div>

{%- endmacro %}