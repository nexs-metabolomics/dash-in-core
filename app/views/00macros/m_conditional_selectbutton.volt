{%- macro m_conditional_selectbutton(btn_text, btn_name, form_id, condition_obj=null) %}

  {% if condition_obj != null %}
    <button form="{{ form_id }}" class="button select" name="btn[{{ btn_name }}]">{{ btn_text }}</button>
  {% endif %}

{%- endmacro %}
