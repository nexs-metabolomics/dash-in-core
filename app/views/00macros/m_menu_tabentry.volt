{%- macro m_menu_tabentry(tab_text, uri,tab_name, current_tab, _ls_querystring_, show=true) %}

  {% if show %}
    {#    <div style="min-width: 10px;min-height: 10px;max-height: 10px;background-color: #ff2d3e"></div> #}
    <div class="tab{% if current_tab is defined and tab_name === current_tab %} current{% endif %}">
      <a href="{{ uri }}/{{ _ls_querystring_ }}">
        {{ tab_text }}
      </a>
    </div>
  {% endif %}

{%- endmacro %}

