{%- macro m_info_element(info_text, info_obj=null,entry="name",len=15) %}

  {% if info_obj %}
    {% set info_arr = to_array(info_obj) %}
    <div class="info-element" title="{{ info_arr[entry] }}">
      <div class="info-element-header">
        {{ info_text }}
      </div>
      <div class="info-element-content">
        {{ strcut(info_arr[entry],len) }}
      </div>
    </div>
  {% endif %}

{%- endmacro %}

