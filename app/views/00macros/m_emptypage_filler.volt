{%- macro m_emptypage_filler(header, btn_text, btn_name, form_id) %}

  <div class="card-container">
    <div class="card emptypage">
      <div class="emptypage-container">

        <header>
          {{ header }}
        </header>

        <button form="{{ form_id }}" class="button select" name="btn[{{ btn_name }}]">{{ btn_text }}</button>

      </div>
    </div>
  </div>

{%- endmacro %}
