{% if _ls_querystring_ is not defined %}{% set _ls_querystring_ = "" %}{% endif %}
<div class="nds-table-searchbar">
  {% if pagination is defined %}
    {{ partial("00element_partials/nds_table_nav_results") }}
  {% else %}
    {{ partial("00element_partials/nds_table_nav_noresults") }}
  {% endif %}
  {% if search_term is defined %}
    {% set input_value_term = 'value="' ~ search_term ~ '" ' %}
    {% set class_term_present = " term-present" %}
  {% else %}
    {% set input_value_term = "" %}
    {% set class_term_present = "" %}
  {% endif %}
  <div class="nds-search-form-wrapper">
    <form action="/{{ act_link }}/{{ _ls_querystring_ }}" accept-charset="utf-8" class="nds-search-form" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
      <div class="nds-fieldset">
        <input name="search_term" {{ input_value_term }}type="text" class="columns search_term" style="border-right:none;height: 2rem">
        <a href="/{{ reset_link }}/{{ _ls_querystring_ }}" class="nds-reset-link{{ class_term_present }}">X

          {#<svg version="1.1" viewBox="0 0 100 100" id="svg3" fill="white">#}
            {#<g>#}
              {#<polygon#}
                  {#points=#}
                  {#"0,15 15,0 50,35 85,0 100,15 65,50 100,85 85,100 50,65 15,100 0,85 35,50"#}
                  {#id="x2"/>#}
            {#</g>#}
          {#</svg>#}

        </a>
        <button name="btn[search]" class="nds-button nds-filter" style="font-weight: bold">Filter</button>
      </div>
    </form>
  </div>
</div>
