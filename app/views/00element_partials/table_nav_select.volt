<div style="display: flex;flex-flow: row;justify-content: space-between;width: 100%">

  {% if has_results is defined and has_results %}
    {{ partial("00element_partials/table_nav_select_results") }}
  {% else %}
    {{ partial("00element_partials/table_nav_select_noresults") }}
  {% endif %}

  <div style="display: flex;flex-flow: row;max-height: 24px">
    <a href="/{{ reset_link }}/{{ _ls_querystring_ }}" style="display: flex;justify-content: center;align-items: center;background-color: #ff1826;padding: 0 4px">
      <svg width="1.2em" height="1.2em" viewBox="0 0 100 100" id="svg3" fill="white" style="display: flex">
        <g>
          <polygon
              points=
              "0,15 15,0 50,35 85,0 100,15 65,50 100,85 85,100 50,65 15,100 0,85 35,50"
              id="x2"/>
        </g>
      </svg>

    </a>
    <input form="form1" name="search_term" type="text" {% if search_term is defined %}value="{{ search_term }}"{% endif %} style="display: flex">
    <button form="form1" type="submit" name="btn[search]" style="display: flex;flex-flow: row;align-items: center;justify-content: center">Filter</button>
  </div>
</div>
