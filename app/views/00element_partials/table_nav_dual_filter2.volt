<div class="table-nav-filter-column">

  {% if has_results is defined and has_results %}
    {{ partial("00element_partials/table_nav_dual_filter2_results") }}
  {% else %}
    {{ partial("00element_partials/table_nav_dual_filter_noresults") }}
  {% endif %}

  <div class="table-filter-container" style="">

    <button form="{{ form_id }}" name="btn[{{ reset_name }}]" class="button filter reset" title="reset search term">
      <svg width="1.2em" height="1.2em" viewBox="0 0 100 100" id="svg3" style="display: flex">
        <g>
          <polygon
              points=
              "0,15 15,0 50,35 85,0 100,15 65,50 100,85 85,100 50,65 15,100 0,85 35,50"
              id="x2"/>
        </g>
      </svg>
    </button>

    <input form="{{ form_id }}" name="search_term{{ p }}" type="text" placeholder="Type search term" class="filter" {% if search_term is defined %}value="{{ search_term }}"{% endif %}>
    <button form="{{ form_id }}" type="submit" name="btn[search]" class="button filter" style="display: flex;flex-flow: row;align-items: center;justify-content: center;cursor: pointer">Filter</button>
  </div>
</div>
