  <nav>
    <ul>
      <li>
        <a href="/{{ nav_link }}">
          {{ partial("00img_partials/triangle-up-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}">
          {{ partial("00img_partials/triangle-up-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}">
          {{ partial("00img_partials/triangle-down-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}">
          {{ partial("00img_partials/triangle-down-double-svg") }}
        </a>
      </li>
      <li class="pg-of-pgs">
        page 0 of 0
      </li>
    </ul>
  </nav>

