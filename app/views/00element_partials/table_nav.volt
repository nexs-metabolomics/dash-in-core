              <nav class="pagination">
                <div class="card-row">
                  <div class="nav-links" style="display: flex;flex-flow: row nowrap;font-family: Roboto;font-size: 150%">
                    <a href="/{{ nav_link }}/{{ pagination.first }}" style="margin: 0 16px">
                      &lt;&lt;
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.previous }}" style="margin: 0 16px">
                      &lt;
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.next }}" style="margin: 0 16px">
                      &gt;
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.last }}" style="margin: 0 16px">
                      &gt;&gt;
                    </a>
                  </div>
                  <div class="pg-of-pgs" style="margin-left: auto">
                    page {{ pagination.current }} of {{ pagination.last }}
                  </div>
                </div>
              </nav>
