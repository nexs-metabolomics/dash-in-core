{% if form is defined %}
  {{ form([form.getAction(),"accept-charset":"utf-8","class":"columns"]) }}
  {% for e in form %}

    {% if e.getAttribute('required') %}
      {% set required_asterisk = " * " %}
      {% set required_popup = ' title="Required field"' %}
    {% else %}
      {% set required_asterisk = "" %}
      {% set required_popup = "" %}
    {% endif %}

    <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
      {% if e.getLabel()!="" %}
        {% if e.getUserOption("type")=="select" %}
          <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
        {% else %}
          <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
        {% endif %}
      {% endif %}
      {{ e.render() }}
    </div>
  {% endfor %}
  <div class="nds-btn-group">
    <a href="/{{ cancel_link }}" class="nds-button nds-small nds-expanded">Cancel</a>
    <button id="btn[submit]" name="btn[submit]" class="nds-button nds-small nds-expanded">
      Ok
    </button>
  </div>
  {{ endForm() }}
{% endif %}
