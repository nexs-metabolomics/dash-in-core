<div class="table-nav-container">
  <div class="table-navbar">
    <nav>
      <ul>
        <li>
          <a href="/{{ nav_link }}/0/0/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-first-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/0/0/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-prev-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/0/0/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-next-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/0/0/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-last-icon.svg") }}
          </a>
        </li>
        <li class="pg-of-pgs">
          page 0 of 0
        </li>
      </ul>
    </nav>
  </div>
</div>
