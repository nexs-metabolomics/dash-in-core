{% if p === 1 %}
  {% set p1_first    = pagination1.first %}
  {% set p1_last     = pagination1.last %}
  {% set p1_next     = pagination1.next %}
  {% set p1_previous = pagination1.previous %}
  {% set pg_current  = pagination1.current %}
  {% set pg_last     = pagination1.last %}
  
  {% set p2_first    = pagination2.current %}
  {% set p2_last     = pagination2.current %}
  {% set p2_next     = pagination2.current %}
  {% set p2_previous = pagination2.current %}
{% elseif p === 2 %}
  {% set p1_first    = pagination1.current %}
  {% set p1_last     = pagination1.current %}
  {% set p1_next     = pagination1.current %}
  {% set p1_previous = pagination1.current %}
  
  {% set p2_first    = pagination2.first %}
  {% set p2_last     = pagination2.last %}
  {% set p2_next     = pagination2.next %}
  {% set p2_previous = pagination2.previous %}
  {% set pg_current  = pagination2.current %}
  {% set pg_last     = pagination2.last %}
{% endif %}
<div class="table-nav-container" style="display: flex;flex-flow: column;width: 100%">
  <div class="table-navbar">
    <nav>
      <ul>
        <li>
          <a href="/{{ nav_link }}/{{ p1_first }}/{{ p2_first }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-first-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ p1_previous }}/{{ p2_previous }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-prev-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ p1_next }}/{{ p2_next }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-next-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ p1_last }}/{{ p2_last }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-last-icon.svg") }}
          </a>
        </li>
        <li class="pg-of-pgs">
          page {{ pg_current }} of {{ pg_last }}
        </li>
      </ul>
    </nav>
  </div>
</div>
