<div class="nds-table-searchbar">
  <nav>
    <ul class="nds-nav-double-row">
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.first }}/{{ pagination_cols.current }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-up-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.previous }}/{{ pagination_cols.current }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-up-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.next }}/{{ pagination_cols.current }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-down-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.last }}/{{ pagination_cols.current }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-down-double-svg") }}
        </a>
      </li>
    </ul>
    <ul class="nds-nav-double-col">
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.first }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-left-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.previous }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-left-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.next }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-right-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.last }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-right-double-svg") }}
        </a>
      </li>
    </ul>
    <ul class="nds-nav-double-pgs">
      <li class="pg-of-pgs">
        rows {{ pagination_rows.page_item_first }} to {{ pagination_rows.page_item_last }} of {{ pagination_rows.total_items }}
      </li>
      <li class="pg-of-pgs">
        cols {{ pagination_cols.page_item_first }} to {{ pagination_cols.page_item_last }} of {{ pagination_cols.total_items }}
      </li>
    </ul>

  </nav>
</div>
