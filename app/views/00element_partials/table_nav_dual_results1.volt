<div class="navbar-container dualpane-left">
  <div class="table-navbar">
    <nav>
      <ul>
        <li>
          <a href="/{{ nav_link }}/{{ pagination1.first }}/{{ pagination2.current }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-double-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination1.previous }}/{{ pagination2.current }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination1.next }}/{{ pagination2.current }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination1.last }}/{{ pagination2.current }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-double-svg") }}
          </a>
        </li>
        <li class="pg-of-pgs">
          page {{ pagination1.current }} of {{ pagination1.last }}
        </li>
      </ul>
    </nav>
  </div>

  <button form="{{ form_id }}" type="submit" name="btn[search1]" style="display: block;position: absolute;left: -100000px">Filter</button>
  <div style="display: flex;flex-flow: row;width: 100%">
    {#    <a href="/{{ reset_link }}/{{ _ls_querystring_ }}" style="display: flex;justify-content: center;align-items: center;background-color: #ff1826;padding: 0 4px"> #}
    <button form="{{ form_id }}" name="btn[reset1]" class="button filter reset" title="Reset search term">
      <svg width="1.2em" height="1.2em" viewBox="0 0 100 100" id="svg3" fill="white" style="display: flex;stroke: black;fill: #0d8eff">
        <g>
          <polygon
              points=
              "0,15 15,0 50,35 85,0 100,15 65,50 100,85 85,100 50,65 15,100 0,85 35,50"
              id="x2"/>
        </g>
      </svg>
    </button>
    {#    </a> #}
    <input form="{{ form_id }}" name="search_term1" type="text" {% if search_term is defined %}value="{{ search_term }}"{% endif %} style="display: flex;width: 100%">
    <button form="{{ form_id }}" type="submit" name="btn[search1]"class="button filter">Filter</button>
  </div>
</div>
