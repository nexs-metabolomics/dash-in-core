<div class="form-container">
  {% for e in form %}

    {% if e.getAttribute('required') %}
      {% set required_asterisk = " * " %}
      {% set required_popup = ' title="Required field"' %}
    {% else %}
      {% set required_asterisk = "" %}
      {% set required_popup = "" %}
    {% endif %}

    <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
      {% if e.getLabel()!="" %}
        {% if e.getUserOption("type")=="select" %}
          <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
        {% else %}
          <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
        {% endif %}
      {% endif %}
      <div class="form-field">
        {{ e.render() }}
      </div>
    </div>
  {% endfor %}

  {% if button_set is defined and button_set %}
    <div class="form-buttonset">
      {% for name,text in button_set %}
        <button name="btn[{{ name }}]" class="button hollow">
          {{ text }}
        </button>
      {% endfor %}
    </div>
  {% endif %}
</div>
