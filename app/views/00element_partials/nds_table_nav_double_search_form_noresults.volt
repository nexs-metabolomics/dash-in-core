<div class="nds-table-searchbar">
  <nav>
    <ul class="nds-nav-double-row">
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-up-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-up-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-down-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-down-double-svg") }}
        </a>
      </li>
    </ul>
    <ul class="nds-nav-double-col">
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-left-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-left-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-right-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ _ls_querystring_ }}/">
          {{ partial("00img_partials/triangle-right-double-svg") }}
        </a>
      </li>
    </ul>

    <ul class="nds-nav-double-pgs">
      <li class="pg-of-pgs">
        rows 0 to 0 of 0
      </li>
      <li class="pg-of-pgs">
        cols 0 to 0 of 0
      </li>
    </ul>

  </nav>
</div>
