{% if has_results is defined and has_results %}
  {{ partial("00element_partials/nds_table_nav_double_search_form_results") }}
{% else %}
  {{ partial("00element_partials/nds_table_nav_double_search_form_noresults") }}
{% endif %}
