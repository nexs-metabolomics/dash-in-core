              <nav class="pagination small-12 columns">
                <div class="inner-wrap row">
                  <div class="nav-links">
                    <a href="/{{ nav_link }}/{{ pagination_rows.first }}/{{ pagination_cols.current }}">
                      {{ partial("00img_partials/triangle-up-double-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.previous }}/{{ pagination_cols.current }}">
                      {{ partial("00img_partials/triangle-up-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.next }}/{{ pagination_cols.current }}">
                      {{ partial("00img_partials/triangle-down-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.last }}/{{ pagination_cols.current }}">
                      {{ partial("00img_partials/triangle-down-double-svg") }}
                    </a>
                  </div>
                  <div class="pg-of-pgs">
                    rows {{ pagination_rows.page_item_first }} to {{ pagination_rows.page_item_last }}  of {{ pagination_rows.total_items }}
                  </div>

                  <div class="column"></div>

                  <div class="pg-of-pgs">
                    cols {{ pagination_cols.page_item_first }} to {{ pagination_cols.page_item_last }}  of {{ pagination_cols.total_items }}
                  </div>
                  <div class="nav-links">
                    <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.first }}">
                      {{ partial("00img_partials/triangle-left-double-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.previous }}">
                      {{ partial("00img_partials/triangle-left-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.next }}">
                      {{ partial("00img_partials/triangle-right-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination_rows.current }}/{{ pagination_cols.last }}">
                      {{ partial("00img_partials/triangle-right-double-svg") }}
                    </a>
                  </div>
              
                </div>
              </nav>
