              {% if item_name is not defined %}{% set item_name="item" %}{% endif %}
              <nav class="pagination small-12 columns">
                <div class="inner-wrap row">
                  <div class="nav-links">
                    <a href="/{{ nav_link }}/{{ pagination.first }}">
                      {{ partial("00img_partials/triangle-up-double-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.previous }}">
                      {{ partial("00img_partials/triangle-up-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.next }}">
                      {{ partial("00img_partials/triangle-down-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/{{ pagination.last }}">
                      {{ partial("00img_partials/triangle-down-double-svg") }}
                    </a>
                  </div>
                  <div class="pg-of-pgs">
                    {{ item_name }} {{ pagination.page_item_first }} to {{ pagination.page_item_last }} of {{ pagination.total_items }}
                  </div>
                </div>
              </nav>
