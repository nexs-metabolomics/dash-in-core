  <nav>
    <ul>
      <li>
        <a href="/{{ nav_link }}/{{ pagination.first }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-up-double-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination.previous }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-up-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination.next }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-down-svg") }}
        </a>
      </li>
      <li>
        <a href="/{{ nav_link }}/{{ pagination.last }}/{{ _ls_querystring_ }}">
          {{ partial("00img_partials/triangle-down-double-svg") }}
        </a>
      </li>
      <li class="pg-of-pgs">
        page {{ pagination.current }} of {{ pagination.last }}
      </li>
    </ul>
  </nav>
