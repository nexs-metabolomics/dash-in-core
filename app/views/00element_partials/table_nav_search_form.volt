              <nav class="pagination small-12 columns">
                <div class="inner-wrap row">
                  <div class="nav-links">
                    <a href="/{{ nav_link }}/">
                      {{ partial("00img_partials/triangle-up-double-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/">
                      {{ partial("00img_partials/triangle-up-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/">
                      {{ partial("00img_partials/triangle-down-svg") }}
                    </a>
                    <a href="/{{ nav_link }}/">
                      {{ partial("00img_partials/triangle-down-double-svg") }}
                    </a>
                  </div>
                  <div class="pg-of-pgs">
                    page {{ pagination.current }} of {{ pagination.last }}
                  </div>
              
                  <div class="columns large-expand small-12 search-form-wrapper">
                    <div class="columns search-form-inner-wrapper">
                      <form action="/{{ act_link }}/" accept-charset="utf-8" class="columns search-form" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
                        <div class="fieldset row">
                          <a href="/{{ reset_link }}" class="button small alert">
                            <svg version="1.1" width="1.2em" height="1.2em" viewBox="0 0 100 100" id="svg3" fill="white">
                              <g>
                                <polygon
                                    points=
                                    "0,15 15,0 50,35 85,0 100,15 65,50 100,85 85,100 50,65 15,100 0,85 35,50"
                                    id="x2"/>
                              </g>
                            </svg>

                          </a>
                          <input name="search_term" type="text" {% if search_term is defined %}value="{{ search_term }}"{% endif %} class="columns search_term">
                          <button name="btn[search]" class="button small">Search</button>
                        </div>
                      </form>
                    </div>
                  </div>
              
                </div>
              </nav>
