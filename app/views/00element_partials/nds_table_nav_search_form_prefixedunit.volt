<div class="nds-table-searchbar">
  {% if pagination is defined %}
    {{ partial("00element_partials/nds_table_nav_results") }}
  {% else %}
    {{ partial("00element_partials/nds_table_nav_noresults") }}
  {% endif %}
  {% if search_term is defined %}
    {% set input_value_term = 'value="' ~ search_term ~ '" ' %}
    {% set class_term_present = " term-present" %}
  {% else %}
    {% set input_value_term = "" %}
    {% set class_term_present = "" %}
  {% endif %}
  <form id="form-search" action="/{{ act_link }}/" accept-charset="utf-8" class="nds-search-form" method="post"></form>
  <form id="form-valid" action="/{{ act_link }}/" accept-charset="utf-8" class="nds-search-form" method="post"></form>
  <div class="nds-search-form-wrapper">
    <div class="nds-fieldset">
      <div style="white-space: nowrap">Filter by valid</div>
      {% if showvalid === 2 %}
        <button form="form-valid" name="btn[showall]" class="nds-button-invisible stretch" tabindex="">
          {{ partial("00img_partials/icons/checkbox-checked-24px.svg") }}
        </button>
      {% else %}
        <button form="form-valid" name="btn[showvalid]" class="nds-button-invisible stretch" tabindex="">
          {{ partial("00img_partials/icons/checkbox-unchecked-24px.svg") }}
        </button>
      {% endif %}
      <input form="form-search" name="search_term" {{ input_value_term }}type="text" class="columns search_term" style="border-right:none;height: 2rem">
      <a href="/{{ reset_link }}" class="nds-reset-link{{ class_term_present }}">X</a>
      <button form="form-search" name="btn[search]" type="submit" class="nds-button nds-filter" style="font-weight: bold" tabindex="0">Filter</button>
    </div>
  </div>
</div>
