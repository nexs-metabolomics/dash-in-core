<div style="display: flex;flex-flow: column;width: 100%">
  <div class="table-navbar">
    <nav>
      <ul>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.first }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-first-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.previous }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-prev-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.next }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-next-icon.svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.last }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/v1/icons/nav-last-icon.svg") }}
          </a>
        </li>
        <li class="pg-of-pgs">
          page {{ pagination.current }} of {{ pagination.last }}
        </li>
      </ul>
    </nav>
  </div>
</div>
