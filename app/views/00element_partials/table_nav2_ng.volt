<div class="table-navbar">
  <nav>
    <ul>
      {% if pagination is defined %}
        {% if item_name is not defined %}{% set item_name="item" %}{% endif %}
        <li>
          <a href="/{{ nav_link }}/{{ pagination.first }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-double-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.previous }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.next }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/{{ pagination.last }}/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-double-svg") }}
          </a>
        </li>
        <li class="pg-of-pgs" style="white-space: nowrap">
          {{ item_name }} {{ pagination.page_item_first }} to {{ pagination.page_item_last }} of {{ pagination.total_items }}
        </li>
      {% else %}
        <li>
          <a href="/{{ nav_link }}/1/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-double-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/1/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-up-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/1/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-svg") }}
          </a>
        </li>
        <li>
          <a href="/{{ nav_link }}/1/{{ _ls_querystring_ }}">
            {{ partial("00img_partials/triangle-down-double-svg") }}
          </a>
        </li>
        <li class="pg-of-pgs">
          {{ item_name }} 0 of 0
        </li>
      {% endif %}
    </ul>
  </nav>
</div>
