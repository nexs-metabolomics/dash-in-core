<div class="nds-frontpage-logo-wrapper">
  <div class="nds-site-logo-container-light">
    <a class="nds-site-logo-link" href="/">
{#      <img id="c" src="/img/logo/dashin-logo-1.png">#}
      <img id="c" src="/img/logo/logo-white-on-plack.png">
    </a>
  </div>
</div>
<div class="nds-other-links">
  <a href="/about/" class="nds-button no-color nds-big" style="color: #e3f2fd">About</a>
  {% if IS_LOGGED_IN is defined and IS_LOGGED_IN %}
    <a href="/welcome/" class="nds-button nds-big">Enter</a>
  {% else %}
    <a href="/login/" onclick="" class="nds-button nds-big">
      <div class="user-login">
        Login
      </div>
    </a>
  {% endif %}
</div>
{# <div class="nds-dropdown-menu-container"> #}
{# {% if IS_LOGGED_IN is defined and IS_LOGGED_IN %} #}
{# {{ partial("00partials/00_nds_main_header_user_menu_partial") }} #}
{# {% else %} #}
{# <a href="/login/" onclick=""> #}
{# <div class="user-login"> #}
{# Login #}
{# </div> #}
{# </a> #}
{# {% endif %} #}
{# </div> #}
