<div class="fp-logo-container">
  <a href="/" class="fp-logo-link">
{#    <img src="/img/logo/logo-white-on-plack.png" class="fp-logo-img">#}
    {{ partial("00img_partials/v1/icons/squidr-logo-text-black.svg") }}
{#<div style="display:flex;flex-flow: row nowrap;height: 42px;max-height: 42px">#}
{#</div>    #}
  </a>
</div>
<div class="fp-login-container">
  {% if IS_LOGGED_IN is defined and IS_LOGGED_IN %}
    <a href="/welcome/" class="fp-user-link">
      <div class="user-link">
      {{ partial("00img_partials/v1/icons/account.svg") }}
        <span>Enter</span>
      </div>
    </a>
  {% else %}
    <a href="/login/" class="fp-login-link">
      <div class="user-link">
        {{ partial("00img_partials/v1/icons/account.svg") }}
        <span>Login</span>
      </div>
    </a>
  {% endif %}
</div>