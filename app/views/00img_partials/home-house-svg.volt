<svg height="4.15em" version="1.1" width="100%" viewBox="0 0 20 20" style="margin: auto">
  <g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1">
    <g fill="#000000" id="Core" transform="translate(-506.000000, -255.000000)">
      <g id="home" transform="translate(506.000000, 255.500000)">
        <path d="M8,17 L8,11 L12,11 L12,17 L17,17 L17,9 L20,9 L10,0 L0,9 L3,9 L3,17 L8,17 Z" id="Shape"/>
      </g>
    </g>
  </g>
</svg>
