<svg height="4.15em" class="svg-home-house" width="100%" viewBox="0 0 20 20" style="margin: auto">
  <g class="svg-c-g1" fill-rule="evenodd" id="svg-i-g1" stroke-width="1">
    <g class="svg-c-g2" id="Core" transform="translate(-506.000000, -255.000000)">
      <g class="svg-c-g3" id="home" transform="translate(506.000000, 255.500000)">
        <path d="M8,17 L8,11 L12,11 L12,17 L17,17 L17,9 L20,9 L10,0 L0,9 L3,9 L3,17 L8,17 Z" id="Shape"/>
      </g>
    </g>
  </g>
</svg>
