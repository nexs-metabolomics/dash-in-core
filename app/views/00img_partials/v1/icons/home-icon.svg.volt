<svg class="home-icon-svg"
     viewBox="0 0 75 63.75"
    xmlns="http://www.w3.org/2000/svg">
  <path
      d="m 30,63.75 v -22.5 h 15 v 22.5 h 18.75 v -30 H 75 L 37.5,0 0,33.75 h 11.25 v 30 z" />
</svg>
