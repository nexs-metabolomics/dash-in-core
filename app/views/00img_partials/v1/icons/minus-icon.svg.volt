<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="minus-icon-svg">
  <path d="M0 0h24v24H0z" class="p1"/>
  <path d="M19 13H5v-2h14v2z" class="p2"/>
</svg>