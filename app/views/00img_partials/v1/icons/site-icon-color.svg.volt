<svg viewBox="0 0 31 34" fill="none" xmlns="http://www.w3.org/2000/svg" class="site-icon-color-svg">
<path d="M2 31.5564C8.17886 32.7316 14.7897 31.7782 18.3158 26.1826C21.0408 21.8582 21.5612 14.8219 19.9123 11.9936C18.2633 9.16521 12.7218 3.34348 7.12782 7.82173C2.46617 14.5391 7.65156 17.7036 9.45865 19.4652C13.7523 23.6507 29 17.5089 29 11.9936C29 6.47825 24.2857 2 19.9123 2" stroke="url(#paint0_linear)" stroke-width="4" stroke-linecap="round"/>
<defs>
<linearGradient id="paint0_linear" x1="12.4595" y1="2" x2="13.1297" y2="49.1426" gradientUnits="userSpaceOnUse">
<stop offset="0.127817" stop-color="#058AFF"/>
<stop offset="0.509508" stop-color="#FD9437"/>
<stop offset="0.681885" stop-color="#FDF537"/>
</linearGradient>
</defs>
</svg>
