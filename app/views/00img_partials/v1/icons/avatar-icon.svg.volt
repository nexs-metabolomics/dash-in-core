<svg  viewBox="0 0 157 154"
{#    style="border: #c2276d solid 1px"#}
>
  <g id="profile-round" stroke-width="1" fill-rule="evenodd">
        <ellipse id="Oval-10" fill="#FFFFFF" cx="77.9073028" cy="76.507646" rx="77.9073028" ry="76.507646"></ellipse>
        <g id="Group" fill="#02C1C0">
          <ellipse id="Oval-7" cx="45.6697982" cy="23.7437522" rx="24.1781284" ry="23.7437522"></ellipse>
          <path d="
          M31.752,54.2922607 
          L56.786905,54.2922607 
          C78.2199715,54.2922607 95.594905,71.6671942 95.594905,93.1002607 
          L95.594905,99.2606153 
          L0,99.2606153 L7.10542736e-15,86.0442607 
          C4.95786893e-15,68.5081154 14.2158546,54.2922607 31.752,54.2922607 
          Z"
                id="Rectangle"></path>
        </g>
  </g>
</svg>