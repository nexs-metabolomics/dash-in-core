{#  {{ m_menu_entry("Home", 'home',submenu_name|default(null), "/dashin/index"                  , "00menu/submenus_narrow/useraccount_submenu_narrow"  , "00img_partials/v1/icons/home-icon.svg", _ls_querystring_, show_submenu|default(null)) }}#}
  {{ m_menu_entry("Datasets", 'import',submenu_name|default(null),"/dashin/owner/import/index", "00menu/submenus_narrow/import_submenu_narrow", "00img_partials/v1/icons/dataset-icon.svg", _ls_querystring_, show_submenu|default(null)) }}
  {{ m_menu_entry("Studies", "study", submenu_name|default(null), "/dashin/owner/study/index" , "00menu/submenus_narrow/study_submenu_narrow" , "00img_partials/v1/icons/studies-icon.svg", _ls_querystring_ ,show_submenu|default(null)) }}
  {{ m_menu_entry("Search", "search", submenu_name|default(null),"/dashin/owner/search/index" , "00menu/submenus_narrow/search_submenu_narrow", "00img_partials/v1/icons/search-simple-icon.svg", _ls_querystring_ ,show_submenu|default(null)) }}
  {{ m_menu_entry("Admin", "admin", submenu_name|default(null)   ,"/dashin/owner/admin/index" , "00menu/submenus_narrow/admin_submenu_narrow" , "00img_partials/v1/icons/dataset-icon.svg"       , _ls_querystring_ ,show_submenu|default(null)) }}

  {{ m_menu_entry_no_submenu("System admin", "/admin/index", "00img_partials/v1/icons/dataset-icon.svg", _ls_querystring_,ROLE_ORG_ALLOW_WRITE) }}

