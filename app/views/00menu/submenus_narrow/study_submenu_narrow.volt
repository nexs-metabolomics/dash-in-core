

<ul class="submenu study">
{#  {{ m_menu_submenuentry("Create study"   ,"/dashin/owner/studycreate/createfromdata"        , _ls_querystring_ ,current|default(null),  "studycreate_index") }}#}
  {{ m_menu_submenuentry("Manage studies" ,"/dashin/owner/study/list"                         , _ls_querystring_ ,current|default(null),  "study_index") }}
  {{ m_menu_submenuentry("Study design"   ,"/dashin/owner/studydesign/viewstudydesign"        , _ls_querystring_ ,current|default(null),  "studydesign_index") }}
  {{ m_menu_submenuentry("Data export"    ,"/dashin/owner/studyexport/listconditions" , _ls_querystring_ ,current|default(null),  "datafilters_index") }}
</ul>

