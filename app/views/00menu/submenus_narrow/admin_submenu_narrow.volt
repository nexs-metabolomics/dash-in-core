<ul class="submenu study">

  {{ m_menu_submenuentry("Consortium" ,"/dashin/owner/consort/list", _ls_querystring_ ,current|default(null),  "consort_index") }}
  {{ m_menu_submenuentry("Assay" ,"/dashin/owner/assay/list", _ls_querystring_ ,current|default(null),  "assay_index") }}

  {{ m_menu_submenuentry("Datacolumntype" ,"/dashin/owner/datacolumntype/list", _ls_querystring_ ,current|default(null),  "datacolumntype_index") }}
  {{ m_menu_submenuentry("Prefixed unit" ,"/dashin/owner/prfxunit/listprfxunits", _ls_querystring_ ,current|default(null),  "prefixedunit_index") }}
</ul>
