

{{ m_menu_entry("Home", 'home',submenu_name|default(null), "/welcome", "00menu/submenus_wide/organization_submenu_wide"  , "00img_partials/v1/icons/home-icon.svg"   , _ls_querystring_, show_submenu|default(null)) }}
{{ m_menu_entry("User", "useraccount",submenu_name|default(null), "/user", "00menu/submenus_wide/useraccount_submenu_wide"  , "00img_partials/v1/icons/user-icon.svg"   , _ls_querystring_, show_submenu|default(null)) }}
{{ m_menu_entry("Datasets", 'import',submenu_name|default(null),"/dashin/owner/import/index", "00menu/submenus_wide/import_submenu_wide", "00img_partials/v1/icons/dataset-icon.svg", _ls_querystring_, show_submenu|default(null)) }}
{{ m_menu_entry("Studies", "study", submenu_name|default(null), "/dashin/owner/study/index" , "00menu/submenus_wide/study_submenu_wide" , "00img_partials/v1/icons/studies-icon.svg", _ls_querystring_ ,show_submenu|default(null)) }}
{{ m_menu_entry("Search", "search", submenu_name|default(null),"/dashin/owner/search/index" , "00menu/submenus_wide/search_submenu_wide", "00img_partials/v1/icons/search-simple-icon.svg", _ls_querystring_ ,show_submenu|default(null)) }}
{{ m_menu_entry("Admin", "admin", submenu_name|default(null)   ,"/dashin/owner/admin/index" , "00menu/submenus_wide/admin_submenu_wide" , "00img_partials/v1/icons/dataset-icon.svg"      , _ls_querystring_ ,show_submenu|default(null),ROLE_APP_ADMIN) }}

{{ m_menu_entry_no_submenu("System admin", "/admin/index", "00img_partials/v1/icons/dataset-icon.svg", _ls_querystring_,ROLE_APP_ADMIN) }}

