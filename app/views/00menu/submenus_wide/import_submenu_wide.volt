<ul class="submenu study">

  {{ m_menu_submenuentry("Dataset" ,"/dashin/owner/import/listdatasets", _ls_querystring_ ,current|default(null) , "dataset_index") }}
  {{ m_menu_submenuentry("Dataset import" ,"/dashin/owner/importguide/upload", _ls_querystring_ ,current|default(null) , "importguide_index") }}
  {{ m_menu_submenuentry("Upload varinfo" ,"/dashin/owner/varinfo/listupvardatasets", _ls_querystring_ ,current|default(null) , "upvarinfo_index") }}
  {{ m_menu_submenuentry("Match varinfo" ,"/dashin/owner/varinfo/listvardatasets", _ls_querystring_ ,current|default(null) , "varinfo_index") }}

</ul>

