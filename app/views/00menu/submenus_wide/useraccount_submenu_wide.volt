<ul class="submenu useraccount">

  {{ m_menu_submenuentry("Select role"  ,"/user/view", _ls_querystring_ ,current|default(null), "user_view") }}
  {{ m_menu_submenuentry("Edit profile" ,"/user/edit", _ls_querystring_ ,current|default(null), "user_edit") }}

</ul>
