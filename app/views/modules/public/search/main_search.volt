{% extends "layouts/public/public-base-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Search studies") }}
    <form id="form1" action="/public/search/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[SUBMIT]" class="button hidden-default" style="position: absolute;left: -90000px">Cancel</button>
  {% endblock %}
 
    {% block page_content %}
      {#      <h1> #}
      {#        {{ get_title(true) }} #}
      {#      </h1> #}
      <div class="landing-page-form">
        {{ m_form_innerloop_card(form,[["reset","Reset","button reset"],["submit","Search studies (main)","button submit"]],"form1") }}
      </div>

      {% if search_result is defined %}

        <div class="listview-container">

          {% for row in search_result %}

            <div class="listview-record">

              <div class="listview-row">
                <div class="listview-title">
                  {{ row.title }}
                </div>
              </div>

              <div class="listview-row">
                <div class="listview-text">
                  {{ row.name }}
                </div>
              </div>

              <div class="listview-row">
                <div class="listview-text">
                  {{ row.description }}
                </div>
              </div>

              {# ============================================== #}
              <input form="form1" type="checkbox" class="show-below-the-fold" id="show-more-{{ loop.index0 }}">
              <label for="show-more-{{ loop.index0 }}" class="show-more-checkbox"></label>
              {# ============================================== #}
              
              <div class="listview-row below-the-fold">
                <div class="listview-table-container">
                  <div class="css-table">

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Start date
                      </div>
                      <div class="css-td">
                        {{ row.start_date }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Endpoint
                      </div>
                      <div class="css-td">
                        {{ row.endpoint }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Objectives
                      </div>
                      <div class="css-td">
                        {{ row.objectives }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Conclusion
                      </div>
                      <div class="css-td">
                        {{ row.conclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Exclusion
                      </div>
                      <div class="css-td">
                        {{ row.exclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Inclusion
                      </div>
                      <div class="css-td">
                        {{ row.inclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Inclusion
                      </div>
                      <div class="css-td">
                        {{ row.inclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Country
                      </div>
                      <div class="css-td">
                        {{ row.country_name }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Consortium
                      </div>
                      <div class="css-td">
                        {{ row.consortium_name }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Published (PubMed)
                      </div>
                      <div class="css-td">
                        {{ row.published }}
                      </div>
                    </div>

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Researchdesign
                      </div>
                      <div class="css-td">
                        
                        <div class="css-table">
                          <div class="css-tr">
                            <div class="css-th">
                              Researchdesign
                            </div>
                            <div class="css-th">
                              Blinding
                            </div>
                            <div class="css-th">
                              Blinding method
                            </div>
                          </div>
                          
                          <div class="css-tr">
                            <div class="css-td">
                              {{ row.researchdesign_name }}
                            </div>
                            <div class="css-td">
                              {{ row.blinding }}
                            </div>
                            <div class="css-td">
                              {{ row.blinding_method }}
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Research design description
                      </div>
                      <div class="css-td">
                        {{ row.researchdesign_text }}
                      </div>
                    </div>

                    {# ================================================= #}

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Recruitment
                      </div>

                      <div class="css-td">
                        <div class="css-table">

                          <div class="css-tr">
                            <div class="css-th">
                              Recruitment start year
                            </div>
                            <div class="css-th">
                              Recruitment end year
                            </div>
                            <div class="css-th">
                              Number of volunteers
                            </div>
                            <div class="css-th">
                              Number of volunteers terminated
                            </div>
                          </div>

                          <div class="css-tr">
                            <div class="css-td">
                              {{ row.recruit_start_year }}
                            </div>
                            <div class="css-td">
                              {{ row.recruit_end_year }}
                            </div>
                            <div class="css-td">
                              {{ row.num_volun }}
                            </div>
                            <div class="css-td">
                              {{ row.num_volun_terminated }}
                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                    {# ================================================= #}

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Factors
                      </div>

                      <div class="css-td">
                        <div class="css-table">

                          <div class="css-tr">
                            <div class="css-th">
                              Number of treatments
                            </div>
                            <div class="css-th">
                              Number of factors
                            </div>
                            <div class="css-th">
                              Number of arms
                            </div>
                          </div>

                          <div class="css-tr">
                            <div class="css-td">
                              {{ row.num_treat }}
                            </div>
                            <div class="css-td">
                              {{ row.num_factor }}
                            </div>
                            <div class="css-td">
                              {{ row.num_arm }}
                            </div>
                          </div>

                        </div>
                      </div>

                    </div>
                    {# ================================================= #}

                  </div>
                </div>
              </div>

            </div>

          {% endfor %}

        </div>

      {% endif %}

    {% endblock %}
