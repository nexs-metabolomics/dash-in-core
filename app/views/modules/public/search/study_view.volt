{% extends "layouts/public/public-base-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Search studies") }}
    <form id="form1" action="/public/studyview/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[SUBMIT]" class="button hidden-default" style="position: absolute;left: -90000px">Cancel</button>
  {% endblock %}
 
    {% block page_content %}
      {#      <h1> #}
      {#        {{ get_title(true) }} #}
      {#      </h1> #}

      {% if form is defined %}
        {{ m_form_innerloop_card(form,[["reset","Reset","button reset"],["submit","Search studies (view)","button submit"]],"form1") }}
      {% endif %}


      {% if study_details is defined %}

        <div class="listview-container">

          <div class="listview-record">

            <div class="listview-row">
              <div class="listview-title">
                {{ study_details.title }}
              </div>
            </div>

            <div class="listview-row">
              <div class="listview-text">
                {{ study_details.name }}
              </div>
            </div>

            <div class="listview-row">
              <div class="listview-text">
                {{ study_details.description }}
              </div>
            </div>

            <div class="listview-row" style="border: #22872d solid 4px">
              <button form="form1" type="submit" name="btn[downloadstudy]" class="button submit" style="width: 100%;justify-content: center">Download selected</button>
              <div style="display: flex;margin: 0 0 0 16px">
                <label style="display: flex">Select separator
                </label>
                <select form="form1" name="sep" style="display: flex">
                <option value="1">, (comma)</option>
                <option value="2">; (semicolon)</option>
                <option value="3">'\t' (tab)</option>
                </select>
              </div>
            </div>

            <div class="listview-row" style="border: #872f4e solid 4px">
              <div class="listview-table-container">
                <div class="table-container-column" style="width: 75%">

                  {# === main study info area ===================================================== #}

                  <diV class="css-table">

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Start date
                      </div>
                      <div class="css-td">
                        {{ study_details.start_date }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Endpoint
                      </div>
                      <div class="css-td">
                        {{ study_details.endpoint }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Objectives
                      </div>
                      <div class="css-td">
                        {{ study_details.objectives }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Conclusion
                      </div>
                      <div class="css-td">
                        {{ study_details.conclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Exclusion
                      </div>
                      <div class="css-td">
                        {{ study_details.exclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Inclusion
                      </div>
                      <div class="css-td">
                        {{ study_details.inclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Inclusion
                      </div>
                      <div class="css-td">
                        {{ study_details.inclusion }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Country
                      </div>
                      <div class="css-td">
                        {{ study_details.country_name }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Consortium
                      </div>
                      <div class="css-td">
                        {{ study_details.consortium_name }}
                      </div>
                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Published (PubMed)
                      </div>
                      <div class="css-td">
                        {{ study_details.published }}
                      </div>
                    </div>

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Researchdesign
                      </div>
                      <div class="css-td">

                        <div class="css-table">
                          <div class="css-tr">
                            <div class="css-th">
                              Researchdesign
                            </div>
                            <div class="css-th">
                              Blinding
                            </div>
                            <div class="css-th">
                              Blinding method
                            </div>
                          </div>

                          <div class="css-tr">
                            <div class="css-td">
                              {{ study_details.researchdesign_name }}
                            </div>
                            <div class="css-td">
                              {{ study_details.blinding }}
                            </div>
                            <div class="css-td">
                              {{ study_details.blinding_method }}
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                    <div class="css-tr">
                      <div class="css-th row-header">
                        Research design description
                      </div>
                      <div class="css-td">
                        {{ study_details.researchdesign_text }}
                      </div>
                    </div>

                    {# ================================================= #}

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Recruitment
                      </div>

                      <div class="css-td">
                        <div class="css-table">

                          <div class="css-tr">
                            <div class="css-th">
                              Recruitment start year
                            </div>
                            <div class="css-th">
                              Recruitment end year
                            </div>
                            <div class="css-th">
                              Number of volunteers
                            </div>
                            <div class="css-th">
                              Number of volunteers terminated
                            </div>
                          </div>

                          <div class="css-tr">
                            <div class="css-td">
                              {{ study_details.recruit_start_year }}
                            </div>
                            <div class="css-td">
                              {{ study_details.recruit_end_year }}
                            </div>
                            <div class="css-td">
                              {{ study_details.num_volun }}
                            </div>
                            <div class="css-td">
                              {{ study_details.num_volun_terminated }}
                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                    {# ================================================= #}

                    <div class="css-tr">

                      <div class="css-th row-header">
                        Factors
                      </div>

                      <div class="css-td">
                        <div class="css-table">

                          <div class="css-tr">
                            <div class="css-th">
                              Number of treatments
                            </div>
                            <div class="css-th">
                              Number of factors
                            </div>
                            <div class="css-th">
                              Number of arms
                            </div>
                          </div>

                          <div class="css-tr">
                            <div class="css-td">
                              {{ study_details.num_treat }}
                            </div>
                            <div class="css-td">
                              {{ study_details.num_factor }}
                            </div>
                            <div class="css-td">
                              {{ study_details.num_arm }}
                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                  </diV>

                  {# ==== END == main study info area ============================================= #}

                </div>

                <div class="table-container-column" style="width: 25%;height: 100%;border: #cccccc solid 1px">
                  <div class="table-container-row" style="width: 100%;height: 100%;border: #cccccc solid 1px">
                    <div class="css-table">

                      <div class="css-thead">
                        <div class="css-thr">
                          <div class="css-th">
                            Datasets
                          </div>
                        </div>
                      </div>

                      <div class="css-tbody">
                        {% if study_details.datasets is not defined %}
                          <div class="css-thr">
                            <div class="css-th">
                              There are no available datasets in this study
                            </div>
                          </div>
                        {% else %}
                          {% for row in study_details.datasets %}
                            <div class="css-tr">
                              <div class="css-td">
                                <label style="cursor: pointer">
                                  <input form="form1" type="checkbox" name="chk_dataset[{{ row.dataset_id }}]" checked>
                                  {{ row.name }}
                                </label>
                              </div>
                            </div>
                          {% endfor %}
                        {% endif %}
                      </div>

                    </div>
                  </div>

                  <div class="table-container-row" style="width: 100%;height: 100%;border: #cccccc solid 1px">
                    <div class="css-table">

                      <div class="css-thead">
                        <div class="css-thr">
                          <div class="css-th">
                            Files
                          </div>
                        </div>
                      </div>

                      <div class="css-tbody">
                        {% if study_details.supportfiles is not defined %}
                          <div class="css-thr">
                            <div class="css-th">
                              There are no available files in this study
                            </div>
                          </div>
                        {% else %}
                          {% for row in study_details.supportfiles %}
                            <div class="css-tr">
                              <div class="css-td" title="{{ row.description }}">
                                <label style="cursor: pointer">
                                  <input form="form1" type="checkbox" name="chk_supportfile[{{ row.supportfile_id }}]" checked>
                                  {{ row.name }}
                                </label>
                              </div>
                            </div>
                          {% endfor %}
                        {% endif %}
                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>

      {% endif %}

    {% endblock %}
