{% extends "layouts/root-indexpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",["submenu_name":"home","current":"home_index"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",["submenu_name":"home_index","current":"home_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-home">
        {{ partial("00img_partials/v1/icons/home-icon.svg") }}
      </div>
      <a href="/dashin/index/{{ _ls_querystring_ }}">Home</a>
    </div>
  {% endblock %}

