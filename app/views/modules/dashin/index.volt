{% extends "modules/dashin/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Data sharing") }}
  {% endblock %}

  {% block page_content_k %}
{#    <a href="/user/{{ _ls_querystring_ }}">#}
{#      <div class="user-profile-box">#}
{#        Manage profile#}
{#      </div>#}
{#    </a>#}
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-home">
      <ul>

        {{ m_indexpage_menu_entry("Datasets", "section-dataset","/dashin/owner/import/index","00img_partials/v1/icons/dataset-icon.svg", _ls_querystring_, "home-index-datasets") }}
        {{ m_indexpage_menu_entry("Studies", "section-studies","/dashin/owner/study/index","00img_partials/v1/icons/studies-icon.svg", _ls_querystring_, "home-index-studies") }}
        {{ m_indexpage_menu_entry("Search", "section-search","/dashin/owner/search/index","00img_partials/v1/icons/search-simple-icon.svg", _ls_querystring_, "home-index-search") }}
        {{ m_indexpage_menu_entry("Admin", "section-admin","/dashin/owner/admin/index","00img_partials/v1/icons/admin-icon.svg", _ls_querystring_, "home-index-admin") }}

      </ul>

    </nav>
  {% endblock %}
