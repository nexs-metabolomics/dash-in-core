{% extends "modules/dashin/owner/import/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View dataset design") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"viewdatadesign"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/viewdatadesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if dataset_info is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% endif %}

    {% if dataset_info is defined %}
      <div class="card-container">

        <div class="card">
          
          <div class="card-title">
            {{ get_title(false) }}
          </div>

          <div class="card-row">
            <div class="card-column">
              <div class="card-field-header">
                {{ dataset_info.name }}
              </div>
            </div>
            <div class="card-column">
              <span style="color: #00bb1f">[{{ dataset_info.dataset_id }}]</span>
            </div>
          </div>

          {# ================================================================= #}
          <div class="card-row">

            <div class="card-column">
              <div class="card-field-header">
                Number of rows in dataset
              </div>
              {% if count_full is defined %}
                <div class="card-field-content">
                  {{ count_full }}
                </div>
              {% endif %}
            </div>

            <div class="card-column">
              <div class="card-field-header">
                Unique design values
              </div>
              {% if count_distinct is defined %}
                <div class="card-field-content">
                  {{ count_distinct }}
                </div>
              {% endif %}
            </div>

          </div>

          {# ================================================================= #}
          <div class="card-row">

            {% if design_exists is defined and design_exists === 1 %}

              <div class="card-column">
                <div class="card-field-header">
                  Design already saved
                </div>
              </div>

              <div class="card-column">
                <div class="card-field-content">
                  {% if confirm_delete is defined and confirm_delete %}
                    <button form="form1" name="btn[confirm]" type="submit" class="nds-button nds-alert">Confirm</button>
                  {% else %}
                    <button form="form1" name="btn[delete]" type="submit" class="nds-button nds-hollow nds-alert">Delete</button>
                  {% endif %}
                </div>
              </div>

            {% else %}

              <div class="card-column">
                <div class="card-field-header">
                  Design not saved
                </div>
              </div>

              <div class="card-column">
                <div class="card-field-content">
                  <button form="form1" name="btn[save]" type="submit" class="nds-button">Save</button>
                </div>
              </div>

            {% endif %}

          </div>

          {% if save_result is defined and save_result %}

            <div class="card-row">
              <div class="card-column">
                <table>
                  <tr>
                    <th>Name</th>
                    <th>Affected rows</th>
                    <th>Description</th>
                  </tr>
                  {% for row in save_result %}
                    <tr>
                      <td>{{ row.name }}</td>
                      <td>{{ row.affected_rows }}</td>
                      <td>{{ row.description }}</td>
                    </tr>
                  {% endfor %}
                </table>
              </div>
            </div>

          {% endif %}

          {# ================================================================= #}
          {# Design summary #}
          {# ================================================================= #}
          <div class="card-row">
            <div class="card-column">
              <header>Design summary</header>

              {% if design_summary is defined and design_summary %}
                <table>
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th>Number of unique values</th>
                      <th>Varname</th>
                      <th>Autogen</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for row in design_summary %}
                      <tr>
                        <td>{{ row.variabletype_label }}</td>
                        <td class="align-right">{{ row.n }}</td>
                        <td class="align-right">{{ row.variable_name }}</td>
                        <td class="align-right">{{ row.autogenerated }}</td>
                      </tr>
                    {% endfor %}
                  </tbody>
                </table>
              {% endif %}
            </div>
          </div>
          {# ================================================================= #}
          {# Event - subevent #}
          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Events
                </summary>

                {% if event is defined and event %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Event names</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in event %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>

          </div>
          {# ===================== #}

          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Subevents
                </summary>

                {% if subevent is defined and subevent %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Subevent names</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in subevent %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

          {# ================================================================= #}
          {# Samplingevent - samplingtime #}
          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Sampling events
                </summary>

                {% if samplingevent is defined and samplingevent %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Samplingevents</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in samplingevent %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Samplingtime
                </summary>

                {% if samplingtime is defined and samplingtime %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Samplingevent</th>
                          <th>Samplingtime</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in samplingtime %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.valuename2 }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Centers
                </summary>

                {% if center is defined and startgroup %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Centers</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in center %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Startgroups
                </summary>

                {% if startgroup is defined and startgroup %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Startgroups</th>
                          <th>Count</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in startgroup %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.n }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Subjects
                </summary>

                {% if startgroup is defined and startgroup %}
                  <div class="table-container">
                    <table>
                      <thead>
                        <tr>
                          <th>Subject</th>
                          <th>Startgroup</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in subject %}
                          <tr>
                            <td>{{ row.valuename }}</td>
                            <td>{{ row.valuename2 }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </details>

            </div>
          </div>

        </div> 
      </div>
    {% endif %}

  {% endblock %}

  {% block footer %}
    {% if dataset_info is defined and dataset_info %}
      <div class="nds-footer-container">
        <div class="nds-footer-info">
          <div title="Currently selected dataset">Dataset: <span style="font-weight: bold">{{ dataset_info.name }}</span></div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
