{% extends "layouts/base-indexpage.volt" %}
  
  {% block menu_wide_module_header %}
    {{ partial("modules/dashin/00partials/menu_wide_module_header_partial") }}
  {% endblock %}

  {% block menu_wide_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_wide_module_submenus_partial") }}
  {% endblock %}

  {% block menu_narrow_module_header %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_header_partial") }}
  {% endblock %}

  {% block menu_narrow_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_submenus_partial") }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-dataset">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
      </div>
      <a href="/dashin/owner/import/index/{{ _ls_querystring_ }}">Datasets - upload</a>
    </div>
  {% endblock %}

