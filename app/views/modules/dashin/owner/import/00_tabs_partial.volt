<div class="nds-tabs-row">
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "listdatasets" %} current{% endif %}">
    <a href="/dashin/owner/import/listdatasets/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "uploaddata" %} current{% endif %}">
    <a href="/dashin/owner/import/uploaddata/{{ _ls_querystring_ }}">
      Upload
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "uploadsummary" %} current{% endif %}">
    <a href="/dashin/owner/import/uploadsummary/{{ _ls_querystring_ }}">
      Summary
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "editdataset" %} current{% endif %}">
    <a href="/dashin/owner/import/editdataset/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "browsedata" %} current{% endif %}">
    <a href="/dashin/owner/import/browsedata/{{ _ls_querystring_ }}">
      Browse
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "listvariables" %} current{% endif %}">
    <a href="/dashin/owner/import/listvariables/{{ _ls_querystring_ }}">
      Variables
    </a>
  </div>

{#  <div class="nds-tab{% if current_tab is defined and current_tab === "editvariable" %} current{% endif %}">#}
{#    <a href="/dashin/owner/import/editvariable/{{ _ls_querystring_ }}">#}
{#      Edit variable#}
{#    </a>#}
{#  </div>#}

  <div class="nds-tab{% if current_tab is defined and current_tab === "viewdatadesign" %} current{% endif %}">
    <a href="/dashin/owner/import/viewdatadesign/{{ _ls_querystring_ }}">
      Design
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "listvdvariables" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listvdvariables/{{ _ls_querystring_ }}">
      Variable info (Variables)
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "browsevardata" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/browsevardata/{{ _ls_querystring_ }}">
      Variable info (Data)
    </a>
  </div>

</div>
