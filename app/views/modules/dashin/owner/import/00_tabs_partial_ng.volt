<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/import/listdatasets", "listdatasets", current_tab, _ls_querystring_) }}
{#  {{ m_menu_tabentry("Upload", "/dashin/owner/import/uploaddata", "uploaddata", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}#}
  {{ m_menu_tabentry("Summary", "/dashin/owner/import/uploadsummary", "uploadsummary", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/import/editdataset", "editdataset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Browse", "/dashin/owner/import/browsedata", "browsedata", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Variables", "/dashin/owner/import/listvariables", "listvariables", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Design", "/dashin/owner/import/viewdatadesign", "viewdatadesign", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Files", "/dashin/owner/import/filelist", "files", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

</div>
