{% extends "layouts/base-deletepage.volt" %}
  {% block set_params %}
    {{ set_title("Delete Dataset") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/deletedataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[cancel]" class="button selectpage cancel">Cancel</button>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      {% if dataset_info is defined %}
        <div>
          {{ dataset_info.name }}
          <header>{{ confirm_msg }}</header>
          <div style="display: flex;flex-flow: row">
            <button form="form1" class="button confirm" name="btn[confirm]" value="{{ dataset_info.dataset_id }}">Confirm</button>
            <div style="display: flex;flex-flow: row;width: 16px"></div>
            <button form="form1" class="button cancel" name="btn[cancel]">Cancel</button>
          </div>
        </div>
      {% endif %}

    </div>

  {% endblock %}

  {% block page_subfooter %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}
