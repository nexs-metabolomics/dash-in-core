{% extends "modules/dashin/owner/import/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Browse dataset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"browsedata"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/browsedata/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_genericbutton("Download dataset","downloaddata","form1", "button download",dataset|default(null)) }}
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Dataset", dataset|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if dataset is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}


      <div class="table-container-fixed-header-card">

        <div class="table-card-title">
          {{ get_title(false) }}
        </div>

        <div class="table-container fixed-header">

          <table>

            <thead>
              <tr>
                {% for col in pagination_cols.items %}
                  <th>{{ col.name }}</th>
                {% endfor %}
              </tr>
            </thead>

            <tbody>
              {% for row in pagination_rows.items %}
                <tr>
                  {% for cell in row.datarow %}
                    <td class="align-datatype{{ cell.t }}">{{ cell.v }}</td>
                  {% endfor %}
                </tr>
              {% endfor %}
            </tbody>

          </table>
        </div>
      </div>

    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {% if dataset is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/import/browsedata/",
        "act_link":"dashin/owner/import/browsedata/",
        "reset_link":"dashin/owner/import/browsedata/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
