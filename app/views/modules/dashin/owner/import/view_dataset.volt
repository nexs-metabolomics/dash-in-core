{% extends "modules/dashin/owner/import/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Dataset summary") }}
  {% endblock %}
  
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"uploadsummary"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/uploadsummary/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if dataset is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          
          <div class="card-title">
            {{ get_title(false) }}
          </div>
          
          <div class="card-column">
            <div class="card-fieldset-header">
              {{ dataset.name }}
            </div>
          </div>

          <div class="card-column">
            <div class="card-field-content">
              {{ dataset.description }}
            </div>
          </div>

          <div class="card-row">
            <div class="card-row">
              <div class="card-column">

                <div class="card-field-row" style="background-color: #F6F6F6;">
                  <div class="card-field-content">
                    No. of variables
                  </div>
                  <div class="card-field-header">
                    {{ dataset.n_var }}
                  </div>
                </div>

                <div class="card-field-row">
                  <div class="card-field-content">
                    No. of rows
                  </div>
                  <div class="card-field-header">
                    {{ dataset.n_row }}
                  </div>
                </div>

              </div>
            </div>

            <div class="card-column">
              <div class="card-button-box">
                <div style="display: flex;flex-flow: row nowrap;background-color: #dedfe1;border-radius: 16px;justify-content: center;align-items: center;width:32px;height:32px">
                  <a href="/dashin/owner/import/editdataset/{{ _ls_querystring_ }}" class="card-img-button" title="Edit dataset"><img src="/img/icons/edit-v01.svg"></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    {% endif %}
  {% endblock %}
  {% block footer %}
    {% if dataset is defined and dataset %}
      <div class="nds-footer-container">
        <div class="nds-footer-info">
          <div title="Currently selected dataset">Dataset: <span style="font-weight: bold">{{ dataset.name }}</span></div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
