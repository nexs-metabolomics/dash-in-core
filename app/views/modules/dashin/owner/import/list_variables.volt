{% extends "modules/dashin/owner/import/00-subroot-variablespage.volt" %}
  {% block set_params %}
    {{ set_title("Variable properties") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"listvariables"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/listvariables/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="setFormSubmitting()" oninput="formSetChanged()"></form>
    <form id="frm-fi" action="/dashin/owner/import/listvariables/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="setFormSubmitting()" oninput="formSetChanged()"></form>
    {% if dataset_info is defined %}
      <div class="filter-input save-button">
        <button form="tableform" name="btn[save]" class="button action" title="Update all rows">
          Save changes
        </button>
      </div>
      <button form="form1" class="button select" name="btn[select_dataset]">Select dataset</button>
      <div style="margin-left: auto"></div>
      <button form="form1" class="button download" name="btn[downloadtemplate]" title="Download variable-info as template">Download Template</button>
    {% endif %}
  {% endblock %}

  {% block nav_header %}
    {% if dataset_info is defined %}
      {{ partial("00element_partials/nds_table_nav2",["item_name":"variable","nav_link":"dashin/owner/import/listvariables/"]) }}
    {% endif %}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block subheader_filterpanel %}
    <form id="filterform" action="/dashin/owner/import/listvariables/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="setFormSubmitting()" oninput="formSetChanged()">
      <div class="filter-input">
        <div style="display: flex;flex-flow: row nowrap">
          <button name="btn[search]" type="submit" class="button filter" form="frm-fi">Filter</button>
          {% if show_filter_reset is defined and show_filter_reset %}
            <button name="btn[reset]" class="button invisible" form="frm-fi" style="padding: 0 0.2rem;margin-right: 0.4rem">
              <div style="margin-right: 0.4rem">Reset</div>
              <img src="/img/x01-outline.svg" width="12px" style="margin-top: 0.1rem;margin-bottom: -0.1rem">
            </button>
          {% endif %}
        </div>
      </div>
      <div class="filter-input">
        {{ select_fi.vn }}
      </div>
      <div class="filter-input">
        {{ select_fi.dt }}
      </div>
      <div class="filter-input">
        {{ select_fi.vt }}
      </div>
      <div class="filter-input">
        {{ select_fi.mt }}
      </div>
    </form>

    {#    <div class="filter-input save-button" style="z-index: 1000"> #}
    {#      <button form="tableform" name="btn[save]" class="button action" title="Update all rows"> #}
    {#        Save #}
    {#      </button> #}
    {#    </div> #}

  {% endblock %}

  {% block page_content %}

    {% if dataset_info is not defined %}
      <div class="nds-emptypage-card">
        <header class="warning">Please select dataset</header>
        <button form="form1" class="button" name="btn[select_dataset]">Select dataset</button>
      </div>
    {% else %}
      {% if current_page is defined %}


        <div class="table-container-fixed-header-card">

          <div class="table-card-title">
            {{ get_title(false) }}
          </div>

          <div class="table-container fixed-header">
            <form id="tableform" action="/dashin/owner/import/listvariables/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="setFormSubmitting()"
                  oninput="formSetChanged()">

              <table>

                <thead>
                  <tr>
                    <th>Name</th>
                    <th> Unit</th>
                    <th>
                      <div style="white-space: nowrap">
                        Data type
                      </div>
                    </th>
                    <th>Variable type</th>
                    <th>Assay</th>
                  </tr>

                </thead>

                <tbody>
                  {% if pagination is defined %}
                    {% for row in pagination.items %}
                      <tr class="variables">
                        <td title="{{ row.name }}">
                          <div style="display: flex;flex-flow: row nowrap;width: 100%">
                            <button name="btn[editvariable]" value="{{ row.variable_id }}" class="button icon24">
                              <img src="/img/icons/edit-v01.svg">
                            </button>
                            {{ row.name_short }}
                          </div>
                        </td>
                        {# <td class="align-center">{{ row.unit_symbol }}</td> #}
                        <td class="td-unit">
                          <div style="display: flex;flex-flow: row">
                            {{ row.un }}/{{ row.ud }}
                          </div>
                        </td>
                        <td class="td-datatype">
                          {{ row.dt }}
                        </td>
                        <td class="td-vartype">
                          {{ row.vt }}
                        </td>
                        <td class="td-assay">
                          {{ row.as }}
                        </td>
                      </tr>
                    {% endfor %}
                  {% endif %}
                </tbody>

              </table>

            </form>

          </div>
        </div>

        <script>
            var formSubmitting = false;
            var setFormSubmitting = function () {
                formSubmitting = true;
            }

            var hasChanged = false;

            function vartypeChange(obj) {
                current_val = obj.value

                if (['2', '3', '4', '5', '6', '7', '8'].includes(current_val)) {
                    s = document.getElementsByClassName('vartype');
                    for (i = 0; i < s.length; i++) {
                        if (s.item(i).value == current_val) {
                            s.item(i).value = 0;
                            s.item(i).dataset.changed = 'changed';
                        }
                    }
                }
                obj.dataset.changed = 'changed';
                obj.value = current_val;
                hasChanged = true;

            }

            function formSetChanged() {
                hasChanged = true;
            }

            window.onbeforeunload = function () {
                if (hasChanged && !formSubmitting) {
                    return "You have unsaved changes";
                }
            }

            function default_focus() {
                document.getElementById("ct2").focus();
            }

        </script>
      {% endif %}
    {% endif %}
  {% endblock %}
  {% block jsscripts %}
  {% endblock %}

  {% block subfooter_content %}
    {% if dataset_info is defined %}
      {{ partial("00element_partials/table_nav2_ng",[
        "item_name":"variable",
        "nav_link":"dashin/owner/import/listvariables/"
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}
