{% extends "modules/dashin/owner/import/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Upload supplementary file") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"files"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/import/00-dataset-files-subnav",["current_tab":"fileupload"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/fileupload/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
{#    {{ partial("modules/dashin/owner/import/dataset_info_partial") }}#}
  {% endblock %}

  {% block page_content %}


    {% if dataset_info is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

      {#      <input form="form1" name="fileup" type="file">#}
{#      <textarea form="form1" name="description"></textarea>#}
{#      <button form="form1" name="btn[submit]">Ok</button>#}

    {% endif %}
  {% endblock %}
