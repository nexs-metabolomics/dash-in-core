{% extends "modules/dashin/owner/import/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List datasets") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}
  
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"listdatasets"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/listdatasets/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <header>
        {{ get_title(false) }}
      </header>

      {% if pagination is defined %}
        {% for row in pagination.items %}

          {% if row.confirm is defined and row.confirm === true %}
            {% set hover_action = "warning" %}
          {% else %}
            {% set hover_action = "hover-hide-action" %}
          {% endif %}

          <div class="card {{ hover_action }}">

            {% if row.confirm is defined and row.confirm === true %}
              <header>DELETE DATASET?</header>
            {% endif %}

            <div class="card-column">
              <div class="card-fieldset-header">
                {{ row.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <div class="card-row-line-divider">
            </div>

            <div class="card-row">

              <div class="card-column">

                <div class="card-field-row">
                  <div class="card-field-content">
                    Num rows
                  </div>
                  <div class="card-field-header">
                    {{ row.n_row }}
                  </div>
                </div>

                <div class="card-field-row">
                  <div class="card-field-content">
                    Num columns
                  </div>
                  <div class="card-field-header">
                    {{ row.n_var }}
                  </div>
                </div>

              </div>

              <div class="card-column">
              </div>

              <div class="card-column">

                <form id="{{ row.dataset_id }}" action="/dashin/owner/import/listdatasets/{{ current_page }}/{{ _ls_querystring_ }}#rowno{{ loop.index }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

                  {{ m_card_button_box_confirm(
                    row.dataset_id,
                    row.confirm|default(null),
                    [
                      ["edit","edit-icon48-filled","button edit icon24","Edit study",ROLE_ORG_ALLOW_WRITE],
                      ["view","view-icon48-filled","button view icon24","View study"],
                      ["delete","delete-icon48-outline","button delete icon24","Delete study",ROLE_ORG_ALLOW_WRITE]
                    ],
                    [
                      ["confirm","Confirm","button confirm","Confirm delete study",ROLE_ORG_ALLOW_WRITE],
                      ["cancel", "Cancel", "button cancel","Cancel delete study"]
                    ],
                    "Do you really want to delete this dataset?"
                  ) }}


                </form>

              </div>

            </div>
          </div>
        {% endfor %}
      {% endif %}

    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/import/listdatasets",
        "reset_link":"dashin/owner/import/listdatasets/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
