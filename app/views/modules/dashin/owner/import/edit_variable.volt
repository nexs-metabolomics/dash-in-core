{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Edit variable") }}

    {% set btn_dataset_selected = "" %}
    {% if dataset_info is defined and dataset_info %}
      {% set btn_dataset_selected=" nds-object-selected" %}
    {% endif %}
    {% if variable is defined and variable %}
      {% set btn_variable_selected=" nds-object-selected" %}
    {% endif %}

  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"show_submenu_import_dataset":1,"current":"dataset_index"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial",["current_tab":"editvariable"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/dashin/owner/import/editvariable/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {% if dataset_info is defined and variable is defined %}
      <button form="form1" class="button select" name="btn[select_dataset]">Select dataset</button>
      <button form="form1" class="button select" name="btn[select_variable]">Select variable</button>
    {% endif %}
  {% endblock %}
  {% block content2 %}

    {% if dataset_info is not defined or variable is not defined %}
      <div class="nds-emptypage-card">
        <div class="nds-card-column">
          <img src="/img/empty/empty-page-01.png">
        </div>
        <div class="nds-card-column">

          {% if dataset_info is not defined %}
            <header class="warning">Please select dataset</header>
          {% else %}
            <header>Selected dataset: {{ dataset_info.name }}</header>

          {% endif %}
          <button form="form1" class="nds-button{{ btn_dataset_selected }}" name="btn[select_dataset]">Select dataset</button>
        </div>
      </div>

      <div class="nds-emptypage-card">
        <div class="nds-card-column">
          <img src="/img/empty/empty-page-01.png">
        </div>
        <div class="nds-card-column">
          {% if dataset_info is not defined %}
            <header class="disabled">Please select variable</header>
            <div style="display: flex;width: 100px">
              <div class="nds-button secondary" title="You must select a dataset before you can select a variable">Select variable</div>
            </div>
          {% else %}
            <header class="warning">Please select variable</header>
            <div style="display: flex;width: 100px">
              <button form="form1" class="nds-button{{ btn_variable_selected }}" name="btn[select_variable]">Select variable</button>
            </div>
          {% endif %}
        </div>
      </div>

    {% else %}

      <div class="nds-card-container">
        <div class="nds-card">
          {# <div class="nds-view-container view-dataset"> #}
          {% if variable is defined %}

            <div class="nds-view-column">
              <div class="nds-view-label">
                Name
              </div>
              <div class="nds-view-header">
                {{ variable.name }}
              </div>
            </div>

            <div class="nds-view-column">
              <div class="nds-view-label">
                Description
              </div>
              <div class="nds-view-content">
                {{ variable.description }}
              </div>
            </div>

            <div class="nds-view-column">
              <div class="nds-view-row">
                <div class="nds-view-row-header">
                  Data type
                </div>
                <div class="nds-view-value">
                  {{ variable.datatype }}
                </div>
              </div>
              <div class="nds-view-row">
                <div class="nds-view-row-header">
                  Variable type
                </div>
                <div class="nds-view-value">
                  {{ variable.variabletype }}
                </div>
              </div>
              <div class="nds-view-row">
                <div class="nds-view-row-header">
                  Assay
                </div>
                <div class="nds-view-value">
                  {{ variable.assay }}
                </div>
              </div>
              <div class="nds-view-row">
                <div class="nds-view-row-header">
                  Unit
                </div>
                <div class="nds-view-value">
                  {{ variable.unit_symbol }}
                </div>
              </div>
            </div>

          {% endif %}
        </div>
      </div>
    {% endif %}
  {% endblock %}
  {% block footer %}
    {% if dataset is defined and dataset %}
      <div class="nds-footer-container">
        <div class="nds-footer-info">
          <div title="Currently selected dataset">Dataset: <span style="font-weight: bold">{{ dataset.name }}</span></div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
