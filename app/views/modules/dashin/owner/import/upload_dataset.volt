{% extends "modules/dashin/owner/import/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Upload dataset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/import/00_tabs_partial_ng",["current_tab":"uploaddata"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/import/uploaddata/{{ _ls_querystring_ }}" accept-charset="utf-8" class="nds upload-dataset" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
   {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}
    

      
    {{ m_form_innerloop_card(form,[["submit","Ok","button submit has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

{#    <form id="form1">#}
{#    </form>#}

  {% endblock %}
