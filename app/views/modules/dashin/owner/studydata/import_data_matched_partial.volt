{# ==================================== #}
{#   event     #}
{# ==================================== #}
<div>
  <header>Matched events</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched event     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.event is defined and study_matched.event %}
        <table>
          <thead>
          <tr>
            <th>Event names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.event %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched event     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.event is defined and data_matched.event %}
        <table>
          <thead>
          <tr>
            <th>Event names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.event %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   subevent     #}
{# ==================================== #}
<div>
  <header>Matched subevents</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched subevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.subevent is defined and study_matched.subevent %}
        <table>
          <thead>
          <tr>
            <th>Subevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.subevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched subevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.subevent is defined and data_matched.subevent %}
        <table>
          <thead>
          <tr>
            <th>Subevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.subevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   samplingevent     #}
{# ==================================== #}
<div>
  <header>Matched samplingevents</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched samplingevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.samplingevent is defined and study_matched.samplingevent %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.samplingevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched samplingevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.samplingevent is defined and data_matched.samplingevent %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.samplingevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   samplingtime     #}
{# ==================================== #}
<div>
  <header>Matched samplingtimes</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched samplingtime     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.samplingtime is defined and study_matched.samplingtime %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Samplingtime names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.samplingtime %}
            <tr>
              <td>{{ row.name2 }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched samplingtime     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.samplingtime is defined and data_matched.samplingtime %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Samplingtime names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.samplingtime %}
            <tr>
              <td>{{ row.name2 }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>
</div>


{# ==================================== #}
{#   startgroup     #}
{# ==================================== #}
<div>
  <header>Matched startgroups</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched startgroup     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.startgroup is defined and study_matched.startgroup %}
        <table>
          <thead>
          <tr>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.startgroup %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched startgroup     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.startgroup is defined and data_matched.startgroup %}
        <table>
          <thead>
          <tr>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.startgroup %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   subject     #}
{# ==================================== #}
<div>
  <header>Matched subjects</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study matched subject     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_matched.subject is defined and study_matched.subject %}
        <table>
          <thead>
          <tr>
            <th>Subject names</th>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_matched.subject %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.name2 }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data matched subject     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_matched.subject is defined and data_matched.subject %}
        <table>
          <thead>
          <tr>
            <th>Subject names</th>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_matched.subject %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.name2 }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

