{# ==================================== #}
{#   event     #}
{# ==================================== #}
<div>
  <header>Unmatched events</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched event     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.event is defined and study_unmatched.event %}
        <table>
          <thead>
          <tr>
            <th>Event names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.event %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched event     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.event is defined and data_unmatched.event %}
        <table>
          <thead>
          <tr>
            <th>Event names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.event %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   subevent     #}
{# ==================================== #}
<div>
  <header>Unmatched subevents</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched subevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.subevent is defined and study_unmatched.subevent %}
        <table>
          <thead>
          <tr>
            <th>Subevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.subevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched subevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.subevent is defined and data_unmatched.subevent %}
        <table>
          <thead>
          <tr>
            <th>Subevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.subevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   samplingevent     #}
{# ==================================== #}
<div>
  <header>Unmatched samplingevents</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched samplingevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.samplingevent is defined and study_unmatched.samplingevent %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.samplingevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched samplingevent     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.samplingevent is defined and data_unmatched.samplingevent %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.samplingevent %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   samplingtime     #}
{# ==================================== #}
<div>
  <header>Unmatched samplingtimes</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched samplingtime     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.samplingtime is defined and study_unmatched.samplingtime %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Samplingtime names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.samplingtime %}
            <tr>
              <td>{{ row.name2 }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched samplingtime     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.samplingtime is defined and data_unmatched.samplingtime %}
        <table>
          <thead>
          <tr>
            <th>Samplingevent names</th>
            <th>Samplingtime names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.samplingtime %}
            <tr>
              <td>{{ row.name2 }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>
</div>


{# ==================================== #}
{#   startgroup     #}
{# ==================================== #}
<div>
  <header>Unmatched startgroups</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched startgroup     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.startgroup is defined and study_unmatched.startgroup %}
        <table>
          <thead>
          <tr>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.startgroup %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched startgroup     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.startgroup is defined and data_unmatched.startgroup %}
        <table>
          <thead>
          <tr>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.startgroup %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   subject     #}
{# ==================================== #}
<div>
  <header>Unmatched subjects</header>
</div>
<div class="card-row">

  {# ==================================== #}
  {#   study unmatched subject     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Study</header>
      {% if study_unmatched.subject is defined and study_unmatched.subject %}
        <table>
          <thead>
          <tr>
            <th>Subject names</th>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in study_unmatched.subject %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.name2 }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

  {# ==================================== #}
  {#   data unmatched subject     #}
  {# ==================================== #}
  <div class="card-column">
    <div class="card-fieldset-column table-container">
      <header>Data</header>
      {% if data_unmatched.subject is defined and data_unmatched.subject %}
        <table>
          <thead>
          <tr>
            <th>Subject names</th>
            <th>Startgroup names</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
          {% for row in data_unmatched.subject %}
            <tr>
              <td>{{ row.name }}</td>
              <td>{{ row.name2 }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

