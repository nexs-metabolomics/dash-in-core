<div class="tabs-row" style="margin-bottom: 1rem">
  
  <div class="tab{% if current_tab is defined and current_tab === "condition" %} current{% endif %}">
    <a href="/dashin/owner/studydata/listconditions/{{ _ls_querystring_ }}">
      Condition
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "studyrowset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/viewstudyrowset/{{ _ls_querystring_ }}">
      Rowset
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "studyvarset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyvarsetvars/{{ _ls_querystring_ }}">
      Variableset
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "studyexportset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/viewstudyexportset/{{ _ls_querystring_ }}">
      Study data export
    </a>
  </div>

</div>
