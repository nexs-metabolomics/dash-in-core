{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("List available datasets") }}

    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

    {% set btn_dataset_selected = "" %}
    {% if dataset_info is defined and dataset_info %}
      {% set btn_dataset_selected=" nds-object-selected" %}
    {% endif %}
    
    {% set ok_img = 
      '<svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18" style="fill: green">
  <path d="M12 2C6.5 2 2 6.5 2 12S6.5 22 12 22 22 17.5 22 12 17.5 2 12 2M12 20C7.59 20 4 16.41 4 12S7.59 4 12 4 20 7.59 20 12 16.41 20 12 20M16.59 7.58L10 14.17L7.41 11.59L6 13L10 17L18 9L16.59 7.58Z"/>
</svg>'
    %}

  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_studydata":1,"current":"studydata_index"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("modules/dashin/owner/studydata/00_tabs_partial",["current_tab":"listdatasets"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/dashin/owner/studydata/listdatasets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}
  {% block nav_header %}
    {{ partial("00element_partials/nds_table_nav_search_form",[
      "nav_link":"dashin/owner/studydata/listdatasets",
      "act_link":"dashin/owner/studydata/listdatasets",
      "reset_link":"dashin/owner/studydata/listdatasets/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}
  {% block content2 %}

    <div class="nds-table-container">


{#      <div>#}
{#        {% if nodups is defined and nodups %}#}
{#          <button form="form1" class="button select" name="btn[showall]">Show all datasets</button>#}
{#        {% else %}#}
{#          <button form="form1" class="button select" name="btn[nodups]">Show only datasets without design errors</button>#}
{#        {% endif %}#}
{#      </div>#}
      <table>

        <thead>
        <tr>
          <th style="width: 5rem"></th>
          <th style="width: 15rem" colspan="6">Design variables</th>
          <th></th>
          <th></th>
        </tr>
        <tr>
          <th style="width: 5rem">Actions</th>
          <th style="width: 2.5rem" title="event">ev</th>
          <th style="width: 2.5rem" title="subevent">se</th>
          <th style="width: 2.5rem" title="startgroup">sg</th>
          <th style="width: 2.5rem" title="subject">sb</th>
          <th style="width: 2.5rem" title="samplingevent">sm</th>
          <th style="width: 2.5rem" title="samplingtime">st</th>
          <th>Name</th>
          <th>Description</th>
        </tr>
        </thead>

        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr id="{{ row.dataset_id }}">
              <td>
                <button form="form1" name="btn[select]" value="{{ row.dataset_id }}"
                        class="nds-button nds-small nds-hollow" title="Edit dataset">
                  Select
                </button>
              </td>
              <td class="align-center">{% if row.event == "x" %}{{ ok_img }}{% endif %}</td>
              <td class="align-center">{% if row.subevent == "x" %}{{ ok_img }}{% endif %}</td>
              <td class="align-center">{% if row.startgroup == "x" %}{{ ok_img }}{% endif %}</td>
              <td class="align-center">{% if row.subject == "x" %}{{ ok_img }}{% endif %}</td>
              <td class="align-center">{% if row.samplingevent == "x" %}{{ ok_img }}{% endif %}</td>
              <td class="align-center">{% if row.samplingtime == "x" %}{{ ok_img }}{% endif %}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}
