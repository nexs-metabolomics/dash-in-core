{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Add dataset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"datasets"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-datasets-subnav",["current_tab":"attachdataset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydata/attachdataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
{#    <button form="form1" class="button select" name="btn[select_study]">Select study</button>#}
{#    <button form="form1" class="button select" name="btn[select_dataset]">Select dataset</button>#}
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", study_info|default(false),"name") }}
    {{ m_info_element("Study", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}
    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif dataset_info is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}

      <div class="card-container">

      {# ================================================================= #}
      {#   Header     #}
      {# ================================================================= #}
      <div class="card">

        <div class="card-title">
          {{ get_title(false) }}
        </div>

        <div class="card-row">
          {# ================== #}
          {#   Study     #}
          {# ================== #}
          <div class="card-column">
            <div class="card-fieldset-column table-container">
              <header>Study</header>
              {% if study_info is defined %}
                {{ study_info.name }}
                <span style="color: #00bb1f">[{{ study_info.study_id }}]</span>
              {% endif %}
            </div>
          </div>
          {# ================== #}
          {#   Data     #}
          {# ================== #}
          <div class="card-column">
            <div class="card-fieldset-column table-container">
              <header>Dataset</header>
              {% if dataset_info is defined %}
                {{ dataset_info.name }}<br>
                <span style="color: #00bb1f">[{{ dataset_info.dataset_id }}]</span>
              {% endif %}
            </div>
          </div>
        </div>

        {# ================================================================= #}
        {#   Buttons     #}
        {# ================================================================= #}
        <div class="nds-row">
          <div class="nds-column-stretch">
            <div class="nds-card-level-2 nds-table-container">
              <div class="nds-row" style="justify-content: space-between">
                <div style="display: flex;flex-flow: row nowrap">

                  {% if dataset_info.in_study is defined and dataset_info.in_study %}
                    <div style="font-weight: bold">Dataset already in study</div>

                    {#                    {% if confirm_remove is defined and confirm_remove %} #}
                    {#                      <span style="margin-right: 2rem;border: #e61119 solid 2px;font-weight: bold;padding: 0.4rem 0.6rem"> #}
                    {#                        This will remove dataset from study #}
                    {#                      </span> #}
                    {#                      <button form="form1" name="btn[confirmremove]" type="submit" class="nds-button">Confirm</button> #}
                    {#                      <button form="form1" name="btn[cancel]" type="submit" class="nds-button nds-hollow">Cancel</button> #}
                    {#                    {% else %} #}
                    {#                      <button form="form1" name="btn[remove]" type="submit" class="nds-button nds-hollow">Remove dataset from study</button> #}
                    {#                    {% endif %} #}


                  {% elseif design_is_valid is defined and design_is_valid === 1 %}
                    <div style="font-weight: bold">Add dataset</div>
                    {% if confirm_import is defined and confirm_import %}

                      <button form="form1" name="btn[confirm]" type="submit" class="nds-button">Confirm</button>
                      <button form="form1" name="btn[cancel]" type="submit" class="nds-button nds-hollow ">Cancel</button>

                    {% else %}

                      <button form="form1" name="btn[save]" type="submit" class="button">Add dataset</button>

                    {% endif %}
                  {% else %}

                    <div style="border: #fb0031 solid 0.2rem;padding: 0 0.4rem;margin-right: 16px">
                      It is not possible to add this dataset because it doesn't match the study design
                    </div>
                    <a href="/dashin/owner/studydesign/viewstudydesign/{{ _ls_querystring_ }}" target="_blank" class="button secondary" title="View design in new tab">View study design</a>
                    {#                    <button class="button action secondary" style="text-decoration: line-through;text-decoration-thickness: 0.15rem">Add dataset</button> #}

                  {% endif %}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {% if (all_summary.data_unmatched is defined and all_summary.data_unmatched > 0) or (all_summary.study_unmatched is defined and all_summary.study_unmatched > 0) %}

        <div class="nds-card">
          {{ partial("modules/dashin/owner/studydata/add_dataset_unmatched_partial") }}
        </div>

      {% endif %}

      {#      {% if all_summary.data_study_matched is defined and all_summary.data_study_matched > 0 %} #}

      {#        <div class="card"> #}
      {#          {{ partial("modules/dashin/owner/studydata/import_data_matched_partial") }} #}
      {#        </div> #}

      {#      {% endif %} #}
    {% endif %}

    </div>

  {% endblock %}
