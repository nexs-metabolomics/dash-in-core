{# ==================================== #}
{#   unmatched event     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.event is defined and data_unmatched.event %}
        <header>Unmatched events</header>
        <table>
          <thead>
            <tr>
              <th>Event names</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.event %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   unmatched subevent     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.subevent is defined and data_unmatched.subevent %}
        <header>Unmatched subevents</header>
        <table>
          <thead>
            <tr>
              <th>Subevent names</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.subevent %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   unmatched samplingevent     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.samplingevent is defined and data_unmatched.samplingevent %}
        <header>Unmatched samplingevents</header>
        <table>
          <thead>
            <tr>
              <th>Samplingevent names</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.samplingevent %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   unmatched samplingtime     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.samplingtime is defined and data_unmatched.samplingtime %}
        <header>Unmatched samplingtimes</header>
        <table>
          <thead>
            <tr>
              <th>Samplingevent names</th>
              <th>Samplingtime names</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.samplingtime %}
              <tr>
                <td>{{ row.name2 }}</td>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>
</div>

{# ==================================== #}
{#   unmatched startgroup     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.startgroup is defined and data_unmatched.startgroup %}
        <header>Unmatched startgroups</header>
        <table>
          <thead>
            <tr>
              <th>Startgroup names</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.startgroup %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   unmatched startgroup     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.center is defined and data_unmatched.center %}
        <header>Unmatched center</header>
        <table>
          <thead>
            <tr>
              <th>Center</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.center %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

{# ==================================== #}
{#   unmatched subject     #}
{# ==================================== #}
<div class="card-row">

  <div class="card-column">
    <div class="card-fieldset-column table-container">
      {% if data_unmatched.subject is defined and data_unmatched.subject %}
        <header>Unmatched subjects</header>
        <table>
          <thead>
            <tr>
              <th>Subject names</th>
              <th>Center</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {% for row in data_unmatched.subject %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.name2 }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      {% endif %}
    </div>
  </div>

</div>

