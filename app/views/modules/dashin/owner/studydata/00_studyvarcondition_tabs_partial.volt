<div class="tabs-row">
  
  <div class="tab{% if current_tab is defined and current_tab === "listconditions" %} current{% endif %}">
    <a href="/dashin/owner/studydata/listconditions/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "createcondition" %} current{% endif %}">
    <a href="/dashin/owner/studydata/createcondition/{{ _ls_querystring_ }}">
      Add condition
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editcondition" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editcondition/{{ _ls_querystring_ }}">
      Edit condition
    </a>
  </div>

</div>
