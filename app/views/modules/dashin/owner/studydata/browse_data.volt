{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Browse study-data") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"datasets"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-datasets-subnav",["current_tab":"browsedata"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydata/browsedata/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", study_info|default(false),"name") }}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif dataset_info is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}

      <div class="table-container-fixed-header-card">
        
        <div class="table-card-title">
          {{ get_title(false) }}
        </div>
        
        <div class="table-container fixed-header">
          <table>

            <thead>
              <tr>
                {% for col in pagination_cols.items %}
                  <th>{{ col.name }}</th>
                {% endfor %}
              </tr>
            </thead>

            <tbody>
              {% for row in pagination_rows.items %}
                <tr>
                  {% for cell in row.datarow %}
                    <td class="align-datatype{{ cell.t }}">{{ cell.v }}</td>
                  {% endfor %}
                </tr>
              {% endfor %}
            </tbody>

          </table>
        </div>
      </div>

    {% endif %}


  {% endblock %}

  {% block subfooter_content %}
    {% if dataset_info is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/studydata/browsedata/",
        "act_link":"dashin/owner/studydata/browsedata/",
        "reset_link":"dashin/owner/studydata/browsedata/" ~ "/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
