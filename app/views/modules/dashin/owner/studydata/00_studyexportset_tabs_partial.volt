<div class="tabs-row">
  
  <div class="tab{% if current_tab is defined and current_tab === "liststudyexportsets" %} current{% endif %}">
    <a href="/dashin/owner/studydata/liststudyexportsets/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "createstudyexportset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/createstudyexportset/{{ _ls_querystring_ }}">
      Create
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editstudyexportset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyexportset/{{ _ls_querystring_ }}">
      Description
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "defstudyexportset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/defstudyexportset/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "viewstudyexportset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/viewstudyexportset/{{ _ls_querystring_ }}">
      View / export
    </a>
  </div>

</div>
