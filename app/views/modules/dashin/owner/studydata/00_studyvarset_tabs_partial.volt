<div class="tabs-row">
  
  <div class="tab{% if current_tab is defined and current_tab === "createstudyvarset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/createstudyvarset/{{ _ls_querystring_ }}">
      Create variableset
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editstudyvarset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyvarset/{{ _ls_querystring_ }}">
      Edit variableset
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editstudyvarsetvars" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyvarsetvars/{{ _ls_querystring_ }}">
      Select variables
    </a>
  </div>

</div>
