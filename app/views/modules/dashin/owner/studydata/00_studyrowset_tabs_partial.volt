<div class="tabs-row">
  
  <div class="tab{% if current_tab is defined and current_tab === "listconditions" %} current{% endif %}">
    <a href="#">
      List
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "createstudyrowset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/createstudyrowset/{{ _ls_querystring_ }}">
      Create new
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editstudyrowset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyrowset/{{ _ls_querystring_ }}">
      Description
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editstudyrowsetvars" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editstudyrowsetvars/{{ _ls_querystring_ }}">
      Edit rowset
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "editcondition" %} current{% endif %}">
    <a href="/dashin/owner/studydata/editcondition/{{ _ls_querystring_ }}">
      Edit condition
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "viewstudyrowset" %} current{% endif %}">
    <a href="/dashin/owner/studydata/viewstudyrowset/{{ _ls_querystring_ }}">
      View rowset
    </a>
  </div>

</div>
