{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Studies") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_studydata":1,"current":"studydata_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/dashin/owner/studydata/attachdataset/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Add dataset
              </header>
              <p>Match dataset to subjects and add to study</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/studydata/browseunmatched/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Browse unmatched
              </header>
              <p>Browse unmatched subjects</p>
            </div>
          </a>
        </li>
        
        <li class="nds-card">
          <a href="/dashin/owner/studydata/browsedata/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Browse data
              </header>
              <p>Browse data</p>
            </div>
          </a>
        </li>
        
        <li class="nds-card">
          <a href="/dashin/owner/studydata/listdatasets/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List datasets
              </header>
              <p>List datasets in study</p>
            </div>
          </a>
        </li>
        
      </ul>
    </nav>
  {% endblock %}
