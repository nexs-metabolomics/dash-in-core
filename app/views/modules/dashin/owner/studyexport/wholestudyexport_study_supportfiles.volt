{#<div class="card">#}

  <div class="card-row">
    <div class="card-column">

      <header>Files</header>

      {% if study_supportfiles is not defined %}

        No files

      {% else %}

        <div class="css-table">
          <div class="css-thead">

            <div class="css-trh">
              <div class="css-th">
                Name
              </div>
              <div class="css-th">
                description
              </div>
              <div class="css-th">
                Is Public
              </div>
            </div>
          </div>
          {% for item in study_supportfiles %}
            <div class="css-tr">

              <div class="css-td">
                {{ item.name }}
              </div>

              <div class="css-td">
                {{ item.description }}
              </div>

              <div class="css-td">
                {% if item.is_public === 1 %}
                  <div style="background-color: #00cd00">
                    File is public
                  </div>
                {% else %}
                  <div style="background-color: #ef5350">
                    File is private
                  </div>
                {% endif %}
              </div>
            </div>

          {% endfor %}

        </div>

      {% endif %}
    </div>
  </div>

{#</div>#}
