{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("List studyrowsets") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyvarset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarset_tabs_partial",["current_tab":"liststudyvarsets"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/liststudyvarsets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_study]">Select study</button>
  {% endblock %}

  {% block subheader_info %}
    {% if study_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Study
        </div>
        <div class="info-element-content">
          {{ study_info.name }}
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      {% if pagination is defined %}
        {% for row in pagination.items %}

          <div class="card{% if row.confirm is defined and row.confirm === true %} warning{% endif %}" id="{{ row.studyvariableset_id }}">

            {% if row.confirm is defined and row.confirm === true %}
              <header>DELETE</header>
            {% endif %}

            <div class="card-column" id="rowno{{ loop.index }}">
              <div class="card-fieldset-header">
                {{ row.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <div class="card-temp-spacing">
            </div>

            <div class="card-row">

              <div class="card-column">

                <div class="card-field-row">

                  <div class="card-field-header">
                    Number of variables
                  </div>

                  <div class="card-field-content">
                    {{ row.nvar }}
                  </div>

                </div>

              </div>

              <div class="card-column">
                <div class="card-button-box">

                  {# <form action="/dashin/owner/studyexport/list#{{ row.studyvariableset_id }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"> #}
                  <form action="/dashin/owner/studyexport/liststudyvarsets/{{ _ls_querystring_ }}#rowno{{ loop.index }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
                    <div class="card-button-row">
                      {% if row.confirm is defined and row.confirm === true %}

                        {% if ROLE_ORG_ALLOW_WRITE %}
                          <button form="form1"
                                  name="btn[confirm]" value="{{ row.studyvariableset_id }}"
                                  class="nds-button nds-small nds-alert"
                                  style="margin-left: 0.2rem"
                                  title="Confirm delete">Confirm
                          </button>
                        {% endif %}

                        {# no form-property: on cancel jump to #id (current position) #}
                        <button name="btn[cancel]" value="{{ row.studyvariableset_id }}"
                                class="nds-button nds-small"
                                title="Cancel delete">Cancel
                        </button>
                      {% else %}
                        <button form="form1"
                                name="btn[view]" value="{{ row.studyvariableset_id }}"
                                class="card-img-button"
                                title="View"><img src="/img/icons/eye.png" style="height: 2rem;width: 2rem">
                        </button>

                        {% if ROLE_ORG_ALLOW_WRITE %}
                          <button form="form1"
                                  name="btn[edit]" value="{{ row.studyvariableset_id }}"
                                  class="card-img-button"
                                  title="Edit study">
                            <img src="/img/icons/edit-v01.svg">
                          </button>
                          {# no form-property: on delete jump to #id (current position) #}
                          <button name="btn[delete]" value="{{ row.studyvariableset_id }}"
                                  class="card-img-button"
                                  title="Delete study"><img src="/img/icons/delete-v02.svg">
                          </button>
                        {% endif %}

                      {% endif %}
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        {% endfor %}
      {% endif %}

    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/import/listvariablesets/",
        "reset_link":"dashin/owner/import/listvariablesets/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
