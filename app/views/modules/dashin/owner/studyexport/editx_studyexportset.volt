{% extends "layouts/base-page-subnav.volt" %}
  {% block set_params %}
    {{ set_title("Describe exportset") }}
  {% endblock %}

  {% block menu_wide_module_header %}
    {{ partial("modules/dashin/00partials/menu_wide_module_header_partial") }}
  {% endblock %}

  {% block menu_wide_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_wide_module_submenus_partial",["show_submenu_study":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block menu_narrow_module_header %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_header_partial") }}
  {% endblock %}

  {% block menu_narrow_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_submenus_partial",["show_submenu_study":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyexportset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyexportset_tabs_partial",["current_tab":"editstudyexportset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/editstudyexportset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_study]">Select study</button>
    <button form="form1" name="btn[select_studyexportset]">Select studyexportset</button>
  {% endblock %}

  {% block subheader_info %}
    {% if study_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Study
        </div>
        <div class="info-element-content">
          {{ study_info.name }}
        </div>
      </div>
    {% endif %}
    {% if studyexportset_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Studyexportset
        </div>
        <div class="info-element-content">
          {{ studyexportset_info.name }}
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyexportset_info is not defined %}

      {{ m_emptypage_filler("Please select a studyexportset", "Select studyexportset", "select_studyexportset", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          {% if form is defined %}
            <form action="{{ form.getAction() }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","apply":"Apply","cancel":"Cancel"]]) }}

            </form>
          {% endif %}
        </div>
      </div>
    {% endif %}
  {% endblock %}
