<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/studyexport/liststudyrowsets", "liststudyrowsets", current_tab, _ls_querystring_) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "liststudyrowsets" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/liststudyrowsets/{{ _ls_querystring_ }}">#}
{#      List#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Create", "/dashin/owner/studyexport/createstudyrowset", "createstudyrowset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "createstudyrowset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/createstudyrowset/{{ _ls_querystring_ }}">#}
{#      Create#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Edit", "/dashin/owner/studyexport/editstudyrowset", "editstudyrowset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "editstudyrowset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/editstudyrowset/{{ _ls_querystring_ }}">#}
{#      Edit#}
{#    </a>#}
{#  </div>#}

</div>
