{% if form is defined %}
{#  <form action="{{ form.getAction() }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()" style="width: 100%;">#}

    <div style="display: flex;flex-flow: column;width: 100%">

      <div class="form-container" style="align-content: flex-start;width: 100%;margin-bottom: 16px">
        <header>Numeric single condition</header>

        {# =================================================================== #}
        {% set e = form.get("conditionname") %}
        {# =================================================================== #}
        <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
          {{ renderMessages(e) }}
          {% if e.getLabel()!="" %}
            {% if e.getUserOption("type")=="select" %}
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% else %}
              <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% endif %}
          {% endif %}
          <div class="form-field">
            {{ e.setAttribute("form","form1") }}
          </div>
        </div>

        {# =================================================================== #}
        {% set e = form.get("description") %}
        {# =================================================================== #}
        <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
          {{ renderMessages(e) }}
          {% if e.getLabel()!="" %}
            {% if e.getUserOption("type")=="select" %}
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% else %}
              <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% endif %}
          {% endif %}
          <div class="form-field">
            {{ e.setAttribute("form","form1") }}
          </div>
        </div>

      </div>

      <div style="display: flex;flex-flow: row nowrap;justify-content: flex-start">


        <div class="form-container" style="align-content: flex-start;width: unset">

          {# =================================================================== #}
          {% set e = form.get("conditionoperator") %}
          {# =================================================================== #}

          <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
            {{ renderMessages(e) }}
            {% if e.getLabel()!="" %}
              {% if e.getUserOption("type")=="select" %}
                <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {% else %}
                <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {% endif %}
            {% endif %}
            <div class="form-field">
              {{ e.setAttribute("form","form1") }}
            </div>
          </div>


        </div>
        <div class="form-container" style="width: unset;margin-left: 16px">

          {# =================================================================== #}
          {% set e = form.get("value_a") %}
          {# =================================================================== #}

          <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
            {{ renderMessages(e) }}
            {% if e.getLabel()!="" %}
              {% if e.getUserOption("type")=="select" %}
                <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {% else %}
                <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
              {% endif %}
            {% endif %}
            <div class="form-field">
              {{ e.setAttribute("form","form1") }}
            </div>
          </div>

          {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_form_buttons",["button_set":["submit":"Ok","apply":"Apply","reset":"reset"],"formattr":"form1"]) }}

        </div>
      </div>
    </div>

{#  </form>#}
{% endif %}
