{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Add studyvariable conditions") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"condition"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarcondition_tabs_partial",["current_tab":"createcondition"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" name="btn[search]" class="button hidden-default">Select study</button>
    <form id="form1" action="/dashin/owner/studyexport/createcondition/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif study_info.n_dataset == 0 %}
      
      {{ m_emptypage_filler("This study has no datasets", "Go to 'Add dataset'", "goto_adddataset", "form1") }}
      
    {% else %}

      <div class="table-container fixed-header">

        <table>
          <thead>
            <tr>
              <th>Actions</th>
              <th>Variable name</th>
              <th>Label</th>
            </tr>
          </thead>

          <tbody>
            {% if pagination is defined %}
              {% for row in pagination.items %}
                <tr>
                  <td class="align-right">
                    <div style="display: flex;flex-flow: row nowrap;justify-content: flex-start" title="Create condition">

                      <button form="form1"
                              name="btn[create]"
                              value="{{ row.studyvariable_id }}"
                              class="button create icon24 table-icon">
                        {{ partial("00img_partials/v1/icon48/libraryadd-opsz48-filled.svg") }}
                      </button>

                    </div>
                  </td>
                  <td title="{{ row.name }}">{{ row.name }}</td>
                  <td title="{{ row.label }}">{{ row.label }}</td>
                </tr>
              {% endfor %}
            {% endif %}
          </tbody>

        </table>
      </div>
    {% endif %}
  {% endblock %}
  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/studyexport/createcondition/",
        "reset_link":"dashin/owner/studyexport/createcondition/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}
