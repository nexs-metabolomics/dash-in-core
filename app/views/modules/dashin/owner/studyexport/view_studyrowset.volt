{% extends "layouts/base-page-subnav.volt" %}
  {% block set_params %}
    {{ set_title("View rowset") }}
  {% endblock %}

  {% block menu_wide_module_header %}
    {{ partial("modules/dashin/00partials/menu_wide_module_header_partial") }}
  {% endblock %}

  {% block menu_wide_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_wide_module_submenus_partial",["show_submenu_study":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block menu_narrow_module_header %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_header_partial") }}
  {% endblock %}

  {% block menu_narrow_module_submenus %}
    {{ partial("modules/dashin/00partials/menu_narrow_module_submenus_partial",["show_submenu_study":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyrowset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyrowset_tabs_partial",["current_tab":"viewstudyrowset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/liststudyvarsets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_study]">Select study</button>
    <button form="form1" name="btn[select_studyrowset]">Select studyrowset</button>
  {% endblock %}

  {% block subheader_info %}
    {% if study_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Study
        </div>
        <div class="info-element-content">
          {{ study_info.name }}
        </div>
      </div>
    {% endif %}
    {% if studyrowset_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Studyrowset
        </div>
        <div class="info-element-content">
          {{ studyrowset_info.name }}
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block page_content %}
    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyrowset_info is not defined %}

      {{ m_emptypage_filler("Please select (or create) a studyrowset", "Select studyrowset", "select_studyrowset", "form1") }}

    {% elseif studyvariableconditions is defined %}
      <div style="display: flex;flex-flow: row">
        {{ studyrowset_info.studyrowset_id }}
      </div>
      <div style="display: flex;flex-flow: row">
        Condition selects {{ studyrowset_info.n }} rows
      </div>

      <div class="table-container fixed-header">

        <table>
          <thead>
            <tr>
              <th>Action</th>
              <th>Name</th>
              <th>Label</th>
            </tr>
          </thead>

          <tbody>
            {% for row in studyvariableconditions %}
              <tr>
                <td>
                  <button form="form1" name="btn[editcondition]" value="{{ row.studyvariablecondition_id }}" class="button">Edit condition</button>
                </td>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          </tbody>

        </table>
      </div>
    {% endif %}

  {% endblock %}

  {% block subfooter_content %}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
