<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/studyexport/listconditions", "listconditions", current_tab, _ls_querystring_) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "listconditions" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/listconditions/{{ _ls_querystring_ }}">#}
{#      List#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Create", "/dashin/owner/studyexport/createcondition", "createcondition", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "createcondition" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/createcondition/{{ _ls_querystring_ }}">#}
{#      Create#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Edit", "/dashin/owner/studyexport/editcondition", "editcondition", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "editcondition" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/editcondition/{{ _ls_querystring_ }}">#}
{#      Edit#}
{#    </a>#}
{#  </div>#}

</div>
