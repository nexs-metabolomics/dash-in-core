{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit study exportset") }}
    {% if pagination1 is defined %}{% set has_results1 = true %}{% else %}{% set has_results1 = false %}{% endif %}
    {% if pagination2 is defined %}{% set has_results2 = true %}{% else %}{% set has_results2 = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyexportset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyexportset_tabs_partial",["current_tab":"editstudyexportset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/editstudyexportset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another exportset","select_studyexportset","form1",studyexportset_info|default(null)) }}
    {{ m_conditional_genericbutton("Download datasets","exportmultiple","form1","button download",studyexportset_info|default(null),"Download as individual datasets") }}
{#    {{ m_conditional_genericbutton("Download single dataset","exportsingle","form1","button download",studyexportset_info|default(null),"Download as single dataset") }}#}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
    {{ m_info_element("Exportset", studyexportset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyexportset_info is not defined %}

      {{ m_emptypage_filler("Please select (or create) a studyexportset", "Select studyexportset", "select_studyexportset", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
        <div class="card-title">
          {{ get_title(false) }}
        </div>
{#          <div style="display: flex;flex-flow: row;justify-content: space-between;height: 24px">#}
          <div class="card-row" style="border: #e9e9e9 solid 3px">

            {% if studyexportset_info.studyrowset_id %}
              <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;width: 50%;margin-right: 4px">
                <button form="form1" name="btn[rmstudyrowset]" value="{{ studyexportset_info.studyrowset_id }}" class="button remove" title="Remove rowset">Remove</button>
                <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;padding: 0 8px">
                  {{ studyexportset_info.studyrowset_name }}
                </div>
                <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;padding: 0 8px">
                  rows: {{ studyexportset_info.n_rows }}
                </div>
              </div>
            {% endif %}

            {% if studyexportset_info.studyvariableset_id %}
              <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;width: 50%;margin-left: 4px">
                <button form="form1" name="btn[rmstudyvarset]" value="{{ studyexportset_info.studyvariableset_id }}" class="button remove" title="Remove variable set">Remove</button>
                <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;padding: 0 8px">
                  {{ studyexportset_info.studyvariableset_name }}
                </div>
                <div style="display: flex;flex-flow: row nowrap;height: 100%;align-items: center;padding: 0 8px">
                  vars: {{ studyexportset_info.n_cols }}
                </div>
              </div>
            {% endif %}

          </div>

{#          <div style="display: flex;flex-flow: row;justify-content: space-evenly">#}
          <div class="card-row">

            {% if pagination1 is defined %}
              <div class="table-container fixed-header" style="width: 50%;margin-right: 4px">
                <table style="table-layout: fixed">
                  <caption>Row sets</caption>
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Name</th>
                      <th>Label</th>
                    </tr>
                  </thead>

                  <tbody>
                    {% for row in pagination1.items %}
                      <tr>
                        <td>
                          <button form="form1" name="btn[setstudyrowset]" value="{{ row.studyrowset_id }}" class="button action small">Select</button>
                          {# {{ row.studyvariable_id }} #}
                        </td>
                        <td>{{ row.name }}</td>
                        <td></td>
                      </tr>
                    {% endfor %}
                  </tbody>

                </table>
              </div>
            {% endif %}

            <div style="height: 32px"></div>
            {% if pagination2 is defined %}
              <div class="table-container fixed-header" style="width: 50%;margin-left: 4px">
                <table style="table-layout: fixed">
                  <caption>Variable sets</caption>
                  <thead>
                    <tr>
                      <th style="width: 120px;min-width: 120px">Action</th>
                      <th>Name</th>
                      <th>Label</th>
                    </tr>
                  </thead>

                  <tbody>
                    {% for row in pagination2.items %}
                      <tr>
                        <td>
                          <form action="/dashin/owner/studyexport/editstudyexportset/{{ _ls_querystring_ }}#cond{{ loop.index }}" accept-charset="utf-8" method="post">
                            <div style="display: flex;flex-flow: row">
                              <button name="btn[setstudyvarset]" value="{{ row.studyvariableset_id }}" class="button action small">Select</button>
                            </div>
                          </form>
                        </td>
                        <td>{{ row.name }}</td>
                        <td></td>
                      </tr>
                    {% endfor %}
                  </tbody>

                </table>
              </div>
            {% endif %}

          </div>
        </div>
      </div>
    {% endif %}

  {% endblock %}

  {% block subfooter_content %}
    <div style="display: flex;flex-flow: row;width: 100%">
      <div style="display: flex;flex-flow: column;flex-grow: 1;justify-content: center">
        {% if pagination1 is defined %}
          {{ partial("00element_partials/table_nav_dual_results1",[
            "nav_link":"dashin/owner/studyexport/editstudyexportset",
            "act_link":"dashin/owner/studyexport/editstudyexportset/",
            "reset_link":"dashin/owner/studyexport/editstudyexportset/",
            "form_id":"form1"
          ]) }}
        {% endif %}
      </div>
      <div style="display: flex;flex-flow: column;flex-grow: 1;justify-content: center">
        {% if pagination1 is defined %}
          {{ partial("00element_partials/table_nav_dual_results2",[
            "nav_link":"dashin/owner/studyexport/editstudyexportset",
            "act_link":"dashin/owner/studyexport/editstudyexportset/",
            "reset_link":"dashin/owner/studyexport/editstudyexportset/",
            "has_results1":has_results1,
            "has_results2":has_results2,
            "this_is":1,
            "form_id":"form1"
          ]) }}
        {% endif %}
      </div>
    </div>
  {% endblock %} 

  {% block footer %}
  {% endblock %}
