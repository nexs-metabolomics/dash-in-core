<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/studyexport/liststudyvarsets", "liststudyvarsets", current_tab, _ls_querystring_) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "liststudyvarsets" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/liststudyvarsets/{{ _ls_querystring_ }}">#}
{#      List#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Create", "/dashin/owner/studyexport/createstudyvarset", "createstudyvarset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "createstudyvarset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/createstudyvarset/{{ _ls_querystring_ }}">#}
{#      Create#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Edit", "/dashin/owner/studyexport/editstudyvarset", "editstudyvarset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "editvariableset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/editstudyvarset/{{ _ls_querystring_ }}">#}
{#      Edit#}
{#    </a>#}
{#  </div>#}

</div>
