{% if form is defined %}
  <div style="display: flex;flex-flow: column;width: 100%">
    {# <div style="display: flex;flex-flow: row nowrap;width: 100%;height: 100%;border: #fa2f55 solid 1px"> #}
    <div classc="form-container" style="align-content: flex-start;margin-bottom: 16px">
      <header>Categorical condition</header>

      {# =================================================================== #}
      {% set e = form.get("conditionname") %}
      {# =================================================================== #}
      <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
        {{ renderMessages(e) }}
        {% if e.getLabel()!="" %}
          {% if e.getUserOption("type")=="select" %}
            <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
          {% else %}
            <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
          {% endif %}
        {% endif %}
        <div class="form-field">
          {{ e.setAttribute("form","form1") }}
        </div>
      </div>

      {# =================================================================== #}
      {% set e = form.get("description") %}
      {# =================================================================== #}
      <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}" style="">
        {{ renderMessages(e) }}
        {% if e.getLabel()!="" %}
          {% if e.getUserOption("type")=="select" %}
            <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
          {% else %}
            <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
          {% endif %}
        {% endif %}
        <div class="form-field">
          {{ e.setAttribute("form","form1") }}
        </div>
      </div>

    </div>
    {# </div> #}
    <div style="display: flex;flex-flow: row nowrap;width: 100%;height: 100%;align-items: center">
      <div style="display: flex;margin-right: 16px;font-weight: bold">
        Samplingevent:
      </div>
      <div>
        {% if studyvariable_selected_maincategories is defined and studyvariable_selected_maincategories %}
          {% for row in studyvariable_selected_maincategories %}
            <button
                form="form1"
                name="btn[rmcategory][{{ row.element_id }}]"
                value="{{ row.value_column }}"
                title="Sampling event category. Press button to remove"
                style="height: 24px">
              {{ row.value_column }}
            </button>
          {% endfor %}
        {% endif %}
      </div>
    </div>
    <div style="display: flex;flex-flow: row nowrap;width: 100%;height: 100%">

      <div class="form-container" style="width: unset">

        {# =================================================================== #}
        {% set e = form.get("conditionoperator") %}
        {# =================================================================== #}

        <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}">
          {{ renderMessages(e) }}
          {% if e.getLabel()!="" %}
            {% if e.getUserOption("type")=="select" %}
              <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% else %}
              <label for="{{ e.getName() }}">{{ e.getLabel() }}</label>
            {% endif %}
          {% endif %}
          <div class="form-field">
            {{ e.setAttribute("form","form1") }}
          </div>
        </div>

        {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_form_buttons",["button_set":["submit":"Ok","apply":"Apply","reset":"reset"],"formattr":"form1"]) }}

      </div>
      <div class="form-container" style="display: flex;flex-flow: column;width: 100%;min-height: 100%">
        <div style="display: flex;margin-right: 16px;font-weight: bold">
          Samplingtimes:
        </div>
        <div style="display: flex;flex-flow: row wrap;align-content: space-evenly;height: 100%;width: 100%;border: #61b061 solid 1px;border-radius: 4px">
          {% if studyvariable_selected_subcategories is defined and studyvariable_selected_subcategories %}
            {% for row in studyvariable_selected_subcategories %}
              <button
                  form="form1"
                  name="btn[rmcategory][{{ row.element_id }}]"
                  value="{{ row.value_column }}"
                  title="Sampling time category. Press button to remove"
                  style="height: 24px">
                {{ row.value_column }}
              </button>
            {% endfor %}
          {% endif %}
        </div>
      </div>

    </div>
  </div>
{% endif %}
