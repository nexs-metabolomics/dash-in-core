<div class="card">
  {# ---------------------------------------------------------- #}

  <div class="card-row">
    <div class="card-column">
      <div class="card-title">
        {{ study_info.name }}
      </div>

    </div>
  </div>

  {# ---------------------------------------------------------- #}

  <div class="card-row">
    <div class="card-column">
      <div class="card-title">
        {% if study_info.is_public === 1 %}
          <div style="background-color: #00873d">
            Study is public
          </div>
        {% else %}
          <div style="background-color: #872f4e">
            Study is private
          </div>
        {% endif %}
      </div>

    </div>
  </div>

  {# ---------------------------------------------------------- #}
  <div class="card-row">
    <div class="card-column">
      <div class="card-title">
        {{ study_info.title }}
      </div>

    </div>
  </div>

  {# ---------------------------------------------------------- #}

  <div class="card-row">
    <div class="card-column">
      <div class="card-content">
        {{ study_info.description }}
      </div>
    </div>
  </div>

  {# ---------------------------------------------------------- #}
  <div class="card-row">
    <div class="card-column">
      <div class="card-content">
        {{ study_info.conclusion }}
      </div>
    </div>
  </div>

  {# ---------------------------------------------------------- #}

  {% if designvar_summary is defined %}

    <div class="card-row">
      <div class="card-column">

        <header>Startgroup</header>

        {% if designvar_summary.startgroup is  not defined %}

          No startgroup

        {% else %}

          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>
            </div>
            {% for item in designvar_summary.startgroup %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}
      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Center</header>

        {% if designvar_summary.center is  not defined %}

          No center

        {% else %}


          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.center %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Event</header>

        {% if designvar_summary.event is  not defined %}

          No event

        {% else %}


          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.event %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Subevent</header>

        {% if designvar_summary.subevent is  not defined %}

          No subevent

        {% else %}


          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.subevent %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Samplingevent</header>
        {% if designvar_summary.samplingevent is  not defined %}

          No sampling event

        {% else %}


          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.samplingevent %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Samplingtime</header>

        {% if designvar_summary.samplingtime is  not defined %}

          No sampling time

        {% else %}


          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  Name
                </div>
                <div class="css-th">
                  Label
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.samplingtime %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.name }}
                </div>
                <div class="css-td">
                  {{ item.label }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}

    <div class="card-row">
      <div class="card-column">

        <header>Studycontact</header>

        {% if designvar_summary.studycontact is not defined %}

          No study contacts

        {% else %}



          <div class="css-table">
            <div class="css-thead">

              <div class="css-trh">
                <div class="css-th">
                  First name
                </div>
                <div class="css-th">
                  Last name
                </div>
                <div class="css-th">
                  Description
                </div>
              </div>

            </div>
            {% for item in designvar_summary.eveent_subevent_startgroup %}
              <div class="css-tr">

                <div class="css-td">
                  {{ item.first_name }}
                </div>
                <div class="css-td">
                  {{ item.last_name }}
                </div>
                <div class="css-td">
                  {{ item.description }}
                </div>
              </div>

            {% endfor %}

          </div>

        {% endif %}

      </div>
    </div>

    {# ---------------------------------------------------------- #}


  {% endif %}

</div>
