{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit variable condition") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"condition"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarcondition_tabs_partial",["current_tab":"editcondition"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" name="btn[apply]" style="position: absolute;left: -90000px"></button>
    <form id="form1" action="/dashin/owner/studyexport/editcondition/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another condition","select_studyvarcondition","form1",studyvariablecondition_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
    {{ m_info_element("Studyrowset", studyrowset_info|default(false),"name") }}
    {{ m_info_element("Studyrowset", studyvariablecondition_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyvariablecondition_info is not defined %}

      {{ m_emptypage_filler("Please select a condition", "Select condition", "select_studyvarcondition", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          {% if studyvariablecondition_info is defined and studyvariablecondition_info %}
            <div class="card-row">
              {{ studyvariablecondition_info.studyvariablecondition_id }}
            </div>
          {% endif %}
          <div class="card-row">

            {% if condition_scenario is not defined or condition_scenario === 0 %}

              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_scenario_0") }}

            {% elseif condition_scenario === 1 %}

              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_scenario_1") }}

            {% elseif condition_scenario === 2 %}

              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_scenario_2") }}

            {% elseif condition_scenario === 3 %}

              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_scenario_3") }}

            {% elseif condition_scenario === 4 %}

              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_scenario_4") }}

            {% endif %}

          </div>

          <div class="card-row">
            <div class="condition-summary-container">
            <header>
              {{ studyvariablecondition_info.studyvariable_name }}
            </header>
            </div>
            <div class="condition-summary-container">
            </div>
            <div class="condition-summary-container">
            </div>
          </div>

          <div class="card-row">

            <div class="condition-summary-container">
              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_categorical") }}
              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_numeric") }}
            </div>

            <div class="condition-summary-container">
              {{ partial("modules/dashin/owner/studyexport/edit_studyvariablecondition_histogram") }}
            </div>

            <div class="condition-summary-container">
            </div>

          </div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
