{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("List studyrowsets") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyrowset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyrowset_tabs_partial",["current_tab":"liststudyrowsets"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/liststudyrowsets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}
      <div class="card-container">
        <header>{{ get_title() }}</header>

        {% if pagination is defined %}
          {% for row in pagination.items %}

            <div class="card{% if row.confirm is defined and row.confirm === true %} warning{% endif %}" id="{{ row.studyrowset_id }}">

              {% if row.confirm is defined and row.confirm === true %}
                <header>DELETE</header>
              {% endif %}

              <div class="card-column" id="rowno{{ loop.index }}">
                <div class="card-fieldset-header">
                  {{ row.name }}
                </div>
              </div>

              <div class="card-column">
                <div class="card-field-content">
                  {{ row.description }}
                </div>
              </div>

              <div class="card-temp-spacing">
              </div>

              <div class="card-row">

                <div class="card-column">

                  <div class="card-field-row">

                    <div class="card-field-header">
                      Approx. number of rows
                    </div>

                    <div class="card-field-content">
                      {{ row.n_studyrows }}
                    </div>

                  </div>

                </div>

                <div class="card-column">
                  <div class="card-button-box">

                    {# <form action="/dashin/owner/studyexport/list#{{ row.studyrowset_id }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"> #}
                    <form action="/dashin/owner/studyexport/liststudyrowsets/{{ _ls_querystring_ }}#rowno{{ loop.index }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
                      <div class="card-button-row">
                        {% if row.confirm is defined and row.confirm === true %}
                          {% if ROLE_ORG_ALLOW_WRITE %}
                            <button form="form1"
                                    name="btn[confirm]" value="{{ row.studyrowset_id }}"
                                    class="nds-button nds-small nds-alert"
                                    style="margin-left: 0.2rem"
                                    title="Confirm delete">Confirm
                            </button>
                          {% endif %}
                          {# no form-property: on cancel jump to #id (current position) #}
                          <button name="btn[cancel]" value="{{ row.studyrowset_id }}"
                                  class="nds-button nds-small"
                                  title="Cancel delete">Cancel
                          </button>
                        {% else %}
                          <button form="form1"
                                  name="btn[view]" value="{{ row.studyrowset_id }}"
                                  class="card-img-button"
                                  title="View"><img src="/img/icons/eye.png" style="height: 2rem;width: 2rem">
                          </button>
                          {% if ROLE_ORG_ALLOW_WRITE %}
                            <button form="form1"
                                    name="btn[edit]" value="{{ row.studyrowset_id }}"
                                    class="card-img-button"
                                    title="Edit study">
                              <img src="/img/icons/edit-v01.svg">
                            </button>
                            {# no form-property: on delete jump to #id (current position) #}
                            <button name="btn[delete]" value="{{ row.studyrowset_id }}"
                                    class="card-img-button"
                                    title="Delete study"><img src="/img/icons/delete-v02.svg">
                            </button>
                          {% endif %}
                        {% endif %}
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          {% endfor %}
        {% endif %}

      </div>
    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/import/liststudyrowsets/",
        "reset_link":"dashin/owner/import/liststudyrowsets/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
