{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit export variable set") }}
    {% if pagination1 is defined %}{% set has_results1 = true %}{% else %}{% set has_results1 = false %}{% endif %}
    {% if pagination2 is defined %}{% set has_results2 = true %}{% else %}{% set has_results2 = false %}{% endif %}

  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyvarset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarset_tabs_partial",["current_tab":"editstudyvarset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/editstudyvarset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <form id="formdr1" action="/dashin/owner/studyexport/editstudyvarset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <form id="formdr2" action="/dashin/owner/studyexport/editstudyvarset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another variable set","select_studyvarset","form1",studyvariableset_info|default(null)) }}
    <button form="formdr1" type="submit" name="btn[search1]" style="display: block;position: absolute;left: -100000px">Filter</button>
    <button form="formdr2" type="submit" name="btn[search2]" style="display: block;position: absolute;left: -100000px">Filter</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
    {{ m_info_element("Variableset", studyvariableset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyvariableset_info is not defined %}

      {{ m_emptypage_filler("Please select (or create) a variableset", "Select variableset", "select_studyvarset", "form1") }}

    {% else %}

      <div class="table-container-studyexport-card">

        <div class="studyexport-card-header-row">
          <div class="table-card-title" style="margin-left: 16px">
            {{ get_title(false) }}
          </div>
        </div>

        <div class="studyexport-card-header-row">

          <div class="studyexport-card-header-pane">
            <div style="padding: 0;font-size: 125%;font-weight: bold">
              Available
            </div>
          </div>

          <div class="studyexport-card-header-pane">
            <div style="padding: 0;font-size: 125%;font-weight: bold">
              Selected
            </div>
          </div>

        </div>

        <div class="studyexport-card-header-row">

          <div class="studyexport-card-header-pane">
            <div class="studyexport-card-header-row">
              <button form="formdr1" name="btn[addall]" class="button action xsmall">Add all</button>
              <button form="formdr1" name="btn[addfiltered]" class="button action xsmall">Add all-filtered</button>
              <button form="formdr1" name="btn[addallonpage]" class="button action xsmall">Add all-on-page</button>
            </div>
          </div>

          <div class="studyexport-card-header-pane">
            <div class="studyexport-card-header-row">
              <button form="formdr2" name="btn[removeall]" class="button action xsmall">Remove all</button>
              <button form="formdr2" name="btn[removefiltered]" class="button action xsmall">Remove all-filtered</button>
              <button form="formdr2" name="btn[removeallonpage]" class="button action xsmall">Remove all-on-page</button>
            </div>
          </div>

        </div>

        <div class="studyexport-card-row">

          {% if pagination1 is defined %}
            <div class="table-container fixed-header" style="width: 50%;height: 100%">

              <table>

                <thead>
                  <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Label</th>
                  </tr>
                </thead>

                <tbody>
                  {% for row in pagination1.items %}
                    <tr>
                      <td>
                        <input form="formdr1" type="hidden" name="varid[]" value="{{ row.studyvariable_id }}">
                        <button form="form1" name="btn[addvariable]" value="{{ row.studyvariable_id }}" class="button table-action icon">
                          {{ partial("00img_partials/v1/icons/plus-icon.svg") }}
                        </button>
                      </td>
                      <td title="{{ row.name }}">{{ row.name }}</td>
                      <td title="{{ row.label }}">{{ row.label }}</td>
                    </tr>
                  {% endfor %}
                </tbody>

              </table>
            </div>
          {% endif %}

          {% if pagination2 is defined %}
            <div class="table-container fixed-header" style="width: 50%">

              <table>

                <thead>
                  <tr>
                    <th style="width: 120px;min-width: 120px">Action</th>
                    <th>Name</th>
                    <th>Label</th>
                  </tr>
                </thead>

                <tbody>
                  {% for row in pagination2.items %}
                    <tr>
                      <td>
                        <input form="formdr2" type="hidden" name="varid[]" value="{{ row.studyvariable_id }}">
                        <form action="/dashin/owner/studyexport/editstudyvarset/{{ _ls_querystring_ }}#cond{{ loop.index }}" accept-charset="utf-8" method="post">
                          <div style="display: flex;flex-flow: row">
                            <button name="btn[removevariable]" value="{{ row.studyvariable_id }}" class="button table-action icon alert">
                              {{ partial("00img_partials/v1/icons/minus-icon.svg") }}
                            </button>
                          </div>
                        </form>
                      </td>
                      <td title="{{ row.name }}">{{ row.name }}</td>
                      <td title="{{ row.label }}">{{ row.label }}</td>
                    </tr>
                  {% endfor %}
                </tbody>

              </table>
            </div>
          {% endif %}

        </div>
      </div>
    {% endif %}

  {% endblock %}

  {% block subfooter_content %}

    {% if pagination1 is defined %}
      {{ partial("00element_partials/table_nav_dual_results1",[
        "nav_link":"dashin/owner/studyexport/editstudyvarset",
        "act_link":"dashin/owner/studyexport/editstudyvarset/",
        "reset_link":"dashin/owner/studyexport/editstudyvarset/",
        "input_name":"res2",
        "form_id":"formdr2",
        "search_term":"" ~ search_term1  ~ ""
      ]) }}
    {% endif %}

    {% if pagination2 is defined %}
      {{ partial("00element_partials/table_nav_dual_results2",[
        "nav_link":"dashin/owner/studyexport/editstudyvarset",
        "act_link":"dashin/owner/studyexport/editstudyvarset/",
        "reset_link":"dashin/owner/studyexport/editstudyvarset/",
        "input_name":"res1",
        "form_id":"formdr1",
        "search_term":"" ~ search_term2  ~ ""
      ]) }}
    {% endif %}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
