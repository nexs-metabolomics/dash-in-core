{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("List studyvariable conditions") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"condition"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarcondition_tabs_partial",["current_tab":"listconditions"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" class="button hidden-default" name="btn[search]" style="position: absolute;left: -90000px"></button>
    <form id="form1" action="/dashin/owner/studyexport/listconditions/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif pagination is not defined %}

      {{ m_emptypage_filler("There are no conditions", "Go to 'Create condition'", "goto_createcondition", "form1") }}

    {% else %}

      <div class="table-container fixed-header">
        <table class="striped">
          <thead>
            <tr>
              <th>Actions</th>
              <th>Variable name</th>
              <th>Condition name</th>
              <th>Condition</th>
            </tr>
          </thead>

          <tbody>
            {% for row in pagination.items %}
              <tr id="rowno{{ loop.index }}">
                <td>

                  {% if loop.index < 6 %}{% set loop_idx = 0 %}{% else %}{% set loop_idx = loop.index-5 %}{% endif %}

                  <form action="/dashin/owner/studyexport/listconditions/{{ current_page }}/{{ _ls_querystring_ }}#rowno{{ loop_idx }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

                    <div style="display: flex;flex-flow: row nowrap;justify-content: flex-start">
                      {% if ROLE_ORG_ALLOW_WRITE %}
                        {% if row.confirm is defined and row.confirm === true %}
                          <a href="/dashin/owner/studyexport/listconditions/{{ _ls_querystring_ }}#rowno{{ loop_idx }}" class="button cancel">Cancel</a>
                          <button name="btn[confirm]"
                                  value="{{ row.studyvariablecondition_id }}"
                                  class="button alert" title="Confirm delete">
                            Confirm
                          </button>
                        {% else %}
                          <button name="btn[edit]"
                                  value="{{ row.studyvariablecondition_id }}"
                                  title="Edit"
                                  class="button edit icon24 table-icon"
                          >
                            {{ partial("00img_partials/v1/icon48/edit-icon48-filled.svg") }}
                          </button>
                          <button name="btn[delete]"
                                  value="{{ row.studyvariablecondition_id }}"
                                  title="Delete"
                                  class="button delete icon24 table-icon"
                          >
                            {{ partial("00img_partials/v1/icon48/delete-icon48-outline.svg") }}
                          </button>
                        {% endif %}
                      {% endif %}
                    </div>
                  </form>
                </td>
                <td title="{{ row.studyvariable_name }}">{{ row.studyvariable_name }}</td>
                <td title="{{ row.condition_name }}">{{ row.condition_name }}</td>
                <td title="{{ row.condition_text }}">{{ row.condition_text }}</td>
              </tr>
            {% endfor %}
          </tbody>

        </table>
      </div>

    {% endif %}

  {% endblock %}
  {% block subfooter_content %}
    {% if pagination is defined %}
      {% set has_results=true %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/studyexport/listconditions/",
        "reset_link":"dashin/owner/studyexport/listconditions/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}
