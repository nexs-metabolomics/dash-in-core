{% if studyvariable_summary_categorical is defined %}
  <div class="card-column" style="display: flex;flex-flow: column">
    <table style="width: unset">
      <caption>
        Categories
      </caption>
      <thead>
      <th>N</th>
      <th>Name</th>
      </thead>
      <tbody>
      {% for row in studyvariable_summary_categorical %}
        <tr>
          <td style="text-align: right">
            {{ row.n }}
          </td>
          <td style="padding: 0px 8px;font-family: monospace;text-align: right">
            {% if row.is_selected %}
              <button style="background-color: #ffffff;border: #c8c8c8 solid 2px">
                {{ row.value_column }}
              </button>
            {% else %}
                <button
                    form="form1"
                    name="btn[addcategory][{{ row.element_id }}]"
                    title="Press button to add category to condition"
                    value="{{ row.value_column }}">
                  {{ row.value_column }}
                </button>
            {% endif %}
          </td>
        </tr>
      {% endfor %}
      </tbody>
    </table>
  </div>
{% endif %}
