{% if studyvariable_summary_numeric is defined %}
  <div class="card-column" style="display: flex;flex-flow: column">
    <table>
      <caption>Summary</caption>
      <thead>
      <th>Name</th>
      <th>Value</th>
      </thead>
      <tbody>
      {% for name,value in studyvariable_summary_numeric %}
        <tr>
          <td>
            {{ name }}
          </td>
          <td style="padding: 0px 8px;font-family: monospace;text-align: right">
            {{ value }}
          </td>
        </tr>
      {% endfor %}
      </tbody>
    </table>
  </div>
{% endif %}
