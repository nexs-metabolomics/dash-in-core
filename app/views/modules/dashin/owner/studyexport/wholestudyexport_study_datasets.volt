{#<div class="card">#}

  <div class="card-row">
    <div class="card-column">

      <header>Datasets</header>

      {% if study_datasets is not defined %}

        No datasets

      {% else %}

        <div class="css-table">
          <div class="css-thead">

            <div class="css-trh">
              <div class="css-th">
                Name
              </div>
              <div class="css-th">
                description
              </div>
              <div class="css-th">
                Is Public
              </div>
            </div>
          </div>
          {% for item in study_datasets %}
            <div class="css-tr">

              <div class="css-td">
                {{ item.name }}
              </div>

              <div class="css-td">
                {{ item.description }}
              </div>

              <div class="css-td">
                {% if item.is_public === 1 %}
                  <div style="background-color: #00cd00">
                    Dataset is public
                  </div>
                {% else %}
                  <div style="background-color: #ef5350">
                    Dataset is private
                  </div>
                {% endif %}
              </div>
            </div>

          {% endfor %}

        </div>

      {% endif %}
    </div>
  </div>

{#</div>#}
