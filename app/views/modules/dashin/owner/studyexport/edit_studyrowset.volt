{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit rowset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyrowset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyrowset_tabs_partial",["current_tab":"editstudyrowset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" name="btn[dummysubmit]" style="position: absolute;left: 90000px"></button>
    <form id="form1" action="/dashin/owner/studyexport/editstudyrowset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another rowset","select_studyrowset","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }}
    {{ m_info_element("Studyrowset", studyrowset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studyrowset_info is not defined %}

      {{ m_emptypage_filler("Please select a studyrowset", "Select studyrowset", "select_studyrowset", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          
          <div class="card-title">
            {{ get_title(false) }}
          </div>
          
          {% if form is defined %}
            <form action="{{ form.getAction() }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","apply":"Apply","cancel":"Cancel"]]) }}

            </form>
          {% endif %}
          {% if studyrowsubsets is not defined %}
            <div style="display: flex;flex-flow: column;justify-content: center;align-items: center;background-color: #ffc5dd;min-height: 32px;text-align: center;font-weight: bold;padding: 16px">
              You must add at least one subset
              <button form="form1" name="btn[addsubset]" class="button select" style="background-color: black;color: white">Add subset</button>
            </div>
          {% else %}
            <div class="form-buttonset" style="display: flex;flex-flow: row nowrap;justify-content: space-between;align-items: center">
              <button form="form1" name="btn[addsubset]" style="display: flex">Add subset</button>
              <div style="display: flex" title="Number of rows is based on rows in the studydesign &#013;matching the search criteria. &#013;Actual number of rows may differ.">Number of
                rows: {{ studyrowset_info.studyrow_count }}</div>
            </div>
            <div class="form-container">
              {% for row in studyrowsubsets %}
                <div class="form-fieldset">
                  <div class="{% if row.active %}subsetactive{% endif %}" style="display: flex;flex-flow: row nowrap">
                    {% if row.active %}
                      <input form="form1" type="hidden" name="activesubset" value="{{ row.studyrowsubset_id }}">
                    {% endif %}
                    <button form="form1" name="btn[deletesubset]" value="{{ row.studyrowsubset_id }}" title="Remove subset">X</button>
                    <button form="form1" name="btn[setactive]" value="{{ row.studyrowsubset_id }}" title="Set focus">F</button>

                    <form id="subsetform{{ loop.index }}" action="/dashin/owner/studyexport/editstudyrowset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
                    <button form="subsetform{{ loop.index }}" name="btn[subsetapply]" value="{{ row.studyrowsubset_id }}" title="Update subset">U</button>
                    <input form="subsetform{{ loop.index }}" name="subsetname[{{ row.studyrowsubset_id }}]" value="{{ row.name }}">

                    <div style="border: #000000 solid 1px;width: 100%">
                      {% if row.conditions %}
                        {% for cond in row.conditions %}
                          <button form="form1" name="btn[rmcondition]" value="cond[{{ row.studyrowsubset_id }},{{ cond.id }}]">{{ cond.name }}</button>
                        {% endfor %}
                      {% endif %}
                    </div>
                  </div>
                </div>
              {% endfor %}
            </div>
          {% endif %}

        </div>
        <div class="card">
          <div class="card-title">
            Available conditions
          </div>
          {% if studyvariableconditions is defined %}
            <div class="table-container fixed-header">
              <table>
                <thead>
                  <tr>
                    <th>Add</th>
                    <th>Name</th>
                    <th>Condition</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>
                  {% for row in studyvariableconditions %}
                    <tr>
                      <td>
                        <button form="form1" name="btn[addcondition]" value="{{ row.studyvariablecondition_id }}" title="Add condition">A</button>
                      </td>
                      <td>{{ row.condition_name }}</td>
                      <td>{{ row.condition_text }}</td>
                      <td>
                        <button form="form1" name="btn[editcondition]" value="{{ row.studyvariablecondition_id }}" title="Edit condition">E</button>
                      </td>
                    </tr>
                  {% endfor %}
                </tbody>
              </table>
            </div>
          {% endif %}
        </div>
      </div>
    {% endif %}
  {% endblock %}
