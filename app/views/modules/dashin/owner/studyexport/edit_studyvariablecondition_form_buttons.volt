  {% if button_set is defined and button_set %}
    {% if formattr is not defined %}{% set form_ref = "" %}{% else %}{% set form_ref = " form=" ~ formattr ~ "" %}{% endif %}
    <div class="form-buttonset condition">
      {% for name,text in button_set %}
        <button {{ form_ref}} name="btn[{{ name }}]" class="button">
          {{ text }}
        </button>
      {% endfor %}

    </div>
  {% endif %}
