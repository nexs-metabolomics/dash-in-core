<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("Condition", "/dashin/owner/studyexport/listconditions", "condition", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Rowset", "/dashin/owner/studyexport/liststudyrowsets", "studyrowset", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Variableset", "/dashin/owner/studyexport/liststudyvarsets", "studyvarset", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Study data export", "/dashin/owner/studyexport/editstudyexportset", "studyexportset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

</div>
