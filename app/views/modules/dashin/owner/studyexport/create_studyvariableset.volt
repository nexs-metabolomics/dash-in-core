{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Create data filter") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"studyvarset"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_studyvarset_tabs_partial",["current_tab":"createstudyvarset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studyexport/createstudyvarset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[select_study]">Select study</button>
  {% endblock %}

  {% block subheader_info %}
    {% if study_info is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Study
        </div>
        <div class="info-element-content">
          {{ study_info.name }}
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          {% if form is defined %}
            <form action="{{ form.getAction() }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","cancel":"Cancel"]]) }}

            </form>
          {% endif %}
        </div>
      </div>
    {% endif %}
  {% endblock %}
