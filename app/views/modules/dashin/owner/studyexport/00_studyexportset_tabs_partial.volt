<div class="tabs-row">
  
  {{ m_menu_tabentry("List", "/dashin/owner/studyexport/liststudyexportsets", "liststudyexportsets", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "liststudyexportsets" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/liststudyexportsets/{{ _ls_querystring_ }}">#}
{#    <a href="#" style="text-decoration: line-through 2px" title="Coming soon...">#}
{#      List#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Create", "/dashin/owner/studyexport/createstudyexportset", "createstudyexportset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "createstudyexportset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/createstudyexportset/{{ _ls_querystring_ }}">#}
{#      Create#}
{#    </a>#}
{#  </div>#}

  {{ m_menu_tabentry("Edit / export", "/dashin/owner/studyexport/editstudyexportset", "editstudyexportset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  <div class="tab{% if current_tab is defined and current_tab === "editstudyexportset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/editstudyexportset/{{ _ls_querystring_ }}">#}
{#      Edit / export#}
{#    </a>#}
{#  </div>#}

{#  <div class="tab{% if current_tab is defined and current_tab === "viewstudyexportset" %} current{% endif %}">#}
{#    <a href="/dashin/owner/studyexport/viewstudyexportset/{{ _ls_querystring_ }}">#}
{#      View / export#}
{#    </a>#}
{#  </div>#}

</div>
