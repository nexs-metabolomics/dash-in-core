{% extends "layouts/root-subnavpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'study',"show_submenu":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'study',"show_submenu":1,"current":"datafilters_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-studies">
        {{ partial("00img_partials/v1/icons/studies-icon.svg") }}
      </div>
      <a href="/dashin/owner/import/index/{{ _ls_querystring_ }}">Study export</a>
    </div>
  {% endblock %}
