{% extends "modules/dashin/owner/studyexport/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Export whole study") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studyexport/00_tabs_partial",["current_tab":"exportstudy"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/studyexport/00_wholestudyexport_tabs_partial",["current_tab":"exportstudy"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" name="btn[dummysubmit]" style="position: absolute;left: 90000px"></button>
    <form id="form1" action="/dashin/owner/studyexport/exportstudy/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {#    {{ partial("modules/dashin/owner/studyexport/study_info_partial") }} #}
    {{ m_info_element("Study", study_info|default(false),"name") }}
    {{ m_info_element("Studyrowset", studyrowset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container card-table-container">
        {# ---------------------------------------------------------- #}

        <div class="card-row">
          <div class="card-column">
            <div class="card-title">
              {{ study_info.name }}
            </div>

          </div>
        </div>

        {# ---------------------------------------------------------- #}

        {# ---------------------------------------------------------- #}
        <div class="card-row">
          <div class="card-column">
            <div class="card-title">
              {{ study_info.title }}
            </div>

          </div>
        </div>

        {# ---------------------------------------------------------- #}

        <div class="card-row">
          <div class="card-column">
            <div class="card-content">
              {{ study_info.description }}
            </div>
          </div>
        </div>

        {# ---------------------------------------------------------- #}
        <div class="card-row">
          <div class="card-column">
            <div class="card-content">
              {{ study_info.conclusion }}
            </div>
          </div>
        </div>

        {# ---------------------------------------------------------- #}
        <div class="card">
          <div class="card-row">
            <div class="card-column">
              {#              <a href="/dashin/owner/study/editpermissions/{{ _ls_querystring_ }}" class="button" target="_blank">Go to permissions</a> #}
              <button form="form1" name="btn[go_permissions]" class="button">
                Go to permissions
              </button>
            </div>
          </div>

          <div class="card-row">
            <div class="card-column">
              <div class="card-title">
                {% if study_info.is_public === 1 %}
                  <div style="background-color: #00cd00">
                    Study is public
                  </div>
                {% else %}
                  <div style="background-color: #ef5350">
                    Study is private
                  </div>
                {% endif %}
              </div>

            </div>
          </div>

          {#          {{ partial("modules/dashin/owner/studyexport/wholestudyexport_designvar_summary") }} #}
          {{ partial("modules/dashin/owner/studyexport/wholestudyexport_study_datasets") }}
          {{ partial("modules/dashin/owner/studyexport/wholestudyexport_study_supportfiles") }}

        </div>

      </div>

    {% endif %}

  {% endblock %}
