{% if studyvariable_summary_histogram is defined %}
  <div class="card-column" style="display: flex;flex-flow: column">
    <table>
      <caption>
        Histogram
      </caption>
      <thead>
      <th>Lower bound</th>
      <th>N</th>
      </thead>
      <tbody>
      {% for row in studyvariable_summary_histogram %}
        <tr>
          <td style="padding: 0px 8px;font-family: monospace;text-align: right">
            {{ row.bin_lower }}
          </td>
          <td style="padding: 0px 8px;font-family: monospace">
            {{ row.n }}
          </td>
        </tr>
      {% endfor %}
      </tbody>
    </table>
  </div>
{% endif %}
