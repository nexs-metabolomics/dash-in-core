<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/dashin/owner/datacolumntype/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create new", "/dashin/owner/datacolumntype/create", "create", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/datacolumntype/edit", "edit", current_tab, _ls_querystring_) }}
  
</div>
