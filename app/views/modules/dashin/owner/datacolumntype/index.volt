{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Column type") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_admin":1,"show_submenu_admin_datacolumntype":1,"current":"datacolumntype_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/dashin/owner/datacolumntype/create/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Create datacolumntype
              </header>
              <p>Create a new datacolumntype</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/datacolumntype/edit/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit datacolumntype
              </header>
              <p>Edit name and description of an datacolumntype</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/datacolumntype/list/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List datacolumntypes
              </header>
              <p>List available datacolumntypes</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
