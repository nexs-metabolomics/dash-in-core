{% extends "modules/dashin/owner/datacolumntype/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit column type") }}
  {% endblock %}
 
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/datacolumntype/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/datacolumntype/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="nds-button" name="btn[select_datacolumntype]">Select datacolumntype</button>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

    </div>

  {% endblock %}
