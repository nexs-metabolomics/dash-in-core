{% extends "modules/dashin/owner/datacolumntype/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List column types") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}
 
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/datacolumntype/00_tabs_partial",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/datacolumntype/list/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="table-container">

      <table style="table-layout: fixed;width: 100%">
        <thead>
          <tr>
            <th style="width: 10rem">Actions</th>
            <th>Shortname</th>
            <th>Name</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>
                  <div class="row">
                    {% if row.confirm is defined and row.confirm === true %}
                      <button form="form1" name="btn[confirm]" value="{{ row.datacolumntype_id }}" class="button confirm">Confirm</button>
                      <a href="/dashin/owner/datacolumntype/list/{{ _ls_querystring_ }}" class="button cancel">Cancel</a>
                    {% else %}
                      <a href="/dashin/owner/datacolumntype/edit/{{ _ls_querystring_ }}" class="button submit">Edit</a>
                      <button form="form1" name="btn[delete]" value="{{ row.datacolumntype_id }}" class="button delete">Remove</button>
                    {% endif %}
                  </div>
                </td>
                <td>{{ row.shortname }}</td>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
              </tr>
            {% endfor %}
          {% endif %}
        </tbody>
      </table>
    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/datacolumntype/list",
        "reset_link":"dashin/owner/datacolumntype/list/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
