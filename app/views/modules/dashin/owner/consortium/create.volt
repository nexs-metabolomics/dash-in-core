{% extends "modules/dashin/owner/consortium/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Create consortium") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/consortium/00_tabs_partial",["current_tab":"create"]) }}
  {% endblock %}

  {% block subheader_action %}
      <form id="form1" action="/dashin/owner/consort/create/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", study_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

    </div>

  {% endblock %}
