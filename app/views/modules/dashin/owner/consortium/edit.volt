{% extends "modules/dashin/owner/consortium/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit consortium") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/consortium/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/consort/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another consortium","select_consort","form1",consortium_info|default(null)) }}
  {% endblock %}

  {% block page_content %}

    {% if consortium_info is not defined %}

      {{ m_emptypage_filler("Please select a consortium", "Select consortium", "select_consort", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Save","button submit"],["apply","Apply","button apply"],["cancel","Cancel","button cancel"]],"form1") }}

    {% endif %}

  {% endblock %}
