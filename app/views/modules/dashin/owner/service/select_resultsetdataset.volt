{% extends "layouts/base-selectpage.volt" %}
  {% block set_params %}
    {{ set_title("Select Dataset") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/service/select/resultsetdataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[cancel]" class="button selectpage cancel">Cancel</button>
  {% endblock %}

  {% block page_content %}

    <div class="table-container fixed-header-select">

      <table>

        <thead>
        <tr>
          <th style="width: 5rem">Actions</th>
          <th>Study</th>
          <th>Dataset</th>
          <th>Number of variables</th>
        </tr>
        </thead>

        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td>
                <button form="form1" name="btn[select]" value="{{ row.dataset_id }}" class="button selectpage select">Select</button>
              </td>
              <td>{{ row.study_name }}</td>
              <td>{{ row.dataset_name }}</td>
              <td>{{ row.n }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}

  {% block page_subfooter %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/service/select/resultsetdataset",
        "reset_link":"dashin/owner/service/select/resultsetdataset/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}
