{% extends "layouts/base-selectpage.volt" %}
  {% block set_params %}
    {{ set_title("Select resultset") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/service/select/searchresultset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[cancel]" class="button selectpage cancel">cancel</button>
  {% endblock %}

  {% block page_content %}

    <div class="table-container fixed-header-select">

      <form action="/dashin/owner/service/select/searchresultset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
        <table class="data-table">

          <thead>
          <tr>
            <th class="td-hide">Actions</th>
            <th class="">Name</th>
            <th class="">Description</th>
          </tr>
          </thead>

          <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr class="tbl-row">
                <td>
                  <button form="form1" name="btn[select]" value="{{ row.resultset_id }}" class="button selectpage select">Select</button>
                </td>
                <td>{{ row.name }}</td>
                <td class="td-hide">{{ row.description }}</td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>

        </table>
      </form>
    </div>
  {% endblock %}

  {% block page_subfooter %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/service/select/searchresultset",
        "reset_link":"dashin/owner/service/select/searchresultset/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}
