{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Select column type") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
  {% endblock %}
  {% block tab_menu %}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/dashin/owner/service/select/datacolumntype/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[cancel]" class="button">Cancel</button>
  {% endblock %}
  {% block nav_header %}
    {{ partial("00element_partials/nds_table_nav_search_form",[
      "nav_link":"dashin/owner/service/select/datacolumntype",
      "act_link":"dashin/owner/service/select/datacolumntype",
      "reset_link":"dashin/owner/service/select/datacolumntype/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}
  {% block content2 %}

    <div class="table-container">

      <table>

        <thead>
        <tr>
          <th style="width: 5rem">Actions</th>
          <th>Shortname</th>
          <th>Name</th>
          <th>Description</th>
        </tr>
        </thead>

        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td>
                <button form="form1" name="btn[select]" value="{{ row.datacolumntype_id }}" class="button selectpage select">Select</button>
              </td>
              <td>{{ row.shortname }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}
