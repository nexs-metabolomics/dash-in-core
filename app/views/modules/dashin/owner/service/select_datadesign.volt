{% extends "layouts/base-selectpage.volt" %}
  {% block set_params %}
    {{ set_title("Select Dataset") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/service/select/datadesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" name="btn[cancel]" class="button selectpage cancel">Cancel</button>
  {% endblock %}

  {% block page_content %}

    <div class="table-container fixed-header-select">
    
      <table>

        <thead>
        <tr>
          <th style="width: 5rem">Actions</th>
          <th style="width: 1.5rem" title="event">ev</th>
          <th style="width: 1.5rem" title="subevent">se</th>
          <th style="width: 1.5rem" title="startgroup">sg</th>
          <th style="width: 1.5rem" title="subject">sb</th>
          <th style="width: 1.5rem" title="samplingevent">sm</th>
          <th style="width: 1.5rem" title="samplingtime">st</th>
          <th>Name</th>
          <th>Description</th>
        </tr>
        </thead>

        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td>
                <button form="form1" name="btn[select]" value="{{ row.dataset_id }}" class="button selectpage select">Select</button>
              </td>
              <td class="align-center">{{ row.event }}</td>
              <td class="align-center">{{ row.subevent }}</td>
              <td class="align-center">{{ row.startgroup }}</td>
              <td class="align-center">{{ row.subject }}</td>
              <td class="align-center">{{ row.samplingevent }}</td>
              <td class="align-center">{{ row.samplingtime }}</td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}

  {% block page_subfooter %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/service/select/datadesign",
        "reset_link":"dashin/owner/service/select/datadesign/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer %}
  {% endblock %}
