{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Select Sampleset") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <form id="form1" action="/dashin/owner/service/select/sampleset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

    <button form="form1" name="btn[cancel]" class="button selectpage cancel">cancel</button>

    <div class="table-container">
      {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

      {{ partial("00element_partials/nds_table_nav_search_form",[
        "nav_link":"dashin/owner/service/select/sampleset",
        "act_link":"dashin/owner/service/select/sampleset",
        "reset_link":"dashin/owner/service/select/sampleset/1/1",
        "has_results":has_results
      ]) }}

      <table>

        <thead>
        <tr>
          <th style="width: 5rem">Actions</th>
          <th>Name</th>
          <th>Description</th>
        </tr>
        </thead>

        <tbody>
        {% if pagination is defined %}
          {% for row in pagination.items %}
            <tr>
              <td>
                <button form="form1" name="btn[select]" value="{{ row.sampleset_id }}" class="button selectpage select">Select</button>
              </td>
              <td>{{ row.name }}</td>
              <td>{{ row.description }}</td>
            </tr>
          {% endfor %}
        {% endif %}
        </tbody>

      </table>
    </div>
  {% endblock %}
