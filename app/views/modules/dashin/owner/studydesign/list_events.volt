{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List events") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"listevents"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/listevents/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container">

        <form action="/dashin/owner/studydesign/listevents/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

          <div class="card table-container fixed-header">
            <div class="card-title">
              {{ get_title(false) }}
            </div>

            <table>

              <thead>
                <tr>
                  <th>Name</th>
                  <th>Label</th>
                  <th>Description</th>
                  <th>Datasets</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                {% if pagination is defined %}
                  {% for row in pagination.items %}
                    <tr>
                      <td>{{ row.name }}</td>
                      <td>{{ row.label }}</td>
                      <td>{{ row.description }}</td>
                      <td class="align-center">{{ row.n_datasets }}</td>
                      <td class="align-right">
                        <div style="display: flex;flex-flow: row nowrap;justify-content: flex-start">
                          {% if ROLE_ORG_ALLOW_WRITE %}
                            {% if row.n_datasets>0 %}
                              <button name="btn[edit]"
                                      value="{{ row.event_id }}"
                                      class="nds-card-img-button">
                                <img src="/img/icons/edit-v01.svg">
                              </button>

                              <button class="nds-card-img-button" style="cursor: auto" readonly="readonly">
                                <img src="/img/icons/delete-disabled-v02.svg">
                              </button>
                            {% elseif row.confirm is defined and row.confirm === true %}
                              <button name="btn[confirm]" value="{{ row.event_id }}"
                                      class="nds-button nds-small nds-alert"
                                      title="Confirm delete">Confirm
                              </button>
                              <a href="/dashin/owner/studydesign/listevents/{{ _ls_querystring_ }}" class="nds-button nds-small">Cancel</a>
                            {% else %}
                              <button name="btn[edit]"
                                      value="{{ row.event_id }}"
                                      class="nds-card-img-button">
                                <img src="/img/icons/edit-v01.svg">
                              </button>
                              <button name="btn[delete]"
                                      value="{{ row.event_id }}"
                                      class="nds-card-img-button"
                                      title="Delete">
                                <img src="/img/icons/delete-v02.svg">
                              </button>
                            {% endif %}
                          {% endif %}
                        </div>
                      </td>
                    </tr>
                  {% endfor %}
                {% endif %}
              </tbody>

            </table>
          </div>
        </form>
      </div>
    {% endif %}
  {% endblock %}
