{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit sampling event") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"editsampling"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/editsampling/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another sampling event","selectsampling","form1",sampling_event_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Samplingevent", sampling_event_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif sampling_event_info is not defined %}

      {{ m_emptypage_filler("Please select a samplingevent", "Select samplingevent", "selectsampling", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">

          <div class="card-title">
            {{ get_title(false) }}
          </div>

          {% if form is defined %}
            <form id="form2" action="/dashin/owner/studydesign/editsampling/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop") }}

              {% if subform is defined %}
                <div class="sampling-subform-container">
                  <div class="sampling-subform-inner">
                    {% for e in subform %}

                      {% if e.getAttribute('required') %}
                        {% set required_asterisk = " * " %}
                        {% set required_popup = ' title="Required field"' %}
                      {% else %}
                        {% set required_asterisk = "" %}
                        {% set required_popup = "" %}
                      {% endif %}

                      <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }}>
                        {% if e.getLabel()!="" %}
                          {% if e.getUserOption("type")=="select" %}
                            <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                          {% else %}
                            <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                          {% endif %}
                        {% endif %}
                        <div class="form-field">
                          {{ e.render() }}
                        </div>
                      </div>
                    {% endfor %}
                  </div>
                </div>

              {% endif %}

              <div class="sampletime-form-container">
                <div class="card-table-container fixed-header">
                  <table>
                    <thead>
                      <tr>
                        <th>Time</th>
                        <th>Move</th>
                        <th>Name</th>
                        <th>Label</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      {% if sampling_times is defined %}
                        {% for row in sampling_times %}
                          <tr>
                            <td>{{ row.ordinal_time }}</td>
                            <td>
                              <div style="display: flex;flex-flow: row nowrap;width: 50px">
                                {% if not row.first %}
                                  <button name="btn[moveup]" value="{{ row.samplingtime_id }}" style="display: flex;width: 100%">↑</button>
                                {% else %}
                                  <button style="display: flex;width: 100%">-</button>
                                {% endif %}
                                {% if not row.last %}
                                  <button name="btn[movedown]" value="{{ row.samplingtime_id }}" style="display: flex;width: 100%">↓</button>
                                {% else %}
                                  <button style="display: flex;width: 100%">-</button>
                                {% endif %}
                              </div>
                            </td>
                            <td>
                              {% if row.n_datasets > 0 %}
                                <input name="st[{{ row.samplingtime_id }}][name]" value="{{ row.name }}" readonly>
                              {% else %}
                                <input name="st[{{ row.samplingtime_id }}][name]" value="{{ row.name }}">
                              {% endif %}
                            </td>
                            <td>
                              <input name="st[{{ row.samplingtime_id }}][label]" value="{{ row.label }}">
                            </td>
                            <td>
                              <input name="st[{{ row.samplingtime_id }}][description]" value="{{ row.description }}">
                            </td>
                          </tr>
                        {% endfor %}
                      {% endif %}
                    </tbody>
                  </table>
                </div>
                <div class="sampletime-button-container">
                  <div class="sampletime-buttonset">
                    <button form="form2" name="btn[addtime]" class="button" title="Add time">
                      +
                    </button>
                    <button form="form2" name="btn[removetime]" class="button" title="Remove time">
                      -
                    </button>
                  </div>
                </div>

              </div>

              <div class="form-container">
                <div class="form-buttonset">
                  <button name="btn[submit]" class="nds-button nds-hollow">
                    Ok
                  </button>
                  <button name="btn[apply]" class="nds-button nds-hollow">
                    Apply
                  </button>
                </div>
              </div>

            </form>

          {% endif %}

        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block jsscripts %}
  {% endblock %}
