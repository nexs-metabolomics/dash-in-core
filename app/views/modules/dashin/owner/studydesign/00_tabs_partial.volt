{% set _ls_querystring_ = localsession.getQuerystring("?") %}
<div class="tabs-row">

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Study design
    </div>

    <div class="tab-actions">

      <a href="/dashin/owner/studydesign/viewstudydesign/{{ _ls_querystring_ }}" title="View study design"
         class="{% if current_tab is defined and current_tab === "viewstudydesign" %}current{% endif %}">
        <img src="/img/ng/svg/actions/view.svg">
      </a>

      {{ m_studydesign_menu_tabentry("Import study design", "/dashin/owner/studydesign/importdatadesign", "importdatadesign", "/img/ng/svg/actions/upload.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

{#      <a href="/dashin/owner/studydesign/importdatadesign/{{ _ls_querystring_ }}" title="Import study design"#}
{#         class="{% if current_tab is defined and current_tab === "importdatadesign" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/upload.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Subject
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create study-subject", "/dashin/owner/subject/createstudysubject", "createstudysubject", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/subject/createstudysubject/{{ _ls_querystring_ }}" title="Create study-subject"#}
{#         class="{% if current_tab is defined and current_tab === "createstudysubject" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/create.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("List study-subjects", "/dashin/owner/subject/liststudysubjects", "liststudysubjects", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
{#      <a href="/dashin/owner/subject/liststudysubjects/{{ _ls_querystring_ }}" title="List study-subjects"#}
{#         class="{% if current_tab is defined and current_tab === "liststudysubjects" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/list.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Edit study-subject", "/dashin/owner/subject/editstudysubject", "editstudysubject", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/subject/editstudysubject/{{ _ls_querystring_ }}" title="Edit study-subject"#}
{#         class="{% if current_tab is defined and current_tab === "editstudysubject" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/edit.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Center
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create center", "/dashin/owner/studydesign/createcenter", "createcenter", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
      {{ m_studydesign_menu_tabentry("List centers", "/dashin/owner/studydesign/listcenters", "listcenters", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
      {{ m_studydesign_menu_tabentry("Edit center", "/dashin/owner/studydesign/editcenter", "editcenter", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Startgroup
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create startgroup", "/dashin/owner/studydesign/createstartgroup", "createstartgroup", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/createstartgroup/{{ _ls_querystring_ }}" title="Create startgroup"#}
{#         class="{% if current_tab is defined and current_tab === "createstartgroup" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/create.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("List startgroups", "/dashin/owner/studydesign/liststartgroups", "liststartgroups", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
{#      <a href="/dashin/owner/studydesign/liststartgroups/{{ _ls_querystring_ }}" title="List startgroups"#}
{#         class="{% if current_tab is defined and current_tab === "liststartgroups" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/list.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Edit startgroup", "/dashin/owner/studydesign/editstartgroup", "editstartgroup", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/editstartgroup/{{ _ls_querystring_ }}" title="Edit startgroup"#}
{#         class="{% if current_tab is defined and current_tab === "editstartgroup" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/edit.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Event
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create event", "/dashin/owner/studydesign/createevent", "createevent", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/createevent/{{ _ls_querystring_ }}" title="Create event"#}
{#         class="{% if current_tab is defined and current_tab === "createevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/create.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("List events", "/dashin/owner/studydesign/listevents", "listevents", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
{#      <a href="/dashin/owner/studydesign/listevents/{{ _ls_querystring_ }}" title="List events"#}
{#         class="{% if current_tab is defined and current_tab === "listevents" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/list.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Edit event", "/dashin/owner/studydesign/editevent", "editevent", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/editevent/{{ _ls_querystring_ }}" title="Edit event"#}
{#         class="{% if current_tab is defined and current_tab === "editevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/edit.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Subevent
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create subevent", "/dashin/owner/studydesign/createsubevent", "createsubevent", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/createsubevent/{{ _ls_querystring_ }}" title="Create subevent"#}
{#         class="{% if current_tab is defined and current_tab === "createsubevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/create.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("List subevents", "/dashin/owner/studydesign/listsubevents", "listsubevents", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
{#      <a href="/dashin/owner/studydesign/listsubevents/{{ _ls_querystring_ }}" title="List subevents"#}
{#         class="{% if current_tab is defined and current_tab === "listsubevents" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/list.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Edit subevent", "/dashin/owner/studydesign/editsubevent", "editsubevent", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/editsubevent/{{ _ls_querystring_ }}" title="Edit subevent"#}
{#         class="{% if current_tab is defined and current_tab === "editsubevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/edit.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Add startgroup to subevent", "/dashin/owner/studydesign/addgrouptosubevent", "addgrouptosubevent", "/img/ng/svg/actions/add.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/addgrouptosubevent/{{ _ls_querystring_ }}" title="Add startgroup to subevent"#}
{#         class="{% if current_tab is defined and current_tab === "addgrouptosubevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/add.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}

  {# ======================================================================================= #}
  <div class="tab-studydesign" style="margin-bottom: 1rem">

    <div class="tab-header">
      Sampling
    </div>


    <div class="tab-actions">

      {{ m_studydesign_menu_tabentry("Create sampling event", "/dashin/owner/studydesign/createsamplingevent", "createsamplingevent", "/img/ng/svg/actions/create.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/createsamplingevent/{{ _ls_querystring_ }}" title="Create sampling event"#}
{#         class="{% if current_tab is defined and current_tab === "createsamplingevent" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/create.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("List sampling events", "/dashin/owner/studydesign/listsamplingevents", "listsamplingevents", "/img/ng/svg/actions/list.svg", current_tab, _ls_querystring_) }}
{#      <a href="/dashin/owner/studydesign/listsamplingevents/{{ _ls_querystring_ }}" title="List sampling events"#}
{#         class="{% if current_tab is defined and current_tab === "listsamplingevents" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/list.svg">#}
{#      </a>#}

      {{ m_studydesign_menu_tabentry("Edit sampling events", "/dashin/owner/studydesign/editsampling", "editsampling", "/img/ng/svg/actions/edit.svg", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#      <a href="/dashin/owner/studydesign/editsampling/{{ _ls_querystring_ }}" title="Edit sampling events"#}
{#         class="{% if current_tab is defined and current_tab === "editsampling" %}current{% endif %}">#}
{#        <img src="/img/ng/svg/actions/edit.svg">#}
{#      </a>#}

    </div>

  </div>
  {# ======================================================================================= #}


</div>
