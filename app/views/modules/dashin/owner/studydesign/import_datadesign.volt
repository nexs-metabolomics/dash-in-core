{# {% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %} #}
{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Import dataset design") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"datasets"]) }}
  {% endblock %}

  {% block subheader_subnav %}
{#    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"importdatadesign"]) }}#}
    {{ partial("modules/dashin/owner/study/00-study-datasets-subnav",["current_tab":"importdatadesign"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/importdatadesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another dataset","selectdatadesign","form1",dataset_info|default(null)) }}
    {#    <button form="form1" class="button select" name="btn[selectdatadesign]">Select dataset</button> #}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}
      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}
    {% elseif dataset_info is not defined %}
      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "selectdatadesign", "form1") }}
    {% else %}

      <div class="card-container">

        <div class="card">
          <div class="card-title">
            {{ get_title() }}
          </div>

          <div class="card-row">

            <div class="card-column">

              {% if dataset_info is defined %}

                <div class="nds-column-stretch">
                  <div class="nds-card-level-2 nds-table-container" title="{{ dataset_info.dataset_id }}">
                    <header>Dataset</header>
                    {# {{ dataset_info.name }} #}
                    <span>Unique design values</span>
                    {% if count_distinct is defined %}
                      {{ count_distinct }}
                    {% endif %}
                  </div>
                </div>
              {% endif %}
            </div>
          </div>

          {% if not (design_exists is defined and design_exists) %}

          {% else %}

            <div class="card-row">

              <div class="card-column">
                <div class="nds-card-level-2 nds-table-container">
                  <header class="nds-view-row-header">
                    {% if design_imported is defined and design_imported === 1 %}
                      Design imported
                    {% else %}
                      Design not imported
                      {% if confirm_import is defined and confirm_import %}
                        <button form="form1" name="btn[confirm]" type="submit" class="nds-button nds-alert">Confirm</button>
                      {% else %}
                        {% if show_importbtn is defined and show_importbtn %}
                          <button form="form1" name="btn[import]" type="submit" class="nds-button nds-hollow nds-alert">Import</button>
                        {% endif %}
                      {% endif %}
                    {% endif %}
                  </header>
                  {% if import_result is defined and import_result %}
                    <table>
                      <tr>
                        <th>Name</th>
                        <th>Affected rows</th>
                      </tr>
                      {% for row in import_result %}
                        <tr>
                          <td>{{ row.name }}</td>
                          <td>{{ row.affected_rows }}</td>
                        </tr>
                      {% endfor %}
                    </table>
                  {% endif %}
                </div>
              </div>
            </div>

            {# ================================================================= #}
            {# Design summary #}
            {# ================================================================= #}
            <div class="card-row">
              <div class="card-table-container">
                <header>Design summary</header>

                {% if design_summary is defined and design_summary %}
                  <table>
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Number of unique values</th>
                        <th>Varname</th>
                        <th>Autogen</th>
                      </tr>
                    </thead>
                    <tbody>
                      {% for row in design_summary %}
                        <tr>
                          <td>{{ row.variabletype_label }}</td>
                          <td class="align-right">{{ row.n }}</td>
                          <td class="align-right">{{ row.variablename }}</td>
                          <td class="align-right">{{ row.autogenerated }}</td>
                        </tr>
                      {% endfor %}
                    </tbody>
                  </table>
                {% endif %}
              </div>
            </div>

            {# ================================================================= #}
            {# Event #}
            {# ================================================================= #}
            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Events
                  </summary>

                  <div class="table-container">
                    {% if event is defined and event %}
                      <table>
                        <thead>
                          <tr>
                            <th>Event names</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in event %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Subevent #}
            {# ================================================================= #}

            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Subevents
                  </summary>

                  <div class="table-container">
                    {% if subevent is defined and subevent %}
                      <table>
                        <thead>
                          <tr>
                            <th>Subevent names</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in subevent %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Samplingevent #}
            {# ================================================================= #}

            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Sampling events
                  </summary>

                  <div class="table-container">
                    {% if samplingevent is defined and samplingevent %}
                      <table>
                        <thead>
                          <tr>
                            <th>Samplingevents</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in samplingevent %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Samplingtime #}
            {# ================================================================= #}

            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Sampling time
                  </summary>

                  <div class="table-container">
                    {% if samplingtime is defined and samplingtime %}
                      <table>
                        <thead>
                          <tr>
                            <th>Samplingevent</th>
                            <th>Samplingtime</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in samplingtime %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.valuename2 }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Startgroup - subject #}
            {# ================================================================= #}
            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Startgroup
                  </summary>

                  <div class="table-container">
                    {% if startgroup is defined and startgroup %}
                      <table>
                        <thead>
                          <tr>
                            <th>Startgroups</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in startgroup %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Center #}
            {# ================================================================= #}
            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Centers
                  </summary>

                  <div class="table-container">
                    {% if center is defined and center %}
                      <table>
                        <thead>
                          <tr>
                            <th>Centers</th>
                            <th>Count</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in center %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.n }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>

            {# ================================================================= #}
            {# Subject #}
            {# ================================================================= #}
            <div class="card-row">
              <div class="design-summary-box">
                <details>

                  <summary title="Click for details">
                    Subjects
                  </summary>

                  <div class="table-container">
                    {% if subject is defined and subject %}
                      <table>
                        <thead>
                          <tr>
                            <th>Subject</th>
                            <th>Startgroup</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for row in subject %}
                            <tr>
                              <td>{{ row.valuename }}</td>
                              <td>{{ row.valuename2 }}</td>
                            </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                    {% endif %}
                  </div>

                </details>
              </div>
            </div>
          {% endif %}
        </div>

      </div>
    {% endif %}
  {% endblock %}
