{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View study design") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"viewstudydesign"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/viewstudydesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {% if study_info is defined %}
      <button form="form1" class="button select" name="btn[select_study]">Select study</button>
    {% endif %}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
          
          <div style="display: flex;flex-flow: row;justify-content: space-between;margin-bottom: 16px">
            
            <div class="card-title">
              {{ get_title() }}
            </div>
            
            {% if design_exists %}
              {% if confirm_delete %}
                <div style="display: flex;flex-flow: row">
                  <button form="form1" name="btn[confirm]" class="button delete alert">Confirm</button>
                  <button form="form1" name="btn[cancel]" class="button delete alert">Cancel</button>
                </div>
              {% else %}
                <button form="form1" name="btn[delete]" class="button delete alert">Delete design</button>
              {% endif %}
            {% endif %}
          
          </div>

          <div class="card-row">
            {{ study_info.name }}
          </div>
          <div class="card-row">
            <span style="color: #00bb1f">[{{ study_info.study_id }}]</span>
          </div>

          {# ================================================================= #}
          {# Event - subevent #}
          {# ================================================================= #}

          <div class="card-row">
            <div class="design-summary-box">
              <details>

                <summary title="Click for details">
                  Events
                </summary>

                <div class="table-container">
                  {% if event is defined and event %}
                    <table>
                      <thead>
                        <tr>
                          <th>Names</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in event %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>
              </details>

            </div>

          </div>
          <div class="card-row">

            {# ===================== #}

            <div class="design-summary-box">

              <details>

                <summary title="Click for details">
                  Subevents
                </summary>

                <div class="table-container">
                  {% if subevent is defined and subevent %}
                    <table>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Type</th>
                          <th>Comment</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in subevent %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                            <td>{{ row.intervention_type }}</td>
                            <td>{{ row.row_comment }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>
              </details>

            </div>
          </div>

          {# ================================================================= #}
          {# Samplingevent - samplingtime #}
          {# ================================================================= #}
          <div class="card-row">

            <div class="design-summary-box">
              <details>
                <summary title="Click for details">
                  Sampling events
                </summary>

                <div class="table-container">
                  {% if samplingevent is defined and samplingevent %}
                    <table>
                      <thead>
                        <tr>
                          <th>Samplingevent</th>
                          <th>Description</th>
                          <th>Sampling type</th>
                          <th>Comment</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in samplingevent %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                            <td>{{ row.samplingtype }}</td>
                            <td>{{ row.row_comment }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>
              </details>
            </div>

          </div>
          <div class="card-row">

            <div class="design-summary-box">
              <details>
                <summary title="Click for details">
                  Sampling time

                </summary>
                <div class="table-container">
                  {% if samplingtime is defined and samplingtime %}
                    <table>
                      <thead>
                        <tr>
                          <th>Sampling</th>
                          <th>Sampling time</th>
                          <th>Sampling type</th>
                          <th>Comment</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in samplingtime %}
                          <tr>
                            <td>{{ row.samplingevent }}</td>
                            <td>{{ row.samplingtime }}</td>
                            <td>{{ row.samplingtype }}</td>
                            <td>{{ row.row_comment }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>
              </details>

            </div>

          </div>

          {# ================================================================= #}
          {# Startgroup - subject #}
          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">

              <details>
                <summary title="Click for details">
                  Startgroup

                </summary>

                <div class="table-container">
                  {% if startgroup is defined and startgroup %}
                    <table>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in startgroup %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>
              </details>

            </div>

          </div>

          {# ================================================================= #}
          {# Center #}
          {# ================================================================= #}
          <div class="card-row">
            <div class="design-summary-box">
              <details>

                <summary title="Click for details">
                  Centers
                </summary>

                <div class="table-container">
                  {% if center is defined and center %}
                    <table>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in center %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}
                </div>

              </details>
            </div>
          </div>

          {# ================================================================= #}
          {# Subject #}
          {# ================================================================= #}
          <div class="card-row">

            <div class="design-summary-box">
              <details>
                <summary title="Click for details">
                  Subjects
                </summary>
                <div class="table-container">

                  {% if subject is defined and subject %}
                    <table>
                      <thead>
                        <tr>
                          <th>Subject ID</th>
                          <th>Startgroup</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in subject %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.startgroup }}</td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  {% endif %}

                </div>
              </details>

            </div>
          </div>
        </div>

      </div>
    {% endif %}
  {% endblock %}
