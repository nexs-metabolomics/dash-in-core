{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Add startgroups to subevent") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"addgrouptosubevent"]) }}
   {% endblock %}

  {% block subheader_action %}

    <form id="form1" action="/dashin/owner/studydesign/addgrouptosubevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another event","selectevent","form1",event_info|default(null)) }}

  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", study_info|default(false),"name") }}
    {{ m_info_element("Event", event_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif event_info is not defined %}

      {{ m_emptypage_filler("Please select an event", "Select event", "selectevent", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">

          <div class="card-fieldset-column">
            <header>Event</header>
            {% if  event_info is defined and event_info %}
              {# <div class="card-column"> #}
              <div class="card-column">
                <div class="card-fieldset-label">
                  Name
                </div>
                <div class="card-field-content">
                  {{ event_info.name }}
                </div>
              </div>

              <div class="card-column">
                <div class="card-fieldset-label">
                  Description
                </div>
                <div class="card-field-content">
                  {{ row.description }}
                </div>
              </div>
              {# </div> #}

            {% endif %}
          </div>

          <div class="card-submenu-container">
            {% if subevents is defined %}
              {# ============================================================================= #}
              {# subevent menu items #}
              {# ============================================================================= #}
              <div class="card-submenu-menu">
                <header>
                  Subevents
                </header>
                {% for row in subevents %}
                  <button form="form1" name="btn[editgroups]" value="{{ row.subevent_id }}" class="button{{ row.current }}">
                    {{ row.name }}
                  </button>
                {% endfor %}
              </div>
            {% endif %}
            {% if subevent_info is defined %}
              {# ============================================================================= #}
              {# single subevent #}
              {# ============================================================================= #}
              <div class="card-submenu-content">
                <headeer>
                  {{ subevent_info.name }}
                </headeer>
                {% if startgroups_selected is defined and startgroups_selected %}
                  <div class="header">
                    Selected startgroups
                  </div>
                  <div class="card-table-container fixed-header">
                    <table>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in startgroups_selected %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                            <td>
                              <button form="form1" name="btn[remove]" value="{{ row.startgroup_id }}" class="">
                                remove
                              </button>
                            </td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
                {% if startgroups_unselected is defined and startgroups_unselected %}
                  <div class="header">
                    Unselected startgroups
                  </div>
                  <div class="card-table-container fixed-header">
                    <table>
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        {% for row in startgroups_unselected %}
                          <tr>
                            <td>{{ row.name }}</td>
                            <td>{{ row.description }}</td>
                            <td>
                              <button form="form1" name="btn[add]" value="{{ row.startgroup_id }}" class="">
                                Add
                              </button>
                            </td>
                          </tr>
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                {% endif %}
              </div>
            {% endif %}
          </div>
        </div>
      </div>
    {% endif %}

    {#    #}{# <div class="card-submenu-menu" style="border: #6a59d4 solid 3px"> #}
    {#    {% for row in subevents %} #}
    {#      <div class="nds2-tbody"> #}
    {#        {% set loop_index = loop.index %} #}

    {#        <div class="tbl-row nds2-tr"> #}

    {#          <form id="formx{{ loop_index }}" action="/dashin/owner/studydesign/addgrouptosubevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" #}
    {#                onchange="formSetChanged()"></form> #}
    {#          <div class="nds-view-column"> #}
    {#            <div class="nds-view-row"> #}
    {#              <div class="nds-view-header"> #}
    {#                {{ row.name }} #}
    {#              </div> #}
    {#            </div> #}
    {#            <div class="nds-view-row"> #}
    {#              <div class="nds-view-content"> #}
    {#                Description #}
    {#              </div> #}
    {#              <div class="nds-view-column"> #}
    {#                {{ row.description }} #}
    {#              </div> #}
    {#            </div> #}
    {#          </div> #}

    {#          <div class="nds2-td-group"> #}
    {#            <div class="nds2-td-group-content"> #}
    {#              <div class="nds-card-level-3"> #}

    {#                C: if not editing groups for this subevent, show the button and currently attached groups #}
    {#                {% if row.subevent_id !== current_subevent_id %} #}

    {#                  <div class="nds-view-column"> #}
    {#                    <div class="nds-view-row"> #}
    {#                      <div class="nds-view-content"> #}
    {#                        Startgroups #}
    {#                      </div> #}
    {#                      <div class="nds-view-content"> #}
    {#                        <button form="formx{{ loop_index }}" name="btn[editgroups]" value="{{ row.subevent_id }}" class="nds-button nds-tiny"> #}
    {#                          Edit groups #}
    {#                        </button> #}
    {#                      </div> #}
    {#                    </div> #}
    {#                  </div> #}

    {#                  {% if row.startgroups is defined %} #}

    {#                    <div class="nds-row"> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        Name #}
    {#                      </div> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        Description #}
    {#                      </div> #}
    {#                    </div> #}

    {#                    {% for grp in row.startgroups %} #}

    {#                      <div class="nds-row"> #}
    {#                        <div class="nds-column" style="flex: 1 0"> #}
    {#                          {{ grp.name }} #}
    {#                        </div> #}
    {#                        <div class="nds-column" style="flex: 1 0"> #}
    {#                          {{ grp.description }} #}
    {#                        </div> #}
    {#                      </div> #}

    {#                    {% endfor %} #}

    {#                  {% endif %} #}

    {#                  C: Else show the list of select groups and the list of unselected groups #}
    {#                {% else %} #}

    {#                  <div class="nds-row" style="width: 100%;justify-content: space-between;font-weight: bold"> #}
    {#                    <div style="font-weight: bold;display: flex;flex: 1 0"> #}
    {#                      Startgroups #}
    {#                    </div> #}
    {#                    <div style="font-weight: bold;display: flex;flex: 0 0"> #}
    {#                      <button form="formx{{ loop_index }}" name="btn[ok]" value="{{ row.subevent_id }}" class="nds-button nds-tiny"> #}
    {#                        Ok #}
    {#                      </button> #}
    {#                    </div> #}
    {#                  </div> #}

    {#                  <div class="nds-row" style="width: 100%;justify-content: space-between;font-weight: bold;color: #a0a0a0;border-bottom: #f2f8ff solid 0.2rem;padding-top: 0.5rem"> #}
    {#                    <div style="font-weight: bold;display: flex;flex: 1 0"> #}
    {#                      Selected #}
    {#                    </div> #}
    {#                  </div> #}

    {#                  {% for grp in startgroups_selected %} #}

    {#                    <div class="nds-row" style="width: 100%;justify-content: space-between;border-bottom: #f2f8ff solid 0.2rem"> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        {{ grp.name }} #}
    {#                      </div> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        {{ grp.description }} #}
    {#                      </div> #}
    {#                      <div class="nds-column" style="flex: 0 0"> #}
    {#                        <button form="formx{{ loop_index }}" name="btn[remove]" value="{{ grp.startgroup_id }}" class="nds-button nds-tiny nds-alert"> #}
    {#                          remove #}
    {#                        </button> #}
    {#                      </div> #}
    {#                    </div> #}

    {#                  {% endfor %} #}

    {#                  <div class="nds-row" style="width: 100%;justify-content: space-between;font-weight: bold;color: #a0a0a0;border-bottom: #f2f8ff solid 0.2rem;padding-top: 0.5rem"> #}
    {#                    <div style="font-weight: bold;display: flex;flex: 1 0"> #}
    {#                      Unselected #}
    {#                    </div> #}
    {#                  </div> #}

    {#                  {% for grp in startgroups_unselected %} #}

    {#                    <div class="nds-row" style="width: 100%;justify-content: space-between;border-bottom: #f2f8ff solid 0.2rem"> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        {{ grp.name }} #}
    {#                      </div> #}
    {#                      <div class="nds-column" style="flex: 1 0"> #}
    {#                        {{ grp.description }} #}
    {#                      </div> #}
    {#                      <div class="nds-column" style="flex: 0 0"> #}
    {#                        <button form="formx{{ loop_index }}" name="btn[add]" value="{{ grp.startgroup_id }}" class="nds-button nds-tiny"> #}
    {#                          Add #}
    {#                        </button> #}
    {#                      </div> #}
    {#                    </div> #}

    {#                  {% endfor %} #}

    {#                {% endif %} #}
    {#              </div> #}
    {#            </div> #}
    {#          </div> #}

    {#        </div> #}

    {#      </div> #}
    {#    {% endfor %} #}
    {# </div> #}
    {# {% endif %} #}

    {# </div> #}
    {# </div> #}
    {# </div> #}
  {% endblock %}
