{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit event") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"editevent"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/editevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another event","selectevent","form1",event_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Event", event_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      {% if form is not defined %}

        {{ m_emptypage_filler("Please select an event", "Select event", "selectevent", "form1") }}

      {% else %}

        <div class="card-container">
          <div class="card">
            
            <div class="card-title">
              {{ get_title(false) }}
            </div>
            
            <form id="form2" action="/dashin/owner/studydesign/editevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","apply":"Apply","cancel":"Cancel"]]) }}

            </form>

            <div class="table-container fixed-header">

              <header>Subevents in event</header>
              <table>
                <thead>
                  <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Type</th>
                    <th>Subtype</th>
                  </tr>
                </thead>
                <tbody>
                  {% if subevents_selected is defined %}
                    {% for sub in subevents_selected %}
                      <tr>
                        <td>
                          {% if sub.is_locked === 1 %}
                            <button class="nds-button nds-tiny secondary">Delete</button>
                          {% else %}
                            <button form="form2" name="btn[remove]" value="{{ sub.subevent_id }}" class="nds-button nds-tiny nds-alert">Remove</button>
                          {% endif %}
                        </td>
                        <td>
                          {{ sub.name }}
                        </td>
                        <td>
                          {{ sub.label }}
                        </td>
                        <td>
                          {{ sub.subevent_type }}
                        </td>
                        <td>
                          {{ sub.action_type }}
                        </td>
                      </tr>
                    {% endfor %}
                  {% endif %}
                </tbody>
              </table>
            </div>
            <div class="table-container fixed-header">
              <header>Subevents not in event</header>
              <table>
                <thead>
                  <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Type</th>
                    <th>Subtype</th>
                  </tr>
                </thead>
                <tbody>
                  {% if subevents_unselected is defined %}
                    {% for sub in subevents_unselected %}
                      <tr>
                        <td>
                          <button form="form2" name="btn[add]" value="{{ sub.subevent_id }}" class="nds-button nds-tiny">Add</button>
                        </td>
                        <td>
                          {{ sub.name }}
                        </td>
                        <td>
                          {{ sub.label }}
                        </td>
                        <td>
                          {{ sub.subevent_type }}
                        </td>
                        <td>
                          {{ sub.action_type }}
                        </td>
                      </tr>
                    {% endfor %}
                  {% endif %}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      {% endif %}
    {% endif %}
  {% endblock %}
