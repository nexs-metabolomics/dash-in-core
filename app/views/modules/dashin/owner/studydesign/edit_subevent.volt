{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit subevent") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"editsubevent"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/studydesign/editsubevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another event","selectevent","form1",event_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another subevent","selectsubevent","form1",subevent_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Event", event_info|default(false),"name") }}
    {{ m_info_element("Subevent", subevent_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}
    
    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif event_info is not defined %}

      {{ m_emptypage_filler("Please select an event", "Select event", "selectevent", "form1") }}

    {% elseif subevent_info is not defined %}

      {{ m_emptypage_filler("Please select a subevent", "Select subevent", "selectsubevent", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
        <div class="card-title">
          {{ get_title() }}
        </div>
          {% if form is defined %}
            <form action="/dashin/owner/studydesign/editsubevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","apply":"Apply","cancel":"Cancel"]]) }}

            </form>

            {# <form id="form2" action="/dashin/owner/studydesign/editsubevent#tab1" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form> #}
            <form id="form2" action="/dashin/owner/studydesign/editsubevent/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
            <div id="tab1">
              <header>Sampling events in subevent</header>
              <div class="table-container fixed-header">
                <table>
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Subtype</th>
                      <th>Comment</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% if sampling_events_selected is defined %}
                      {% for smp in sampling_events_selected %}
                        <tr>
                          <td>
                            <button form="form2" name="btn[remove]" value="{{ smp.samplingevent_id }}" class="nds-button nds-small nds-alert">Remove</button>
                          </td>
                          <td>
                            {{ smp.name }}
                          </td>
                          <td>
                            {{ smp.subevent_type }}
                          </td>
                          <td>
                            {{ smp.intervention_type }}
                          </td>
                          <td>
                            {{ smp.row_comment }}
                          </td>
                        </tr>
                      {% endfor %}
                    {% endif %}
                  </tbody>
                </table>
              </div>
            </div>

            <div>
              <header>Sampling events not in subevent</header>
              <div class="table-container fixed-header">
                <table>
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Description</th>
                      <th>No of times</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% if sampling_events_unselected is defined %}
                      {% for smp in sampling_events_unselected %}
                        <tr>
                          <td>
                            <button form="form2" name="btn[add]" value="{{ smp.samplingevent_id }}" class="nds-button nds-small">Add</button>
                          </td>
                          <td>
                            {{ smp.name }}
                          </td>
                          <td>
                            {{ smp.sampling_type }}
                          </td>
                          <td>
                            {{ smp.sampling_type_description }}
                          </td>
                          <td>
                            {{ smp.n_times }}
                          </td>
                        </tr>
                      {% endfor %}
                    {% endif %}
                  </tbody>
                </table>
              </div>
            </div>

          {% endif %}
        </div>
      </div>
    {% endif %}

  {% endblock %}
