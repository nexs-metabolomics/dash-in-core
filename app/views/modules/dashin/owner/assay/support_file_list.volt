{% extends "modules/dashin/owner/assay/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("List supplementary files") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/assay/00_tabs_partial",["current_tab":"files"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/assay/00-files-subnav",["current_tab":"filelist"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/assay/filelist/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another assay","select_assay","form1",assay_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Assay", assay_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if assay_info is not defined %}

      {{ m_emptypage_filler("Please select a assay", "Select assay", "select_assay", "form1") }}

    {% else %}
      <div class="card-container table-container">

        {% if assayfile_list is not defined %}
          <div class="card">
            No files
          </div>
        {% else %}
          <div class="card">
            <table class="striped framed">
              <thead>
                <tr>
                  <th>Action</th>
                  <th style="border-left: #eceff1 solid 1px">File</th>
                </tr>
              </thead>
              <tbody>
                {% for row in assayfile_list %}
                  <tr class="no-hover">

                    <td>
                      <div style="display: flex;flex-flow: row nowrap;justify-content: flex-start">
                        {% if ROLE_ORG_ALLOW_WRITE %}
                          {% if row.confirm is defined and row.confirm === true %}
                            <a href="/dashin/owner/assay/filelist/{{ _ls_querystring_ }}#rowno{{ loop_idx }}" class="button alert cancel">Cancel</a>
                            <button form="form1"
                                    name="btn[confirm]"
                                    value="{{ row.supportfile_id }}"
                                    class="button alert" title="Confirm delete">
                              Confirm
                            </button>
                          {% else %}
                            <button form="form1"
                                    name="btn[delete]"
                                    value="{{ row.supportfile_id }}"
                                    title="Delete"
                                    class="button delete icon24 table-icon"
                            >
                              {{ partial("00img_partials/v1/icon48/delete-icon48-outline.svg") }}
                            </button>
                            <button form="form1"
                                    name="btn[download]"
                                    value="{{ row.supportfile_id }}"
                                    class="button icon24 table-icon"
                                    title="Download file"
                            >
                              {{ partial("00img_partials/v1/icon48/download-icon48-filled.svg") }}
                            </button>
                            <button form="form1"
                                    name="btn[edit]"
                                    value="{{ row.supportfile_id }}"
                                    class="button icon24 table-icon"
                                    title="Edit file"
                            >
                              {{ partial("00img_partials/v1/icon48/edit-icon48-filled.svg") }}
                            </button>
                          {% endif %}
                        {% endif %}
                      </div>
                    </td>

                    <td style="border-left: #fafdff solid 1px">
                      <div class="flex-column fullwidth">
                        <div class="flex-row fullwidth" style="border-bottom: #b9b9b9 solid 1px;margin-top: 4px;font-weight: bold">
                          {{ row.name }}
                        </div>
                        <div class="flex-row fullwidth">
                          {{ row.description }}
                        </div>
                      </div>
                    </td>

                  </tr>
                {% endfor %}
              </tbody>
            </table>
          </div>


        {% endif %}

      </div>

    {% endif %}
  {% endblock %}
