<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/dashin/owner/assay/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create new", "/dashin/owner/assay/create", "create", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/assay/edit", "edit", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Files", "/dashin/owner/assay/filelist", "files", current_tab, _ls_querystring_) }}
  
</div>
