<div class="tabs-row">

  {{ m_menu_tabentry("List files", "/dashin/owner/assay/filelist", "filelist", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Upload file", "/dashin/owner/assay/fileupload", "fileupload", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit description", "/dashin/owner/assay/fileedit", "fileedit", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  
</div>
