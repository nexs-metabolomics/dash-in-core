{% extends "modules/dashin/owner/assay/00-subroot-standardpage.volt" %}
   {% block set_params %}
    {{ set_title("Create Assay") }}
  {% endblock %}
 
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/assay/00_tabs_partial",["current_tab":"create"]) }}
  {% endblock %}
 
  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/assay/create/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

  {% endblock %}
