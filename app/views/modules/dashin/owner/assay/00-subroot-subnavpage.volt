{% extends "layouts/root-subnavpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'assay',"show_submenu":1,"current":"assay_index"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'assay',"show_submenu":1,"current":"assay_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-admin">
        {{ partial("00img_partials/v1/icons/admin-icon.svg") }}
      </div>
      <a href="/dashin/owner/assay/index/{{ _ls_querystring_ }}">Assays</a>
    </div>
  {% endblock %}
