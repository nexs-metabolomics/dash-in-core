{% extends "modules/dashin/owner/assay/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit Assay") }}
  {% endblock %}
 
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/assay/00_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/assay/edit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another assay","select_assay","form1",assay_info|default(null)) }}
  {% endblock %}

  {% block page_content %}

    {% if assay_info is not defined %}

      {{ m_emptypage_filler("Please select an assay", "Select assay", "select_assay", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

    {% endif %}

  {% endblock %}
