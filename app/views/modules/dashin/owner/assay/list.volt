{% extends "modules/dashin/owner/assay/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List assays") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/assay/00_tabs_partial",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/assay/list/{{ current_page }}/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      {% if pagination is not defined %}

      <div class="card">
        There are no assays
      </div>

      {% else %}
        {% for row in pagination.items %}

          {% if row.confirm is defined and row.confirm === true %}
            {% set hover_action = " warning" %}
          {% else %}
            {% set hover_action = " hover-hide-action" %}
          {% endif %}

          <div class="card{{ hover_action }}" id="rowno{{ loop.index }}">
            <div class="card-column">
              <div class="card-fieldset-header">
                {{ row.name }}
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.researchfield_name }}
              </div>
            </div>

            <form action="/dashin/owner/assay/list/{{ current_page }}/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              <div class="card-column" style="height: 100%">
                {{ m_card_button_box_confirm(
                  row.assay_id,
                  row.confirm|default(null),
                  [
                    ["edit","edit-icon48-filled","button edit icon24","Edit assay",ROLE_ORG_ALLOW_WRITE],
                    ["delete","delete-icon48-outline","button delete icon24","Delete assay",ROLE_ORG_ALLOW_WRITE]
                  ],
                  [
                    ["confirm","Confirm","button confirm","Confirm delete assay",ROLE_ORG_ALLOW_WRITE],
                    ["cancel", "Cancel", "button cancel","Cancel delete assay"]
                  ],
                  "Do you really want to delete this assay?"
                ) }}

              </div>

            </form>
          </div>

        {% endfor %}
      {% endif %}
    </div>

  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/assay/list",
        "reset_link":"dashin/owner/assay/list/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
