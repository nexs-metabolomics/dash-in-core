{% extends "modules/dashin/owner/assay/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Upload supplementary file") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/assay/00_tabs_partial",["current_tab":"files"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/assay/00-files-subnav",["current_tab":"fileupload"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/assay/fileupload/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another assay","select_assay","form1",assay_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Assay", assay_info|default(false),"name") }}
{#    {{ partial("modules/dashin/owner/assay/assay_info_partial") }}#}
  {% endblock %}

  {% block page_content %}


    {% if assay_info is not defined %}

      {{ m_emptypage_filler("Please select a assay", "Select assay", "select_assay", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

      {#      <input form="form1" name="fileup" type="file">#}
{#      <textarea form="form1" name="description"></textarea>#}
{#      <button form="form1" name="btn[submit]">Ok</button>#}

    {% endif %}
  {% endblock %}
