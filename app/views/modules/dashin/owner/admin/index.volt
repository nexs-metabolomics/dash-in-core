{% extends "modules/dashin/owner/admin/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Admin menu") }}
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-small">

      <ul>

        {{ m_indexpage_menu_entry("Consortium", "section-admin","/dashin/owner/consort/list","00img_partials/v1/icons/admin-consortium.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Assay", "section-admin","/dashin/owner/assay/list","00img_partials/v1/icons/admin-assay.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Datacolumn", "section-admin","/dashin/owner/datacolumntype/list","00img_partials/v1/icons/admin-datacolumn.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Person", "section-admin","/dashin/owner/person/list","00img_partials/v1/icons/admin-person.svg", _ls_querystring_) }}

      </ul>
    </nav>
  {% endblock %}
