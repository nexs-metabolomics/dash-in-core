{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit study permissions") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"editpermissions"]) }}
  {% endblock %}

{#  {% block subheader_subnav %} #}
{#    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"editpermissions"]) }} #}
{#  {% endblock %} #}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/editpermissions/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_genericbutton("Export study as sqdr-file","export_study","form1", "button download",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("ID", study_info|default(false),"study_id") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container">

        <div class="card">

          <form action="/dashin/owner/study/editpermissions/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
          <div class="table-container form-container">

            {# === hidden default button ================================================= #}
            <div style="position: absolute;left: -1000px">
              <button form="form1" name="btn[submit]"></button>
            </div>
            {# =========================================================================== #}

            <div class="css-table">

              <div class="css-thead">
                <div class="css-trh">
                  <div class="css-th">
                  </div>
                  <div class="css-th">
                  </div>
                  <div class="css-th">
                  </div>
                </div>
              </div>
              <div class="css-tbody">

                {# =========================================================================== #}
                {# Study #}
                {# =========================================================================== #}
                <div class="css-tr">
                  <div class="css-td">
                    Study is {% if study_status.study_is_public === 1 %}public{% else %}private{% endif %}
                  </div>

                  {# =========================================================================== #}
                  {# Set all elements to public #}
                  {# =========================================================================== #}
                  <div class="css-td">
                    {% if study_status.study_all_public_confirm is defined and study_status.study_all_public_confirm === 1 %}
                      <button form="form1" name="btn[study_all_public_confirm]" class="button toggle">
                        Set all to public
                      </button>
                      <button form="form1" name="btn[study_all_public_cancel]" class="button toggle">
                        Cancel
                      </button>
                    {% else %}
                      <button form="form1" name="btn[study_all_public]" class="button toggle">
                        Set all to public
                      </button>
                    {% endif %}
                  </div>
                  
                  {# =========================================================================== #}
                  {# Set study to public, leave elements private #}
                  {# =========================================================================== #}
                  <div class="css-td" style="max-width: 120px;width: 120px">
                    {% if study_status.private_confirm is defined and study_status.private_confirm === 1 %}
                      <button form="form1" name="btn[study_private_confirm]" class="button toggle confirm">
                        Set to private
                      </button>
                      <button form="form1" name="btn[study_private_cancel]" class="button toggle confirm">
                        Cancel
                      </button>
                    {% elseif study_status.public_confirm is defined and study_status.public_confirm === 1 %}
                      <button form="form1" name="btn[study_public_confirm]" class="button toggle confirm">
                        Set to public
                      </button>
                      <button form="form1" name="btn[study_public_cancel]" class="button toggle confirm">
                        Cancel
                      </button>
                    {% else %}
                      <button form="form1" name="btn[study_{{ study_status.study_toggle_text }}]" class="button toggle{% if study_status.study_is_public === 1 %} on{% else %} off{% endif %}">
                        {% if study_status.study_is_public === 1 %}Public{% else %}Private{% endif %}
                      </button>
                    {% endif %}
                  </div>
                </div>
                
                {# =========================================================================== #}
                {# datasets #}
                {# =========================================================================== #}
                <div class="css-tr" style="background-color: #d4d4d4">
                  <div class="css-td">
                    Individual datasets
                  </div>
                  <div class="css-td">
                  </div>
                  <div class="css-td">
                  </div>
                </div>

                {% for row in study_status.datasets %}

                  <div class="css-tr">
                    <div class="css-td">
                      {{ row.name }}
                    </div>
                    <div class="css-td">
                    </div>
                    <div class="css-td">

                      {% if row.private_confirm is defined and row.private_confirm == 1 %}

                        <button form="form1" name="btn[dataset_private_confirm]" value="{{ row.dataset_id }}" class="button toggle confirm">
                          Set to private
                        </button>
                        <button form="form1" name="btn[dataset_private_cancel]" value="{{ row.dataset_id }}">
                          Cancel
                        </button>

                      {% elseif row.public_confirm is defined and row.public_confirm === 1 %}

                        <button form="form1" name="btn[dataset_public_confirm]" value="{{ row.dataset_id }}" class="button toggle confirm">
                          Set to public
                        </button>
                        <button form="form1" name="btn[dataset_public_cancel]" value="{{ row.dataset_id }}" class="button toggle confirm">
                          Cancel
                        </button>

                      {% else %}

                        {% if study_status.study_is_public !== 1 %}

                          <div class="button toggle off-disabled" ½>
                            Private
                          </div>

                        {% else %}

                          <button form="form1" name="btn[dataset_{{ row.toggle_text }}]" value="{{ row.dataset_id }}" class="button toggle{% if row.is_public === 1 %} on{% else %} off{% endif %}">
                            {% if row.is_public === 1 %}Public{% else %}Private{% endif %}
                          </button>
                        {% endif %}

                      {% endif %}

                    </div>
                  </div>

                {% endfor %}

                {# =========================================================================== #}
                {# supportfiles #}
                {# =========================================================================== #}
                <div class="css-tr" style="background-color: #d4d4d4">
                  <div class="css-td">
                    Individual supportfiles
                  </div>
                  <div class="css-td">
                  </div>
                  <div class="css-td">
                  </div>
                </div>

                {% for row in study_status.supportfiles %}
                  <div class="css-tr">
                    <div class="css-td">
                      {{ row.name }}
                    </div>
                    <div class="css-td">
                    </div>
                    <div class="css-td">

                      {# =========================================================================== #}
                      {# set supportfiles private #}
                      {# =========================================================================== #}
                      {% if row.private_confirm is defined and row.private_confirm == 1 %}

                        <button form="form1" name="btn[supportfile_private_confirm]" value="{{ row.supportfile_id }}" class="button toggle confirm">
                          Set to private
                        </button>

                        <button form="form1" name="btn[supportfile_private_cancel]" value="{{ row.supportfile_id }}" class="button toggle confirm">
                          Cancel
                        </button>

                        {# =========================================================================== #}
                        {# set supportfiles public #}
                        {# =========================================================================== #}
                      {% elseif row.public_confirm is defined and row.public_confirm === 1 %}

                        <button form="form1" name="btn[supportfile_public_confirm]" value="{{ row.supportfile_id }}" class="button toggle confirm">
                          Set to public
                        </button>

                        <button form="form1" name="btn[supportfile_public_cancel]" value="{{ row.supportfile_id }}" class="button toggle confirm">
                          Cancel
                        </button>

                      {% else %}

                        {% if study_status.study_is_public !== 1 %}

                          <div class="button toggle off-disabled" ½>
                            Private
                          </div>

                        {% else %}

                          <button form="form1" name="btn[supportfile_{{ row.toggle_text }}]" value="{{ row.supportfile_id }}" class="button toggle{% if row.is_public === 1 %} on{% else %} off{% endif %}">
                            {% if row.is_public === 1 %}Public{% else %}Private{% endif %}
                          </button>

                        {% endif %}

                      {% endif %}

                    </div>
                  </div>

                  {# =========================================================================== #}

                {% endfor %}

              </div>

            </div>

          </div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
  {% block subfooter_content %}
  {% endblock %}
