{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit file description") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"files"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-files-subnav",["current_tab":"fileedit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/fileedit/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another file","select_file","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Study", study_info|default(false),"name") }}
    {{ m_info_element("File", supportfile_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif supportfile_info is not defined %}

      {{ m_emptypage_filler("Please select a file", "Select file", "select_file", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Save and continue","button submit"],["apply","Apply","button apply"],["cancel","Cancel","button cancel"]],"form1") }}


    {% endif %}
  {% endblock %}
