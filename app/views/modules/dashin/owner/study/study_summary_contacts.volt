{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Study summary - contacts") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"viewgeneral"]) }}
  {% endblock %}

{#  {% block subheader_subnav %}#}
{#    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"summary"]) }}#}
{#  {% endblock %}#}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/summarycontacts/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_study]">Select study</button>
    <button form="form1" class="button download" name="btn[downloadstructure]">Download structure</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      {{ m_study_description(study_info,300,"/dashin/owner/study/summarycontacts/" ~ _ls_querystring_) }}


      {{ partial("modules/dashin/owner/study/00_study_summary_tabs_partial",["summary_current_tab":"contacts"]) }}

      <div class="card-container">


        <div class="card">
          <div class="card-table-container study-summary">
            <div class="css-table">

              <div class="css-thead">
                <div class="css-trh">
                  <div class="css-th">Name</div>
                  <div class="css-th">Email</div>
                  <div class="css-th">Role</div>
                </div>
              </div>

              <div class="css-tbody">
                {% if studycontacts is defined %}
                  {% for row in studycontacts %}
                    <div class="css-tr">
                      <div class="css-td">
                        {{ row.first_name }} {{ row.last_name }}
                      </div>
                      <div class="css-td">
                        {{ row.email }}
                      </div>
                      <div class="css-td">
                        {{ row.study_role }}
                      </div>
                    </div>
                  {% endfor %}
                {% endif %}
              </div>

            </div>
          </div>
        </div>
      </div>

    {% endif %}
  {% endblock %}
