{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Studies") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_study":1,"current":"study_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/dashin/owner/study/create/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Create  study
              </header>
              <p>Create a new study</p>
            </div>
          </a>
        </li>
        
        <li class="nds-card">
          <a href="/dashin/owner/study/editgeneral/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit study
              </header>
              <p>Edit properties and metadata of the study</p>
            </div>
          </a>
        </li>
        
        <li class="nds-card">
          <a href="/dashin/owner/study/list/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List studies
              </header>
              <p>List available stidues</p>
            </div>
          </a>
        </li>
        
      </ul>
    </nav>
  {% endblock %}
