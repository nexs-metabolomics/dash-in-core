{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("View study metadata") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_study":1,"current":"studyoverview_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>
    <form id="service-select" action="/dashin/owner/study/viewstudydesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

    <div class="nds-form-container">
      {{ partial("modules/dashin/owner/study/00_tabs_view_partial",["current_tab":"viewstudydesign"]) }}
    </div>

    <button form="service-select" class="button select" name="btn[select_study]">Select study</button>

    <div class="nds-view-container view-dataset">
      {% if researchdesign is defined %}
        {% for key,value in researchdesign %}
          <div class="nds-view-block">
            <div class="nds-view-header">
              {{ key }}
            </div>
            <div class="nds-view-value">
              {{ value }}
            </div>
          </div>
        {% endfor %}
      {% endif %}
    </div>
  {% endblock %}
