<div class="tabs-row">

  {{ m_menu_tabentry("List contacts", "/dashin/owner/study/liststudycontacts", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Create", "/dashin/owner/study/createstudycontact", "create", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/study/editstudycontact", "edit", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  
</div>
