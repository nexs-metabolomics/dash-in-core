{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit study metadata") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"create"]) }}
  {% endblock %}

  {% block subheader_subnav %} 
    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"editgeneral"]) }} 
  {% endblock %} 

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/editgeneral/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("ID", study_info|default(false),"study_id") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container">

        <div class="card-title">
          {{ get_title(false) }}
        </div>

        {% if form_namedesc is defined %}
          <form action="/dashin/owner/study/editgeneral/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
          <div class="form-container">

            <input form="form1" type="hidden" name="togglestate_general" value="{{ details_general }}">
            <input form="form1" type="hidden" name="togglestate_design" value="{{ details_design }}">

            {# Add a hidden first button to prevent the toggle button from being submitted when form is submitted by "enter" #}
            <div style="position: absolute;left: -1000px">
              <button form="form1" name="btn[submit]"></button>
            </div>

            {{ m_form_innerloop_card(form_namedesc,null,"form1") }}

            <details {{ details_general }}>
              <summary style="display: flex;flex-flow: row">

                {% if details_general === "closed" %}

                  {{ partial("00img_partials/triangle-right-svg") }}
                  <button form="form1" type="submit" name="btn[togglegeneral]" value="open" class="button details-toggle">General</button>

                {% else %}

                  {{ partial("00img_partials/triangle-down-svg") }}
                  <button form="form1" type="submit" name="btn[togglegeneral]" value="closed" class="button details-toggle">General</button>

                {% endif %}

              </summary>

              {{ m_form_innerloop_card(form_general,null,"form1") }}

            </details>

            <details {{ details_design }}>
              <summary style="display: flex;flex-flow: row">

                {% if details_design === "closed" %}

                  {{ partial("00img_partials/triangle-right-svg") }}
                  <button form="form1" type="submit" name="btn[toggledesign]" value="open" class="button details-toggle">Design</button>

                {% else %}

                  {{ partial("00img_partials/triangle-down-svg") }}
                  <button form="form1" type="submit" name="btn[toggledesign]" value="closed" class="button details-toggle">Design</button>

                {% endif %}

              </summary>
              {{ m_form_innerloop_card(form_studydesign,null,"form1") }}

            </details>

          </div>
        {% endif %}
      </div>
    {% endif %}
  {% endblock %}
  {% block subfooter_content %}
      <div class="form-container">
        <div class="form-buttonset" style="padding-right: 32px">
          <button form="form1" name="btn[submit]" class="button submit">
            Ok
          </button>
          <button form="form1" name="btn[apply]" class="button apply">
            Apply
          </button>
        </div>
      </div>
  {% endblock %}
