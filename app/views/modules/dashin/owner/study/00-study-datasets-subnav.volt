<div class="tabs-row">

  {{ m_menu_tabentry("List datasets", "/dashin/owner/study/viewdatasets", "viewdatasets", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Browse dataset", "/dashin/owner/studydata/browsedata", "browsedata", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Add dataset", "/dashin/owner/studydata/attachdataset", "attachdataset", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Remove datasets", "/dashin/owner/study/removedatasets", "removedatasets", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Import design", "/dashin/owner/studydesign/importdatadesign", "importdatadesign", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

</div>
