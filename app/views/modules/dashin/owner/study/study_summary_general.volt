{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Study summary - general") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"edit"]) }}
  {% endblock %}

{#  {% block subheader_subnav %}#}
{#    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"summary"]) }}#}
{#  {% endblock %}#}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/summarygeneral/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_study]">Select study</button>
    <button form="form1" class="button download" name="btn[downloadstructure]">Download structure</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      {{ m_study_description(study_info,300,"/dashin/owner/study/summarygeneral/" ~ _ls_querystring_) }}

      {{ partial("modules/dashin/owner/study/00_study_summary_tabs_partial",["summary_current_tab":"general"]) }}


      <div class="card-container">
        
        {% if study_general is defined %}

          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Start date
              </div>
              <div class="card-field-content">
                {{ study_general.start_date }}
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                End point
              </div>
              <div class="card-field-content">
                {{ study_general.endpoint|default("")|nl2br }}
{#                {{ null|default('')|nl2br }}#}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Objectives
              </div>
              <div class="card-field-content">
                {{ study_general.objectives|default("")|nl2br }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Conclusion
              </div>
              <div class="card-field-content">
                {{ study_general.conclusion|default("")|nl2br }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Exclusion criteria
              </div>
              <div class="card-field-content">
                {{ study_general.exclusion|default("")|nl2br }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Inclusion criteria
              </div>
              <div class="card-field-content">
                {{ study_general.inclusion|default("")|nl2br }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Institute
              </div>
              <div class="card-field-content">
                {{ study_general.institute }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                country
              </div>
              <div class="card-field-content">
                {{ study_general.country_name }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Cornsotium
              </div>
              <div class="card-field-content">
                {{ study_general.consortium_name }}
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Published (PubMedID)
              </div>
              <div class="card-field-content">
                {{ study_general.published }}
              </div>
            </div>
          </div>

        {% endif %}

      </div>
    {% endif %}

  {% endblock %}
