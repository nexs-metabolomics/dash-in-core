{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("View study contacts") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"studycontacts"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-contacts-subnav",["current_tab":"list"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/liststudycontacts/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    <input type="hidden" name="_prevent_accidental_delete_" value="{{ prevent_delete_token }}">
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
          
          <div class="card-title">
            {{ get_title(false) }}
          </div>
          
          <div class="card-table-container study-summary">
            <div class="css-table">

              <div class="css-thead">
                <div class="css-trh">
                  <div class="css-th">Actions</div>
                  <div class="css-th">Name</div>
                  <div class="css-th">Email</div>
                  <div class="css-th">Role</div>
                </div>
              </div>

              <div class="css-tbody">
                {% if studycontacts is defined %}
                  {% for row in studycontacts %}
                    <div class="css-tr">

                      <div class="css-td">
                        <div style="display: flex;flex-flow: row">
                          {% if ROLE_ORG_ALLOW_WRITE %}


                            {% if row.confirm is defined and row.confirm === true %}
                              <a href="/dashin/owner/study/liststudycontacts/{{ _ls_querystring_ }}" class="nds-button nds-small">Cancel</a>
                              <button form="form1" name="btn[confirm]" value="{{ row.studycontact_id }}" class="nds-button nds-small nds-alert" title="Confirm delete">Confirm</button>
                            {% else %}
                              <button form="form1" name="btn[delete]" value="{{ row.studycontact_id }}" class="button delete icon24 table-icon" title="Delete">
                                {{ partial("00img_partials/v1/icon48/delete-icon48-outline.svg") }}
                              </button>
                              <button form="form1" name="btn[edit]" value="{{ row.studycontact_id }}" class="button edit icon24 table-icon" title="Edit">
                                {{ partial("00img_partials/v1/icon48/edit-icon48-filled.svg") }}
                              </button>
                            {% endif %}
                          {% endif %}

                        </div>
                      </div>

                      <div class="css-td">
                        {{ row.first_name }} {{ row.last_name }}
                      </div>

                      <div class="css-td">
                        {{ row.email }}
                      </div>

                      <div class="css-td">
                        {{ row.study_role }}
                      </div>

                    </div>
                  {% endfor %}
                {% endif %}
              </div>

            </div>
          </div>

        </div>
      </div>
    {% endif %}
  {% endblock %}
