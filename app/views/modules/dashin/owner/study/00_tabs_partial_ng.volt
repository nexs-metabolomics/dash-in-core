<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/dashin/owner/study/list", "list", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Add/Edit", "/dashin/owner/study/create", "create", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Permissions/Export", "/dashin/owner/study/editpermissions", "editpermissions", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

  {{ m_menu_tabentry("Datasets", "/dashin/owner/study/viewdatasets", "datasets", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Contacts", "/dashin/owner/study/liststudycontacts", "studycontacts", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Files", "/dashin/owner/study/filelist", "files", current_tab, _ls_querystring_) }}

  
{#  {{ m_menu_tabentry("Edit", "/dashin/owner/study/editgeneral", "editgeneral", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}#}
{#  {{ m_menu_tabentry("View", "/dashin/owner/study/summary", "summary", current_tab, _ls_querystring_) }}#}
{#  {{ m_menu_tabentry("Import design", "/dashin/owner/studydesign/importdatadesign", "importdatadesign", current_tab, _ls_querystring_) }}#}
{#  {{ m_menu_tabentry("Browse dataset", "/dashin/owner/studydata/browsedata", "browsedata", current_tab, _ls_querystring_) }}#}
{#  {{ m_menu_tabentry("Add dataset", "/dashin/owner/studydata/attachdataset", "attachdataset", current_tab, _ls_querystring_) }}#}
</div>
