{% extends "modules/dashin/owner/study/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Studies") }}
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-small">

      <ul>

{#        {{ m_indexpage_menu_entry("Create study", "section-studies","/dashin/owner/studycreate/createfromdata","00img_partials/v1/icons/manage-studies-icon.svg", _ls_querystring_) }}#}

        {{ m_indexpage_menu_entry("Manage studies", "section-studies","/dashin/owner/study/list","00img_partials/v1/icons/manage-studies-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Study design", "section-studies","/dashin/owner/studydesign/viewstudydesign","00img_partials/v1/icons/study-design-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Data export", "section-studies","/dashin/owner/studyexport/listconditions","00img_partials/v1/icons/dataexport-icon.svg", _ls_querystring_) }}

      </ul>
    </nav>
  {% endblock %}
