{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit study contact") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"studycontacts"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-contacts-subnav",["current_tab":"edit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form1" name="btn[submit]" class="button hidden-default"></button>
    <form id="form1" action="/dashin/owner/study/editstudycontact/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another study contact","select_studycontact","form1",studycontact_info|default(null)) }}
{#    <button form="form1" class="button select" name="btn[select_studycontact]">Select study contact</button>#}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Contact", studycontact_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% elseif studycontact_info is not defined %}
      
      {{ m_emptypage_filler("Please select a study contact", "Select study contact", "select_studycontact", "form1") }}

    {% else %}
      
      {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}
      
    {% endif %}


  {% endblock %}
