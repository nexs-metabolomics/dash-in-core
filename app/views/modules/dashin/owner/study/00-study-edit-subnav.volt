<div class="tabs-row">

  {{ m_menu_tabentry("Create new", "/dashin/owner/study/create", "create", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/study/editgeneral", "editgeneral", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("View", "/dashin/owner/study/summary", "summary", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Import", "/dashin/owner/study/import", "import", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
{#  {{ m_menu_tabentry("Import design", "/dashin/owner/studydesign/importdatadesign", "importdatadesign", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}#}
{#  {{ m_menu_tabentry("Summary", "/dashin/owner/study/summary", "summary", current_tab, _ls_querystring_) }}#}

</div>
