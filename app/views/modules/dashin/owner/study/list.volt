{% extends "modules/dashin/owner/study/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List studies") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"list"]) }}
    <form id="form1" action="/dashin/owner/study/list/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <header>
        {{ get_title(false) }}
      </header>

      {% if pagination is defined %}
        {% for row in pagination.items %}

          {% if row.confirm is defined and row.confirm === true %}
            {% set hover_action = "warning" %}
          {% else %}
            {% set hover_action = "hover-hide-action" %}
          {% endif %}

          {# Make it visually explicit when a study is public #}
          {% if row.is_public %}
            {% set pubclass = " is-public" %}
            {% set title = "This study is public" %}
          {% else %}
            {% set pubclass = "" %}
            {% set title = "" %}
          {% endif %}

          <div class="card {{ hover_action }}{{ pubclass }}" title="{{ title }}">
            <div class="card-column">
              <div class="card-fieldset-header" id="rowno{{ loop.index }}">
                {{ row.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <div class="card-row-line-divider">
            </div>

            <div class="card-row">

              <div class="card-column" style="width: unset">

                <div class="card-field-row">
                  <div class="card-field-content" style="white-space: nowrap;margin-right: 16px">
                    Number of subjects
                  </div>
                  <div class="card-field-headert">
                    {{ row.n_subjects }}
                  </div>
                </div>

                <div class="card-field-row">
                  <div class="card-field-content" style="white-space: nowrap;margin-right: 16px">
                    Number of datasets
                  </div>
                  <div class="card-field-header">
                    {{ row.n_datasets }}
                  </div>
                </div>

                <div class="card-field-row">
                  <div class="card-field-content" style="white-space: nowrap;margin-right: 16px">
                    Number of files
                  </div>
                  <div class="card-field-header">
                    {{ row.n_files }}
                  </div>
                </div>
              </div>

              <div class="card-column">

                <form action="/dashin/owner/study/list/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

                  {{ m_card_button_box_confirm(
                    row.study_id,
                    row.confirm|default(null),
                    [
                      ["downloadstructure","download-icon48-filled","button download icon24","Download study structure"],
                      ["edit","edit-icon48-filled","button edit icon24","Edit study",ROLE_ORG_ALLOW_WRITE],
                      ["view","view-icon48-filled","button view icon24","View study"],
                      ["delete","delete-icon48-outline","button delete icon24","Delete study",ROLE_ORG_ALLOW_WRITE]
                    ],
                    [
                      ["confirm","Confirm","button confirm","Confirm delete study",ROLE_ORG_ALLOW_WRITE],
                      ["cancel", "Cancel", "button cancel","Cancel delete study"]
                    ],
                    "Do you really want to delete this study?"
                  ) }}

                </form>
              </div>
            </div>

          </div>
        {% endfor %}
      {% endif %}

    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/study/list",
        "reset_link":"dashin/owner/study/list/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
