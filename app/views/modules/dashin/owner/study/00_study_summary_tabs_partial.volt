<div class="tabs-row study-summary">


  {% if summary_current_tab === "general" %}
    <div class="tab current">
      <a href="/dashin/owner/study/summary/{{ _ls_querystring_ }}">
        General
      </a>
    </div>
  {% else %}
    <div class="tab">
      <a href="/dashin/owner/study/summarygeneral/{{ _ls_querystring_ }}">
        General
      </a>
    </div>
  {% endif %}

  {% if summary_current_tab === "design" %}
    <div class="tab current">
      <a href="/dashin/owner/study/summary/{{ _ls_querystring_ }}">
        Design
      </a>
    </div>
  {% else %}
    <div class="tab">
      <a href="/dashin/owner/study/summarydesign/{{ _ls_querystring_ }}">
        Design
      </a>
    </div>
  {% endif %}

  {% if summary_current_tab === "contacts" %}
    <div class="tab current">
      <a href="/dashin/owner/study/summary/{{ _ls_querystring_ }}">
        Contacts
      </a>
    </div>
  {% else %}
    <div class="tab">
      <a href="/dashin/owner/study/summarycontacts/{{ _ls_querystring_ }}">
        Contacts
      </a>
    </div>
  {% endif %}

  {% if summary_current_tab === "datasets" %}
    <div class="tab current">
      <a href="/dashin/owner/study/summary/{{ _ls_querystring_ }}">
        Datasets
      </a>
    </div>
  {% else %}
    <div class="tab">
      <a href="/dashin/owner/study/summarydatasets/{{ _ls_querystring_ }}">
        Datasets
      </a>
    </div>
  {% endif %}

</div>
