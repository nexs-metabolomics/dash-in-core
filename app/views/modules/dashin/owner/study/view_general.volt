{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("View study") }}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_study":1,"current":"study_index"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial",["current_tab":"viewgeneral"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="service-select" action="/dashin/owner/study/summary/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="service-select" class="button select" name="btn[select_study]">Select study</button>
    <button form="service-select" class="button download" name="btn[downloadstructure]">Download structure</button>
  {% endblock %}
  {% block content2 %}

    <div class="nds-card-container">
      <div class="nds-card">
        {% if study_general is defined %}

          <div class="nds-card-section">
            <div class="nds-view-container view-study">

              <div class="nds-view-block">
                <div class="nds-view-value">
                  Name
                </div>
                <div class="nds-view-value">
                  Study id
                </div>
              </div>

              <div class="nds-view-block">
                <div class="nds-view-value nds-study-name">
                  {{ study_general.name }}
                </div>
                <div class="nds-view-value nds-study-id">
                  {{ study_general.study_id }}
                </div>
              </div>

              <div class="nds-view-block">
                <div class="nds-view-value">
                  Title
                </div>
              </div>

              <div class="nds-view-block">
                <div class="nds-view-value nds-study-title">
                  {{ study_general.title }}
                </div>
              </div>

              <div class="nds-view-block">
                <div class="nds-view-value nds-study-description-header">
                  Description
                </div>
              </div>

              <div class="nds-view-block">
                <div class="nds-view-value nds-view-text">
                  {{ study_general.description }}
                </div>
              </div>

            </div>
          </div>
        {% endif %}
        <div class="nds-card-section">

          <details closed>
            <summary class="header">
              General
            </summary>
            <div class="nds-view-container view-dataset">
              {% if study_general is defined %}
                {% for key,value in study_general %}
                  <div class="nds-view-block">
                    <div class="nds-view-header">
                      {{ key }}
                    </div>
                    <div class="nds-view-value">
                      {{ value }}
                    </div>
                  </div>
                {% endfor %}
              {% endif %}
            </div>
          </details>

          <details>
            <summary class="header">
              Design
            </summary>
            <div class="nds-view-container view-dataset">
              {% if researchdesign is defined %}
                {% for key,value in researchdesign %}
                  <div class="nds-view-block">
                    <div class="nds-view-header">
                      {{ key }}
                    </div>
                    <div class="nds-view-value">
                      {{ value }}
                    </div>
                  </div>
                {% endfor %}
              {% endif %}
            </div>
          </details>

          <details>
            <summary class="header">
              Contacts
            </summary>
            <div class="nds-view-container view-dataset">
              {% if studycontacts is defined %}
                <div class="nds-table-container">
                  <table class="nds-data-table">

                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Role</th>
                    </tr>
                    </thead>

                    <tbody>
                    {% for row in studycontacts %}
                      <tr>
                        <td>
                          {{ row.first_name }} {{ row.last_name }}
                        </td>
                        <td>
                          {{ row.email }}
                        </td>
                        <td>
                          {{ row.study_role }}
                        </td>
                      </tr>
                    {% endfor %}
                    </tbody>

                  </table>
                </div>

              {% endif %}
            </div>
          </details>
          <details>
            <summary class="header">
              Datasets
            </summary>
            <div class="nds-view-container view-dataset">
              <div class="nds-table-container">
                <table class="nds-data-table">

                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Rows</th>
                    <th>Columns</th>
                  </tr>
                  </thead>

                  <tbody>
                  {% if study_datasets is defined %}
                    {% for row in study_datasets %}
                      <tr>
                        <td>
                          {{ row.name }}
                        </td>
                        <td>
                          {{ row.description }}
                        </td>
                        <td>
                          {{ row.nrows }}
                        </td>
                        <td>
                          {{ row.ncols }}
                        </td>
                      </tr>
                    {% endfor %}
                  {% endif %}
                  </tbody>

                </table>
              </div>

              <div style="margin: 20px;min-height: 120px"></div>
            </div>
          </details>
        </div>
      </div>
    </div>

  {% endblock %}
