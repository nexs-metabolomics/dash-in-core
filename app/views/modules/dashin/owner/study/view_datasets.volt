{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("View datasets") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"datasets"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/study/00-study-datasets-subnav",["current_tab":"viewdatasets"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/viewdatasets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card table-container study-summary">

          <div class="card-title">
            {{ get_title(false) }}
          </div>

          <div class="css-table">

            <div class="css-thead">
              <div class="css-trh">
                <div class="css-th">Actions</div>
                <div class="css-th">Name</div>
                <div class="css-th">Description</div>
              </div>
            </div>

            <div class="css-tbody">
              {% if pagination is defined %}
                {% for row in pagination.items %}
                  <div class="css-tr" id="{{ row.dataset_id }}">
                    <div class="css-td">
                      {#                      {% if ROLE_ORG_ALLOW_WRITE %} #}
                      {#                        {% if row.confirm is defined and row.confirm === true %} #}
                      {#                          <button form="form1" name="btn[confirm]" value="{{ row.dataset_id }}" #}
                      {#                                  class="nds-button nds-small nds-alert" title="Confirm remove"> #}
                      {#                            Really remove? #}
                      {#                          </button> #}
                      {#                          <button form="form1" name="btn[cancel]" value="{{ row.dataset_id }}" #}
                      {#                                  class="nds-button nds-small" title="Cancel delete"> #}
                      {#                            Cancel #}
                      {#                          </button> #}
                      {#                        {% else %} #}
                      {#                          #}{# <button form="form1" name="btn[edit]" value="{{ row.dataset_id }}" #}
                      {#                          #}{# class="nds-button nds-small" title="Edit dataset"> #}
                      {#                          #}{# Edit #}
                      {#                          #}{# </button> #}
                      {#                        {% endif %} #}
                      {#                      {% endif %} #}
                      <button name="btn[browse]"
                              form="form1"
                              value="{{ row.dataset_id }}"
                              title="Browse"
                              class="button view icon24 table-icon"
                      >
                        {{ partial("00img_partials/v1/icon48/view-icon48-filled.svg") }}
                      </button>
                    </div>
                    <div class="css-td">
                      {{ row.name }}
                    </div>
                    <div class="css-td">
                      {{ row.description }}
                    </div>
                  </div>
                {% endfor %}
              {% endif %}
            </div>

          </div>
        </div>
      </div>

    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {{ partial("00element_partials/table_nav_filter",[
      "nav_link":"dashin/owner/study/viewdatasets",
      "reset_link":"dashin/owner/study/viewdatasets/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
