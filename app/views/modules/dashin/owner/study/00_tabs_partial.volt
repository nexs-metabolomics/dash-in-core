<div class="nds-tabs-row" style="margin-bottom: 1rem">
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "list" %} current{% endif %}">
    <a href="/dashin/owner/study/list/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "create" %} current{% endif %}">
    <a href="/dashin/owner/study/create/{{ _ls_querystring_ }}">
      Create new
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "editgeneral" %} current{% endif %}">
    <a href="/dashin/owner/study/editgeneral/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>

{#  <div class="nds-tab{% if current_tab is defined and current_tab === "editcontact" %} current{% endif %}">#}
{#    <a href="/dashin/owner/study/editcontact/{{ _ls_querystring_ }}">#}
{#      Contacts#}
{#    </a>#}
{#  </div>#}

  <div class="nds-tab{% if current_tab is defined and current_tab === "viewgeneral" %} current{% endif %}">
    <a href="/dashin/owner/study/summary/{{ _ls_querystring_ }}">
      Summary
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "viewdatasets" %} current{% endif %}">
    <a href="/dashin/owner/study/viewdatasets/{{ _ls_querystring_ }}">
      Datasets
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "studycontacts" %} current{% endif %}">
    <a href="/dashin/owner/study/liststudycontacts/{{ _ls_querystring_ }}">
      Study contacts
    </a>
  </div>

</div>
