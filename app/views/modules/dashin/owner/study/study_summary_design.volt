{% extends "modules/dashin/owner/study/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Study summary - design") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/study/00_tabs_partial_ng",["current_tab":"viewgeneral"]) }}
  {% endblock %}

{#  {% block subheader_subnav %}#}
{#    {{ partial("modules/dashin/owner/study/00-study-edit-subnav",["current_tab":"summary"]) }}#}
{#  {% endblock %}#}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/study/summarydesign/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_study]">Select study</button>
    <button form="form1" class="button download" name="btn[downloadstructure]">Download structure</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      {{ m_study_description(study_info,300,"/dashin/owner/study/summarydesign/" ~ _ls_querystring_) }}

      {{ partial("modules/dashin/owner/study/00_study_summary_tabs_partial",["summary_current_tab":"design"]) }}

      <div class="card-container">

        {% if researchdesign is defined %}
          <div class="card">

            <div class="card-row">

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Study design
                </div>
                <div class="card-field-content">
                  {{ researchdesign.study_design }}
                </div>
              </div>

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Number treatments
                </div>
                <div class="card-field-content">
                  {{ researchdesign.numtreat }}
                </div>
              </div>

            </div>

            <div class="card-row">

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Number or arms
                </div>
                <div class="card-field-content">
                  {{ researchdesign.numarm }}
                </div>
              </div>

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Number of factors
                </div>
                <div class="card-field-content">
                  {{ researchdesign.numfactor }}
                </div>
              </div>

            </div>

            <div class="card-row">

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Number of volunteers
                </div>
                <div class="card-field-content">
                  {{ researchdesign.numvolun }}
                </div>
              </div>

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Number volunteers who terminated
                </div>
                <div class="card-field-content">
                  {{ researchdesign.numvolun_term }}
                </div>
              </div>

            </div>

            <div class="card-row">

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Recruitment start year
                </div>
                <div class="card-field-content">
                  {{ researchdesign.recruit_start_year }}
                </div>
              </div>

              <div class="card-fieldset-column">
                <div class="card-fieldset-header">
                  Recruitment end year
                </div>
                <div class="card-field-content">
                  {{ researchdesign.recruit_end_year }}
                </div>
              </div>

            </div>

          </div>

          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Research design
              </div>
              <div class="card-field-content">
                {{ researchdesign.researchdesign_text|default("")|nl2br }}
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-column">
              <div class="card-fieldset-header">
                Blinding (yes/no)
              </div>
              <div class="card-field-content">
                {{ researchdesign.blinding }}
              </div>
            </div>
            <div class="card-column">
              <div class="card-fieldset-header">
                Blinding method
              </div>
              <div class="card-field-content">
                {{ researchdesign.blinding_method }}
              </div>
            </div>
          </div>
        {% endif %}
      </div>
    {% endif %}

  {% endblock %}
