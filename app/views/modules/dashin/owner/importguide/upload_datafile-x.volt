{% extends "layouts/root-importguide-page.volt" %}
  {% block set_params %}
    {{ set_title("Upload data file") }}
  {% endblock %}

  {% block subheader_nav %}
    <div class="nav-item">
      1. Upload
    </div>
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/upload/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["upload","Ok","button submit has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

    
  {% endblock %}
