{% extends "modules/dashin/owner/importguide/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Choose dataset type") }}
    {% if header_pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"datasettype"]) }}
  {% endblock %}



  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/datasettype/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

  {% endblock %}

  {% block page_content %}

    {% if dataset_info is not defined %}

      {{ m_emptypage_filler("No selected dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
          <div class="card-title">
            {{ get_title(false) }}
          </div>

          <div class="card-row">
            <div class="card-column">

              <div class="card-field-row">
                <div class="card-field-header">
                  {{ dataset_info.name }}
                </div>
              </div>

              <div class="card-field-row">
                <div class="card-field-content">
                  {{ dataset_info.description }}
                </div>
              </div>

            </div>
          </div>

          <div class="card-row-line-divider">
          </div>
          <div class="card-row">

              <div class="card-field-row" style="justify-content: flex-start">
                <div class="card-field-header" style="margin-right: 16px">
                  Current dataset type
                </div>
                <div class="card-field-content">
                  {{ dataset_info.datasetype_name }}
                </div>
              </div>

          </div>

          <div class="form-container">
            <div class="card-row">

              <div class="card-column">
              </div>
              <div class="card-column">
              </div>

              <div class="card-column">
                <div class="form-buttonset">
                  <button form="form1" name="btn[primarydata]" class="button standard expand">
                    Primary dataset
                  </button>
                </div>
                <div class="form-buttonset">
                  <button form="form1" name="btn[qualitycontrol]" class="button standard expand">
                    Quality control dataset
                  </button>
                </div>

              </div>

            </div>
          </div>

        </div>
      </div>

    {% endif %}

  {% endblock %}

