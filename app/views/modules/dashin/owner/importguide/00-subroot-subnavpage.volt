{% extends "layouts/root-subnavpage.volt" %}
  
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'import',"show_submenu":1,"current":"importguide_index"]) }}
  {% endblock %}

  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'import',"show_submenu":1,"current":"importguide_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-dataset">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
      </div>
      <a href="/dashin/owner/importguide/index/{{ _ls_querystring_ }}">Dataset import</a>
    </div>
  {% endblock %}

