{% extends "modules/dashin/owner/importguide/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Import data") }}
    {% if header_pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"import"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/importguide/00_import_data_tabs_partial",["current_tab":"import"]) }}
  {% endblock %}


  {% block page_content %}

    {% if form is not defined %}

      {{ m_emptypage_filler("No newly imported dataset", "Go to 'Upload'", "gotoupload", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Import","button submit has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

    {% endif %}

  {% endblock %}


  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/import/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

    {% if header_pagination is defined %}
      <table>
        <thead>
          <tr>
            <th>Variable name</th>
          </tr>
        </thead>
        <tbody>
          {% for row in header_pagination.items %}
            <tr>
              <td>
                {{ row.name }}
              </td>
            </tr>
          {% endfor %}
        </tbody>
      </table>
      <div class="subfooter-inner">
        <div class="page-subfooter">
          <div class="table-nav-container narrow">
            {{ partial("00element_partials/table_nav_filter",[
              "nav_link":"dashin/owner/importguide/import",
              "reset_link":"dashin/owner/importguide/import/1/1",
              "has_results":has_results,
              "pagination":header_pagination
            ]) }}
          </div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
