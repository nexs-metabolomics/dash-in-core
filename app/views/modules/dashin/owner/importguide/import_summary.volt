{% extends "modules/dashin/owner/importguide/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Import summary") }}
    {% if header_pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"import"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/importguide/00_import_data_tabs_partial",["current_tab":"summary"]) }}
  {% endblock %}


  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/importsummary/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

  {% endblock %}

  {% block page_content %}

    {% if import_summary is not defined %}

      {{ m_emptypage_filler("No newly imported dataset", "Go to 'Upload'", "gotoupload", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
          <div class="card-title">
            {{ get_title(false) }}
          </div>

          <div class="form-container">

            <div class="form-fieldset" title="Required field">
              <label for="name">Name *</label>
              <div class="form-field">
                {{ form.render("name",["form":"form1"]) }}
              </div>
            </div>

            <div class="form-fieldset">
              <label for="name">Description</label>
              <div class="form-field">
                {{ form.render("description",["form":"form1"]) }}
              </div>
            </div>

            <div class="form-buttonset">
              <button form="form1" name="btn[update]" class="button standard">
                Update
              </button>
              <button form="form1" name="btn[continue]" class="button submit">
                Save and continue
              </button>
            </div>

          </div>

          <div class="card-row-line-divider" style="margin-top: 16px">
          </div>

          <div class="card-row">
            <div class="card-column">

              <div class="card-field-row">
                <div class="card-field-content">
                  Number of rows
                </div>
                <div class="card-field-header">
                  {{ import_summary.n_rows }}
                </div>
              </div>

              <div class="card-field-row">
                <div class="card-field-content">
                  Number of columns
                </div>
                <div class="card-field-header">
                  {{ import_summary.n_cols }}
                </div>
              </div>

            </div>

            <div class="card-column">
            </div>

          </div>

        </div>
      </div>
    {% endif %}

  {% endblock %}

