<div class="tabs-row">

  {{ m_menu_tabentry("1. Upload", "/dashin/owner/importguide/upload", "upload", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("2. Import", "/dashin/owner/importguide/import", "import", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("3. Design", "/dashin/owner/importguide/designedit", "datasetdesign", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("4. Complete", "/dashin/owner/importguide/complete", "complete", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

</div>
