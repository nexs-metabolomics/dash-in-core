{% extends "modules/dashin/owner/importguide/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("Define dataset design") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"datasetdesign"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/importguide/00_dataset_design_tabs_partial",["current_tab":"edit"]) }}
  {% endblock %}


  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/designedit/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    {% if dataset_info is not defined %}

      {{ m_emptypage_filler("No newly imported dataset", "Go to 'Upload'", "gotoupload", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">
          <div class="card-title">
            {{ get_title(false) }}
          </div>

          <div class="card-row">
            <div class="card-column">

              <div class="card-field-row">
                <div class="card-field-header">
                  {{ dataset_info.name }}
                </div>
              </div>

              <div class="card-field-row">
                <div class="card-field-content">
                  {{ dataset_info.description }}
                </div>
              </div>

            </div>
          </div>

          <div class="card-row-line-divider">
          </div>

          <div class="form-container">
            <div class="card-row">

              <div class="card-column">
                {% if design_has_duplicates is defined %}
                  <div class="card-field-row">
                    <div class="card-field-header" style="white-space: nowrap">
                      This design has duplicate rows
                      <button form="form1" name="btn[viewdesign]" class="button standard">
                        View
                      </button>
                    </div>
                  </div>


                {% endif %}

              </div>

              <div class="card-column">
              </div>

              <div class="card-column">
                <div class="form-buttonset">
                  <button form="form1" name="btn[update]" class="button standard">
                    Update
                  </button>
                  {% if design_has_duplicates %}
                  <button form="form1" name="btn[nosave]" class="button submit">
                    Continue without saving
                  </button>
{#                  <button class="button disabled" disabled>#}
{#                    Save and continue#}
{#                  </button>#}
                  {% else %}
                  <button form="form1" name="btn[continue]" class="button submit">
                    Save and continue
                  </button>
                  {% endif %}
                </div>

              </div>

            </div>
          </div>

        </div>
      </div>

      <div class="table-container">
        <div class="table-container fixed-header">
          {% if pagination is defined %}
            <table>
              <thead>
                <tr>
                  <th>
                    Variable name
                  </th>
                  <th>
                    Variable type
                  </th>
                </tr>
              </thead>
              <tbody>
                {% for row in pagination.items %}
                  <tr>
                    <td>
                      {{ row.name }}
                    </td>
                    <td>
                      {{ row.vartype }}
                    </td>
                  </tr>
                {% endfor %}
              </tbody>
            </table>
          {% endif %}
        </div>
      </div>



    {% endif %}

    <script>
        var formSubmitting = false;
        var setFormSubmitting = function () {
            formSubmitting = true;
        }

        var hasChanged = false;

        function vartypeChange(obj) {
            current_val = obj.value

            if (['2', '3', '4', '5', '6', '7', '8'].includes(current_val)) {
                s = document.getElementsByClassName('vartype');
                for (i = 0; i < s.length; i++) {
                    if (s.item(i).value == current_val) {
                        s.item(i).value = 0;
                        s.item(i).dataset.changed = 'changed';
                    }
                }
            }
            obj.dataset.changed = 'changed';
            obj.value = current_val;
            hasChanged = true;

        }

        function formSetChanged() {
            hasChanged = true;
        }

        window.onbeforeunload = function () {
            if (hasChanged && !formSubmitting) {
                return "You have unsaved changes";
            }
        }

        function default_focus() {
            document.getElementById("ct2").focus();
        }

    </script>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/importguide/designedit",
        "reset_link":"dashin/owner/importguide/designedit/list/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
