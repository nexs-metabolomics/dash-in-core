{% extends "modules/dashin/owner/importguide/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Upload data file") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"upload"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/upload/{{ _ls_querystring_ }}" accept-charset="utf-8" class="nds upload-dataset" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["cancel","Cancel","button cancel"],["submit","Upload","button submit has-spinner"]],"form1") }}

  {% endblock %}
