{% extends "modules/dashin/owner/importguide/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Import complete") }}
    {% if header_pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"complete"]) }}
  {% endblock %}



  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/upload/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

  {% endblock %}

  {% block page_content %}

    {% if dataset_info is not defined %}

      {{ m_emptypage_filler("No newly imported dataset", "Go to 'Upload'", "gotoupload", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          <div class="card-title" style="font-size: 200%">
            {{ get_title(false) }}
          </div>
          <div class="card-row-line-divider">
          </div>
          <div class="card-row">
            <div class="card-column">

              <div class="card-field-row">
                <div class="card-field-header">
                  {{ dataset_info.name }}
                </div>
              </div>

              <div class="card-field-row">
                <div class="card-field-content">
                  {{ dataset_info.description }}
                </div>
              </div>

            </div>
          </div>
          <div class="card-row">
            <button form="form1" name="btn[gotupload]" class="button submit">
              Upload another dataset
            </button>
            <button form="form1" name="btn[newstudy]" class="button submit" style="margin-left: 8px">
              Create new study from dataset
            </button>
          </div>
        </div>
      </div>

    {% endif %}

  {% endblock %}

