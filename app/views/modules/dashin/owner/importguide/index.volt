{% extends "modules/dashin/owner/import/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Datasets") }}
  {% endblock %}

  {% block index_header %}
    <header>{{ get_title(false) }}</header>
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-small">

      <ul>

        {{ m_indexpage_menu_entry("Dataset", "section-dataset","/dashin/owner/import/listdatasets","00img_partials/v1/icons/dataset-upload-icon.svg", _ls_querystring_,"datasets-index-datasets") }}
        {{ m_indexpage_menu_entry("Upload varinfo", "section-dataset","/dashin/owner/varinfo/listupvardatasets","00img_partials/v1/icons/upload-varinfo-icon.svg", _ls_querystring_, "datasets-index-variable_info") }}
        {{ m_indexpage_menu_entry("Match varinfo", "section-dataset","/dashin/owner/varinfo/listvardatasets","00img_partials/v1/icons/match-varinfo-icon.svg", _ls_querystring_, "datasets-index-variable_info") }}

      </ul>

    </nav>
  {% endblock %}
