{% extends "modules/dashin/owner/importguide/00-subroot-subnavpage.volt" %}
  {% block set_params %}
    {{ set_title("View dataset design") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/importguide/00_tabs_partial",["current_tab":"datasetdesign"]) }}
  {% endblock %}

  {% block subheader_subnav %}
    {{ partial("modules/dashin/owner/importguide/00_dataset_design_tabs_partial",["current_tab":"view"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/importguide/designview/{{ _ls_querystring_ }}" accept-charset="utf-8" enctype="multipart/form-data" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    {% if pre_dataset_design is not defined %}

      {{ m_emptypage_filler("No newly imported dataset", "Go to 'Upload'", "gotoupload", "form1") }}

    {% else %}


      <div class="table-container-fixed-header-card">

        <div class="table-card-title">
          {{ get_title(false) }}
        </div>

        <div class="table-container fixed-header">

          <table>

            <thead>
              <tr>
                <th>Event</th>
                <th>Subevent</th>
                <th>Sampling event</th>
                <th>Sampling ordinal time</th>
                <th>Center</th>
                <th>Startgroup</th>
                <th>Subject</th>
                <th>n</th>
              </tr>
            </thead>

            <tbody>
              {% for row in pre_dataset_design %}
                <tr>
                  <td title="{{ row.event_name }}">
                    {{ row.event_name }}
                  </td>
                  <td title="{{ row.subevent_name }}">
                    {{ row.subevent_name }}
                  </td>
                  <td title="{{ row.samplingevent_name }}">
                    {{ row.samplingevent_name }}
                  </td>
                  <td title="{{ row.samplingtime_name }}">
                    {{ row.samplingtime_name }}
                  </td>
                  <td title="{{ row.center_name }}">
                    {{ row.center_name }}
                  </td>
                  <td title="{{ row.startgroup_name }}">
                    {{ row.startgroup_name }}
                  </td>
                  <td title="{{ row.subject_name }}">
                    {{ row.subject_name }}
                  </td>
                  <td title="{{ row.n }}">
                    {{ row.n }}
                  </td>
                </tr>
              {% endfor %}
            </tbody>

          </table>
        </div>
      </div>

    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {% if dataset is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/import/browsedata/",
        "act_link":"dashin/owner/import/browsedata/",
        "reset_link":"dashin/owner/import/browsedata/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
