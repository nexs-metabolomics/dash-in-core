{% extends "modules/dashin/owner/varinfo/00-subroot-var-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Match view") }}

  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial_ng",["current_tab":"matchsummary"]) }}
  {% endblock %}

 {% block subheader_action %}
   <form id="form1" action="/dashin/owner/varinfo/matchsummary/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
   <button form="form1" name="btn[selectvds]" class="button select">Select variableset</button>
 {% endblock %}

   {% block subheader_info %}
     {{ partial("modules/dashin/owner/varinfo/var_info_partial") }}
   {% endblock %}

  {% block page_content %}

    {% if var_dataset is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info set", "selectvds", "form1") }}

    {% else %}
      <div class="card-container">
        <div class="card">

          <div class="card-title">
            {{ get_title(false) }}
          </div>

          {% if var_dataset is defined %}
            <div class="card-column">
              <div class="card-fieldset-header">
                <header>{{ var_dataset.name }}</header>
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ var_dataset.description }}
              </div>
            </div>

            <div class="card-row">
              <div class="card-row">
                <div class="card-column">

                  <div class="card-field-row">
                    <div class="card-field-header">
                      Number of variables
                    </div>
                    <div class="card-field-content">
                      {{ var_dataset.n_var }}
                    </div>
                  </div>


                  <div class="card-field-row">
                    <div class="card-field-header">
                      Number of properties
                    </div>
                    <div class="card-field-content">
                      {{ var_dataset.n_row }}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-column">
                <div class="card-button-box">
                  <a href="/dashin/owner/varinfo/varedit/{{ _ls_querystring_ }}" class="card-img-button" title="Edit variable info"><img src="/img/icons/edit-v01.svg"></a>
                </div>
              </div>
            </div>
          {% endif %}

        </div>
      </div>
    {% endif %}
  {% endblock %}
