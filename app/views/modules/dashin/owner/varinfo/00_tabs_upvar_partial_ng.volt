<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/varinfo/listupvardatasets", "listupvardatasets", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Upload", "/dashin/owner/varinfo/uploadvarinfo", "uploadvarinfo", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Summary", "/dashin/owner/varinfo/upvarreport", "upvarreport", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/varinfo/upvaredit", "upvaredit", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Browse", "/dashin/owner/varinfo/browseupvardata", "browseupvardata", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Variables", "/dashin/owner/varinfo/viewupvarv", "viewupvarv", current_tab, _ls_querystring_) }}

</div>
