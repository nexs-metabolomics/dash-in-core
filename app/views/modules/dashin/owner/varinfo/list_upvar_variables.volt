{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View uploaded variable info variables") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"viewupvarv"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/viewupvarv/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[selectvds]">Select variableset</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/varinfo/upvar_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if pagination is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info", "selectvds", "form1") }}

    {% else %}

      <div class="table-container-fixed-header-card">

        <div class="table-card-title">
          {{ get_title(false) }}
        </div>

        <div class="table-container fixed-header">
          <form action="/dashin/owner/varinfoupload/listvariables/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
            <table style="width: 100%">

              <thead>
                <tr>
                  <th>Name</th>
                  <th>Datatype</th>
                  <th>Description</th>
                </tr>
              </thead>

              <tbody>
                {% for row in pagination.items %}
                  <tr>
                    <td>{{ row.name }}</td>
                    <td>{{ row.datatype }}</td>
                    <td>{{ row.description }}</td>
                  </tr>
                {% endfor %}
              </tbody>

            </table>
          </form>
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/varinfoupload/listvariables/",
        "reset_link":"dashin/owner/varinfoupload/listvariables/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
