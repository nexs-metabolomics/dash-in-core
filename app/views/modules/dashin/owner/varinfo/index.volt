{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Upload variable info") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"show_submenu_import_upvar":1,"current":"varinfo_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/uploadvarinfo/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Upload variable info
              </header>
              <p>Variable info is a set of additional information related to individual variables</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Summary
              </header>
              <p>Summary report for the uploaded variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/upvaredit/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit
              </header>
              <p>Edit name and description of uploaded variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/browseupvardata/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Browse data
              </header>
              <p>Browse data of the uploaded variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/viewupvarv/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Variables
              </header>
              <p>View variables in the uploaded variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List sets
              </header>
              <p>List uploaded variable info sets</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
