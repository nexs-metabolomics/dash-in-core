<div class="tabs-row">

  {{ m_menu_tabentry("List", "/dashin/owner/varinfo/listvardatasets", "listvardatasets", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Add to dataset", "/dashin/owner/varinfo/match", "match", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Summary", "/dashin/owner/varinfo/matchsummary", "matchsummary", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/varinfo/varedit", "varedit", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}
  {{ m_menu_tabentry("Browse", "/dashin/owner/varinfo/browsevardata", "browsevardata", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Variables", "/dashin/owner/varinfo/listvdvariables", "listvdvariables", current_tab, _ls_querystring_, ROLE_ORG_ALLOW_WRITE) }}

</div>
