{% extends "modules/dashin/owner/varinfo/00-subroot-var-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit var dataset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial_ng",["current_tab":"varedit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/varedit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button" name="btn[selectvardataset]">Select variableset</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/varinfo/var_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if var_dataset is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info", "selectvardataset", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit has-spinner"],["apply","Apply","button apply has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

    {% endif %}
  {% endblock %}
