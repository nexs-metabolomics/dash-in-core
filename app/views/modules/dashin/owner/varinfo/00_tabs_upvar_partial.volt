<div class="nds-tabs-row">
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "listupvardatasets" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "uploadvarinfo" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/uploadvarinfo/{{ _ls_querystring_ }}">
      Upload
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "upvarreport" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}">
      Summary
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "upvaredit" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/upvaredit/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "browseupvardata" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/browseupvardata/{{ _ls_querystring_ }}">
      Browse
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "viewupvarv" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/viewupvarv/{{ _ls_querystring_ }}">
      Variables
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "match" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}">
      Add to dataset
    </a>
  </div>
</div>
