{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit variable info set") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"upvaredit"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/upvaredit/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button" name="btn[selectvs]">Select uploaded variableset</button>
  {% endblock %}


  {% block subheader_info %}
    {{ partial("modules/dashin/owner/varinfo/upvar_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if upvar_dataset is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info", "selectvs", "form1") }}

    {% else %}

      {{ m_form_innerloop_card(form,[["submit","Ok","button submit has-spinner"],["apply","Apply","button apply has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

    {% endif %}
  {% endblock %}
