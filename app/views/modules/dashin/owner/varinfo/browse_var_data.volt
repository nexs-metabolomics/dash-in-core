{% extends "modules/dashin/owner/varinfo/00-subroot-var-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Inspect variable info data") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial_ng",["current_tab":"browsevardata"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/browsevardata/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="nds-button{{ btn_selectvds_selected }}" name="btn[selectvds]">Select variableset</button>
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Variable info", var_dataset_info|default(false),"name") }}
  {% endblock %}


  {% block page_content %}
    {% if pagination_rows is not defined %}

      {{ m_emptypage_filler("Please select a variable-info set", "Select variableset", "selectvds", "form1") }}

    {% else %}
      
      <div class="table-container-fixed-header-card">
        
        <div class="table-card-title">
          {{ get_title(false) }}
        </div>
        
        <div class="table-container fixed-header">
          <table>

            <thead>
              <tr>
                <th>Varname</th>
                {% for col in pagination_cols.items %}
                  <th>
                    <div class="nds-row" style="justify-content: space-between;padding: 0 0.2rem">
                      {{ col.name }}
                      {# <button form="form1" name="btn[fi{% if fi_idx === loop.index0 %}_x{% endif %}]" #}
                      {# value="{{ loop.index0 }}/{{ pagination_cols.page_item_first }}/{{ pagination_rows.current }}/{{ pagination_cols.current }}" #}
                      {# class="nds-fi-button{% if fi_idx === loop.index0 %} mark{% endif %}"></button> #}
                    </div>
                  </th>
                {% endfor %}
              </tr>
            </thead>

            <tbody>
              {% for row in pagination_rows.items %}
                <tr>
                  <td>{{ row.name }}</td>
                  {% for cell in row.datarow %}
                    <td class="format-c">{{ cell }}</td>
                  {% endfor %}
                </tr>
              {% endfor %}
            </tbody>

          </table>

        </div>
      </div>

    {% endif %}
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination_rows is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/varinfo/browsevardata/",
        "act_link":"dashin/owner/varinfo/browsevardata/",
        "reset_link":"dashin/owner/varinfo/browsevardata/" ~ "/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
