{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List variable info sets") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"listupvardatasets"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      <header>
        {{ get_title(false) }}
      </header>

      {% if pagination is defined %}
        {% for row in pagination.items %}

          <div class="card{% if row.confirm is defined and row.confirm === true %} warning{% endif %}" id="rowno{{ loop.index }}">

            {% if row.confirm is defined and row.confirm === true %}
              <header>DELETE UPLOADED VARIABLE INFO DATASET?</header>
            {% endif %}

            <div class="card-column">
              <div class="card-fieldset-header">
                {{ row.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <div class="card-temp-spacing">
            </div>

            <div class="card-row">

              <div class="card-column">
                <div class="card-field-row">
                  <div class="card-field-header">
                    Upload date
                  </div>
                  <div class="card-field-content">
                    {{ row.uploaded_at }}
                  </div>
                </div>
                <div class="card-field-row">
                  <div class="card-field-header">
                    Variables
                  </div>
                  <div class="card-field-content">
                    {{ row.nrow }}
                  </div>
                </div>
                <div class="card-field-row">
                  <div class="card-field-header">
                    Features
                  </div>
                  <div class="card-field-content">
                    {{ row.ncol }}
                  </div>
                </div>
              </div>

              <div class="card-column">
                <div class="card-button-box">

                  <form action="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
                  
                    <div class="card-button-row">
                      {% if row.confirm is defined and row.confirm === true %}
                        <button name="btn[confirm]" value="{{ row.upvar_dataset_id }}"
                                class="button small alert confirm"
                                title="Confirm delete">Confirm
                        </button>
                        <button name="btn[cancel]" value="{{ row.upvar_dataset_id }}"
                                class="button small"
                                title="Cancel">Cancel
                        </button>
                      {% else %}
                        <button name="btn[view]" value="{{ row.upvar_dataset_id }}"
                                class="card-img-button"
                                title="View dataset">
                          <img src="/img/icons/eye.png" style="height: 2rem;width: 2rem">
                        </button>
                        <button name="btn[edit]" value="{{ row.upvar_dataset_id }}"
                                class="card-img-button"
                                title="Edit variable info">
                          <img src="/img/icons/edit-v01.svg">
                        </button>
                        <button name="btn[delete]" value="{{ row.upvar_dataset_id }}"
                                class="card-img-button"
                                title="Delete variable info">
                          <img src="/img/icons/delete-v02.svg">
                        </button>
                      {% endif %}

                    </div>

                  </form>
                </div>

              </div>
            </div>

          </div>
        {% endfor %}
      {% endif %}
    </div>
  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/varinfo/listupvardatasets",
        "reset_link":"dashin/owner/varinfo/listupvardatasets/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block footer %}
  {% endblock %}

