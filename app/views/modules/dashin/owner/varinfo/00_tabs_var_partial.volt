<div class="nds-tabs-row" style="margin-bottom: 1rem">
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "listvardatasets" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listvardatasets/{{ _ls_querystring_ }}">
      List
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "match" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}">
      Match
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "matchsummary" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/matchsummary/{{ _ls_querystring_ }}">
      Summary
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "varedit" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/varedit/{{ _ls_querystring_ }}">
      Edit
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "browsevardata" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/browsevardata/{{ _ls_querystring_ }}">
      Browse
    </a>
  </div>
  
  <div class="nds-tab{% if current_tab is defined and current_tab === "listvdvariables" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listvdvariables/{{ _ls_querystring_ }}">
      Variables
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "match" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}">
      Add to dataset
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "mapsearchcols" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}">
      Map search columns
    </a>
  </div>
  
</div>
