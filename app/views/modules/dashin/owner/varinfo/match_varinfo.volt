{% extends "modules/dashin/owner/varinfo/00-subroot-var-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Add variable info to dataset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial_ng",["current_tab":"match"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_dataset]">Select Dataset</button>
    <button form="form1" class="button select" name="btn[selectup]">Select Varinfo</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/varinfo/dataset_info_partial") }}
    {{ partial("modules/dashin/owner/varinfo/upvar_info_partial") }}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      {% if dataset is defined and upvar_dataset is defined and match_stats.matched and not has_errors %}

        <div class="card">
          <div class="card-column">
            <header>
              Add variable info set to dataset
            </header>
            <div>
              The uploaded variable info will be copied and attached to the current dataset. A name is required.
            </div>
          </div>

          {% if form is defined %}
            <form id="match-form" action="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ partial("00element_partials/standard_form_inner_loop",["button_set":["submit":"Ok","cancel":"Cancel"]]) }}

              <div class="form-container">
                <div class="form-buttonset">
                  {% if confirm is defined and confirm %}
                    <a href="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}" class="button">Cancel</a>
                    <button form="match-form" class="button" name="btn[confirm]">Confirm</button>
                  {% else %}
                    <button form="match-form" class="button" name="btn[match]">Match</button>
                  {% endif %}
                </div>
              </div>

            </form>


          {% endif %}

        </div>
      {% endif %}

      <div class="card">

        {% if var_dataset is defined and var_dataset %}

          <div class="card-row">
            <header>
              Dataset and variable info are already matched
            </header>
          </div>

        {% endif %}

        <div class="card-row">

          <div class="card-column">

            {% if dataset is not defined %}

              <div class="emptypage-container">
                <div class="emptypage-msg">
                  Please select a dataset
                </div>
                <div class="emptypage-action">
                  <button form="form1" class="button select" name="btn[select_dataset]">Select Dataset</button>
                </div>
              </div>

            {% else %}

              <header>
                Dataset
              </header>

              <div class="card-field-row">
                <div class="card-column">
                  <div class="card-fieldset-header">
                    {{ dataset.name }}
                  </div>

                  <div class="card-field-content">
                    {{ dataset.description }}
                  </div>
                </div>
              </div>

              <div class="card-fieldset-column">

                <div class="card-fieldset-row">
                  <div class="card-field-header">
                    No. of variables
                  </div>
                  <div class="card-field-content">
                    {{ dataset.n_var }}
                  </div>
                </div>

                <div class="card-fieldset-row">
                  <div class="card-field-header">
                    No. of rows
                  </div>
                  <div class="card-field-content">
                    {{ dataset.n_row }}
                  </div>
                </div>

              </div>

            {% endif %}

          </div>

          {# =================================================================== #}

          <div class="card-column">

            {% if upvar_dataset is not defined %}

              <div class="emptypage-container">
                <div class="emptypage-msg">
                  Please select variable info
                </div>
                <div class="emptypage-action">
                  <button form="form1" class="button select" name="btn[selectup]">Select Variable info</button>
                </div>
              </div>

            {% else %}

              <header>
                Variable info
              </header>

              <div class="card-field-row">
                <div class="card-column">
                  <div class="card-fieldset-header">
                    {{ upvar_dataset.name }}
                  </div>

                  <div class="card-field-content">
                    {{ upvar_dataset.description }}
                  </div>
                </div>
              </div>

              <div class="card-fieldset-column">

                <div class="card-fieldset-row">
                  <div class="card-field-header">
                    No. of variables
                  </div>
                  <div class="card-field-content">
                    {{ upvar_dataset.n_vars }}
                  </div>
                </div>

                <div class="card-fieldset-row">
                  <div class="card-field-header">
                    No. of properties
                  </div>
                  <div class="card-field-content">
                    {{ upvar_dataset.n_info }}
                  </div>
                </div>

              </div>

              {% if upvar_dataset.has_dup_rownames or upvar_dataset.has_dup_colnames or upvar_dataset.has_empty_colnames or upvar_dataset.has_empty_rownames %}

                {% set has_errors = true %}
                <div class="card-fieldset-row">
                  <div class="card-field-header">
                    Errors
                  </div>
                  <div class="card-field-content">
                    <a href="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}" class="nds-button">Review errors</a>
                  </div>
                </div>

              {% else %}
                {% set has_errors = false %}
              {% endif %}

            {% endif %}


            {# =================================================================== #}
          </div>
        </div>

        {% if match_stats is defined %}

          <div class="card-row">
            <header>
              Match overview
            </header>
          </div>
          <div class="card-field-row">
            <div class="card-column">

              <div class="card-fieldset-row">
                <div class="card-field-header">
                  Number of matched variables:
                </div>
                <div class="card-field-content">
                  {{ match_stats.matched }}
                </div>
              </div>

              <div class="card-fieldset-row">
                <div class="card-field-header">
                  Number of unmatched variables in data:
                </div>
                <div class="card-field-content">
                  {{ match_stats.dvar_unmatched }}
                </div>
              </div>

              <div class="card-fieldset-row">
                <div class="card-field-header">
                  Number of unmatched variables in variable info:
                </div>
                <div class="card-field-content">
                  {{ match_stats.upvar_unmatched }}
                </div>
              </div>

            </div>
            <div class="card-column">
            </div>
          </div>


        {% endif %}

      </div>

    </div>
  {% endblock %}
