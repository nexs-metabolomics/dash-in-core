{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Map search columns") }}

    {% set btn_selectvds_selected = "" %}
    {% if var_dataset_info is defined %}
      {% set btn_selectvds_selected = " nds-object-selected" %}
    {% endif %}

  {% endblock %}

  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"show_submenu_import_match":1,"current":"matchindex"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial",["current_tab":"mapsearchcols"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="nds-button{{ btn_selectvds_selected }}" name="btn[selectvds]">Select variableset</button>
  {% endblock %}
  {% block content2 %}

    <div class="nds-row" style="width: 100%;justify-content: space-between">
      <div class="nds-table-container">
        <header>Mapped columns</header>
        <table class="nds-data-table">
          <thead>

          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Column type</th>
            <th>Column Description</th>
            <th>Actions</th>
          </tr>
          </thead>

          <tbody>
          {% if mappedvars is defined %}
            {% for row in mappedvars %}
              <tr>
                <td>{{ row.var_name }}</td>
                <td>{{ row.var_description }}</td>
                <td>{{ row.coltype_name }}</td>
                <td>{{ row.coltype_description }}</td>
                <td class="actions">
                  {% if row.mapped === "show_map" %}
                    <button form="form1" class="nds-button nds-tiny" value="{{ row.search_column_type_id }}" name="btn[unselect]">Unselect</button>
                    <button form="form1" class="nds-button nds-tiny nds-notice" value="{{ row.search_column_type_id }}" name="btn[map]">Map</button>
                  {% elseif row.mapped === "show_unmap" %}
                    <button form="form1" class="nds-button nds-tiny nds-alert" value="{{ row.var_variable_id }}" name="btn[unmap]">Unmap</button>
                  {% else %}
                    <button form="form1" class="nds-button nds-tiny" value="{{ row.search_column_type_id }}" name="btn[unselect]">Unselect</button>
                  {% endif %}
                </td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>

        </table>
      </div>
    </div>

    <div class="nds-row" style="width: 100%;justify-content: space-between">
      <div class="" style="display: flex;flex:1 0 auto;padding: 0 1rem">
        <header>Variable set</header>
      </div>
      <div class="" style="display: flex;flex:1 0 auto;padding: 0 1rem">
        <header>Column types</header>
      </div>
    </div>

    <div class="nds-row">
      <div class="nds-table-container">

        {{ partial("00element_partials/nds_table_nav2",[
          "item_name":"variable",
          "nav_link":"dashin/owner/varinfo/mapsearchcols/"
        ]) }}

        <table class="nds-data-table">
          <thead>
          <tr>
            <th>Name</th>
            <th>Datatype</th>
            <th>Description</th>
            <th>Map Search column</th>
            <th>Actions</th>
          </tr>
          </thead>

          <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.datatype }}</td>
                <td>{{ row.description }}</td>
                <td>
                  <button form="form1" class="nds-button nds-tiny" value="{{ row.var_variable_id }}" name="btn[selectvar]">Select</button>
                </td>
                <td class="actions">
                </td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>

        </table>
      </div>
      <div class="nds-table-container">

        {# OBS add top-space to column types table to align with variable-set table #}
        <table class="nds-data-table" style="margin-top: 2rem">

          <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
          </thead>

          <tbody>
          {% if search_column_types is defined %}
            {% for row in search_column_types %}
              <tr>
                <td>{{ row.name }}</td>
                <td>{{ row.description }}</td>
                <td>
                  <div style="display: flex;flex-flow: row nowrap;justify-content: flex-end">
                    <button form="form1" class="nds-button nds-tiny" value="{{ row.search_column_type_id }}" name="btn[selectct]">Select</button>
                  </div>
                </td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>

        </table>
      </div>
    </div>
  {% endblock %}
