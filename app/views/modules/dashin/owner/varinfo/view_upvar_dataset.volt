{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Summary for uploaded variable info set") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"upvarreport"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[selectvs]">Select uploaded variableset</button>
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/varinfo/upvar_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if upvar_dataset is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info set", "selectvs", "form1") }}

    {% else %}

      <div class="card-container">
        <div class="card">
          
          <div class="card-title">
            {{ get_title(false) }}
          </div>

          {% if upvar_dataset is defined %}
            <div class="card-column">
              <div class="card-fieldset-header">
                {{ upvar_dataset.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-field-content">
                {{ upvar_dataset.description }}
              </div>
            </div>

            <div class="card-row">
              <div class="card-row">
                <div class="card-column">

                  <div class="card-field-row">
                    <div class="card-field-header">
                      Number of variables
                    </div>
                    <div class="card-field-content">
                      {{ upvar_dataset.n_vars }}
                    </div>
                  </div>

                  <div class="card-field-row">
                    <div class="card-field-header">
                      Number of properties
                    </div>
                    <div class="card-field-content">
                      {{ upvar_dataset.n_info }}
                    </div>
                  </div>

                </div>
              </div>

              <div class="card-column">
                <div class="card-button-box">
                  <a href="/dashin/owner/varinfo/upvaredit/{{ _ls_querystring_ }}" class="card-img-button" title="Edit variable info"><img src="/img/icons/edit-v01.svg"></a>
                </div>
              </div>
              
            </div>
          {% endif %}

          <div class="nds-view-container view-dataset">
            {% if duplicate_varnames is defined %}
              <form id="dup" action="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
              <header>Duplicate variable names</header>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  Name
                </div>
                <div class="nds-view-value">
                  number of dups
                </div>
              </div>
              {% for dpv in duplicate_varnames %}
                <div class="nds-view-block mark">
                  <div class="nds-view-header" style="width: 100%">
                    <button form="dup" class="nds-button-transparent" name="btn[dupvname]" value="{{ dpv.name }}">{{ dpv.name }}</button>
                  </div>
                  <div class="nds-view-value" style="width: 2rem">
                    {{ dpv.n }}
                  </div>
                </div>
              {% endfor %}
            {% endif %}
          </div>

          <div class="nds-view-container view-dataset">
            {% if empty_varnames is defined %}
              <header>Empty variable names</header>
              {% for ev in empty_varnames %}
                <div class="nds-view-block mark">
                  <div class="nds-view-header">
                    {{ ev.name }}
                  </div>
                  <div class="nds-view-value">
                    {{ ev.n }}
                  </div>
                </div>
              {% endfor %}
            {% endif %}
          </div>

          <div class="nds-view-container view-dataset">
            {% if duplicate_upvarnames is defined %}
              <header>Duplicate variable info names</header>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  Name
                </div>
                <div class="nds-view-value">
                  number of dups
                </div>
              </div>
              {% for dpuv in duplicate_upvarnames %}
                <div class="nds-view-block mark">
                  <div class="nds-view-header">
                    {{ dpuv.name }}
                  </div>
                  <div class="nds-view-value">
                    {{ dpuv.n }}
                  </div>
                </div>
              {% endfor %}
            {% endif %}
          </div>

          <divx3 class="nds-view-container view-dataset">
            {% if empty_upvarnames is defined %}
              <header>Empty variable info names</header>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  ID
                </div>
                <div class="nds-view-value">
                  Name
                </div>
              </div>
              {% for euv in empty_upvarnames %}
                <div class="nds-view-block mark">
                  <div class="nds-view-header" style="width: 100%">
                    <button form="dup" class="nds-button-transparent" name="btn[emptyname]" value="{{ euv.upvar_variable_id }}">{{ euv.upvar_variable_id }}</button>
                  </div>
                  <div class="nds-view-value">
                    {{ euv.name }}
                  </div>
                </div>
              {% endfor %}
            {% endif %}
          </divx3>

        </div>
      </div>
    {% endif %}
  {% endblock %}
