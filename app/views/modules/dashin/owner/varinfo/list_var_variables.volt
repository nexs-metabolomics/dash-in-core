{% extends "modules/dashin/owner/varinfo/00-subroot-var-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("View varinfo variables") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial_ng",["current_tab":"listvdvariables"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/listvdvariables/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[select_dataset]">Select dataset</button>
    <div style="margin-left: auto"></div>
    <button form="form1" class="button save" name="btn[save]">Save mapping</button>
    {#    <button form="form1" class="nds-button" name="btn[convert]" title="Detect and convert datatypes">Convert</button> #}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Dataset", dataset_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}

    {% if pagination is not defined %}

      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}

    {% else %}
      <div class="table-container-fixed-header-card">

        <div class="table-card-title">
          {{ get_title(false) }}
        </div>

        <div class="table-container fixed-header">
          <table>

            <thead>
              <tr>
                <th>Name</th>
                <th>Datatype</th>
                <th>Description</th>
                <th>Mapped Search column</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {% for row in pagination.items %}
                <tr>
                  <td>{{ row.name }}</td>
                  <td>{{ row.datatype }}</td>
                  <td>{{ row.description }}</td>
                  <td>{{ row.mapped_column.setAttribute("form","form1") }}</td>
                  <td class="actions">
                  </td>
                </tr>
              {% endfor %}
            </tbody>

          </table>
        </div>
      </div>
    {% endif %}

  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/varinfo/listvdvariables/",
        "act_link":"dashin/owner/varinfo/listvdvariables/",
        "reset_link":"dashin/owner/varinfo/listvdvariables/1/1"
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
