{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Browse variable info set data") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"browseupvardata"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/browseupvardata/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="button select" name="btn[selectvs]">Select variableset</button>
    <button form="form1" class="button download" name="btn[download]" style="margin-left: auto" title="Download variableset">Download variableset</button>
  {% endblock %}

  {% block subheader_info %}
    {% if upvar_dataset is defined %}
      <div class="info-element">
        <div class="info-element-header">
          Variable info
        </div>
        <div class="info-element-content">
          {{ upvar_dataset.name }}
        </div>
      </div>
    {% endif %}
  {% endblock %}

  {% block page_content %}

    {% if pagination_rows is not defined %}

      {{ m_emptypage_filler("Please select a variable info set", "Select variable info", "selectvs", "form1") }}

    {% else %}

      <div class="table-container-fixed-header-card">
      
        <div class="table-card-title">
          {{ get_title(false) }}
        </div>
      
        <div class="table-container fixed-header">
          <table>
            <thead>
              <tr>
                <th>Varname</th>
                {% for col in pagination_cols.items %}
                  <th>
                    {{ col.name }}
                  </th>
                {% endfor %}
              </tr>
            </thead>

            <tbody>
              {% for row in pagination_rows.items %}
                <tr>
                  <td>{{ row.name }}</td>
                  {% for cell in row.datarow %}
                    <td class="format-c">{{ cell }}</td>
                  {% endfor %}
                </tr>
              {% endfor %}
            </tbody>

          </table>
        </div>
      </div>
    {% endif %}

  {% endblock %}

  {% block subfooter_content %}
    {% if pagination_rows is defined %}
      {{ partial("00element_partials/table_nav_double_ng",[
        "nav_link":"dashin/owner/varinfo/browseupvardata/",
        "act_link":"dashin/owner/varinfo/browseupvardata/",
        "reset_link":"dashin/owner/varinfo/browseupvardata//1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
