{% extends "modules/dashin/owner/varinfo/00-subroot-upvar-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Upload variable info set") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_upvar_partial_ng",["current_tab":"uploadvarinfo"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/varinfo/uploadvarinfo/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" enctype="multipart/form-data" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_info %}
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["submit","Ok","button submit has-spinner"],["cancel","Cancel","button cancel"]],"form1") }}

  {% endblock %}
