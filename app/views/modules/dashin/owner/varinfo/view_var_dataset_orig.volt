{% extends "layouts/basepage-nav.volt" %}
  {% block set_params %}
    {{ set_title("Match view") }}

    {% set btn_selectvds_selected = "" %}
    {% if var_dataset is defined %}
      {% set btn_selectvds_selected = " nds-object-selected" %}
    {% endif %}

  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"show_submenu_import_match":1,"current":"matchindex"]) }}
  {% endblock %}
  {% block tab_menu %}
    {{ partial("modules/dashin/owner/varinfo/00_tabs_var_partial",["current_tab":"matchsummary"]) }}
  {% endblock %}
  {% block toolbar_1 %}
    <form id="form1" action="/dashin/owner/varinfo/matchsummary/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <button form="form1" class="nds-button{{ btn_selectvds_selected }}" name="btn[selectvds]">Select variableset</button>
  {% endblock %}
  {% block content2 %}

    <div class="nds-card-container">
      <div class="nds-card">
        <div class="nds-card-section">
          {% if var_dataset is defined %}
            <div class="nds-view-container view-dataset">
              <div class="nds-view-block">
                <div class="nds-view-header">
                  <header>Variable info-set</header>
                </div>
                <div class="nds-view-value">
                  <header>{{ var_dataset.name }}</header>
                </div>
              </div>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  Description
                </div>
                <div class="nds-view-value">
                  {{ var_dataset.description }}
                </div>
              </div>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  Number of variables
                </div>
                <div class="nds-view-value">
                  {{ var_dataset.n_var }}
                </div>
              </div>
              <div class="nds-view-block">
                <div class="nds-view-header">
                  Number of properties
                </div>
                <div class="nds-view-value">
                  {{ var_dataset.n_row }}
                </div>
              </div>
              <a href="/dashin/owner/varinfo/varedit/{{ _ls_querystring_ }}" class="nds-button nds-expanded">Edit</a>
            </div>
          {% endif %}

        </div>
      </div>
    </div>
  {% endblock %}
