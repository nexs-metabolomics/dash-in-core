{% extends "layouts/basepage-index.volt" %}
  {% block set_params %}
    {{ set_title("Match variable info") }}
  {% endblock %}
  {% block menu_wide_1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu_wide_2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"show_submenu_import_match":1,"current":"matchindex"]) }}
  {% endblock %}
  {% block content2 %}

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/match/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Match
              </header>
              <p>Match uploaded varinfo to variables in dataset and create a new varinfoset linked to the dataset</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/matchsummary/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Summary
              </header>
              <p>Summary report for the matched variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/varedit/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Edit
              </header>
              <p>Edit name and description of variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/browsevardata/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Browse data
              </header>
              <p>Browse data of the matched variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/viewvarv/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Variables
              </header>
              <p>View variables in the uploaded variable info set</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                Map search columns
              </header>
              <p>Map columns in dataset to corresponding column types</p>
            </div>
          </a>
        </li>

        <li class="nds-card">
          <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">
            <div class="info-group-inner">
              <header>
                List sets
              </header>
              <p>List uploaded variable info sets</p>
            </div>
          </a>
        </li>

      </ul>
    </nav>
  {% endblock %}
