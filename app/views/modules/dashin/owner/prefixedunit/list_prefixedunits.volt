{% extends "modules/dashin/owner/prefixedunit/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List prefixed units") }}
    {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}
  {% endblock %}
 
  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/prefixedunit/00_tabs_partial",["current_tab":"listprfxunits"]) }}
  {% endblock %}
  {% block toolbar_1 %}
  {% endblock %}
  {% block nav_header %}
    {{ partial("00element_partials/nds_table_nav_search_form_prefixedunit",[
      "nav_link":"dashin/owner/prfxunit/listprfxunits",
      "act_link":"dashin/owner/prfxunit/listprfxunits",
      "reset_link":"dashin/owner/prfxunit/listprfxunits/1/1",
      "has_results":has_results
    ]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/prfxunit/listprfxunits/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="table-container">


        <table>
          <thead>
          <tr>
            <th style="width: 5rem">Valid</th>
            <th>Symbol</th>
            <th>Name</th>
            <th>Domain</th>
          </tr>
          </thead>
          <tbody>
          {% if pagination is defined %}
            {% for row in pagination.items %}
              <tr>
                <td>
                  <div class="nds-row">
                  {% if row.valid === 2 %}
                    <button form="form1" name="btn[disable]" value="{{ row.prefixedunit_id }}" class="button disable">
                      {{ partial("00img_partials/icons/checkbox-checked-24px.svg") }}
                    </button>
                  {% else %}
                    <button form="form1" name="btn[enable]" value="{{ row.prefixedunit_id }}" class="button enable">
                      {{ partial("00img_partials/icons/checkbox-unchecked-24px.svg") }}
                    </button>
                  {% endif %}
                  </div>
                </td>
                <td>{{ row.combined_symbol }}</td>
                <td>{{ row.name }}</td>
                <td>{{ row.unitdomain_name }}</td>
              </tr>
            {% endfor %}
          {% endif %}
          </tbody>
        </table>
    </div>
  {% endblock %}
