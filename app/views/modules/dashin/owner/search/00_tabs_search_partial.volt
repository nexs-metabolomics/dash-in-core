<div class="tabs-row" style="margin-bottom: 1rem">
  
  <div class="tab{% if current_tab is defined and current_tab === "metabolomics" %} current{% endif %}">
    <a href="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}">
      Search type 1
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "type02" %} current{% endif %}">
    <a href="/dashin/owner/search/metabolomicstype02/{{ _ls_querystring_ }}">
      Search type 2
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "savemetabolomics" %} current{% endif %}">
    <a href="/dashin/owner/search/savemetabolomics/{{ _ls_querystring_ }}">
      Save resultset (type 1)
    </a>
  </div>

  <div class="tab{% if current_tab is defined and current_tab === "savetype02" %} current{% endif %}">
    <a href="/dashin/owner/search/savemetabolomicstype02/{{ _ls_querystring_ }}">
      Save resultset (type 2)
    </a>
  </div>

</div>
