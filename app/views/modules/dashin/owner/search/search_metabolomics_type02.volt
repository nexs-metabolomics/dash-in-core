{% extends "modules/dashin/owner/search/00-subroot-standardpage-m2.volt" %}
  {% block set_params %}
    {{ set_title("Search metabolomics 2") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_search_m2_partial",["current_tab":"metabolomics02"]) }}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      <header style="font-family: Roboto;font-size: 150%;font-weight: bold">Search</header>

      <div class="form-container metabolsearch">

        {% if no_search_columns is defined and no_search_columns %}
          Could not find any searchable data. <a href="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}" style="text-decoration: underline;color: #0000FF">Did you define search columns?</a>
        {% endif %}

        {% if form is defined %}
          <form id="form1" action="/dashin/owner/search/metabolomicstype02/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

          <input form="form1" type="hidden" name="togglestate_study" value="{{ details_study }}">
          <input form="form1" type="hidden" name="togglestate_metabol" value="{{ details_metabol }}">

          {# Add a hidden first button to prevent the toggle button from being submitted when form is submitted by "enter" #}
          <div style="position: absolute;left: -1000px">
            <button form="form1" name="btn[search]"></button>
          </div>

          <div class="card">
            <details {{ details_metabol }}>

              <summary style="display: flex;flex-flow: row">

                {% if details_metabol === "closed" %}

                  {{ partial("00img_partials/triangle-right-svg") }}
                  <button form="form1" type="submit" name="btn[togglemetabol]" value="open" class="button metabolsearch details-toggle">Metabolomics search parameters</button>

                {% else %}

                  {{ partial("00img_partials/triangle-down-svg") }}
                  <button form="form1" type="submit" name="btn[togglemetabol]" value="closed" class="button metabolsearch details-toggle">Metabolomics search parameters</button>

                {% endif %}

              </summary>

              <div class="form-fieldset">
                {% if form.has("fulltext") %}

                  <label>{{ form.get("fulltext").getLabel() }}:</label>

                  <div class="form-field metabolsearch">
                    {{ form.render("fulltext",["form":"form1"]) }}
                  </div>

                {% endif %}
              </div>
              <div class="form-fieldset">
                {% if form.has("metadata") %}
                  <label>{{ form.get("metadata").getLabel() }}:</label>
                  <div class="form-field">
                    {{ form.render("metadata",["form":"form1"]) }}
                  </div>
                {% endif %}
              </div>

              <div class="form-fieldset">
                <label>Mz:</label>
                <div class="form-field">
                  <div style="margin-right: 0.5rem">Value:</div>{% if form.has("mz_center") %}{{ form.render("mz_center",["form":"form1"]) }}{% endif %}
                  <div style="margin: 0 0.5rem">Range:</div>{% if form.has("mz_range") %}{{ form.render("mz_range",["form":"form1"]) }}{% endif %}
                </div>
              </div>

              <div class="form-fieldset">
                <label>Rt:</label>
                <div class="form-field">
                  <div style="margin-right: 0.5rem">Value:</div>{% if form.has("rt_center") %}{{ form.render("rt_center",["form":"form1"]) }}{% endif %}
                  <div style="margin: 0 0.5rem">Range:</div>{% if form.has("rt_range") %}{{ form.render("rt_range",["form":"form1"]) }}{% endif %}
                </div>
              </div>
            </details>
          </div>

          <div class="card">
            <details {{ details_study }}>

              <summary style="display: flex;flex-flow: row">

                {% if details_study === "closed" %}

                  {{ partial("00img_partials/triangle-right-svg") }}
                  <button form="form1" type="submit" name="btn[togglestudy]" value="open" class="button metabolsearch details-toggle">Study search parameters</button>

                {% else %}

                  {{ partial("00img_partials/triangle-down-svg") }}
                  <button form="form1" type="submit" name="btn[togglestudy]" value="closed" class="button metabolsearch details-toggle">Study search parameters</button>

                {% endif %}

              </summary>

              {% if form_study is defined %}
                {% for e in form_study %}

                  {% if e.getAttribute('required') %}
                    {% set required_asterisk = " * " %}
                    {% set required_popup = ' title="Required field"' %}
                  {% else %}
                    {% set required_asterisk = "" %}
                    {% set required_popup = "" %}
                  {% endif %}

                  <div class="form-fieldset{% if e.hasMessages() %} error{% endif %}"{{ required_popup }} style="margin-top: 8px">
                    {% if e.getLabel()!="" %}
                      {% if e.getUserOption("type")=="select" %}
                        <label id="select-label" for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                      {% else %}
                        <label for="{{ e.getName() }}">{{ e.getLabel() }}{{ required_asterisk }}</label>
                      {% endif %}
                    {% endif %}

                    <div class="form-field" style="margin-top: -8px;padding-top: 0">
                      {{ e.render(["form":"form1"]) }}

                    </div>
                  </div>

                {% endfor %}
              {% endif %}
            </details>
          </div>

          <div class="form-buttonset" style="margin-top: 12px">
            <button form="form1" name="btn[search]" class="button metabolsearch search" title="Search metabolomics metadata">Search</button>
            <button form="form1" name="btn[save]" class="button metabolsearch save" title="Save the search result for download">Save</button>
            <a href="/dashin/owner/search/metabolomicstype02/1/1/{{ _ls_querystring_ }}" class="button metabolsearch reset" title="Reset the search form">Reset</a>
          </div>

        {% endif %}
      </div>

      {% if summary is defined and summary %}
        <div class="card">
          <div class="nds-table-container">
            <header>Summary</header>
            <table class="nds-data-table">
              <thead>
                <tr>
                  <th>Study</th>
                  <th>Dataset</th>
                  <th>Results</th>
                  <th>M/Z min/max</th>
                  <th>Rt min/max</th>
                </tr>
              </thead>
              <tbody>
                {% for ds in summary %}
                  <tr>
                    <td>{{ ds.study_name }}</td>
                    <td>{{ ds.dataset_name }}</td>
                    <td>{{ ds.n }}</td>
                    <td>{{ ds.mz_min }} - {{ ds.mz_max }}</td>
                    <td>{{ ds.rt_min }} - {{ ds.rt_max }}</td>
                  </tr>
                {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      {% endif %}
      {% if pagination is defined %}
        <div class="card">
          <div class="nds-table-container">
            <header>Results</header>

            {{ partial("00element_partials/table_nav",[
              "item_name":"variable",
              "nav_link":"dashin/owner/search/metabolomicstype02"
            ]) }}

            <table class="nds-data-table">
              <thead>
                <tr>
                  <th>Study name</th>
                  <th>Dataset name</th>
                  <th>Variable name</th>
                  <th>Mz</th>
                  <th>Rt</th>
                </tr>
              </thead>
              <tbody>
                {% for row in pagination.items %}
                  <tr>
                    <td title="{{ row.study_name }}">{{ row.study_name }}</td>
                    <td title="{{ row.dataset_name }}">{{ row.dataset_name }}</td>
                    <td>{{ row.variable_name }}</td>
                    <td>{{ row.mz }}</td>
                    <td>{{ row.rt }}</td>
                  </tr>
                {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      {% endif %}
    </div>

  {% endblock %}
