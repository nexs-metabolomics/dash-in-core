{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Search general") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("welcome/welcome_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"current":"search"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <div class="nds-form-container">
      <div class="nds-tabs-row">
        <div class="nds-tab current">
          <a href="/dashin/owner/search/default/{{ _ls_querystring_ }}">
            General
          </a>
        </div>
        <div class="nds-tab">
          <a href="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}">
            Metbolomics
          </a>
        </div>
        <div class="nds-tab">
          <a href="/dashin/owner/search/anthropometry/{{ _ls_querystring_ }}">
            Anthropometry
          </a>
        </div>
      </div>
      <form action="" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
        <div class="nds-fieldset">
          <label>Fulltext (search any field):
            <input type="text">
          </label>
        </div>
        <div class="nds-btn-group">
          <button name="btn[submit]" class="nds-button nds-expanded">
            Search
          </button>
        </div>
      </form>
    </div>

  {% endblock %}
