{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Search Anthropometry") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_import":1,"current":"search"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <div class="nds-form-container">
      <div class="nds-tabs-row">
        <div class="nds-tab">
          <a href="/dashin/owner/search/default/{{ _ls_querystring_ }}">
            General
          </a>
        </div>
        <div class="nds-tab">
          <a href="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}">
            Metbolomics
          </a>
        </div>
        <div class="nds-tab current">
          <a href="/dashin/owner/search/anthropometry/{{ _ls_querystring_ }}">
            Anthroprometry
          </a>
        </div>
      </div>
      <form action="" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">
        <div class="nds-fieldset">
          <label>Fulltext (search any field):
            <input type="text">
          </label>
        </div>
        <div class="nds-fieldset">
          <div class="nds-row">
            <div class="nds-column">
              Sex:
              <select>
                <option>Any</option>
                <option>Male</option>
                <option>Female</option>
              </select>
            </div>
            <div class="nds-column">
              Adult/child:
              <select>
                <option>Any</option>
                <option>Adults</option>
                <option>Children</option>
              </select>
            </div>
          </div>
        </div>
        <div class="nds-fieldset">
          Age:
          <div class="nds-row">
            <input type="number">
            <select style="width: 20%">
              <option>Years</option>
              <option>Weeks</option>
              <option>Days</option>
            </select>
            <div style="width: 25%;text-align: center">&lt; X &lt;</div>
            <input type="number">
            <select style="width: 20%">
              <option>Years</option>
              <option>Weeks</option>
              <option>Days</option>
            </select>
          </div>
        </div>
        <div class="nds-fieldset">
          Weight:
          <div class="nds-row">
            <input type="number">
            <select style="width: 20%">
              <option>Kg</option>
              <option>g</option>
            </select>
            <div style="width: 25%;text-align: center">&lt; X &lt;</div>
            <input type="number">
            <select style="width: 20%">
              <option>Kg</option>
              <option>g</option>
            </select>
          </div>
        </div>
        <div class="nds-fieldset">
          Height:
          <div class="nds-row">
            <input type="number">
            <select style="width: 20%">
              <option>m</option>
              <option>cm</option>
              <option>mm</option>
            </select>
            <div style="width: 25%;text-align: center">&lt; X &lt;</div>
            <input type="number">
            <select style="width: 20%">
              <option>m</option>
              <option>cm</option>
              <option>mm</option>
            </select>
          </div>
        </div>
        <div class="nds-btn-group">
          <button name="btn[submit]" class="nds-button nds-expanded">
            Search
          </button>
        </div>
      </form>
    </div>

  {% endblock %}
