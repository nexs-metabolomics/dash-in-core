{% extends "layouts/root-standardpage.volt" %}
  

  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'search',"show_submenu":1,"current":"metabolomics_index"]) }}
  {% endblock %}


  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'search',"show_submenu":1,"current":"metabolomics_index"]) }}
  {% endblock %}

  {% block page_section_header %}
    <div class="section-header-inner">
      <div class="section-logo-container section-search">
        {{ partial("00img_partials/v1/icons/search-simple-icon.svg") }}
      </div>
      <a href="/dashin/owner/import/index/{{ _ls_querystring_ }}">Search</a>
    </div>
  {% endblock %}

