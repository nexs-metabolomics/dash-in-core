{% extends "modules/dashin/owner/search/00-subroot-standardpage-resultset.volt" %}
  {% block set_params %}
    {{ set_title("List resultsets") }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/search/listresults/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_resultset_partial",["current_tab":"listresults"]) }}
  {% endblock %}

  {% block page_content %}

    <div class="card-container">
      <header>Resultsets</header>
      {% if pagination is defined %}{% set has_results = true %}{% else %}{% set has_results = false %}{% endif %}

      {% if pagination is defined %}
        {% for row in pagination.items %}

          {% if row.confirm is defined and row.confirm === true %}
            {% set hover_action = " warning" %}
          {% else %}
            {% set hover_action = " hover-hide-action" %}
          {% endif %}

          <div class="card{{ hover_action }}" id="rowno{{ loop.index }}">
            <div class="card-column">
              <div class="card-fieldset-header">
                Name
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.name }}
              </div>
            </div>

            <div class="card-column">
              <div class="card-fieldset-header">
                Description
              </div>
            </div>
            <div class="card-column">
              <div class="card-field-content">
                {{ row.description }}
              </div>
            </div>

            <form action="/dashin/owner/search/listresults/{{ _ls_querystring_ }}#rowno{{ loop.index0 }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()">

              {{ m_card_button_box_confirm(
                row.resultset_id,
                row.confirm|default(null),
                [
                  ["editresultset","edit-icon48-filled","button edit icon24","Edit resultset",ROLE_ORG_ALLOW_WRITE],
                  ["download","view-icon48-filled","button delete icon24","Delete resultset",ROLE_ORG_ALLOW_WRITE]
                ]
              ) }}


            </form>
          </div>
        {% endfor %}
      {% endif %}
    </div>
  {% endblock %}
  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/search/listresults",
        "reset_link":"dashin/owner/search/listresults/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %} 

  {% block footer %}
  {% endblock %}
