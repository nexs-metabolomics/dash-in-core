{% extends "modules/dashin/owner/search/00-subroot-standardpage-resultset.volt" %}
  {% block set_params %}
    {{ set_title("Edit resultsets") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_resultset_partial",["current_tab":"editresultdataset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <button form="form2" name="btn[search]" class="button hidden-default"></button>
    <button form="form3" name="btn[search]" class="button hidden-default"></button>
    <form id="form1" action="/dashin/owner/search/editresultdataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <form id="form2" action="/dashin/owner/search/editresultdataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    <form id="form3" action="/dashin/owner/search/editresultdataset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_genericbutton("Go to download","finish","form1","button metabolsearch save",dataset_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another resultset","select_resultset","form1",resultset_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another dataset","select_dataset","form1",dataset_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Resultset", resultset_info|default(false),"name",33) }}
    {{ m_info_element("Dataset", dataset_info|default(false),"name",33) }}
  {% endblock %}

  {% block page_content %}

    {% if resultset_info is not defined %}
      {{ m_emptypage_filler("Please select a resultset", "Select resultset", "select_resultset", "form1") }}

    {% elseif dataset_info is not defined %}
      {{ m_emptypage_filler("Please select a dataset", "Select dataset", "select_dataset", "form1") }}
    {% else %}

      <div style="display: flex;flex-flow: row">
        <div class="card-container">
          <header>Selected variables</header>
          <div class="card">
            {% if selected is defined %}{% set has_sresults = true %}{% else %}{% set has_sresults = false %}{% endif %}

            {{ partial("00element_partials/table_nav_dual_filter2",[
              "nav_link":"dashin/owner/search/editresultdataset",
              "has_results":has_sresults,
              "pagination1":selected,
              "pagination2":unselected,
              "search_term":search_term1,
              "reset_name":"reset1",
              "form_id":"form2",
              "p":1
            ]) }}

            <table class="data-table" style="table-layout: fixed;width: 100%">

              <thead>
                <tr>
                  <th>Selected variables</th>
                  {#                <th>Name</th> #}
                </tr>
              </thead>

              <tbody>
                {% if selected is defined %}
                  {% for row in selected.items %}
                    <tr class="tbl-row">
                      <td>
                        <button form="form1" name="btn[remove]" value="{{ row.variable_id }}" class="button table-action icon alert">
                          {{ partial("00img_partials/v1/icons/minus-icon.svg") }} {{ row.name }}
                        </button>
                      </td>
                      {#                    <td>{{ row.name }}</td> #}
                    </tr>
                  {% endfor %}
                {% endif %}
              </tbody>

            </table>
          </div>
        </div>

        <div class="card-container">
          <header>Unselected variables</header>
          <div class="card">
            {% if unselected is defined %}{% set has_uresults = true %}{% else %}{% set has_uresults = false %}{% endif %}

            {{ partial("00element_partials/table_nav_dual_filter2",[
              "nav_link":"dashin/owner/search/editresultdataset",
              "has_results":has_sresults,
              "pagination1":selected,
              "pagination2":unselected,
              "search_term":search_term2,
              "reset_name":"reset2",
              "form_id":"form3",
              "p":2
            ]) }}

            <table class="data-table" style="table-layout: fixed;width: 100%">

              <thead>
                <tr>
                  <th>Unselected variables</th>
                  {#                <th>Name</th> #}
                </tr>
              </thead>

              <tbody>
                {% if unselected is defined %}
                  {% for row in unselected.items %}
                    <tr class="tbl-row">
                      <td>
                        <button form="form1" name="btn[add]" value="{{ row.variable_id }}" class="button table-action icon">
                          {{ partial("00img_partials/v1/icons/plus-icon.svg") }} {{ row.name }}
                        </button>
                      </td>
                      {#                    <td>{{ row.name }}</td> #}
                    </tr>
                  {% endfor %}
                {% endif %}
              </tbody>

            </table>
          </div>
        </div>

      </div>

    {% endif %}

  {% endblock %}
