{% extends "modules/dashin/owner/search/00-subroot-indexpage.volt" %}
  {% block set_params %}
    {{ set_title("Search") }}
  {% endblock %}

  {% block page_content %}

    <nav class="box-menu-container box-menu-small">

      <ul>

        {{ m_indexpage_menu_entry("Metabolomics 1", "section-search","/dashin/owner/search/metabolomics","00img_partials/v1/icons/search-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Metabolomics 2", "section-search","/dashin/owner/search/metabolomicstype02","00img_partials/v1/icons/search-icon.svg", _ls_querystring_) }}
        {{ m_indexpage_menu_entry("Resultsets", "section-search","/dashin/owner/search/listresults","00img_partials/v1/icons/list-icon.svg", _ls_querystring_) }}

      </ul>
    </nav>
  {% endblock %}
