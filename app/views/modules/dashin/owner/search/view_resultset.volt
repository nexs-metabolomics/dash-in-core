{% extends "modules/dashin/owner/search/00-subroot-standardpage-resultset.volt" %}
  {% block set_params %}
    {{ set_title("Download resultsets") }}
  {% endblock %}

{# ======================================================================= #}
  {% block main_menu_wide %}
    {{ partial("00menu/main_menu_wide",['submenu_name':'search',"show_submenu":1,"current":"resultsets_index"]) }}
  {% endblock %}


  {% block main_menu_narrow %}
    {{ partial("00menu/main_menu_narrow",['submenu_name':'search',"show_submenu":1,"current":"resultsets_index"]) }}
  {% endblock %}
{# ======================================================================= #}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_resultset_partial",["current_tab":"viewresultset"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/search/viewresultset/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another resultset","select_resultset","form1",resultset_info|default(null)) }}
    {#    <button form="form1" class="button select" name="btn[select_resultset]">Select resultset</button> #}
    <button form="form1" class="button download" name="btn[export]">Download</button>
  {% endblock %}

  {% block subheader_info %}
    {{ m_info_element("Resultset", resultset_info|default(false),"name",33) }}
  {% endblock %}

  {% block page_content %}

    {% if not (resultset_info is defined and resultset_info) %}

      {{ m_emptypage_filler("Please select a resultset", "Select resultset", "select_resultset", "form1") }}

    {% else %}


      <div class="card-container">
        <header>Resultset</header>
          <div class="card table-container">
                <div style="font-weight: bold;font-size: 150%;margin-bottom: 24px">
                  {{ resultset_info.name }}
                </div>

            <div style="font-weight: bold;font-size: 120%">
              Datasets in resultset
            </div>
            <table class="data-table">

              <thead>
                <tr>
                  <th>Actions</th>
                  <th>Study</th>
                  <th>Dataset</th>
                  <th>Number of variables</th>
                </tr>
              </thead>

              <tbody>
                {% if resultset is defined %}
                  {% for row in resultset %}
                    <tr class="tbl-row">
                      <td>
                        <button form="form1" name="btn[edit]" value="{{ row.dataset_id }}" class="button">Edit</button>
                      </td>
                      <td>{{ row.study_name }}</td>
                      <td>{{ row.dataset_name }}</td>
                      <td style="text-align: center">{{ row.n }}</td>
                    </tr>
                  {% endfor %}
                {% endif %}
              </tbody>

            </table>
          </div>
        </div>
      </div>
    {% endif %}
  {% endblock %}
