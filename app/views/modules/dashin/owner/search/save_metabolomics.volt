{% extends "modules/dashin/owner/search/00-subroot-standardpage-m1.volt" %}
  {% block set_params %}
    {{ set_title("Save resultset") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_search_m1_partial",["current_tab":"savemetabolomics"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/search/savemetabolomics/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    {{ m_form_innerloop_card(form,[["save","Save","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

    {% if not (summary is defined and summary) %}

      {{ m_emptypage_filler("No resultset, please make a search first", "Search metabolomics", "cancel", "form1") }}

    {% else %}
    
      <div class="card-container">

          <header>Summary</header>

        <div class="card table-container">
          <table class="data-table">
            <thead>
              <tr>
                <th>Study</th>
                <th>Dataset</th>
                <th>Results</th>
                <th>M/Z min/max</th>
                <th>Rt min/max</th>
              </tr>
            </thead>
            <tbody>
              {% for ds in summary %}
                <tr>
                  <td>{{ ds.study_name }}</td>
                  <td>{{ ds.dataset_name }}</td>
                  <td>{{ ds.n }}</td>
                  <td>{{ ds.mz_min }} - {{ ds.mz_max }}</td>
                  <td>{{ ds.rt_min }} - {{ ds.rt_max }}</td>
                </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
      
    {% endif %}
  {% endblock %}
