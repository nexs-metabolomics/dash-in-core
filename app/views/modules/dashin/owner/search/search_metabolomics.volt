{% extends "modules/dashin/owner/search/00-subroot-standardpage-m1.volt" %}
  {% block set_params %}
    {{ set_title("Search metabolomics 1") }}
  {% endblock %}

  {% block subheader_nav %}
    {{ partial("modules/dashin/owner/search/00_tabs_search_m1_partial",["current_tab":"metabolomics"]) }}
  {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
  {% endblock %}

  {% block page_content %}

    <div class="card-container">

      <header style="font-family: Roboto;font-size: 150%;font-weight: bold">Search</header>

      <div class="card">

        <div class="form-container metabolsearch">

          {% if no_search_columns is defined and no_search_columns %}
            Could not find any searchable data. <a href="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}" style="text-decoration: underline;color: #0000FF">Did you define search columns?</a>
          {% endif %}

          {% if form is defined %}
            <form id="form1" action="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>

            <div class="form-fieldset">

              <label>Mz:</label>

              <div class="form-field metabolsearch">
                <div class="field-left">Value:</div>{% if form.has("mz_center") %}{{ form.render("mz_center",["form":"form1"]) }}{% endif %}
                <div class="field-right">Range:</div>{% if form.has("mz_range") %}{{ form.render("mz_range",["form":"form1"]) }}{% endif %}
              </div>

            </div>

            <div class="form-fieldset">

              <label class="metabolsearch">Rt:</label>

              <div class="form-field metabolsearch">
                <div class="field-left">Value:</div>{% if form.has("rt_center") %}{{ form.render("rt_center",["form":"form1"]) }}{% endif %}
                <div class="field-right">Range:</div>{% if form.has("rt_range") %}{{ form.render("rt_range",["form":"form1"]) }}{% endif %}
              </div>

            </div>

            <div class="form-buttonset" style="margin-top: 12px">
              <button form="form1" name="btn[search]" class="button metabolsearch search">Search</button>
              <button form="form1" name="btn[save]" class="button metabolsearch save">Save</button>
              <a href="/dashin/owner/search/metabolomics/1/1/{{ _ls_querystring_ }}" class="button metabolsearch reset">Reset</a>
            </div>

          {% endif %}
        </div>
      </div>
    </div>


    {% if summary is defined and summary %}
      <div class="card-container metabolsearch">
        <header>Summary</header>
        <div class="card table-container">
          <table class="data-table">
            <thead>
              <tr>
                <th>Study</th>
                <th>Dataset</th>
                <th>Results</th>
                <th>M/Z min/max</th>
                <th>Rt min/max</th>
              </tr>
            </thead>
            <tbody>
              {% for ds in summary %}
                <tr>
                  <td>{{ ds.study_name }}</td>
                  <td>{{ ds.dataset_name }}</td>
                  <td>{{ ds.n }}</td>
                  <td>{{ ds.min_mz }} - {{ ds.max_mz }}</td>
                  <td>{{ ds.min_rt }} - {{ ds.max_rt }}</td>
                </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    {% endif %}

    {% if pagination is defined %}
      <div class="card-container">
        <header>Results</header>
        <div class="card table-container metabolsearch">

          {{ partial("00element_partials/table_nav",[
            "item_name":"variable",
            "nav_link":"dashin/owner/search/metabolomics"
          ]) }}

          <table class="data-table">
            <thead>
              <tr>
                <th>Study name</th>
                <th>Dataset name</th>
                <th>Variable name</th>
                <th>Mz</th>
                <th>Rt</th>
              </tr>
            </thead>
            <tbody>
              {% for row in pagination.items %}
                <tr>
                  <td title="{{ row.study_name }}">{{ row.study_name }}</td>
                  <td title="{{ row.dataset_name }}">{{ row.dataset_name }}</td>
                  <td>{{ row.variable_name }}</td>
                  <td>{{ row.mz }}</td>
                  <td>{{ row.rt }}</td>
                </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    {% endif %}

  {% endblock %}
