<div class="tabs-row" style="margin-bottom: 1rem">

  {{ m_menu_tabentry("List", "/dashin/owner/search/listresults", "listresults", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Edit", "/dashin/owner/search/editresultdataset", "editresultdataset", current_tab, _ls_querystring_) }}
  {{ m_menu_tabentry("Download", "/dashin/owner/search/viewresultset", "viewresultset", current_tab, _ls_querystring_) }}
  
</div>
