<ul class="nds-submenu">
  
  <li class="head{% if current is defined and current === "study_index" %} current{% endif %}">
    <a href="/dashin/owner/study/list/{{ _ls_querystring_ }}">Manage studies</a>
  </li>

  <li class="head{% if current is defined and current === "event_index" %} current{% endif %}">
    <a href="/dashin/owner/studydesign/viewstudydesign/{{ _ls_querystring_ }}">Study design</a>
  </li>

  <li class="head{% if current is defined and current === "studydata_index" %} current{% endif %}">
    <a href="/dashin/owner/studydata/createstudyrowset/{{ _ls_querystring_ }}">Data export</a>
  </li>

  <li class="head{% if current is defined and current === "studydata_index" %} current{% endif %}">
    <a href="#">Column types-2</a>
  </li>

  <li class="head{% if current is defined and current === "studydata_index" %} current{% endif %}">
    <a href="#">Column types-3</a>
  </li>

</ul>

