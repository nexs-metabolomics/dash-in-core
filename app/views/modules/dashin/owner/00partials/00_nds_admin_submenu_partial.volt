<ul class="nds-submenu">

  <li class="head{% if current is defined and current === "consort_index" %} current{% endif %}">
    <a href="/dashin/owner/consort/list/">Consortium</a>
  </li>

  <li class="head{% if current is defined and current === "assay_index" %} current{% endif %}">
    <a href="/dashin/owner/assay/list/">Assay</a>
  </li>

  <li class="head{% if current is defined and current === "datacolumntype_index" %} current{% endif %}">
    <a href="/dashin/owner/datacolumntype/list/">Datacolumntype</a>
  </li>

  <li class="head{% if current is defined and current === "prefixedunit_index" %} current{% endif %}">
    <a href="/dashin/owner/prfxunit/listprfxunits/">Prefixed unit</a>
  </li>

</ul>

