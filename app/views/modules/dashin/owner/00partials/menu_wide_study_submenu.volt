

<ul class="submenu study">
  {{ m_menu_submenuentry("Manage studies" ,"/dashin/owner/study/list"                         , _ls_querystring_ ,current , "study_index") }}
  {{ m_menu_submenuentry("Study design"   ,"/dashin/owner/studydesign/viewstudydesign"        , _ls_querystring_ ,current , "studydesign_index") }}
  {{ m_menu_submenuentry("Data export"    ,"/dashin/owner/studyexport/listconditions" , _ls_querystring_ ,current , "datafilters_index") }}
</ul>

