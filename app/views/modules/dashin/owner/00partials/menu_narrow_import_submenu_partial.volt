<ul class="submenu import">

  <li class="head{% if current is defined and current === "dataset_index" %} current{% endif %}">
    <a href="/dashin/owner/import/listdatasets/{{ _ls_querystring_ }}" title="Datasets">D-set</a>
  </li>

  <li class="head{% if current is defined and current === "varinfo_index" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}" title="Variable info">V-info</a>
  </li>

</ul>

