<ul class="submenu import">

  <li class="head{% if current is defined and current === "dataset_index" %} current{% endif %}">
    <a href="/dashin/owner/import/listdatasets/{{ _ls_querystring_ }}">Dataset</a>
  </li>

  <li class="head{% if current is defined and current === "varinfo_index" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">Variable info</a>
  </li>

</ul>

