<ul class="nds-submenu">
  <li class="head{% if current is defined and current === "metaboltype1" %} current{% endif %}">
    <a href="/dashin/owner/search/metabolomics/{{ _ls_querystring_ }}">Metabolomics Type 1</a>
  </li>

  <li class="head{% if current is defined and current === "metaboltype2" %} current{% endif %}">
    <a href="/dashin/owner/search/metabolomicstype02/{{ _ls_querystring_ }}">Metabolomics Type 2</a>
  </li>

  <li class="head{% if current is defined and current === "resultset" %} current{% endif %}">
    <a href="/dashin/owner/search/listresults/{{ _ls_querystring_ }}">Resultsets</a>
  </li>

  {# <li class="{% if current is defined and current === "viewresultset" %}current{% endif %}"> #}
  {# <a href="/dashin/owner/search/viewresultset/{{ _ls_querystring_ }}">View resultset</a> #}
  {# </li> #}
  
  {# <li class="{% if current is defined and current === "editresultdataset" %}current{% endif %}"> #}
  {# <a href="/dashin/owner/search/editresultdataset/{{ _ls_querystring_ }}">Edit resultset</a> #}
  {# </li> #}

</ul>

