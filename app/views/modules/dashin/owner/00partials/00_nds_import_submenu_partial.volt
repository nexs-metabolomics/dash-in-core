<ul class="nds-submenu">

  <li class="head{% if current is defined and current === "dataset_index" %} current{% endif %}">
    <a href="/dashin/owner/import/listdatasets/{{ _ls_querystring_ }}">Dataset</a>
  </li>

{#  {% if show_submenu_import_dataset is defined and show_submenu_import_dataset %}#}
{#    <li class="sub{% if current is defined and current === "uploaddata" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/uploaddata/{{ _ls_querystring_ }}">Upload</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "uploadguide" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/uploadsummary/{{ _ls_querystring_ }}">summary</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "editdataset" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/editdataset/{{ _ls_querystring_ }}">Edit</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "viewdata" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/browsedata/{{ _ls_querystring_ }}">data</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "listvars" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/listvariables/{{ _ls_querystring_ }}">Variables</a>#}
{#    </li>#}

{#    <li class="sub{% if current is defined and current === "listdatasets" %} current{% endif %}">#}
{#      <a href="/dashin/owner/import/listdatasets/{{ _ls_querystring_ }}">List datasets</a>#}
{#    </li>#}
{#  {% endif %}#}


  <li class="head{% if current is defined and current === "varinfo_index" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">Variable info</a>
  </li>

{#  {% if show_submenu_import_upvar is defined and show_submenu_import_upvar %}#}
{#    <li class="sub{% if current is defined and current === "uploadvarinfo" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/uploadvarinfo/{{ _ls_querystring_ }}">Upload varinfo</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "upvarreport" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/upvarreport/{{ _ls_querystring_ }}">Summary</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "upvaredit" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/upvaredit/{{ _ls_querystring_ }}">Edit</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "browsevarinfo" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/browseupvardata/{{ _ls_querystring_ }}">Data</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "viewupvarv" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/viewupvarv/{{ _ls_querystring_ }}">Variables</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "listupvardatasets" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/listupvardatasets/{{ _ls_querystring_ }}">List upvar sets</a>#}
{#    </li>#}
{#  {% endif %}#}

  <li class="head{% if current is defined and current === "matchindex" %} current{% endif %}">
    <a href="/dashin/owner/varinfo/mapsearchcols/{{ _ls_querystring_ }}">Search columns</a>
  </li>
{#  {% if show_submenu_import_match is defined and show_submenu_import_match %}#}
{#    <li class="sub{% if current is defined and current === "matchvarinfo" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/match/">Match varinfo</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "matchsummary" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/matchsummary/">Summary</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "varedit" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/varedit/">Edit</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "listvdvariables" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/listvdvariables/">Variables</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "browsevardata" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/browsevardata/">Varinfo data</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "listvardatasets" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/listvardatasets/">List varinfo sets</a>#}
{#    </li>#}
{#    <li class="sub{% if current is defined and current === "mapsearchcols" %} current{% endif %}">#}
{#      <a href="/dashin/owner/varinfo/mapsearchcols/">Map search columns</a>#}
{#    </li>#}
{#  {% endif %}#}
  {#<li class="head{% if current is defined and current === "search" %} current{% endif %}">#}
    {#<a href="/dashin/owner/search/default/">4. SEARCH</a>#}
  {#</li>#}
</ul>

