<ul class="submenu study">

  <li class="head{% if current is defined and current === "study_index" %} current{% endif %}">
    <a href="/dashin/owner/study/list/{{ _ls_querystring_ }}">Manage studies</a>
  </li>

  <li class="head{% if current is defined and current === "studydesign_index" %} current{% endif %}">
    <a href="/dashin/owner/studydesign/viewstudydesign/{{ _ls_querystring_ }}">Study design</a>
  </li>

  <li class="head{% if current is defined and current === "datafilters_index" %} current{% endif %}">
    <a href="/dashin/owner/studyexport/listconditions/{{ _ls_querystring_ }}">Data export</a>
  </li>

</ul>

