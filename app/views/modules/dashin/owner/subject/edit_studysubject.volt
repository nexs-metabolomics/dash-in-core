{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("Edit subject") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"editstudysubject"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/subject/editstudysubject/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
    {{ m_conditional_selectbutton("Select another subject","selectsubject","form1",subject_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
    {{ m_info_element("Subject", subject_info|default(false),"name") }}
  {% endblock %}

  {% block page_content %}


    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% else %}

      {% if form is not defined %}

        {{ m_emptypage_filler("Please select a subject", "Select subject", "selectsubject", "form1") }}

      {% else %}

        {{ m_form_innerloop_card(form,[["submit","Ok","button submit"],["cancel","Cancel","button cancel"]],"form1") }}

      {% endif %}
    {% endif %}

  {% endblock %}
