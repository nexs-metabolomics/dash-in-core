<div class="nds-tabs-row" style="margin-bottom: 1rem">

  <div class="nds-tab{% if current_tab is defined and current_tab === "createstudysubjects" %} current{% endif %}">
    <a href="/dashin/owner/subject/extractsubjects/{{ _ls_querystring_ }}">
      Import subjects
    </a>
  </div>

  <div class="nds-tab{% if current_tab is defined and current_tab === "editsubject" %} current{% endif %}">
    <a href="/dashin/owner/subject/editstudysubject/{{ _ls_querystring_ }}">
      Edit study subject
    </a>
  </div>

</div>
