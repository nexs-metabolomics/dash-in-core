{% extends "layouts/dashboard-1.volt" %}
  {% block set_params %}
    {{ set_title("Extract subjects") }}
  {% endblock %}
  {% block menu1 %}
    {{ partial("modules/dashin/00partials/module_header_menu_partial") }}
  {% endblock %}
  {% block menu2 %}
    {{ partial("modules/dashin/00partials/00_nds_index_submenu_partial",["show_submenu_study":1,"show_submenu_study_subject":1,"current":"subject_index"]) }}
  {% endblock %}
  {% block content2 %}
    <header>{{ get_title(false) }}</header>

    <nav class="nds-box-menus">
      <ul class="nds-card-menu">


      </ul>
    </nav>
  {% endblock %}
