{% extends "modules/dashin/owner/studydesign/00-subroot-standardpage.volt" %}
  {% block set_params %}
    {{ set_title("List subjects") }}
  {% endblock %}

   {% block subheader_nav %}
     {{ partial("modules/dashin/owner/studydesign/00_tabs_partial",["current_tab":"liststudysubjects"]) }}
   {% endblock %}

  {% block subheader_action %}
    <form id="form1" action="/dashin/owner/subject/liststudysubjects/{{ _ls_querystring_ }}" accept-charset="utf-8" method="post" onsubmit="formSetSubmit()" onchange="formSetChanged()"></form>
    {{ m_conditional_selectbutton("Select another study","select_study","form1",study_info|default(null)) }}
  {% endblock %}

  {% block subheader_info %}
    {{ partial("modules/dashin/owner/study/study_info_partial") }}
  {% endblock %}

  {% block page_content %}

    {% if study_info is not defined %}

      {{ m_emptypage_filler("Please select a study", "Select study", "select_study", "form1") }}

    {% endif %}

    {% if study_info is defined %}


        {% if pagination is defined %}



          <div class="table-container-fixed-header-card">
            
            <div class="table-card-title">
              {{ get_title(false) }}

              <div class="form-container">
                <div class="form-buttonset">
                  <button form="form1" name="btn[updateall]" class="button submit" title="Update all">
                    Save changes
                  </button>
                </div>
              </div>

            </div>
            
            <div class="table-container fixed-header" style="height: 100%!important;">

              <table style="height: 100%">

                <thead>
                  <tr>
                    <th style="width: 4rem">Actions</th>
                    <th>Subject key</th>
                    <th>Description</th>
                    <th>Startgroup</th>
                  </tr>
                </thead>

                <tbody>
                  {% for row in pagination.items %}
                    <tr id="row{{ loop.index0 }}" class="{% if row.confirm is defined and row.confirm === true %}warning{% endif %}">
                      <td>

                        {% if ROLE_ORG_ALLOW_WRITE %}

                          <form id="tblform{{ loop.index0 }}" action="/dashin/owner/subject/liststudysubjects/{{ current_page }}/{{ _ls_querystring_ }}{{ row.link_target }}" accept-charset="utf-8" method="post"></form>

                          <button form="tblform{{ loop.index0 }}" name="btn[edit]" value="{{ row.subject_id }}" class="button">Edit</button>
                          {% if row.confirm is defined and row.confirm === true %}
                            <button form="tblform{{ loop.index0 }}" name="btn[confirm]" value="{{ row.subject_id }}" class="button">Confirm</button>
                            <button form="tblform{{ loop.index0 }}" name="btn[cancel]" value="{{ row.subject_id }}" class="button">Cancel</button>
                          {% else %}
                            <button form="tblform{{ loop.index0 }}" name="btn[delete]" value="{{ row.subject_id }}" class="button">Delete</button>

                          {% endif %}
                        {% endif %}

                      </td>
                      <td>{{ row.name }}</td>
                      <td>{{ row.description }}</td>
                      <td class="align-left">
                        {{ row.startgroup_select }}
                        {{ row.hidden_field }}
                      </td>
                    </tr>
                  {% endfor %}
                </tbody>

              </table>

            </div>
          </div>
        {% endif %}

    {% endif %}

  {% endblock %}

  {% block subfooter_content %}
    {% if pagination is defined %}
      {{ partial("00element_partials/table_nav_filter",[
        "nav_link":"dashin/owner/subject/liststudysubjects/",
        "reset_link":"dashin/owner/subject/liststudysubjects/1/1",
        "has_results":has_results
      ]) }}
    {% endif %}
  {% endblock %}

  {% block page_footer_content %}
  {% endblock %}
