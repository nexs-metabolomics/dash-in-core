<li>
  <div class="module-header-inner{% if current is defined and current == "index_page" %} current{% endif %}">
    <a href="/dashin/index/{{ _ls_querystring_ }}">
      <div class="link-inner">
        {{ partial("00img_partials/v1/icons/home-icon.svg") }}
        Home
      </div>
    </a>
  </div>
</li>
