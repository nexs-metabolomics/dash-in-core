<ul class="module-submenu">

  {# ============================================================================== #}
  {% if show_submenu_import is defined %}
    <li class="main current">
      <a class="menu-entry" href="/dashin/owner/import/index/{{ show_submenu_import }}/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        Datasets
      </a>
    </li>
    {% if show_submenu_import %}
      {{ partial("modules/dashin/owner/00partials/menu_wide_import_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/dashin/owner/import/index/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        Datasets
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
  {% if show_submenu_study is defined %}
    <li class="main current">
      <a class="menu-entry" href="/dashin/owner/study/index/{{ show_submenu_study }}/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/studies-icon.svg") }}
        Studies
      </a>
    </li>
    {% if show_submenu_study === 1 %}
      {{ partial("modules/dashin/owner/00partials/menu_wide_study_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/dashin/owner/study/index/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/studies-icon.svg") }}
        Studies
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
  {% if show_submenu_search is defined %}
    <li class="main current">
      <a class="menu-entry" href="/dashin/owner/search/index/{{ show_submenu_search }}/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/search-simple-icon.svg") }}
        Search
      </a>
    </li>
    {% if show_submenu_search === 1 %}
      {{ partial("modules/dashin/owner/00partials/menu_wide_search_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/dashin/owner/search/index/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/search-simple-icon.svg") }}
        Search
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
  {% if show_submenu_admin is defined %}
    <li class="main current">
      <a class="menu-entry" href="/dashin/owner/admin/index/{{ show_submenu_admin }}/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        Admin
      </a>
    </li>
    {% if show_submenu_admin === 1 %}
      {{ partial("modules/dashin/owner/00partials/00_nds_admin_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/dashin/owner/admin/index/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        Admin
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
  {% if show_submenu_sysadmin is defined %}
    <li class="main current">
      <a class="menu-entry" href="/admin/index/{{ show_submenu_admin }}/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        System admin
      </a>
    </li>
    {% if show_submenu_sysadmin === 1 %}
      {{ partial("admin_modules/system/00partials/00_nds_admin_submenu_partial") }}
    {% endif %}
  {% else %}
    <li class="main">
      <a class="menu-entry" href="/admin/index/{{ _ls_querystring_ }}">
        {{ partial("00img_partials/v1/icons/dataset-icon.svg") }}
        System admin
      </a>
    </li>
  {% endif %}
  {# ============================================================================== #}
</ul>
