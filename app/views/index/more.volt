{% extends "layouts/base-frontpage.volt" %}
  {% block set_params %}
    {{ set_title("Knowledge Lab") }}
  {% endblock %}

  {% block content2 %}
    <div class="nds-fp-img">
      {#      <img src="/img/big-icons/fp-img.png">#}
    </div>
    <div class="nds-fp-text-container">
      <h1>
        DASH-IN - Read more
      </h1>

      <h3>
        DASH-IN datasharing application
      </h3>
      <p>
        The system provides a full infrastructure to register studies with all relevant information and enables the user to store data, control access to data, perform a free text queries on all meta- and accessible data, make inquires to data owners, combine data from accessible data and export of data.
        Datasets can be added to the study in the following way: Datsets must first be uploaded through the minimal management interface. When they are properly curated they can be added to a study
      </p>

      <p>
        The DASH-IN environment allows for free text search on all uploaded metadata, curation, selection and combination of variables and data as well as export of existing and combined data sets in a large number of commonly used formats{# (click here for more information on data formats)#}.
      </p>
      <hr>
      <p>
        <a href="/{{ _ls_querystring_ }}" class="nds-button nds-hollow">Back to front page</a>
      </p>
    </div>
    <div class="nds-fp-img">
      {#      <img src="/img/big-icons/fp-img.png">#}
    </div>
  {% endblock %}