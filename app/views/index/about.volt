{% extends "layouts/base-frontpage.volt" %}
  {% block set_params %}
    {{ set_title("Knowledge Lab") }}
  {% endblock %}

  {% block content2 %}
    <div class="nds-fp-img">
      <img src="/img/big-icons/fp-img.png">
    </div>
    <div class="nds-fp-text-container">
      <h1>
        About
      </h1>

      <p>
        The KdLab repository was founded 2018, with the purpose to create an user friendly tool where researchers can upload data with a minimum of preparation and a minimum of skills regarding data handling.
      </p>
      <hr>
      <h3>
        Primary goal
      </h3>
      <p>
        Ensure long term storage of data in a way that they are Findable,  Accessible Interoperable, Reusable (FAIR <a href="https://www.go-fair.org/fair-principles/">https://www.go-fair.org/fair-principles</a>). (Meta-)data must use vocabularies that follow the FAIR principles - The controlled vocabulary used to describe datasets needs to be documented and resolvable using a globally unique and persistent ontology.
      </p>
      <h3>
        Secondary goal
      </h3>
      <p>
        One major obstacle to uploading data is the technical issues of preparing complex data for general usage by people who are not familiar with the data. Therefore we see it as vitally important to make the process of uploading, curating and sharing data as simple and easy as possible, and to make full use of the knowledge that can be automatically extracted from data in order to help the data provider preparing the data.
      </p>
      <h3>
        We seek to solve three different but related types of problems
      </h3>
      <p>
        The first problem is to get research data under some form of minimal systematic management. The second problem is to store data long term together with the full context of the study that produced the data (including other datasets and metadata). The third problem is to allow for secure sharing of data for further analyses in colaboration with or independently by other researchers.
      </p>
      <p>
        <a href="/{{ _ls_querystring_ }}">Back to front page</a>
      </p>
    </div>
  {% endblock %}