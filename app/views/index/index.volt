{% extends "layouts/base-frontpage.volt" %}
  {% block set_params %}
    {{ set_title("Dashin") }}
  {% endblock %}

  {% block content2 %}
    <div class="" >
{#      <img src="/img/big-icons/frontpage-image-v2.png">#}
      <img src="/img/big-icons/blue-diamond.png" style="width: 400px;height: 300px">
    </div>

    <div class="nds-fp-text-container">
      <p style="font-size: 150%;margin-bottom: 0">
        Welcome to DASH-IN, a place to
      </p>

      <header style="font-size: 48pt;line-height: 56pt;margin-bottom: 0">
        Store and share<br>scientific data
      </header>

      <p style="font-size: 150%;margin-top: 0">
        A safe and secure place to store and create<br>accessible datasets and conduct research
      </p>
      <div style="display: flex;flex-flow: row nowrap;">
        <a href="/login/" class="nds-button nds-big">Explore</a>
        <div style="width: 2rem"></div>
        <a href="/more/" class="nds-button nds-hollow nds-big">Learn more</a>
      </p>

    </div>
  {% endblock %}