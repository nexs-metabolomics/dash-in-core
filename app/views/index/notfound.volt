<!DOCTYPE html>
<html>
  <head>
    {% block set_params %}{% endblock %}
    {% block head %}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
{#      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->#}
      {{ partial("00partials/global_favicon_partial") }}
      {#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/> #}
      <link rel="stylesheet" href="{{ url.getStaticCss('css/style.css') }}">
      <title>
        {{ get_title() }}
      </title>
    {% endblock %}
  </head>
  <body onload="default_focus()">
    <div class="page-container">

      <div class="frontpage-header">
        {{ partial("00partials/frontpage_header_partial") }}
      </div>

      <div id="scrollfocus" class="frontpage-content" style="outline: none">

        {# First row #}

        <div class="fp-row fp-r1">
          <div class="fp-col">

            <div class="fp-row-inner" style="justify-content: center;padding: 100px 0">
              <header style="display: flex;font-weight: bold;font-size: 200%">
                The page you are looking for could not be found
              </header>
            </div>
            <div class="fp-row-inner">
              <img src="/img/fp/people.png">
            </div>

          </div>
        </div>

      </div>
    </div>

    {% block jsscripts %}
      <script>
          function default_focus() {
              document.getElementById("scrollfocus").focus();
          }
      </script>
    {% endblock %}
  </body>
</html>
