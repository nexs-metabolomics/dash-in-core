<!DOCTYPE html>
<html>
<head>
  {% block set_params %}{% endblock %}
  {% block head %}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    {{ partial("00partials/global_favicon_partial") }}
    <link rel="stylesheet" href="{{ url.getStaticCss('css/v1/style-003.css') }}">
    <title>
      {{ get_title() }}
    </title>
  {% endblock %}
</head>
<body onload="default_focus()">
<div class="page-container">

  {# ==================================================================== #}
  {# page header #}
  {# ==================================================================== #}
  <div class="page-header">
    <div class="page-header-inner">
      <div class="header-title">
        {% block page_section_header %}
        {% endblock %}
      </div>
      {% if LOGGED_IN_USER_FULL_NAME is defined %}
        <div class="user-menu">
          <a class="user-link" href="/user/{{ _ls_querystring_ }}" onclick="" title="Profile">
            {{ partial("00img_partials/icons/account.svg") }}
          </a>
          <a class="logout-link" href="/logout/" onclick="" title="Logout">
            Logout
            {# {{ partial("00img_partials/icons/logout-variant.svg") }} #}
          </a>
        </div>
      {% endif %}
    </div>
  </div>

  {# ==================================================================== #}
  {# page menu #}
  {# ==================================================================== #}
  <div class="page-menu">

    {# ==================================================================== #}
    {# slider logic #}
    {# ==================================================================== #}

    {# ==================================================================== #}
    {# wide menu #}
    {# ==================================================================== #}
    <div class="page-menu-container menu-wide">

      <div class="site-logo-container">
        {{ partial("layouts/site_logo_wide_partial") }}
      </div>

      <div class="page-menu-inner">

        <nav>
          <ul class="module-header">
            {% block menu_wide_module_header %}
            {% endblock %}
          </ul>
        </nav>

        <nav>
          <ul class="module-submenus">
            {% block menu_wide_module_submenus %}
            {% endblock %}
          </ul>
        </nav>

        <div class="version-string">
          {{ VERSION_STRING }}
        </div>

      </div>

    </div>

    {# ==================================================================== #}
    {# narrow menu #}
    {# ==================================================================== #}
    <div class="page-menu-container menu-narrow">

      <div class="site-logo-container">
        {{ partial("layouts/site_logo_narrow_partial") }}
      </div>

      <div class="page-menu-inner">
        <nav>
          {{ partial("layouts/site_menu_narrow_partial") }}
        </nav>

        <hr class="menu-sep">

        <nav>
          <ul class="module-header">
            {% block menu_narrow_module_header %}
            {% endblock %}
          </ul>
        </nav>

        <hr class="menu-sep">

        <nav>
          {% block menu_narrow_module_submenus %}
          {% endblock %}
        </nav>

        <div class="version-string">
          {{ VERSION_STRING }}
        </div>

      </div>

    </div>

  </div>
  {# === END page menu #}

  {# ==================================================================== #}
  {# page subheader nav #}
  {# ==================================================================== #}
  <div class="page-subheader-nav">
    <div class="subheader-nav-inner">

      <div class="subheader-nav-content">
        {% block subheader_nav %}
        {% endblock %}
      </div>
    </div>
  </div>

  <div class="page-sub-k-nav">
    {% block subheader_k_nav %}
    {% endblock %}
  </div>

  {# ==================================================================== #}
  {# message row #}
  {# ==================================================================== #}
  <div class="page-subheader-msg">
    <div class="subheader-msg-inner">
      <div class="subheader-msg-content">
        {{ partial("layouts/base-message_handler_partial") }}
      </div>
    </div>
  </div>
  <div class="page-sub-k-msg">
    {% block subheader_k_msg %}
    {% endblock %}
  </div>

  {# ==================================================================== #}
  {# action row #}
  {# ==================================================================== #}
  <div class="page-content-k" style="justify-content: flex-start">
    <div class="subheader-action-inner">
      <div class="subheader-action-content" style="flex-flow: column">
        {% block subheader_action %}
        {% endblock %}
      </div>
    </div>
  </div>
  <div class="page-sub-k-action">
    {% block subheader_k_action %}
    {% endblock %}
  </div>

  {# ==================================================================== #}
  {# info row #}
  {# ==================================================================== #}
  <div class="page-subheader-info">
    <div class="subheader-info-inner">
      <div class="subheader-info-content">
        {% block subheader_info %}
        {% endblock %}
      </div>

    </div>
  </div>
  <div class="page-sub-k-info">
    {% block subheader_k_info %}
    {% endblock %}
  </div>
  {# === END page subheader #}

  {# ==================================================================== #}
  {# page content #}
  {# ==================================================================== #}
  <div class="page-content">
    <div class="page-content-inner" id="pci">
      {% block page_content %}
      {% endblock %}
    </div>
  </div>
{#  <div class="page-content-k">#}
{#    <div class="page-content-k-inner">#}
{#      {% block page_content_k %}#}
{#      {% endblock %}#}
{#    </div>#}
{#  </div>#}

  {# ==================================================================== #}
  {# page subfooter #}
  {# ==================================================================== #}
  <div class="page-subfooter">
    <div class="subfooter-inner">
      <div class="table-nav-container">
        {% block subfooter_content %}
        {% endblock %}
      </div>
    </div>
  </div>
  <div class="page-subfooter-k">
    {% block subfooter_content_k %}
    {% endblock %}
  </div>

  {# ==================================================================== #}
  {# page footer #}
  {# ==================================================================== #}
  <div class="page-footer">
    {% block page_footer_content %}
    {% endblock %}
  </div>

</div>
{% block jsscripts %}
  <script>
      function default_focus() {
          document.getElementById("pci").focus();
      }

      var hasChanged = false;
      var isSubmit = false;

      var formSetSubmit = function () {
          isSubmit = true;
      }

      function formSetChanged() {
          hasChanged = true;
      }

      window.onbeforeunload = function () {
          if (hasChanged && !isSubmit) {
              return "";
          }
      }
  </script>
{% endblock %}
</body>
