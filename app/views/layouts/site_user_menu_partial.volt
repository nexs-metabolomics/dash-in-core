<ul>
  {% if LOGGED_IN_USER_FULL_NAME is defined %}
    <li class="user-link">
      <a class="nds-menu-entry" href="/user/{{ _ls_querystring_ }}" onclick="" title="Edit profile">
        {{ partial("00img_partials/icons/account.svg") }}
        {{ LOGGED_IN_USER_FULL_NAME }}
      </a>
    </li>
    <li class="user-link">
      <a class="nds-menu-entry" href="/logout/" onclick="" title="Logout">
        {{ partial("00img_partials/icons/logout-variant.svg") }}
        logout
      </a>
    </li>
    <li class="user-link">
      <a class="nds-menu-entry" href="/user/view/{{ _ls_querystring_ }}" onclick="" title="Select role">
        {{ partial("00img_partials/icons/account-convert.svg") }}
        {% if SU.getActiveManagerOrgName() %}{{ SU.getActiveManagerOrgName() }}{% endif %}
      </a>
    </li>
  {% else %}
    <li class="user-link">
      <a href="/user/" onclick="">
        User
      </a>
    </li>
  {% endif %}

</ul>
