{% include "00macros/00-include-macros.volt" %}
<!DOCTYPE html>
<html>
  <head>
    {% block set_params %}{% endblock %}
    {% block head %}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      {{ partial("00partials/global_favicon_partial") }}
      {#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/> #}
      <link rel="stylesheet" href="{{ url.getStaticCss('css/v1/public.css') }}">
      <title>
        {{ get_title() }}
      </title>
    {% endblock %}
    <style>
      .public-enter
      {
        padding: 16px;
      }

      .public-enter:hover
      {
        background-color: rgba(255, 0, 140, 0.13);
        transition: background-color 0.4s;
        border-radius: 16px;
      }
    </style>
  </head>
  <body onload="default_focus()">
    <div class="page-container">

      {# ==================================================================== #}
      {# page header #}
      {# ==================================================================== #}
      <div class="page-header">

        {# logo link #}
        <div class="page-header-inner">
          <a href="/" style="display: flex;flex-flow: row">
            <div class="site-logo-container">
              {{ partial("00img_partials/v1/icons/squidr-logo-text-neutral.svg") }}
            </div>
          </a>
{#          <a href="/public/index" style="display: flex;flex-flow: row;margin-left: 32px">#}
{#            <div style="display: flex;flex-flow: column;align-items: center;justify-content: center;font-weight: bold;font-size: 150%">#}
{#              Browse studies#}
{#            </div>#}
{#          </a>#}
        </div>

      </div>
      
      <div class="page-subheader-nav">
        
      </div>

      {# ==================================================================== #}
      {# page content #}
      {# ==================================================================== #}
      <div class="page-content">
        <div class="page-content-inner" id="pci">
          {% block page_content %}
          {% endblock %}
        </div>
      </div>
      {#      <a href="/public/page2/" class="button" style="border: #848484 solid 1px;background-color: #c9c9c9"><span style="font-size: 200%; font-weight: bold">Search here!</span></a> #}

    </div>
  </body>