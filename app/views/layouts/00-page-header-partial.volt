{#      <div class="page-header">#}
{#        <div class="page-header-inner">#}
{#          <div class="header-title">#}
{#            {% block page_section_header %}#}
{#            {% endblock %}#}
{#          </div>#}
{#          {% if LOGGED_IN_USER_FULL_NAME is defined %}#}
            <div style="display: flex;flex-flow: row;padding: 0 16px 0 0;align-items: center">
              <div style="display: flex;flex-flow: row;white-space: nowrap;padding: 4px 16px;background-color: #eaeaea;margin-right: 4px" title="Organization name">
              {{ ACTIVE_MANAGER_ORG_NAME }}
              </div>
              <div style="display: flex;flex-flow: row;white-space: nowrap;padding: 4px 16px;background-color: #eaeaea" title="User name">
              {{ LOGGED_IN_USER_FULL_NAME }}
              </div>
            </div>
            <div class="user-menu">
              <a class="user-link" href="/user/{{ _ls_querystring_ }}" onclick="" title="Manage profile">
                {{ partial("00img_partials/icons/account.svg") }}
              </a>
              <a class="logout-link" href="/logout/" onclick="" title="Logout">
                Logout
              </a>
            </div>
{#          {% endif %}#}
{#        </div>#}
{#      </div>#}
