{% if flashMessage.hasMessage() %}
  <div class="subheader-msg-column" style="border:none">
    {% for type in ["error","warning","notice","success"] %}
      {% if flashMessage.has(type) %}
        {% for msg in flashMessage.getMessages(type) %}
          <div class="subheader-msg-row {{ type }}">
            <div class="smr-message-header">
              {{ type|capitalize }}
            </div>
            <div class="smr-message-text">
              {{ msg }}
            </div>
          </div>
        {% endfor %}
      {% endif %}
    {% endfor %}
  </div>
{#  <div class="subheader-msg-column" style="width: unset;min-height: 100%">#}
{#    <div style="display: flex;flex-flow: column;height: 100%;justify-content: center">#}
{#      <button form="form1" name="btn[msgbtn]" type="submit" class="button" style="background-color: transparent;border:none" aria-label="Close banner">#}
{#        <svg class="msg-button" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img">#}
{#          <g transform="rotate(-45 50 50)">#}
{#            <rect height="121.7" rx="6.76" width="13.52" x="43.24" y="-10.85"></rect>#}
{#            <rect height="13.52" rx="6.76" width="121.7" x="-10.85" y="43.24"></rect>#}
{#          </g>#}
{#        </svg>#}
{#      </button>#}
{#    </div>#}
{#  </div>#}
{% endif %}
