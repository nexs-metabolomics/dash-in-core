<!DOCTYPE html>
<html>
<head>
  {% block set_params %}{% endblock %}
  {% block head %}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    {{ partial("00partials/global_favicon_partial") }}
{#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/>#}
    <link rel="stylesheet" href="{{ url.getStaticCss('css/login.css') }}">
    {#{{ url.getStaticCss('css/00master.css') }}#}
    {#<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">#}
    <title>
      {{ get_title() }}
    </title>
  {% endblock %}
</head>
{% if extra_page_container_class is not defined %}{% set extra_page_container_class = "" %}{% endif %}
<body>
{% block show_errors %}
  {% if flashSession.has() %}
    <div class="nds-error-container">
      <div class="message-header">
        {{ flashSession.output() }}
      </div>
    </div>
  {% endif %}
{% endblock %}
<div class="nds-page-container {{ extra_page_container_class }}">
  <div class="nds-header">
    {% block header %}
      {{ partial("00partials/00_nds_main_header_partial") }}
    {% endblock %}
  </div>

  <div class="nds-menu">
    {% block menu_start %}
    {#<input type="checkbox" id="nds-chk-slide-menu">#}
    <a href="#" class="nds-chk-slide-menu-label"></a>
    <div class="nds-nav-container">
      <nav>

        <ul>
          <li class="user-link">
            <a href="/user/{{ _ls_querystring_ }}" onclick="">
              {% if LOGGED_IN_USER_FULL_NAME is defined %}{{ LOGGED_IN_USER_FULL_NAME }}{% else %}User{% endif %}
            </a>
          </li>
        </ul>
    {% endblock %}
    {% block menu1 %}{% endblock %}
    {% block menu_separator %}
      <hr class="nds-menu-sep">
    {% endblock %}
    {% block menu2 %}{% endblock %}
    {% block menu_end %}
      </nav>
    </div>
    {% endblock %}
  </div>

  <div class="nds-header2">
    {% block header2 %}{% endblock %}
  </div>

  <div class="nds-content">
    {% block content %}{% endblock %}
  </div>

  <div class="nds-content2">
    {% block content2 %}{% endblock %}
  </div>

  <div class="nds-footer">
    {% block footer %}{% endblock %}
  </div>
</div>
</body>
</html>
