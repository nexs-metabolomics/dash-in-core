<!DOCTYPE html>
<html>
<head>
  {% block set_params %}{% endblock %}
  {% block head %}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    {{ partial("00partials/global_favicon_partial") }}
{#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/>#}
    <link rel="stylesheet" href="{{ url.getStaticCss('css/00qelements.css') }}">
    <link rel="stylesheet" href="{{ url.getStaticCss('css/00master2.css') }}">
    <title>
      {{ get_title() }}
    </title>
  {% endblock %}
</head>
{% if extra_page_container_class is not defined %}{% set extra_page_container_class = "" %}{% endif %}
<body onload="default_focus()">

{% block show_errors %}
  {% if flashSession.has() %}
    <div class="nds-error-container">
      <div class="message-header">
        {{ flashSession.output() }}
      </div>
    </div>
  {% endif %}
{% endblock %}

<div class="nds-page-container {{ extra_page_container_class }}">

  <div class="nds-header">

    <div class="nds-main-header-left">
      {% block header_site_logo %}
        <a class="nds-site-logo-link" href="/">
          <img id="a" src="/img/kdlab-logo.png">
        </a>
      {% endblock %}

      <div class="nds-other-links">
        {% block header_links %}
          {# <a href="/about/{{ _ls_querystring_ }}">About</a> #}
        {% endblock %}
      </div>
    </div>

    {% block header_dropdown_menu %}
      <div class="nds-dropdown-menu-container">
        {% if IS_LOGGED_IN is defined and IS_LOGGED_IN %}
          {{ partial("00partials/00_nds_main_header_user_menu_partial") }}
        {% else %}
          <a href="/login/" onclick="">
            <div class="user-login">
              Login
            </div>
          </a>
        {% endif %}
      </div>
    {% endblock %}

  </div>

  <div class="nds-menu">

    <label for="chk-slide" class="nds-slide-menu-activator-label"></label>
    <input type="checkbox" id="chk-slide" class="nds-slide-menu-activator">

    {% block menu_big_start %}
    <div class="nds-nav-container" style="padding-top: 3rem">

      <div style="background-color: #545454">
        <a class="nds-site-logo-link" href="/">
          {# <img src="/img/kdlab-logo.png"> #}
        </a>
      </div>

      <nav>

        <ul>
          {% if LOGGED_IN_USER_FULL_NAME is defined %}
            <li class="user-link">
              <a href="/user/{{ _ls_querystring_ }}" onclick="" title="Edit profile">
                {{ LOGGED_IN_USER_FULL_NAME }}
              </a>
            </li>
            <li class="user-link">
              <a href="/logout/{{ _ls_querystring_ }}" onclick="" title="Logout">
                logout
              </a>
            </li>
            <li class="user-link" style="min-height: 2rem">
              <a href="/user/view/{{ _ls_querystring_ }}" onclick="" title="Select role">
                {% if SU.getActiveManagerOrgName() %}({{ SU.getActiveManagerOrgName() }}){% endif %}
              </a>
            </li>
          {% else %}
            <li class="user-link">
              <a href="/user/{{ _ls_querystring_ }}" onclick="">
                User
              </a>
            </li>
          {% endif %}
          <li>
            <a href="/welcome/{{ _ls_querystring_ }}">Tools</a>
          </li>

        </ul>
        <hr class="nds-menu-sep">
        {% endblock %}

        <ul>
          {% block menu_wide_1 %}
          {% endblock %}
        </ul>
        <hr class="nds-menu-sep">

        {% block menu_big_separator %}
        {% endblock %}

        {% block menu_wide_2 %}
        {% endblock %}

      </nav>
    </div>

    {% block menu_narrow_start %}
    <div class="nds-nav-container2">

      <div style="background-color: #545454">
        <a class="nds-site-logo-link" href="/">
          <img id="d" src="/img/kdlab-logo.png">
        </a>
      </div>

      <nav>

        <ul>
          {% if LOGGED_IN_USER_FULL_NAME is defined %}
            <li class="user-link">
              <a href="/user/{{ _ls_querystring_ }}" onclick="" title="Edit profile">
                {{ LOGGED_IN_USER_FULL_NAME }}
              </a>
            </li>
            <li class="user-link" style="min-height: 2rem">
              <a href="/user/view/{{ _ls_querystring_ }}" onclick="" title="Select role">
                {% if SU.getActiveManagerOrgName() %}({{ SU.getActiveManagerOrgName() }}){% endif %}
              </a>
            </li>
          {% else %}
            <li class="user-link">
              <a href="/user/{{ _ls_querystring_ }}" onclick="">
                User
              </a>
            </li>
          {% endif %}
          <li>
            <a href="/welcome/{{ _ls_querystring_ }}">Tools</a>
          </li>

        </ul>
        <hr class="nds-menu-sep">
        {% endblock %}

        {% block menu_narrow_1 %}
        {% endblock %}

        {% block menu_narrow_separator %}
        {% endblock %}

        {% block menu_narrow_2 %}
        {% endblock %}

      </nav>
    </div>

  </div>

  <div id="ct2" class="nds-content2" tabindex="1">


    <div class="nds-content2-inner">
      {% block titleheader %}
        <header>
          {{ get_title(false) }}
        </header>
      {% endblock %}

      <nav class="nds-form-container">
        {% block tab_menu %}
        {% endblock %}
      </nav>

      <div class="nds-form-container">
        <div class="nds-tabs-row">
          {% block toolbar_1 %}
          {% endblock %}
        </div>
      </div>

      {% block content2 %}
      {% endblock %}
    </div>

  </div>

  <div class="nds-footer">
    {% block footer %}
    {% endblock %}
  </div>
</div>
{% block jsscripts %}
  <script>
      function default_focus() {
          document.getElementById("ct2").focus();
      }

      var hasChanged = false;
      var isSubmit = false;

      var formSetSubmit = function () {
          isSubmit = true;
      }

      function formSetChanged() {
          hasChanged = true;
      }

      window.onbeforeunload = function () {
          if (hasChanged && !isSubmit) {
              return "";
          }
      }
  </script>
{% endblock %}
</body>
</html>
