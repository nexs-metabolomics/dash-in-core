{% include "00macros/00-include-macros.volt" %}
<!DOCTYPE html>
<html>
  <head>
    {% block set_params %}{% endblock %}
    {% block head %}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      {{ partial("00partials/global_favicon_partial") }}
      <link rel="stylesheet" href="{{ url.getStaticCss('css/v1/style-008.css') }}">
      <title>
        {{ get_title() }}
      </title>
    {% endblock %}
  </head>
  <body onload="default_focus()">
    <div class="page-container">

      {# ==================================================================== #}
      {# page header #}
      {# ==================================================================== #}
      <div class="page-header-l">
      </div>

      <div class="page-header">
        <div class="page-header-inner">
      
          <div class="page-header-content">
            <div class="importguide-page-title">
              {{ get_title(false) }}
            </div>
          </div>
            
        </div>
      </div>

      <div class="page-header-r">
      </div>

      {# ==================================================================== #}
      {# page subheader nav #}
      {# ==================================================================== #}
      <div class="page-sub-l-nav">
        {% block subheader_l_nav %}
        {% endblock %}
      </div>

      <div class="page-subheader-nav">
        <div class="subheader-nav-inner">

          <div class="subheader-nav-content">
            {% block subheader_nav %}
            {% endblock %}
          </div>
        </div>
      </div>

      <div class="page-sub-r-nav">
        {% block subheader_r_nav %}
        {% endblock %}
      </div>

      {# ==================================================================== #}
      {# message row #}
      {# ==================================================================== #}
      <div class="page-sub-l-msg">
        {% block subheader_l_msg %}
        {% endblock %}
      </div>

      <div class="page-subheader-msg">
        <div class="subheader-msg-inner">
          <div class="subheader-msg-content">
            {{ partial("layouts/base-message_handler_partial") }}
          </div>
        </div>
      </div>

      <div class="page-sub-r-msg">
        {% block subheader_r_msg %}
        {% endblock %}
      </div>

      {# ==================================================================== #}
      {# action row #}
      {# ==================================================================== #}
      <div class="page-sub-l-action">
        {% block subheader_l_action %}
        {% endblock %}
      </div>

      <div class="page-subheader-action" style="justify-content: flex-start">
        <div class="subheader-action-inner">
          <div class="subheader-action-content" style="flex-flow: row;justify-content: space-between;width: 100%;padding: 0 8px">
            {% block subheader_action %}
            {% endblock %}
          </div>
        </div>
      </div>

      <div class="page-sub-r-action">
        {% block subheader_r_action %}
        {% endblock %}
      </div>

      {# ==================================================================== #}
      {# info row #}
      {# ==================================================================== #}
      <div class="page-sub-l-info">
        {% block subheader_l_info %}
        {% endblock %}
      </div>

      <div class="page-subheader-info">
        <div class="subheader-info-inner">
          <div class="subheader-info-content">
            {% block subheader_info %}
            {% endblock %}
          </div>

        </div>
      </div>

      <div class="page-sub-r-info">
        {% block subheader_r_info %}
        {% endblock %}
      </div>
      {# === END page subheader #}

      {# ==================================================================== #}
      {# page content #}
      {# ==================================================================== #}
      <div class="page-content-l">
        <div class="page-content-l-inner">
          {% block page_content_l %}
          {% endblock %}
        </div>
      </div>

      <div class="page-content">
        <div class="page-content-inner" id="pci">
          {% block page_content %}
          {% endblock %}
        </div>
      </div>

      <div class="page-content-r">
        <div class="page-content-r-inner">
          {% block page_content_r %}
          {% endblock %}
        </div>
      </div>

      {# ==================================================================== #}
      {# page subfooter #}
      {# ==================================================================== #}
      <div class="page-subfooter-l">
        {% block page_subfooter_l %}
        {% endblock %}
      </div>

      <div class="page-subfooter">
        <div class="subfooter-inner">
          <div class="table-nav-container">
            {% block page_subfooter %}
            {% endblock %}
          </div>
        </div>
      </div>

      <div class="page-subfooter-r">
        {% block page_subfooter_r %}
        {% endblock %}
      </div>

      {# ==================================================================== #}
      {# page footer #}
      {# ==================================================================== #}
      <div class="page-footer">
        {% block page_footer %}
        {% endblock %}
      </div>

    </div>
    {% block jsscripts %}
      <script>
          function default_focus() {
              document.getElementById("pci").focus();
          }

          var hasChanged = false;
          var isSubmit = false;

          var formSetSubmit = function () {
              isSubmit = true;
          }

          function formSetChanged() {
              hasChanged = true;
          }

          window.onbeforeunload = function () {
              if (hasChanged && !isSubmit) {
                  return "";
              }
          }
      </script>
    {% endblock %}
  </body>
