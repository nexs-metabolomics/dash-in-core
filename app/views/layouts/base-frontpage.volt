<!DOCTYPE html>
<html>
  <head>
    {% block set_params %}{% endblock %}
    {% block head %}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      {{ partial("00partials/global_favicon_partial") }}
      {#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/> #}
      <link rel="stylesheet" href="{{ url.getStaticCss('css/v1/style-001.css') }}">
      <title>
        {{ get_title() }}
      </title>
    {% endblock %}
    <style>
      .public-enter
      {
        padding: 16px;
      }

      .public-enter:hover
      {
        background-color: rgba(255, 0, 140, 0.13);
        transition: background-color 0.4s;
        border-radius: 16px;
      }
    </style>
  </head>
  <body onload="default_focus()">
    <div class="page-container frontpage">

      <div class="frontpage-header">
        {{ partial("00partials/frontpage_header_partial") }}
      </div>

      <div id="scrollfocus" class="frontpage-content" style="outline: none">

        {# First row #}

        <div class="fp-row fp-r1">
          <div class="fp-row-inner">

            <div class="fp-col">

              <div class="fp-r1c1">

                <div class="fp-main-text">

                  <a href="/public/index/" class="public-enter">
                    <p class="head-text">
                      Open<br>
                      Science<br>
                      Repository
                    </p>
                    <p class="body-text">
                      Squidr is a time saving tool to effortlessly sharing and find scientific data.
                    </p>
                  </a>
                </div>

                <div class="fp-main-enter">
                  <a href="/public/index/">
                    Enter
                  </a>
                </div>

              </div>
            </div>

            <div class="fp-col">
              <div class="fp-r1c2">
                <div class="fp-main-img">
                  <img src="/img/fp/blue-diamond.png">
                </div>
              </div>

            </div>

          </div>
        </div>

        {# Second row #}

        <div class="fp-row" style="padding: 0 25%">
          <div class="fp-col">
            <img src="/img/fp/modules.png">
          </div>
        </div>

        <div class="fp-row" style="padding: 2% 25% 0 25%">
          <div class="fp-col">
            <img src="/img/fp/people.png">
          </div>
        </div>

        <div class="fp-row" style="padding: 2% 25% 0 25%">
          <div class="fp-col">
            <img src="/img/fp/floatchart-and-text.png">
          </div>
        </div>

        <div class="fp-row" style="padding: 2% 25% 0 25%">
          <div class="fp-col">
            <img src="/img/fp/datacuration.png">
          </div>
        </div>
        <div class="fp-row" style="background-color: black;margin-top: 40px;padding: 2% 25% 0 25%">
          <div class="fp-col">
            <img src="/img/fp/funding-white.png">
          </div>
        </div>
        <div class="fp-row" style="padding: 2% 25% 0 25%">
          <div class="fp-col">
            <img src="/img/fp/whodunnit.png">
          </div>
        </div>
        <hr style="border: #cdcdcd solid 1px; width: 100%">

        <div class="fp-row" style="padding: 2% 25% 0 25%;margin-bottom: 50px">
          <div class="fp-col" style="height: 75%;flex-flow:column;">
            <p style="display: flex">
              {{ partial("00img_partials/v1/icons/squidr-logo-text-black.svg") }}
            </p>
            <p style="display: flex;font-size: 250%">
              Contact:
            </p>
          </div>
        </div>

      </div>
    </div>

    {% block jsscripts %}
      <script>
          function default_focus() {
              document.getElementById("scrollfocus").focus();
          }
      </script>
    {% endblock %}
  </body>
</html>
