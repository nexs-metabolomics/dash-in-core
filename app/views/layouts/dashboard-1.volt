<!DOCTYPE html>
<html>
<head>
  {% block set_params %}{% endblock %}
  {% block head %}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    {{ partial("00partials/global_favicon_partial") }}
{#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/>#}
    <link rel="stylesheet" href="{{ url.getStaticCss('css/00master2.css') }}">
    <title>
      {{ get_title() }}
    </title>
  {% endblock %}
</head>
{% if extra_page_container_class is not defined %}{% set extra_page_container_class = "" %}{% endif %}
<body>
{% block show_errors %}
  {% if flashSession.has() %}
    <div class="nds-error-container">
      <div class="message-header">
        {{ flashSession.output() }}
      </div>
    </div>
  {% endif %}
{% endblock %}
<div class="nds-page-container {{ extra_page_container_class }}">
  <div class="nds-header">

    <div class="nds-main-header-left">
      {% block header_site_logo %}
        <a class="nds-site-logo-link" href="/">
          <img id="e" src="/img/kdlab-logo.png">
        </a>
      {% endblock %}
      <div class="nds-other-links">
        {% block header_links %}
          {#<a href="/about/">About</a>#}
        {% endblock %}
      </div>
    </div>
    {% block header_dropdown_menu %}
      <div class="nds-dropdown-menu-container">
        {% if IS_LOGGED_IN is defined and IS_LOGGED_IN %}
          {{ partial("00partials/00_nds_main_header_user_menu_partial") }}
        {% else %}
          <a href="/login/" onclick="">
            <div class="user-login">
              Login
            </div>
          </a>
        {% endif %}
      </div>
    {% endblock %}
  </div>

  <div class="nds-menu">
    {#<input type="checkbox" id="nds-chk-slide-menu">#}
    {#    <a href="#" class="nds-slide-menu-activator"></a>#}

    <label for="chk-slide" class="nds-slide-menu-activator-label"></label>
    <input type="checkbox" id="chk-slide" class="nds-slide-menu-activator">

    {% block menu_start %}
    <div class="nds-nav-container">
      <nav>

        <ul>
          {% if LOGGED_IN_USER_FULL_NAME is defined %}
            <li class="user-link">
              <a href="/user/{{ localsession.getQuerystring('?') }}" onclick="" title="Edit profile">
                {{ LOGGED_IN_USER_FULL_NAME }}
              </a>
            </li>
            <li class="user-link" style="min-height: 2rem">
              <a href="/user/view/{{ localsession.getQuerystring('?') }}" onclick="" title="Select role">
                {% if SU.getActiveManagerOrgName() %}({{ SU.getActiveManagerOrgName() }}){% endif %}
              </a>
            </li>
          {% else %}
            <li class="user-link">
              <a href="/user/{{ localsession.getQuerystring('?') }}" onclick="">
                User
              </a>
            </li>
          {% endif %}
        <hr>
        </ul>
        {% endblock %}
        <ul>
          <li>
            <a href="/">Front page</a>
          </li>
        {% block menu1 %}{% endblock %}
        </ul>
        {% block menu_separator %}
          <hr class="nds-menu-sep">
        {% endblock %}
        {% block menu2 %}{% endblock %}
        {% block menu_end %}
      </nav>
    </div>
    {% endblock %}
  </div>

  <div class="nds-content2">
    {% block content2 %}{% endblock %}
  </div>

  <div class="nds-footer">
    {% block footer %}{% endblock %}
  </div>
</div>
<script>
    function default_focus() {
        document.getElementById("ct2").focus();
    }

    var hasChanged = false;
    var isSubmit = false;

    var formSetSubmit = function () {
        isSubmit = true;
    }

    function formSetChanged() {
        hasChanged = true;
    }

    window.onbeforeunload = function () {
        if (hasChanged && !isSubmit) {
            return "";
        }
    }
</script>
</body>
</html>
