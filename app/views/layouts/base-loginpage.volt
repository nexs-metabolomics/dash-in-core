{% include "00macros/00-include-macros.volt" %}
<!DOCTYPE html>
<html>
  <head>
    {% block set_params %}{% endblock %}
    {% block head %}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      {{ partial("00partials/global_favicon_partial") }}
      {#    <link rel="shortcut icon" type="image/png" href="/img/logo/das-in-favicon2.png"/> #}
      <link rel="stylesheet" href="{{ url.getStaticCss('css/v1/style-007.css') }}">
      {# {{ url.getStaticCss('css/00master.css') }} #}
      {# <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> #}
      <title>
        {{ get_title() }}
      </title>
    {% endblock %}
  </head>
  {% if extra_page_container_class is not defined %}{% set extra_page_container_class = "" %}{% endif %}
  <body>
    <div class="page-container login">

      <div class="page-subheader-msg">
        <div class="subheader-msg-inner">
          <div class="subheader-msg-content">
            {{ partial("layouts/base-message_handler_partial") }}
          </div>
        </div>
      </div>

      <div class="page-content">
        <div class="page-content-inner">
          {% block page_content %}
          {% endblock %}
        </div>
      </div>

    </div>
  </body>
</html>
