<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\ApplicationBase;

use App\Library\ApplicationRoot\ControllerRoot;

class ApplicationControllerBase extends ControllerRoot
{
    protected $_viewBasePath;

    public function initialize()
    {
        $versionString = preg_filter('#[^0-9.]#', "", @file_get_contents("../.version"));
        $this->view->VERSION_STRING = $versionString ? "v $versionString" : "";
    }

    public function beforeExecuteRoute()
    {
        $this->_pickView($this->_viewBasePath);
    }

    public function afterExecuteRoute()
    {
        $this->view->setVar("_ls_querystring_", $this->localsession->getQuerystring("?"));
    }

    protected function _setView($namespace)
    {
        if (!$this->router->wasMatched()) {
            $this->view->setMainView("/index/notfound");
        } else {
            # use custom params instead of namespace + controller
            $params = $this->dispatcher->getParams();
            if (isset($params["view_base"])) {
                $viewBase = $params["view_base"];
            } else {
                $crontrollerName = $this->dispatcher->getControllerName();
                $viewBase = "$namespace/$crontrollerName";
            }
            $viewBase = $this->_decamelize(trim($viewBase, "/"));
            $action = $this->_decamelize($this->dispatcher->getActionName());
            $path = "$viewBase/$action";

            $this->view->setMainView($path);
            $this->view->disableLevel([0, 1, 2, 3, 4]);
        }
    }

    /**
     * Pick custom view
     * or pick snake_cased view (namespace/controller/action)
     *
     * @param string|null $namespaceName
     * @param string|null $controllerName
     * @param string|null $actoinName
     */
    public function _pickView($namespaceName = null, $controllerName = null, $actoinName = null)
    {
        if (!$this->router->wasMatched()) {
            $this->view->setMainView("index/notfound");
        } elseif ($this->dispatcher->getParam("view_base")) {
            $this->_setView($namespaceName);
        } else {
            # namespace
            $x = $this->dispatcher->getNamespaceName();
            $nname = (null !== $namespaceName) ? $namespaceName : basename($this->_decamelize($x));
            $cname = (null !== $controllerName) ? $controllerName : $this->_decamelize($this->dispatcher->getControllerName(), false);
            $aname = $this->dispatcher->getActionName();
            $aname = (null !== $actoinName) ? $actoinName : $this->_decamelize($aname, false);
            $path = "$nname/$cname/$aname";
            $this->view->setMainView($path);
        }
    }

    /**
     * Return a snake_cased string string
     *
     * @param           $input
     * @param bool|true $handleBackslash
     *
     * @return string
     */
    protected function _decamelize($input, $handleBackslash = true)
    {
        if ($handleBackslash && false !== strpos($input, "\\")) {
            $input = str_replace("\\", "/", $input);
        }
        return mb_strtolower(preg_replace('/([a-z0-9])([A-Z])/', '$1_$2', $input));
    }
    
    public function toggleSubmenu()
    {
        # toggle main submenu on / off
        $showSubmenu = $this->dispatcher->getParam(0);
        if(isset($showSubmenu) && $showSubmenu === "1") {
            $this->view->show_submenu = 0;
        } else {
            $this->view->show_submenu = 1;
        }
    }
}