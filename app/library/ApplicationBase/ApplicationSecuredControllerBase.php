<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-28
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\ApplicationBase;

class ApplicationSecuredControllerBase extends ApplicationControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute()
    {
        if (!$this->_isLoggedIn()) {
            return false;
        }
        if (!$this->_managerIsAllowed()) {
            return false;
        }

        parent::beforeExecuteRoute();
    }


    /**
     * Checks if the user logged in
     *
     * @return bool
     */
    protected function _isLoggedIn()
    {
        if (!$this->SU->securedLogggedIn()) {
            $this->flashSession->error("You need to login");
            $this->response->redirect("/login/")->send();
            return false;
        }
        $this->view->IS_LOGGED_IN = true;

        $this->view->LOGGED_IN_USER_FULL_NAME = $this->SU->getUserFullName();
        $this->view->ACTIVE_MANAGER_ORG_NAME = $this->SU->getActiveManagerOrgName();
        return true;
    }

    /**
     * Checks if the current manager is allowed to perform
     * the current action
     *
     * @return bool
     */
    protected function _managerIsAllowed()
    {

        if (!$this->SU->isAllowed()) {
//            $this->flashSession->warning("Current manager not allowed");
//            return true;
            
            $this->flashSession->warning("Your current manager role does not have permissions to access that page, please select another role.");
            $this->response->redirect("/user/view/");
            $this->response->send();
            return false;
        }
        return true;
    }
}
