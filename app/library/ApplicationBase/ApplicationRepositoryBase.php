<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-11-11
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace App\Library\ApplicationBase;

use App\Library\Utils\Json;
use Phalcon\Db\Enum;

/**
 * Class ApplicationRepositoryBase
 * @package App\Library\ApplicationBas
 */
class ApplicationRepositoryBase extends ApplicationComponentBase
{
    protected function _preparePagination2(&$db,$countQuery, $countParams, $itemQuery, $itemParams, $currentPos, $limit)
    {
        try {
            #---------------------------------------------------------
            # count number of entries
            #---------------------------------------------------------
            $count = $db->fetchOne($countQuery,Enum::FETCH_NUM,$countParams);
            if(!(isset($count[0])) && $count[0]) {
                return false;
            }
            $totalItems = $count[0];

            #---------------------------------------------------------
            # offset >=0
            #---------------------------------------------------------
            $currentPos = (int)$currentPos;
            if($currentPos < 1) {
                $currentPos = 1;
            } elseif ($currentPos > $totalItems) {
                $currentPos = $totalItems;
            }

            #---------------------------------------------------------
            # number of entries pr chunk >=1
            #---------------------------------------------------------
            $limit = (int)$limit;
            $limit = ($limit < 1) ? 1 : $limit;

            #---------------------------------------------------------
            # total number of chunks
            #---------------------------------------------------------
            $lastChunkEntries = $totalItems % $limit;
            $totalChunks = intdiv(floatval($totalItems), floatval($limit));
            $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;
            
            if ($currentPos > $totalChunks) {
                $currentPos = $totalChunks;
            }

            # before
            $before = ($currentPos > 1) ? $currentPos - 1 : 1;

            # next
            $next = ($currentPos < $totalChunks) ? $currentPos + 1: $totalChunks;

            # offset
            $offset = ($currentPos - 1) * $limit;
            if($offset>=($totalItems) && $totalItems>0) {
                $offset = $totalItems-1;
            } elseif ($offset<0 || $totalItems<=0) {
                $offset = 0;
            }
            # first and last item on page (eg: row a to b of n)
            $pageFirst = $offset+1;
            $pageLast = $offset + $limit;
            if($pageLast > $totalItems) {
                $pageLast = $totalItems;
            }

            $itemParams["limit"] = $limit;
            $itemParams["offset"] = $offset;

            $items = $db->fetchAll($itemQuery,Enum::FETCH_OBJ,$itemParams);

            return (object)[
                "items" => $items,
                "offset" => $offset,
                "first" => 1,
                "before" => $before,
                "previous" => $before,
                "current" => $currentPos,
                "last" => $totalChunks,
                "next" => $next,
                "total_pages" => $totalChunks,
                "total_items" => $totalItems,
                "limit" => $limit,
                "page_item_first" => $pageFirst,
                "page_item_last" => $pageLast,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    protected function _preparePaginationSpecial(&$db,$itemAndCountQuery, $itemAndCountParams, $currentPos, $limit)
    {
        try {

            $itemAndCountParams["limit"] = (int)$limit;
            $itemAndCountParams["current_pos"] = (int)$currentPos;
            $itemsAndCount = $db->fetchAll($itemAndCountQuery,Enum::FETCH_OBJ,$itemAndCountParams);
            if ($itemsAndCount) {
                $obj = Json::jsonDecode($itemsAndCount[0]->obj,JSON_OBJECT_AS_ARRAY);
                foreach ($itemsAndCount as &$item) {
                    unset($item->obj);
                }
                unset($item);
            }
            $currentPos = $obj["current_pos"];
            $totalItems = $obj["total_items"];
            $limit = $obj["chunk_size"];
            $totalChunks = $obj["total_chunks"];
            $before = $obj["before"];
            $next = $obj["next"];
            $offset = $obj["chunk_offset"];
            

            echo "";
            # first and last item on page (eg: row a to b of n)
            $pageFirst = $offset+1;
            $pageLast = $offset + $limit;
            if($pageLast > $totalItems) {
                $pageLast = $totalItems;
            }

            return (object)[
                "items" => $itemsAndCount,
                "offset" => $offset,
                "first" => 1,
                "before" => $before,
                "previous" => $before,
                "current" => $currentPos,
                "last" => $totalChunks,
                "next" => $next,
                "total_pages" => $totalChunks,
                "total_items" => $totalItems,
                "limit" => $limit,
                "page_item_first" => $pageFirst,
                "page_item_last" => $pageLast,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }

    protected function _preparePaginationSearch(&$db,$totalItems, $itemQuery, $itemParams, $currentPos, $limit)
    {
        try {
            $totalItems = (int)$totalItems;

            #---------------------------------------------------------
            # offset >=0
            #---------------------------------------------------------
            $currentPos = (int)$currentPos;
            if($currentPos < 1) {
                $currentPos = 1;
            } elseif ($currentPos > $totalItems) {
                $currentPos = $totalItems;
            }

            #---------------------------------------------------------
            # number of entries pr chunk >=1
            #---------------------------------------------------------
            $limit = (int)$limit;
            $limit = ($limit < 1) ? 1 : $limit;

            #---------------------------------------------------------
            # total number of chunks
            #---------------------------------------------------------
            $lastChunkEntries = $totalItems % $limit;
            $totalChunks = intdiv(floatval($totalItems), floatval($limit));
            $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;

            # before
            $before = ($currentPos > 1) ? $currentPos - 1 : 1;

            # next
            $next = ($currentPos < $totalChunks) ? $currentPos + 1: $totalChunks;

            # offset
            $offset = ($currentPos - 1) * $limit;
            if($offset>=($totalItems) && $totalItems>0) {
                $offset = $totalItems-1;
            } elseif ($offset<0 || $totalItems<=0) {
                $offset = 0;
            }
            # first and last item on page (eg: row a to b of n)
            $pageFirst = $offset+1;
            $pageLast = $offset + $limit;
            if($pageLast > $totalItems) {
                $pageLast = $totalItems;
            }

            $itemParams["limit"] = $limit;
            $itemParams["offset"] = $offset;

            $items = $db->fetchAll($itemQuery,Enum::FETCH_OBJ,$itemParams);

            return (object)[
                "items" => $items,
                "offset" => $offset,
                "first" => 1,
                "before" => $before,
                "previous" => $before,
                "current" => $currentPos,
                "last" => $totalChunks,
                "next" => $next,
                "total_pages" => $totalChunks,
                "total_items" => $totalItems,
                "limit" => $limit,
                "page_item_first" => $pageFirst,
                "page_item_last" => $pageLast,
            ];

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}