<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-25
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\ApplicationBase;


use App\Library\ApplicationRoot\ComponentRoot;

/**
 * Class ApplicationComponentBase
 * @package App\Library\ApplicationBase
 */
class ApplicationComponentBase extends ComponentRoot
{
    /**
     * Get searchTerm either from form input or from cache
     *
     * @param $key
     * @param $reset
     * @return bool|mixed|string
     */
    public function getSearchTerm($key, $reset = null, $viewvarName = "search_term")
    {
        if ($reset) {
            $this->removeKeyValue($key);
            $this->view->setVar($viewvarName, "");
            return false;
        }
        $organizationId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organizationId . $userId . $key;

        # if not isPost then get from cache or return false
        if (!$this->request->has($viewvarName)) {
            $searchTerm = $this->session->get($saveKey);
            if (strlen($searchTerm)) {
                $this->view->setVar($viewvarName, $searchTerm);
                return $searchTerm;
            }
            $this->view->setVar($viewvarName, "");
            return false;
        }

        # else get from form input and save to cache
        $searchTerm = substr($this->request->getPost($viewvarName), 0, 500);
        if (strlen($searchTerm)) {
            $this->session->set($saveKey, $searchTerm);
            $this->view->setVar($viewvarName, $searchTerm);
            return $searchTerm;
        } else {
            # if empty searchTerm reset cache
            $this->session->remove($saveKey);
            $this->view->setVar($viewvarName, "");
            return false;
        }
    }

    /**
     * Remove search term
     *
     * @param $key
     */
    public function removeSearchTerm($key)
    {
        $organaizeionId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organaizeionId . $userId . $key;
        $this->session->remove($saveKey);
        return;
    }

    /**
     * Simplifying manager specific session-vars
     * Used when saving UX relaved values
     *
     * @param $key
     * @param $valu
     */
    public function setKeyValue($key, $value)
    {
        $organaizeionId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organaizeionId . $userId . $key;
        $this->session->set($saveKey, $value);
    }

    public function hasKeyValue($key)
    {
        $organaizeionId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organaizeionId . $userId . $key;
        return $this->session->has($saveKey);
    }

    /**
     * Simplifying manager specific session-vars
     * Used when saving UX relaved values
     *
     * @param $key
     * @return mixed
     */
    public function getKeyValue($key)
    {
        $organaizeionId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organaizeionId . $userId . $key;
        return $this->session->get($saveKey);
    }

    /**
     * Simplifying manager specific session-vars
     * Used when saving UX relaved values
     *
     * @param $key
     */
    public function removeKeyValue($key)
    {
        $organaizeionId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $saveKey = $organaizeionId . $userId . $key;
        return $this->session->remove($saveKey);
    }
}