<?php

namespace App\Library\Paginator;

use App\Library\ApplicationBase\ApplicationComponentBase;
use App\Library\Utils\Csv;

class CsvHeaderPaginator extends ApplicationComponentBase
{
    private $file;

    private $csv;

    private $currentPos;

    private $limit;

    private $seaarchTerm;

    public function __construct(string $file, $currentPos, $limit, $searchTerm = "")
    {
        $this->csv = new Csv();
        $this->file = new \SplFileObject($file);
        $this->currentPos = (int)$currentPos;
        $this->limit = (int)$limit;
        $this->seaarchTerm = $searchTerm;
    }

    public function paginate()
    {
        $header = $this->csv->getHeader($this->file);

        if (strlen($this->searchTerm) > 0) {
            # regular expression
            # however: mb_ereg_match always matches from start of string
            if (str_starts_with($this->searchTerm, "^")) {
                $searchTermRepl = str_replace([".", "*"], ["\.", ".*"], $this->searchTerm);
            } else {
                $searchTermRepl = ".*" . str_replace([".", "*"], ["\.", ".*"], $this->searchTerm);
            }

            $newHeader = [];
            foreach ($header as $item) {
                $matched = mb_ereg_match($searchTermRepl, $item);
                if ($matched) {
                    $newHeader[] = $item;
                }
            }
            $header = $newHeader;
        }

        $totalItems = count($header);

        #---------------------------------------------------------
        # offset >=0
        #---------------------------------------------------------
        if ($this->currentPos < 1) {
            $this->currentPos = 1;
        } elseif ($this->currentPos > $totalItems) {
            $this->currentPos = $totalItems;
        }

        #---------------------------------------------------------
        # number of entries pr chunk >=1
        #---------------------------------------------------------
        $this->limit = ($this->limit < 1) ? 1 : $this->limit;

        #---------------------------------------------------------
        # total number of chunks
        #---------------------------------------------------------
        $lastChunkEntries = $totalItems % $this->limit;
        $totalChunks = intdiv(floatval($totalItems), floatval($this->limit));
        $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;

        if ($this->currentPos > $totalChunks) {
            $this->currentPos = $totalChunks;
        }

        # before
        $before = ($this->currentPos > 1) ? $this->currentPos - 1 : 1;

        # next
        $next = ($this->currentPos < $totalChunks) ? $this->currentPos + 1 : $totalChunks;

        # offset
        $offset = ($this->currentPos - 1) * $this->limit;
        if ($offset >= ($totalItems) && $totalItems > 0) {
            $offset = $totalItems - 1;
        } elseif ($offset < 0 || $totalItems <= 0) {
            $offset = 0;
        }

        # first and last item on page (eg: row a to b of n)
        $pageFirst = $offset + 1;
        $pageLast = $offset + $this->limit;
        if ($pageLast > $totalItems) {
            $pageLast = $totalItems;
        }

        $itemElements = array_slice($header, $offset, $this->limit);
        $items = [];
        foreach ($itemElements as $element) {
            $items[] = (object)["name" => $element];
        }

        echo "";

        return (object)[
            "items"           => $items,
            "offset"          => $offset,
            "first"           => 1,
            "before"          => $before,
            "previous"        => $before,
            "current"         => $this->currentPos,
            "last"            => $totalChunks,
            "next"            => $next,
            "total_pages"     => $totalChunks,
            "total_items"     => $totalItems,
            "limit"           => $this->limit,
            "page_item_first" => $pageFirst,
            "page_item_last"  => $pageLast,
        ];
    }
}