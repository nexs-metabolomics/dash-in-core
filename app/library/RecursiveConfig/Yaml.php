<?php
/**
 * *
 *  * This file is part of the Phalcon Framework.
 *  *
 *  * (c) Dash-In Dev-team
 *  *
 *  * For the full copyright and license information, please view the LICENSE.txt
 *  * file that was distributed with this source code.
 *  
 *
 */
namespace App\Library\RecursiveConfig;

use Phalcon\Config\Config;
use Phalcon\Config\Exception;

class Yaml extends Config
{
    /**
     * Parse yaml files recursively
     *
     * @param string $filePath
     * @throws Exception
     */
    public function __construct($filePath)
    {
        $this->parseRecursive($filePath);
    }

    /**
     * Parse a set of yaml files recursive
     *
     * use this pattern to trigger inclusion of incfile.yml:
     * ---
     * imports:
     *     - { resource: incfile.yml }
     *
     * ---
     * include paths can be relative or absolute
     *
     * @param $filePath
     * @param null $dir
     * @throws Exception
     */
    private function parseRecursive($filePath, $dir = null)
    {
        if ($this->pathIsRelative($filePath)) {
            if ($dir === null) {
                $dir = getcwd();
            }
            $filePath = realpath($dir . DIRECTORY_SEPARATOR . $filePath);
        }
        $dir = dirname($filePath);

        $parsed = [];
        $import = [];
        $rawParsed = yaml_parse_file($filePath);
        foreach ($rawParsed as $key => $value) {
            if ($key === "imports") {
                foreach ($value as $importResource) {
                    if (isset($importResource["resource"])) {
                        $import[] = $importResource["resource"];
                    }
                }
            } else {
                $parsed[$key] = $value;
            }
        }
        if (is_array($parsed)) {
            $parsedConfig = new Config($parsed);
            parent::merge($parsedConfig);
        }

        foreach ($import as $importFilePath) {
            $this->parseRecursive($importFilePath, $dir);
        }
    }

    /**
     * Check if filename is relative or absolute
     *
     * @param $filename
     * @return bool
     * @throws Exception
     */
    private function pathIsRelative($filename)
    {
        $netdir = "(^\\\\)";
        $windir = "(^[a-zA-Z]\\:)";
        $unxdir = "(^/)";
        $alldirs = "#" . $windir . "|" . $netdir . "|" . $unxdir . "#";
        $res = preg_match($alldirs, $filename);
        if ($res === 0) {
            return true;
        } elseif ($res === 1) {
            return false;
        }
        throw new Exception(__METHOD__ . ": an error occurred.");
    }
}
