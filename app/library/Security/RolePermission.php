<?php

namespace App\Library\Security;

use App\Library\ApplicationRoot\ComponentRoot;
use App\Library\Utils\UniqueId;
use Phalcon\Db\Enum;

/**
 * Permissions
 */
class RolePermission extends ComponentRoot
{
    /**
     * @var bool $ROLE_SYS_ADMIN sysadmin role
     */
    public $ROLE_SYS_ADMIN = false;

    /**
     * @var bool $ROLE_APP_ADMIN appadkin role
     */
    public $ROLE_APP_ADMIN = false;

    /**
     * @var bool $ROLE_ORG_ADMIN orgadmin role
     */
    public $ROLE_ORG_ADMIN = false;

    /**
     * @var bool $ROLE_ORG_USER standard org-user role
     */
    public $ROLE_ORG_USER = false;

    /**
     * @var bool $ROLE_ORG_ALLOW_WRITE permission to write in the context of org-user
     *                           (opposite of readonly)
     *                           A ROLE without explicit role_id which makes
     *                           filtering against readonly easier.
     *                           Is set true by any role that allows write.
     */
    public $ROLE_ORG_ALLOW_WRITE = false;

    /**
     * @var bool $ROLE_ORG_READONLY readonly role
     */

    public $ROLE_ORG_READONLY = false;

    /**
     * @var bool $ALLOW_ROUTE is the current route allowed for the current user
     *                       in the current context
     */
    public $ALLOW_ROUTE = false;

    public function __construct($userId)
    {
        $this->_init($userId);
    }

    /**
     * Initialize permissions for current user
     *
     * @param $userId
     *
     * @return false|void
     */
    private function _init($userId)
    {
        if(!$this->router->wasMatched()) {
            return false;
        }
        $matchedRoutePaths = $this->router->getMatchedRoute()->getPaths();
        $securitylabelId = (array_key_exists("seclab_id", $matchedRoutePaths) ? $matchedRoutePaths["seclab_id"] : false);

        if (!is_int($securitylabelId)) {
            return false;
        }
        
        $roles = $this->_getPermissionsFromDb($userId, $securitylabelId);
        if (!$roles) {
            return false;
        }

        $keys = array_column($roles, "seclab_name");
        $permissions = array_column($roles, "role_allow");
        $rolePermissions = array_combine($keys, $permissions);

        $this->_setPermissions($rolePermissions);
    }

    /**
     * Set permissions
     *
     * @param $rolePermissions
     *
     * @return void
     */
    protected function _setPermissions($rolePermissions)
    {
        $this->view->ROLE_SYS_ADMIN = false;
        $this->view->ROLE_APP_ADMIN = false;
        $this->view->ROLE_ORG_ADMIN = false;
        $this->view->ROLE_ORG_USER = false;
        if ($rolePermissions["ROLE_SYS_ADMIN"] === true) {
            $this->view->ROLE_SYS_ADMIN = $this->ROLE_SYS_ADMIN = true;
            $this->ROLE_ORG_ALLOW_WRITE = true;
        }
        if ($rolePermissions["ROLE_APP_ADMIN"] === true) {
            $this->view->ROLE_APP_ADMIN = $this->ROLE_APP_ADMIN = true;
            $this->ROLE_ORG_ALLOW_WRITE = true;
        }
        if ($rolePermissions["ROLE_ORG_ADMIN"] === true) {
            $this->view->ROLE_ORG_ADMIN = $this->ROLE_ORG_ADMIN = true;
            $this->ROLE_ORG_ALLOW_WRITE = true;
        }
        if ($rolePermissions["ROLE_ORG_USER"] === true) {
            $this->view->ROLE_ORG_USER = $this->ROLE_ORG_USER = true;
            $this->ROLE_ORG_ALLOW_WRITE = true;
        }
        if ($rolePermissions["ROLE_ORG_READONLY"] === true) {
            $this->view->ROLE_ORG_READONLY = $this->ROLE_ORG_READONLY = true;
        }
        if ($rolePermissions["ALLOW_ROUTE"] === true) {
            $this->ALLOW_ROUTE = true;
        }
        $this->view->ROLE_ORG_ALLOW_WRITE = $this->ROLE_ORG_ALLOW_WRITE;
    }

    /**
     * Get permission from database
     *
     * @param $userId
     * @param $securitylabelId
     *
     * @return array|false
     */
    private function _getPermissionsFromDb($userId, $securitylabelId)
    {
        if (!UniqueId::uuidValidate($userId)) {
            return false;
        }
        try {
            $sql = RolePermissionSql::$manager_role_allow;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "user_id"   => $userId,
                "seclab_id" => $securitylabelId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }
        return false;
    }
}