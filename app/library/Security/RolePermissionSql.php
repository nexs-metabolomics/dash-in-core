<?php

namespace App\Library\Security;

class RolePermissionSql
{

    static $manager_role_allow =
        <<<'EOD'
            WITH
              manager_roles_query  AS (
                                        SELECT min(c.role_id::TEXT::INT) AS role_id
                                             , (:seclab_id)::INT         AS seclab_id
                                        FROM admin.users                                                            a
                                             LEFT JOIN admin.organization                                           b ON b.organization_id = a.active_org_id
                                           , jsonb_array_elements(admin.get_manager_roles(active_org_id , user_id)) c (role_id)
                                        WHERE user_id = :user_id
                                      )
              , seclab_query       AS (
                                        SELECT seclab_name
                                             , role_id
                                             , role_array::INT[]
                                        FROM (
                                               VALUES ('ROLE_SYS_ADMIN'   ,    1, '{1,10,500,510,590,1000}')
                                                    , ('ROLE_APP_ADMIN'   ,   10, '  {10,500,510,590,1000}')
                                                    , ('ROLE_ORG_ADMIN'   ,  500,      '{500,510,590,1000}')
                                                    , ('ROLE_ORG_USER'    ,  510,          '{510,590,1000}')
                                                    , ('ROLE_ORG_READONLY',  590,              '{590,1000}')
                                                    , ('ROLE_ALL'         , 1000,                  '{1000}')
                                             ) a(seclab_name, role_id, role_array)
                                      )
              , match_permission_query AS (
                                            SELECT b0.role_array
                                                 , b1.seclab_id
                                                 , 'ALLOW_ROUTE'                            AS seclab_name
                                                 , ARRAY [b1.seclab_id] <@ b0.role_array AS route_allow
                                            FROM seclab_query                b0
                                              INNER JOIN manager_roles_query b1 ON b0.role_id = b1.role_id
                                          )
              , role_permission_query  AS (
                                            SELECT a.seclab_name
                                                 , a.role_id
                                                 , ARRAY [a.role_id] <@ b.role_array AS role_allow
                                            FROM seclab_query           a
                                               , match_permission_query b
              
                                          )
            SELECT seclab_name
                 , role_id
                 , role_allow
            FROM role_permission_query
            UNION ALL
            SELECT seclab_name
                 , seclab_id
                 , route_allow
            FROM match_permission_query;
        EOD;

}