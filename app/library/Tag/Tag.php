<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-12-04
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\Tag;

class Tag extends \Phalcon\Tag
{

    public static function plainText($parameters)
    {

        if (!is_array($parameters)) {
            $params = [$parameters];
        } else {
            $params = $parameters;
        }

        if (!isset($params[0])) {
            if (isset($params["id"])) {
                $params[0] = $params["id"];
            }
        }

        $id = $params[0];
        if (!isset($params["name"])) {
            $params["name"] = $id;
        } else {
            $name = $params["name"];
            if (empty($name)) {
                $params["name"] = $id;
            }
        }

        if (!isset($params["id"])) {
            $params["id"] = $id;
        }

        if (isset($params["value"])) {
            $value = $params["value"];
            unset($params["value"]);
        } else {
            # 'content' is removed must be 'value'
            if (isset($params["content"])) {
                $value = $params["content"];
                unset($params["content"]);
            } else {
                $value = "";
            }
        }

        if (isset($params["tag"]) && strlen($params["tag"])) {
            $tag = $params["tag"];
            unset($params["tag"]);
            if (!preg_match("#^[a-zA-Z_:][-a-zA-Z0-9_:.]*#", $tag)
                || in_array($tag, ["input", "form", "button", "select", "textarea"])) {
                user_error("Illegal tag name '$tag' for PlainText element");
                $tag = false;
            }
        } else {
            $tag = false;
        }

        if (strlen($tag) > 0) {
            $code = self::renderAttributes("<$tag", $params);
            $code .= ">" . $value . "</$tag>";
        } else {
            $code = $value;
        }

        return $code;
    }

    public static function button($parameters, $rawContent = false)
    {

        if (!is_array($parameters)) {
            $params = [$parameters];
        } else {
            $params = $parameters;
        }

        if (!isset($params[0])) {
            if (isset($params["id"])) {
                $params[0] = $params["id"];
            }
        }

        $id = $params[0];
        if (!isset($params["name"])) {
            $params["name"] = $id;
        } else {
            $name = $params["name"];
            if (empty($name)) {
                $params["name"] = $id;
            }
        }

        if (!isset($params["id"])) {
            $params["id"] = $id;
        }

        if (isset($params["btn_txt_class"])) {
            $class = " {$params["btn_txt_class"]}";
            unset($params["btn_txt_class"]);
        } else {
            $class = "";
        }

        if (isset($params["content"])) {
            if ($rawContent) {
                $content = $params["content"];
            } else {
                $content = htmlentities($params["content"]);
            }
            unset($params["content"]);
        } else {
            $content = "Submit";
        }

        $code = self::renderAttributes("<button", $params);
        $code .= ">" . $content . "</button>";

        return $code;
    }

}