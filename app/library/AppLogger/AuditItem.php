<?php

namespace App\Library\AppLogger;

use App\Library\ApplicationRoot\ComponentRoot;

class AuditItem extends ComponentRoot
{
    
    public function getMessage($message="")
    {
        $organizationId = $this->SU->getActiveManagerOrgId();
        $userId = $this->SU->getUserId();
        $view = $this->view->getMainView();

        $lsVars = (array)$this->localsession->getDataFromQuerystring();
        $logVarsStr = "";
        if ($lsVars) {
            $logVars = [];
            foreach ($lsVars as $key => $var) {
                if (substr($key, 0, 1) != "_" && isset($var["l"])) {
                    $logVars[] = "$key=$var[v]";
                }
            }
            if ($logVars) {
                $logVarsStr = join(";", $logVars);
            }
        }
        $message = substr($message, 0, 255);

        $message = "m:$message|u:$userId|o:$organizationId|a:$view|v:$logVarsStr";
        
        return $message;

    }
}