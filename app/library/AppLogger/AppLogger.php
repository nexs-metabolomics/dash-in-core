<?php

namespace App\Library\AppLogger;

use Phalcon\Logger\Logger;

class AppLogger extends Logger
{
    /**
     * @var bool whether to log or not
     */
    private bool $doLogging;

    /**
     * @param string    $name
     * @param array     $adapters
     * @param bool|null $doLogging
     */
    public function __construct(string $name = "", array $adapters = [], bool $doLogging = null)
    {
        $this->doLogging = $doLogging === true;
        if ($this->doLogging) {
            parent::__construct($name, $adapters);
        }
    }

    /**
     * @param string $message logging message
     *
     * @return void
     */
    public function auditlog($context, string $message = "")
    {
        if (!$this->doLogging) {
            return;
        }

        $auditItem = new AuditItem();
        $auditMessage = $auditItem->getMessage($message);

        try {
            @$this->addMessage(Logger::INFO, $auditMessage);
        } catch (\UnexpectedValueException $e) {
            $context->flashSession->error("Audit logging is turned on but did not succeed.");
            error_log($e->getMessage());
        }
    }
}