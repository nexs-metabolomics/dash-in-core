<?php

namespace App\Library\StudyTransfer\V1;

class Properties
{

    const VERSION = "1";

    /**
     * @var array
     */
    protected array $_tableProperties;

    public function initialize()
    {
        $this->_tableProperties["study"]["fields"] = "study_id,name,title,description,start_date,endpoint,objectives,conclusion,exclusion,inclusion,institute,country_id,consortium_id,published,researchdesign_id,num_treat,num_factor,num_arm,researchdesign_text,num_volun,num_volun_terminated,recruit_start_year,recruit_end_year,blinding,blinding_method,created_at,updated_at,details";
        $this->_tableProperties["assay"]["fields"] = "assay_id,name,description,researchfield_id,created_at,updated_at";
        $this->_tableProperties["dataset"]["fields"] = "dataset_id,name,description,status,datasettype_id,created_at,updated_at,details";
        $this->_tableProperties["variable"]["fields"] = "variable_id,local_id,dataset_id,name,description,status,variabletype_id,datatype_id,researchfield_id,assay_id,nunitprefix_id,nunit_id,dunitprefix_id,dunit_id,created_at,updated_at,details";
        $this->_tableProperties["var_dataset"]["fields"] = "var_dataset_id,dataset_id,name,description,status,created_at,updated_at,details";
        $this->_tableProperties["study_event"]["fields"] = "event_id";
        $this->_tableProperties["var_variable"]["fields"] = "var_variable_id,var_dataset_id,local_id,name,description,status,search_single_id,search_generic_id,datatype_id,created_at,updated_at,details";
        $this->_tableProperties["study_center"]["fields"] = "center_id";
        $this->_tableProperties["var_datatable"]["fields"] = "variable_id,var_dataset_id,ordinal,status,created_at,updated_at,details,datarow";
        $this->_tableProperties["study_subject"]["fields"] = "subject_id";
        $this->_tableProperties["study_subevent"]["fields"] = "subevent_id";
        $this->_tableProperties["study_startgroup"]["fields"] = "startgroup_id";
        $this->_tableProperties["study_samplingtime"]["fields"] = "samplingtime_id";
        $this->_tableProperties["study_samplingevent"]["fields"] = "samplingevent_id";
        $this->_tableProperties["datadesign_datatable"]["fields"] = "datarow_id,datarow_ordinal,studyrow_id,dataset_id,study_id,datasetevent_id,datasetsubevent_id,datasetsamplingevent_id,datasetsamplingtime_id,datasetcenter_id,datasetstartgroup_id,datasetsubject_id,datasetevent_name,datasetsubevent_name,datasetsamplingevent_name,datasetsamplingtime_name,datasetcenter_name,datasetstartgroup_name,datasetsubject_name,datasetevent_status,datasetsubevent_status,datasetsamplingevent_status,datasetsamplingtime_status,datasetcenter_status,datasetstartgroup_status,datasetsubject_status,studyevent_id,studysubevent_id,studysamplingevent_id,studysamplingtime_id,studycenter_id,studystartgroup_id,studysubject_id,datarow";
        $this->_tableProperties["studydesign_datatable"]["fields"] = "studyrow_id,study_id,studyevent_id,studysubevent_id,studysamplingevent_id,studysamplingtime_id,studycenter_id,studystartgroup_id,studysubject_id,studyevent_ordinal,studyevent_created_at,studyevent_updated_at,studyevent_name,studyevent_label,studyevent_description,studysubevent_subeventtype_id,studysubevent_ordinal,studysubevent_intervention_type_id,studysubevent_created_at,studysubevent_updated_at,studysubevent_name,studysubevent_label,studysubevent_description,studysubevent_row_comment,studysubevent_additional_data,studysamplingevent_samplingtype_id,studysamplingevent_ordinal,studysamplingevent_created_at,studysamplingevent_updated_at,studysamplingevent_name,studysamplingevent_label,studysamplingevent_description,studysamplingevent_row_comment,studysamplingevent_additional_data,studysamplingtime_ordinal_time,studysamplingtime_created_at,studysamplingtime_updated_at,studysamplingtime_name,studysamplingtime_label,studysamplingtime_description,studycenter_ordinal,studycenter_created_at,studycenter_updated_at,studycenter_name,studycenter_label,studycenter_description,studystartgroup_ordinal,studystartgroup_created_at,studystartgroup_updated_at,studystartgroup_name,studystartgroup_label,studystartgroup_description,studysubject_name,studysubject_label,studysubject_description,studysubject_status,studysubject_created_at,studysubject_updated_at,studysubject_details";
        $this->_tableProperties["datadesign_x_study_event"]["fields"] = "dataset_id,datadesign_event_id,study_id,study_event_id";
        $this->_tableProperties["datadesign_x_study_center"]["fields"] = "dataset_id,datadesign_center_id,study_id,study_center_id";
        $this->_tableProperties["datadesign_x_study_subject"]["fields"] = "dataset_id,datadesign_subject_id,study_id,study_subject_id";
        $this->_tableProperties["datadesign_x_study_subevent"]["fields"] = "dataset_id,datadesign_subevent_id,study_id,study_subevent_id";
        $this->_tableProperties["datadesign_x_study_startgroup"]["fields"] = "dataset_id,datadesign_startgroup_id,study_id,study_startgroup_id";
        $this->_tableProperties["datadesign_x_study_samplingtime"]["fields"] = "dataset_id,datadesign_samplingtime_id,study_id,study_samplingtime_id";
        $this->_tableProperties["datadesign_x_study_samplingevent"]["fields"] = "dataset_id,datadesign_samplingevent_id,study_id,study_samplingevent_id";

    }

}