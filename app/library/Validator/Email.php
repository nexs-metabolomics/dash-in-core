<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-02-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\Validator;

use Phalcon\Filter\Validation;
use Phalcon\Messages\Message;
use Phalcon\Filter\Validation\AbstractValidator;

class Email extends AbstractValidator
{
    /**
     * Email validator that allows for empty email
     *
     * @param \Phalcon\Filter\Validation $validation
     * @param string              $field
     *
     * @return bool
     */
    public function validate(Validation $validation, $field): bool
    {
        $value = $validation->getValue($field);
        # only validate if not empty string
        if ($value !== "") {
            # if not valid email
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $message = $this->getOption('message');
                if (!$message) {
                    $message = "Not a valid email address";
                }
                $validation->appendMessage(new Message($message, $field));
                return false;
            }
        }
        return true;
    }
}