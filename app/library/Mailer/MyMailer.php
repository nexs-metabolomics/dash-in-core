<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-02-11
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
namespace App\Library\Mailer;

use Phalcon\Ext\Mailer\Manager;

class MyMailer extends Manager
{
    protected function registerSwiftTransport()
    {
        parent::registerSwiftTransport();
        $driver = $this->getConfig("driver");
        if($driver === "smtp") {
            $this->transport = $this->registerTransportSmtp();
        } elseif ($driver === "mail") {
            $this->transport = $this->registerTransportMail();
        } elseif ($driver === "sendmail") {
            $this->transport = $this->registerTransportSendmail();
        } elseif ($driver === "file") {
            $this->transport = $this->registerTransportFileSpool();
        } elseif ($driver === "memory") {
            $this->transport = $this->registerTransportMemorySpool();
        } else {
            throw new \InvalidArgumentException(sprintf("Driver-mail '%s' is not supported",$driver));
        }
    }
    
    protected function registerTransportFileSpool()
    {
        $path = $this->getConfig("spoolpath");
        
        /** @var \Swift_Spool $spool */
        $spool = $this->getDI()->get('\Swift_FileSpool',[$path]);
        
        /** @var \Swift_SpoolTransport $transport */
        $transport = $this->getDI()->get('\Swift_SpoolTransport',[$spool]);
        return $transport;
    }
    
    protected function registerTransportMemorySpool()
    {
        /** @var \Swift_MemorySpool $spool */
        $spool = $this->getDI()->get('\Swift_MemorySpool');

        /** @var \Swift_SpoolTransport $transport */
        $transport = $this->getDI()->get('\Swift_SpoolTransport',[$spool]);
        return $transport;
    }
    
    public function simpleMailer($to, $name=null, $subject="", $content="", $attachments = null, $cc = null, $bcc = null)
    {
        try {

            $out = [];

            /** @var \Phalcon\Ext\Mailer\Message $msg */
            $msg = $this->createMessage();
            $msg->to($to,$name);
            $msg->subject($subject);
            $msg->content($content);

            # attachments
            if($attachments) {
                if(isset($attachments[0])) {
                    # multiple attachments
                    foreach ($attachments as $attachment) {
                        $this->_handleAttachments($msg,$attachment);
                    }
                } elseif(isset($attachments["file"]) || isset($attachments["data"])) {
                    # single attachment
                    $this->_handleAttachments($msg,$attachments);
                }
            }
            
            # cc
            if($cc) {
                $msg->cc($cc);
            }
            
            # bcc
            if($bcc) {
                $msg->bcc();
            }
            
            $out["result"] = $msg->send();

        } catch (\Exception $e) {
            throw new MyMailerException($e->getMessage());
        }

        $out["failed"] = $msg->getFailedRecipients();
        return $out;
    }

    /**
     * @param \Phalcon\Ext\Mailer\Message $msg
     * @param $attachment
     */
    private function _handleAttachments(&$msg,$attachment)
    {
        if(isset($attachment["data"])) {

            if(!isset($attachment["name"]) || strlen($attachment["name"])===0) {
                $name = "Unknown";
            } else {
                $name = $attachment["name"];
            }

            if(!isset($attachment["options"])) {
                $options = [];
            } elseif (is_array($attachment["options"])) {
                $options = (array)$attachment["options"];
            } else {
                $options = $attachment["options"];
            }
            
            $msg->attachmentData($attachment["data"],$name,$options);

        } elseif (isset($attachment["file"]) && strlen($attachment["file"])) {

            if(!isset($attachment["options"])) {
                $options = [];
            } elseif (is_array($attachment["options"])) {
                $options = (array)$attachment["options"];
            } else {
                $options = $attachment["options"];
            }

            $msg->attachment($attachment["file"],$options);
        }
    }
    
    
}