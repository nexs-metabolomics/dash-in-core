<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use Phalcon\Http\Request\File;

/**
 *
 */
class SupportFileHandler extends ComponentRoot
{

    /**
     * file object or false
     *
     * @var bool|File
     */
    protected bool|File $_file = false;

    /**
     * Hash of file content
     *
     * @var string
     */
    protected string $_filehash = "";

    /**
     * @var string
     */
    protected string $_filenameClean = "";

    /**
     * Repository
     *
     * @var bool|SupportFileRepository
     */
    protected bool|SupportFileRepository $_repository = false;

    /**
     * Get the repository
     *
     * @return SupportFileRepository|bool
     */
    protected function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new SupportFileRepository();
        }

        return $this->_repository;
    }

    /**
     * Verify location. Create if not exists
     *
     * @param $location
     *
     * @return false
     */
    protected function _verifyLocation($location)
    {

        if (is_dir($location)) {
            if (is_writeable($location)) {

                # exists, writable
                return true;
            } else {
                # exists, not writable
            }
        } else {
            if (mkdir($location, recursive: true)) {
                if (is_writeable($location)) {

                    # created, writable
                    return true;
                } else {
                    # created, not writable
                }
            }
        }

        return false;
    }

    /**
     * Return the storage location of study support files
     *
     * @param $studyId        string (uuid)
     *
     * @return string path
     */
    protected function _getStudySupportfileStorageLocation($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        if (!$studyInfo) {
            return false;
        }
        $organizationId = $studyInfo["organization_id"];

        $organizationStorageLocation = $this->storage->getStorageLocation(["organization_id" => $organizationId, "type" => "organization"]);
        $studySupportfileStorageLocation = "$organizationStorageLocation/studies/$studyId/supportfiles";

        if ($this->_verifyLocation($studySupportfileStorageLocation)) {
            return $studySupportfileStorageLocation;
        }

        return false;
    }

    /**
     * Return the storage location of dataset support files
     *
     * @param $organizationId
     * @param $datasetId
     *
     * @return string
     */
    protected function _getDatasetSupportfileStorageLocation($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        $datasetInfo = $this->_getRepository()->getDatasetInfo($datasetId);
        if (!$datasetInfo) {
            return false;
        }
        $organizationId = $datasetInfo["organization_id"];

        $organizationStorageLocation = $this->storage->getStorageLocation(["organization_id" => $organizationId, "type" => "organization"]);
        $datasetSupportfileStorageLocation = "$organizationStorageLocation/datasets/$datasetId/supportfiles";

        if ($this->_verifyLocation($datasetSupportfileStorageLocation)) {
            return $datasetSupportfileStorageLocation;
        }

        return false;

    }

    /**
     * Return storage location of assay support files
     *
     * @param $assayId
     *
     * @return false|string
     */
    protected function _getAssaySupportfileStorageLocation($assayId)
    {

        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }

        $adminStorageLocation = $this->storage->getStorageLocation(["type" => "admin"]);
        $assaySupportfileStorageLocation = "$adminStorageLocation/assays/$assayId/supportfiles";

        if ($this->_verifyLocation($assaySupportfileStorageLocation)) {
            return $assaySupportfileStorageLocation;
        }

        return false;
    }

    /**
     * Handle file upload
     *
     * returns true if successful
     *
     * @return bool
     */
    public function handleUpload()
    {

        if (!$this->request->hasFiles()) {
            return false;
        }
        $this->_file = $this->request->getUploadedFiles()[0];

        return true;
    }

    /**
     * Get has on temp file name
     *
     * @return false|string
     */
    protected function getFilehash()
    {

        if (strlen($this->_filehash) == 0) {
            $this->_filehash = sha1_file($this->_file->getTempName());
        }

        return $this->_filehash;
    }

    /**
     * Create combined hash on file content and name
     *
     * @return string
     */
    protected function getFileDigest()
    {

        $fileHash = $this->getFilehash();
        $fileName = $this->_file->getName();

        return sha1($fileHash . $fileName);
    }

    #-------------------------------------------------------------------------------------------------
    # common
    #-------------------------------------------------------------------------------------------------

    /**
     * Prepare newly uploaded file file for save and registration
     *
     * @param $storageDir string
     *
     * @return array|false
     */
    protected function _saveFile(string $storageDir)
    {

        if (!$this->_file) {
            return false;
        }

        # get file info
        $fileExt = $this->_file->getExtension();
        $fileType = $this->_file->getRealType();
        $fileName = $this->_file->getName();

        # create hash-digest of file
        $inputFileDigest = $this->getFileDigest();
        $inputFileDigestExt = "{$inputFileDigest}.{$fileExt}";
        $inputFilePath = "$storageDir/$inputFileDigestExt";

        # only move file if not already present
        if (!is_file($inputFilePath)) {
            $saveOk = $this->_file->moveTo($inputFilePath);
            if (!$saveOk) {
                return false;
            }
        }

        return [
            "filename"      => $inputFileDigestExt,
            "fileextension" => $fileExt,
            "mimetype"      => $fileType,
            "name"          => $fileName,
        ];

    }

    /**
     * Delete a support file from disk
     *
     * @param $filePath
     *
     * @return bool
     */
    protected function _deleteFile($filePath)
    {

        if (!file_exists($filePath)) {
            return true;
        }
        $ok = unlink($filePath);

        return $ok;
    }

    /**
     * Get info on support file relevant for view
     *
     * @param $supportfileId
     *
     * @return array|false
     */
    public function getSupportFileInfo($supportfileId)
    {

        $supportfileInfo = $this->_getRepository()->getSupportfileInfo($supportfileId);

        return $supportfileInfo;
    }

    /**
     * Update the description of a study support file
     *
     * @param $supportfileId
     * @param $description
     *
     * @return bool
     */
    public function updateFileDescription($supportfileId, $description)
    {

        $ok = $this->_getRepository()->updateFileDescription($supportfileId, $description);

        return $ok;
    }

    /**
     * Download a support file
     *
     * @param $params
     * @param $supportfileId
     *
     * @return false|void
     */
    public function downloadSupportFile($params, $supportfileId)
    {

        $result = $this->_getRepository()->getSupportFileInfo($supportfileId);
        if (!$result) {
            $this->flashSession->error("Could not find file in database");

            return false;
        }

        $storageLocation = $this->storage->getStorageLocation($params);

        $filename = $result["filename"];
        $filePath = "$storageLocation/$filename";

        $mimeType = $result["mimetype"];
        $ext = $result["fileextension"];
        $originalName = $result["name"];

        $fileContents = file_get_contents($filePath);
        if ($fileContents === false) {
            $this->flashSession->error("Could not find file");
        }
        $this->downloader->send($fileContents, $originalName, $ext, $mimeType);
    }

    /**
     * Get raw contents of files for download or adding to zip etc.
     *
     * @param $params        array of the form ["study_id"0> <study_id>,"type" => "study"]
     * @param $supportFileId uuid|string
     *
     * @return false|string
     */
    public function getStudySupportFileContents(array $params, uuid|string $supportFileId)
    {

        $result = $this->_getRepository()->getSupportFileInfo($supportFileId);
        if (!$result) {
            error_log("Could not find file in database");

            return false;
        }
        $params["organization_id"] = $result["organization_id"];
        $storageLocation = $this->storage->getStorageLocation($params);

        $filename = $result["filename"];
        $filePath = "$storageLocation/$filename";

        $fileContents = file_get_contents($filePath);
        if ($fileContents === false) {
            return false;
        }

        return $fileContents;
    }

    /**
     * Register a support file in the database
     *
     * @param array $params
     * @param array $fileInfo
     *
     * @return false|mixed
     */
    protected function _registerFile(array $params, array $fileInfo)
    {

        $supportfileId = false;

        $type = $params["type"];

        # register file
        if ($type === "study") {
            $supportfileId = $this->_getRepository()->registerStudyFile($params["study_id"], $params["organization_id"], $fileInfo);

        } elseif ($type === "dataset") {
            $supportfileId = $this->_getRepository()->registerDatasetFile($params["dataset_id"], $params["organization_id"], $fileInfo);

        } elseif ($type === "assay") {
            $supportfileId = $this->_getRepository()->registerAssayFile($params["assay_id"], $fileInfo);

        } else {
            $this->flashSession->warning("Unknown type '$type'");
        }

        return $supportfileId;

    }

    /**
     * Remove file entry from database
     *
     * @param $params
     * @param $supportfileId
     *
     * @return bool
     */
    protected function _unregisterFile($params, $supportfileId)
    {

        $isUnregistered = false;

        $type = $params["type"];

        if ($type === "study") {
            $studyId = $params["study_id"];
            $isUnregistered = $this->_getRepository()->unregisterFileFromStudy($studyId, $supportfileId);
        } elseif ($type === "dataset") {
            $datasetId = $params["dataset_id"];
            $isUnregistered = $this->_getRepository()->unregisterFileFromDataset($datasetId, $supportfileId);
        } elseif ($type === "assay") {
            $assayId = $params["assay_id"];
            $isUnregistered = $this->_getRepository()->unregisterFileFromAssay($assayId, $supportfileId);
        } else {
            $this->flashSession->warning("Unknown type '$type'");
        }

        if (!$isUnregistered) {
            $this->flashSession->error("Could not unregister file");

            return false;
        }

        return true;
    }

    /**
     * Register uploaded file as support file
     * and save to disk
     *
     * @param $params array [id, type]
     *
     * @return false|mixed|void
     */
    public function saveAndRegister(array $params)
    {

        # get storage location
        $storageDir = $this->storage->getStorageLocation($params);
        if (!$storageDir) {
            $this->flashSession->error("Could not find storage location");

            return false;
        }

        # save file
        $fileInfo = $this->_saveFile($storageDir);
        if (!$fileInfo) {
            $this->flashSession->error("Could not save file");

            return false;
        }

        # register file
        $supportfileId = $this->_registerFile($params, $fileInfo);
        if ($supportfileId) {
            return $supportfileId;
        }

        $this->flashSession->error("Could not register file");

    }

    /**
     * Delete and unregister a support file
     *
     * @param $params array [object-id,type]
     * @param $supportfileId
     *
     * @return false|void
     */
    public function deleteFile(array $params, $supportfileId)
    {

        $fileInfo = $this->_getRepository()->getSupportFileInfo($supportfileId);
        if (!$fileInfo) {
            $this->flashSession->error("Could not find registered file");

            return false;
        }

        # get storage location
        $storageDir = $this->storage->getStorageLocation($params);
        if (!$storageDir) {
            $this->flashSession->error("Could not find storage location");

            return false;
        }

        $filename = $fileInfo["filename"];
        $filePath = "$storageDir/$filename";

        # try to delete file
        $isDeleted = $this->_deleteFile($filePath);
        if (!$isDeleted) {

            $fileOriginalName = $fileInfo["name"];
            $this->flashSession->error("Could not delete '$fileOriginalName'");

            return false;
        }
        $isUnregistered = $this->_unregisterFile($params, $supportfileId);
        if (!$isUnregistered) {
            $fileOriginalName = $fileInfo["name"];
            $this->flashSession->error("Could not unregister '$fileOriginalName'");

            return false;
        }

        return true;

    }

    /**
     * Return a file list linked to the object-id
     *
     * @param $params array
     *
     * @return array|false
     */
    public function getFileList(array $params)
    {

        $fileList = false;
        $type = $params["type"];
        if ($type === "study") {
            $studyId = $params["study_id"];
            $fileList = $this->_getRepository()->getStudyfileList($studyId);
        } elseif ($type === "dataset") {
            $datasetId = $params["dataset_id"];
            $fileList = $this->_getRepository()->getDatasetfileList($datasetId);
        } elseif ($type === "assay") {
            $assayId = $params["assay_id"];
            $fileList = $this->_getRepository()->getAssayfileList($assayId);
        } else {
            $this->flashSession->warning("Unknown type '$type'");
        }

        return $fileList;
    }

}