<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class Json extends ComponentRoot
{

    /**
     * @param mixed $value
     * @param int $options
     * @param int $depth
     * @return string
     * @throws JsonException
     */
    public static function jsonEncode($value, $options = 0, $depth = 512)
    {
        # returns json-string or false
        $out = json_encode($value, $options, $depth);
        self::_handleJsonError(json_last_error());
        return $out;
    }

    /**
     * @param $string
     * @param bool $assoc
     * @param int $depth
     * @param int $options
     * @throws JsonException
     * @return mixed
     */
    public static function jsonDecode($string, $assoc = false, $depth = 512, $options = 0)
    {
        # returns php type/object/array or null
        #
        # OBS!! returns null on null-type AND null on error
        # php7.0: An empty PHP string or value that after casting to string
        #   is an empty string (NULL, FALSE) results in JSON syntax error.
        # retain old behavior
        if(!strlen($string)) {
            return null;
        }
        $out = json_decode($string, $assoc, $depth, $options);
        $eNum = json_last_error();
        self::_handleJsonError($eNum);
        return $out;
    }

    /**
     * @param $lastError
     * @throws JsonException
     */
    protected static function _handleJsonError($lastError)
    {
        if ($lastError === JSON_ERROR_NONE) {
            return;
        }
        throw new JsonException(json_last_error_msg(), $lastError);
    }

}