<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-03-17
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class Csv extends ComponentRoot
{
    /**
     * @param $array
     * @param $sep
     * @param $header
     *
     * @return false|string
     */
    public function arrayToCsv($array, $sep, $header = false)
    {
        if (!is_array($array) && $array) {
            return false;
        }
        $fh = fopen("php://memory", "a+");
        if ($header && is_array($header)) {
            fputcsv($fh, $header, $sep, '"', "\\");
        }
        # avoid an empty last line
        foreach ($array as $row) {
            fputcsv($fh, $row, $sep, '"', "\\");
        }

        rewind($fh);
        $contens = stream_get_contents($fh);
        return $contens;
    }

    /**
     * @param \SplFileObject $file
     * @param                $sep
     * @param                $nrow
     *
     * @return false|mixed
     */
    public function validateSeparator(\SplFileObject $file, $sep, $nrow = 10)
    {
        $file->rewind();
        for ($i = 0; $i < $nrow; ++$i) {
            # get number of columns in (up to) first 10 rows

            # if last line is a single newline then stop
            # if last line is a dataline followed by a newline - don't stop
            $x = $file->fgetcsv($sep);
            $c = $file->current();
            if ($file->eof() && !$c) {
                break;
            }
            $nCols[$i] = count($x);
        }
        $file->rewind();
        # do all rows have the same number of columns?
        $min = min($nCols);
        $max = max($nCols);
        $res = ($min === $max) ? $min : false;
        return $res;
    }

    /**
     * Try to determine the seperator from data
     *
     * returns selected separator or false
     *
     * @param \SplFileObject $file
     * @param null           $sep if $sep == null try to determine sep
     *
     * @return bool|string
     */
    public function guessSeparator(\SplFileObject $file, $sep = null)
    {
        # list of separators to guess
        $separators = [",", ";", "\t", " "];

        # check selected separator (if any)
        if (in_array($sep, $separators)) {
            # validate by counting number of resulting columns
            $res = $this->validateSeparator($file, $sep);
            if ($res > 1) {
                # return first likely correct sep
                return $sep;
            }
        } else {
            # loop through separators to search for likely correct separator
            foreach ($separators as $sep) {
                # returns
                #   false:  unequal number of columns
                #   1:      one column in all rows
                #   1 < x:  equal of columns (number more than 1) in all rows
                $res = $this->validateSeparator($file, $sep);
                if ($res > 1) {
                    # return first likely correct sep
                    return $sep;
                }
            }
        }
    }

    /**
     * Return first row as header or false if first header is not all string
     *
     * @param \SplFileObject $file
     * @param                $sep
     *
     * @return array|false|void
     */
    public function getHeader(\SplFileObject $file, $sep = null)
    {
        $sep = $this->guessSeparator($file, $sep);
        if ($sep) {
            $file->rewind();
            $header = $file->fgetcsv($sep);
            # if header is not all string => header=FALSE
            foreach ($header as $field) {
                if (!is_string($field)) {
                    return false;
                }
            }
            return $header;
        }
        return false;
    }
}