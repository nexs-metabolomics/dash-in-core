<?php

namespace App\Library\Utils;

use App\Library\ApplicationBase\ApplicationComponentBase;
use Phalcon\Encryption\Crypt\Exception\Exception;
use Phalcon\Encryption\Crypt\Exception\Mismatch as CryptMismatchException;

/**
 * Class LocalSession
 *
 * maintains a per tab type of session variable that
 * is local to the individual tab.
 * It is transferred as an encrupted GET qeurystring variable
 * The local session variable is automatically unserialized.
 *
 *
 * @package App\Library\Utils
 */
class LocalSession extends ApplicationComponentBase
{
    /**
     * Data held in the current iteration (local session)
     * Will be automatically loaded when server receives request
     * within the context of the current session.
     *
     * OBS: two local sessions could potentially collide
     * if the user sends two pages at (appr.) the exact same time
     *
     * @var array
     */
    private $_data;
    /**
     * The querystring to be appended to the uri
     * possibly including the preceding separator (% or ?)
     *
     * @var string
     */
    private $_querystring;
    private $_isreset = false;
    /**
     * The querystring variable name
     * (i.e. 'ls' in: https://example.com/?ls=xxx)
     *
     * @var string
     */
    private $_querystringVariableName = "ls";

    public function __construct()
    {
        $this->_setDataFromQuerystring();
    }

    /**
     * Retrieve local session data from the querystring
     * unserialize and store in $_data
     */
    private function _setDataFromQuerystring()
    {
        if ($this->_isreset) {
            return;
        }
        if ($this->_data) {
            return;
        }

        if (!$this->request->has($this->_querystringVariableName)) {
            return;
        }

        try {
            $secret = $this->config->ls_querystring_secret;

            $dataJsonB64Encrypted = $this->request->get($this->_querystringVariableName);
            $dataJson = $this->crypt->decryptBase64($dataJsonB64Encrypted, $secret, true);

            $data = Json::jsonDecode($dataJson, JSON_OBJECT_AS_ARRAY);
            $this->_data = $data;
        } catch (CryptMismatchException $e) {
            error_log("Mismatch error");
            error_log($e);
        } catch (JsonException $e) {
            error_log("Json decoding error");
            error_log($e);
        }
    }

    /**
     * Return all data from querystring directly
     *
     * @return mixed|void|null
     */
    public function getDataFromQuerystring()
    {
        if (!$this->request->has($this->_querystringVariableName)) {
            return;
        }

        try {
            $secret = $this->config->ls_querystring_secret;

            $dataJsonB64Encrypted = $this->request->get($this->_querystringVariableName);
            $dataJson = $this->crypt->decryptBase64($dataJsonB64Encrypted, $secret, true);

            $data = Json::jsonDecode($dataJson, JSON_OBJECT_AS_ARRAY);
            return $data;
        } catch (CryptMismatchException $e) {
            error_log("Mismatch error");
            error_log($e);
        } catch (JsonException $e) {
            error_log("Json decoding error");
            error_log($e);
        }
    }

    /**
     * Serialize $_data and set it as the querystring session variable
     */
    private function _setQuerystring()
    {
        if ($this->_isreset) {
            return;
        }

        if ($this->_querystring) {
            return;
        }
        if (!$this->_data) {
            return;
        }
        try {
            $secret = $this->config->ls_querystring_secret;
            $this->setKeyValue("__random_salt__", UniqueId::uuid4());
            $dataJson = Json::jsonEncode($this->_data);
            $dataJsonB64Encrypted = $this->crypt->encryptBase64($dataJson, $secret, true);
            $this->_querystring = $dataJsonB64Encrypted;
        } catch (Exception $e) {
            error_log("Encryption error");
            error_log($e);
        } catch (JsonException $e) {
            error_log("Json encoding error");
            error_log($e);
        }
    }

    /**
     * Has the querystring been created (serialized) yet
     *
     * @return bool
     */
    public function hasQuerystring()
    {
        if ($this->_isreset) {
            false;
        }
        if ($this->_querystring) {
            return true;
        }
        return false;
    }

    public function reset()
    {
        $this->_querystring = null;
        $this->_data = null;
        $this->_isreset = true;
    }

    /**
     * Get the serialized querystring ready to be appended to an URI
     * possibly with preceding separator (% or ?) prepended
     *
     * @param string $prefix preceding separator (% or ?)
     *
     * @return string
     */
    public function getQuerystring($prefix = "")
    {
        if ($this->_isreset) {
            return "";
        }
        $this->_setQuerystring();
        if ($this->_querystring) {
            if (!($prefix === "?" || $prefix === "&" || $prefix === "")) {
                $prefix = "";
            }
            return $prefix .
                $this->_querystringVariableName .
                "=" .
                $this->_querystring;
        }
        return "";
    }

    /**
     * Validate key for key/value pairs
     *
     * @param $key
     *
     * @return bool
     */
    private function _keyIsValid($key)
    {
        if ($this->_isreset) {
            return;
        }
        if (strlen($key) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Set a key/value pair
     *
     * @param $key
     * @param $value
     */
    public function setKeyValue($key, $value, int $loglevel = 0)
    {
        if ($this->_isreset) {
            return;
        }
        if ($this->_keyIsValid($key)) {
            $this->_data[$key]["v"] = $value;
            $this->_data[$key]["l"] = $loglevel;
        }
    }

    /**
     * Check if key is present
     *
     * @param $key
     *
     * @return bool
     */
    public function hasKeyValue($key)
    {
        return isset($this->_data[$key]["v"]);
    }

    /**
     * Retrieve the value of a key/value pair
     *
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function getKeyValue($key, $default = null)
    {
        if (isset($this->_data[$key]["v"])) {
            return $this->_data[$key]["v"];
        }
    }

    /**
     * Remove a key/value pair
     *
     * @param $key
     */
    public function removeKeyValue($key)
    {
        unset($this->_data[$key]);
    }

    /**
     * Return key/value paires that should be logged at level of $loglevel
     *
     * @param $loglevel int
     *
     * @return array
     */
    public function getLoggableKeyValues(int $loglevel = 1)
    {
        if ($loglevel <= 0) {
            return [];
        }
        $out = [];
        foreach ($this->_data as $key => $value) {
            if ($value["l"] == $loglevel) {
                $out[$key] = $value["v"];
            }
        }
        return $out;
    }
}