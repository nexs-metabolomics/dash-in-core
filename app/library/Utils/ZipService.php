<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use ZipArchive;

class ZipService extends ComponentRoot
{

    protected $_zip = null;

    public function __construct()
    {

        $this->_zip = new ZipArchive();
    }

    public function create($zipPath)
    {

        $this->_zip->open($zipPath, ZipArchive::CREATE);
    }

    public function addFile($path)
    {

        ZipArchive::FL_ENC_UTF_8;
        $this->_zip->addFile($path);
    }

    public function addFromString($name, $content, $flags)
    {

        $this->_zip->addFromString($name, $content, $flags);
    }

}