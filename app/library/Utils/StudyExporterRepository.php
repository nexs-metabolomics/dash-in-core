<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use Phalcon\Db\Enum;

class StudyExporterRepository extends ComponentRoot
{
    public function checkDbversion($dbversion)
    {
        try {
            $sql = StudyExporterQuery::$dbversion_greater_or_equal;
            

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dbversion"=>$dbversion,
            ]);

            if ($result) {
                $result["minimum_version"] = $dbversion;
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get minimal info on a study
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyInfo($studyId)
    {
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        try {
            $sql = StudyExporterQuery::$dashin_get_study_info;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"  => $studyId,
                "owner_org" => $organizationId,
            ]);
            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            error_log("exportCopyDataToSchema: ".$e->getMessage());
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function exportCopyDataToSchema($studyId)
    {
        try {
            $sql = StudyExporterQuery::$export_copy_data_to_schema;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id"      => $studyId,
                "source_schema" => "dashin",
            ]);

            if ($result and $result["status"]) {
                return $result["status_text"];
            }

        } catch (\PDOException $e) {
            error_log("exportCopyDataToSchema: ".$e->getMessage());
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }
    
    public function exportDropSchema($schemaname)
    {
        try {
            $sql = StudyExporterQuery::$export_drop_schema;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "schemaname" => $schemaname,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    public function exportSupportfiles($studyId)
    {

        try {
            $sql = StudyExporterQuery::$export_supportfile_paths;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            error_log("exportSupportfiles: ".$e->getMessage());
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Generic function to return named sql-queries for wholestudy export
     *
     * @param $queryname
     * @param $schemaname
     *
     * @return false|mixed
     */
    private function _returnExportTableSql($queryname, $schemaname)
    {

        try {
            $sql = StudyExporterQuery::${"export_{$queryname}_sql"};
            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "schemaname" => $schemaname
            ]);

            if (isset($result["sqlquery"])) {
                return $result["sqlquery"];
            }

        } catch (\PDOException $e) {
            error_log("_returnExportTableSql: ".$e->getMessage());
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * wholestudy export to zip-file
     *
     * @param $schemaname
     * @param $queryname
     *
     * @return array|false
     */
    public function exportSqdr($schemaname, $queryname)
    {

        try {

            $sql = $this->_returnExportTableSql($queryname, $schemaname);

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}