<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class CsvFile extends ComponentRoot
{

    /**
     * @var
     */
    private \SplFileObject $file;

    /**
     * @var string $separator;
     */
    private string $separator = '';

    public function __construct($file)
    {

        $this->file = new \SplFileObject($file);
    }

    public function getSeparator($sep = null)
    {

        if (!$this->separator) {
            $sep = $this->guessSeparator($sep);
            if (strlen($sep) != 1) {
                if (strlen($sep) == 0) {
                    throw new CsvFileException("Empty separator");
                } else{
                    throw new CsvFileException("Invalid separator");
                }
            }
            $this->separator = $sep;
        }

        return $this->separator;
    }

    /**
     * Validate separator
     *
     * Check that the $numRows have the same number of
     * columns with the given separator
     *
     * @param                $sep     string
     * @param                $numRows int
     *
     * @return false|mixed
     */
    public function validateSeparator(string $sep, int $numRows = 10)
    {

        $this->file->rewind();
        for ($i = 0; $i < $numRows; ++$i) {
            # get number of columns in (up to) first $numRows rows

            # if last line is a single newline then stop early
            # if last line is a dataline followed by a newline - don't stop
            $x = $this->file->fgetcsv($sep);
            $c = $this->file->current();
            if ($this->file->eof() && !$c) {
                break;
            }
            $nCols[$i] = count($x);
        }
        $this->file->rewind();

        # do all rows have the same number of columns?
        $min = min($nCols);
        $max = max($nCols);
        $res = ($min === $max) ? $min : false;

        return $res;
    }

    /**
     * Try to determine the seperator from data
     *
     * returns selected separator or false
     *
     * @param null $sep if $sep == null try to determine sep
     *
     * @return bool|string
     */
    public function guessSeparator($sep = null)
    {

        # list of separators to guess
        $separators = [",", ";", "\t", " "];

        # check selected separator (if any)
        if (in_array($sep, $separators)) {
            # validate by counting number of resulting columns
            $res = $this->validateSeparator($sep);
            if ($res > 1) {
                # return first likely correct sep
                return $sep;
            }
        } else {
            # loop through separators to search for likely correct separator
            foreach ($separators as $sep) {
                # returns
                #   false:  unequal number of columns
                #   1:      one column in all rows
                #   1 < x:  equal of columns (number more than 1) in all rows
                $res = $this->validateSeparator($sep);
                if ($res > 1) {
                    # return first likely correct sep
                    return $sep;
                }
            }
        }
    }

    /**
     * Return first row as header or false if first header is not all string
     *
     * @param                $sep
     *
     * @return array|false|void
     */
    public function getHeader($sep = null)
    {

        try {

            $sep = $this->getSeparator($sep);
            if ($sep) {
                $this->file->rewind();
                $header = $this->file->fgetcsv($sep);

                # if header is not all string => header=FALSE
                foreach ($header as $field) {
                    if (!is_string($field)) {
                        return false;
                    }
                }

                return $header;
            }
        } catch (\Exception $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Try to guess if the csv-file has a header
     *
     * If any field in the first row can be converted
     * to a number, it is assumed that the first row
     * is not a header
     *
     * @param $sep
     *
     * @return bool
     */
    public function guessHeader($sep = null)
    {

        $header = $this->getHeader($sep);
        if(!$header) {
            return false;
        }
        foreach ($header as $hcol) {
            if (is_numeric((string)$hcol)) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     *
     * @param $currentPos int|null
     * @param $limit      int|null
     * @param $searchTerm string|null
     *
     * @return object
     */
    public function paginate(?int $currentPos, ?int $limit, ?string $searchTerm = "")
    {

        $currentPos = (int)$currentPos;
        $limit = (int)$limit;

        $header = $this->getHeader($this->file);
        if(!$header) {
            return false;
        }

        if (strlen($searchTerm) > 0) {
            # regular expression
            # however: mb_ereg_match always matches from start of string
            if (str_starts_with($searchTerm, "^")) {
                $searchTermRepl = str_replace([".", "*"], ["\.", ".*"], $searchTerm);
            } else {
                $searchTermRepl = ".*" . str_replace([".", "*"], ["\.", ".*"], $searchTerm);
            }

            $newHeader = [];
            foreach ($header as $item) {
                $matched = mb_ereg_match($searchTermRepl, $item);
                if ($matched) {
                    $newHeader[] = $item;
                }
            }
            $header = $newHeader;
        }

        $totalItems = count($header);

        #---------------------------------------------------------
        # offset >=0
        #---------------------------------------------------------
        if ($currentPos < 1) {
            $currentPos = 1;
        } elseif ($currentPos > $totalItems) {
            $currentPos = $totalItems;
        }

        #---------------------------------------------------------
        # number of entries pr chunk >=1
        #---------------------------------------------------------
        $limit = ($limit < 1) ? 1 : $limit;

        #---------------------------------------------------------
        # total number of chunks
        #---------------------------------------------------------
        $lastChunkEntries = $totalItems % $limit;
        $totalChunks = intdiv(floatval($totalItems), floatval($limit));
        $totalChunks = ($lastChunkEntries > 0) ? $totalChunks + 1 : $totalChunks;

        if ($currentPos > $totalChunks) {
            $currentPos = $totalChunks;
        }

        # before
        $before = ($currentPos > 1) ? $currentPos - 1 : 1;

        # next
        $next = ($currentPos < $totalChunks) ? $currentPos + 1 : $totalChunks;

        # offset
        $offset = ($currentPos - 1) * $limit;
        if ($offset >= ($totalItems) && $totalItems > 0) {
            $offset = $totalItems - 1;
        } elseif ($offset < 0 || $totalItems <= 0) {
            $offset = 0;
        }

        # first and last item on page (eg: row a to b of n)
        $pageFirst = $offset + 1;
        $pageLast = $offset + $limit;
        if ($pageLast > $totalItems) {
            $pageLast = $totalItems;
        }

        $itemElements = array_slice($header, $offset, $limit);
        $items = [];
        foreach ($itemElements as $element) {
            $items[] = (object)["name" => $element];
        }

        echo "";

        return (object)[
            "items"           => $items,
            "offset"          => $offset,
            "first"           => 1,
            "before"          => $before,
            "previous"        => $before,
            "current"         => $currentPos,
            "last"            => $totalChunks,
            "next"            => $next,
            "total_pages"     => $totalChunks,
            "total_items"     => $totalItems,
            "limit"           => $limit,
            "page_item_first" => $pageFirst,
            "page_item_last"  => $pageLast,
        ];
    }

}