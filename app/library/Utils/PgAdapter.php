<?php

namespace App\Library\Utils;

use Phalcon\Db\Adapter\Pdo\Postgresql;

class PgAdapter extends Postgresql
{

    /**
     * @return \PDO
     */
    public function pgsqlCopyFromArray(string $tableName, array $rows, string $separator = "\t", string $nullAs = "\\\\N", ?string $fields = null)
    {
        return $this->pdo->pgsqlCopyFromArray(
            $tableName,
            $rows,
            $separator,
            $nullAs,
            $fields
        );
    }
    
    public function pgsqlGetPid()
    {
        return $this->pdo->pgsqlGetPid();
    }

}