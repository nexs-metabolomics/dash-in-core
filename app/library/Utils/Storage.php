<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class Storage extends ComponentRoot
{

    /**
     * Indicating whether this system has storage
     *
     * @var bool
     */
    protected bool $_hasStorage = false;

    /**
     * Path to storage location
     *
     * @var string|false|null
     */
    protected string $_storageRoot;

    /**
     * Indicating whether this system has temporary directory
     *
     * @var bool
     */
    protected bool $_hasTempdir = false;

    /**
     * Path to temporary directory
     *
     * @var string|false|null
     */
    protected string $_tempdir;

    /**
     * constructor
     */
    public function __construct()
    {

        $this->_tempdir = $this->config->tempdir;

        $this->_storageRoot = mb_ereg_replace("/+$", "", $this->config->storage);

        if (mb_strlen($this->_storageRoot) === 0) {

            $this->flashSession->warning("Storage location is not correctly configured.");

        } else {

            # ensure trailing "/"
            if (!str_ends_with($this->_storageRoot, "/")) {

                $this->_storageRoot = $this->_storageRoot . "/";

            } elseif (str_ends_with($this->_storageRoot, "//")) {

                $this->_storageRoot = mb_ereg_replace("/+$", "/", $this->_storageRoot);

            }

            if (is_dir($this->_storageRoot) && is_writeable($this->_storageRoot)) {
                $this->_hasStorage = true;
            }

        }
        if (mb_strlen($this->_tempdir) === 0) {

            $this->flashSession->warning("Storage location is not correctly configured.");

        } else {

            # ensure trailing "/"
            if (!str_ends_with($this->_tempdir, "/")) {

                $this->_tempdir = $this->_tempdir . "/";

            } elseif (str_ends_with($this->_tempdir, "//")) {

                $this->_tempdir = mb_ereg_replace("/+$", "/", $this->_tempdir);
            }

            if (is_dir($this->_tempdir) && is_writeable($this->_tempdir)) {
                $this->_hasTempdir = true;
            }
        }
    }

    /**
     * Returns the root storage location of this system
     * of false if it does not exist
     *
     * @return false|string|null
     */
    public function getStorageRoot()
    {

        if (!$this->hasStorage()) {
            $this->flashSession->error("Storage is not correctly configured.");

            return false;
        }

        return $this->_storageRoot;
    }

    /**
     * Returns location of temporary directory
     * throws exception if it doesn't exist
     *
     * @throws StorageException
     */
    public function getTempdirLocation()
    {

        if (!$this->hasTempdir()) {
            $this->flashSession->error("Tempdir is not correctly configured.");

            return false;
        }

        return $this->_tempdir;
    }

    /**
     * Returns true if this system have a valid storage location
     *
     * @return bool
     */
    public function hasStorage()
    {

        return $this->_hasStorage;
    }

    /**
     * Returns true if this system has a valid temporary directory
     *
     * @return bool
     */
    public function hasTempdir()
    {

        return $this->_hasTempdir;
    }

    /**
     * Return storage location for the type (organization or admin)
     *
     *
     * @param $params array [object-id,type]
     *
     * @return false|string
     */
    public function getStorageLocation($params)
    {

        $storageRoot = $this->getStorageRoot();
        if (strlen($storageRoot) === 0) {
            return false;
        }

        $storageLocation = false;

        $type = $params["type"];
        if ($type === "study") {
            $storageLocation = $this->getStorageLocationStudyRelated($params,$storageRoot);
        } elseif ($type === "dataset") {
            $storageLocation = $this->getStorageLocationDatasetRelated($params,$storageRoot);
        } elseif ($type === "assay") {
            $storageLocation = $this->getStorageLocationAssayRelated($params,$storageRoot);
        } else {
            $this->flashSession->error("Unknow type '$type'");
        }

        # validate or create storage location
        if (is_dir($storageLocation)) {
            if (is_writeable($storageLocation)) {

                # dir exist and is writable
                return $storageLocation;
            } else {
                # dir exists but is not writable
            }
        } else {
            if (mkdir($storageLocation, recursive: true)) {
                if ((is_writeable($storageLocation))) {

                    # dir created and is writable
                    return $storageLocation;
                } else {
                    # dir created but not writable
                }
            } else {
                # dir not created
            }
        }

        return false;
    }

    /**
     * Returns the storage location for a particular organization
     * or false if a storage location does not exist
     *
     * @param $storageRoot
     * @param $organizationId
     *
     * @return false|string
     */
    public function getOrganizationStorageLocation($storageRoot, $organizationId)
    {

        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }

        return "$storageRoot/organizations/$organizationId";

    }

    /**
     * Return storage location for files related to assay
     *
     * @param $params
     * @param $storageRoot
     *
     * @return false|string
     */
    public function getStorageLocationAssayRelated($params, $storageRoot)
    {

        $assayId = $params["assay_id"] ?? "";
        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }

        return "$storageRoot/admin/assays/$assayId/supportfiles";
    }

    /**
     * Return storage location for files related to study
     *
     * @param $params
     * @param $storageRoot
     *
     * @return false|string
     */
    public function getStorageLocationStudyRelated($params, $storageRoot)
    {

        $organizationId = $params["organization_id"] ?? "";
        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        $studyId = $params["study_id"];
        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        $locationPattern = $this->storage_location_pattern->dataset;

        return "$storageRoot/organizations/$organizationId/studies/$studyId/supportfiles";
    }

    /**
     * Return storage location for files related to study
     *
     * @param $params
     * @param $storageRoot
     *
     * @return false|string
     */
    public function getStorageLocationDatasetRelated($params, $storageRoot)
    {

        $organizationId = $params["organization_id"] ?? "";
        if (!UniqueId::uuidValidate($organizationId)) {
            return false;
        }
        $datasetId = $params["dataset_id"];
        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        $locationPattern = $this->storage_location_pattern->dataset;

        return "$storageRoot/organizations/$organizationId/datasets/$datasetId/supportfiles";
    }

}