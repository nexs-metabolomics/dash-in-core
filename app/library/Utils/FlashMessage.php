<?php


namespace App\Library\Utils;

use Phalcon\Flash\Session;

class FlashMessage extends Session
{
    private function _showMessage($messages)
    {
        $msg = "";
        foreach ($messages as $message) {
            $msg .= "$message\n";
        }
        return $msg;
    }

    private function _getCss($type)
    {
        $css = (isset($this->_cssClasses[$type]) ? $this->_cssClasses[$type] : "");
        return $css;
    }

    /**
     * Error
     * 
     * @return bool
     */
    public function hasError()
    {
        return $this->has("error");
    }

    public function showError($remove = true)
    {
        return $this->_showMessage($this->getMessages("error", $remove));
    }

    public function getErrorCss()
    {
        return $this->_getCss("error");
    }

    /**
     * Notice
     * 
     * @return bool
     */
    public function hasNotice()
    {
        return $this->has("notice");
    }

    public function showNotice($remove = true)
    {
        return $this->_showMessage($this->getMessages("notice", $remove));
    }

    public function getNoticeCss()
    {
        return $this->_getCss("notice");
    }

    /**
     * Warning
     * 
     * @return bool
     */
    public function hasWarning()
    {
        return $this->has("warning");
    }

    public function showWarning($remove = true)
    {
        return $this->_showMessage($this->getMessages("warning", $remove));
    }

    public function getWarningCss()
    {
        return $this->_getCss("warning");
    }

    /**
     * Success
     * 
     * @return bool
     */
    public function hasSuccess()
    {
        return $this->has("success");
    }

    public function showSuccess($remove = true)
    {
        return $this->_showMessage($this->getMessages("success", $remove));
    }

    public function getSuccessCss()
    {
        return $this->_getCss("success");
    }

    /**
     * Message
     * 
     * @return bool
     */
    public function hasMessage()
    {
        $msg = $this->getMessages(null,false);
        if($msg) {
            return true;
        }
        return false;
    }

    public function showMessage($remove = true)
    {
        $msg = $this->showError($remove);
        $msg .= $this->showWarning($remove);
        $msg .= $this->showNotice($remove);
        $msg .= $this->showSuccess($remove);
        return $msg;
    }

    public function getMessageCss()
    {
        if ($this->hasError() && isset($this->_cssClasses["error"])) {
            return $this->_cssClasses["error"];
        } elseif ($this->hasWarning() && isset($this->_cssClasses["warning"])) {
            return $this->_cssClasses["warning"];
        } elseif ($this->hasNotice() && isset($this->_cssClasses["notice"])) {
            return $this->_cssClasses["notice"];
        } elseif ($this->hasSuccess() && isset($this->_cssClasses["success"])) {
            return $this->_cssClasses["success"];
        }
    }

}