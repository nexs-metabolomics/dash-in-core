<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\Utils;


class Url extends \Phalcon\Mvc\Url
{

    /**
     * Add 'fake' timestamp to the end of css files
     * 
     * @param null $uri
     * @return mixed|string
     */
    public function getStaticCss($uri = null)
    {
        $file = parent::getStatic($uri);
        if(strpos($file, '/') !== 0 || !isset($_SERVER['DOCUMENT_ROOT']) || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file)) {
            return $file;
        }

        $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
        $file = preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
        return $file;
    }
}