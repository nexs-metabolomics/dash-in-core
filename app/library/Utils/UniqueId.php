<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace App\Library\Utils;


use App\Library\ApplicationRoot\ComponentRoot;

class UniqueId extends ComponentRoot
{
    const CHARS = "ABCDEFGHIJKLMINOPQRSTUVWXYZ";
    const chars = "abcdefghijklminopqrstuvwxyz";
    const digits = "0123456789";
    const uuid_regex_pattern = '/^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$/i';

    /**
     * @param $string
     * @return mixed
     */
    public static function randomchar($string)
    {
        return $string[mt_rand(0,strlen($string)-1)];
    }

    /**
     * Return a UUID
     * 
     * @param string $v version
     * @param string $d domain
     * @param string $s
     * @return string
     */
    /**
     * @param string $v
     * @param string $d
     * @param string $s
     * @return false|string
     */
    public static function uuid(string $v, $d = '', $s = '')
    {
        $u = hash($v ^ 3 ? 'sha1' : 'md5', $v ^ 4 ? $d . $s : gmp_strval(gmp_random(4)));
        $u[12] = $v;
        $u[16] = dechex(+"0x$u[16]" & 3 | 8);
        return substr(preg_replace('/^.{8}|.{4}/', '\0-', $u, 4), 0, 36);
    }

    /**
     * Returns a UUID string version 4
     * using the gmp_random function
     *
     * @param bool $noDash
     * @return string
     */
    public static function uuid4($noDash=false)
    {
        if($noDash) {
            $d = bin2hex(openssl_random_pseudo_bytes(16));
            $d[12] = "4";
            $d[16] = dechex(hexdec($d[16]) & 3 | 8);
        } else {
            $d = bin2hex(openssl_random_pseudo_bytes(18));
            $d[8] = "-";
            $d[14] = "4";
            $d[13] = "-";
            $d[18] = "-";
            $d[19] = dechex(hexdec($d[19]) & 3 | 8);
            $d[23] = "-";
        }
        return $d;
    }

    /**
     * Returns a v1 time based UUID
     * 
     * @param bool $noDash
     * @return string
     */
    public static function uuid1mc($noDash=false)
    {
        #---------------------------------------------------------------------
        # 0x01b21dd213814000: time difference between uuid-base (1582-10-15)
        # and unix-base 1970-01-01 in 100ns ticks
        # microtime * 1000000: convert microtime to 100ns ticks
        # 0x1000: version bits
        #---------------------------------------------------------------------

        # Convert microtime to uuid-time-base
        $t = (int) (microtime(true) * 10000000) + 0x01b21dd213814000;

        # time_low: extract last 32 bits
        $t1 = ($t & 0xffffffff);
        # time_mid: extract next 16 bits
        $t2 = (($t >> 32) & 0xffff);
        # time_high_version: extract first 12 bits, add/prepend version number
        $t3 = ((($t >> 48) & 0x0fff) | 0x1000);

        # clock_sequence:
        # Random clock_sequence/simulated MAC address
        $r1x = hexdec(bin2hex(openssl_random_pseudo_bytes(2)));
        # clock_sequence_low: first bits must be 10:
        # (x1 = x0 | 0x8000 -> 1), (x2 = x1 & 0xbfff -> 0)
        $r1 = ((($r1x & 0xffff) | 0x8000) & 0xbfff);

        # node: set IEEE802 unicast/multicast bit 0x3... (local/global bit)
        $r2x = hexdec(bin2hex(openssl_random_pseudo_bytes(6)));
        $r2 = ($r2x & 0xffffffffffff) | 0x300000000000;

        # if generating too fast, increase clock_sequence by 1
        if($noDash) {
            $u = sprintf("%08x%04x%04x%04x%012x",$t1,$t2,$t3,$r1,$r2);
        } else {
            $u = sprintf("%08x-%04x-%04x-%04x-%012x",$t1,$t2,$t3,$r1,$r2);
        }

        return $u;
    }

    /**
     * Validate a UUID
     * 
     * @param $id
     * @return int
     */
    public static function uuidValidate($id)
    {
        $uuid_ok = preg_match(UniqueId::uuid_regex_pattern,$id);
        return ($uuid_ok);
    }

    /**
     * @param $len
     * @param $chars
     * @return string
     */
    public static function randomChars($len, $chars = null)
    {
        if(null === $chars) {
            $chars = UniqueId::chars.UniqueId::CHARS.UniqueId::digits;
        }
        $out = '';
        for ($i = 0; $i < $len; ++$i) {
            $out .= self::randomchar($chars);
        }
        return $out;
    }

    /**
     * @param int $len
     * @return string
     */
    public static function generatepassword($len=8)
    {
        $out = '';
        for($i=0;$i<$len;++$i) {
            $out .= self::randomchar(self::CHARS.self::chars.self::digits.self::digits.self::digits);
        }
        return $out;
    }

    /**
     * @param string $prefix
     * @param int $nchars
     * @param int $ndigits
     * @param string $sep
     * @return string
     */
    public static function accountname($prefix="",$nchars=3,$ndigits=3,$sep='_')
    {
        return $prefix.self::randomChars($nchars,self::CHARS).$sep.self::randomChars($ndigits,self::digits);
    }

    /**
     * Returns a random uppercase code in this pattern:
     * <$pre><$num_char><$sep><$num_digits>, (e.g. USER_XQRT-4097)
     * @param string $pre
     * @param string $sep
     * @param integer $num_chars
     * @param integer $num_digits
     * @return string
     */
    public static function usercode($pre="",$sep="",$num_chars=4,$num_digits=4)
    {
        $out = $pre;
        $out .= self::randomChars($num_chars,self::CHARS);
        $out .= $sep;
        $out .= self::randomChars($num_digits,self::digits);
        return $out;
    }
    public static function usercode2($pre="",$sep="",$num_chars=4,$num_digits1=4,$num_digits2=4)
    {
        $out = $pre;
        $out .= self::randomChars($num_chars,self::CHARS);
        $out .= $sep;
        $out .= self::randomChars($num_digits1,self::digits);
        $out .= $sep;
        $out .= self::randomChars($num_digits2,self::digits);
        return $out;
    }
}