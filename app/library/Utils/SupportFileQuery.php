<?php

namespace App\Library\Utils;

class SupportFileQuery
{

    static string $get_study_owner_organization_id =
        <<<'EOD'
            SELECT owner_org AS organization_id
            FROM dashin.study
            WHERE study_id = :study_id;
        EOD;

    static string $get_supportfile_info =
        <<<'EOD'
            SELECT organization_id
                 , filename
                 , mimetype
                 , name
                 , fileextension
                 , description
            FROM dashin.supportfile
            WHERE supportfile_id = :supportfile_id;
        EOD;

    static string $get_study_info =
        <<<'EOD'
            SELECT name
                 , title
                 , description
                 , owner_org AS organization_id
            FROM dashin.study
            WHERE study_id = :study_id;
        EOD;

    static string $get_dataset_info =
        <<<'EOD'
            SELECT name
                 , description
                 , owner_org AS organization_id
            FROM dashin.dataset
            WHERE dataset_id = :dataset_id;
        EOD;

    /**
     * Register a supporting file linked to a study
     *
     * @var string $register_study_supportfile
     */
    static string $register_study_supportfile =
        <<<'EOD'
            WITH
              register_file_query   AS (
                                         INSERT
                                           INTO dashin.supportfile (organization_id, name, filename, fileextension, mimetype)
                                             SELECT :organization_id
                                                  , :name
                                                  , :filename
                                                  , :fileextension
                                                  , :mimetype
                                             WHERE exists
                                               (
                                                 SELECT *
                                                 FROM dashin.study
                                                 WHERE (study_id, owner_org) = (:study_id, :organization_id)
                                               )
                                             RETURNING supportfile_id
                                       
                                       )
              , link_to_study_query AS (
                                         INSERT INTO dashin.study_x_supportfile (study_id, supportfile_id)
                                           SELECT :study_id::UUID
                                                , supportfile_id
                                           FROM register_file_query
                                           WHERE supportfile_id IS NOT NULL
                                           ON CONFLICT (study_id,supportfile_id) DO NOTHING
                                           RETURNING supportfile_id
                                       )
            SELECT supportfile_id
            FROM link_to_study_query;
        EOD;

    static string $delete_supportfile_from_study =
        <<<'EOD'
            DELETE
            FROM dashin.supportfile a
            WHERE (a.supportfile_id, a.organization_id) = (:supportfile_id, :organization_id)
              AND exists
              (
                SELECT
                FROM dashin.study_x_supportfile z
                WHERE (z.study_id, z.supportfile_id) = (:study_id, :supportfile_id)
              );
        EOD;

    static string $get_study_supportfile_list =
        <<<'EOD'
            SELECT a.supportfile_id
               , a.organization_id
               , a.name
               , a.description
               , to_char(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') AS last_update
            FROM dashin.supportfile                 a
              INNER JOIN dashin.study_x_supportfile b ON a.supportfile_id = b.supportfile_id
              INNER JOIN dashin.study               d ON b.study_id = d.study_id
            WHERE a.organization_id = :organization_id
              AND b.study_id = :study_id
        EOD;

    static string $get_supportfile_list =
        <<<'EOD'
            WITH
              main_query             AS (
                                          SELECT a.supportfile_id
                                               , a.organization_id
                                               , a.name
                                               , d.name                                         AS studyname
                                               , to_char(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') AS last_update
                                               , d.study_id
                                          FROM dashin.supportfile                a
                                            LEFT JOIN dashin.study_x_supportfile b ON a.supportfile_id = b.supportfile_id
                                            LEFT JOIN dashin.study               d ON b.study_id = d.study_id
                                          WHERE a.organization_id = :organization_id
                                            AND b.study_id = :study_id
                                        )
              , relatedstudy_query   AS (
                                          SELECT a.supportfile_id
                                               , c.study_id           AS related_study_id
                                               , c.name               AS related_studyname
                                               , row_number() OVER () AS rn_study
                                          FROM main_query                         a
                                            INNER JOIN dashin.study_x_supportfile b ON a.supportfile_id = b.supportfile_id
                                            INNER JOIN dashin.study               c ON b.study_id = c.study_id
                                          WHERE b.study_id != a.study_id
                                        )
              , relateddataset_query AS (
                                          SELECT a.supportfile_id
                                               , b.dataset_id
                                               , c.name AS datasetname
                                          FROM main_query                           a
                                            INNER JOIN dashin.dataset_x_supportfile b ON a.supportfile_id = b.supportfile_id
                                            INNER JOIN dashin.dataset               c ON b.dataset_id = c.dataset_id
                                        )
              , combineall_query     AS (
                                          SELECT a.supportfile_id
                                               , a.name
                                               , a.studyname
                                               , a.last_update
                                               , b.related_studyname
                                               , c.datasetname
                                               , a.study_id
                                               , b.related_study_id
                                               , b.rn_study
                                               , c.dataset_id
                                               , a.organization_id
                                          FROM main_query                  a
                                            LEFT JOIN relatedstudy_query   b
                                                        ON a.supportfile_id = b.supportfile_id
                                            LEFT JOIN relateddataset_query c ON a.supportfile_id = c.supportfile_id
                                        )
            SELECT a.supportfile_id
                 , a.study_id
                 , a.name
                 , a.studyname
                 , jsonb_agg(jsonb_build_object('study_id',related_study_id,'studyname',related_studyname)) FILTER ( WHERE related_study_id IS NOT NULL)  AS related_study_list
            FROM combineall_query a
            GROUP BY a.supportfile_id
                   , a.study_id
                   , a.studyname
                   , a.name;
        EOD;

    static string $update_supportfile_description =
        <<<'EOD'
            UPDATE dashin.supportfile
            SET (description, updated_at) = (:description,now())
            WHERE (supportfile_id, organization_id) = (:supportfile_id, :organization_id);
        EOD;

    #--------------------------------------------------------------------------
    # dataset
    #--------------------------------------------------------------------------
    static string $register_dataset_supportfile =
        <<<'EOD'
            WITH
              register_file_query     AS (
                                           INSERT
                                             INTO dashin.supportfile (organization_id, name, filename, fileextension, mimetype)
                                               SELECT :organization_id
                                                    , :name
                                                    , :filename
                                                    , :fileextension
                                                    , :mimetype
                                               WHERE exists
                                                 (
                                                   SELECT *
                                                   FROM dashin.dataset
                                                   WHERE (dataset_id, owner_org) = (:dataset_id, :organization_id)
                                                 )
                                               RETURNING supportfile_id
                                         
                                         )
              , link_to_dataset_query AS (
                                           INSERT INTO dashin.dataset_x_supportfile (dataset_id, supportfile_id)
                                             SELECT :dataset_id::UUID
                                                  , supportfile_id
                                             FROM register_file_query
                                             WHERE supportfile_id IS NOT NULL
                                             ON CONFLICT (dataset_id,supportfile_id) DO NOTHING
                                             RETURNING supportfile_id
                                         )
            SELECT supportfile_id
            FROM link_to_dataset_query;
        EOD;

    static string $get_dataset_supportfile_list =
        <<<'EOD'
            SELECT a.supportfile_id
               , a.organization_id
               , a.name
               , a.description
               , to_char(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') AS last_update
            FROM dashin.supportfile                   a
              INNER JOIN dashin.dataset_x_supportfile b ON a.supportfile_id = b.supportfile_id
              INNER JOIN dashin.dataset               d ON b.dataset_id = d.dataset_id
            WHERE a.organization_id = :organization_id
              AND b.dataset_id = :dataset_id
        EOD;

    static string $delete_supportfile_from_dataset =
        <<<'EOD'
            DELETE
            FROM dashin.supportfile a
            WHERE (a.supportfile_id, a.organization_id) = (:supportfile_id, :organization_id)
              AND exists
              (
                SELECT
                FROM dashin.dataset_x_supportfile z
                WHERE (z.dataset_id, z.supportfile_id) = (:dataset_id, :supportfile_id)
              );
        EOD;

    #--------------------------------------------------------------------------
    # assay
    #--------------------------------------------------------------------------
    static string $register_assay_supportfile =
        <<<'EOD'
            WITH
              register_file_query     AS (
                                           INSERT
                                             INTO dashin.supportfile (organization_id, name, filename, fileextension, mimetype)
                                               SELECT :organization_id
                                                    , :name
                                                    , :filename
                                                    , :fileextension
                                                    , :mimetype
                                               RETURNING supportfile_id
                                         
                                         )
              , link_to_dataset_query AS (
                                           INSERT INTO dashin.assay_x_supportfile (assay_id, supportfile_id)
                                             SELECT :assay_id::UUID
                                                  , supportfile_id
                                             FROM register_file_query
                                             WHERE supportfile_id IS NOT NULL
                                             ON CONFLICT (assay_id,supportfile_id) DO UPDATE SET supportfile_id = dashin.assay_x_supportfile.supportfile_id
                                             RETURNING supportfile_id
                                         )
            SELECT supportfile_id
            FROM link_to_dataset_query;
        EOD;

    static string $delete_supportfile_from_assay =
        <<<'EOD'
            DELETE
            FROM dashin.supportfile a
            WHERE (a.supportfile_id, a.organization_id) = (:supportfile_id, :organization_id)
              AND exists
              (
                SELECT
                FROM dashin.assay_x_supportfile z
                WHERE (z.assay_id, z.supportfile_id) = (:assay_id, :supportfile_id)
              );
        EOD;

    static string $get_assay_supportfile_list =
        <<<'EOD'
            SELECT a.supportfile_id
                 , a.organization_id
                 , a.name
                 , a.description
                 , to_char(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') AS last_update
            FROM dashin.supportfile                   a
              INNER JOIN dashin.assay_x_supportfile b ON a.supportfile_id = b.supportfile_id
              INNER JOIN dashin.assay               d ON b.assay_id = d.assay_id
            WHERE a.organization_id = :organization_id
              AND b.assay_id = :assay_id;
        EOD;

}
