<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use ZipArchive;

class StudyExporter extends ComponentRoot
{

    /**
     * @var StudyExporterProperty|null
     */
    protected ?StudyExporterProperty $_tableProperties = null;

    private function _getTableProperties($name)
    {
        if (!is_a($this->_tableProperties, 'StudyExporterProperty')) {
            $this->_tableProperties = new StudyExporterProperty();
        }
    }

    /**
     * @var StudyExporterRepository|null
     */
    protected ?StudyExporterRepository $_repository = null;

    /**
     * @return StudyExporterRepository
     */
    private function _getRepository()
    {
        if (!is_a($this->_repository, "StudyExporterRepository")) {
            $this->_repository = new StudyExporterRepository();
        }

        return $this->_repository;
    }

    /**
     * Extract entry from array as header for csv/dataset
     *
     * @param $array
     *
     * @return int[]|string[]
     */
    private function _getHeader($array)
    {

        $header = array_keys($array[0]);

        return $header;
    }

    /**
     * Add a table from export schema to export zip-file
     *
     * @param $queryName
     * @param $internalPath
     * @param $schemaname
     * @param $zip
     * @param $CSV
     *
     * @return void
     */
    private function _addTableToZip($queryName, $internalPath, $schemaname, &$zip, $CSV)
    {

        $data = $this->_getRepository()->exportSqdr($schemaname, $queryName);
        if (!$data) {
            return;
        }
        if ($data) {
            $header = $this->_getHeader($data);
            $dataCsv = $CSV->arrayToCsv($data, ",", $header);
            $zip->addFromString($internalPath, $dataCsv);
        }

    }

    /**
     * Add supportfile to export zip-file
     *
     * @param            $studyId
     * @param ZipArchive $zip
     *
     * @return void
     */
    private function _addSupportfilesToZip($studyId, \ZipArchive &$zip)
    {

        $storageRoot = $this->storage->getStorageRoot();
        $result = $this->_getRepository()->exportSupportfiles($studyId);
        if (!$result) {
            return;
        }
        foreach ($result as $finfo) {
            $path = "$storageRoot/$finfo[path]";
            $internalPath = "supportfiles/$finfo[name]";
            $zip->addFile($path, $internalPath);
        }

    }

    /**
     * Checks if the database is of the required minimum version
     *
     * @return array|false
     */
    public function checkDbversion()
    {
        $ok = $this->_getRepository()->checkDbversion("2024-11-30");

        return $ok;
    }

    /**
     * Export whole study to sqdr-format zip-file
     *
     * @param $studyId
     *
     * @return false|void
     * @throws StorageException
     */
    public function exportStudy($studyId)
    {

//        $schemaname = "studyexport_0a831053630d4b2585788986ea859307";

        $schemaname = $this->_getRepository()->exportCopyDataToSchema($studyId);
        if (!$schemaname) {
            $this->flashSession->error("There was an error retrieving data from the database");

            return false;
        }
        $dateStr = date('Ymd-His');

        $studyInfo = $this->_getRepository()->getStudyInfo($studyId);
        $zipOutName = substr($studyInfo["name"], 0, 100) . "_$dateStr";
        $zipOutNameClean = mb_ereg_replace('[^\p{L}\p{N}_-]', '_', $zipOutName) . ".sqdr";
        $zip = new ZipArchive();

        $tmpdir = $this->storage->getTempdirLocation();
        $tempZipFilePath = $tmpdir . "/" . UniqueId::uuid4();

        $zip->open($tempZipFilePath, ZipArchive::CREATE);
        $CSV = new Csv();

        #-----------------------------------------------------------------------
        # add version
        #-----------------------------------------------------------------------
        $zip->addFromString("version", "v1");

        #-----------------------------------------------------------------------
        # go through all tables
        #-----------------------------------------------------------------------
        $this->_addTableToZip("study", "data/study.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("dataset", "data/dataset.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("assay", "data/assay.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_datatable", "data/datadesign_datatable.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_center", "data/datadesign_x_study_center.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_event", "data/datadesign_x_study_event.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_samplingevent", "data/datadesign_x_study_samplingevent.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_samplingtime", "data/datadesign_x_study_samplingtime.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_startgroup", "data/datadesign_x_study_startgroup.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_subevent", "data/datadesign_x_study_subevent.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("datadesign_x_study_subject", "data/datadesign_x_study_subject.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_center", "data/study_center.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_event", "data/study_event.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_samplingevent", "data/study_samplingevent.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_samplingtime", "data/study_samplingtime.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_startgroup", "data/study_startgroup.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_subevent", "data/study_subevent.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("study_subject", "data/study_subject.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("studydesign_datatable", "data/studydesign_datatable.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("var_dataset", "data/var_dataset.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("var_datatable", "data/var_datatable.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("var_variable", "data/var_variable.csv", $schemaname, $zip, $CSV);
        $this->_addTableToZip("variable", "data/variable.csv", $schemaname, $zip, $CSV);

        #-----------------------------------------------------------------------
        # add support files
        #-----------------------------------------------------------------------
        $this->_addSupportfilesToZip($studyId, $zip);

        #-----------------------------------------------------------------------
        # cleanup
        #-----------------------------------------------------------------------
        $zip->close();
        if (!file_exists($tempZipFilePath)) {
            $this->flashSession->error("There was an error exporting data (tempfile deleted before export complete)");

            return false;
        }
        $zipfile = file_get_contents($tempZipFilePath);

        $this->_getRepository()->exportDropSchema($schemaname);

        $this->downloader->send($zipfile, $zipOutNameClean, "zip");

    }

    /**
     * Convert csv imported from sqdr-files to array
     * to be able to use psql-copy
     * 
     * @param $str
     * @param $header
     *
     * @return array
     */
    private function _csvToArray($str, $header = true)
    {
        $csvRows = explode("\n", trim($str));
        $datArr = [];
        if ($header) {
            $headerArr = str_getcsv($csvRows[0]);
            unset($csvRows[0]);
            foreach ($csvRows as $row) {
                $datArr[] = array_combine($headerArr, str_getcsv($row));
            }

        } else {
            foreach ($csvRows as $row) {
                $datArr[] = str_getcsv($row);
            }

        }

        return $datArr;
    }

    /**
     * FIXME: This function is under development
     * 
     * @param $uploadedFilePath
     *
     * @return void
     */
    public function importStudy($uploadedFilePath)
    {
        $dsc = $this->db->getDescriptor();
        $pid = $this->db->pgsqlGetPid();
        $pid = $this->db->pgsqlGetPid();

        $zip = new \ZipArchive();
        $zip->open($uploadedFilePath);
        $x = $zip->getFromName("version");
        $study = $zip->getFromName("data/study.csv");
        $dataset = $zip->getFromName("data/dataset.csv");
        $assay = $zip->getFromName("data/assay.csv");
        $datadesign_datatable = $zip->getFromName("data/datadesign_datatable.csv");

        $datadesign_x_study_center = $zip->getFromName("data/datadesign_x_study_center.csv");
        $datadesign_x_study_event = $zip->getFromName("data/datadesign_x_study_event.csv");
        $datadesign_x_study_samplingevent = $zip->getFromName("data/datadesign_x_study_samplingevent.csv");
        $datadesign_x_study_samplingtime = $zip->getFromName("data/datadesign_x_study_samplingtime.csv");
        $datadesign_x_study_startgroup = $zip->getFromName("data/datadesign_x_study_startgroup.csv");
        $datadesign_x_study_subevent = $zip->getFromName("data/datadesign_x_study_subevent.csv");
        $datadesign_x_study_subject = $zip->getFromName("data/datadesign_x_study_subject.csv");

        $study_center = $zip->getFromName("data/study_center.csv");
        $study_event = $zip->getFromName("data/study_event.csv");
        $study_samplingevent = $zip->getFromName("data/study_samplingevent.csv");
        $study_samplingtime = $zip->getFromName("data/study_samplingtime.csv");
        $study_startgroup = $zip->getFromName("data/study_startgroup.csv");
        $study_subevent = $zip->getFromName("data/study_subevent.csv");
        $study_subject = $zip->getFromName("data/study_subject.csv");
        $studydesign_datatable = $zip->getFromName("data/studydesign_datatable.csv");
        $var_dataset = $zip->getFromName("data/var_dataset.csv");
        $var_datatable = $zip->getFromName("data/var_datatable.csv");
        $var_variable = $zip->getFromName("data/var_variable.csv");
        $variable = $zip->getFromName("data/variable.csv");

        $zip->close();

    }

}