<?php


namespace App\Library\Utils;

use Phalcon\Encryption\Security as PhSecurity;

class Security extends PhSecurity
{
    protected $_alphabet = "0123456789abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ/{[]}_-&%#!;*";
    public function getRandomString($length=16)
    {
        $max = mb_strlen($this->_alphabet)-1;
        $random_string = "";
        try {
            for($i=0;$i<$length;$i++) {
                $random_string .= $this->_alphabet[random_int(0,$max)];
            }
            return $random_string;
        } catch (\Exception $e) {
            error_log($e);
            return false;
        }

    }
}