<?php
/*********************************************************************
 * 3Q Data Platform
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2016-02-27
 *
 * This file is part of the '3Q Data Platform' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
namespace App\Library\Utils;


use App\Library\ApplicationRoot\ComponentRoot;

class Utils extends ComponentRoot
{
    public static function copyDir($src,$dst,$mod=null) {
        $dir = opendir($src);
        $res = mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    self::copyDir($src . '/' . $file,$dst . '/' . $file,$mod);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                    if($mod) {
                        chmod($dst . '/' . $file,$mod);
                    }
                }
            }
        }
        closedir($dir);
    }

    /**
     * Sanitize uuid
     * 
     * @param $uuid
     * @return mixed|string
     */
    public static function sanitizeUuid($uuid)
    {
        # sanitize projectId
        $uuid = substr($uuid, 0, 45);
        $uuid = preg_replace('#[^a-fA-F0-9-]#', '', $uuid);
        return $uuid;
    }

    public static function preg_grep_keys($pattern, $input, $flags = 0)
    {
        return array_intersect_key($input, array_flip(preg_grep($pattern, array_keys($input), $flags)));

    }
    
    public static function rescale($x, $inMin, $inMax, $outMin, $outMax, $trunc=false)
    {
        $xOut = (($x-$inMin)/($inMax-$inMin) * ($outMax - $outMin)) + $outMin;
        if($trunc) {
            if($xOut>max([$outMax,$outMin])) {
                $xOut = max([$outMax,$outMin]);
            } elseif ($xOut<min([$outMax,$outMin])) {
                $xOut = min([$outMax,$outMin]);
            }
        }
        return $xOut;
    }

    public static function tscoreToFiveCat($x)
    {
        if($x < 30) {

            return 1;

        } elseif (30 <= $x && $x < 42) {

            return 2;

        }elseif (42 <= $x && $x < 59) {

            return 3;

        }elseif (59 <= $x && $x < 71) {

            return 4;

        }

        return 5;
    }

    public static function tscoreToFiveCat2($x)
    {
        if($x < 30) {

            return 1;

        } elseif (30 <= $x && $x < 45) {

            return 2;

        }elseif (45 <= $x && $x < 56) {

            return 3;

        }elseif (56 <= $x && $x < 71) {

            return 4;

        }

        return 5;
    }

    public static function fitscoreToFiveCat($x)
    {
        if($x < 30) {

            return 1;

        } elseif (30 <= $x && $x < 42) {

            return 2;

        }elseif (42 <= $x && $x < 59) {

            return 3;

        }elseif (59 <= $x && $x < 71) {

            return 4;

        }

        return 5;
    }

    public static function stenToFiveCat($x)
    {
        if(in_array($x,[1,2])) {
            return 1;
        } elseif (in_array($x,[3,4])) {
            return 2;
        } elseif (in_array($x,[5,6])) {
            return 3;
        } elseif (in_array($x,[7,8])) {
            return 4;
        } elseif (in_array($x,[9,10])) {
            return 5;
        }
        return false;
    }
    
    public static function isInt($input){
        return(ctype_digit(strval($input)));
    }
}

//$src = '/path/of/source/';
//$dst = '/path/to/destination/';
//recurse_copy($src,$dst);
