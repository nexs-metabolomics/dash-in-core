<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use Phalcon\Db\Enum;
//use PhpParser\Error;

class StudyexporterRepositoryOld extends ComponentRoot
{

    public function exportGetDesignvariableData($studyId)
    {

        try {
            $sql = StudyExporterQueryOld::$export_designvariables;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId,
            ]);

            if ($result) {
                return $result;
            }
        } catch (\PDOException $e) {
            error_log($e->getMessage());
            error_log($e->getTraceAsString());
            $this->flashSession->error("Failed to retrieve design variables for export. Error code: " . $e->getCode());
        }

        return false;

    }

    public function getWholeexportData($studyId, $queryName)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {
            $queryName = "export_$queryName";
            $sql = StudyExporterQueryOld::$$queryName;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            error_log($e->getMessage());
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

}