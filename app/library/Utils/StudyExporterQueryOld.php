<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class StudyExporterQueryOld extends ComponentRoot
{

    public static string $export_designvar =
        <<<'EOD'
            WITH
              input_query                       AS (
                                                     SELECT study_id AS _study_id
                                                     FROM dashin.study
                                                     WHERE study_id = :study_id
                                                       AND jsonb_extract_path_text(details, 'permissions', 'public') = 'yes'
                                                   )
              , startgroup_query                AS (
                                                     SELECT 'startgroup'                     AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'startgroup_id', startgroup_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_startgroup
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , center_query                    AS (
                                                     SELECT 'center'                         AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'center_id', center_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_center
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , event_query                     AS (
                                                     SELECT 'event'                          AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_event
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , subevent_query                  AS (
                                                     SELECT 'subevent'                       AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'subevent_id', subevent_id
                                                              , 'subeventtype_id', subevent_type_id
                                                              , 'interventiontype_id', intervention_type_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_subevent
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , samplingevent_query             AS (
                                                     SELECT 'samplingevent'                  AS objectname
                                                          , jsonb_agg(row_obj ORDER BY name) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'samplingevent_id', samplingevent_id
                                                              , 'subeventtype_id', sampling_type_id
                                                              , 'name', name
                                                              , 'label', label
                                                              , 'description', description) AS row_obj
                                                                 , name
                                                            FROM dashin.study_samplingevent
                                                               ,        input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , samplingtime_query              AS (
                                                     SELECT 'samplingtime'                            AS objectname
                                                          , jsonb_agg(row_obj ORDER BY sename,stname) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'samplingtime_id', a.samplingtime_id
                                                              , 'samplingevent_id', a.samplingevent_id
                                                              , 'ordinal_time', a.ordinal_time
                                                              , 'name', a.name
                                                              , 'description', a.description
                                                                   )      AS row_obj
                                                                 , a.name AS stname
                                                                 , b.name AS sename
                                                            FROM dashin.study_samplingtime a
                                                              INNER JOIN (
                                                                           SELECT z.samplingevent_id
                                                                                , z.name
                                                                           FROM dashin.study_samplingevent z
                                                                              ,                            input_query
                                                                           WHERE z.study_id = _study_id
                                                                         )                 b ON a.samplingevent_id = b.samplingevent_id
                                                          ) x
                                                   )
              , event_subevent_startgroup_query AS (
                                                     SELECT 'eveent_subevent_startgroup' AS objectname
                                                          , jsonb_agg(row_obj)           AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'subevent_id', subevent_id
                                                              , 'startgroup_id', startgroup_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_event_subevent_x_startgroup a
                                                               ,                                          input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , event_subevent_query            AS (
                                                     SELECT 'eveent_subevent'  AS objectname
                                                          , jsonb_agg(row_obj) AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'event_id', event_id
                                                              , 'subevent_id', subevent_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_event_x_subevent a
                                                               ,                               input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , subevent_samplingevent_query    AS (
                                                     SELECT 'subevent_samplingevent' AS objectname
                                                          , jsonb_agg(row_obj)       AS obj
                                                     FROM (
                                                            SELECT jsonb_build_object(
                                                              'subevent_id', subevent_id
                                                              , 'samplingevent_id', samplingevent_id
                                                                   ) AS row_obj
                                                            FROM dashin.study_subevent_x_samplingevent a
                                                               ,                                       input_query
                                                            WHERE study_id = _study_id
                                                          ) x
                                                   )
              , studycontact                    AS (
                                                     SELECT 'studycontact' AS objectname
                                                          , jsonb_build_object(
                                                       'studycontact_id', studycontact_id
                                                       , 'first_name', first_name
                                                       , 'last_name', last_name
                                                       , 'email', email
                                                       , 'description', description
                                                       , 'study_role_id', study_role_id
                                                            )              AS obj
                                                     FROM dashin.studycontact a
                                                        ,                     input_query
                                                     WHERE study_id = _study_id
                                                   )
              , combine_query                   AS (
                                                     SELECT objectname
                                                          , obj
                                                     FROM startgroup_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM center_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM subevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM samplingevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM samplingtime_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_subevent_startgroup_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM event_subevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM subevent_samplingevent_query
                                                     UNION ALL
                                                     SELECT objectname
                                                          , obj
                                                     FROM studycontact
                                                   )
            SELECT *
            FROM combine_query;
        EOD;

    public static string $summary_dataset_list =
        <<<'EOD'
            SELECT a.dataset_id
                 , b.name
                 , count(*) AS n
            FROM dashin.studyrow_x_datarow a
              INNER JOIN dashin.dataset    b ON a.dataset_id = b.dataset_id
            WHERE study_id = :study_id
            GROUP BY a.dataset_id , b.name;
        EOD;

    public static string $summary_studydata_design =
        <<<'EOD'
            WITH
              input_query     AS (
                                 --  SELECT :study_id::UUID AS _study_id
                                     SELECT study_id AS _study_id
                                     FROM dashin.study
                                     WHERE study_id = :study_id
                                       AND jsonb_extract_path_text(details, 'permissions', 'public') = 'yes'
                                 )
              , summary_query AS (
                                   SELECT 'studydatarow' AS summaryname
                                        , count(*)       AS n
                                   FROM dashin.studydatatable
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'subject'
                                        , count(*)
                                   FROM dashin.study_subject
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'startgroup'
                                        , count(*)
                                   FROM dashin.study_startgroup
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'center'
                                        , count(*)
                                   FROM dashin.study_center
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'event'
                                        , count(*)
                                   FROM dashin.study_event
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'subevent'
                                        , count(*)
                                   FROM dashin.study_subevent
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'samplingevent'
                                        , count(*)
                                   FROM dashin.study_samplingevent
                                      ,        input_query
                                   WHERE study_id = _study_id
                                   UNION ALL
                                   SELECT 'samplingtime'
                                        , count(*)
                                   FROM dashin.study_samplingtime a
                                        INNER JOIN dashin.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
                                      , input_query
                                   WHERE study_id = _study_id
                                 )
            SELECT *
            FROM summary_query;
        EOD;

    public static string $export_designvariables =
        <<<'EOD'
            WITH
              input_query              AS (
                                            -- SELECT :study_id::UUID AS _study_id
                                             SELECT study_id AS _study_id
                                             FROM dashin.study
                                             WHERE study_id = :study_id
                                               AND jsonb_extract_path_text(details, 'permissions', 'public') = 'yes'
                                          )
              , startgroup_query       AS (
                                            SELECT 'startgroup'                     AS objectname
                                                 , jsonb_agg(row_obj ORDER BY name) AS obj
                                            FROM (
                                                   SELECT jsonb_build_object(
                                                     'startgroup_id', startgroup_id
                                                     , 'name', name
                                                     , 'label', label
                                                     , 'description', description) AS row_obj
                                                        , name
                                                   FROM dashin.study_startgroup
                                                      ,        input_query
                                                   WHERE study_id = _study_id
                                                 ) x
                                          )
              , center_query           AS (
                                            SELECT 'center'                         AS objectname
                                                 , jsonb_agg(row_obj ORDER BY name) AS obj
                                            FROM (
                                                   SELECT jsonb_build_object(
                                                     'center_id', center_id
                                                     , 'name', name
                                                     , 'label', label
                                                     , 'description', description) AS row_obj
                                                        , name
                                                   FROM dashin.study_center
                                                      ,        input_query
                                                   WHERE study_id = _study_id
                                                 ) x
                                          )
              , event_query            AS (
                                            SELECT 'event'                          AS objectname
                                                 , jsonb_agg(row_obj ORDER BY name) AS obj
                                            FROM (
                                                   SELECT jsonb_build_object(
                                                     'event_id', event_id
                                                     , 'name', name
                                                     , 'label', label
                                                     , 'description', description) AS row_obj
                                                        , name
                                                   FROM dashin.study_event
                                                      ,        input_query
                                                   WHERE study_id = _study_id
                                                 ) x
                                          )
              , subevent_query         AS (
                                            SELECT 'subevent'                       AS objectname
                                                 , jsonb_agg(row_obj ORDER BY name) AS obj
                                            FROM (
                                                   SELECT jsonb_build_object(
                                                     'subevent_id', subevent_id
                                                     , 'subeventtype_id', subevent_type_id
                                                     , 'interventiontype_id', intervention_type_id
                                                     , 'name', name
                                                     , 'label', label
                                                     , 'description', description) AS row_obj
                                                        , name
                                                   FROM dashin.study_subevent
                                                      ,        input_query
                                                   WHERE study_id = _study_id
                                                 ) x
                                          )
              , samplingevent_query    AS (
                                            SELECT samplingevent_id
                                                 , jsonb_build_object(
                                              'samplingevent_id', samplingevent_id
                                              , 'subeventtype_id', sampling_type_id
                                              , 'name', name
                                              , 'label', label
                                              , 'description', description) AS row_obj
                                                 , name
                                            FROM dashin.study_samplingevent
                                               ,        input_query
                                            WHERE study_id = _study_id
                                          )
              , samplingtime_query     AS (
                                            SELECT jsonb_agg(row_obj ORDER BY ordinal_time) AS rowgroup_obj
                                                 , samplingevent_id
                                            FROM (
                                                   SELECT a.samplingevent_id
                                                        , jsonb_build_object(
                                                     'samplingevent_id', a.samplingevent_id
                                                     , 'samplingtime_id', a.samplingtime_id
                                                     , 'ordinaltime', a.ordinal_time
                                                     , 'name', a.name
                                                     , 'label', a.label
                                                     , 'description', a.description) AS row_obj
                                                        , ordinal_time
                                                   FROM dashin.study_samplingtime   a
                                                     INNER JOIN samplingevent_query b ON a.samplingevent_id = b.samplingevent_id
                                                 ) x
                                            GROUP BY samplingevent_id
                                          )
              , samplingcombined_query AS (
                                            SELECT 'samplingevent'            AS objectname
                                                 , jsonb_agg(combinedrow_obj) AS obj
                                            FROM (
                                                   SELECT jsonb_build_object('samplingevent_id', a.samplingevent_id, 'samplingevent', a.row_obj, 'samplingtime', b.rowgroup_obj) AS combinedrow_obj
                                                   FROM samplingevent_query       a
                                                     LEFT JOIN samplingtime_query b ON a.samplingevent_id = b.samplingevent_id
                                                 ) x
                                          )
              , combine_query          AS (
                                            SELECT objectname
                                                 , obj
                                            FROM startgroup_query
                                            UNION ALL
                                            SELECT objectname
                                                 , obj
                                            FROM center_query
                                            UNION ALL
                                            SELECT objectname
                                                 , obj
                                            FROM event_query
                                            UNION ALL
                                            SELECT objectname
                                                 , obj
                                            FROM subevent_query
                                            UNION ALL
                                            SELECT objectname
                                                 , obj
                                            FROM samplingcombined_query
                                          )
            SELECT *
            FROM combine_query;
        EOD;

}