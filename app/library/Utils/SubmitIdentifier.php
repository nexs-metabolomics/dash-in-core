<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-23
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace App\Library\Utils;


use App\Library\ApplicationRoot\ComponentRoot;

class SubmitIdentifier extends ComponentRoot
{
    /**
     * @var null|string
     */
    public $_action;

    public $_params;
    
    private $_value;

    /**
     * part of a dispatch logic built around submit buttons
     * Submit button input fields must be named <submit_array_name>[xxx] (eg. btn[xxx]) where xxx identifies the intended action (decided by the developer)
     * Because the "value" of a submit button is used for non-data purposes the xxx-key replaces this functionality
     * it is through this possible to treat the xxx-key equivalently to the "value" of other form fields
     *
     * @param string  $name root name of the button array: default is btn
     *
     */
    public function initialize($name='btn')
    {
        if ($this->request->isPost() && $this->request->has($name)) {
            $btn = $this->request->get($name);
            if(is_array($btn)) {
                $key = key($btn);
                $this->_action = $key;
                if(is_array($btn[$key])) {
                    $this->_params = key($btn[$key]);
                }
            }
        }
    }

    /**
     * Returns the index for the pressed button in the btn[] array
     * @param $name
     * @return mixed|null
     */
    public function getAction($name='btn')
    {
        if(!$this->_action) {
            $this->initialize($name);
        }
        return $this->_action;
    }

    /**
     * Return the value of the submit button
     * 
     * @param string $name
     * @return mixed
     */
    public function getValue($name='btn')
    {
        if(!$this->_value) {
            $action = $this->getAction($name);
            if($action) {
                $this->_value = $this->request->getPost($name)[$action];
            }
        }
        return $this->_value;
    }
}