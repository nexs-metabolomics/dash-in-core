<?php

namespace App\Library\Utils;

use App\Library\ApplicationBase\ApplicationRepositoryBase;
use Phalcon\Db\Enum;

class SupportFileRepository extends ApplicationRepositoryBase
{

    /**
     * Get info on support file relevant for download
     * like original name, mime-type, current path etc.
     *
     * @param $supportfileId
     *
     * @return array|false
     */
    public function getSupportFileInfo($supportfileId)
    {

        if (!UniqueId::uuidValidate($supportfileId)) {
            return false;
        }

        try {

            $sql = SupportFileQuery::$get_supportfile_info;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "supportfile_id" => $supportfileId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * Return data for study
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyInfo($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {

            $sql = SupportFileQuery::$get_study_info;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "study_id" => $studyId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Return data for study
     *
     * @param $datasetid
     *
     * @return array|false
     */
    public function getDatasetInfo($datasetid)
    {

        if (!UniqueId::uuidValidate($datasetid)) {
            return false;
        }

        try {

            $sql = SupportFileQuery::$get_dataset_info;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "dataset_id" => $datasetid,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Update description of a support file
     *
     * @param $supportfileId
     * @param $description
     *
     * @return bool
     */
    public function updateFileDescription($supportfileId, $description)
    {

        if (!UniqueId::uuidValidate($supportfileId)) {
            return false;
        }

        $organizationId = $this->SU->getActiveManagerOrgId();

        try {
            $sql = SupportFileQuery::$update_supportfile_description;

            $state = $this->db->execute($sql, [
                "supportfile_id"  => $supportfileId,
                "organization_id" => $organizationId,
                "description"     => $description,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    #-------------------------------------------------------------------------------------------------
    # study
    #-------------------------------------------------------------------------------------------------
    /**
     * Register a study support file
     *
     * @param $studyId
     * @param $fileInfo
     *
     * @return false|mixed
     */
    public function registerStudyFile($studyId, $organizationId, $fileInfo)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }

        try {

//            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = SupportFileQuery::$register_study_supportfile;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "name"            => $fileInfo["name"],
                "filename"        => $fileInfo["filename"],
                "fileextension"   => $fileInfo["fileextension"],
                "mimetype"        => $fileInfo["mimetype"],
                "study_id"        => $studyId,
            ]);

            if (isset($result["supportfile_id"])) {
                return $result["supportfile_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * Get a list of support files for a study
     *
     * @param $studyId
     *
     * @return array|false
     */
    public function getStudyfileList($studyId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = SupportFileQuery::$get_study_supportfile_list;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "study_id"        => $studyId,
                "organization_id" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Delete support file from a study
     *
     * @param $studyId
     * @param $supportfileId
     *
     * @return bool
     */
    public function unregisterFileFromStudy($studyId, $supportfileId)
    {

        if (!UniqueId::uuidValidate($studyId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($supportfileId)) {
            return false;
        }

        try {
            $sql = SupportFileQuery::$delete_supportfile_from_study;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "study_id"        => $studyId,
                "organization_id" => $organizationId,
                "supportfile_id"  => $supportfileId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    #-------------------------------------------------------------------------------------------------
    # dataset
    #-------------------------------------------------------------------------------------------------
    /**
     * Register a dataset support file
     *
     * @param $datasetId
     * @param $fileInfo
     *
     * @return false|mixed
     */
    public function registerDatasetFile($datasetId, $organizationId, $fileInfo)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }

        try {

//            $organizationId = $this->SU->getActiveManagerOrgId();

            $sql = SupportFileQuery::$register_dataset_supportfile;

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "name"            => $fileInfo["name"],
                "filename"        => $fileInfo["filename"],
                "fileextension"   => $fileInfo["fileextension"],
                "mimetype"        => $fileInfo["mimetype"],
                "dataset_id"      => $datasetId,
            ]);

            if (isset($result["supportfile_id"])) {
                return $result["supportfile_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * Delete support file from a dataset
     *
     * @param $datasetId
     * @param $supportfileId
     *
     * @return bool
     */
    public function unregisterFileFromDataset($datasetId, $supportfileId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($supportfileId)) {
            return false;
        }

        try {
            $sql = SupportFileQuery::$delete_supportfile_from_dataset;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "dataset_id"      => $datasetId,
                "organization_id" => $organizationId,
                "supportfile_id"  => $supportfileId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get a list of support files for a dataset
     *
     * @param $datasetId
     *
     * @return array|false
     */
    public function getDatasetfileList($datasetId)
    {

        if (!UniqueId::uuidValidate($datasetId)) {
            return false;
        }
        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = SupportFileQuery::$get_dataset_supportfile_list;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "dataset_id"      => $datasetId,
                "organization_id" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    #-------------------------------------------------------------------------------------------------
    # assay
    #-------------------------------------------------------------------------------------------------
    /**
     * Register a assay support file
     *
     * @param $assayId
     * @param $fileInfo
     *
     * @return false|mixed
     */
    public function registerAssayFile($assayId, $fileInfo)
    {

        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }

        try {

            # there is no organization_id reference to assays

            $sql = SupportFileQuery::$register_assay_supportfile;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $result = $this->db->fetchOne($sql, Enum::FETCH_ASSOC, [
                "organization_id" => $organizationId,
                "name"            => $fileInfo["name"],
                "filename"        => $fileInfo["filename"],
                "fileextension"   => $fileInfo["fileextension"],
                "mimetype"        => $fileInfo["mimetype"],
                "assay_id"        => $assayId,
            ]);

            if (isset($result["supportfile_id"])) {
                return $result["supportfile_id"];
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;

    }

    /**
     * Delete support file from a dataset
     *
     * @param $assayId
     * @param $supportfileId
     *
     * @return bool
     */
    public function unregisterFileFromAssay($assayId, $supportfileId)
    {

        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }
        if (!UniqueId::uuidValidate($supportfileId)) {
            return false;
        }

        try {
            $sql = SupportFileQuery::$delete_supportfile_from_assay;

            $organizationId = $this->SU->getActiveManagerOrgId();

            $state = $this->db->execute($sql, [
                "assay_id"        => $assayId,
                "organization_id" => $organizationId,
                "supportfile_id"  => $supportfileId,
            ]);

            return $state;

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

    /**
     * Get a list of support files for a dataset
     *
     * @param $assayId
     *
     * @return array|false
     */
    public function getAssayfileList($assayId)
    {

        if (!UniqueId::uuidValidate($assayId)) {
            return false;
        }
        $organizationId = $this->SU->getActiveManagerOrgId();
        try {
            $sql = SupportFileQuery::$get_assay_supportfile_list;

            $result = $this->db->fetchAll($sql, Enum::FETCH_ASSOC, [
                "assay_id"        => $assayId,
                "organization_id" => $organizationId,
            ]);

            if ($result) {
                return $result;
            }

        } catch (\PDOException $e) {
            $this->flashSession->error($e->getMessage());
        }

        return false;
    }

}