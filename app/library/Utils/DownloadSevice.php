<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2018-03-17
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;

class DownloadSevice extends ComponentRoot
{

    /**
     * Path to reference list of mime-types
     * 
     * @var $_mimeTypes array|null
     */
    private array|null $_mimeTypes=null;

    /**
     * Get mime-type from a reference list of mime-types based on file extension
     * 
     * @param $ext
     *
     * @return mixed|void
     */
    private function _getMimeType($ext)
    {

        if (!$this->_mimeTypes) {
            $this->_mimeTypes = include APP_PATH . "/app/config/reference_mime_types.php";
        }
        if (isset($this->_mimeTypes[$ext])) {
            return $this->_mimeTypes[$ext];
        }
    }

    /**
     * Send af file to the client (download)
     *
     * @param string $fileContents
     * @param string $filename
     * @param string $ext
     * @param string $mimeType
     */
    public function send(string $fileContents, string $filename, string $ext, string $mimeType = "")
    {

        if (strlen($mimeType) == 0) {
            $mimeType = $this->_getMimeType($ext);
        }

        if (strlen($fileContents)) {
            header('Content-Type: ' . $mimeType);
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Expires: Sat, 26 Jul 1997 00:00:01 GMT');
            header('Cache-Control: no-cache, no-store, must-revalidate');
            header("Pragma: no-cache");
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Content-Length: ' . strlen($fileContents));
            echo $fileContents;
            exit;
        }

        return false;
    }

}