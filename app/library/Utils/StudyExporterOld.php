<?php

namespace App\Library\Utils;

use App\Library\ApplicationRoot\ComponentRoot;
use ZipArchive;

class StudyExporterOld extends ComponentRoot
{

    /**
     * @var StudyexporterRepositoryOld
     */
    private $_repository;

    /**
     * @var ZipArchive
     */
    private $_zip;

    /**
     * @var string
     */
    private $_zipPath;

    /**
     * @var int
     */
    private int $ENC_UTF8 = ZipArchive::FL_ENC_UTF_8;

    /**
     * @var int
     */
    private int $COMPRESS_XZ = ZipArchive::CM_XZ;

    /**
     * @var Csv
     */
    private $_csv;

    /**
     * @return StudyexporterRepositoryOld
     */

    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new StudyexporterRepositoryOld();
        }

        return $this->_repository;
    }

    public function __construct($zipPath)
    {

        $this->_zip = new ZipArchive();
        $this->init($zipPath);
        $this->_csv = new Csv();
    }

    /**
     * @param $zipPath
     *
     * @return void
     */
    public function init()
    {

        if (!($tmpDir = $this->storage->getTempdirLocation())) {
            $this->flashSession->error("Could not save temporary files for ZipArchive");

            return false;
        }
        $tmpfileName = tempnam($tmpDir, "tempzip_");
        $this->_zipPath = "$tmpfileName";

        $this->_zip->open($this->_zipPath, ZipArchive::CREATE);
    }

    /**
     * @param $studyid
     *
     * @return void
     */
    public function addWholestudyData($studyid)
    {

        $designVariableData = $this->_getRepository()->getWholeexportData($studyid, "designvar");
        if(!$designVariableData) {
            return false;
        }

        if (!$designVariableData) {
            error_log("Could not retrieve design variables");
            $this->flashSession->error("Could not retrieve design variables");

            return false;
        }

        $designVariableDataCsv = $this->_csv->arrayToCsv($designVariableData, ",", true);
        if (!$designVariableDataCsv) {
            error_log("Could not convert design variables to csv");
            $this->flashSession->error("Could not convert design variables to csv");

            return false;
        }

        $ok = $this->_zip->addFromString("desingvariable", $designVariableDataCsv, $this->ENC_UTF8);
        if (!$ok) {
            error_log("Could not add design variables to zip archive");
            $this->flashSession->error("Could not add design variables to zip archive");

            return false;
        }

        $ok = $this->_zip->setCompressionName("desingvariable", $this->COMPRESS_XZ);
        if (!$ok) {
            error_log("Could not change compression method for design variables");
            $this->flashSession->error("Could not change compression method for design variables");
//            return false;
        }

        return true;
    }

    /**
     * @return false|string
     */
    public function getContents()
    {

        $contents = file_get_contents($this->_zipPath);

        return $contents;
    }

    /**
     * @return bool
     */
    public function close()
    {

        $ok = $this->_zip->close();

        return $ok;
    }

}