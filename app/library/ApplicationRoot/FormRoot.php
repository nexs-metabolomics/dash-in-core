<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace App\Library\ApplicationRoot;


use Phalcon\Forms\Form;

/**
 * Class FormMaster
 * @package App\Library\Masters
 *
 * @property \App\Library\Utils\SubmitIdentifier $Btn
 * @property \System\Library\SessionUser\SessionUser $SU
 * @property \App\Library\Mailer\MyMailer $mailer
 * @property \App\Library\Utils\DownloadSevice $downloader
 * @property \App\Library\Utils\Security $security
 * @property \App\Library\Utils\LocalSession $localsession
 * @property \App\Library\Utils\Storage $storage
 * @property \App\Library\Utils\SupportFileHandler $supportFiles
 *
 */
class FormRoot extends Form
{

}