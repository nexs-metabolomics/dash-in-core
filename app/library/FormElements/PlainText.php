<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-12-06
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\FormElements;

use App\Library\Tag\Tag;
use Phalcon\Forms\Element\AbstractElement;

/**
 * Component PlainText for forms
 */
class PlainText extends AbstractElement
{
    /**
     * Renders the element widget returning HTML
     */
    public function render($attributes = []): string
    {
        $result = Tag::plainText($this->prepareAttributes($attributes));
        return $result;
    }

    /**
     * Returns an array of prepared attributes for Phalcon\Html\TagFactory
     * helpers according to the element parameters
     */
    protected function prepareAttributes(array $attributes = [])
    {

        $name = $this->name;

        $attributes[0] = $name;

        /**
         * Merge passed parameters with default ones
         */
        $defaultAttributes = $this->attributes;
        $mergedAttributes = array_merge($defaultAttributes, $attributes);

        /**
         * Get the current element value
         */
        $value = $this->getValue();

        /**
         * If the widget has a value set it as default value
         */
        if ($value !== null) {
            $mergedAttributes["value"] = $value;
        }

        return $mergedAttributes;
    }
}