<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-09-29
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Library\FormElements;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\AbstractElement;
use Phalcon\Forms\Element\ElementInterface;

class CheckMulti extends AbstractElement
{

    protected $_options;

    private $_itemLabels;

    private $_items;

    private $_defaults;

//    private $_defaults;

    /**
     * $options can be an array of values, one for each item.
     *    Each item can also be represente by an array. If so, the first element is the value
     *    all subsequent elements are interpreted as "key"=>"value" attributes rendered as html attributes in the <input> element
     * Example of the simple form:
     *    $options = ["value1","value2","value3"]
     * Example of mix between the two forms:
     *    $options = [["value1","id"=>"item1"],"value2",["value3","id"=>"item3"]]
     *
     * @inheritdoc
     *
     * @param string     $name       name for all items
     * @param null|array $options    values for individual items.
     * @param array      $attributes additional attributes for all items
     */
    public function __construct($name, $options = null, array $attributes = [])
    {

        parent::__construct($name, $attributes);
        $this->_options = $options;
    }

    public function setDefault($value): ElementInterface
    {
        return parent::setDefault($value);
    }

    public function getDefault()
    {

        $default = parent::getDefault();
        if (!is_array($default)) {
            $default = (array)$default;
        }

        return $default;
    }

    /**
     * generate items
     *
     * @param null|array $attributes
     */
    private function _generate(array $attributes = [])
    {

        if (is_array($attributes)) {
            $tempAttributes = $this->getAttributes();
            $this->setAttributes($attributes + $tempAttributes);
        }

        $name = $this->getAttribute("name");
        if ($name == null) {
            $name = $this->getName();
        }

        $postValues = $this->getValue();
        if (!is_array($postValues)) {
            $postValues = [$postValues];
        }

        # selected options
        $defaults = $this->getDefault();
        $i = 0;
        foreach ($this->_options as $key => $value) {
            $attributes = $this->getAttributes();
            $id = $name . "_" . $key;
            $attributes["id"] = $id;
            $attributes["name"] = $name . "[$key]";
            $attributes["value"] = $key;
            if (in_array($key, $defaults) || in_array($key, $postValues)) {
                $attributes["checked"] = $key;
            }

            # array means extra attributes are set for this checkbox
            # extract extra values into attributes while value is 
            # element's label
            if (is_array($value)) {
                # put first element aside
                $tmp = array_shift($value);
                # "value" takes precedence over "attributes"
                $attributes = $value + $attributes;
                $value = $tmp;
            }

            $chkFld = new Check($id, $attributes);
            if (isset($this->_itemLabels[$i])) {
                $chkFld->setLabel($this->_itemLabels[$i]);
            } else {
                $chkFld->setLabel($value);
            }
            $this->_items[] = $chkFld;
            ++$i;
        }
    }

    /**
     * Return items
     *
     * @param $attributes
     *
     * @return mixed
     */
    public function getItems($attributes)
    {

        if (!$this->_items) {
            $this->_generate($attributes);
        }

        return $this->_items;
    }

    /**
     * Set labels for individual items
     *
     * @param array $labels
     * @param bool  $numIndex use numeric index
     */
    public function setItemLabels(array $labels, $numIndex = false)
    {

        if ($numIndex) {
            $labels = array_values($labels);
            $i = 0;
            foreach ($this->_options as &$option) {
                $option = $labels[$i];
                ++$i;
            }
        } else {
            $keys = array_keys($labels);
            foreach ($keys as $key) {
                if (array_key_exists($key, $this->_options)) {
                    $this->_options[$key] = $labels[$key];
                }
            }
        }
    }

    /**
     * Get item labels
     *
     * @return mixed
     */
    public function getItemLabels()
    {

        return $this->_itemLabels;
    }

    /**
     * Set options
     *
     * @param $options
     */
    public function setOptions($options)
    {

        $this->_options = (array)$options;
    }

    /**
     * Return options
     *
     * @return array|null
     */
    public function getOptions()
    {

        return $this->_options;
    }

    /**
     * @param null $attributes
     * @param bool $force
     *
     * @return array|bool|string
     */
    public function render($attributes = array(), bool $force = false): string
    {

        if (!$this->_items || $force) {
            $this->_generate($attributes);
        }

        /** @var Check $chkFld */
        $fields = [];
        foreach ($this->_items as $key => $chkFld) {
            $itemLabel = $chkFld->getLabel();
            $id = $chkFld->getAttribute("id");
            if ($itemLabel) {
                $itemLabelStr = "\n<label for=\"$id\"><div class=\"labdecor\"></div><div class='label-text'>" . $itemLabel . "</div></label>\n";
            } else {
                $itemLabelStr = "\n<label for=\"$id\"></label>\n";
            }
            $fields[] = "\n    <div class=\"field\" id=\"li-$id\">" . $chkFld->render() . $itemLabelStr . "</div>";
            echo "";
        }

        if ($fields) {
            $name = $this->getName();
            $labelId = $this->getUserOption("label_id");
            $labelIdStr = strlen($labelId) ? "id=\"$labelId\"" : "";
            $class = isset($attributes["class"]) ? $attributes["class"] : "cl-$name";

            $fields =
                "\n<div class=\"checkmulti $class\">\n  <div class=\"label\" $labelIdStr>" .
                $this->getAttribute("label") .
                "</div>\n  <div class=\"field-group\">" .
                join("\n", $fields) .
                "</div>\n</div>";

            return $fields;
        }

        return false;
    }

}