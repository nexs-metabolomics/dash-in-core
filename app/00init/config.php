<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

# workaround for an xdebug-related bug
# see https://forum.phalconphp.com/discussion/19801/unable-to-debug-phalcon-app-in-visual-studio-code-vscode-with-xd#C60227
use Phalcon\Config\Config as DefaultConfig;
if(!class_exists("Config")) {
    class Config extends DefaultConfig{}
}


return new Config(array(
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'voltDir'        => APP_PATH . '/app/cache/volt/',
        'voltTestDir'    => APP_PATH . '/tests/_cache/',
        'baseUri'        => '/',
    )
));
