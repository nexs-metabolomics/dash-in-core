<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config\Config $config
 */

# workaround for an xdebug-related bug
# see https://forum.phalconphp.com/discussion/19801/unable-to-debug-phalcon-app-in-visual-studio-code-vscode-with-xd#C60227
//use App\Library\Utils\PgAdapter;
use App\Library\Utils\SupportFileHandler;
use Phalcon\Di\FactoryDefault as DefaultFactory;

if (!class_exists("FactoryDefault")) {
    class FactoryDefault extends DefaultFactory
    {

    }
}

use Phalcon\Mvc\View;

//use Phalcon\Mvc\Url as UrlResolver;
use App\Library\Utils\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;

//use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;
use System\Library\Session\Adapter\Database as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use App\Library\Utils\SubmitIdentifier;
use App\Library\RecursiveConfig\Yaml as ConfigLoader;
use App\Library\Utils\LocalSession;

//use App\Library\Utils\Security;

#--------------------------------------------------------------
# factoryDefault
#--------------------------------------------------------------
/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */

$di = new FactoryDefault();

$di->setShared("config", function () use ($config) {

    $ymlConfig = new ConfigLoader(__DIR__ . '/../config/config.yml');
    $ymlConfig->merge($config);

    return $ymlConfig;
});

#--------------------------------------------------------------
# Audit logger
#--------------------------------------------------------------
$di->setShared("auditLogger", function () {

    $config = $this->getConfig();
    $doLogging = $config->logger->do_audit_logging;

    if (!$doLogging) {
        $adapter = new Phalcon\Logger\Adapter\Noop();

        $logger = new App\Library\AppLogger\AppLogger(
            "messages",
            [
                "main" => $adapter
            ],
            $doLogging
        );

        return $logger;
    }

    $logfile = $config->logger->audit_logfile;
    $adapter = new Phalcon\Logger\Adapter\Stream($logfile);

    $logger = new App\Library\AppLogger\AppLogger(
        "messages",
        [
            "main" => $adapter
        ],
        $doLogging
    );

    return $logger;
});

#--------------------------------------------------------------
# security roles
#--------------------------------------------------------------

#--------------------------------------------------------------
# url
#--------------------------------------------------------------
/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {

    $config = $this->getConfig();
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

#--------------------------------------------------------------
# localsession
#--------------------------------------------------------------
$di->setShared("localsession", function () {

    $localsession = new LocalSession();

    return $localsession;
});

#--------------------------------------------------------------
# routes
#--------------------------------------------------------------
$di->setShared('router', function () {

    return require __DIR__ . '/routes.php';
});

#--------------------------------------------------------------
# SessionUser
#--------------------------------------------------------------
use System\Library\SessionUser\SessionUser;

$di->setShared("SU", function () {

    return new SessionUser();
});

#--------------------------------------------------------------
# view
#--------------------------------------------------------------
if (!function_exists("renderMessages")) {
    function renderMessages($e)
    {

        if (method_exists($e, "hasMessages") && $e->hasMessages()) {
            $msg = "<div class=\"nds-messages\">\n";
            foreach ($e->getMessages() as $m) {
                $msg .= "<div class=\"nds-message\">$m</div>\n";
            }
            $msg .= "</div>\n";

            return $msg;
        }
    }
};
if (!function_exists("strcut")) {
    function strcut($str, $len)
    {

        if (strlen($str) > $len) {
            $outstr = substr($str, 0, $len - 3) . "...";

            return $outstr;
        }

        return $str;
    }
};

if (!function_exists("to_array")) {
    function to_array($obj)
    {

        return (array)$obj;
    }
}

if (!function_exists("contains")) {
    function contains($haystack, $needle)
    {

        return strlen(strstr($haystack, $needle)) > 0;
    }
}

/**
 * Setting up the view component
 */
$di->setShared('view', function () {

    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt'  => function ($view) {

            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $path = getenv("APPLICATION_CONTEXT") == "testing" ? $config->application->voltTestDir : $config->application->voltDir;

            $volt->setOptions(array(
                'path'      => $path,
                'separator' => '|',
                'always'    => true,
            ));

            # extend volt engine
            $compiler = $volt->getCompiler();
            $compiler->addFunction("contains", "contains");
            $compiler->addFunction("count", "count");
            $compiler->addFunction("strcut", "strcut");
            $compiler->addFunction("in_array", "in_array");
            $compiler->addFunction("to_array", "to_array");
            $compiler->addFunction("renderMessages", "renderMessages");

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});

#--------------------------------------------------------------
# db
#--------------------------------------------------------------
/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {

    $dbConfig = $this->getConfig()->database->db->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);
    
    # custom adapter inherits directly from PDO in order
    # to expose pg-copy, needed to import sqdr-files
    # phalcon-db exposes only a subset of pg-functionality
    $class = 'App\Library\Utils\PgAdapter';

//    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

#--------------------------------------------------------------
# models metadata
#--------------------------------------------------------------
/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {

    return new MetaDataAdapter();
});

#--------------------------------------------------------------
# flash
#--------------------------------------------------------------
/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {

    $flash = new Flash();
    $flash->setCssClasses([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);

    return $flash;
});

$di->set('flashMessage', function () {

    $flash = new \App\Library\Utils\FlashMessage();
    $flash->setCssClasses([
        'error'   => 'error',
        'success' => 'success',
        'notice'  => 'notice',
        'warning' => 'warn',
    ]);

    return $flash;
});

#--------------------------------------------------------------
# session
#--------------------------------------------------------------
/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {

    $session = new SessionManager();

    $sessionDb = new SessionAdapter([
        'db' => $this->get("db"),
    ]);

    $session->setAdapter($sessionDb);
    $session->start();

    return $session;
});

#--------------------------------------------------------------
# submit identifier
#--------------------------------------------------------------
$di->set("Btn", function () {

    return new SubmitIdentifier();
});

$di->set("mailer", function () {

    require_once __DIR__ . '/../../vendor/autoload.php';
    $mailAccount = $this->getConfig()->mail->admin_account->toArray();
    $Mailer = new \App\Library\Mailer\MyMailer($mailAccount);

    return $Mailer;
});

$di->setShared("downloader", function () {

    $Downloader = new \App\Library\Utils\DownloadSevice();

    return $Downloader;
});

#--------------------------------------------------------------
# security
#--------------------------------------------------------------
$di->setShared("security", function () {

    $security = new \App\Library\Utils\Security();

    return $security;
});

#--------------------------------------------------------------
# storage
#--------------------------------------------------------------
use App\Library\Utils\Storage;

$di->setShared("storage", function () {

    return new Storage();
});

#--------------------------------------------------------------
# support files
#--------------------------------------------------------------
$di->setShared("supportFiles", function () {

    return new SupportFileHandler();
});
