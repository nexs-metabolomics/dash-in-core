<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
# don't change this after session is started
# when testing, this file is read repeatedly
if(session_status() === PHP_SESSION_NONE) {
    ini_set("session.gc_maxlifetime",18000);
}
/**
 * Read the configuration
 */
$config = include __DIR__ . "/config.php";

/**
 * Read auto-loader
 */
include __DIR__ . "/loader.php";

/**
 * Read services
 */
include __DIR__ . "/services.php";

/** @var $di Phalcon\Di\FactoryDefault */
if(!defined("STORAGE_PATH")) {
    define("STORAGE_PATH",$di["config"]->storage);
}
if(!defined("TEMPDIR_PATH")) {
    if(!is_dir($di["config"]->tempdir)) {
        $ok = mkdir($di["config"]->tempdir,recursive: true);
    }
    define("TEMPDIR_PATH", $di["config"]->tempdir);
}
include __DIR__ . "/../admin_modules/System/Config/services.php";
include __DIR__ . "/../admin_modules/Fgac/Config/services.php";
include __DIR__ . "/../modules/Dashin/Config/services.php";
