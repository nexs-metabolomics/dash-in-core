<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-19
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

$loader = new \Phalcon\Autoload\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->setNamespaces(
    array(
        'App\Controllers' => dirname(__DIR__) ."/controllers",
        'App\Models'      => dirname(__DIR__) ."/models",
        'App\Library'     => dirname(__DIR__) ."/library",
        'Zep3q'           => dirname(__DIR__) ."/library/Zep3q",
        'App\Forms'       => dirname(__DIR__) ."/forms",
        #----------------------------------------------------------------
        'System'          => dirname(__DIR__) ."/admin_modules/System",
        'Files'           => dirname(__DIR__) ."/admin_modules/Files",
        'Fgac'            => dirname(__DIR__) ."/admin_modules/Fgac",
        #----------------------------------------------------------------
        'Dashin'          => dirname(__DIR__) ."/modules/Dashin",
        'Public'          => dirname(__DIR__) ."/modules/Public",
        #----------------------------------------------------------------
        'Phalcon\Ext\Mailer'  => dirname(__DIR__) ."/../vendor/phalcon-ext/mailer/src",
    )
);
$loader->register();
