<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

//const SECLAB_SYS_ADMIN = 1;
if (!defined("SECLAB_SYS_ADMIN")) {
    define("SECLAB_SYS_ADMIN", 1);
}
//const SECLAB_APP_ADMIN = 10;
if (!defined("SECLAB_APP_ADMIN")) {
    define("SECLAB_APP_ADMIN", 10);
}
//const SECLAB_ORG_ADMIN = 500;
if (!defined("SECLAB_ORG_ADMIN")) {
    define("SECLAB_ORG_ADMIN", 500);
}
//const SECLAB_ORG_USER = 510;
if (!defined("SECLAB_ORG_USER")) {
    define("SECLAB_ORG_USER", 510);
}
//const SECLAB_ORG_READONLY = 590;
if (!defined("SECLAB_ORG_READONLY")) {
    define("SECLAB_ORG_READONLY", 590);
}
//const SECLAB_ALL = 1000;
if (!defined("SECLAB_ALL")) {
    define("SECLAB_ALL", 1000);
}

$router = new Phalcon\Mvc\Router(false);
$router->removeExtraSlashes(true);
$router->setDefaultNamespace('App\Controllers');

$router->add(
    "/notfound(/)?",
    [
        "namespace"  => 'App\Controllers',
        "controller" => "Index",
        "action"     => "notfound",
        "params"     => 1,
        "view_base"  => "/index",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "(/)?",
    [
        "namespace"  => 'App\Controllers',
        "controller" => "Index",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "/index",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/index(/)?",
    [
        "namespace"  => 'App\Controllers',
        "controller" => "Index",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "/index",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/about(/)?",
    [
        "namespace"  => 'App\Controllers',
        "controller" => "Index",
        "action"     => "about",
        "params"     => 1,
        "view_base"  => "/index",
        "seclab_id"  => SECLAB_ALL,
    ]
);
$router->add(
    "/more(/)?",
    [
        "namespace"  => 'App\Controllers',
        "controller" => "Index",
        "action"     => "more",
        "params"     => 1,
        "view_base"  => "/index",
        "seclab_id"  => SECLAB_ALL,
    ]
);

#-----------------------------------------------
# welcome
#-----------------------------------------------
//$router->add(
//    "/coming/soon/:params",
//    [
//        "namespace"  => 'App\Controllers',
//        "controller" => "Welcome",
//        "action"     => "dummy",
//        "params"     => 1,
//        "view_base"  => "/welcome",
//        "seclab_id"  => SECLAB_ALL,
//    ]
//);

#-----------------------------------------------
# dashin
#-----------------------------------------------
$router->add(
    "/welcome/:params",
    [
        "namespace"  => 'Dashin\Controllers',
        "controller" => "Dashin",
        "action"     => "index",
        "params"     => 1,
        "view_base"  => "/modules/dashin",
        "seclab_id"  => SECLAB_ALL,
    ]
);

include __DIR__ . "/../admin_modules/System/Config/routes.php";
include __DIR__ . "/../admin_modules/Fgac/Config/routes.php";
include __DIR__ . "/../modules/Dashin/Config/routes.php";
include __DIR__ . "/../modules/Public/Config/routes.php";

return $router;
