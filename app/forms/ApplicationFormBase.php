<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Forms;

use App\Library\ApplicationRoot\ComponentRoot;
use App\Library\ApplicationRoot\FormRoot;
use App\Library\Utils\UniqueId;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Radio;

class ApplicationFormBase extends FormRoot
{

    /**
     * @var string
     */
    protected $_clientsideLegalChars = '^[^\u0021-\u0026\u0028-\u002C\u002E-\u002F\u003A-\u003E\u0040\u005B-\u005E\u0060\u007B-\u007F\u0080-\u009F\u00A1-\u00BF\u00D7\u00F7\u2014-\u2018\u2020-\u206F]+$';

    /**
     * @var string
     */
    protected $_clientsideEmailRegex = '(.*@.+\..+$)|(^$)';

    /**
     * @var string
     */
    protected $_serversideLegalChars = "/(^[ '_\\pL0-9-]+$)|(^$)/u";

    /**
     * Populate form with values not coming from post
     *
     * @param array|object $values bind values to form matching
     *                             on keys in $values and form-fields
     *
     * @param array|object $values
     */
    public function bindValues($values)
    {

        if (!is_array($values)) {
            $values = $values->toArray();
        }
        $elements = $this->getElements();
        $keys = array_keys(array_intersect_key($elements, $values));

        foreach ($keys as $key) {

            # elements with options need special treatment
            # note: Select::getOptions() is the only from phalcon
            # I use the same pattern for my multi-valued FormElements
            # - value may return string or array
            if (method_exists($this->get($key), "getOptions")) {
                $opt = $this->get($key)->getOptions();

                # if options is array, place values at the right position/key
                if (is_array($opt)) {

                    # values not array i.e. Select() (and maybe others?)
                    if (!is_array($values[$key])) {
                        $res = array_intersect($opt, [$values[$key]]);
                        $this->get($key)->setDefault(array_keys($res));
                    } else {
                        $res = array_intersect($opt, $values[$key]);
                        $this->get($key)->setDefault($res);
                    }

                } else {
                    $this->get($key)->setAttribute("value", $values[$key]);
                }

            } elseif ($this->get($key) instanceof Check
                || $this->get($key) instanceof Radio) {

                if ($values[$key] !== false) {
                    $this->get($key)->setAttribute("checked", $values[$key]);
                }

            } else {
                $this->get($key)->setAttribute("value", $values[$key]);
            }
        }
    }

    /**
     * Set search term
     * Convenience function to support search forms
     *
     * @param $searchTerm string|false|null
     * @param $fieldname string
     *
     * @return void
     */
    public function setSearchTerm(string|false|null $searchTerm, string $fieldname = "search_term")
    {

        if ($this->has($fieldname)) {
            # cast to to string b/c getSearchTerm() may return false
            $this->get($fieldname)->setAttribute("value", (string)$searchTerm);
        }
    }

    /**
     * Reset search term
     * Convenience function to support search forms
     *
     * @param $fieldname string
     *
     * @return void
     */
    public function resetSearchTerm(string $fieldname = "search_term")
    {

        if ($this->has($fieldname)) {
            $this->get($fieldname)->setAttribute("value", "");
        }
    }

    /**
     * Prevent a form from being posted twice
     * Works together with isRepeatPost()
     *
     * @param string $fieldName
     */
    public function preventRepeatPost($fieldName = "_prevent_repeat_")
    {

        $randomToken = UniqueId::uuid4();
        $tokenField = new Hidden($fieldName, ["value" => $randomToken]);
        $this->add($tokenField);

        # force phalcon to update value
        $this->get($fieldName)->clear();
        $this->session->set($fieldName, $randomToken);
    }

    /**
     * Prevent a form from being posted twice
     * Works together with preventRepeatPost()
     *
     * <code>
     *
     * //create a form, add a preventRepeat field
     * $form = new FormBase();
     * $form->preventRepeatPost();
     *
     * // somewhere else in the code
     * // note that you need to inject the access to the injected sservices
     * // in particular Request and Session.
     * if(FormBase::isRepeatPostStatic($this)) {
     *     // resend the current form instead of proceeding.
     * }
     *
     * </code>
     *
     * @param ComponentRoot $injectable
     * @param string        $fieldName
     *
     * @return bool
     */
    public static function isRepeatPostStatic(ComponentRoot $injectable, $fieldName = "_prevent_repeat_")
    {

        $postValue = $injectable->request->getPost($fieldName);
        $expectedValue = $injectable->session->get($fieldName);
        # if not equal this is a repeat post
        if (null !== $postValue && $postValue !== $expectedValue) {
            return true;
        }

        return false;
    }

    public function isRepeatPost($fieldName = "_prevent_repeat_")
    {

        $postValue = $this->request->getPost($fieldName);
        $expectedValue = $this->session->get($fieldName);
        # if not equal this is a repeat post
        if (null !== $postValue && $postValue !== $expectedValue) {
            return true;
        }

        return false;
    }

    public function setReadonly($fieldName, $criteria)
    {

        if ($criteria && $this->has($fieldName)) {
            $this->get($fieldName)->setAttribute("readonly", "readonly");
        }
    }

}