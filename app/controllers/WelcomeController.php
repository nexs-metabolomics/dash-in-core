<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-10-17
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */


namespace App\Controllers;


use App\Library\Helpers\WelcomeHelper;

class WelcomeController extends ControllerBase
{
    /**
     * @var WelcomeHelper
     */
    private $_helper;

    /**
     * @return WelcomeHelper
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = new WelcomeHelper();
        }
        return $this->_helper;
    }

    public function beforeExecuteRoute()
    {
        # _isLoggendIn() sets view variable IS_LOGGED_IN
        if(!$this->_isLoggedIn()) {
            return false;
        }
    }

    public function initialize()
    {
        parent::initialize();
        $this->view->pick("welcome/index");
    }
    
    public function indexAction()
    {
//        $this->view->setVar("page_class","welcomexx");
                      echo "";
    }
    
    public function dummyAction()
    {
        $this->view->pick("welcome/dummy");
        
    }
}