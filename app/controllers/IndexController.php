<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */

namespace App\Controllers;

use App\Library\Tag\Tag;

class IndexController extends UnsecuredControllerBase
{

    public function initialize()
    {

        parent::initialize();
    }

    public function indexAction()
    {
    }

    public function moreAction()
    {

        $this->view->pick("index/more");
    }

    public function aboutAction()
    {

        $this->view->pick("index/about");
    }

    public function notfoundAction()
    {
    }

}

