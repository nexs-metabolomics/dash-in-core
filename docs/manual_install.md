## Manual setting up an Ubuntu server

### Instaling php and phalcon

In order to install phalcon on your ubuntu server you first need to install the unofficial php repository for the latest version of php:

[Unofficial php repository](https://launchpad.net/~ondrej/+archive/ubuntu/php)

Please go there and familiarize yourself with the PPA and the procedure.

If you already know the basics you can go straight to installing the PPA and phalcon with the following commands

Install the repository:

    sudo add-apt-repository ppa:ondrej/php
    sudo apt update
    sudo apt install php-phalcon

Install the web server, php and the framework, and other packages needed for the application to work properly:

    sudo apt install apache2 php-yaml php-twig php-uuid php-json php-json-schema php-mbstring php-pgsql php-curl php-bz2 php-imagick php-phalcon3

Modules for apache web server if you are going to use OpenID Connect (OIDC):

    sudo apt install libapache2-mod-auth-openidc

Other necessary modules `mod-rewrite` and `mod-ssl` are included in the default install on Ubuntu.


### Installing PostgreSQL

The app is tested on PostgreSQL v10+ (tested on v10 - v13)

If your OS does not have a suitable version it can be installed from the official postgres repository for your platform:

[Install postgresql](https://www.postgresql.org/download/)



## clone the app

When the server is correctly installed you need to

1. clone the code to server (eg. clone to /var/www/dashin)

        git clone gitlab:nexs-metabolomics/dash-in-core.git /var/www/dashin

2. Make the site folders ready, create the cache dirs, make them writable by the web server

        cd /var/www/dashin
        mkdir -p app/cache/volt

    On Debian and derivatives like Ubuntu

        sudo chown www-data:www-data app/cache -R

    On Red Hat derivatives like CentOS

        sudo chown apache:apache app/cache -R

3. Define setup parameters

   1. Prepare the database setup parameters

           cp install/parameters-example.sh install/parameters.sh

      Fill in the fields in ```parameters.sh```:

            #----------------------------------------------------
            # database server address
            #----------------------------------------------------
            H="localhost"
            
            #----------------------------------------------------
            # database server port
            #----------------------------------------------------
            P=5432
            
            #----------------------------------------------------
            # superuser (the user who creates the
            # application user and application database)
            #----------------------------------------------------
            SU="postgres"
            # superuser database
            SD="postgres"
            # superuser password
            SP="<superuser password>"
            #----------------------------------------------------
            # application user (the user that the
            # application will use to access the database)
            #----------------------------------------------------
            AU="app_user"
            # application database
            AD="dashindb"
            # application user password
            AP="<application user password>"

   2. Prepare the runtime parameters for the application

           cp app/config/parameters-example.yml app/config/parameters.yml

      Fill in the fields in ```parameters.yml```:

            database:
              db:
                username: <db user>
                password: <db user's password to the database>
                host    : localhost
                port    : 5432
                dbname  : <database name>
            storage: "</path/to/storage/folder writable by the webserver>"
            tempdir: "/tmp"
            mail:
              admin_account:
                driver: smtp
                host: email.example.com
                port: 465
                encryption: ssl
                username: <email_username>
                password: <email_password>
                from:
                  email: dashin_admin@example.com
                  name: Dashin Admin
            ls_querystring_secret: "<secret (random string) to encrypt and decrypt querystrings for localized sessions>"

   3. Prepare cache folder

           mkdir -p app/cache/volt

      Make the cache folder writable for the web server

            chown <webserver-user>:<webserver-user> app/cache -R

4. setup the apache virtual host on the server. 

   #####This is just an example as the exact procedure depends on your specific environment

   The important thing to note is that ```DocumentRoot``` and ```<directory>``` must point to the ```public``` 
   folder. 

   Here is an example of a virtual host listening or port 80.

   It is assumed that you web address is ```dashin.example.com``` and that you installed the application in the folder ```/var/www/dashin```

        <VirtualHost *:80>
            ServerName dashin.example.com
        
            DocumentRoot /var/www/dashin/public
            <Directory /var/www/dashin/public>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
            </Directory>
        </VirtualHost>

5. Run the setup script in the install directory

   install/setup.sh

6. Go to ```http://dashin.example.com``` and log in to the application with user-code ```usrab12345678``` and password ```admin```

