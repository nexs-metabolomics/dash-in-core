# Versioning

We used the [semantic versioning](https://semver.org) system. The version number takes the following form where "Major" releases are assumed to include breaking changes, "Minor" releases include feature changes/additions and "patches" are fixes. Commits containing only refactoring, tests or documentation changes do not result in version changes.

<img src="https://upload.wikimedia.org/wikipedia/commons/8/82/Semver.jpg" align="left" width="180">

<br clear="left"/>

# Commit messages

We use [semantic-release](https://semantic-release.gitbook.io/semantic-release/) to automatically interpret commit messages to increment the version number appropriately. The commit messages should follow the standards from [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) that uses [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format). The basic idea is that using standardized commit messages the version number is determined automatically.

Commit messages should take for following form:

```
<type>(<scope>): <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope: some context, e.g. ci, search, formatting, db, page_name
  │                          
  └─⫸ Commit Type: fix|feat|build|ci|docs|perf|refactor|test
```



## Type

The main types are:

- **fix**: A bug fix  (patch --> X.X.X+)
- **feat**: A new feature (minor --> X.X+.X)



The following types are also allowed:

- **perf**: A code change that improves performance (patch --> X.X.X+)
- **ci**: Changes to our CI configuration files and scripts  (no new version)
- **docs**: Documentation only changes (no new version)
- **refactor**: A code change that neither fixes a bug nor adds a feature (no new version)
- **build**: Changes that affect the build system or external dependencies (no new version)
- **test**: Adding missing tests or correcting existing tests (no new version)



## Scope

Can be anything sensible that tells where the changes take place. A scope is encouraged but not mandatory.



## Tagging

You can [tag a commit](https://git-scm.com/book/en/v2/Git-Basics-Tagging) to causes specific version incrementation. Supported tags: 

*  Breaking: (major --> X+.X.X)
*  FEATURE: (minor --> X.X+.X)
*  Update: (minor --> X.X+.X)
*  New: (minor --> X.X+.X)
*  BUGFIX: (patch --> X.X.X+)
*  SECURITY: (patch --> X.X.X+)
*  Fix:  (patch --> X.X.X+)



## Breaking changes / Major versions

If "BREAKING CHANGE:" (first in a line, upper case) is in the commit description it is treated as a major release. Commits with `BREAKING CHANGE` in the commits, regardless of type, should be translated to `MAJOR` releases. Breaking Change section should start with the phrase "BREAKING CHANGE: " followed by a summary of the breaking change, a blank line, and a detailed description of the breaking change that also includes migration instructions.

```
feat: allow provided config object to extend other configs

BREAKING CHANGE: `extends` key in config file is now used for extending other config files
```

**Commit message with `!` to draw attention to breaking change**

```
feat!: send an email to the customer when a product is shipped
```

```
feat(api)!: send an email to the customer when a product is shipped
```



## When is semantic-release activated?

Only to commits on the master branch. Commits leading to a new version gets tagged with the version and a docker images with the version tagged is created. Other CI (docker image creation) is run on all branches. 




##  Do all my contributors need to use the Conventional Commits specification?

No! If you use a squash based workflow on Git lead maintainers can clean up the commit messages as they’re merged—adding no workload to casual committers. A common workflow for this is to have your git system automatically squash commits from a pull request and present a form for the lead maintainer to enter the proper git commit message for the merge.



## Revert commits

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit. The content of the commit message body should contain:

- information about the SHA of the commit being reverted in the following format: `This reverts commit <SHA>`,
- a clear description of the reason for reverting the commit message.
