# Installing docker

For most systems refer to the [official guides](https://docs.docker.com/get-docker/).

RedHat 8 ships with the alternative podman and docker is not in the official repositories. Read below for how to install docker in RedHat 8.


## RedHat 8

```bash
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
```

### Remove podman and install docker and a dependency

```bash
sudo yum erase podman buildah
sudo yum install docker-ce libseccomp-devel
```

### Enable docker daemon

```bash
sudo systemctl enable --now docker
```

### Check status
```bash
systemctl status docker
```


### Install docker compose
Check which version is available and adjust the link.

```bash
curl -L "https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-$(uname -s)-$(uname -m)" -o docker-compose

sudo mv docker-compose /usr/local/bin && sudo chmod +x /usr/local/bin/docker-compose
```

### Allow docker in the firewall

```bash
sudo firewall-cmd --permanent --zone=trusted  --add-interface=docker0 && sudo firewall-cmd --reload
```

### Test docker
```bash
sudo docker run hello-world
```



# Install DASH-IN docker

Make folder for database backups:

```bash
sudo mkdir /backup
```



Make folder for uploaded files:

```bash
sudo mkdir /squidr-storage
```



Download the docker compose file:

```bash
wget https://gitlab.com/nexs-metabolomics/dash-in-core/-/raw/docker/CI/docker-compose.yml
```



Then open the file in a text editor like nano and adjust:

1. the passwords
2. you *can* change the default port of 8080 if you want to access the app on another port.
```bash
nano docker-compose.yml
```



Then pull the docker images:

```bash
sudo /usr/local/bin/docker-compose --file docker-compose.yml --project-directory "." pull
```



To build and start the app run:

```bash
sudo /usr/local/bin/docker-compose --file docker-compose.yml --project-directory "." up --build --force-recreate
```



## Enable https/SSL

Close the app again by `CTRL+C` and continue the following.

The webserver in the docker image needs to be changed to enable HTTPS. This [guide](https://hub.docker.com/_/httpd) explains how which is summarized below.


Download a sample setup file:
```bash
sudo docker run --rm httpd:2.4 cat /usr/local/apache2/conf/httpd.conf > my-httpd.conf
```



Then modify it to enable HTTPS.

```bash
sed -i \
        -e 's/^#\(Include .*httpd-ssl.conf\)/\1/' \
        -e 's/^#\(LoadModule .*mod_ssl.so\)/\1/' \
        -e 's/^#\(LoadModule .*mod_socache_shmcb.so\)/\1/' \
         my-httpd.conf
```



Then you need to open again your docker compose file (`nano docker-compose.yml`) and add the `volumes` section so that the dashin section of the file contains the volume lines:

```yaml
ports:
  - "80:80"
  - "443:443"
volumes:
    - ./my-httpd.conf:/usr/local/apache2/conf/httpd.conf
restart: always
```



## Check installation locally

Restart the app as explained below and the test the app in another session.

```bash
curl -k https://localhost
```

# Update DASH-IN

Close the app again by `CTRL+C` and continue the following.
```bash
sudo /usr/local/bin/docker-compose --file docker-compose.yml --project-directory "." pull
```



## Update DB manually

Normal users should not need this!

Should you need to manually insert a function to the DB you can:
* Update the code first
* log into the DB docker container
* use `sudo docker ps` to get the container ID.
* `sudo docker exec -it b324883c029c bash`
* `psql -U postgres -d dashindb < studyvariable_guess_datatype.sql`


# Run or restart DASH-IN
```bash
sudo /usr/local/bin/docker-compose --file docker-compose.yml --project-directory "." up --build --force-recreate
```



Go to ```http://dashin.example.com``` and log in to the application with user-code ```usrab12345678``` and password ```admin```

# Delete the database

```bash
sudo /usr/local/bin/docker-compose down --volumes
```




# Initial login

Go to ```http://dashin.example.com``` and log in to the application with user-code ```usrab12345678``` and password ```admin```

