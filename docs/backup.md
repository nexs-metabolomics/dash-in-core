# Backup



## Create the backup

On the *source* machine find the container id (`sudo docker ps`) of the `dashin-db` container.

Then make the backup:

```bash
sudo docker exec CONTAINER_ID pg_dump -Fc -C -f mydb.dump -h localhost -U postgres dashindb
```

And then copy to the host:

```bash
sudo docker cp CONTAINER_ID:/dashin/install/mydb.dump .
```



## Restore

In the *target* machine first clear existing database (this clears all volumes made by docker-compose, so careful if there are other things on the machine):

```bash
sudo /usr/local/bin/docker-compose down --volumes
```



Restart the container and then copy the backup file to the docker container:

```bash
sudo docker cp mydb.dump CONTAINER_ID:/dashin/install/mydb.dump
```



Then restore inside the docker container:

```bash
sudo docker exec CONTAINER_ID pg_restore --clean -C -d postgres -h localhost -U postgres mydb.dump
```



