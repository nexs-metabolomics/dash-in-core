<?php

namespace Tests\Unit;

use App\Library\Security\RolePermission;
use Phalcon\Mvc\Application;
use Tests\Support\UnitTester;

class OtherClassesTest extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    protected string $userId = "9deee782-ba62-412e-91df-54e58138c174";

    #----------------------------------------------------------

    protected RolePermission $RolePerm;

    protected $method1;

    protected function _before()
    {
        #----------------------------------------------------------
        # initiralize
        #----------------------------------------------------------
        if (!defined('APP_PATH')) {
            define('APP_PATH', realpath(__DIR__ . '/..'));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        $this->di = new Application($di);

        #----------------------------------------------------------
        # set user
        #----------------------------------------------------------
        $this->di->SU->register($this->userId);

        #----------------------------------------------------------
        # init classes
        #----------------------------------------------------------
    }

    // tests
    
    #----------------------------------------------------------
    # OBS OBS
    # testing route permissions does not work because of
    # private functions
    # TODO: needs rewrite of route permission logic
    #----------------------------------------------------------
    
    public function testRolePermission()
    {

//        $RolePerm = new RolePermission($this->userId);
//        $this->tester->assertTrue($RolePerm->ALLOW_ROUTE);
        echo "\n======================================\n";
//        print_r($_SERVER);
//        var_dump($RolePerm->ALLOW_ROUTE);
//        var_dump($RolePerm->ROLE_APP_ADMIN);
//        var_dump($RolePerm->ROLE_ORG_ADMIN);
//        var_dump($RolePerm->ROLE_ORG_USER);
//        var_dump($RolePerm->ROLE_ORG_ALLOW_WRITE);
//        var_dump($RolePerm->ROLE_ORG_READONLY);
        echo "\n======================================\n";
        var_dump($this->di->router->getMatches());
        echo "\n======================================\n";
        ob_flush();
    }

}
