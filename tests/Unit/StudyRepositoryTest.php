<?php

namespace Tests\Unit;

use Tests\Support\UnitTester;

use Dashin\Helpers\Owner\StudyRepository;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Application;

class StudyRepositoryTest extends \Codeception\Test\Unit
{

    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var Injectable
     */
    protected Injectable $di;

    protected function _before()
    {

        if (!defined('APP_PATH')) {
            define('APP_PATH', dirname(__DIR__, 2));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        # set user
        $this->di = new Application($di);
        $this->di->SU->register("9deee782-ba62-412e-91df-54e58138c174");
    }

    protected function _after()
    {
    }

    /**
     * @var StudyRepository|null
     */
    private StudyRepository|null $_repository;

    /**
     * @return StudyRepository
     */
    private function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new StudyRepository();
        }

        return $this->_repository;
    }

    // tests
    public function testCountriesMinimal()
    {

        $result = $this->_getRepository()->getCountriesMinimal();

        $this->tester->assertIsArray($result);
        $this->tester->assertArrayHasKey(0, $result);
        $this->tester->assertArrayHasKey("country_id", $result[0]);
        $this->tester->assertArrayHasKey("short_name", $result[0]);
    }

    public function testConsortiumsMinimal()
    {

        $result = $this->_getRepository()->getConsortiumsMinimal();

        $this->tester->assertIsArray($result);
        $this->tester->assertArrayHasKey(0, $result);
        $this->tester->assertArrayHasKey("consortium_id", $result[0]);
        $this->tester->assertArrayHasKey("name", $result[0]);
        $this->tester->assertTrue(\uuid_is_valid($result[0]["consortium_id"]));
    }

    public function testResearchdesignsMinimal()
    {

        $result = $this->_getRepository()->getResearchdesignsMinimal();

        $this->tester->assertArrayHasKey(0, $result);
        $this->tester->assertArrayHasKey("researchdesign_id", $result[0]);
        $this->tester->assertArrayHasKey("name", $result[0]);
        $resNames = array_column($result, "name");
        $this->tester->assertTrue(in_array("Other", $resNames));
    }

    public function testStudyRoles()
    {

        $result = $this->_getRepository()->getStudyRoles();

        $this->tester->assertArrayHasKey(0, $result);
        $this->tester->assertArrayHasKey("study_role_id", $result[0]);
        $this->tester->assertArrayHasKey("name", $result[0]);
        $resNames = array_column($result, "name");
        $this->tester->assertTrue(in_array("Unknown", $resNames));
        $this->tester->assertTrue(in_array("Data submission", $resNames));
        $this->tester->assertTrue(in_array("Principal Investigator", $resNames));
        $this->tester->assertTrue(in_array("Scientist involved in study", $resNames));
    }

    public function xtestStudyDatasetsForView()
    {

        $this->tester->seeInSession("session_user");
        $this->tester->assertEquals($this->di->SU->getUserId(), "9deee782-ba62-412e-91df-54e58138c174");

        $result = $this->_getRepository()->getStudyDatasetsForView('8c846159-ef6c-469b-9406-17ad50ab4a19');

        $this->tester->assertIsArray($result);
        $this->tester->assertArrayHasKey(0, $result);
        $this->tester->assertArrayHasKey("nrows", $result[0]);
        $this->tester->assertTrue($result[0]["nrows"] > 0);
    }

}