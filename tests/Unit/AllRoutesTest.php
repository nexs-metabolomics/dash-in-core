<?php

namespace Tests\Unit;

use Tests\Support\UnitTester;

use Phalcon\Mvc\Application;

class AllRoutesTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

        if (!defined('APP_PATH')) {
            define('APP_PATH', realpath(__DIR__ . '/..'));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        $this->di = new Application($di);
    }

    protected function _setTestuser1()
    {

        $this->userCode = "USRPE-8449-2484";
        $this->password = "testuser1234";
        $this->userid = "5ec1957a-11e4-4155-be43-6f4edbf7de61";
        
        $this->managerOrgId = "3e9ee213-5f8d-423c-80d5-6e310bf41630";
    }

//USRPE-8449-2484
    protected function _after()
    {
    }

    // tests
    public function testAllRoutes()
    {

//        $this->di->router->handle("/admin/organization/index/");

//        $this->tester->amOnRoute("/admin/organization/index/");
//        $x = $this->di->router->getRoutes();
//        file_put_contents("all_routes.csv", "uri,action\n");
//        foreach ($x as $route) {
//            $paths = $route->getPaths();
//            $n = $paths["namespace"];
//            $c = $paths["controller"];
//            $a = $paths["action"];
//            $path = "$n\\$c\\$a";
//            $pattern = str_replace(['(', ')', '?'], ['', '', ''], $route->getPattern());
//            file_put_contents("all_routes.csv", "$pattern,$path\n", FILE_APPEND);
//        }

    }

}