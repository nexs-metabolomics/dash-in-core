<?php


namespace Tests\Unit;

use Phalcon\Mvc\Application;
use Tests\Support\UnitTester;

class StudyInfoTest extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    protected function _before()
    {
        if (!defined('APP_PATH')) {
            define('APP_PATH', dirname(__DIR__, 2));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        # set user
        $this->di = new Application($di);
        $this->di->SU->register("9deee782-ba62-412e-91df-54e58138c174");
    }

    // tests
    public function testStudyinfoRenaming()
    {
        $this->tester->amOnRoute();
    }
}
