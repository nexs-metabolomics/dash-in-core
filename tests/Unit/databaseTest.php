<?php

namespace Tests\Unit;

use Tests\Support\UnitTester;

use Phalcon\Db\Enum;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Application;
use Codeception\Test\Unit;
use App\Library\Utils\UniqueId;

use Dashin\Helpers\Owner\StudyRepository;

class databaseTest extends Unit
{

    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var Injectable
     */
    protected Injectable $di;

    protected function _before()
    {

        if (!defined('APP_PATH')) {
            define('APP_PATH', realpath(__DIR__ . '/..'));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();
        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        $this->di = new Application($di);
    }

    protected function _after()
    {
    }

    public function testTestingUniqueId()
    {

        $this->tester->wantTo("Create unit tests");
        $res = UniqueId::uuidValidate("fc919551-2e4e-4d34-b1e0-1abaebd969a9");
        $this->tester->assertEquals(1, $res);
        $res = uuid_is_valid("fc919551-2e4e-4d34-b1e0-1abaebd969a9");
        $this->tester->assertTrue($res);
    }

    public function testDatabaseConnection()
    {

        $res = $this->di->db->fetchOne("SELECT 2 AS test_result", Enum::FETCH_ASSOC);
        $this->tester->assertArrayHasKey("test_result", $res);
        $this->tester->assertEquals(2, $res["test_result"]);
    }

    public function testStudyRepository()
    {

        $StudyRepository = new StudyRepository();
        $result = $StudyRepository->getCountriesMinimal();
        $this->tester->assertEquals(44, count($result));
    }

}