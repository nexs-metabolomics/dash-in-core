<?php

namespace Tests\Unit;

use Phalcon\Di\Injectable;
use Phalcon\Mvc\Application;
use System\Helpers\Admin\AdminRepositoryBase;
use System\Helpers\Admin\ManagerRepository;
use System\Helpers\Admin\OrganizationRepository;
use System\Helpers\Admin\UserRepository;
use System\Helpers\Login\LoginRepository;
use System\Helpers\Service\ServiceRepository;
use System\Helpers\Sysadmin\RoleaclRepository;
use System\Helpers\Sysadmin\RoleRepository;

//use System\Helpers\Sysadmin\SysadmRepository;
use System\Helpers\User\HomeRepository;
use System\Library\SessionUser\SessionUserRepository;
use Tests\Support\UnitTester;

class QueryRefactorTest extends \Codeception\Test\Unit
{

    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    /**
     * @var Injectable
     */
    protected Injectable $di;

    protected AdminRepositoryBase $AdminRepoBase;

    protected ManagerRepository $ManagerRepo;

    protected OrganizationRepository $OrgRepo;

    protected UserRepository $UserRepo;

    protected LoginRepository $LoginRepo;

    protected ServiceRepository $ServiceRepo;

    protected RoleaclRepository $RoleaclRepo;

    protected RoleRepository $RoleRepo;

//    protected SysadmRepository $SysadmRepo;
    protected HomeRepository $HomeRepo;

    protected SessionUserRepository $SUserRepo;

    protected string $organizationId = "1de448a2-f4f1-4491-b6a5-a7064f9090d9";

    protected string $userId = "9deee782-ba62-412e-91df-54e58138c174";

    protected string $roleaclIdRoot = "0f224d2b-7220-4fe3-adfb-f80618b2d433";

    protected function _before()
    {

        #----------------------------------------------------------
        # initiralize
        #----------------------------------------------------------
        if (!defined('APP_PATH')) {
            define('APP_PATH', realpath(__DIR__ . '/..'));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        $this->di = new Application($di);
        #----------------------------------------------------------
        # set user
        #----------------------------------------------------------
        $this->di->SU->register($this->userId);

        #----------------------------------------------------------
        # load classes
        #----------------------------------------------------------
        $this->AdminRepoBase = new AdminRepositoryBase();
        $this->ManagerRepo = new ManagerRepository();
        $this->OrgRepo = new OrganizationRepository();
        $this->UserRepo = new UserRepository();
        $this->LoginRepo = new LoginRepository();
        $this->ServiceRepo = new ServiceRepository();
        $this->RoleaclRepo = new RoleaclRepository();
        $this->RoleRepo = new RoleRepository();
//        $this->SysadmRepo = new SysadmRepository();
        $this->HomeRepo = new HomeRepository();
        $this->SUserRepo = new SessionUserRepository();
    }

    // tests
    /*
    public function testAdminRepositoryBaseId()
    {

        $result = $this->AdminRepoBase->getUserMinimalByIdent("9deee782-ba62-412e-91df-54e58138c174");
        $this->tester->assertIsArray($result);
    }

    public function testAdminRepositoryBaseCode1()
    {

        $result = $this->AdminRepoBase->getUserMinimalByIdent("USRAB-1234-5678");
        $this->tester->assertIsArray($result);
    }

    public function testAdminRepositoryBaseCode2()
    {

        $result = $this->AdminRepoBase->getUserMinimalByIdent("usrab12345678");
        $this->tester->assertIsArray($result);
    }

    public function testAdminRepositoryBaseEmail()
    {

        $result = $this->AdminRepoBase->getUserMinimalByIdent("admin@squidr.example.com");
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetOrgRoles()
    {

        $result = $this->ManagerRepo->getOrganizationRoles($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetManager()
    {

        $result = $this->ManagerRepo->getManager($this->organizationId, $this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetManagerRoles()
    {

        $result = $this->ManagerRepo->getManagerRoles($this->organizationId, $this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetOrganization()
    {

        $result = $this->ManagerRepo->getOrganization($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetOrganizations()
    {

        $result = $this->ManagerRepo->getOrganizations();
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetOrganizationsSearch()
    {

        $result = $this->ManagerRepo->getOrganizations("admin");
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetOrganizationsSearchNone()
    {

        $result = $this->ManagerRepo->getOrganizations("--x--y");
        $this->tester->assertIsNotArray($result);
    }

    public function testManagerRepoGetUser()
    {

        $result = $this->ManagerRepo->getUser($this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetUsers()
    {

        $result = $this->ManagerRepo->getUsers('');
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetUsersSearch()
    {

        $result = $this->ManagerRepo->getUsers('admin');
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetUsersSearchNone()
    {

        $result = $this->ManagerRepo->getUsers('--x--y--');
        $this->tester->assertIsNotArray($result);
    }

    public function testManagerRepoGetManagers()
    {

        $result = $this->ManagerRepo->getManagers('');
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetManagersSearch()
    {

        $result = $this->ManagerRepo->getManagers('admin');
        $this->tester->assertIsArray($result);
    }

    public function testManagerRepoGetManagersSearchNone()
    {

        $result = $this->ManagerRepo->getManagers('--x--y--');
        $this->tester->assertIsNotArray($result);
    }
    
    # test to create later:
    # createManager: needs create organization or create user
    # updateManager: needs some work

    
    public function testOrganizationRepoGetManagersByOrganization()
    {

        $result = $this->OrgRepo->getManagersByOrganization($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    # createOrganization
    # updateOrganization

    public function testOrganizationRepoGetOrganization()
    {

        $result = $this->OrgRepo->getOrganization($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    public function testOrganizationRepoGetOrganizations()
    {

        $result = $this->OrgRepo->getOrganizations();
        $this->tester->assertIsArray($result);
    }
    public function testOrganizationRepoGetOrganizationsSearch()
    {

        $result = $this->OrgRepo->getOrganizations("admin");
        $this->tester->assertIsArray($result);
    }

    public function testOrganizationRepoGetOrganizationsSearchNone()
    {

        $result = $this->OrgRepo->getOrganizations("--x--y--");
        $this->tester->assertIsNotArray($result);
    }

    public function testOrganizationRepoGetSelectedRoles()
    {

        $result = $this->OrgRepo->getSelectedRoles($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    public function testOrganizationRepoGetUnelectedRoles()
    {

        $result = $this->OrgRepo->getUnselectedRoles($this->organizationId);
        $this->tester->assertIsArray($result);
    }
    
    # add role
    # remove role
    
    public function testOrganizationRepoGetOrganizationRoles()
    {

        $result = $this->OrgRepo->getOrganizationRoles($this->organizationId);
        $this->tester->assertIsArray($result);
    }

    public function testOrganizationRepoGetManager()
    {

        $result = $this->OrgRepo->getManager($this->organizationId,$this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testUserRepoGetManagerByUserId()
    {

        $result = $this->UserRepo->getManagersByUserId($this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testUserRepoCreateResetToken()
    {

        $result = $this->UserRepo->createResetToken($this->userId);
        $this->tester->assertTrue(uuid_is_valid($result));
        $this->tester->assertIsString($result);
        $result = $this->UserRepo->getResetToken($this->userId);
        $this->tester->assertIsArray($result);
    }
    
    # createUser
    # createUserWithOrganization
    # updateUser

    public function testUserRepoGetUser()
    {

        $result = $this->UserRepo->getUser($this->userId);
        $this->tester->assertIsArray($result);
    }
    
    public function testUserRepoGetResetToken()
    {

        $result = $this->UserRepo->getResetToken($this->userId);
        $this->tester->assertIsArray($result);
    }
    
    public function testUserRepoGetUsers()
    {

        $result = $this->UserRepo->getUsers();
        $this->tester->assertIsArray($result);
    }
    
    public function testLoginRepoGetEmailTemplate()
    {

        $result = $this->LoginRepo->getEmailTemplate("email_reset_pw", "en");
        $this->tester->assertIsArray($result);
        $this->tester->assertCount(2, $result);
        $this->tester->assertArrayHasKey("subject", $result, "Array is missing key 'subject'");
        $this->tester->assertArrayHasKey("body", $result, "Array is missing key 'body'");
    }

    public function testLoginRepoGetUser()
    {

        $result = $this->LoginRepo->getUser($this->userId);
        $this->tester->assertIsArray($result);
    }

    public function testLoginRepoGetUserById()
    {

        $result = $this->LoginRepo->getUserByIdent("9deee782-ba62-412e-91df-54e58138c174");
        $this->tester->assertIsArray($result);
    }

    public function testLoginRepoGetUserByCode1()
    {

        $result = $this->LoginRepo->getUserByIdent("USRAB-1234-5678");
        $this->tester->assertIsArray($result);
    }

    public function testLoginRepoGetUserByCode2()
    {

        $result = $this->LoginRepo->getUserByIdent("usrab12345678");
        $this->tester->assertIsArray($result);
    }

    public function testLoginRepoGetUserByEmail()
    {

        $result = $this->LoginRepo->getUserByIdent("admin@squidr.example.com");
        $this->tester->assertIsArray($result);
    }

    public function testLoginRepoCreateResetToken()
    {

        $result = $this->LoginRepo->createResetToken($this->userId);
        $this->tester->assertTrue(uuid_is_valid($result));

        $resetToken = $result;

        $result = $this->LoginRepo->getUserByResetToken($resetToken);
        $this->tester->assertIsArray($result);

        return $resetToken;

    }

    /**
     * @depends testLoginRepoCreateResetToken
     * @return void
     */
    /*    public function testLoginRepoGetUserByResetToken($resetToken)
        {
    
            $this->tester->assertTrue(uuid_is_valid($resetToken),"resetToken: ($resetToken)");
    
            $result = $this->LoginRepo->getUserByResetToken($resetToken);
            $this->tester->assertIsArray($result);
    
            $this->tester->assertCount(2,$result);
            $this->tester->assertTrue(uuid_is_valid($result["user_id"]));
            $this->tester->assertTrue($result["user_id"] == $this->userId);
        }
    
        # setUserPassword

    public function testAdminServiceRepoGetPaginatedUser()
    {

        $result = $this->ServiceRepo->getPaginatedUser(1, 10, '');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);

    }

    public function testAdminServiceRepoGetPaginatedUserSearch()
    {

        $result = $this->ServiceRepo->getPaginatedUser(1, 10, 'admin');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);
        $this->tester->assertGreaterOrEquals(1, count($result->items));

    }

    public function testAdminServiceRepoGetPaginatedUserSearchNone()
    {

        $result = $this->ServiceRepo->getPaginatedUser(1, 10, '--x--y--');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);
        $this->tester->assertCount(0, $result->items);
        
    }

    public function testAdminServiceRepoGetPaginatedOrganization()
    {

        $result = $this->ServiceRepo->getPaginatedOrganization(1, 10, '');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);

    }

    public function testAdminServiceRepoGetPaginatedOrganizationSearch()
    {

        $result = $this->ServiceRepo->getPaginatedOrganization(1, 10, 'admin');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);
        $this->tester->assertGreaterOrEquals(1, count($result->items));

    }

    public function testAdminServiceRepoGetPaginatedOrganizationSearchNone()
    {

        $result = $this->ServiceRepo->getPaginatedOrganization(1, 10, '--x--y--');
        $this->tester->assertIsObject($result);
        $this->tester->assertIsArray($result->items);
        $this->tester->assertCount(0, $result->items);
        
    }

    #----------------------------------------------------------
    # RoleaclRepository
    #----------------------------------------------------------
    # missing tests:
    #--------------------
    # createRoleacl
    # updateRoleacl
    # deleteRoleacl
    #----------------------------------------------------------

    public function testRoleAclRepoGetRoles()
    {

        $result = $this->RoleaclRepo->getRoles();
        $this->tester->assertIsArray($result);

    }

    public function testRoleAclRepoGetRoleacls()
    {

        $result = $this->RoleaclRepo->getRoleacls();
        $this->tester->assertIsArray($result);

    }

    public function testRoleAclRepoGetRoleacl()
    {

        $result = $this->RoleaclRepo->getRoleacl($this->roleaclIdRoot);
        $this->tester->assertIsArray($result);
        $this->tester->assertArrayHasKey("class_allow", $result);
        
        $this->tester->assertTrue($result["class_allow"] == "*", "class_allow != '*' (" . print_r($result, true) . ")");
        $this->tester->assertTrue($result["name"] == "Root", "Name != 'Root' (" . print_r($result, true) . ")");

    }

    public function testRoleAclRepoGetRoleaclForEdit()
    {

        $result = $this->RoleaclRepo->getRoleaclForEdit($this->roleaclIdRoot);
        $this->tester->assertIsArray($result);

    }

    #----------------------------------------------------------
    # RoleRepository
    #----------------------------------------------------------
    # missing tests:
    #--------------------
    # createRole
    # createAccessRule
    # updateAccessRule
    # changeRoleHierarchy
    #----------------------------------------------------------
    public function testRoleAclRepoGetRoles()
    {

        $result = $this->RoleRepo->getRoles();
        $this->tester->assertIsArray($result);

    }

    public function testRoleAclRepoGetRole()
    {

        $result = $this->RoleRepo->getRole(1);
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testRoleAclRepoGetRoleaclFromRole()
    {

        $result = $this->RoleRepo->getRoleaclFromRole(510);
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testRoleAclRepoGetAccessRules()
    {

        $result = $this->RoleRepo->getAccessRules();
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testRoleAclRepoGetAccessRule()
    {

        $result = $this->RoleRepo->getAccessRule($this->roleaclIdRoot);
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testRoleAclRepoGetRolesForHierarchyRootIncluded()
    {

        $result = $this->RoleRepo->getRolesForHierarchy(1,"included");
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    #----------------------------------------------------------
    # SysadmRepository
    #----------------------------------------------------------
    # missing tests:
    #--------------------
    # updateUris
    # getUris
    # getUriNamespaces
    # getUriControllers
    #----------------------------------------------------------

    #----------------------------------------------------------
    # HomeRepository
    #----------------------------------------------------------
    # missing tests:
    #--------------------
    # updateUser
    # 
    #----------------------------------------------------------

    public function testHomeRepoGetUserForView()
    {

        $result = $this->HomeRepo->getUserForView();
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testHomeRepoGetManagersByUser()
    {

        $result = $this->HomeRepo->getManagersByUser();
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testHomeRepoGetManagerInfo()
    {

        $result = $this->HomeRepo->getManagerInfo();
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testHomeRepoGetUser()
    {

        $result = $this->HomeRepo->getUser($this->userId);
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    //    */
    #----------------------------------------------------------
    # SessionUserRepository
    #----------------------------------------------------------
    # missing tests:
    #--------------------
    # setActiveManager
    # unsetActiveManager
    #----------------------------------------------------------
    public function testSUserRepoGetUser()
    {

        # [12, 100, 11, 1, 590, 500, 10, 510]
        $result = $this->SUserRepo->getUser($this->userId);
        $this->tester->assertIsArray($result);
//        print_r($result);
//        ob_flush();

    }

    public function testSUserRepoGetActiveManagerOrgId()
    {

        $result = $this->SUserRepo->getActiveManagerOrgId();
        $this->tester->assertTrue(uuid_is_valid($result));
//        echo "\n==============================\n$result\n==============================\n";
//        ob_flush();

    }

    public function testSUserRepoGetOrganization()
    {

        $organizationId = $this->SUserRepo->getActiveManagerOrgId();
        $this->tester->assertTrue(uuid_is_valid($organizationId));
//        echo "\n==============================\n$result\n==============================\n";
        $result = $this->SUserRepo->getOrganization($organizationId);
        $this->tester->assertIsArray($result);
//        echo "\n==============================\n";
//        print_r($result);
//        ob_flush();

    }

    public function testSUserRepoGetManagerRoles()
    {

        $result = $this->SUserRepo->getManagerRoles();
        $this->tester->assertIsArray($result);
//        echo "\n==============================\n";
//        print_r($result);
//        ob_flush();

    }

    public function testSUserRepoActionAllow()
    {

//        $result = $this->SUserRepo->actionAllow([1,10],);
//        $this->tester->assertIsArray($result);
//        echo "\n==============================\n";
//        print_r($result);
//        ob_flush();

    }

}
