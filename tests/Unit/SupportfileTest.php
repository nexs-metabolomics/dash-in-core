<?php

namespace Tests\Unit;

use App\Library\Utils\SupportFileRepository;
use Dashin\Helpers\Owner\AssayHelper;
use Dashin\Helpers\Owner\ImportHelper;
use Dashin\Helpers\Owner\StudyHelper;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Application;
use Tests\Support\UnitTester;

class SupportfileTest extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    /**
     * @var Injectable
     */
    protected Injectable $di;

    protected function _before()
    {

        if (!defined('APP_PATH')) {
            define('APP_PATH', realpath(__DIR__ . '/..'));
        }
        putenv("APPLICATION_CONTEXT=testing");

        $config = include APP_PATH . "/app/00init/config.php";
        include APP_PATH . "/app/00init/loader.php";

        $di = new \Phalcon\DI\FactoryDefault();

        include APP_PATH . "/app/00init/services.php";
        include APP_PATH . "/app/admin_modules/System/Config/services.php";
        include APP_PATH . "/app/modules/Dashin/Config/services.php";

        # set user
        $this->di = new Application($di);
        $this->di->SU->register("9deee782-ba62-412e-91df-54e58138c174");
    }

    /**
     * @var AssayHelper
     */
    private $_assayHelper;

    protected function _getAssayHelper()
    {

        if (!$this->_assayHelper) {
            $this->_assayHelper = new AssayHelper();
        }

        return $this->_assayHelper;
    }

    /**
     * @var StudyHelper
     */
    private $_studyHelper;

    protected function _getStudyHelper()
    {

        if (!$this->_studyHelper) {
            $this->_studyHelper = new StudyHelper();
        }

        return $this->_studyHelper;
    }

    /**
     * @var ImportHelper
     */
    private $_datasetHelper;

    protected function _getDatasetHelper()
    {

        if (!$this->_datasetHelper) {
            $this->_datasetHelper = new ImportHelper();
        }

        return $this->_datasetHelper;
    }

    /**
     * @var SupportFileRepository
     */
    private $_repository;

    /**
     * @return SupportFileRepository
     */
    protected function _getRepository()
    {

        if (!$this->_repository) {
            $this->_repository = new SupportFileRepository();
        }

        return $this->_repository;
    }

    // tests

    public function testSupportFileRepositoryGetSupportFileInfo()
    {

        # set to random uuid
        # returns false if table is accessible, otherwise an exception 
        $supportfileId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getSupportFileInfo($supportfileId);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryGetStudyInfo()
    {

        # set to random uuid
        # returns false if table is accessible, otherwise an exception 
        $supportfileId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getStudyInfo($supportfileId);
        $this->tester->assertFalse($result);
    }

    public function testSupportFileRepositoryGetDatasetInfo()
    {

        # set to random uuid
        # returns false if table is accessible, otherwise an exception 
        $supportfileId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getDatasetInfo($supportfileId);
        $this->tester->assertFalse($result);
    }

    public function testSupportFileRepositoryUpdateFileDescription()
    {

        # set to random uuid
        # returns false if table is accessible, otherwise an exception 
        $supportfileId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->updateFileDescription($supportfileId, '');
        $this->tester->assertTrue($result);
    }

    public function testSupportFileRepositoryRegisterStudyFile()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $fileInfo = [
            "name"          => "unit-test name",
            "filename"      => "unit-test filename",
            "fileextension" => "unit-test extension",
            "mimetype"      => "unit-test mimetype",
        ];

        $result = $this->_getRepository()->registerStudyFile($randomId, $fileInfo);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryGetStudyFileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getStudyfileList($randomId);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryUnregisterFileFromStudy()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->unregisterFileFromStudy($randomId, $randomId);
        $this->tester->assertTrue($result);

    }

    public function testSupportFileRepositoryRegisterDatasetFile()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $fileInfo = [
            "name"          => "unit-test name",
            "filename"      => "unit-test filename",
            "fileextension" => "unit-test extension",
            "mimetype"      => "unit-test mimetype",
        ];

        $result = $this->_getRepository()->registerDatasetFile($randomId, $fileInfo);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryUnregisterFileFromDataset()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->unregisterFileFromDataset($randomId, $randomId);
        $this->tester->assertTrue($result);

    }

    public function testSupportFileRepositoryGetDatasetfileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getDatasetfileList($randomId);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryRegisterAssayFile()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $fileInfo = [
            "name"          => "unit-test name",
            "filename"      => "unit-test filename",
            "fileextension" => "unit-test extension",
            "mimetype"      => "unit-test mimetype",
        ];

        $result = $this->_getRepository()->registerAssayFile($randomId, $fileInfo);
        $this->tester->assertFalse($result);

    }

    public function testSupportFileRepositoryUnregisterFileFromAssay()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->unregisterFileFromAssay($randomId, $randomId);
        $this->tester->assertTrue($result);

    }

    public function testSupportFileRepositoryGetAssayfileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $result = $this->_getRepository()->getAssayfileList($randomId);
        $this->tester->assertFalse($result);

    }

    #-----------------------------------------------------------------------
    # assay helper
    #-----------------------------------------------------------------------
    public function testAssayHelperSupportfileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getAssayHelper()->supportFileList($randomId);
        $this->tester->assertArrayHasKey("assay_info", $results);
        $this->tester->assertArrayHasKey("assayfile_list", $results);
        $this->tester->assertFalse($results["assayfile_list"]);
        $this->tester->assertFalse($results["assay_info"]);
    }

    public function testAssayHelperSupportfileEdit()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getAssayHelper()->supportFileEdit($randomId, $randomId);
        $this->tester->assertArrayHasKey("assay_info", $results);
        $this->tester->assertArrayHasKey("supportfile_info", $results);
        $this->tester->assertArrayHasKey("form", $results);
        $this->tester->assertFalse($results["assay_info"]);
        $this->tester->assertFalse($results["supportfile_info"]);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\SupportFileEditForm');

    }

    public function testAssayHelperSupportfileUpload()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';
        $results = $this->_getAssayHelper()->supportFileUpload($randomId);

        $this->tester->assertArrayHasKey("assay_info", $results);
        $this->tester->assertFalse($results["assay_info"]);

        $this->tester->assertArrayHasKey("form", $results);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\UploadSupportFileForm');
    }

    #-----------------------------------------------------------------------
    # dataset helper
    #-----------------------------------------------------------------------
    public function testDatasetHelperSupportfileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getDatasetHelper()->supportFileList($randomId);
        $this->tester->assertArrayHasKey("dataset_info", $results);
        $this->tester->assertArrayHasKey("datasetfile_list", $results);
        $this->tester->assertFalse($results["datasetfile_list"]);
        $this->tester->assertFalse($results["dataset_info"]);
    }

    public function testDatasetHelperSupportfileEdit()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getDatasetHelper()->supportFileEdit($randomId, $randomId);
        $this->tester->assertArrayHasKey("dataset_info", $results);
        $this->tester->assertArrayHasKey("supportfile_info", $results);
        $this->tester->assertArrayHasKey("form", $results);
        $this->tester->assertFalse($results["dataset_info"]);
        $this->tester->assertFalse($results["supportfile_info"]);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\SupportFileEditForm');

    }

    public function testDatasetHelperSupportfileUpload()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';
        $results = $this->_getDatasetHelper()->supportFileUpload($randomId);

        $this->tester->assertArrayHasKey("dataset_info", $results);
        $this->tester->assertFalse($results["dataset_info"]);

        $this->tester->assertArrayHasKey("form", $results);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\UploadSupportFileForm');
    }

    #-----------------------------------------------------------------------
    # study helper
    #-----------------------------------------------------------------------
    public function testStudyHelperSupportfileList()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getStudyHelper()->supportFileList($randomId);
        $this->tester->assertArrayHasKey("study_info", $results);
        $this->tester->assertArrayHasKey("studyfile_list", $results);
        $this->tester->assertFalse($results["studyfile_list"]);
        $this->tester->assertFalse($results["study_info"]);
    }

    public function testStudyHelperSupportfileEdit()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';

        $results = $this->_getStudyHelper()->supportFileEdit($randomId, $randomId);
        $this->tester->assertArrayHasKey("study_info", $results);
        $this->tester->assertArrayHasKey("supportfile_info", $results);
        $this->tester->assertArrayHasKey("form", $results);
        $this->tester->assertFalse($results["study_info"]);
        $this->tester->assertFalse($results["supportfile_info"]);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\SupportFileEditForm');

    }

    public function testStudyHelperSupportfileUpload()
    {

        $randomId = '5405054f-1531-4e62-8745-8eb2b72a8e94';
        $results = $this->_getStudyHelper()->supportFileUpload($randomId);

        $this->tester->assertArrayHasKey("study_info", $results);
        $this->tester->assertFalse($results["study_info"]);

        $this->tester->assertArrayHasKey("form", $results);
        $className = get_class($results["form"]);
        $this->tester->assertTrue($className == 'Dashin\Forms\Owner\UploadSupportFileForm');
    }

}
