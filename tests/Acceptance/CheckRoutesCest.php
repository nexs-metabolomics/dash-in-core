<?php

namespace Tests\Acceptance;

use App\Library\Utils\LocalSession;
use Tests\Support\AcceptanceTester;

class CheckRoutesCest
{


    public function _before(AcceptanceTester $I)
    {

        $this->user = "usrab12345678";
        $this->password = "admin";
        $this->orgid = "c9af6c9d-84e5-4f45-8ab8-6aaa0abcabbe";
        $this->filepath = "search-test-data-03-glucose.csv";
        $this->localsession = new LocalSession();
        $I->amOnPage('/login');
        $I->submitForm('form#form1', ['username' => $this->user, 'password' => $this->password]);
        $this->sessionCookie = $I->grabCookie("PHPSESSID");

    }

    // tests

    public function routesSysadminTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - Sysadmin");

        #---------------------------------------------
        # sysadmin
        #---------------------------------------------
        $I->amOnPage("/sysadmin/index/");
        $I->seeInTitle("System Administration");

    }

    public function routesRoleaclTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - roleacl");
        #---------------------------------------------
        # roleacl
        #---------------------------------------------
        $I->amOnPage("/sysadmin/roleacl/index/");
        $I->seeInTitle("System Administration");

        $I->amOnPage("/sysadmin/roleacl/create/");
        $I->seeInTitle("System Administration");

        $I->amOnPage("/sysadmin/roleacl/edit/");
        $I->seeInTitle("System Administration");

        $I->amOnPage("/sysadmin/roleacl/delete/");
        $I->seeInTitle("Delete access rule");

        $I->amOnPage("/sysadmin/roleacl/list/");
        $I->seeInTitle("List access rule");

    }

    public function routesAccessruleTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - accessrule");
        #---------------------------------------------
        # access rule
        #---------------------------------------------
        $I->amOnPage("/sysadmin/accessrule/view/");
        $I->seeInTitle("View access rules");

        $I->amOnPage("/sysadmin/accessrule/create/");
        $I->seeInTitle("Create Access Rule");

    }

    public function routesRoleTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - role");
        #---------------------------------------------
        # role
        #---------------------------------------------
        $I->amOnPage("/sysadmin/role/index/");
        $I->seeInTitle("System Administration");

        $I->amOnPage("/sysadmin/role/list/");
        $I->seeInTitle("List Roles");

        $I->amOnPage("/sysadmin/role/create/");
        $I->seeInTitle("Create Role");

        $I->amOnPage("/sysadmin/role/view/");
        $I->seeInTitle("View Role");

        $I->amOnPage("/sysadmin/role/edit/");
        $I->seeInTitle("Edit Role");

    }

    public function routesAdminTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - admin");
        #---------------------------------------------
        # admin 
        #---------------------------------------------
        $I->amOnPage("/admin/index/");
        $I->seeInTitle("Sysadmin module");

    }

    public function routesAdminOrganizationTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - admin / organization");
        #---------------------------------------------
        # admin organization 
        #---------------------------------------------
        $I->amOnPage("/admin/organization/index/");
        $I->seeInTitle("List Organizations");
        $I->amOnPage("/admin/organization/list/");
        $I->seeInTitle("List Organizations");

        $I->amOnPage("/admin/organization/create/");
        $I->seeInTitle("Create Organization");

        $I->amOnPage("/admin/organization/edit/");
        $I->seeInTitle("Edit Organization");

        $I->amOnPage("/admin/organization/view/");
        $I->seeInTitle("View Organization");

        $I->amOnPage("/admin/organization/editroles/");
        $I->seeInTitle("Edit Organization Permissions");

    }

    public function routesAdminUserTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - admin / user");
        #---------------------------------------------
        # admin user
        #---------------------------------------------
        $I->amOnPage("/admin/user/index/");
        $I->seeInTitle("Users");

        $I->amOnPage("/admin/user/create/");
        $I->seeInTitle("Create User");

        $I->amOnPage("/admin/user/createwithorg/");
        $I->seeInTitle("Create user with organization");

        $I->amOnPage("/admin/user/edit/");
        $I->seeInTitle("Edit User");

        $I->amOnPage("/admin/user/view/");
        $I->seeInTitle("View User");

        $I->amOnPage("/admin/user/list/");
        $I->seeInTitle("List User");
    }

    public function routesAdminManagerTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - admin / manager");
        #---------------------------------------------
        # admin manager
        #---------------------------------------------
        $I->amOnPage("/admin/manager/index/");
        $I->seeInTitle("Managers");

        $I->amOnPage("/admin/manager/create/");
        $I->seeInTitle("Create Manager");

        $I->amOnPage("/admin/manager/edit/");
        $I->seeInTitle("Edit Manager");

        $I->amOnPage("/admin/manager/list/");
        $I->seeInTitle("List Managers");

        $I->amOnPage("/admin/manager/view/");
        $I->seeInTitle("View Manager");

    }

    public function routesUserTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - user");
        #---------------------------------------------
        # user
        #---------------------------------------------
        $I->amOnPage("/user/");
        $I->seeInTitle("My account");
        $I->amOnPage("/user/index/");
        $I->seeInTitle("My account");

        $I->amOnPage("/user/view/");
        $I->seeInTitle("Select Manager Role");

        $I->amOnPage("/user/edit/");
        $I->seeInTitle("Edit profile");

    }

    public function routesImportTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - impoer");
        #---------------------------------------------
        # squidr import
        #---------------------------------------------
        $I->amOnPage("/dashin/index/");
        $I->seeInTitle("Data sharing");

        $I->amOnPage("/dashin/owner/import/index/");
        $I->seeInTitle("Datasets");

        $I->amOnPage("/dashin/owner/import/uploadsummary/");
        $I->seeInTitle("Dataset summary");

        $I->amOnPage("/dashin/owner/import/listdatasets/");
        $I->seeInTitle("List datasets");

        $I->amOnPage("/dashin/owner/import/uploaddata/");
        $I->seeInTitle("Upload dataset");

        $I->amOnPage("/dashin/owner/import/editdataset/");
        $I->seeInTitle("Edit dataset");

        $I->amOnPage("/dashin/owner/import/listvariables/");
        $I->seeInTitle("Variable properties");

        $I->amOnPage("/dashin/owner/import/editvariable/");
        $I->seeInTitle("Edit variable");

        $I->amOnPage("/dashin/owner/import/browsedata/");
        $I->seeInTitle("Browse dataset");

        $I->amOnPage("/dashin/owner/import/viewdatadesign/");
        $I->seeInTitle("View dataset design");

    }

    public function routesImportguideTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - importguide");
        #---------------------------------------------
        # squidr importguide
        #---------------------------------------------
        $I->amOnPage("/dashin/owner/importguide/index/");
        $I->seeInTitle("Datasets");

        $I->amOnPage("/dashin/owner/importguide/index/");
        $I->seeInTitle("Datasets");

        $I->amOnPage("/dashin/owner/importguide/upload/");
        $I->seeInTitle("Upload data file");

        $I->amOnPage("/dashin/owner/importguide/import/");
        $I->seeInTitle("Import data");

        $I->amOnPage("/dashin/owner/importguide/importsummary/");
        $I->seeInTitle("Import summary");

        $I->amOnPage("/dashin/owner/importguide/datasettype/");
        $I->seeInTitle("Choose dataset type");

        $I->amOnPage("/dashin/owner/importguide/designedit/");
        $I->seeInTitle("Define dataset design");

        $I->amOnPage("/dashin/owner/importguide/designview/");
        $I->seeInTitle("View dataset design");

        $I->amOnPage("/dashin/owner/importguide/qcdataset/");
        $I->seeInTitle("Quality control dataset");

        $I->amOnPage("/dashin/owner/importguide/complete/");
        $I->seeInTitle("Import complete");

    }

    public function routesVariableinfoTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - variable info");
        #---------------------------------------------
        # squidr variable-info
        #---------------------------------------------
        $I->amOnPage("/dashin/owner/varinfo/index/");
        $I->seeInTitle("Upload variable info");

        $I->amOnPage("/dashin/owner/varinfo/matchindex/");
        $I->seeInTitle("Match variable info");

        $I->amOnPage("/dashin/owner/varinfo/uploadvarinfo/");
        $I->seeInTitle("Upload variable info set");

        $I->amOnPage("/dashin/owner/varinfo/viewupvarv/");
        $I->seeInTitle("View uploaded variable info variables");

        $I->amOnPage("/dashin/owner/varinfo/upvarreport/");
        $I->seeInTitle("Summary for uploaded variable info set");

        $I->amOnPage("/dashin/owner/varinfo/upvaredit/");
        $I->seeInTitle("Edit variable info set");

        $I->amOnPage("/dashin/owner/varinfo/browseupvardata/");
        $I->seeInTitle("Browse variable info set data");

        $I->amOnPage("/dashin/owner/varinfo/listupvardatasets/");
        $I->seeInTitle("List variable info sets");

        $I->amOnPage("/dashin/owner/varinfo/match/");
        $I->seeInTitle("Add variable info to dataset");

        $I->amOnPage("/dashin/owner/varinfo/matchsummary/");
        $I->seeInTitle("Match view");

        $I->amOnPage("/dashin/owner/varinfo/varedit/");
        $I->seeInTitle("Edit var dataset");

        $I->amOnPage("/dashin/owner/varinfo/listvardatasets/");
        $I->seeInTitle("List Variable-dataset");

        $I->amOnPage("/dashin/owner/varinfo/listvdvariables/");
        $I->seeInTitle("View varinfo variables");

        $I->amOnPage("/dashin/owner/varinfo/browsevardata/");
        $I->seeInTitle("Inspect variable info data");

        $I->amOnPage("/dashin/owner/varinfo/mapsearchcols/");
        $I->seeInTitle("Map search columns");

    }

    public function routesStudyTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - study");
        #---------------------------------------------
        # squidr study
        #---------------------------------------------
        $I->amOnPage("/dashin/owner/study/index/");
        $I->seeInTitle("Studies");

        $I->amOnPage("/dashin/owner/study/studyindex/");
        $I->seeInTitle("Studies");

        $I->amOnPage("/dashin/owner/study/create/");
        $I->seeInTitle("Create study");

        $I->amOnPage("/dashin/owner/study/editgeneral/");
        $I->seeInTitle("Edit study metadata");

        $I->amOnPage("/dashin/owner/study/list/");
        $I->seeInTitle("List studies");

        $I->amOnPage("/dashin/owner/study/viewgeneral/");
        $I->seeInTitle("Study summary");

        $I->amOnPage("/dashin/owner/study/summary/");
        $I->seeInTitle("Study summary");

        $I->amOnPage("/dashin/owner/study/summarygeneral/");
        $I->seeInTitle("Study summary - general");

        $I->amOnPage("/dashin/owner/study/summarydesign/");
        $I->seeInTitle("Study summary - design");

        $I->amOnPage("/dashin/owner/study/summarycontacts/");
        $I->seeInTitle("Study summary - contacts");

        $I->amOnPage("/dashin/owner/study/summarydatasets/");
        $I->seeInTitle("Study summary - datasets");

        $I->amOnPage("/dashin/owner/study/viewstudydesign/");
        $I->seeInTitle("View study metadata");

        $I->amOnPage("/dashin/owner/study/viewdatasets/");
        $I->seeInTitle("View datasets");

        $I->amOnPage("/dashin/owner/study/createstudycontact/");
        $I->seeInTitle("Create study contact");

        $I->amOnPage("/dashin/owner/study/editstudycontact/");
        $I->seeInTitle("Edit study contact");

        $I->amOnPage("/dashin/owner/study/liststudycontacts/");
        $I->seeInTitle("View study contacts");
    }

    public function routesStudydesignTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - studydesign");
        #---------------------------------------------
        # squidr study design
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/studydesign/index/");
        $I->seeInTitle("View study design");

        $I->amOnPage("/dashin/owner/studydesign/viewstudydesign/");
        $I->seeInTitle("View study design");

        $I->amOnPage("/dashin/owner/studydesign/createcenter/");
        $I->seeInTitle("Create center");

        $I->amOnPage("/dashin/owner/studydesign/editcenter/");
        $I->seeInTitle("Edit center");

        $I->amOnPage("/dashin/owner/studydesign/listcenters/");
        $I->seeInTitle("List center");

        $I->amOnPage("/dashin/owner/studydesign/createstartgroup/");
        $I->seeInTitle("Create startgroup");

        $I->amOnPage("/dashin/owner/studydesign/editstartgroup/");
        $I->seeInTitle("Edit startgroup");

        $I->amOnPage("/dashin/owner/studydesign/liststartgroups/");
        $I->seeInTitle("List startgroups");

        $I->amOnPage("/dashin/owner/studydesign/listevents/");
        $I->seeInTitle("List events");

        $I->amOnPage("/dashin/owner/studydesign/createevent/");
        $I->seeInTitle("Add event to study");

        $I->amOnPage("/dashin/owner/studydesign/editevent/");
        $I->seeInTitle("Edit event");

        $I->amOnPage("/dashin/owner/studydesign/createsubevent/");
        $I->seeInTitle("Create subevent");

        $I->amOnPage("/dashin/owner/studydesign/editsubevent/");
        $I->seeInTitle("Edit subevent");

        $I->amOnPage("/dashin/owner/studydesign/listsubevents/");
        $I->seeInTitle("List subevent");

        $I->amOnPage("/dashin/owner/studydesign/addgrouptosubevent/");
        $I->seeInTitle("Add startgroups to subevent");

        $I->amOnPage("/dashin/owner/studydesign/createsamplingevent/");
        $I->seeInTitle("Create sampling event");

        $I->amOnPage("/dashin/owner/studydesign/editsampling/");
        $I->seeInTitle("Edit sampling event");

        $I->amOnPage("/dashin/owner/studydesign/listsamplingevents/");
        $I->seeInTitle("List sampling events");

        $I->amOnPage("/dashin/owner/studydesign/importdatadesign/");
        $I->seeInTitle("Import dataset design");

    }

    public function routesStudydataTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - studydata");
        #---------------------------------------------
        # squidr studydata
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/studydata/index/");
        $I->seeInTitle("Studies");

        $I->amOnPage("/dashin/owner/studydata/attachdataset/");
        $I->seeInTitle("Add dataset");

        $I->amOnPage("/dashin/owner/studydata/listdatasets/");
        $I->seeInTitle("List available datasets");

        $I->amOnPage("/dashin/owner/studydata/browsedata/");
        $I->seeInTitle("Browse study-data");

        $I->amOnPage("/dashin/owner/studydata/viewdatadesign/");
        $I->seeInTitle("View dataset design");

    }

    public function routesSubjectTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - subject");

        #---------------------------------------------
        # squidr subject
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/subject/index/");
        $I->seeInTitle("Extract subjects");

        $I->amOnPage("/dashin/owner/subject/createstudysubject/");
        $I->seeInTitle("Create study subjects");

        $I->amOnPage("/dashin/owner/subject/liststudysubjects/");
        $I->seeInTitle("List subjects");

        $I->amOnPage("/dashin/owner/subject/editstudysubject/");
        $I->seeInTitle("Edit subject");
    }

    public function routesStudyexportTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - studyexport");

        #---------------------------------------------
        # squidr studyexport
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/studyexport/listconditions/");
        $I->seeInTitle("List studyvariable conditions");

        $I->amOnPage("/dashin/owner/studyexport/createcondition/");
        $I->seeInTitle("Add studyvariable conditions");

        $I->amOnPage("/dashin/owner/studyexport/editcondition/");
        $I->seeInTitle("Edit variable condition");

        $I->amOnPage("/dashin/owner/studyexport/createstudyrowset/");
        $I->seeInTitle("Create row set");

        $I->amOnPage("/dashin/owner/studyexport/editstudyrowset/");
        $I->seeInTitle("Edit rowset");

        $I->amOnPage("/dashin/owner/studyexport/liststudyrowsets/");
        $I->seeInTitle("List studyrowsets");

        $I->amOnPage("/dashin/owner/studyexport/createstudyvarset/");
        $I->seeInTitle("Create data filter");

        $I->amOnPage("/dashin/owner/studyexport/editstudyvarset/");
        $I->seeInTitle("Edit export variable set");

        $I->amOnPage("/dashin/owner/studyexport/liststudyvarsets/");
        $I->seeInTitle("List studyrowsets");

        $I->amOnPage("/dashin/owner/studyexport/createstudyexportset/");
        $I->seeInTitle("Create study export set");

        $I->amOnPage("/dashin/owner/studyexport/editstudyexportset/");
        $I->seeInTitle("Edit study exportset");

    }

    public function routesAppadminTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - appadmin");

        #---------------------------------------------
        # squidr appadmin
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/admin/index/");
        $I->seeInTitle("Admin menu");

        #---------

        $I->amOnPage("/dashin/owner/consort/create/");
        $I->seeInTitle("Create consortium");

        $I->amOnPage("/dashin/owner/consort/edit/");
        $I->seeInTitle("Edit consortium");

        $I->amOnPage("/dashin/owner/consort/list/");
        $I->seeInTitle("List consortiums");

        $I->amOnPage("/dashin/owner/consort/create/");
        $I->seeInTitle("Create consortium");

        #---------

        $I->amOnPage("/dashin/owner/assay/list/");
        $I->seeInTitle("List assays");

        $I->amOnPage("/dashin/owner/assay/edit/");
        $I->seeInTitle("Edit Assay");

        $I->amOnPage("/dashin/owner/datacolumntype/index/");
        $I->seeInTitle("Column type");

        $I->amOnPage("/dashin/owner/datacolumntype/create/");
        $I->seeInTitle("Create column type");

        $I->amOnPage("/dashin/owner/datacolumntype/edit/");
        $I->seeInTitle("Edit column type");

        $I->amOnPage("/dashin/owner/datacolumntype/list/");
        $I->seeInTitle("List column types");

        $I->amOnPage("/dashin/owner/prfxunit/listprfxunits/");
        $I->seeInTitle("List prefixed units");

    }

    public function routesSearchTest(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - search");

        #---------------------------------------------
        # squidr search
        #---------------------------------------------

        $I->amOnPage("/dashin/owner/search/default/");
        $I->seeInTitle("Search general");

        $I->amOnPage("/dashin/owner/search/index/");
        $I->seeInTitle("Search");

        $I->amOnPage("/dashin/owner/search/metabolomics/");
        $I->seeInTitle("Search metabolomics 1");

        $I->amOnPage("/dashin/owner/search/metabolomicstype02/");
        $I->seeInTitle("Search metabolomics 2");

        $I->amOnPage("/dashin/owner/search/savemetabolomicstype02/");
        $I->seeInTitle("Save resultset");

        $I->amOnPage("/dashin/owner/search/savemetabolomics/");
        $I->seeInTitle("Save resultset");

        $I->amOnPage("/dashin/owner/search/listresults/");
        $I->seeInTitle("List resultsets");

        $I->amOnPage("/dashin/owner/search/viewresultset/");
        $I->seeInTitle("Download resultsets");

        $I->amOnPage("/dashin/owner/search/editresultdataset/");
        $I->seeInTitle("Edit resultsets");

        $I->amOnPage("/dashin/owner/search/anthropometry/");
        $I->seeInTitle("Search Anthropometry");
    }
//
//
//    public function routesRoleaclTest(AcceptanceTester $I)
//    {
//
//        $I->wantTo("Test Routes - roleacl");
//    }
//

    public function routes0Test(AcceptanceTester $I)
    {

        $I->wantTo("Test Routes - base");

        #---------------------------------------------
        # squidr service select
        #---------------------------------------------
        $I->amOnPage("/dashin/owner/assay/edit/");
        $I->click('btn[select_assay]');
        $I->seeInTitle("Select assay");

        $I->amOnPage("/dashin/owner/consort/edit/");
        $I->click('btn[select_consort]');
        $I->seeInTitle("Select consortium");

        $I->amOnPage("/dashin/owner/datacolumntype/edit/");
        $I->click('btn[select_datacolumntype]');
        $I->seeInTitle("Select column type");

        $I->amOnPage("/dashin/owner/import/uploadsummary/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/import/editdataset/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/import/listvariables/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/import/editvariable/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        # must select dataset first
        # $I->amOnPage("/dashin/owner/import/editvariable/");
        # $I->click('btn[select_variable]');
        # $I->seeInTitle("Select variable");

        $I->amOnPage("/dashin/owner/import/browsedata/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/import/viewdatadesign/");
        $I->click('btn[select_dataset]');
        $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/search/viewresultset/");
        $I->click('btn[select_resultset]');
        $I->seeInTitle("Select resultset");

        $I->amOnPage("/dashin/owner/search/editresultdataset/");
        $I->click('btn[select_resultset]');
        $I->seeInTitle("Select resultset");
        $I->click('btn[cancel]');
//        $I->seeInTitle("Edit resultset");

        # must select resultset first
        # $I->amOnPage("/dashin/owner/search/editresultdataset/");
        # $I->click('btn[select_dataset]');
        # $I->seeInTitle("Select Dataset");

        $I->amOnPage("/dashin/owner/study/editgeneral/");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");

        $I->amOnPage("/dashin/owner/study/summary/");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");

    }
    
    public function routesServiceSelectTest(AcceptanceTester $I)
    {
        /*

        $I->amOnPage("/user/view/");
        $I->seeInTitle("Select Manager Role");
        $I->submitForm('form#form1', ['btn[select]' => $this->orgid]);

        $I->amOnPage("/dashin/owner/search/editresultdataset/");
        $I->click('btn[select_resultset]');
        $I->seeInTitle("Select resultset");
        $I->seeElement("button",["name"=>"btn[cancel]"]);
        $I->click('btn[cancel]');
//        $I->seeInTitle("Edit resultset");
        
        $I->wantTo("Test select service - study - 1st and 2nd step");

        # study

        $I->amOnPage("/dashin/owner/study/summarygeneral/");
        $I->seeInTitle("Study summary - general");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("Study summary - general");

        $I->amOnPage("/dashin/owner/study/summarydesign/");
        $I->seeInTitle("Study summary - design");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("Study summary - design");

        $I->amOnPage("/dashin/owner/study/summarycontacts/");
        $I->seeInTitle("Study summary - contacts");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("Study summary - contacts");

        $I->amOnPage("/dashin/owner/study/summarydatasets/");
        $I->seeInTitle("Study summary - datasets");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("Study summary - datasets");

        $I->amOnPage("/dashin/owner/study/viewdatasets/");
        $I->seeInTitle("View datasets");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("View datasets");

        $I->amOnPage("/dashin/owner/study/createstudycontact/");
        $I->seeInTitle("Create study contact");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("Create study contact");

        #-----------------------------------------------------
        $I->amOnPage("/dashin/owner/study/editstudycontact/");
        $I->seeInTitle("Edit study contact");
        $I->click('btn[select_study]');
        #--
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        #--
        $I->seeInTitle("Edit study contact");
        $I->seeElement("button",["name"=>"btn[select_studycontact]"]);
        $I->click('btn[select_studycontact]');
        #--
        $I->seeInTitle("Select studycontact");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        #--
        $I->seeInTitle("Edit study contact");

        #-----------------------------------------------------
        $I->amOnPage("/dashin/owner/study/liststudycontacts/");
        $I->seeInTitle("View study contacts");
        $I->click('btn[select_study]');
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        $I->seeInTitle("View study contacts");
        */

    }
    public function routesTest(AcceptanceTester $I)
    {
        $I->wantTo("Test service select - studydata");

        #-----------------------------------------------------
        $I->amOnPage("/dashin/owner/studydata/browsedata/");
        $I->seeInTitle("Browse study-data");
        $I->click('btn[select_study]');
        #--
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        #--
        $I->seeInTitle("Browse study-data");
        $I->seeElement("button",["name"=>"btn[select_dataset]"]);
        $I->click('btn[select_dataset]');
        #--
        $I->seeInTitle("Select study-dataset");
        $I->seeElement("button",["name"=>"btn[cancel]"]);
        $I->click('btn[cancel]');
        #--
        $I->seeInTitle("Browse study-data");
        
        #-----------------------------------------------------
        $I->amOnPage("/dashin/owner/studydata/attachdataset/");
        $I->seeInTitle("Add dataset");
        $I->click('btn[select_study]');
        #--
        $I->seeInTitle("Select study");
        $I->seeElement("button",["name"=>"btn[select]"]);
        $I->click('btn[select]');
        #--
        $I->seeInTitle("Add dataset");
        $I->seeElement("button",["name"=>"btn[select_dataset]"]);
        $I->click('btn[select_dataset]');
        #--
        $I->seeInTitle("Select Dataset");
        $I->seeElement("button",["name"=>"btn[cancel]"]);
        $I->click('btn[cancel]');
        #--
        $I->seeInTitle("Add dataset");
        
    }

}
