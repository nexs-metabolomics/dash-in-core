<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

use App\Library\Utils\LocalSession;

class ViewSelectServicesCest
{

    /**
     * @var null|string
     */
    protected $user = null;

    /**
     * @var null|string
     */
    protected $password = null;

    /**
     * @var null|string
     */
    private $sessionCookie = null;

    /**
     * @var null|string
     */
    private $filepath = null;

    /**
     * @var null|LocalSession
     */
    private $localsession = null;

    private $module = null;

    public function _before(AcceptanceTester $I)
    {

        $this->user = "usrab12345678";
        $this->password = "admin";
        $this->filepath = "search-test-data-03-glucose.csv";
        $this->localsession = new LocalSession();
        $I->amOnPage('/login');
        $I->submitForm('form#form1', ['username' => $this->user, 'password' => $this->password]);
        $this->sessionCookie = $I->grabCookie("PHPSESSID");

    }

    public function tryToTest(AcceptanceTester $I)
    {

        # validate that select pages _generally_ work as expected
        $I->wantTo('Test Select Sevice pages');
        /*
        $I->setCookie("PHPSESSID", $this->sessionCookie);

        $I->amOnPage("/dashin/owner/studydesign/editcenter/");
        $I->seeInTitle("Edit center");
        $I->submitForm("#form1", ["btn[select_study]" => ""]);
        $I->seeInSource("nav-first-icon");
        $I->seeInTitle("Select study");
        $I->submitForm("#form1", ["btn[select]" => "ed10f8f5-161e-4a21-b5e3-cce552fa7220"]);
        $I->see("PROKA");
        $I->see("Select center");
        $I->submitForm("#form1", ["btn[selectcenter]" => ""]);
        $I->seeInTitle("Select center");
        $I->submitForm("#form1", ["btn[select]" => "f9336529-0aa3-43cd-a436-e49b34c4714b"]);
        $I->see("Select another center");
        */
    }

}

# html body div.page-container div.page-content div#pci.page-content-inner div.table-container.fixed-header-select table tbody tr td button.button.selectpage.select