<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

use App\Library\Utils\LocalSession;

class LoginCest
{

    /**
     * @var null|string
     */
    protected $user = null;

    /**
     * @var null|string
     */
    protected $password = null;

    /**
     * @var null|string
     */
    private $sessionCookie = null;

    /**
     * @var null|string
     */
    private $filepath = null;

    /**
     * @var null|LocalSession
     */
    private $localsession = null;

    public function _before(AcceptanceTester $I)
    {

        $this->user = "usrab12345678";
        $this->password = "admin";
        $this->filepath = "search-test-data-03-glucose.csv";
        $this->localsession = new LocalSession();
    }

    // tests
    public function loginTest(AcceptanceTester $I)
    {

        $I->wantTo('Testing login');
        $I->amOnPage('/login');

        $I->see("User code");
        $I->seeInSource("form1");

        $I->submitForm('form#form1', ['username' => $this->user, 'password' => $this->password]);

        $I->see("Logout");
        $I->seeInCurrentUrl("/dashin/index/");
        $this->sessionCookie = $I->grabCookie("PHPSESSID");
        # index page link to import subsection

    }

    public function clickAroundTest(AcceptanceTester $I)
    {
        /*

        $I->wantTo("Click-navigate down to 'view dataset' (coffee pilot data)");
        $I->setCookie("PHPSESSID", $this->sessionCookie);

        # click down to view specific dataset
        $I->amOnPage("/dashin/index/");

        # click to first level
        $I->click('a#home-index-datasets');
        $I->seeInCurrentUrl("/dashin/owner/import/index/");

        # click to second level
        $I->click('a#datasets-index-datasets');
        $I->seeInCurrentUrl("/dashin/owner/import/listdatasets/");

        # view/select specific dataset
        $I->submitForm('form#f7fbdffa-0e43-4782-b332-d40b83a1531a', ['username' => $this->user, 'password' => $this->password, "btn[view]" => "f7fbdffa-0e43-4782-b332-d40b83a1531a"]);
        $I->see("Coffee_pilot1_design");
//        $I->seeInCurrentUrl("ls=");
////        $datasetId = $this->localsession->getKeyValue("dashin_owner_dataset_id");
//        $qstr = $I->grabFromCurrentUrl('/(?<=ls=)([^#]+)/');
//        $I->assertGreaterThan(0, strlen($qstr), "querystring: '$qstr'");
//        $data = $this->localsession->getDataFromQuerystring($qstr);
//        $I->assertIsArray($data, "not array");
//        $I->eq;
        */
    }

    public function uploadDatasetTest(AcceptanceTester $I)
    {

        $I->wantTo("Upload a dataset");
//        $I->setCookie("PHPSESSID", $this->sessionCookie);
//
//        $I->amOnPage("/dashin/owner/import/uploadsummary/");
//        $I->click("Upload");
//        $I->see("Upload dataset");
//        $I->seeInCurrentUrl("import/uploaddata/");
//
//        $I->seeInSource("file_up");
//        $I->attachFile("body input#file_up", $this->filepath);
//        $I->fillField('body input#name', "test uploaded file " . gmdate('D, d M Y H:i:s'));
//        $I->fillField('body textarea#description', "Description of test uploaded file");
//        $I->click("Ok");
//        $I->see("Dataset summary");

    }

}
