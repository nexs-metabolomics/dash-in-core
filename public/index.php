<?php
/*********************************************************************
 * dash-in datasharing
 *
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 *
 * Created 2017-08-20
 *
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 *
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
 */
//echo(xdebug_info());
use Phalcon\Mvc\Application;

if(!defined('APP_PATH')) {
    define('APP_PATH', realpath(__DIR__.'/..'));
}

try {
    
    include __DIR__ . "/../app/00init/00init.php";

    /**
     * Handle the request
     * 
     * @var $di Phalcon\Di\FactoryDefault
     */
    $application = new Application($di);

    if (getenv("APPLICATION_CONTEXT") == "testing") {
        
        return $application;
        
    } else {

        $appHand = $application->handle($_SERVER['REQUEST_URI']);

        echo $appHand->getContent();
    }

} catch (\Exception $e) {
//    TODO: how to log exceptions?
//    $application->response->redirect("/")->send();
//    $application->flashSession->error("An error occurred");
//    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getMessage() . '<br>' . $e->getTraceAsString() . '</pre>';
}
