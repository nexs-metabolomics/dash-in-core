#!/bin/bash
# google-font-download "Open Sans:300" "Open Sans:400" "Open Sans:700" "Poppins:300" "Poppins:400" "Poppins:700" -f all -l latin -o osans-poppins-fonts.css
# google-font-download "Poppins:bold"  -f all -l latin -o fonts-poppins-bold.css

# google-font-download "Poppins:regular" "Poppins:italic" "Poppins:bold" "Poppins:bold italic"  -f all -l latin -o fonts-poppins-standard.css
# google-font-download "Poppins:thin" "Poppins:thin italic" "Poppins:medium" "Poppins:medium italic"  -f all -l latin -o fonts-poppins-thin-medium.css
# 
# google-font-download "Open Sans:regular" "Open Sans:italic" "Open Sans:bold" "Open Sans:bold italic"  -f all -l latin -o fonts-opensans-standard.css
# google-font-download "Open Sans:light" "Open Sans:light italic" "Open Sans:semibold" "Open Sans:semibold italic"  -f all -l latin -o fonts-opensans-thin-medium.css

google-font-download  "Roboto:black" "Roboto:black italic" "Roboto:bold" "Roboto:bold italic" "Roboto:italic" "Roboto:light" "Roboto:light italic" "Roboto:medium" "Roboto:medium italic" "Roboto:regular" "Roboto:thin" "Roboto:thin italic"  -f all -l latin -o fonts-roboto-bold.css

# Roboto/Roboto-Black.ttf
# Roboto/Roboto-BlackItalic.ttf
# Roboto/Roboto-Bold.ttf
# Roboto/Roboto-BoldItalic.ttf
# Roboto/Roboto-Italic.ttf
# Roboto/Roboto-Light.ttf
# Roboto/Roboto-LightItalic.ttf
# Roboto/Roboto-Medium.ttf
# Roboto/Roboto-MediumItalic.ttf
# Roboto/Roboto-Regular.ttf
# Roboto/Roboto-Thin.ttf
# Roboto/Roboto-ThinItalic.ttf
