## [1.1.14](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.13...1.1.14) (2024-2-26)


### Bug Fixes

* **CI:** updated docker image that runs semantic release ([26cf4eb](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/26cf4eb8c418350ab2de6be69db3f5ceb2ef742c))

## [1.1.13](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.12...1.1.13) (2023-12-15)


### Bug Fixes

* **CI:** install package needed to add repo ([b269d12](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/b269d1261cf437e9895f9dba927b5631640a72a7))
* **CI:** install package needed to add repo ([706c330](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/706c3304c3ebbdea5d82c0c3d084fdb6f1b5f5da))
* **CI:** install pre-compiled phalcon ([30df03b](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/30df03bd1a84f4aaa39f3d2b498f468c80828efa))


### Reverts

* Revert "RELEASE: 1.1.12" ([270fa53](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/270fa53622b893eb2caaefd7ca073cd95bddb21b))

## [1.1.12](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.11...1.1.12) (2023-12-15)


### Bug Fixes

* **CI:** updated the way nodejs is installed ([12d203e](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/12d203effa705dc563c0f640464f34bf371e05f1))
* **CI:** updated the way nodejs is installed ([a29fe9a](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/a29fe9acb54118c2b0a31128a8b83bc669d047d5))

## [1.1.11](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.10...1.1.11) (2023-12-06)


### Bug Fixes

* **CI:** chown in docker file doesn't work when the dir is then mapped. It needs to be changed by the container after launch. ([1cf4bcd](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/1cf4bcdbb2394ec1487d030e8fa35ef31e134778))

## [1.1.10](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.9...1.1.10) (2023-12-06)


### Bug Fixes

* **CI:** set permissions on storage folder ([8c130f9](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/8c130f95c55433aa3c4a0b49e8e9a507227e166a))

## [1.1.9](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.8...1.1.9) (2023-12-05)


### Bug Fixes

* **CI:** check if storage issue is because the webserver cannot reach outside its own folder ([f8afb33](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/f8afb3306ed7a866bc951266202ec094889421cf))

## [1.1.8](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.7...1.1.8) (2023-12-05)


### Bug Fixes

* **CI:** added storage folder ([16c0e87](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/16c0e8702b47ed1ec9d1a9c4de6d6e0fb9b957ae))
* **CI:** hardcoded version number in docker parameter file. ([f1a1fff](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/f1a1fffa4511eabec76a1e0bdaa15317122db03d))
* **CI:** typo ([6331195](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/6331195b6cefd976c0e6b47c7f4d3eac26415c2b))

## [1.1.7](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.6...1.1.7) (2023-12-05)


### Bug Fixes

* user,organiztion select. php8: required return type. Misc warnings ([0bd6ce2](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/0bd6ce27a9c72129419b67ebf9628e9fd7f0a4ce))
* **CI:** fix working folders ([a9081c7](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/a9081c79b70af5ab46a0f4b3b443aa2567308d53))
* **CI:** make sure sh files have execute permissions ([854eedf](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/854eedf2808593bd84c769e553649437b1ccb8f2))

## [1.1.6](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.5...1.1.6) (2022-08-27)


### Bug Fixes

* **GUI:** typo ([d31d2cc](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/d31d2ccb2ae750df221a880a5195494077ee8b5a))

## [1.1.5](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.4...1.1.5) (2022-08-27)


### Bug Fixes

* **CI:** upgrade mailer extension ([565bf2a](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/565bf2ab4e60e299c420dba411b3dac08c53088d))

### [1.1.4](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.3...1.1.4) (2022-04-20)


### Bug Fixes

* **CI:** Install extensions in app folder and add the mailer extension. closes [#187](https://gitlab.com/nexs-metabolomics/dash-in-core/issues/187) ([a608cc6](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/a608cc6b6a8b26237dbfba49e5b5ddbc1ac9708a))

### [1.1.3](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.2...1.1.3) (2022-04-06)


### Bug Fixes

* liststudyvariabecondition jumps to first page when deleting ([91006d7](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/91006d710f1e61e3bbb0835f43741188d489e184))
* **search:** migration 2 -> 3, fix search categories are not deleted with condition ([70b0d6a](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/70b0d6a2903804e99616170ff32adb1534c412c2))

### [1.1.2](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.1...1.1.2) (2022-03-08)


### Bug Fixes

* **CI:** install swiftmailer required for sending emails ([b2230ab](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/b2230ab4d0667b873bcfa941927a3b0b41ea1ee1))
* **CI:** now with correct syntax for consecutive commands ([6624ad9](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/6624ad928be84b2a06a06a123fe9cd5e949c4a8b))

### [1.1.1](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.1.0...1.1.1) (2022-03-08)


### Bug Fixes

* **CI:** pass all parameters for SMTP ([d0e7335](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/d0e733549c4b79b2aef96e97a42710fb1b5f6820))

## [1.1.0](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.6...1.1.0) (2022-02-18)


### Bug Fixes

* migration1_2 ([81db52d](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/81db52d94b6dff68a71c54e0cbe5fcfc16c7fdbc))
* **backup:** correct network setup ([839bab7](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/839bab798b8ab713354d71e8137ca54901ada1c1))
* **backup:** fix path ([061b585](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/061b585be14fad3a39e9c481d978e77d14b90c8c))
* **backup:** now with full date and time ([de57949](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/de579493a39802f2bd481a737b1c89a4136edb92))
* **backup:** pull correct container ([7098214](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/7098214d67de5d04bf378d6917959ec788642cbf))
* **CI:** moved TZ to common variables ([7a726d4](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/7a726d496c8aab577424470d26add58e7f1cb441))
* **CI:** share environmental variables ([be49f70](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/be49f70092f738e9d66b118008eca3ea99d43179))
* **migration:** fix delete dangling objects before updating constraints to ensure proper deletion of study. Fixes [#182](https://gitlab.com/nexs-metabolomics/dash-in-core/issues/182) ([e1e6256](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/e1e62568d2b45b9aa8708587ad0133f4baa2f065))


### Features

* **backup:** lets try to add a lightweight image to do the backup ([337d071](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/337d071fd40d5e861e7e106dedf53fc1d94ec619))

### [1.0.6](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.5...1.0.6) (2021-12-14)


### Bug Fixes

* add version number to ui, bump version number. ([daa2b8e](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/daa2b8ef0d37471c73e87505ced352364a4bd4b7))
* bump version (test automation) ([18a4771](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/18a4771931d7317628b5af2814e18a2fa9dde11f))
* test-2 of autamic versioning ([68231ff](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/68231ff224e52f2677ffce785a829e6fda651c83))


### Reverts

* **ci:** semantic release config needs to be in root it seems ([ea47659](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/ea476591ba9b1dd86173f4046e415ce3c137c36f))

### [1.0.5](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.4...1.0.5) (2021-12-03)


### Bug Fixes

* **versioning:** disable success/fail so the github plugin doesn't try to run. source: https://github.com/semantic-release/semantic-release/issues/827 ([ea100f9](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/ea100f9a12df096abf16c353b8eb86571c950cb9))

### [1.0.4](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.3...1.0.4) (2021-12-03)


### Bug Fixes

* **versioning:** argument moved to below the plugin ([c779d37](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/c779d371dd1be4183287641a26233fb6c84aa671))

### [1.0.3](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.2...1.0.3) (2021-12-02)


### Bug Fixes

* **versioning:** changed order of prepare plugins ([2f9d239](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/2f9d23965f2d034af54f1ac9ad8e98e9cef692a3))

### [1.0.2](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.1...1.0.2) (2021-12-02)


### Bug Fixes

* **versioning:** versioning assets are now made in the prepare step ([fae1d2b](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/fae1d2b731255dff1f5e21b2a102bec176598294))

### [1.0.1](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/1.0.0...1.0.1) (2021-12-02)


### Bug Fixes

* **ci:** updated CI to write a file with the version number ([474197c](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/474197c77e2d28b4e97cbe239a203a69c52a2891))

## [1.0.0](https://gitlab.com/nexs-metabolomics/dash-in-core/compare/...1.0.0) (2021-12-02)


### Bug Fixes

* cannot add permissions to organization ([882d512](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/882d512cab5a01ea6a26ad3a1c743a6e0e449e91))
* Data structure download is not working ([85329b8](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/85329b83c753d18a8628f9fd4552c516fbfedf6d))
* Edit rowconditions with designvariable doesn't work. ([17ad64b](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/17ad64b183662621262c4d304320d2c7943d0443))
* remove some copy-paste misplaced code ([de7c7ca](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/de7c7cadeda4b9c8562b8f08117f788c6c132dd6))
* **CI:** add config file for semantic release ([3d5c0a4](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/3d5c0a4b9e352175014949a598f2a1d98388dd3e))
* **CI:** different guide. no config. probably newer version ([7ab57c6](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/7ab57c6668e9e7ac658ccb2b4cc704156516527f))
* **CI:** I believe settings needs to be in the root ([bf44f78](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/bf44f786c22140f80522840e97c7621d886e1b35))
* **CI:** removed erroneous comma ([7b4a05f](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/7b4a05f5bb7c085381a8a1baba47cd0d5cebc5ca))
* **CI:** update to newer node ([87606eb](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/87606ebf8a15fd7cb8663db483feec4f5772e681))
* **CI:** windows removed the doc from the filename... ([9f1c0b5](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/9f1c0b57aa5292f5a58e3112af00aee1007dbae8))


### Features

* **CI:** try semantic versioning ([81e8c11](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/81e8c11679f8ee77a959695d9ac33bbd006d6b02))


### Reverts

* Revert "dat03: refactor serviceSelect" ([10a19c5](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/10a19c53dee4e4f049492a0fd9efcc6d554a62f6))
* Revert "nds preparing version 2" ([ca3affc](https://gitlab.com/nexs-metabolomics/dash-in-core/commit/ca3affc7fdeafefc6bf6639d81bbd0d25807fbbb))
