Setup the database (example):

```shell
cd install
./setup.sh parameters-example.sh


```

Migration (example - using the same config-file as with setup above):

```shell
cd install/migrations/2023_05_21__2023_11_30
./migration_2023_05_21__2023_11_30.sh -p ../../parameters-example.sh


```