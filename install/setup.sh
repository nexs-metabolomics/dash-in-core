#!/bin/bash

# handle arguments
USAGE_MESSAGE="
    Usage:
    
    setup.sh <path-to-parameter-file> (required) 
    
"

# path to config file
ARG_PARAM_FILE="$@"


[[ -f "$ARG_PARAM_FILE" ]] || { echo "paramter file '$ARG_PARAM_FILE' could not be found";echo -e "$USAGE_MESSAGE"; exit 0;}


#----------------------------------------------------------------------------
# read config file
#----------------------------------------------------------------------------
echo "Reading parameters from: '$ARG_PARAM_FILE'"
source "$ARG_PARAM_FILE"


#----------------------------------------------------------------------------
# check that all required arguments are present
#----------------------------------------------------------------------------
missing_args=false

if [ -z "$APPLICATION_USER" ];          then echo "APPLICATION_USER is not defined or empty";          missing_args=true;fi; 
if [ -z "$APPLICATION_USER_PASSWORD" ]; then echo "APPLICATION_USER_PASSWORD is not defined or empty"; missing_args=true;fi; 
if [ -z "$APPLICATION_DBNAME" ];        then echo "APPLICATION_DBNAME is not defined or empty";        missing_args=true;fi; 
if [ -z "$SUPER_USER" ];                then echo "SUPER_USER is not defined or empty";                missing_args=true;fi; 
if [ -z "$SUPER_USER_DBNAME" ];         then echo "SUPER_USER_DBNAME is not defined or empty";         missing_args=true;fi; 
if [ -z "$HOST" ];                      then echo "HOST is not defined or empty";                      missing_args=true;fi; 
if [ -z "$PORT" ];                      then echo "PORT is not defined or empty";                      missing_args=true;fi; 
if [ -z "$DB_VERSION" ];                then echo "DB_VERSION is not defined or empty";                missing_args=true;fi; 

# exit if any argument is missing (or empty)
if $missing_args ; then echo "Some arguments are missing. Stopping!";exit 1;fi;

#----------------------------------------------------------------------------
# print out arguments
#----------------------------------------------------------------------------
echo "user: $APPLICATION_USER" 
echo "password: $APPLICATION_USER_PASSWORD" 
echo "dbname: $APPLICATION_DBNAME" 
echo "super user: $SUPER_USER" 
echo "super user password: $SUPER_USER_PASSWORD" 
echo "super user dbname: $SUPER_USER_DBNAME" 
echo "host: $HOST" 
echo "port: $PORT" 

#----------------------------------------------------------------------------
# set path to password file
#----------------------------------------------------------------------------
CWD=$(pwd -P)
#echo "cwd: $CWD"
PGPASS_PATH="$CWD/.pgpass"
echo "creating pg password file '$PGPASS_PATH'"

# set environment variable so psql can find the password file
export PGPASSFILE="$PGPASS_PATH"


# setup pgpass
echo "${HOST}:${PORT}:${SUPER_USER_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}"  > "$PGPASS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}" >> "$PGPASS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${APPLICATION_USER}:${APPLICATION_USER_PASSWORD}" >> "$PGPASS_PATH"
chmod 0600 "$PGPASS_PATH"

# make psql be more quiet
export PGOPTIONS='--client-min-messages=warning'






#============================================================================
# DATABASE SETUP
#============================================================================

#----------------------------------------------------------------------------
# create user
#----------------------------------------------------------------------------
# check if user exists.
USER_EXISTS=$(psql -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST -qtAX -c "SELECT count(*)=1 FROM pg_user WHERE usename = '${APPLICATION_USER}';")
if [  "$USER_EXISTS" = "t" ]; then
  echo "User ${APPLICATION_USER} already exists. Skipping."
else
  # create user
  echo "User ${APPLICATION_USER} does not exist; creating user"
  echo "SELECT 'CREATE USER ${APPLICATION_USER}' WHERE NOT EXISTS (SELECT FROM pg_user WHERE usename = '${APPLICATION_USER}')\gexec" | psql -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST
  psql -v ON_ERROR_STOP=1 -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST -b -c "ALTER USER $APPLICATION_USER WITH PASSWORD '${APPLICATION_USER_PASSWORD}'"
fi


#----------------------------------------------------------------------------
# create database
#----------------------------------------------------------------------------
# check if database exists.
DATABASE_EXISTS=$(psql -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST -qtAX -c "SELECT count(*)=1 FROM pg_database WHERE datname = '${APPLICATION_DBNAME}';")
if [  "$DATABASE_EXISTS" = "t" ]; then
  echo "Database ${APPLICATION_DBNAME} already exists. Stopping!"
  exit 1
fi

# create database
echo "Database ${APPLICATION_DBNAME} does not exist; creating database"
echo "SELECT 'CREATE DATABASE ${APPLICATION_DBNAME}' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '${APPLICATION_DBNAME}')\gexec" | psql -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST
# set owner to user
psql -U $SUPER_USER -d $SUPER_USER_DBNAME -p $PORT -h $HOST -b -c "ALTER DATABASE ${APPLICATION_DBNAME} OWNER TO ${APPLICATION_USER}"

#----------------------------------------------------------------------------
# generate sql files
#----------------------------------------------------------------------------
source "write-sql-files.sh" "$DB_VERSION"

#----------------------------------------------------------------------------
# create extensions (must be super user)
#----------------------------------------------------------------------------
psql -v application_dbname="$APPLICATION_DBNAME" -U $SUPER_USER -d $APPLICATION_DBNAME -p $PORT -h $HOST -f "generated-sql/${DB_VERSION}/${DB_VERSION}_run-01.sql"

#----------------------------------------------------------------------------
# create tables and functions
#----------------------------------------------------------------------------
psql -v application_user="$APPLICATION_USER" -v dbversion_string="'$DB_VERSION'" -U $SUPER_USER -d $APPLICATION_DBNAME -p $PORT -h $HOST -f "generated-sql/${DB_VERSION}/${DB_VERSION}_run-02.sql"
echo "DB VERSION STRING: $DB_VERSION"






#============================================================================
# cleanup
#============================================================================
echo "removing pgpass file"
rm -f "$PGPASSFILE"
