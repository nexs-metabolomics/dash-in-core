CREATE TABLE dashin.datacolumntype
(
  datacolumntype_id UUID
    CONSTRAINT datacolumntype_pk
      PRIMARY KEY DEFAULT uuid_generate_v4(),
  name              TEXT NOT NULL
    CONSTRAINT datacolumntype_name_uq
      UNIQUE,
  shortname              TEXT NOT NULL
    CONSTRAINT datacolumntype_shortname_uq
      UNIQUE,
  description       TEXT,
  label             TEXT,
  owner_org         UUID,
  owner_user        UUID
);

ALTER TABLE dashin.datacolumntype OWNER TO :application_user;
