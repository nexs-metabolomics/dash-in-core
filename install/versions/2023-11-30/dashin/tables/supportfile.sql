CREATE TABLE IF NOT EXISTS dashin.supportfile
(
  supportfile_id  UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT supportfile_pk PRIMARY KEY,
  organization_id UUID,
  name            TEXT,
  description     TEXT,
  filename        TEXT,
  fileextension   TEXT,
  mimetype        TEXT,
  updated_at      TIMESTAMP WITH TIME ZONE DEFAULT now(),
  created_at      TIMESTAMP WITH TIME ZONE DEFAULT now()
);

ALTER TABLE dashin.supportfile OWNER TO :application_user;
