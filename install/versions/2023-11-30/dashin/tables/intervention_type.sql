CREATE TABLE dashin.intervention_type
(
  intervention_type_id INTEGER NOT NULL
    CONSTRAINT intervention_type_pk
      PRIMARY KEY,
  ordinal              INTEGER,
  name                 TEXT,
  description          TEXT
);

ALTER TABLE dashin.intervention_type OWNER TO :application_user;
