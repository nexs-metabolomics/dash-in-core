CREATE TABLE dashin.studyrowset_x_studyvariable
(
  studyrowset_id   UUID,
  study_id         UUID,
  studyvariable_id UUID,
  condition        JSONB,
  CONSTRAINT studyrowset_x_studyvariable_uk UNIQUE (studyrowset_id,studyvariable_id)
);

ALTER TABLE dashin.studyrowset_x_studyvariable OWNER TO :application_user;
