CREATE TABLE dashin.studyvariable_x_variable
(
  studyvariable_id UUID NOT NULL,
  study_id         UUID NOT NULL,
  variable_id      UUID NOT NULL,
  dataset_id       UUID NOT NULL,
  CONSTRAINT studyvariable_x_variable_pk PRIMARY KEY (studyvariable_id, variable_id)
);

ALTER TABLE dashin.studyvariable_x_variable OWNER TO :application_user;



