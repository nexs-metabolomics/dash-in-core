CREATE TABLE dashin.studydatatable
(
  studyrow_id      UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id         UUID,
  event_id         UUID,
  subevent_id      UUID,
  samplingevent_id UUID,
  samplingtime_id  UUID,
  center_id        UUID,
  startgroup_id    UUID,
  subject_id       UUID,
  CONSTRAINT studydatatable_all_unique_uk UNIQUE (study_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id),
  -- used by studyrow_x_datarow
  CONSTRAINT studydatatable_study_studyrow_uk UNIQUE (study_id, studyrow_id)
);

ALTER TABLE dashin.studydatatable OWNER TO :application_user;

