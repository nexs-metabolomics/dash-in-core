CREATE TABLE dashin.datadesign_x_study_samplingevent
(
  dataset_id                  UUID,
  datadesign_samplingevent_id UUID,
  study_id                    UUID,
  study_samplingevent_id      UUID,
  CONSTRAINT dxs_samplingevent_pk PRIMARY KEY (datadesign_samplingevent_id),
  CONSTRAINT dxs_dataset_id_samplingevent_id_uk UNIQUE (dataset_id,study_samplingevent_id)
);

ALTER TABLE dashin.datadesign_x_study_samplingevent OWNER TO :application_user;

