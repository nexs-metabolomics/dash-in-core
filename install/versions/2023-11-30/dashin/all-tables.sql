
\echo '----------------------------------------------------------------'
\echo ' CREATE DASHIN TABLES'
\echo '----------------------------------------------------------------'

CREATE TABLE dashin.assay
(
  assay_id        UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT assay_pk
      PRIMARY KEY,
  name            TEXT,
  description     TEXT,
  researchfield_id INTEGER DEFAULT 1 NOT NULL,
  created_at      TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at      TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);

ALTER TABLE dashin.assay OWNER TO :application_user;
CREATE TABLE dashin.consortium
(
  consortium_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT consortium_pk
      PRIMARY KEY,
  name          TEXT,
  description   TEXT,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details       JSONB
);

ALTER TABLE dashin.consortium OWNER TO :application_user;
CREATE TABLE dashin.datacolumntype
(
  datacolumntype_id UUID
    CONSTRAINT datacolumntype_pk
      PRIMARY KEY DEFAULT uuid_generate_v4(),
  name              TEXT NOT NULL
    CONSTRAINT datacolumntype_name_uq
      UNIQUE,
  shortname              TEXT NOT NULL
    CONSTRAINT datacolumntype_shortname_uq
      UNIQUE,
  description       TEXT,
  label             TEXT,
  owner_org         UUID,
  owner_user        UUID
);

ALTER TABLE dashin.datacolumntype OWNER TO :application_user;
CREATE TABLE dashin.datadesign
(
  dataset_id UUID NOT NULL
    CONSTRAINT datadesign_pk
      PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now()
);

ALTER TABLE dashin.datadesign OWNER TO :application_user;
CREATE TABLE dashin.datadesign_datatable
(
  datarow_id           UUID NOT NULL CONSTRAINT datadesign_datatable_pk PRIMARY KEY,
  dataset_id           UUID,
  event_id             UUID,
  subevent_id          UUID,
  samplingevent_id     UUID,
  samplingtime_id      UUID,
  center_id            UUID,
  startgroup_id        UUID,
  subject_id           UUID,
  event_name           TEXT,
  subevent_name        TEXT,
  samplingevent_name   TEXT,
  samplingtime_name    TEXT,
  center_name          TEXT,
  startgroup_name      TEXT,
  subject_name         TEXT,
  event_status         INTEGER,
  subevent_status      INTEGER,
  samplingevent_status INTEGER,
  samplingtime_status  INTEGER,
  center_status        INTEGER,
  startgroup_status    INTEGER,
  subject_status       INTEGER,
  -- used by studyrow_x_datarow
  CONSTRAINT datadesign_datatable_dataset_datarow_uk UNIQUE (dataset_id, datarow_id)
);

ALTER TABLE dashin.datadesign_datatable OWNER TO :application_user;

CREATE TABLE dashin.datadesign_x_study_center
(
  dataset_id           UUID,
  datadesign_center_id UUID,
  study_id             UUID,
  study_center_id      UUID,
  CONSTRAINT d_x_s_center_pk PRIMARY KEY (datadesign_center_id),
  CONSTRAINT d_x_s_center_uk UNIQUE (dataset_id, study_center_id)
);

ALTER TABLE dashin.datadesign_x_study_center OWNER TO :application_user;
CREATE TABLE dashin.datadesign_x_study_event
(
  dataset_id          UUID,
  datadesign_event_id UUID,
  study_id            UUID,
  study_event_id      UUID,
  CONSTRAINT dxs_event_pk PRIMARY KEY (datadesign_event_id),
  CONSTRAINT dxs_dataset_id_event_id_uk UNIQUE (dataset_id, study_event_id)
);

ALTER TABLE dashin.datadesign_x_study_event OWNER TO :application_user;

CREATE TABLE dashin.datadesign_x_study_samplingevent
(
  dataset_id                  UUID,
  datadesign_samplingevent_id UUID,
  study_id                    UUID,
  study_samplingevent_id      UUID,
  CONSTRAINT dxs_samplingevent_pk PRIMARY KEY (datadesign_samplingevent_id),
  CONSTRAINT dxs_dataset_id_samplingevent_id_uk UNIQUE (dataset_id,study_samplingevent_id)
);

ALTER TABLE dashin.datadesign_x_study_samplingevent OWNER TO :application_user;

CREATE TABLE dashin.datadesign_x_study_samplingtime
(
  dataset_id                 UUID,
  datadesign_samplingtime_id UUID PRIMARY KEY,
  study_id                   UUID,
  study_samplingtime_id      UUID,
  CONSTRAINT dxs_dataset_id_samplingtime_id_key UNIQUE (dataset_id, study_samplingtime_id)
);

ALTER TABLE dashin.datadesign_x_study_samplingtime OWNER TO :application_user;
CREATE TABLE dashin.datadesign_x_study_startgroup
(
  dataset_id               UUID,
  datadesign_startgroup_id UUID PRIMARY KEY,
  study_id                 UUID,
  study_startgroup_id      UUID,
  CONSTRAINT dxs_dataset_id_startgroup_id_key UNIQUE (dataset_id, study_startgroup_id)
);

ALTER TABLE dashin.datadesign_x_study_startgroup OWNER TO :application_user;
CREATE TABLE dashin.datadesign_x_study_subevent
(
  dataset_id             UUID,
  datadesign_subevent_id UUID PRIMARY KEY,
  study_id               UUID,
  study_subevent_id      UUID,
  CONSTRAINT dxs_dataset_id_subevent_id_key UNIQUE (dataset_id, study_subevent_id)
);

ALTER TABLE dashin.datadesign_x_study_subevent OWNER TO :application_user;
CREATE TABLE dashin.datadesign_x_study_subject
(
  dataset_id            UUID,
  datadesign_subject_id UUID PRIMARY KEY,
  study_id              UUID,
  study_subject_id      UUID,
  CONSTRAINT dxs_dataset_id_subject_id_key UNIQUE (dataset_id, study_subject_id)
);

ALTER TABLE dashin.datadesign_x_study_subject OWNER TO :application_user;

CREATE TABLE dashin.dataset
(
  dataset_id  UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT dataset_pkey
      PRIMARY KEY,
  name        TEXT,
  description TEXT,
  status      SMALLINT DEFAULT 1,
  owner_org   UUID,
  owner_user  UUID,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details     JSONB,
  CONSTRAINT dataset_owner_key
    UNIQUE (dataset_id, owner_org)
);

ALTER TABLE dashin.dataset OWNER TO :application_user;
CREATE TABLE dashin.dataset_x_study
(
  dataset_id UUID NOT NULL
    CONSTRAINT dataset_x_study_pk
      PRIMARY KEY,
  study_id   UUID NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  -- target constraint for studyrow_x_datarow_dataset_fk on dashin.studyrow_x_datarow
  CONSTRAINT dataset_x_study_uk
    UNIQUE (dataset_id, study_id)
);

ALTER TABLE dashin.dataset_x_study OWNER TO :application_user;
CREATE TABLE dashin.datatable
(
  datarow_id   UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT datatable_pk
      PRIMARY KEY,
  dataset_id   UUID,
  subject_xxcode TEXT,
  ordinal      INTEGER,
  status       SMALLINT DEFAULT 1,
  created_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details      JSONB,
  datarow      JSONB,
  CONSTRAINT datatable_unique_uk UNIQUE (datarow_id, dataset_id)
);

ALTER TABLE dashin.datatable OWNER TO :application_user;
CREATE TABLE dashin.datatype
(
  datatype_id SMALLINT NOT NULL
    CONSTRAINT datatype_pk
      PRIMARY KEY,
  name        TEXT,
  description TEXT
);

ALTER TABLE dashin.datatype OWNER TO :application_user;
CREATE TABLE dashin.intervention_type
(
  intervention_type_id INTEGER NOT NULL
    CONSTRAINT intervention_type_pk
      PRIMARY KEY,
  ordinal              INTEGER,
  name                 TEXT,
  description          TEXT
);

ALTER TABLE dashin.intervention_type OWNER TO :application_user;
CREATE TABLE dashin.researchfield
(
  researchfield_id INT NOT NULL
    CONSTRAINT researchfield_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT
);

ALTER TABLE dashin.researchfield OWNER TO :application_user;
CREATE TABLE dashin.resultset
(
  resultset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT resultset_pk
      PRIMARY KEY,
  name         TEXT,
  description  TEXT,
  owner_user   UUID NOT NULL,
  created_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details      JSONB
);

ALTER TABLE dashin.resultset OWNER TO :application_user;
CREATE TABLE dashin.resultset_x_variable
(
  resultset_id UUID NOT NULL,
  variable_id  UUID NOT NULL,
  CONSTRAINT resultset_x_variable_pk PRIMARY KEY (resultset_id, variable_id)
);

ALTER TABLE dashin.resultset_x_variable OWNER TO :application_user;

CREATE TABLE dashin.search_column_type
(
  search_column_type_id  INTEGER NOT NULL
    CONSTRAINT search_column_type_pk
      PRIMARY KEY,
  datatype_id            INT,
  multiple               INTEGER DEFAULT 1,
  researchfield_id       INTEGER,
  search_column_type_key TEXT
    CONSTRAINT search_column_type_search_column_type_key_uk
      UNIQUE,
  name                   TEXT,
  description            TEXT
);

ALTER TABLE dashin.search_column_type OWNER TO :application_user;
CREATE TABLE dashin.study
(
  study_id             UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_pk
      PRIMARY KEY,
  name                 TEXT,
  title                TEXT,
  description          TEXT,
  start_date           TIMESTAMP,
  endpoint             TEXT,
  objectives           TEXT,
  conclusion           TEXT,
  exclusion            TEXT,
  inclusion            TEXT,
  institute            TEXT,
  country_id           INTEGER,
  consortium_id        UUID,
  published            TEXT,
  researchdesign_id    INTEGER,
  num_treat            TEXT,
  num_factor           INTEGER,
  num_arm              INTEGER,
  researchdesign_text  TEXT,
  num_volun            TEXT,
  num_volun_terminated TEXT,
  recruit_start_year   INTEGER,
  recruit_end_year     INTEGER,
  blinding             INTEGER,
  blinding_method      TEXT,
  owner_org            UUID,
  owner_user           UUID,
  created_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details              JSONB
);

ALTER TABLE dashin.study OWNER TO :application_user;
CREATE TABLE dashin.studycontact
(
  studycontact_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id        UUID,
  first_name      TEXT,
  last_name       TEXT,
  email           TEXT,
  description     TEXT,
  status          INT DEFAULT 1 NOT NULL,
  study_role_id   INT DEFAULT 0 NOT NULL
);
ALTER TABLE dashin.studycontact OWNER TO :application_user;

CREATE TABLE dashin.researchdesign
(
  researchdesign_id INTEGER NOT NULL
    CONSTRAINT researchdesign_pk
      PRIMARY KEY,
  name              TEXT NOT NULL,
  description       TEXT
);

ALTER TABLE dashin.researchdesign OWNER TO :application_user;
CREATE TABLE dashin.study_role
(
  study_role_id INTEGER NOT NULL
    CONSTRAINT study_role_pk
      PRIMARY KEY,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.study_role OWNER TO :application_user;
CREATE TABLE dashin.upvar_dataset
(
  upvar_dataset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_dataset_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT,
  status           SMALLINT DEFAULT 1,
  owner_org        UUID,
  owner_user       UUID,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB
);

ALTER TABLE dashin.upvar_dataset OWNER TO :application_user;
CREATE TABLE dashin.upvar_datatable
(
  upvar_datarow_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_datatable_pk
      PRIMARY KEY,
  upvar_dataset_id UUID NOT NULL,
  variable_id      UUID,
  name             TEXT,
  description      TEXT,
  ordinal          INTEGER NOT NULL,
  status           INT DEFAULT 1,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB,
  datarow          JSONB
);

ALTER TABLE dashin.upvar_datatable OWNER TO :application_user;
CREATE TABLE dashin.upvar_variable
(
  upvar_variable_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_variable_pk
      PRIMARY KEY,
  upvar_dataset_id  UUID,
  local_id          INTEGER NOT NULL,
  name              TEXT,
  description       TEXT,
  status            INT DEFAULT 1,
  datatype_id       INT DEFAULT 1,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details           JSONB
);

ALTER TABLE dashin.upvar_variable OWNER TO :application_user;
CREATE TABLE dashin.var_dataset
(
  var_dataset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT var_dataset_pk
      PRIMARY KEY,
  dataset_id     UUID UNIQUE,
  name           TEXT,
  description    TEXT,
  status         INT DEFAULT 1,
  created_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details        JSONB,
  CONSTRAINT var_dataset_dataset_id_var_dataset_id_key UNIQUE (var_dataset_id, dataset_id)
);

ALTER TABLE dashin.var_dataset OWNER TO :application_user;
CREATE TABLE dashin.var_datatable
(
  variable_id    UUID NOT NULL,
  var_dataset_id UUID NOT NULL,
  ordinal        INTEGER NOT NULL,
  status         INT DEFAULT 1,
  created_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details        JSONB,
  datarow        JSONB,
  CONSTRAINT var_datatable_pk
    PRIMARY KEY (variable_id, var_dataset_id),
  CONSTRAINT var_datatable_ordinal_uk UNIQUE (var_dataset_id, ordinal)
);

ALTER TABLE dashin.var_datatable OWNER TO :application_user;
CREATE TABLE dashin.var_variable
(
  var_variable_id   UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT var_variable_pk
      PRIMARY KEY,
  var_dataset_id    UUID,
  local_id          INTEGER NOT NULL,
  name              TEXT,
  description       TEXT,
  status            INT DEFAULT 1,
  search_single_id  INT,
  search_generic_id INT,
  datatype_id       INT DEFAULT 1,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details           JSONB,
  CONSTRAINT var_variable_search_map_uk UNIQUE (var_dataset_id, search_single_id),
  CONSTRAINT var_variable_local_id_uk UNIQUE (var_dataset_id, local_id)
);

ALTER TABLE dashin.var_variable OWNER TO :application_user;
CREATE TABLE dashin.variable
(
  variable_id      UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT variable_pk
      PRIMARY KEY,
  local_id         INTEGER NOT NULL,
  dataset_id       UUID,
  name             TEXT,
  description      TEXT,
  status           INT DEFAULT 1,
  variabletype_id  INT DEFAULT 1,
  datatype_id      INT DEFAULT 1,
  researchfield_id INT DEFAULT 1,
  assay_id         UUID,
  nunitprefix_id   INT DEFAULT 0,
  nunit_id         INT DEFAULT 0,
  dunitprefix_id   INT DEFAULT 0,
  dunit_id         INT DEFAULT 0,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB,
  CONSTRAINT dataset_local_id_uk UNIQUE (dataset_id, local_id),
  CONSTRAINT variable_variable_id_variabletype_id_uk
    UNIQUE (variable_id, variabletype_id),
  -- needed by studyvariable_x_variable (prevent dataset-variable mismatch in studyvariable and studyrowset)
  CONSTRAINT variable_dataset_id_variable_id_uk
    UNIQUE (dataset_id, variable_id)
);

ALTER TABLE dashin.variable OWNER TO :application_user;

CREATE TABLE dashin.variabletype
(
  variabletype_id INT NOT NULL
    CONSTRAINT variabletype_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT
);

ALTER TABLE dashin.variabletype OWNER TO :application_user;
CREATE TABLE dashin.studydesign_sampling_type
(
  sampling_type_id       INTEGER NOT NULL
    CONSTRAINT studydesign_sampling_type_pk
      PRIMARY KEY,
  ordinal                INTEGER,
  created_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                   TEXT,
  description            TEXT,
  additional_data_schema JSONB
);

ALTER TABLE dashin.studydesign_sampling_type OWNER TO :application_user;
CREATE TABLE dashin.studydesign_subevent_type
(
  subevent_type_id       INTEGER NOT NULL
    CONSTRAINT studydesign_subevent_type_pk
      PRIMARY KEY,
  ordinal                TEXT,
  created_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                   TEXT NOT NULL,
  description            TEXT,
  additional_data_schema JSONB
);

ALTER TABLE dashin.studydesign_subevent_type OWNER TO :application_user;
CREATE TABLE dashin.studydesign_x_datadesign
(
  study_id   UUID REFERENCES dashin.study ON DELETE CASCADE ON UPDATE CASCADE,
  dataset_id UUID,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT studydesign_import_from_data_pk PRIMARY KEY (study_id, dataset_id)
);

ALTER TABLE dashin.studydesign_x_datadesign OWNER TO :application_user;
CREATE TABLE dashin.study_center
(
  center_id   UUID CONSTRAINT study_center_pk PRIMARY KEY,
  study_id    UUID,
  ordinal     INT,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now(),
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT now(),
  name        TEXT,
  label       TEXT,
  description TEXT,
  CONSTRAINT study_center_name_uk UNIQUE (study_id, name)
);
ALTER TABLE dashin.study_center OWNER TO :application_user;

CREATE TABLE dashin.study_event
(
  event_id    UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_event_pk
      PRIMARY KEY,
  study_id    UUID,
  ordinal     INT,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name        TEXT,
  label       TEXT,
  description TEXT,
  CONSTRAINT study_event_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_event OWNER TO :application_user;
CREATE TABLE dashin.study_subevent
(
  subevent_id          UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_subevent_pk
      PRIMARY KEY,
  subevent_type_id     INTEGER NOT NULL,
  study_id             UUID,
  ordinal              INTEGER,
  intervention_type_id INTEGER,
  created_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                 TEXT,
  label                TEXT,
  description          TEXT,
  row_comment          TEXT,
  additional_data      JSONB,
  CONSTRAINT study_subevent_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_subevent OWNER TO :application_user;
CREATE TABLE dashin.study_samplingevent
(
  samplingevent_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_samplingevent_pk
      PRIMARY KEY,
  sampling_type_id INTEGER,
  study_id         UUID NOT NULL,
  ordinal          INTEGER,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name             TEXT,
  label            TEXT,
  description      TEXT,
  row_comment      TEXT,
  additional_data  JSONB,
  CONSTRAINT study_samplingevent_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_samplingevent OWNER TO :application_user;

CREATE TABLE dashin.study_samplingtime
(
  samplingtime_id  UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_samplingtime_pk
      PRIMARY KEY,
  samplingevent_id UUID NOT NULL,
  ordinal_time     INTEGER DEFAULT 0,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name             TEXT DEFAULT '0'::TEXT,
  label            TEXT DEFAULT '0'::TEXT,
  description      TEXT,
  CONSTRAINT study_samplingtime_samplingevent_name_uk UNIQUE (samplingevent_id,name)
);

ALTER TABLE dashin.study_samplingtime OWNER TO :application_user;

CREATE TABLE dashin.study_startgroup
(
  startgroup_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_startgroup_pk
      PRIMARY KEY,
  study_id      UUID,
  ordinal       INT,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name          TEXT,
  label         TEXT,
  description   TEXT,
  -- needed by fk from studydesign_startgroup_x_subject
  CONSTRAINT study_startgroup_startgroup_id_study_id_uk UNIQUE (startgroup_id, study_id),
  CONSTRAINT study_startgroup_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_startgroup OWNER TO :application_user;
CREATE TABLE dashin.study_subject
(
  subject_id    UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT subject_pk
      PRIMARY KEY,
  study_id      UUID NOT NULL,
  center_id     UUID,
  startgroup_id UUID,
  name  TEXT NOT NULL,
  label         TEXT,
  description   TEXT,
  status        INTEGER DEFAULT 1,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now(),
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now(),
  details       JSONB,
  CONSTRAINT study_subject_study_id_name_uk UNIQUE (study_id, center_id, name)
);

ALTER TABLE dashin.study_subject OWNER TO :application_user;
CREATE TABLE dashin.study_event_x_subevent
(
  event_id    UUID NOT NULL,
  subevent_id UUID NOT NULL,
  study_id    UUID NOT NULL,
  CONSTRAINT study_event_x_subevent_pk
    PRIMARY KEY (event_id, subevent_id)
);

ALTER TABLE dashin.study_event_x_subevent OWNER TO :application_user;
CREATE TABLE dashin.study_subevent_x_samplingevent
(
  subevent_id      UUID NOT NULL,
  samplingevent_id UUID NOT NULL,
  study_id         UUID NOT NULL,
  CONSTRAINT study_subevent_x_samplingevent_pk
    PRIMARY KEY (subevent_id, samplingevent_id)
);

ALTER TABLE dashin.study_subevent_x_samplingevent OWNER TO :application_user;
CREATE TABLE dashin.study_event_subevent_x_startgroup
(
  event_id      UUID NOT NULL,
  subevent_id   UUID NOT NULL,
  study_id      UUID NOT NULL,
  startgroup_id UUID NOT NULL,
  CONSTRAINT study_event_subevent_x_startgroup_pk
    PRIMARY KEY (event_id, subevent_id, study_id, startgroup_id)
);

ALTER TABLE dashin.study_event_subevent_x_startgroup OWNER TO :application_user;
CREATE TABLE dashin.studyvariable
(
  studyvariable_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id         UUID NOT NULL,
  ordinal          INT,
  name             TEXT NOT NULL,
  label            TEXT,
  description      TEXT,
  datatype_id      INT,
  variabletype_id  INT,
  CONSTRAINT studyvariable_uk UNIQUE (study_id, name),
  -- needed by fk from studyrowset_x_studyvariable (prevents studyvar from wrong study)
  CONSTRAINT studyvariable_study_id_studyvariable_id_uk UNIQUE (study_id, studyvariable_id)
);

ALTER TABLE dashin.studyvariable OWNER TO :application_user;
CREATE TABLE dashin.studyvariable_x_variable
(
  studyvariable_id UUID NOT NULL,
  study_id         UUID NOT NULL,
  variable_id      UUID NOT NULL,
  dataset_id       UUID NOT NULL,
  CONSTRAINT studyvariable_x_variable_pk PRIMARY KEY (studyvariable_id, variable_id)
);

ALTER TABLE dashin.studyvariable_x_variable OWNER TO :application_user;



CREATE TABLE dashin.studydataset
(
  study_id   UUID PRIMARY KEY,
  status     INT DEFAULT 1,
  created_at TIMESTAMPTZ DEFAULT now()
);

ALTER TABLE dashin.studydataset OWNER TO :application_user;
CREATE TABLE dashin.studydatatable
(
  studyrow_id      UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id         UUID,
  event_id         UUID,
  subevent_id      UUID,
  samplingevent_id UUID,
  samplingtime_id  UUID,
  center_id        UUID,
  startgroup_id    UUID,
  subject_id       UUID,
  CONSTRAINT studydatatable_all_unique_uk UNIQUE (study_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id),
  -- used by studyrow_x_datarow
  CONSTRAINT studydatatable_study_studyrow_uk UNIQUE (study_id, studyrow_id)
);

ALTER TABLE dashin.studydatatable OWNER TO :application_user;

CREATE TABLE dashin.studyrow_x_datarow
(
  studyrow_id UUID NOT NULL,
  study_id    UUID NOT NULL,
  datarow_id  UUID NOT NULL,
  dataset_id  UUID NOT NULL,
  CONSTRAINT studyrow_x_datarow_datarow_uk UNIQUE (datarow_id),
  CONSTRAINT studyrow_x_datarow_all_uk UNIQUE (studyrow_id, datarow_id)
);

ALTER TABLE dashin.studyrow_x_datarow OWNER TO :application_user;
CREATE TABLE dashin.selection_choice
(
  selection_choice_id INTEGER NOT NULL,
  selection_type_id   INTEGER NOT NULL,
  ordinal             INTEGER,
  name                TEXT,
  description         TEXT,
  CONSTRAINT selection_choice_pk
    PRIMARY KEY (selection_type_id, selection_choice_id)
);

ALTER TABLE dashin.selection_choice OWNER TO :application_user;
CREATE TABLE dashin.selection_type
(
  selection_type_id INTEGER NOT NULL
    CONSTRAINT selection_type_pk
      PRIMARY KEY,
  name              TEXT,
  description       TEXT
);

ALTER TABLE dashin.selection_type OWNER TO :application_user;
CREATE TABLE dashin.var_variable_x_search_column_type
(
  search_column_type_id INTEGER NOT NULL,
  var_variable_id       UUID NOT NULL,
  dataset_id            UUID,
  var_dataset_id        UUID,
  CONSTRAINT var_variable_x_search_column_type_pk
    PRIMARY KEY (var_variable_id)
);

ALTER TABLE dashin.var_variable_x_search_column_type OWNER TO :application_user;

CREATE TABLE dashin.unit
(
  unit_id       INT PRIMARY KEY ,
  unitdomain_id INT,
  ordinal       INT,
  symbol        TEXT,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.unit OWNER TO :application_user;

CREATE TABLE dashin.unitdomain
(
  unitdomain_id INT PRIMARY KEY,
  ordinal       INT,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.unitdomain OWNER TO :application_user;
CREATE TABLE dashin.unitprefix
(
  unitprefix_id     INT PRIMARY KEY,
  unitprefixdomain_id INT,
  ordinal           INT,
  symbol            TEXT,
  numeric_label     TEXT,
  name              TEXT,
  description       TEXT,
  value             NUMERIC

);

ALTER TABLE dashin.unitprefix OWNER TO :application_user;
CREATE TABLE dashin.unitprefixdomain
(
  unitprefixdomain_id INT PRIMARY KEY,
  ordinal             INT,
  name                TEXT,
  description         TEXT
);

ALTER TABLE dashin.unitprefixdomain OWNER TO :application_user;
CREATE TABLE dashin.unit_conversion_factor
(
  conventional_id   INT,
  si_id             INT,
  conversion_factor NUMERIC
);
ALTER TABLE dashin.unit_conversion_factor OWNER TO :application_user;
CREATE TABLE dashin.prefixedunit
(
  prefixedunit_id     SERIAL,
  unitprefix_id       INT,
  unit_id             INT,
  combined_symbol     TEXT,
  name                TEXT,
  unitdomain_id       INT,
  unitprefixdomain_id INT,
  description         TEXT,
  valid               INT DEFAULT 0,
  CONSTRAINT prefixedunit_pk PRIMARY KEY (unitprefix_id, unit_id),
  CONSTRAINT prefixedunit_uk UNIQUE (prefixedunit_id)
);

ALTER TABLE dashin.prefixedunit OWNER TO :application_user;
CREATE TABLE dashin.conditionoperator
(
    conditionoperator_id INT PRIMARY KEY,
    context_flag         INT NOT NULL,
    symbol               TEXT NOT NULL,
    shortname            TEXT NOT NULL,
    name                 TEXT NOT NULL,
    label                TEXT NOT NULL,
    description          TEXT NOT NULL
);

ALTER TABLE dashin.conditionoperator OWNER TO :application_user;

CREATE TABLE dashin.studyvariablecondition
(
  studyvariablecondition_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  studyvariable_id          UUID NOT NULL,
  study_id                  UUID NOT NULL,
  name                      TEXT,
  description               TEXT,
  datatype_id               INT,
  conditionoperator_id      INT NOT NULL DEFAULT 0,
  txtval1                   TEXT,
  txtval2                   TEXT,
  details                   JSONB,
  CONSTRAINT studyvariablecondition_uk UNIQUE (study_id, studyvariablecondition_id)
);

ALTER TABLE dashin.studyvariablecondition OWNER TO :application_user;
CREATE TABLE dashin.studyvariablecondition_x_category
(
  studyvariablecondition_id UUID,
  study_id                  UUID,
  element_id                UUID,
  value_column              TEXT,
  CONSTRAINT studyvariablecondition_x_category_var_id_element_id_uk UNIQUE (studyvariablecondition_id, study_id, element_id),
  CONSTRAINT studyvariablecondition_x_category_var_id_value_uk UNIQUE (studyvariablecondition_id, value_column)
);

ALTER TABLE dashin.studyvariablecondition_x_category OWNER TO :application_user;
CREATE TABLE dashin.studyvariablecondition_x_samplingtimecategory
(
  studyvariablecondition_id UUID,
  study_id                  UUID,
  samplingevent_id          UUID,
  samplingtime_id           UUID,
  value_column              TEXT,
  CONSTRAINT studyvariablecondition_x_stimecategory_var_id_element_id_uk
    UNIQUE (studyvariablecondition_id, samplingtime_id)
);

ALTER TABLE dashin.studyvariablecondition_x_samplingtimecategory OWNER TO :application_user;
CREATE TABLE dashin.studyrowset
(
  studyrowset_id UUID CONSTRAINT studyrowset_pk PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id       UUID,
  name           TEXT,
  description    TEXT,
  created_at     TIMESTAMPTZ DEFAULT now(),
  updated_at     TIMESTAMPTZ DEFAULT now(),
  CONSTRAINT studyrowset_study_id_studyrowset_id_uk UNIQUE (studyrowset_id, study_id)
);

ALTER TABLE dashin.studyrowset OWNER TO :application_user;
CREATE TABLE dashin.studyrowsubset
(
  studyrowsubset_id UUID CONSTRAINT studyrowsubset_pk DEFAULT uuid_generate_v4() NOT NULL,
  studyrowset_id    UUID,
  study_id          UUID,
  name              TEXT,
  description       TEXT,
  CONSTRAINT studyrowsubset_subset_x_varset_uk
    UNIQUE (studyrowsubset_id, study_id)
);

ALTER TABLE dashin.studyrowsubset OWNER TO :application_user;
CREATE TABLE dashin.studyrowsubset_x_studyvariablecondition
(
  studyrowsubset_id         UUID NOT NULL,
  studyvariablecondition_id UUID NOT NULL,
  study_id                  UUID,
  CONSTRAINT studyrowsubset_x_studyvariablecondition_pk
    PRIMARY KEY (studyrowsubset_id, studyvariablecondition_id)
);

ALTER TABLE dashin.studyrowsubset_x_studyvariablecondition OWNER TO :application_user;
CREATE TABLE dashin.studyvariableset
(
  studyvariableset_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id            UUID,
  name                TEXT,
  description         TEXT,
  created_at          TIMESTAMPTZ DEFAULT now(),
  updated_at          TIMESTAMPTZ DEFAULT now(),
  CONSTRAINT studyvariableset_study_id_studyvariableset_id_uk UNIQUE (studyvariableset_id, study_id)
);

ALTER TABLE dashin.studyvariableset OWNER TO :application_user;
CREATE TABLE dashin.studyvariableset_x_studyvariable
(
    studyvariableset_id UUID,
    studyvariable_id    UUID,
    study_id            UUID,
    CONSTRAINT studyvariableset_x_studyvariable_pk PRIMARY KEY (studyvariableset_id, studyvariable_id)
);

ALTER TABLE dashin.studyvariableset_x_studyvariable OWNER TO :application_user;
CREATE TABLE dashin.studyexportset
(
    studyexportset_id   UUID CONSTRAINT studyexportset_pk PRIMARY KEY DEFAULT uuid_generate_v4(),
    study_id            UUID,
    studyrowset_id      UUID,
    studyvariableset_id UUID,
    name                TEXT,
    description         TEXT,
    created_at          TIMESTAMPTZ DEFAULT now(),
    updated_at          TIMESTAMPTZ DEFAULT now()
);

ALTER TABLE dashin.studyexportset OWNER TO :application_user;
