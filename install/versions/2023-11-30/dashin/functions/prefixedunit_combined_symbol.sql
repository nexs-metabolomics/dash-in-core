CREATE FUNCTION dashin.prefixedunit_combined_symbol
(
    _n_unit_id   INT
  , _n_prefix_id INT
  , _d_unit_id   INT
  , _d_prefix_id INT
  , _n_symbol    TEXT
  , _d_symbol    TEXT
) RETURNS TEXT
  LANGUAGE sql
  IMMUTABLE
AS
$$
SELECT CASE
         WHEN (_n_prefix_id, _n_unit_id) IN ((0, 0)) OR (_d_prefix_id, _d_unit_id) IN ((0, 0)) THEN ''
         WHEN length(_n_symbol) = 0 AND length(_d_symbol) = 0                                  THEN ''
         WHEN length(_n_symbol) = 0 AND length(_d_symbol) > 0                                  THEN '1/' || _d_symbol
         WHEN length(_n_symbol) > 0 AND length(_d_symbol) = 0                                  THEN _n_symbol
         WHEN length(_n_symbol) > 0 AND length(_d_symbol) > 0                                  THEN _n_symbol || '/' || _d_symbol
         ELSE ''
       END
$$;

