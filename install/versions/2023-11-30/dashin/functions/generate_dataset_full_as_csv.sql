CREATE FUNCTION dashin.generate_dataset_full_as_csv
(
  _dataset_id UUID
)
  RETURNS TABLE
          (
            DATASET TEXT
          )
  LANGUAGE sql
AS
$$
SELECT string_agg(dat , E'\n')
FROM (
       SELECT dataset_id                               AS datarow_id
            , 0                                        AS ordinal
            , string_agg(name , ';' ORDER BY local_id) AS dat
       FROM dashin.variable
       WHERE dataset_id = _dataset_id
       GROUP BY dataset_id
       UNION ALL
       SELECT datarow_id
            , ordinal
            , string_agg(value , ';' ORDER BY ordinality) AS dat
       FROM (
              SELECT datarow_id
                   , ordinal
                   , value
                   , ordinality
              FROM dashin.datatable
                 , jsonb_array_elements_text(datarow)
                WITH ORDINALITY
              WHERE dataset_id = _dataset_id
            ) x0
       GROUP BY datarow_id
              , ordinal
       ORDER BY ordinal
     ) x1;
$$;

ALTER FUNCTION dashin.generate_dataset_full_as_csv(UUID) OWNER TO :application_user;
