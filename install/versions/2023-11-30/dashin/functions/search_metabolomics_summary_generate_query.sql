CREATE FUNCTION dashin.search_metabolomics_summary_generate_query
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
)
  RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry        TEXT;
  _mz_str    TEXT DEFAULT '';
  _rt_str    TEXT DEFAULT '';
  _mz_rt_str TEXT DEFAULT '';
BEGIN
  -- mz
  IF _mz_center IS NOT NULL AND _mz_range IS NOT NULL
  THEN
    _mz_str := '(mz BETWEEN ' || _mz_center - _mz_range || ' AND ' || _mz_center + _mz_range || ' OR mz IS NULL)';
  ELSEIF _mz_center IS NOT NULL
  THEN
    _mz_str := '(mz = ' || _mz_center || ' OR mz IS NULL)';
  END IF;
  
  -- rt
  IF _rt_center IS NOT NULL AND _rt_range IS NOT NULL
  THEN
    _rt_str := '(rt BETWEEN ' || _rt_center - _rt_range || ' AND ' || _rt_center + _rt_range || ' OR rt IS NULL)';
  ELSEIF _rt_center IS NOT NULL
  THEN
    _rt_str := '(rt = ' || _rt_center || ' OR rt IS NULL)';
  END IF;
  
  -- rt+mz
  IF length(_mz_str) > 0 AND length(_rt_str) > 0 THEN
    _mz_rt_str := _mz_str || ' AND ' || _rt_str;
  ELSEIF length(_mz_str) > 0 THEN
    _mz_rt_str := _mz_str;
  ELSEIF length(_rt_str) > 0 THEN
    _mz_rt_str := _rt_str;
  END IF;
  
  qry := (
           SELECT E'SELECT\n' ||
                  E'  f.study_id' ||
                  E', d.dataset_id\n' ||
                  E', f.name AS study_name' ||
                  E', d.name AS dataset_name' ||
                  E', min(x1.mz) AS min_mz' ||
                  E', max(x1.mz) AS max_mz' ||
                  E', min(x1.rt) AS min_rt' ||
                  E', max(x1.rt) AS max_rt' ||
                  E', count(*)::INT AS n\n' ||
                  E', max(x1.n_all) AS n_all\n' ||
                  E'FROM\n(\n' ||
                  E'SELECT\n variable_id,mz,rt,(count(*) OVER ())::INT AS n_all\nFROM\n(\n' ||
                  E'SELECT coalesce(a.variable_id,b.variable_id) as variable_id,mz,rt \n' ||
                  E'FROM\n(' || dashin.search_generate_column_query_numeric(3) || E') a (variable_id,rt)\n' ||
                  E'FULL JOIN\n(\n' || dashin.search_generate_column_query_numeric(2) || E')b (variable_id,mz) ON a.variable_id = b.variable_id\n' ||
                  E'WHERE ' || _mz_rt_str || E'\n' ||
                  E'AND NOT (rt IS NULL AND mz IS NULL)\n' ||
                  E') x0\n' ||
                  E') x1\n' ||
                  E' LEFT JOIN dashin.variable        v ON x1.variable_id = v.variable_id\n' ||
                  E' LEFT JOIN dashin.dataset         d ON v.dataset_id = d.dataset_id\n' ||
                  E' LEFT JOIN dashin.dataset_x_study e ON d.dataset_id = e.dataset_id\n' ||
                  E' LEFT JOIN dashin.study           f ON e.study_id = f.study_id\n' ||
                  E'GROUP BY f.study_id,f.name,d.dataset_id,d.name'
         );
  RETURN qry;
END;
$$
;


ALTER FUNCTION dashin.search_metabolomics_summary_generate_query OWNER TO :application_user;
