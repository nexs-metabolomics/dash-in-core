CREATE FUNCTION dashin.datadesign_event_x_subevent_exists
(
    _event_id    UUID
  , _subevent_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT exists(
  SELECT datadesign_event_id
       , datadesign_subevent_id
       , b.dataset_id
  FROM
    -- event-subevent link
    dashin.study_event_x_subevent                   a
      -- study-to-data link for individual elements
      INNER JOIN dashin.datadesign_x_study_event    b ON a.event_id = b.study_event_id
      INNER JOIN dashin.datadesign_x_study_subevent c ON (b.dataset_id, a.subevent_id) = (c.dataset_id, c.study_subevent_id)
                   -- event-x-subevent link in datadesign
      INNER JOIN dashin.datadesign_datatable        d ON (d.event_id, d.subevent_id) = (b.datadesign_event_id, c.datadesign_subevent_id)
    -- specific event + subevent
  WHERE (a.event_id, a.subevent_id) = (_event_id, _subevent_id)
  );
$$
;
