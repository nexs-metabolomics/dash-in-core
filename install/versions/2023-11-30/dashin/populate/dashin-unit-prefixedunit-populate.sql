-- -------------------------------------------------------------------------------------
-- pupulate table
-- -------------------------------------------------------------------------------------
INSERT INTO
  dashin.prefixedunit (unitprefix_id, unit_id, combined_symbol, name)
SELECT
  b.unitprefix_id
  , c.unit_id
  , CASE
      WHEN (b.unitprefix_id, c.unit_id) = (0, 0) THEN 'N/A'
      WHEN (b.unitprefix_id, c.unit_id) = (0, 1) THEN ''
      WHEN (b.unitprefix_id, c.unit_id) = (1, 0) THEN ''
      WHEN unit_id = 16 AND unitprefix_id = 1    THEN ''
      WHEN unit_id = 16 AND unitprefix_id = 8    THEN 'x10^6'
      WHEN unit_id = 16 AND unitprefix_id = 9    THEN 'x10^9'
      WHEN unit_id = 16 AND unitprefix_id = 10   THEN 'x10^12'
      ELSE b.symbol || c.symbol
    END AS combined_symbol
  , CASE
      WHEN (b.unitprefix_id, c.unit_id) = (0, 0)       THEN 'N/A'
      WHEN (b.unitprefix_id, c.unit_id) = (1, 0)       THEN 'one'
      WHEN b.unitprefix_id = 0 AND c.unit_id >= 1      THEN 'none'
      WHEN b.unitprefix_id = 1 AND c.unit_id > 0       THEN c.name
      WHEN b.unitprefix_id > 1 AND c.unit_id > 0       THEN b.name || c.name
      WHEN b.unitprefix_id IN (0, 1) AND c.unit_id = 1 THEN c.name
    END AS name
FROM
  dashin.unitprefix        b
    CROSS JOIN dashin.unit c
WHERE
  NOT ((b.unitprefix_id = 0 AND c.unit_id > 1) OR (b.unitprefix_id > 0 AND c.unit_id = 0))
ORDER BY
  unit_id
  , unitprefix_id
ON CONFLICT (unitprefix_id,unit_id) DO NOTHING
;

-- -------------------------------------------------------------------------------------
-- populate: update valid
-- -------------------------------------------------------------------------------------
UPDATE dashin.prefixedunit
SET
  valid=
    CASE
      WHEN unitprefix_id = 0 AND unit_id = 0   THEN 2
      WHEN unitprefix_id = 0 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 1 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 3 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 4 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 1   THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 1   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 1   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 1  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 1  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 1  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 1  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 2   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 4 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 2   THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 2   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 2   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 2  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 2  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 2  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 2  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 3   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 3   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 3   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 3   THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 3   THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 3   THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 3   THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 3   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 3   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 3  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 3  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 3  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 3  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 3 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 4 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 4   THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 4   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 4   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 4  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 4  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 4  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 4  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 5   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 5   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 5  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 5  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 5  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 5  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 6   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 6   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 6  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 6  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 6  THEN 2
      WHEN unitprefix_id = 13 AND unit_id = 6  THEN 2
      WHEN unitprefix_id = 1 AND unit_id = 7   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 7   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 7  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 7  THEN 2
      WHEN unitprefix_id = 12 AND unit_id = 7  THEN 2
      WHEN unitprefix_id = 13 AND unit_id = 7  THEN 2
      WHEN unitprefix_id = 1 AND unit_id = 8   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 8   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 8  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 8  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 8  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 8  THEN 2
      WHEN unitprefix_id = 1 AND unit_id = 9   THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 9   THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 9   THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 9   THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 9   THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 9   THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 9   THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 9   THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 9   THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 9  THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 9  THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 9  THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 9  THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 10  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 10  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 10 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 10 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 10 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 10 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 11  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 11  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 11 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 11 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 11 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 11 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 12  THEN 0
      WHEN unitprefix_id = 2 AND unit_id = 12  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 12  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 12  THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 12  THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 12  THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 12  THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 12  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 12  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 12 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 12 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 12 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 12 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 13  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 13  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 13  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 13  THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 13  THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 13  THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 13  THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 13  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 13  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 13 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 13 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 13 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 13 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 14  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 14  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 14  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 14  THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 14  THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 14  THEN 2
      WHEN unitprefix_id = 7 AND unit_id = 14  THEN 2
      WHEN unitprefix_id = 8 AND unit_id = 14  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 14  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 14 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 14 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 14 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 14 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 15  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 15  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 15 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 15 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 15 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 15 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 16  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 16  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 16  THEN 2
      WHEN unitprefix_id = 9 AND unit_id = 16  THEN 2
      WHEN unitprefix_id = 10 AND unit_id = 16 THEN 2
      WHEN unitprefix_id = 11 AND unit_id = 16 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 16 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 16 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 17  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 17  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 17 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 17 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 17 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 17 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 18  THEN 2
      WHEN unitprefix_id = 2 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 5 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 6 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 18  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 18 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 18 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 18 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 18 THEN 0
      WHEN unitprefix_id = 1 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 2 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 3 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 4 AND unit_id = 19  THEN 2
      WHEN unitprefix_id = 5 AND unit_id = 19  THEN 2
      WHEN unitprefix_id = 6 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 7 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 8 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 9 AND unit_id = 19  THEN 0
      WHEN unitprefix_id = 10 AND unit_id = 19 THEN 0
      WHEN unitprefix_id = 11 AND unit_id = 19 THEN 0
      WHEN unitprefix_id = 12 AND unit_id = 19 THEN 0
      WHEN unitprefix_id = 13 AND unit_id = 19 THEN 0
    END
WHERE
    valid = 0
;