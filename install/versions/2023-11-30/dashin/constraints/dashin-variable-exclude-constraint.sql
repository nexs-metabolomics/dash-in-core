-- ---------------------------------------------------------------------
-- used as part of unique exclusion constraint for datadesign
-- ---------------------------------------------------------------------
CREATE FUNCTION dashin.variable_exclude_return_datasetdesign_variabletype_ids
(
  _vartype INT
)
  RETURNS INT
  LANGUAGE sql
  IMMUTABLE
AS
$$
SELECT _vartype
WHERE _vartype IN (2, 3, 4, 5, 6, 7, 8)
$$;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_datadesign_variabletype_key_exclude EXCLUDE
    USING gist (dataset_id WITH =,dashin.variable_exclude_return_datasetdesign_variabletype_ids(variabletype_id) WITH =);
