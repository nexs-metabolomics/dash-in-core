\echo '----------------------------------------------------------------'
\echo ' CREATE DASHIN FUNCTIONS'
\echo '----------------------------------------------------------------'

CREATE FUNCTION dashin.get_designvariabletype_ids
(
  VARIADIC _vartype TEXT[] DEFAULT '{all}'::TEXT[]
)
  RETURNS TABLE
          (
            VARIABLETYPE_ID INTEGER
          )
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
BEGIN
  IF _vartype = '{all}' THEN
    RETURN QUERY VALUES (2), (3), (4), (5), (6), (7), (8);
  ELSE
    RETURN QUERY
      SELECT x0.variabletype_id
      FROM (
             VALUES ('subject', 2)
                  , ('startgroup', 3)
                  , ('event', 4)
                  , ('subevent', 5)
                  , ('samplingevent', 6)
                  , ('samplingtime', 7)
                  , ('center', 8)
           ) x0(name, variabletype_id)
      WHERE ARRAY [x0.name] <@ _vartype;
  END IF;
  RETURN;
END;
$$;
CREATE FUNCTION dashin.datadesign_design_variablenames
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID      UUID,
            N_VALID         INT,
            VARIABLE_ID     UUID,
            VARIABLETYPE_ID INT,
            COLUMN_ORDER    INT,
            TYPE_CODE       TEXT,
            VARIABLETYPE    TEXT,
            VARIABLENAME    TEXT
          )
  LANGUAGE sql
AS
$$
SELECT _dataset_id::UUID                                  AS dataset_id
     , (sum((variable_id IS NOT NULL)::INT) OVER ())::INT AS n
     , v.variable_id
     , t.variabletype_id
     , t.column_order
     , t.type_code
     , t.name                                             AS variabletype
     , v.variablename
FROM (
       SELECT variabletype_id
            , CASE variabletype_id
                WHEN 8 THEN 1
                WHEN 4 THEN 2
                WHEN 5 THEN 3
                WHEN 6 THEN 4
                WHEN 7 THEN 5
                WHEN 3 THEN 6
                WHEN 2 THEN 7
              END AS column_order
            , CASE variabletype_id
                WHEN 4 THEN 'ev'
                WHEN 3 THEN 'sg'
                WHEN 2 THEN 'sb'
                WHEN 5 THEN 'se'
                WHEN 6 THEN 'sm'
                WHEN 7 THEN 'st'
                WHEN 8 THEN 'cn'
              END AS type_code
            , name
            , description
       FROM dashin.variabletype
       WHERE variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
       ORDER BY variabletype_id
     ) t
  LEFT JOIN
(
  SELECT variable_id
       , variabletype_id
       , v.name AS variablename
  FROM dashin.variable       v
    LEFT JOIN dashin.dataset d USING (dataset_id)
  WHERE v.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
    AND v.dataset_id = _dataset_id
    AND (d.owner_org = _owner_org OR _owner_org IS NULL)
)      v USING (variabletype_id)
ORDER BY column_order;
$$;

ALTER FUNCTION dashin.datadesign_design_variablenames OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_variable_get_local_id
(
  _dataset_id    UUID
, _variabletype_id INT
) RETURNS INT
  LANGUAGE SQL
  IMMUTABLE STRICT
AS $$
SELECT
  local_id
FROM
  dashin.variable
WHERE dataset_id = _dataset_id
  AND variabletype_id = _variabletype_id;
$$;

ALTER FUNCTION dashin.datadesign_variable_get_local_id(_dataset_id UUID, _variabletype_id INT) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_variable_get_local_id
(
    _dataset_id    UUID
  , _variabletype TEXT
) RETURNS INT
  LANGUAGE sql
  IMMUTABLE STRICT
AS
$$
SELECT local_id
FROM dashin.variable
WHERE dataset_id = _dataset_id
  AND variabletype_id = (
  CASE _variabletype
    WHEN 'subject'       THEN 2
    WHEN 'startgroup'    THEN 3
    WHEN 'event'         THEN 4
    WHEN 'subevent'      THEN 5
    WHEN 'samplingevent' THEN 6
    WHEN 'sampling'      THEN 6
    WHEN 'time'          THEN 7
    WHEN 'samplingtime'  THEN 7
    WHEN 'center'        THEN 8
  END
  );
$$;

ALTER FUNCTION dashin.datadesign_variable_get_local_id(_dataset_id UUID, _variabletype TEXT) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_unvalidated_or_datatable
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID           UUID,
            DATASET_ID           UUID,
            EVENT_ID             UUID,
            SUBEVENT_ID          UUID,
            SAMPLINGEVENT_ID     UUID,
            SAMPLINGTIME_ID      UUID,
            CENTER_ID            UUID,
            STARTGROUP_ID        UUID,
            SUBJECT_ID           UUID,
            EVENT_NAME           TEXT,
            SUBEVENT_NAME        TEXT,
            SAMPLINGEVENT_NAME   TEXT,
            SAMPLINGTIME_NAME    TEXT,
            CENTER_NAME          TEXT,
            STARTGROUP_NAME      TEXT,
            SUBJECT_NAME         TEXT,
            EVENT_STATUS         INT,
            SUBEVENT_STATUS      INT,
            SAMPLINGEVENT_STATUS INT,
            SAMPLINGTIME_STATUS  INT,
            CENTER_STATUS        INT,
            STARTGROUP_STATUS    INT,
            SUBJECT_STATUS       INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  design_exists BOOL;
BEGIN
  design_exists := dashin.datadesign_exists(_dataset_id , _owner_org);
  IF design_exists THEN
    RETURN QUERY SELECT a.datarow_id
                      , a.dataset_id
                      , a.event_id
                      , a.subevent_id
                      , a.samplingevent_id
                      , a.samplingtime_id
                      , a.center_id
                      , a.startgroup_id
                      , a.subject_id
                      , a.event_name
                      , a.subevent_name
                      , a.samplingevent_name
                      , a.samplingtime_name
                      , a.center_name
                      , a.startgroup_name
                      , a.subject_name
                      , a.event_status
                      , a.subevent_status
                      , a.samplingevent_status
                      , a.samplingtime_status
                      , a.center_status
                      , a.startgroup_status
                      , a.subject_status
                 FROM dashin.datadesign_datatable a
                 WHERE a.dataset_id = _dataset_id;
    ELSE
      RETURN QUERY SELECT a.datarow_id
                        , a.dataset_id
                        , a.event_id
                        , a.subevent_id
                        , a.samplingevent_id
                        , a.samplingtime_id
                        , a.center_id
                        , a.startgroup_id
                        , a.subject_id
                        , a.event_name
                        , a.subevent_name
                        , a.samplingevent_name
                        , a.samplingtime_name
                        , a.center_name
                        , a.startgroup_name
                        , a.subject_name
                        , a.event_status
                        , a.subevent_status
                        , a.samplingevent_status
                        , a.samplingtime_status
                        , a.center_status
                        , a.startgroup_status
                        , a.subject_status
                   FROM
                     dashin.datadesign_unvalidated_design_datatable(_dataset_id , _owner_org) a;
  END IF;
  RETURN;
END;
$$;
CREATE FUNCTION dashin.conditionoperator_sqlcode
(
      _conditionoperator_id INT
    , _secondary_parm       TEXT DEFAULT ''
)
    RETURNS TEXT
    LANGUAGE sql
AS
$$
SELECT
    CASE
        WHEN _conditionoperator_id = 1                            THEN ' > '
        WHEN _conditionoperator_id = 2                            THEN ' >= '
        WHEN _conditionoperator_id = 3                            THEN ' = '
        WHEN _conditionoperator_id = 4                            THEN ' <= '
        WHEN _conditionoperator_id = 5                            THEN ' < '
        WHEN _conditionoperator_id = 6                            THEN ' <> '
        WHEN _conditionoperator_id = 7 AND _secondary_parm = 'l'  THEN ' < '
        WHEN _conditionoperator_id = 7 AND _secondary_parm = 'r'  THEN ' < '
        WHEN _conditionoperator_id = 8 AND _secondary_parm = 'l'  THEN ' < '
        WHEN _conditionoperator_id = 8 AND _secondary_parm = 'r'  THEN ' <= '
        WHEN _conditionoperator_id = 9 AND _secondary_parm = 'l'  THEN ' <= '
        WHEN _conditionoperator_id = 9 AND _secondary_parm = 'r'  THEN ' < '
        WHEN _conditionoperator_id = 10 AND _secondary_parm = 'l' THEN ' <= '
        WHEN _conditionoperator_id = 10 AND _secondary_parm = 'r' THEN ' <= '
    END
$$;
CREATE FUNCTION dashin.studyrowset_get_studyrows
(
    _studyrowset_id UUID
  , _study_id       UUID
  , _owner_org      UUID
)
  RETURNS TABLE
          (
            STUDYROW_ID UUID
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  _subset_is_empty BOOL;
  result_query     TEXT;
BEGIN
  
  -- check if studyrowsubset has any conditions 
  _subset_is_empty := NOT exists(SELECT
                                 FROM dashin.studyrowsubset                                  a
                                   INNER JOIN dashin.studyrowsubset_x_studyvariablecondition b ON a.studyrowsubset_id = b.studyrowsubset_id
                                 WHERE a.studyrowset_id = _studyrowset_id
                                   AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org)));
  
  -- if studyrowset has no conditions, return all studyrows
  IF _subset_is_empty THEN
    RETURN QUERY SELECT a.studyrow_id
                 FROM dashin.studydatatable a
                 WHERE a.study_id = _study_id
                   AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
    RETURN;
  END IF;
  
  -- generate query based on conditions
  WITH
    -- setup the basics, filter on 
    -- available conditions
    -- available stydvariables, datavariables
    -- check owner permissions
    base_query         AS (
                            SELECT a.studyrowset_id            AS a_studyrowset_id
                                 , a.study_id                  AS a_study_id
                                 , b.studyrowsubset_id         AS b_studyrowsubset_id
                                 , c.studyvariablecondition_id AS c_studyvariablecondition_id
                                 , d.conditionoperator_id      AS d_conditionoperator_id
                                 , d.txtval1                   AS d_txtval1
                                 , d.txtval2                   AS d_txtval2
                                 , e.studyvariable_id          AS e_studyvariable_id
                                 , e.ordinal                   AS e_ordinal
                                 , e.variabletype_id           AS e_variabletype_id
                            FROM dashin.studyrowset                                     a
                              INNER JOIN dashin.studyrowsubset                          b ON a.studyrowset_id = b.studyrowset_id
                              INNER JOIN dashin.studyrowsubset_x_studyvariablecondition c ON b.studyrowsubset_id = c.studyrowsubset_id
                              INNER JOIN dashin.studyvariablecondition                  d ON c.studyvariablecondition_id = d.studyvariablecondition_id
                              INNER JOIN dashin.studyvariable                           e ON d.studyvariable_id = e.studyvariable_id
                            WHERE a.studyrowset_id = _studyrowset_id
                              AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org))
                          )
    -- design variable conditions: samplingevent + samplingtime
    , dvars_smpt       AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'samplingtime_id IN (' || string_agg(quote_literal(x0.samplingtime_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , c.samplingtime_id
                                   FROM base_query                                                   a
                                     INNER JOIN dashin.studyvariablecondition_x_category             b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                     INNER JOIN dashin.studyvariablecondition_x_samplingtimecategory c ON a.c_studyvariablecondition_id = c.studyvariablecondition_id AND b.element_id = c.samplingevent_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('samplingevent' , 'samplingtime') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: event
    , dvars_event      AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'event_id IN (' || string_agg(quote_literal(x0.event_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS event_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('event') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: subevent
    , dvars_subevent   AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'subevent_id IN (' || string_agg(quote_literal(x0.subevent_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS subevent_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('subevent') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: subject
    , dvars_subject    AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'subject_id IN (' || string_agg(quote_literal(x0.subject_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS subject_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('subject') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: startgroup
    , dvars_startgroup AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'startgroup_id IN (' || string_agg(quote_literal(x0.startgroup_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS startgroup_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('startgroup') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: center
    , dvars_center     AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'center_id IN (' || string_agg(quote_literal(x0.center_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS center_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('center') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: combine all design variable conditions in a single query
    , dvars_all        AS (
                            SELECT *
                            FROM dvars_smpt
                            UNION
                            SELECT *
                            FROM dvars_event
                            UNION
                            SELECT *
                            FROM dvars_subevent
                            UNION
                            SELECT *
                            FROM dvars_subject
                            UNION
                            SELECT *
                            FROM dvars_startgroup
                            UNION
                            SELECT *
                            FROM dvars_center
                          )
    -- combine design-var conditions for each studyrowsubset (intersect: AND)
    , dvars_subset     AS (
                            SELECT E'(\nSELECT studyrow_id FROM dashin.studydatatable \nWHERE ' || string_agg(a.where_cond , E' AND \n') || E' \n)' AS dvars_single_rowsubset_query
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                            FROM dvars_all a
                            GROUP BY a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                          )
    -- non-design-vars, basic setup
    , othbase_query    AS (
                            SELECT a.a_studyrowset_id
                                 , a.a_study_id
                                 , a.b_studyrowsubset_id
                                 , a.c_studyvariablecondition_id
                                 , a.d_conditionoperator_id
                                 , a.d_txtval1
                                 , a.d_txtval2
                                 , a.e_studyvariable_id
                                 , a.e_ordinal
                                 , a.e_variabletype_id
                                 , g.local_id   AS g_local_id
                                 , h.dataset_id AS h_dataset_id
                            FROM base_query                              a
                              INNER JOIN dashin.studyvariable_x_variable f ON a.e_studyvariable_id = f.studyvariable_id
                              INNER JOIN dashin.variable                 g ON f.variable_id = g.variable_id
                              INNER JOIN dashin.dataset                  h ON g.dataset_id = h.dataset_id
                            WHERE a.e_variabletype_id NOT IN (
                                                               SELECT z.variabletype_id
                                                               FROM dashin.get_designvariabletype_ids('all') z
                                                             )
                          )
    -- non-design-vars: extract all conditions
    , othvars_query    AS (
                            SELECT 'jsonb_array_element_text(datarow,' || a.g_local_id || ')'                        AS varvalue
                                 , quote_ident(a.h_dataset_id || '_' || a.g_local_id || '_' || row_number() OVER ()) AS varname
                                 , a.d_txtval1
                                 , a.d_txtval2
                                 , a.d_conditionoperator_id
                                 , a.g_local_id
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.h_dataset_id
                                 , a.c_studyvariablecondition_id
                                 , a.e_studyvariable_id
                            FROM othbase_query a
                          )
    -- non-design-vars: categorical conditions
    , othvars_category AS (
                            SELECT a.varname || ' IN (' || string_agg(DISTINCT quote_literal(b.value_column) , ',') || ')' AS cond_string
                                 , a.varvalue || ' AS ' || a.varname                                                       AS catvar
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.c_studyvariablecondition_id
                                 , a.e_studyvariable_id
                                 , a.h_dataset_id
                            FROM othvars_query                                    a
                              INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                            WHERE a.d_conditionoperator_id IN (
                                                                SELECT z.conditionoperator_id
                                                                FROM dashin.conditionoperator z
                                                                WHERE z.context_flag = 3
                                                              )
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                                   , a.c_studyvariablecondition_id
                                   , a.e_studyvariable_id
                                   , a.h_dataset_id
                                   , a.g_local_id
                                   , a.d_conditionoperator_id
                                   , b.studyvariablecondition_id
                                   , a.varname
                                   , a.varvalue
                          )
    -- non-design-vars: numeric conditions
    , othvars_numeric  AS (
                            SELECT E'text_to_numeric(' || a.varvalue || ') AS ' || a.varname AS numvar
                                 , CASE
                                     WHEN a.d_conditionoperator_id IN (1, 2, 3, 4, 5, 6) THEN a.varname || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id) || a.d_txtval1
                                     WHEN a.d_conditionoperator_id IN (7, 8, 9)          THEN
                                                   coalesce(text_to_numeric(a.d_txtval1) , 0) || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id , 'l') || a.varname || ' AND ' ||
                                                   a.varname || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id , 'r') || coalesce(text_to_numeric(a.d_txtval2) , 0)
                                   END                                                       AS cond_string
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.h_dataset_id
                                 , a.e_studyvariable_id
                            FROM othvars_query a
                            WHERE a.d_conditionoperator_id IN (
                                                                SELECT z.conditionoperator_id
                                                                FROM dashin.conditionoperator z
                                                                WHERE z.context_flag = 2
                                                              )
                          )
    -- non-design-vars: condition-queries for each dataset
    , othvars_dataset  AS (
                            SELECT E'(\nSELECT x.studyrow_id \nFROM (\n SELECT b.studyrow_id\n,' ||
                                   string_agg(x0.selectvar , E'\n ,') ||
                                   E'\nFROM dashin.datatable a\n' ||
                                   E'INNER JOIN dashin.studyrow_x_datarow b ON a.datarow_id = b.datarow_id\n' ||
                                   E'WHERE a.dataset_id = ' || quote_literal(x0.h_dataset_id) ||
                                   E'\n) x\n WHERE ' || string_agg('(' || x0.cond_string || ')' , E' AND \n') || E'\n)' AS dataset_query
                                 , x0.a_study_id
                                 , x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , x0.e_studyvariable_id
                                 , x0.h_dataset_id
                            FROM (
                                   SELECT a.catvar AS selectvar
                                        , a.cond_string
                                        , a.a_study_id
                                        , a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , a.e_studyvariable_id
                                        , a.h_dataset_id
                                   FROM othvars_category a
                                   UNION
                                   SELECT b.numvar AS selectvar
                                        , b.cond_string
                                        , b.a_study_id
                                        , b.a_studyrowset_id
                                        , b.b_studyrowsubset_id
                                        , b.e_studyvariable_id
                                        , b.h_dataset_id
                                   FROM othvars_numeric b
                                 ) x0
                            GROUP BY x0.a_study_id
                                   , x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                                   , x0.e_studyvariable_id
                                   , x0.h_dataset_id
                          )
    -- non-design-vars: map dataset-rows to study-rows
    , othvars_studyvar AS (
                            SELECT E'SELECT stv.studyrow_id\n FROM (\n' || string_agg(a.dataset_query , E'\nUNION\n') || E'\n) stv' AS stvar_query
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.e_studyvariable_id
                            FROM othvars_dataset a
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                                   , a.e_studyvariable_id
                          )
    -- non-design-vars: combine conditions within each subset (INTERSECT ALL)
    , othvars_subset   AS (
                            SELECT E'(\n' || string_agg(a.stvar_query , E'\nINTERSECT ALL\n') || E'\n)' AS ovars_single_rowsubset_query
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                            FROM othvars_studyvar a
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                          )
    -- combined all conditinos within each subset (INTERSECT ALL)
    , allvars_subset   AS (
                            SELECT a.b_studyrowsubset_id
                                 , a.dvars_single_rowsubset_query
                                 , b.ovars_single_rowsubset_query
                                 , CASE
                                     WHEN a.dvars_single_rowsubset_query IS NULL OR b.ovars_single_rowsubset_query IS NULL THEN
                                       CASE
                                         WHEN a.dvars_single_rowsubset_query IS NOT NULL THEN a.dvars_single_rowsubset_query
                                         ELSE b.ovars_single_rowsubset_query
                                       END
                                     ELSE a.dvars_single_rowsubset_query || E'\n INTERSECT ALL \n' || b.ovars_single_rowsubset_query
                                   END AS dovars_subset_query
      
                            FROM dvars_subset          a
                              FULL JOIN othvars_subset b
                                          ON a.b_studyrowsubset_id = b.b_studyrowsubset_id
                          )
    -- combine subsets into one query (UNION)
    , allvars_rowset   AS (
                            SELECT E'(\n' || string_agg(dovars_subset_query , E'\n)\n UNION \n(\n') || E'\n);\n' AS search_query
                            FROM allvars_subset
                          )
  SELECT search_query
  INTO result_query
  FROM allvars_rowset;
  IF result_query IS NULL THEN
    RETURN QUERY SELECT NULL::UUID;
  ELSE
    RETURN QUERY EXECUTE result_query;
  END IF;
  
  RETURN;

END;
$$;
CREATE FUNCTION dashin.studyvariable_data_generate_query
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT NULL
) RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  limit_val  TEXT DEFAULT '';
  return_val TEXT;
BEGIN
  
  IF _limit >= 0 THEN
    limit_val := E'LIMIT ' || _limit;
  END IF;
  
  return_val := (
                  SELECT E'(\n' || string_agg(query_inner , E'\n)\nUNION ALL\n(\n') || E'\n)\n' || limit_val
                  FROM (
                         SELECT 'SELECT datarow_id,(' || x0.datatype_id || ')::INT, jsonb_array_element_text(datarow,' || x0.local_id || E') AS ' || quote_ident(x0.name) || E'\n' ||
                                E'FROM dashin.datatable WHERE dataset_id = ' || quote_literal(x0.dataset_id) AS query_inner
                         FROM (
                                SELECT c.local_id
                                     , b.name
                                     , b.datatype_id
                                     , a.dataset_id
                                FROM dashin.studyvariable_x_variable a
                                  LEFT JOIN  dashin.studyvariable    b ON a.studyvariable_id = b.studyvariable_id
                                  LEFT JOIN  dashin.variable         c ON a.variable_id = c.variable_id
                                  INNER JOIN dashin.study            d ON a.study_id = d.study_id
                                WHERE a.studyvariable_id = _studyvariable_id
                                  AND (d.study_id, d.owner_org) = (_study_id, _owner_org)
                              ) x0
                       ) x1
  );
  RETURN return_val;
END ;
$$;

CREATE FUNCTION dashin.studyvariable_data
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID   UUID,
            DATATYPE_ID  INT,
            VALUE_COLUMN TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  value_query TEXT;
BEGIN
  value_query := dashin.studyvariable_data_generate_query(_studyvariable_id , _study_id , _owner_org , _limit);
  IF value_query IS NOT NULL THEN
    RETURN QUERY EXECUTE value_query;
    RETURN;
  END IF;
  RETURN QUERY SELECT NULL::UUID, NULL::INT, NULL::TEXT;
END;
$$;

COMMENT ON FUNCTION dashin.studyvariable_data(_studyvariable_id UUID, _study_id UUID, _owner_org UUID, _limit INT) IS E'Extract data from dataset. Do not take designvariable information into account.\n'
  'Typically used in pagination queries where _limit is the pagination limit\n'
  '  @PARAM _studyvariable_id UUID\n'
  '  @PARAM _study_id UUID\n'
  '  @PARAM _owner_org UUID, organization_id for the owning organization\n'
  '  @PARAM _limit INT, number of rows to return\n'
  '  @RETURN table(datarow_id UUID, datatype_id INT, value_column TEXT';
CREATE FUNCTION dashin.studyvariable_data_designvariable
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
)
  RETURNS TABLE
          (
            DATAROW_ID   UUID,
            ELEMENT_ID   UUID,
            VALUE_COLUMN TEXT,
            LABEL_COLUMN TEXT,
            DATATYPE_ID  INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  _dataquery        TEXT;
  _variabletype_id INT;
BEGIN
  _variabletype_id := (
                         SELECT
                           variabletype_id
                         FROM dashin.studyvariable
                         WHERE studyvariable_id = _studyvariable_id
                       );
  IF _variabletype_id = 2 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.subject_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_subject      b ON b.subject_id = a.subject_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 3 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.startgroup_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_startgroup   b ON b.startgroup_id = a.startgroup_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 4 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.event_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_event        b ON b.event_id = a.event_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 5 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.subevent_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_subevent     b ON b.subevent_id = a.subevent_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 6 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.samplingevent_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable               a
        LEFT JOIN dashin.study_samplingevent b ON b.samplingevent_id = a.samplingevent_id
        LEFT JOIN dashin.studyrow_x_datarow  c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable       d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 7 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.samplingtime_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_samplingtime b ON b.samplingtime_id = a.samplingtime_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 8 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.center_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_center       b ON b.center_id = a.center_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id IN (0, 1) AND _studyvariable_id IS NOT NULL THEN
    _dataquery := (
                    SELECT
                          E'SELECT\n datarow_id\n ,NULL::UUID\n,value_column\n,NULL::TEXT\n,datatype_id\n FROM (\n' ||
                          b.dataquery ||
                          E'\n) x (datarow_id, datatype_id, value_column)'
                    FROM dashin.studyvariable_data_generate_query(_studyvariable_id , _study_id , _owner_org) b (dataquery)
                  );
    RETURN QUERY EXECUTE _dataquery;
  END IF;
  RETURN;
END;
$$;
CREATE FUNCTION dashin.studyvariable_guess_datatype
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT 10
) RETURNS INT
  LANGUAGE plpgsql
AS
$$
DECLARE
  -- --------------------------------------
  evaluation_result         BOOL DEFAULT FALSE;
  -- --------------------------------------
  datatype_text    CONSTANT INT :=       1;
  datatype_numeric CONSTANT INT :=       2;
  -- --------------------------------------
BEGIN
  -- check numeric
  evaluation_result := (
                         SELECT TRUE = ALL (array_agg(utils.text_is_numeric(value_column)))
                         FROM dashin.studyvariable_data(_studyvariable_id , _study_id , _owner_org , _limit)
                       );
  IF evaluation_result THEN
    RETURN datatype_numeric;
  ELSE
    RETURN datatype_text;
  END IF;
END;
$$;
CREATE FUNCTION dashin.datadesign_design_variable_distinct
(
    _dataset_id      UUID
  , _variabletype_id INT
  , _owner_org       UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID      UUID,
            LOCAL_ID        INT,
            NAME            TEXT,
            VARIABLE_ID     UUID,
            VARIABLETYPE    TEXT,
            VARIABLETYPE_ID INT
          )
  LANGUAGE sql
AS
$$
SELECT DISTINCT dataset_id
              , local_id
              , jsonb_array_element_text(d.datarow , local_id) AS name
              , variable_id
              , t.name                                         AS variabletype
              , v.variabletype_id::INT
FROM dashin.variable            v
  LEFT JOIN dashin.datatable    d USING (dataset_id)
  LEFT JOIN dashin.variabletype t USING (variabletype_id)
WHERE variable_id = (
                      SELECT variable_id
                      FROM
                        dashin.datadesign_design_variablenames(_dataset_id , _owner_org)
                      WHERE variabletype_id = _variabletype_id
                    )
ORDER BY name
$$
;


ALTER FUNCTION dashin.datadesign_design_variable_distinct(_dataset_id UUID, _variabletype_id INT, _owner_org UUID) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_design_variable_distinct
(
    _dataset_id        UUID
  , _variabletype_name TEXT
  , _owner_org         UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID      UUID,
            LOCAL_ID        INT,
            NAME            TEXT,
            VARIABLE_ID     UUID,
            VARIABLETYPE    TEXT,
            VARIABLETYPE_ID INT
          )
  LANGUAGE sql
AS
$$
SELECT DISTINCT dataset_id
              , local_id
              , jsonb_array_element_text(d.datarow , local_id) AS name
              , variable_id
              , t.name                                         AS variabletype
              , v.variabletype_id::INT
FROM dashin.variable            v
  LEFT JOIN dashin.datatable    d USING (dataset_id)
  LEFT JOIN dashin.variabletype t USING (variabletype_id)
WHERE variable_id IN (
                       SELECT variable_id
                       FROM
                         dashin.datadesign_design_variablenames(_dataset_id , _owner_org)
                       WHERE variabletype_id = (
                         CASE _variabletype_name
                           WHEN 'subject'       THEN 2
                           WHEN 'startgroup'    THEN 3
                           WHEN 'event'         THEN 4
                           WHEN 'subevent'      THEN 5
                           WHEN 'samplingevent' THEN 6
                           WHEN 'sampling'      THEN 6
                           WHEN 'time'          THEN 7
                           WHEN 'samplingtime'  THEN 7
                           WHEN 'center'        THEN 8
                         END)
                     )
ORDER BY name
$$
;

ALTER FUNCTION dashin.datadesign_design_variable_distinct(_dataset_id UUID, _variabletype_name TEXT, _owner_org UUID) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_delete_design
(
    _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            MESSAGE TEXT,
            STATUS  TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  status TEXT DEFAULT 'none';
BEGIN
  DELETE
  FROM dashin.datadesign
  WHERE dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  IF found THEN
    status := 'deleted';
  END IF;
  RETURN QUERY
    SELECT t.element_name
         , t.affected_rows
    FROM (
           VALUES ('status', status)
         ) t(element_name, affected_rows);
END ;
$$;

ALTER FUNCTION dashin.datadesign_delete_design(_dataset_id UUID, _owner_org UUID) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_design_variabletypes_defined_jsonb
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
  , _char       TEXT DEFAULT 'x'
  , _null_char  TEXT DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID    UUID,
            VARIABLETYPES JSONB
          )
  LANGUAGE sql
AS
$$
SELECT _dataset_id::UUID                        AS dataset_id
     , jsonb_object_agg(type_code , is_defined) AS design_variables
FROM (
       SELECT CASE
                WHEN variable_id IS NOT NULL THEN _char
                ELSE _null_char
              END AS is_defined
            , *
       FROM (
              SELECT variabletype_id
                   , CASE variabletype_id
                       WHEN 8 THEN 1
                       WHEN 4 THEN 2
                       WHEN 5 THEN 3
                       WHEN 6 THEN 4
                       WHEN 7 THEN 5
                       WHEN 3 THEN 6
                       WHEN 2 THEN 7
                     END AS sort_order
                   , CASE variabletype_id
                       WHEN 4 THEN 'ev'
                       WHEN 3 THEN 'sg'
                       WHEN 2 THEN 'sb'
                       WHEN 5 THEN 'se'
                       WHEN 6 THEN 'sm'
                       WHEN 7 THEN 'st'
                       WHEN 8 THEN 'cn'
                       WHEN 7 THEN 'st'
                     END AS type_code
                   , name
                   , description
              FROM dashin.variabletype
              WHERE variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
              ORDER BY variabletype_id
            ) t
         LEFT JOIN
       (
         SELECT variable_id
              , variabletype_id
              , v.name AS variablename
         FROM dashin.variable       v
           LEFT JOIN dashin.dataset d USING (dataset_id)
         WHERE v.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
           AND v.dataset_id = _dataset_id
           AND (d.owner_org = _owner_org OR _owner_org IS NULL)
       )      v USING (variabletype_id)
     ) x0;
$$
;


ALTER FUNCTION dashin.datadesign_design_variabletypes_defined_jsonb(_dataset_id UUID, _owner_org UUID, _char TEXT, _null_char TEXT) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_design_variabletypes_defined_text
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID    UUID,
            VARIABLETYPES TEXT
          )
  LANGUAGE sql
AS
$$
SELECT _dataset_id::UUID                                 AS dataset_id
     , string_agg(vartype_code , '' ORDER BY sort_order) AS design_variables
FROM (
       SELECT CASE
                WHEN variable_id IS NOT NULL THEN '[' || type_code || ']'
                ELSE '[--]'
              END AS vartype_code
            , sort_order
       FROM (
              SELECT variabletype_id
                   , CASE variabletype_id
                       WHEN 8 THEN 1
                       WHEN 4 THEN 2
                       WHEN 5 THEN 3
                       WHEN 6 THEN 4
                       WHEN 7 THEN 5
                       WHEN 3 THEN 6
                       WHEN 2 THEN 7
                     END AS sort_order
                   , CASE variabletype_id
                       WHEN 4 THEN 'ev'
                       WHEN 3 THEN 'sg'
                       WHEN 2 THEN 'sb'
                       WHEN 5 THEN 'se'
                       WHEN 6 THEN 'sm'
                       WHEN 7 THEN 'st'
                       WHEN 8 THEN 'cn'
                       WHEN 7 THEN 'st'
                     END AS type_code
                   , name
                   , description
              FROM dashin.variabletype
              WHERE variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
              ORDER BY variabletype_id
            ) t
         LEFT JOIN
       (
         SELECT variable_id
              , variabletype_id
              , v.name AS variablename
         FROM dashin.variable       v
           LEFT JOIN dashin.dataset d USING (dataset_id)
         WHERE v.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
           AND v.dataset_id = _dataset_id
           AND (d.owner_org = _owner_org OR _owner_org IS NULL)
       )      v USING (variabletype_id)
     ) x0;
$$
;


ALTER FUNCTION dashin.datadesign_design_variabletypes_defined_text(_dataset_id UUID, _owner_org UUID) OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_exists
(
    _dataset_id UUID
  , _owner_org  UUID
) RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT exists(
  SELECT
  FROM dashin.datadesign_datatable dt0
  WHERE dt0.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org))
  );
$$;
CREATE FUNCTION dashin.datadesign_save_design
(
    _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            NAME          TEXT,
            AFFECTED_ROWS INT,
            DESCRIPTION   TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  overall_n          INT;
  distinct_n         INT;
  missing_status_all INT;
  design_exists      BOOL DEFAULT FALSE;
  return_early       BOOL DEFAULT FALSE;
BEGIN
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK IF DESIGN ALREADY SAVED
  -- ---------------------------------------------------------------------------------------------------------------------
  design_exists := exists(SELECT FROM dashin.datadesign_datatable dt0 WHERE dt0.dataset_id = _dataset_id AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org)));
  IF design_exists THEN
    RETURN QUERY
      VALUES ('exists', 0, 'Datadesign already saved');
    RETURN;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- DISTINCT DESIGN VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE design_distinct
  (
    datarow_id           UUID,
    dataset_id           UUID,
    
    event_name           TEXT,
    subevent_name        TEXT,
    samplingevent_name   TEXT,
    samplingtime_name    TEXT,
    center_name          TEXT,
    startgroup_name      TEXT,
    subject_name         TEXT,
    
    event_status         INT DEFAULT 1,
    subevent_status      INT DEFAULT 1,
    samplingevent_status INT DEFAULT 1,
    samplingtime_status  INT DEFAULT 1,
    center_status        INT DEFAULT 1,
    startgroup_status    INT DEFAULT 1,
    subject_status       INT DEFAULT 1
  )
    ON COMMIT DROP;
  INSERT INTO design_distinct (event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT DISTINCT jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org));
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK DUPLICATES
  -- ---------------------------------------------------------------------------------------------------------------------
  
  -- @formatter:off
  -- -------------------------------
  -- overall count
  -- -------------------------------
  overall_n := (SELECT count(*) FROM dashin.datatable dt WHERE dt.dataset_id = _dataset_id);
  
  -- -------------------------------
  -- distinct count
  -- -------------------------------
  distinct_n := (SELECT count(*) FROM design_distinct);
  -- @formatter:on
  IF overall_n = 0 OR distinct_n = 0 THEN
    RETURN QUERY VALUES ('empty', 0, 'Design was not saved because dataset or datadesign is empty');
    RETURN;
  END IF;
  
  IF overall_n != distinct_n THEN
    RETURN QUERY VALUES ('duplicate', 0, 'Design was not saved because the combination of design variables leads to duplicate entries');
    return_early := TRUE;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK MISSING VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE check_missing
  (
    name       TEXT,
    missing    INT,
    status     INT,
    statustext TEXT
  )
    ON COMMIT DROP;
  -- -------------------------------------------
  -- status:
  --  0: mixed (missing and non-missing)
  --  1: all missing
  --  2: all non-missing
  -- -------------------------------------------
  INSERT INTO check_missing
  SELECT c.name
       , c.missing
       , CASE
           WHEN c.missing = 0 THEN 2
           ELSE ((distinct_n - c.missing) = 0)::INT
         END AS status
       , CASE
           WHEN c.missing = 0                  THEN 'All non-missing'
           WHEN ((distinct_n - c.missing) = 0) THEN 'All missing'
           ELSE 'Mixed missing and non-missing'
         END AS description
       -- @formatter:off
  FROM (
         (SELECT 'event'         AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.event_name         IS NULL OR d.event_name         = '')) UNION ALL
         (SELECT 'subevent'      AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subevent_name      IS NULL OR d.subevent_name      = '')) UNION ALL
         (SELECT 'samplingevent' AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingevent_name IS NULL OR d.samplingevent_name = '')) UNION ALL
         (SELECT 'samplingtime'  AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingtime_name  IS NULL OR d.samplingtime_name  = '')) UNION ALL
         (SELECT 'center'        AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.center_name        IS NULL OR d.center_name        = '')) UNION ALL
         (SELECT 'startgroup'    AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.startgroup_name    IS NULL OR d.startgroup_name    = '')) UNION ALL
         (SELECT 'subject'       AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subject_name       IS NULL OR d.subject_name       = ''))
       )c;
  IF (SELECT c.status != 2 FROM check_missing c WHERE c.name = 'subject') THEN
    RETURN QUERY VALUES
                 ('subject_missing',0,'Design was not saved because subject variable contains missing values');
    return_early := TRUE;
  END IF;
  
  missing_status_all := (SELECT min(c.status) FROM check_missing c);
  IF missing_status_all = 0 THEN
    RETURN QUERY VALUES
                 ('mixed',0,'Design was not saved because some design variables have mixed missing and non-missing values');
    return_early := TRUE;
  END IF;
  
  IF return_early THEN
    RETURN;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- UPDATE FULL DESIGN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- insert data again (including datarow_id, datasetid now that we know it's unique)
  TRUNCATE design_distinct;
  INSERT INTO design_distinct (datarow_id, dataset_id, event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT
    dt.datarow_id
    ,  dt.dataset_id
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM
    dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- AUTOGENERATE DUMMIES FOR MISSING DESIGN VARIABLES
  -- ---------------------------------------------------------------------------------------------------------------------
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET (event_name, event_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET (subevent_name, subevent_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET (samplingevent_name, samplingevent_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET (samplingtime_name, samplingtime_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET (center_name, center_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET (startgroup_name, startgroup_status) = ('1', 100);
  END IF;
  
  -- @formatter:on
  
  INSERT INTO dashin.datadesign (dataset_id)
  SELECT _dataset_id
  ON CONFLICT DO NOTHING;
  
  -- -------------------------------------------------------------------------------------------------
  -- prepare individual elements
  -- -------------------------------------------------------------------------------------------------
  
  -- -------------------------------------------------------------
  -- event
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE event_distinct
  (
    event_id     UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO event_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.event_name   AS name
                , d.event_status AS status
                , 'event'
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- subevent
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE subevent_distinct
  (
    subevent_id  UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO subevent_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.subevent_name   AS name
                , d.subevent_status AS status
                , 'subevent'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- samplingevent
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE samplingevent_distinct
  (
    samplingevent_id UUID DEFAULT uuid_generate_v4(),
    dataset_id       UUID,
    name             TEXT,
    status           INT,
    element_name     TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO samplingevent_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.samplingevent_name   AS name
                , d.samplingevent_status AS status
                , 'samplingevent'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- samplingtime
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE samplingtime_distinct
  (
    samplingtime_id  UUID DEFAULT uuid_generate_v4(),
    dataset_id       UUID,
    samplingevent_id UUID,
    name             TEXT,
    status           INT,
    element_name     TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO samplingtime_distinct (dataset_id, samplingevent_id, name, status, element_name)
  SELECT d.dataset_id
       , sp.samplingevent_id
       , d.name
       , d.status
       , 'samplingtime' AS element_name
  FROM (
         SELECT DISTINCT d0.dataset_id
                       , d0.samplingtime_name   AS name
                       , d0.samplingtime_status AS status
                       , d0.samplingevent_name
         FROM design_distinct d0
       )                             d
    LEFT JOIN samplingevent_distinct sp ON d.samplingevent_name = sp.name;
  
  -- -------------------------------------------------------------
  -- center
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE center_distinct
  (
    center_id    UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO center_distinct(dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.center_name   AS name
                , d.center_status AS status
                , 'center'        AS element_name
  FROM design_distinct d;
  -- -------------------------------------------------------------
  -- startgroup
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE startgroup_distinct
  (
    startgroup_id UUID DEFAULT uuid_generate_v4(),
    dataset_id    UUID,
    name          TEXT,
    status        INT,
    element_name  TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO startgroup_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.startgroup_name   AS name
                , d.startgroup_status AS status
                , 'startgroup'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- subject
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE subject_distinct
  (
    subject_id    UUID DEFAULT uuid_generate_v4(),
    dataset_id    UUID,
    center_id     UUID,
    startgroup_id UUID,
    name          TEXT,
    status        INT,
    element_name  TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO subject_distinct (dataset_id, center_id, startgroup_id, name, status, element_name)
  SELECT d.dataset_id
       , e.center_id
       , g.startgroup_id
       , d.name
       , d.status
       , 'subject' AS element_name
  FROM (
         SELECT DISTINCT d0.dataset_id
                       , d0.center_name
                       , d0.subject_name   AS name
                       , d0.subject_status AS status
                       , d0.startgroup_name
         FROM design_distinct d0
       )                          d
    LEFT JOIN startgroup_distinct g ON d.startgroup_name = g.name
    LEFT JOIN center_distinct     e ON d.center_name = e.name;
  
  -- ----------------------------------------------------------------------------------
  -- insert into datadesign_x_studydesign link tables
  -- ----------------------------------------------------------------------------------
  -- @formatter:off
  INSERT INTO dashin.datadesign_x_study_event (dataset_id, datadesign_event_id)
  SELECT dataset_id,event_id FROM event_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_subevent (dataset_id, datadesign_subevent_id)
  SELECT dataset_id,subevent_id FROM subevent_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_samplingevent (dataset_id, datadesign_samplingevent_id)
  SELECT dataset_id,samplingevent_id FROM samplingevent_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_samplingtime (dataset_id, datadesign_samplingtime_id)
  SELECT dataset_id,samplingtime_id FROM samplingtime_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_center (dataset_id, datadesign_center_id)
  SELECT dataset_id,center_id FROM center_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_startgroup (dataset_id, datadesign_startgroup_id)
  SELECT dataset_id,startgroup_id FROM startgroup_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_subject (dataset_id, datadesign_subject_id)
  SELECT dataset_id,subject_id FROM subject_distinct;
  -- @formatter:on

  -- ----------------------------------------------------------------------------------
  -- save datadesign
  -- ----------------------------------------------------------------------------------
  INSERT INTO dashin.datadesign_datatable ( datarow_id, dataset_id
                                          , event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id
                                          , event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name
                                          , event_status, subevent_status, samplingevent_status, samplingtime_status, center_status, startgroup_status, subject_status)
  SELECT dd.datarow_id
       , dd.dataset_id
       
       , ev.event_id
       , se.subevent_id
       , sm.samplingevent_id
       , st.samplingtime_id
       , cd.center_id
       , sg.startgroup_id
       , sb.subject_id
       
       , dd.event_name
       , dd.subevent_name
       , dd.samplingevent_name
       , dd.samplingtime_name
       , dd.center_name
       , dd.startgroup_name
       , dd.subject_name
       
       , dd.event_status
       , dd.subevent_status
       , dd.samplingevent_status
       , dd.samplingtime_status
       , dd.center_status
       , dd.startgroup_status
       , dd.subject_status
  FROM design_distinct               dd
    LEFT JOIN event_distinct         ev ON dd.event_name = ev.name
    LEFT JOIN subevent_distinct      se ON dd.subevent_name = se.name
    LEFT JOIN samplingevent_distinct sm ON dd.samplingevent_name = sm.name
    LEFT JOIN samplingtime_distinct  st ON (dd.samplingtime_name, sm.samplingevent_id) = (st.name, st.samplingevent_id)
    LEFT JOIN center_distinct        cd ON dd.center_name = cd.name
    LEFT JOIN startgroup_distinct    sg ON dd.startgroup_name = sg.name
    LEFT JOIN subject_distinct       sb ON dd.subject_name = sb.name
  ON CONFLICT
    DO NOTHING;
  
  RETURN QUERY
    VALUES ('saved', overall_n, 'Design was saved');
  RETURN;
END;
$$;
CREATE FUNCTION dashin.datadesign_startgroup_event_sequence_text
(
    _dataset_id         UUID
  , _owner_org          UUID DEFAULT NULL
  , _group_name_prefix  TEXT DEFAULT 'group '
  , _group_name_postfix TEXT DEFAULT ' (estimated)'
)
  RETURNS TABLE
          (
            DATASET_ID     UUID,
            STARTGROUP     TEXT,
            EVENT_SEQUENCE TEXT,
            SUBJECTS       TEXT
          )
  LANGUAGE sql
AS
$$
SELECT dataset_id
     , coalesce(startgroup , _group_name_prefix || rn || _group_name_postfix) AS startgroup
     , group_sequence
     , group_members
FROM (
       SELECT dataset_id
            , startgroup
            , group_sequence
            , string_agg(subject , ', ') AS group_members
            , row_number() OVER ()       AS rn
       FROM (
              SELECT dataset_id
                   , startgroup
                   , subject
                   , string_agg(group_event , ', ' ORDER BY event,subevent) AS group_sequence
              FROM (
                     SELECT -- DISTINCT 
                       dataset_id
                          , '[' || event || ', ' || subevent || ']' AS group_event
                          , subject
                          , startgroup
                          , event
                          , subevent
                     FROM (
                            SELECT dataset_id::UUID
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))      AS event
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))   AS subevent
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup')) AS startgroup
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))    AS subject
                            FROM dashin.datatable      dt
                              LEFT JOIN dashin.dataset d USING (dataset_id)
                            WHERE dt.dataset_id = _dataset_id
                              AND (d.owner_org = _owner_org OR _owner_org IS NULL)
                            GROUP BY dataset_id
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))
                          ) x0
                   ) x1
              GROUP BY dataset_id
                     , startgroup
                     , subject
            ) x2
       GROUP BY dataset_id
              , startgroup
              , group_sequence
     ) x3;
$$;
-- -------------------------------------------------------------------------

ALTER FUNCTION dashin.datadesign_startgroup_event_sequence_text OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_subject_event_sequence_text
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID     UUID,
            SUBJECT        TEXT,
            STARTGROUP     TEXT,
            EVENT_SEQUENCE TEXT
          )
  LANGUAGE sql
AS
$$
SELECT dataset_id
     , subject
     , startgroup
     , string_agg(group_event , ', ' ORDER BY event,subevent) AS event_sequence
FROM (
       SELECT dataset_id
            , '[' || event || ', ' || subevent || ']' AS group_event
            , subject
            , startgroup
            , event
            , subevent
       FROM (
              SELECT dataset_id::UUID
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))      AS event
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))   AS subevent
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup')) AS startgroup
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))    AS subject
              FROM dashin.datatable      dt
                LEFT JOIN dashin.dataset d USING (dataset_id)
              WHERE dt.dataset_id = _dataset_id
                AND (d.owner_org = _owner_org OR _owner_org IS NULL)
              GROUP BY dataset_id
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))
            ) x0
     ) x1
GROUP BY dataset_id
       , startgroup
       , subject
$$;

ALTER FUNCTION dashin.datadesign_subject_event_sequence_text OWNER TO :application_user;

CREATE FUNCTION dashin.datadesign_summary
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
  , _sortorder  TEXT[] DEFAULT ARRAY ['event','subevent','samplingevent','samplingtime','center','startgroup','subject']
)
  RETURNS TABLE
          (
            SUMMARY_TYPE       TEXT,
            N                  INT,
            VARIABLETYPE_ID    INT,
            VARIABLETYPE       TEXT,
            VARIABLETYPE_LABEL TEXT,
            VARIABLENAME       TEXT,
            AUTOGENERATED      TEXT,
            SORTORDER          INT,
            STATUS             INT,
            STATUS2            INT,
            VALUENAME          TEXT,
            VALUENAME2         TEXT
          )
  LANGUAGE sql
AS
$$
WITH
  design_table         AS (
                            SELECT a.datarow_id
                                 , a.dataset_id
                                 , a.event_id
                                 , a.subevent_id
                                 , a.samplingevent_id
                                 , a.samplingtime_id
                                 , a.center_id
                                 , a.startgroup_id
                                 , a.subject_id
                                 , a.event_name
                                 , a.subevent_name
                                 , a.samplingevent_name
                                 , a.samplingtime_name
                                 , a.center_name
                                 , a.startgroup_name
                                 , a.subject_name
                                 , a.event_status
                                 , a.subevent_status
                                 , a.samplingevent_status
                                 , a.samplingtime_status
                                 , a.center_status
                                 , a.startgroup_status
                                 , a.subject_status
                            FROM dashin.datadesign_unvalidated_or_datatable(_dataset_id , _owner_org) a
                          )
  , variablename_qry   AS (
                            SELECT variabletype_id
                                 , variabletype
                                 , variablename
                            FROM
                              dashin.datadesign_design_variablenames(_dataset_id , _owner_org)
                          )
  , count_overall      AS (
                            SELECT 'count_distinct' AS summary_type
                                 , count(*)         AS n
                            FROM (
                                   SELECT DISTINCT ON (event_id,subevent_id,samplingevent_id,samplingtime_id,center_id,startgroup_id,subject_id) 1
                                   FROM design_table
                                 ) x0
                            UNION ALL
                            SELECT 'count_full' AS summary_type
                                 , count(*)     AS n
                            FROM dashin.datatable
                            WHERE dataset_id = (
                                                 SELECT d.dataset_id
                                                 FROM design_table d
                                                 LIMIT 1
                                               )
                            UNION ALL
                            SELECT 'design_exists'
                                 , dashin.datadesign_exists(_dataset_id , _owner_org)::INT AS n
                          )
  , variable_summaries AS (
                            SELECT summary_type
                                 , valuename
                                 , valuename2
                                 , status
                                 , status2
                                 , n
                            FROM (
                                   (
                                     SELECT 'event'      AS summary_type
                                          , event_name   AS valuename
                                          , NULL::TEXT   AS valuename2
                                          , event_status AS status
                                          , NULL::INT    AS status2
                                          , count(*)     AS n
                                     FROM design_table
                                     GROUP BY event_name
                                            , event_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'subevent'      AS summary_type
                                          , subevent_name   AS valuename
                                          , NULL::TEXT      AS valuename2
                                          , subevent_status AS status
                                          , NULL::INT       AS status2
                                          , count(*)        AS n
                                     FROM design_table
                                     GROUP BY subevent_name
                                            , subevent_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'samplingevent'      AS summary_type
                                          , samplingevent_name   AS valuename
                                          , NULL::TEXT           AS valuename2
                                          , samplingevent_status AS status
                                          , NULL::INT            AS status2
                                          , count(*)             AS n
                                     FROM design_table
                                     GROUP BY samplingevent_name
                                            , samplingevent_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'samplingtime'       AS summary_type
                                          , samplingevent_name   AS valuename
                                          , samplingtime_name    AS valuename2
                                          , samplingtime_status  AS status
                                          , samplingevent_status AS status2
                                          , count(*)             AS n
                                     FROM design_table
                                     GROUP BY samplingevent_name
                                            , samplingtime_name
                                            , samplingevent_status
                                            , samplingtime_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'center'      AS summary_type
                                          , center_name   AS valuename
                                          , NULL::TEXT    AS valuename2
                                          , center_status AS status
                                          , NULL::INT     AS status2
                                          , count(*)      AS n
                                     FROM (
                                            SELECT DISTINCT center_name
                                                          , center_status
                                                          , subject_name
                                            FROM design_table
                                          ) x
                                     GROUP BY center_name
                                            , center_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'startgroup'      AS summary_type
                                          , startgroup_name   AS valuename
                                          , NULL::TEXT        AS valuename2
                                          , startgroup_status AS status
                                          , NULL::INT         AS status2
                                          , count(*)          AS n
                                     FROM (
                                            SELECT DISTINCT startgroup_name
                                                          , startgroup_status
                                                          , subject_name
                                            FROM design_table
                                          ) x
                                     GROUP BY startgroup_name
                                            , startgroup_status
                                   )
                                   UNION ALL
                                   (
                                     SELECT 'subject'         AS summary_type
                                          , subject_name      AS valuename
                                          , startgroup_name   AS valuename2
                                          , subject_status    AS status
                                          , startgroup_status AS status2
                                          , count(*)          AS n
                                     FROM design_table
                                     GROUP BY startgroup_name
                                            , subject_name
                                            , startgroup_status
                                            , subject_status
                                   )
                                 ) x0
                          )
  , count_summary      AS (
                            SELECT 'count_summary' AS summary_type
                                 , summary_type    AS variabletype
                                 , variabletype_id
                                 , vn.variabletype AS variabletype_label
                                 , variablename
                                 , status
                                 , CASE
                                     WHEN status = 100 THEN 'Autogenerated'
                                     WHEN status = 0   THEN 'Invalid'
                                   END             AS autogenerated
                                 , sortorder
                                 , n
                            FROM (
                                   SELECT *
                                        , CASE
                                            WHEN summary_type = 'event'         THEN 4
                                            WHEN summary_type = 'subevent'      THEN 5
                                            WHEN summary_type = 'samplingevent' THEN 6
                                            WHEN summary_type = 'samplingtime'  THEN 7
                                            WHEN summary_type = 'center'        THEN 8
                                            WHEN summary_type = 'startgroup'    THEN 3
                                            WHEN summary_type = 'subject'       THEN 2
                                          END AS variabletype_id
                                        , CASE
                                            WHEN summary_type = 'event'         THEN array_position(_sortorder , 'event')
                                            WHEN summary_type = 'subevent'      THEN array_position(_sortorder , 'subevent')
                                            WHEN summary_type = 'samplingevent' THEN array_position(_sortorder , 'samplingevent')
                                            WHEN summary_type = 'samplingtime'  THEN array_position(_sortorder , 'samplingtime')
                                            WHEN summary_type = 'center'        THEN array_position(_sortorder , 'center')
                                            WHEN summary_type = 'startgroup'    THEN array_position(_sortorder , 'startgroup')
                                            WHEN summary_type = 'subject'       THEN array_position(_sortorder , 'subject')
                                          END AS sortorder
                                   FROM (
                                          SELECT summary_type
                                               , status
                                               , count(*) AS n
                                          FROM variable_summaries
                                          GROUP BY summary_type
                                                 , status
                                        ) x0
                                 )                       vs
                              LEFT JOIN variablename_qry vn USING (variabletype_id)
                          )
SELECT summary_type
     , n::INT
     , variabletype_id
     , variabletype
     , variabletype_label
     , variablename
     , autogenerated
     , sortorder
     , status
     , status2
     , valuename
     , valuename2
FROM (
       (
         SELECT summary_type
              , n
              , variabletype_id
              , variabletype
              , variabletype_label
              , variablename
              , autogenerated
              , sortorder
              , status
              , NULL::INT  AS status2
              , NULL::TEXT AS valuename
              , NULL::TEXT AS valuename2
         FROM count_summary
       )
       UNION ALL
       (
         SELECT summary_type
              , n
              , NULL::INT  AS variabletype_id
              , NULL::TEXT AS variabletype
              , NULL::TEXT AS variabletype_label
              , NULL::TEXT AS variablename
              , NULL::TEXT AS autogenerated
              , NULL::INT  AS sortorder
              , status
              , status2
              , valuename
              , valuename2
         FROM variable_summaries
       )
       UNION ALL
       (
         SELECT summary_type
              , n
              , NULL::INT  AS variabletype_id
              , NULL::TEXT AS variabletype
              , NULL::TEXT AS variabletype_label
              , NULL::TEXT AS variablename
              , NULL::TEXT AS autogenerated
              , NULL::INT  AS sortorder
              , NULL::INT  AS status
              , NULL::INT  AS status2
              , NULL::TEXT AS valuename
              , NULL::TEXT AS valuename2
         FROM count_overall
       )
       ORDER BY summary_type
              , sortorder
              , valuename
              , valuename2
     ) s0;
$$;
CREATE FUNCTION dashin.datadesign_unvalidated_design_datatable
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID           UUID,
            DATASET_ID           UUID,
            EVENT_ID             UUID,
            SUBEVENT_ID          UUID,
            SAMPLINGEVENT_ID     UUID,
            SAMPLINGTIME_ID      UUID,
            CENTER_ID            UUID,
            STARTGROUP_ID        UUID,
            SUBJECT_ID           UUID,
            EVENT_NAME           TEXT,
            SUBEVENT_NAME        TEXT,
            SAMPLINGEVENT_NAME   TEXT,
            SAMPLINGTIME_NAME    TEXT,
            CENTER_NAME          TEXT,
            STARTGROUP_NAME      TEXT,
            SUBJECT_NAME         TEXT,
            EVENT_STATUS         INT,
            SUBEVENT_STATUS      INT,
            SAMPLINGEVENT_STATUS INT,
            SAMPLINGTIME_STATUS  INT,
            CENTER_STATUS        INT,
            STARTGROUP_STATUS    INT,
            SUBJECT_STATUS       INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  overall_n  INT;
  distinct_n INT;
BEGIN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- DISTINCT DESIGN VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE design_distinct
  (
    datarow_id           UUID,
    dataset_id           UUID,
    
    event_name           TEXT,
    subevent_name        TEXT,
    samplingevent_name   TEXT,
    samplingtime_name    TEXT,
    center_name          TEXT,
    startgroup_name      TEXT,
    subject_name         TEXT,
    
    event_status         INT DEFAULT 1,
    subevent_status      INT DEFAULT 1,
    samplingevent_status INT DEFAULT 1,
    samplingtime_status  INT DEFAULT 1,
    center_status        INT DEFAULT 1,
    startgroup_status    INT DEFAULT 1,
    subject_status       INT DEFAULT 1
  )
    ON COMMIT DROP;
  INSERT INTO design_distinct (event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT DISTINCT jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK DUPLICATES
  -- ---------------------------------------------------------------------------------------------------------------------
  
  -- @formatter:off
  -- -------------------------------
  -- distinct count
  -- -------------------------------
  distinct_n := (SELECT count(*) FROM design_distinct);
  
  IF distinct_n = 0 THEN
    RETURN QUERY VALUES (NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT
    ) LIMIT 0;
    RETURN;
  END IF;
  
  -- -------------------------------
  -- overall count
  -- -------------------------------
  overall_n := (SELECT count(*) FROM dashin.datatable dt WHERE dt.dataset_id = _dataset_id AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id,d.owner_org) = (_dataset_id,_owner_org)));
  -- @formatter:on

  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK MISSING VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE check_missing
  (
    name       TEXT,
    missing    INT,
    status     INT,
    statustext TEXT
  )
    ON COMMIT DROP;
  -- -------------------------------------------
  -- status:
  --  0: mixed (missing and non-missing)
  --  1: all missing
  --  2: all non-missing
  -- -------------------------------------------
  INSERT INTO check_missing
  SELECT c.name
       , c.missing
       , CASE
           WHEN c.missing = 0 THEN 2
           ELSE ((distinct_n - c.missing) = 0)::INT
         END AS status
       , CASE
           WHEN c.missing = 0                  THEN 'All non-missing'
           WHEN ((distinct_n - c.missing) = 0) THEN 'All missing'
           ELSE 'Mixed missing and non-missing'
         END AS description
       -- @formatter:off
  FROM (
         (SELECT 'event'         AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.event_name         IS NULL OR d.event_name         = '')) UNION ALL
         (SELECT 'subevent'      AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subevent_name      IS NULL OR d.subevent_name      = '')) UNION ALL
         (SELECT 'samplingevent' AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingevent_name IS NULL OR d.samplingevent_name = '')) UNION ALL
         (SELECT 'samplingtime'  AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingtime_name  IS NULL OR d.samplingtime_name  = '')) UNION ALL
         (SELECT 'center'        AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.center_name        IS NULL OR d.center_name        = '')) UNION ALL
         (SELECT 'startgroup'    AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.startgroup_name    IS NULL OR d.startgroup_name    = '')) UNION ALL
         (SELECT 'subject'       AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subject_name       IS NULL OR d.subject_name       = ''))
       )c;
--   missing_status_all := (SELECT min(c.status) FROM check_missing c);
--   IF (SELECT c.status != 2 FROM check_missing c WHERE c.name = 'subject') THEN
--     RAISE EXCEPTION 'Subject has missing values';
--   END IF;
--   
--   IF missing_status_all = 0 THEN
--     RAISE EXCEPTION 'Some variables have mixed missing and non-missing values';
--   END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- UPDATE FULL DESIGN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- insert data again (including datarow_id, datasetid now that we know it's unique)
  TRUNCATE design_distinct;
  INSERT INTO design_distinct (datarow_id, dataset_id, event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT
    dt.datarow_id
    ,  dt.dataset_id
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM
    dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id;
  
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET (event_name, event_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET event_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET (subevent_name, subevent_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET subevent_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET (samplingevent_name, samplingevent_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET samplingevent_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET (samplingtime_name, samplingtime_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET samplingtime_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET (center_name, center_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET center_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET (startgroup_name, startgroup_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET startgroup_status = 0;
  END IF;
  
  -- @formatter:on
  RETURN QUERY
    SELECT *
    FROM (
           WITH
             event_distinct           AS (
                                           SELECT uuid_generate_v4() AS event_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'event'            AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.event_name   AS name
                                                                , d.event_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , subevent_distinct      AS (
                                           SELECT uuid_generate_v4() AS subevent_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'subevent'         AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.subevent_name   AS name
                                                                , d.subevent_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , samplingevent_distinct AS (
                                           SELECT uuid_generate_v4() AS samplingevent_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'samplingevent'    AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.samplingevent_name   AS name
                                                                , d.samplingevent_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , samplingtime_distinct  AS (
                                           SELECT uuid_generate_v4() AS samplingtime_id
                                                , d.dataset_id
                                                , d.samplingevent_name
                                                , d.name
                                                , d.status
                                                , 'samplingtime'     AS element_name
                                           FROM (
                                                  SELECT DISTINCT d0.dataset_id
                                                                , d0.samplingtime_name   AS name
                                                                , d0.samplingtime_status AS status
                                                                , d0.samplingevent_name
                                                  FROM design_distinct d0
                                                ) d
                                         )
             , center_distinct        AS (
                                           SELECT uuid_generate_v4() AS center_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'center'           AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.center_name   AS name
                                                                , d.center_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , startgroup_distinct    AS (
                                           SELECT uuid_generate_v4() AS startgroup_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'startgroup'       AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.startgroup_name   AS name
                                                                , d.startgroup_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , subject_distinct       AS (
                                           SELECT uuid_generate_v4() AS subject_id
                                                , d.dataset_id
                                                , d.name
                                                , d.status
                                                , 'subject'          AS element_name
                                           FROM (
                                                  SELECT DISTINCT d0.dataset_id
                                                                , d0.subject_name   AS name
                                                                , d0.subject_status AS status
                                                  FROM design_distinct d0
                                                ) d
                                         )
           SELECT dd.datarow_id
                , dd.dataset_id
           
                , ev.event_id
                , se.subevent_id
                , sm.samplingevent_id
                , st.samplingtime_id
                , cd.center_id
                , sg.startgroup_id
                , sb.subject_id
           
                , dd.event_name
                , dd.subevent_name
                , dd.samplingevent_name
                , dd.samplingtime_name
                , dd.center_name
                , dd.startgroup_name
                , dd.subject_name
           
                , dd.event_status
                , dd.subevent_status
                , dd.samplingevent_status
                , dd.samplingtime_status
                , dd.center_status
                , dd.startgroup_status
                , dd.subject_status
           FROM design_distinct               dd
             LEFT JOIN event_distinct         ev ON dd.event_name = ev.name
             LEFT JOIN subevent_distinct      se ON dd.subevent_name = se.name
             LEFT JOIN samplingevent_distinct sm ON dd.samplingevent_name = sm.name
             LEFT JOIN samplingtime_distinct  st ON (dd.samplingtime_name, dd.samplingevent_name) = (st.name, st.samplingevent_name)
             LEFT JOIN center_distinct        cd ON dd.startgroup_name = cd.name
             LEFT JOIN startgroup_distinct    sg ON dd.startgroup_name = sg.name
             LEFT JOIN subject_distinct       sb ON dd.subject_name = sb.name
         ) x0;
  RETURN;
END;
$$;
CREATE FUNCTION dashin.generate_dataset_full_as_csv
(
  _dataset_id UUID
)
  RETURNS TABLE
          (
            DATASET TEXT
          )
  LANGUAGE sql
AS
$$
SELECT string_agg(dat , E'\n')
FROM (
       SELECT dataset_id                               AS datarow_id
            , 0                                        AS ordinal
            , string_agg(name , ';' ORDER BY local_id) AS dat
       FROM dashin.variable
       WHERE dataset_id = _dataset_id
       GROUP BY dataset_id
       UNION ALL
       SELECT datarow_id
            , ordinal
            , string_agg(value , ';' ORDER BY ordinality) AS dat
       FROM (
              SELECT datarow_id
                   , ordinal
                   , value
                   , ordinality
              FROM dashin.datatable
                 , jsonb_array_elements_text(datarow)
                WITH ORDINALITY
              WHERE dataset_id = _dataset_id
            ) x0
       GROUP BY datarow_id
              , ordinal
       ORDER BY ordinal
     ) x1;
$$;

ALTER FUNCTION dashin.generate_dataset_full_as_csv(UUID) OWNER TO :application_user;
CREATE FUNCTION dashin.studydesign_delete_design
(
    _study_id        UUID
  , _owner_org       UUID DEFAULT NULL
  , _delete_subjects BOOL DEFAULT FALSE
)
  RETURNS TABLE
          (
            ELEMENT_NAME  TEXT,
            AFFECTED_ROWS INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  event                       INT;
  subevent                    INT;
  samplingevent               INT;
  center                      INT;
  startgroup                  INT;
  subject                     INT;
  samplingtime                INT;
  event_x_subevent            INT;
  event_subevent_x_startgroup INT;
  subevent_x_samplingevent    INT;
  datadesign                  INT;
  study_exists                INT;
BEGIN
  
  --   @formatter:off
  study_exists := (SELECT count(*) FROM dashin.study st WHERE (st.study_id,st.owner_org) = (_study_id,_owner_org));
  IF study_exists != 1 THEN 
    RETURN QUERY VALUES 
    ('no_study',0);
    RETURN;
  END IF;
  
  UPDATE dashin.datadesign_x_study_event SET (study_id,study_event_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_subevent SET (study_id,study_subevent_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_samplingevent SET (study_id,study_samplingevent_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_samplingtime SET (study_id,study_samplingtime_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_center SET (study_id,study_center_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_startgroup SET (study_id,study_startgroup_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_subject SET (study_id,study_subject_id) = (NULL,NULL) WHERE study_id = _study_id;
 
  -- -------------------------------------------------------------------------------------------
  -- event_subevent_x_startgroup
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event_subevent_x_startgroup WHERE study_id = _study_id;
  GET DIAGNOSTICS event_subevent_x_startgroup = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- subevent_x_samplingevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_subevent_x_samplingevent WHERE study_id = _study_id;
  GET DIAGNOSTICS subevent_x_samplingevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- event_x_subevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event_x_subevent WHERE study_id = _study_id;
  GET DIAGNOSTICS event_x_subevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- event
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event WHERE study_id = _study_id;
  GET DIAGNOSTICS event = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- subevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_subevent WHERE study_id = _study_id;
  GET DIAGNOSTICS subevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- samplingtime get count - deleted by FK cascade
  -- -------------------------------------------------------------------------------------------
  samplingtime := (SELECT count(*) FROM dashin.study_samplingtime
                   WHERE samplingevent_id IN (SELECT se.samplingevent_id
                                              FROM dashin.study_samplingevent se WHERE se.study_id = _study_id));
  
  -- -------------------------------------------------------------------------------------------
  -- samplingevent - deletes samplingtime by FK cascade
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_samplingevent WHERE study_id = _study_id;
  GET DIAGNOSTICS samplingevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- do not automatically delete subjects
  -- -------------------------------------------------------------------------------------------
  IF _delete_subjects THEN
    DELETE FROM dashin.study_subject WHERE study_id = _study_id;
    GET DIAGNOSTICS subject = ROW_COUNT;
  ELSE
    -- cannot set NULL on delete, so do it explicitly before delete
    UPDATE dashin.study_subject SET startgroup_id = NULL WHERE study_id = _study_id;
    subject := 0;
  END IF;
  
  -- can only delete center if subjects have been deleted
  IF _delete_subjects then
    DELETE FROM dashin.study_center WHERE study_id = _study_id;
    get DIAGNOSTICS center = ROW_COUNT;
  ELSE
    center := 0;
  END IF;
  
  -- -------------------------------------------------------------------------------------------
  -- startgroup
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_startgroup WHERE study_id = _study_id;
  GET DIAGNOSTICS startgroup = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- datadesign
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.studydesign_x_datadesign                WHERE study_id = _study_id;
  GET DIAGNOSTICS datadesign                  = ROW_COUNT;
  --   @formatter:on
  RETURN QUERY
    SELECT *
    FROM (
           VALUES ('event', event)
                , ('subevent', subevent)
                , ('event_subevent_x_startgroup', event_subevent_x_startgroup)
                , ('subevent_x_samplingevent', subevent_x_samplingevent)
                , ('event_x_subevent', event_x_subevent)
                , ('samplingtime', samplingtime)
                , ('samplingevent', samplingevent)
                , ('subject', subject)
                , ('center', center)
                , ('startgroup', startgroup)
                , ('datadesign', datadesign)
         ) t(name, n);
END
$$;
CREATE FUNCTION dashin.studydesign_import_design_from_data
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            NAME          TEXT,
            AFFECTED_ROWS INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  study_exists                      BOOL;
  dataset_exists                    BOOL;
  studydesign_exists                BOOL;
  is_allowed                        BOOL;
  datadesign_count                  INT;
  studydesign_x_datadesign          INT;
  event_count                       INT;
  subevent_count                    INT;
  samplingevent_count               INT;
  samplingtime_count                INT;
  center_count                      INT;
  startgroup_count                  INT;
  subject_count                     INT;
  event_x_subevent_count            INT;
  subevent_x_samplingevent_count    INT;
  event_subevent_x_startgroup_count INT;
BEGIN
  datadesign_count := (
                        SELECT count(*)
                        FROM dashin.datadesign_datatable dt
                        WHERE dt.dataset_id = _dataset_id
                      );
  IF datadesign_count = 0 THEN
    RETURN QUERY
      VALUES ('empty', 0);
    RETURN;
  END IF;
  
  studydesign_exists := exists(SELECT
                               FROM dashin.studydesign_x_datadesign x
                               WHERE study_id = _study_id);
  
  study_exists := exists(SELECT
                         FROM dashin.study s
                         WHERE (s.study_id, s.owner_org) = (_study_id, _owner_org));
  
  dataset_exists := exists(SELECT
                           FROM dashin.dataset d
                           WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  is_allowed := (
                  SELECT dataset_exists AND study_exists
                );
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- STUDY, DATASET PERMISSIONS
  -- ----------------------------------------------------------------------------------------------------------------------------
  IF NOT is_allowed THEN
    RETURN QUERY
      VALUES ('denied', 0)
           , ('study', study_exists::INT)
           , ('dataset', dataset_exists::INT)
           , ('studydesign', study_exists::INT)
           , ('allowed', is_allowed::INT);
    RETURN;
  END IF;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- DESIGN EXISTS
  -- ----------------------------------------------------------------------------------------------------------------------------
  IF NOT studydesign_exists THEN
    INSERT INTO dashin.studydesign_x_datadesign (study_id, dataset_id)
    VALUES (_study_id, _dataset_id)
    ON CONFLICT DO NOTHING;
    GET DIAGNOSTICS studydesign_x_datadesign = ROW_COUNT;
  END IF;
  
  IF studydesign_x_datadesign != 1 OR studydesign_exists THEN
    RETURN QUERY
      VALUES ('exists', 0);
    RETURN;
  END IF;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event (event_id, study_id, name, description)
  SELECT DISTINCT ON (dt.event_name) uuid_generate_v4()
                                    , _study_id
                                    , dt.event_name
                                    , 'Imported from data' || CASE
                                                                WHEN dt.event_status = 100 THEN ' (Auto-generated)'
                                                                ELSE ''
                                                              END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subevent (subevent_id, subevent_type_id, study_id, intervention_type_id, name, description)
  SELECT DISTINCT ON (dt.subevent_name) uuid_generate_v4()
                                       , 0
                                       , _study_id
                                       , 0
                                       , dt.subevent_name
                                       , 'Imported from data' || CASE
                                                                   WHEN dt.subevent_status = 100 THEN ' (Auto-generated)'
                                                                   ELSE ''
                                                                 END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SAMPLINGEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_samplingevent (samplingevent_id, sampling_type_id, study_id, name, description)
  SELECT DISTINCT ON (dt.samplingevent_name) uuid_generate_v4()
                                            , 0
                                            , _study_id
                                            , dt.samplingevent_name
                                            , 'Imported from data' || CASE
                                                                        WHEN dt.samplingevent_status = 100 THEN ' (Auto-generated)'
                                                                        ELSE ''
                                                                      END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS samplingevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SAMPLINGTIME
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_samplingtime (samplingtime_id, samplingevent_id, ordinal_time, name, description)
  SELECT uuid_generate_v4() AS samplingtime_id
       , samplingevent_id
       , row_number() OVER (PARTITION BY samplingevent_id ORDER BY samplingtime_name)
       , samplingtime_name
       , description
  FROM (
         SELECT DISTINCT ON (dt.samplingevent_name,dt.samplingtime_name) sm.samplingevent_id
                                                                          , dt.samplingtime_name
                                                                          , 'Imported from data' || CASE
                                                                                                      WHEN dt.samplingtime_status = 100 THEN ' (Auto-generated)'
                                                                                                      ELSE ''
                                                                                                    END AS description
         FROM dashin.datadesign_datatable       dt
           LEFT JOIN dashin.study_samplingevent sm ON dt.samplingevent_name = sm.name
         WHERE (dt.dataset_id, sm.study_id) = (_dataset_id, _study_id)
       ) x0
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS samplingtime_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- CENTER
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_center (center_id, study_id, name, description)
  SELECT DISTINCT ON (dt.center_name) uuid_generate_v4()
                                     , _study_id
                                     , dt.center_name
                                     , 'Imported from data' || CASE
                                                                 WHEN dt.center_status = 100 THEN ' (Auto-generated)'
                                                                 ELSE ''
                                                               END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS center_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- STARTGROUP
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_startgroup (startgroup_id, study_id, name, description)
  SELECT DISTINCT ON (dt.startgroup_name) uuid_generate_v4()
                                         , _study_id
                                         , dt.startgroup_name
                                         , 'Imported from data' || CASE
                                                                     WHEN dt.startgroup_status = 100 THEN ' (Auto-generated)'
                                                                     ELSE ''
                                                                   END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS startgroup_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBJECT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subject (subject_id, study_id, center_id, startgroup_id, name, description)
  SELECT DISTINCT ON (dt.subject_name,dt.startgroup_name) uuid_generate_v4()
                                                           , _study_id
                                                           , sc.center_id
                                                           , sg.startgroup_id
                                                           , dt.subject_name
                                                           , 'Imported from data' || CASE
                                                                                       WHEN dt.subject_status = 100 THEN ' (Auto-generated)'
                                                                                       ELSE ''
                                                                                     END AS description
  FROM dashin.datadesign_datatable     dt
    INNER JOIN dashin.study_center sc ON (sc.study_id, sc.name) = (_study_id, dt.center_name)
    INNER JOIN dashin.study_startgroup sg ON (sg.study_id, sg.name) = (_study_id, dt.startgroup_name)
  WHERE dt.dataset_id = _dataset_id
  
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subject_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT_X_SUBEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event_x_subevent (event_id, subevent_id, study_id)
  SELECT DISTINCT ev.event_id
                , se.subevent_id
                , _study_id
  FROM dashin.datadesign_datatable   dt
    INNER JOIN dashin.study_event    ev ON (ev.study_id, ev.name) = (_study_id, dt.event_name)
    INNER JOIN dashin.study_subevent se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_x_subevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBEVENT_X_SAMPLINGEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subevent_x_samplingevent (subevent_id, samplingevent_id, study_id)
  SELECT DISTINCT se.subevent_id
                , sm.samplingevent_id
                , _study_id
  FROM dashin.datadesign_datatable        dt
    INNER JOIN dashin.study_subevent      se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
    INNER JOIN dashin.study_samplingevent sm ON (sm.study_id, sm.name) = (_study_id, dt.samplingevent_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subevent_x_samplingevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT_SUBEVENT_X_STARTGROUP
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event_subevent_x_startgroup (event_id, subevent_id, study_id, startgroup_id)
  SELECT DISTINCT ev.event_id
                , se.subevent_id
                , _study_id
                , sg.startgroup_id
  FROM dashin.datadesign_datatable     dt
    INNER JOIN dashin.study_event      ev ON (ev.study_id, ev.name) = (_study_id, dt.event_name)
    INNER JOIN dashin.study_subevent   se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
    INNER JOIN dashin.study_startgroup sg ON (sg.study_id, sg.name) = (_study_id, dt.startgroup_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_subevent_x_startgroup_count = ROW_COUNT;
  
  RETURN QUERY VALUES ('design_count', datadesign_count)
                    , ('event_count', event_count)
                    , ('subevent_count', subevent_count)
                    , ('samplingevent_count', samplingevent_count)
                    , ('samplingtime_count', samplingtime_count)
                    , ('center_count', center_count)
                    , ('startgroup_count', startgroup_count)
                    , ('subject_count', subject_count)
                    , ('event_x_subevent_count', event_x_subevent_count)
                    , ('subevent_x_samplingevent_count', subevent_x_samplingevent_count)
                    , ('event_subevent_x_startgroup_count', event_subevent_x_startgroup_count);
  RETURN;
END;
$$;
CREATE FUNCTION dashin.studydesign_validate_data_for_import
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            ELEMENT_NAME       TEXT,
            ID                 UUID,
            ID2                UUID,
            NAME               TEXT,
            NAME2              TEXT,
            DESCRIPTION        TEXT,
            DESCRIPTION2       TEXT,
            DATA_STUDY_MATCHED INT,
            DATA_UNMATCHED     INT,
            STUDY_UNMATCHED    INT,
            MATCH_TEXT         TEXT
          )
  LANGUAGE sql
AS
$$
WITH
  gatekeeper
                         AS (
                              SELECT _study_id
                                   , _dataset_id
                                   , _owner_org
                              WHERE exists(SELECT FROM dashin.study st WHERE (st.study_id, st.owner_org) = (_study_id, _owner_org))
                                AND exists(SELECT FROM dashin.dataset ds WHERE (ds.dataset_id, ds.owner_org) = (_dataset_id, _owner_org))
                            )
  , match_event          AS (
                              SELECT study_event_id
                                   , study_event_name
                                   , study_event_description
                                   , data_event_id
                                   , data_event_name
                              FROM (
                                     SELECT event_id    AS study_event_id
                                          , name        AS study_event_name
                                          , description AS study_event_description
                                     FROM dashin.study_event
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT event_id   AS data_event_id
                                              , event_name AS data_event_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_event_name = d.data_event_name
                            )
  , match_subevent       AS (
                              SELECT study_subevent_id
                                   , study_subevent_name
                                   , study_subevent_description
                                   , data_subevent_id
                                   , data_subevent_name
                              FROM (
                                     SELECT subevent_id AS study_subevent_id
                                          , name        AS study_subevent_name
                                          , description AS study_subevent_description
                                     FROM dashin.study_subevent
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT subevent_id   AS data_subevent_id
                                              , subevent_name AS data_subevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_subevent_name = d.data_subevent_name
                            )
  , match_samplingevent  AS (
                              SELECT study_samplingevent_id
                                   , study_samplingevent_name
                                   , study_samplingevent_description
                                   , data_samplingevent_id
                                   , data_samplingevent_name
                              FROM (
                                     SELECT samplingevent_id AS study_samplingevent_id
                                          , name             AS study_samplingevent_name
                                          , description      AS study_samplingevent_description
                                     FROM dashin.study_samplingevent
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT samplingevent_id   AS data_samplingevent_id
                                              , samplingevent_name AS data_samplingevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_samplingevent_name = d.data_samplingevent_name
                            )
  , match_samplingtime   AS (
                              SELECT study_samplingtime_id
                                   , study_samplingevent_id
                                   , study_samplingevent
                                   , study_samplingevent_description
                                   , study_samplingtime
                                   , study_samplingtime_description
                                   , data_samplingtime_id
                                   , data_samplingevent_id
                                   , data_samplingtime_name
                                   , data_samplingevent_name
                              FROM (
                                     SELECT samplingtime_id     AS study_samplingtime_id
                                          , sm.samplingevent_id AS study_samplingevent_id
                                          , sm.name             AS study_samplingevent
                                          , sm.description      AS study_samplingevent_description
                                          , st.name             AS study_samplingtime
                                          , st.description      AS study_samplingtime_description
                                     FROM dashin.study_samplingtime             st
                                          INNER JOIN dashin.study_samplingevent sm USING (samplingevent_id)
                                        ,                                       gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT samplingtime_id    AS data_samplingtime_id
                                              , samplingevent_id   AS data_samplingevent_id
                                              , samplingtime_name  AS data_samplingtime_name
                                              , samplingevent_name AS data_samplingevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON (s.study_samplingevent, s.study_samplingtime) = (d.data_samplingevent_name, d.data_samplingtime_name)
                            )
  , match_center         AS (
                              SELECT *
                              FROM (
                                     SELECT center_id   AS study_center_id
                                          , name        AS study_center_name
                                          , description AS study_center_description
                                     FROM dashin.study_center
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT center_id   AS data_center_id
                                              , center_name AS data_center_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_center_name = d.data_center_name
                            )
  , match_startgroup     AS (
                              SELECT *
                              FROM (
                                     SELECT startgroup_id AS study_startgroup_id
                                          , name          AS study_startgroup_name
                                          , description   AS study_startgroup_description
                                     FROM dashin.study_startgroup
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT startgroup_id   AS data_startgroup_id
                                              , startgroup_name AS data_startgroup_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_startgroup_name = d.data_startgroup_name
                            )
  , match_subject        AS (
                              SELECT *
                              FROM (
                                     SELECT subject_id     AS study_subject_id
                                          , a.center_id    AS study_center_id
                                          , a.name AS study_subject_name
                                          , a.description  AS study_subject_description
                                          , b.name         AS study_center_name
                                          , b.description  AS study_center_description
                                     FROM dashin.study_subject           a
                                          INNER JOIN dashin.study_center b ON a.center_id = b.center_id
                                        ,                                gatekeeper
                                     WHERE a.study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT subject_id   AS data_subject_id
                                              , center_id    AS data_center_id
                                              , subject_name AS data_subject_name
                                              , center_name  AS data_center_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON (s.study_center_name, s.study_subject_name) = (d.data_center_name, d.data_subject_name)
                            )
  , design_match         AS (
                              SELECT *
                              FROM (
                                     (
                                       SELECT 'event'                           AS element_name
                                            , data_event_id                     AS data_id
                                            , NULL::UUID                        AS data_id2
                                            , data_event_name                   AS data_name
                                            , NULL::TEXT                        AS data_name2
                                            , study_event_description           AS data_description
                                            , NULL::TEXT                        AS data_description2
                                            , study_event_id                    AS study_id
                                            , NULL::UUID                        AS study_id2
                                            , study_event_name                  AS study_name
                                            , NULL::TEXT                        AS study_name2
                                            , study_event_description           AS study_description
                                            , NULL::TEXT                        AS study_description2
                                            , (data_event_id IS NOT NULL)::INT  AS data_exists
                                            , (study_event_id IS NOT NULL)::INT AS study_exists
                                       FROM match_event
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'subevent'                           AS element_name
                                            , data_subevent_id                     AS data_id
                                            , NULL::UUID                           AS data_id2
                                            , data_subevent_name                   AS data_name
                                            , NULL::TEXT                           AS data_name2
                                            , study_subevent_description           AS data_description
                                            , NULL::TEXT                           AS data_description2
                                            , study_subevent_id                    AS study_id
                                            , NULL::UUID                           AS study_id2
                                            , study_subevent_name                  AS study_name
                                            , NULL::TEXT                           AS study_name2
                                            , study_subevent_description           AS study_description
                                            , NULL::TEXT                           AS study_description2
                                            , (data_subevent_id IS NOT NULL)::INT  AS data_exists
                                            , (study_subevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_subevent
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'samplingevent'                           AS element_name
                                            , data_samplingevent_id                     AS data_id
                                            , NULL::UUID                                AS data_id2
                                            , data_samplingevent_name                   AS data_name
                                            , NULL::TEXT                                AS data_name2
                                            , study_samplingevent_description           AS data_description
                                            , NULL::TEXT                                AS data_description2
                                            , study_samplingevent_id                    AS study_id
                                            , NULL::UUID                                AS study_id2
                                            , study_samplingevent_name                  AS study_name
                                            , NULL::TEXT                                AS study_name2
                                            , study_samplingevent_description           AS study_description
                                            , NULL::TEXT                                AS study_description2
                                            , (data_samplingevent_id IS NOT NULL)::INT  AS data_exists
                                            , (study_samplingevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_samplingevent
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'samplingtime'                            AS element_name
                                            , data_samplingtime_id                      AS data_id
                                            , data_samplingevent_id                     AS data_id2
                                            , data_samplingtime_name                    AS data_name
                                            , data_samplingevent_name                   AS data_name2
                                            , study_samplingtime_description            AS data_description
                                            , study_samplingevent_description           AS data_description2
                                            , study_samplingtime_id                     AS study_id
                                            , study_samplingevent_id                    AS study_id2
                                            , study_samplingtime                        AS study_name
                                            , study_samplingevent                       AS study_name2
                                            , study_samplingtime_description            AS study_description
                                            , study_samplingevent_description           AS study_description2
                                            , (data_samplingtime_id IS NOT NULL)::INT   AS data_exists
                                            , (study_samplingevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_samplingtime
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'center'                           AS element_name
                                            , data_center_id                     AS data_id
                                            , NULL::UUID                         AS data_id2
                                            , data_center_name                   AS data_name
                                            , NULL::TEXT                         AS data_name2
                                            , study_center_description           AS data_description
                                            , NULL::TEXT                         AS data_description2
                                            , study_center_id                    AS study_id
                                            , NULL::UUID                         AS study_id2
                                            , study_center_name                  AS study_name
                                            , NULL::TEXT                         AS study_name2
                                            , study_center_description           AS study_description
                                            , NULL::TEXT                         AS study_description2
                                            , (data_center_id IS NOT NULL)::INT  AS data_exists
                                            , (study_center_id IS NOT NULL)::INT AS study_exists
                                       FROM match_center
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'startgroup'                           AS element_name
                                            , data_startgroup_id                     AS data_id
                                            , NULL::UUID                             AS data_id2
                                            , data_startgroup_name                   AS data_name
                                            , NULL::TEXT                             AS data_name2
                                            , study_startgroup_description           AS data_description
                                            , NULL::TEXT                             AS data_description2
                                            , study_startgroup_id                    AS study_id
                                            , NULL::UUID                             AS study_id2
                                            , study_startgroup_name                  AS study_name
                                            , NULL::TEXT                             AS study_name2
                                            , study_startgroup_description           AS study_description
                                            , NULL::TEXT                             AS study_description2
                                            , (data_startgroup_id IS NOT NULL)::INT  AS data_exists
                                            , (study_startgroup_id IS NOT NULL)::INT AS study_exists
                                       FROM match_startgroup
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'subject'                           AS element_name
                                            , data_subject_id                     AS data_id
                                            , data_center_id                      AS data_id2
                                            , data_subject_name                   AS data_name
                                            , data_center_name                    AS data_name2
                                            , study_subject_description           AS data_description
                                            , study_center_description            AS data_description2
                                            , study_subject_id                    AS study_id
                                            , study_center_id                     AS study_id2
                                            , study_subject_name                  AS study_name
                                            , study_center_name                   AS study_name2
                                            , study_subject_description           AS study_description
                                            , study_center_description            AS study_description2
                                            , (data_subject_id IS NOT NULL)::INT  AS data_exists
                                            , (study_subject_id IS NOT NULL)::INT AS study_exists
                                       FROM match_subject
                                     )
                                   ) x0
                            )
  , design_match_summary AS (
                              SELECT element_name
                                   , data_id
                                   , data_id2
                                   , data_name
                                   , data_name2
                                   , data_description
                                   , data_description2
                                   , study_id
                                   , study_id2
                                   , study_name
                                   , study_name2
                                   , study_description
                                   , study_description2
                                   , ((data_exists, study_exists) = (1, 1))::INT AS data_and_study
                                   , ((data_exists, study_exists) = (1, 0))::INT AS data_not_study
                                   , ((data_exists, study_exists) = (0, 1))::INT AS study_not_data
                                   , CASE
                                       WHEN (data_exists, study_exists) = (1, 1) THEN 'data_study_matched'
                                       WHEN (data_exists, study_exists) = (1, 0) THEN 'data_unmatched'
                                       WHEN (data_exists, study_exists) = (0, 1) THEN 'study_unmatched'
                                     END                                         AS match_text
                              FROM design_match
                            )
  , data_unmatched       AS (
                              SELECT 'data_unmatched_' || element_name AS element_name
                                   , data_id                           AS id
                                   , data_id2                          AS id2
                                   , data_name                         AS name
                                   , data_name2                        AS name2
                                   , data_description                  AS description
                                   , data_description2                 AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_unmatched'
                            )
  , study_unmatched      AS (
                              SELECT 'study_unmatched_' || element_name AS element_name
                                   , study_id                           AS id
                                   , study_id2                          AS id2
                                   , study_name                         AS name
                                   , study_name2                        AS name2
                                   , study_description                  AS description
                                   , study_description2                 AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'study_unmatched'
                            )
  , study_matched        AS (
                              SELECT 'study_matched_' || element_name AS element_name
                                   , study_id                         AS id
                                   , study_id2                        AS id2
                                   , study_name                       AS name
                                   , study_name2                      AS name2
                                   , study_description                AS description
                                   , study_description2               AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_study_matched'
                            )
  , data_matched         AS (
                              SELECT 'data_matched_' || element_name AS element_name
                                   , data_id                         AS id
                                   , data_id2                        AS id2
                                   , data_name                       AS name
                                   , data_name2                      AS name2
                                   , data_description                AS description
                                   , data_description2               AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_study_matched'
                            )
  , all_summary          AS (
    
                              SELECT 'all_summary'            AS element_name
                                   , NULL::UUID               AS id
                                   , NULL::UUID               AS id2
                                   , NULL::TEXT               AS name
                                   , NULL::TEXT               AS name2
                                   , NULL::TEXT               AS description
                                   , NULL::TEXT               AS description2
                                   , sum(data_and_study)::INT AS data_study_matched
                                   , sum(data_not_study)::INT AS data_unmatched
                                   , sum(study_not_data)::INT AS study_unmatched
                                   , NULL::TEXT               AS match_text
                              FROM design_match_summary
                            )
SELECT *
FROM all_summary
UNION ALL
SELECT *
FROM data_unmatched
UNION ALL
SELECT *
FROM study_unmatched
UNION ALL
SELECT *
FROM data_matched
UNION ALL
SELECT *
FROM study_matched
ORDER BY element_name
       , name
       , name2;
$$;
CREATE FUNCTION dashin.prefixedunit_combined_symbol
(
    _n_unit_id   INT
  , _n_prefix_id INT
  , _d_unit_id   INT
  , _d_prefix_id INT
  , _n_symbol    TEXT
  , _d_symbol    TEXT
) RETURNS TEXT
  LANGUAGE sql
  IMMUTABLE
AS
$$
SELECT CASE
         WHEN (_n_prefix_id, _n_unit_id) IN ((0, 0)) OR (_d_prefix_id, _d_unit_id) IN ((0, 0)) THEN ''
         WHEN length(_n_symbol) = 0 AND length(_d_symbol) = 0                                  THEN ''
         WHEN length(_n_symbol) = 0 AND length(_d_symbol) > 0                                  THEN '1/' || _d_symbol
         WHEN length(_n_symbol) > 0 AND length(_d_symbol) = 0                                  THEN _n_symbol
         WHEN length(_n_symbol) > 0 AND length(_d_symbol) > 0                                  THEN _n_symbol || '/' || _d_symbol
         ELSE ''
       END
$$;

CREATE FUNCTION dashin.datadesign_subject_x_startgroup_exists
(
    _subject_id    UUID
  , _startgroup_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT
  exists(
    SELECT
      b.study_subject_id
      , c.study_startgroup_id
      , e.subject_name
      , e.startgroup_name
    FROM
      -- element combination linked in studydesign
      dashin.study_subject                              a
        -- study-to-data link for individual elements
        INNER JOIN dashin.datadesign_x_study_subject    b ON a.subject_id = b.study_subject_id
        INNER JOIN dashin.datadesign_x_study_startgroup c ON a.startgroup_id = c.study_startgroup_id
                     -- element combination linked in datadesign
        INNER JOIN dashin.datadesign_datatable          e ON (e.subject_id, e.startgroup_id) = (b.datadesign_subject_id, c.datadesign_startgroup_id)
      -- specific element combination
    WHERE
      (a.subject_id, a.startgroup_id) = (_subject_id, _startgroup_id)
    );
$$
;
CREATE FUNCTION dashin.datadesign_subject_x_center_exists
(
    _subject_id    UUID
  , _center_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT
  exists(
    SELECT *
    FROM
      -- element combination linked in studydesign
      dashin.study_subject                              a
        -- study-to-data link for individual elements
        INNER JOIN dashin.datadesign_x_study_subject    b ON a.subject_id = b.study_subject_id
        INNER JOIN dashin.datadesign_x_study_center c ON a.center_id = c.study_center_id
                     -- element combination linked in datadesign
        INNER JOIN dashin.datadesign_datatable          e ON (e.subject_id, e.center_id) = (b.datadesign_subject_id, c.datadesign_center_id)
      -- specific element combination
    WHERE
      (a.subject_id, a.center_id) = (_subject_id, _center_id)
    );
$$
;
CREATE OR REPLACE FUNCTION dashin.move_study_to_organization
(
    _study_id       UUID
  , _from_owner_org UUID
  , _to_owner_org   UUID
)
  RETURNS TABLE
          (
            STUDY_ID      UUID,
            DATASET_ID    UUID,
            OWNER_ORG     UUID,
            NEW_ORG_NAME  TEXT,
            OLD_ORG_NAME  TEXT,
            OLD_OWNER_ORG UUID,
            ORG_MATCH     BOOL
          )
  LANGUAGE sql
AS
$$
WITH
  study_set_owner_org     AS (
                               UPDATE dashin.study SET owner_org = _to_owner_org
                                 WHERE (study_id, owner_org) = (_study_id, _from_owner_org)
                                 RETURNING study_id,owner_org
                             )
  , dataset_set_owner_org AS (
                               UPDATE dashin.dataset a SET owner_org = _to_owner_org
                                 WHERE a.dataset_id IN (
                                                         SELECT dataset_id
                                                         FROM dashin.dataset_x_study x
                                                         WHERE x.study_id = _study_id
                                                       )
                                   AND a.owner_org = _from_owner_org
                                 RETURNING dataset_id,owner_org
                             )
SELECT a.study_id
     , b.dataset_id
     , a.owner_org
     , c1.name                   AS new_org_name
     , c2.name                   AS old_org_name
     , _from_owner_org           AS old_owner_org
     , a.owner_org = b.owner_org AS matching_orgs
FROM study_set_owner_org          a
     LEFT JOIN admin.organization c1 ON a.owner_org = c1.organization_id
     LEFT JOIN admin.organization c2 ON _from_owner_org = c2.organization_id
   , dataset_set_owner_org        b
  ;
$$;
CREATE FUNCTION dashin.generate_upvar_dataset_data_full_as_csv
(
    _upvar_dataset_id UUID
  , _owner_org        UUID
)
  RETURNS TABLE
          (
            UPVAR_DATASET TEXT
          )
  LANGUAGE sql
AS
$$
SELECT string_agg(dat , E'\n')
FROM (
       (
         -- header
         SELECT a.upvar_dataset_id                                         AS upvar_datarow_id
              , 0                                                          AS ordinal
              , 'varname;' || string_agg(a.name , ';' ORDER BY a.local_id) AS dat
         FROM dashin.upvar_variable        a
           INNER JOIN dashin.upvar_dataset b ON a.upvar_dataset_id = b.upvar_dataset_id
         WHERE (b.upvar_dataset_id, b.owner_org) = (_upvar_dataset_id, _owner_org)
         GROUP BY a.upvar_dataset_id
       )
       UNION ALL
       (
         -- data
         SELECT x0.upvar_datarow_id
              , x0.ordinal
              , x0.name || ';' || string_agg(x0.value , ';' ORDER BY x0.ordinality) AS dat
         FROM (
                SELECT a.upvar_datarow_id
                     , a.ordinal
                     , a.name
                     , c.value
                     , c.ordinality
                FROM dashin.upvar_datatable                             a
                     INNER JOIN dashin.upvar_dataset                    b ON a.upvar_dataset_id = b.upvar_dataset_id
                   , jsonb_array_elements_text(datarow) WITH ORDINALITY c(value, ordinality)
                WHERE (b.upvar_dataset_id, b.owner_org) = (_upvar_dataset_id, _owner_org)
              ) x0
         GROUP BY x0.upvar_datarow_id
                , x0.ordinal
                , x0.name
       )
       ORDER BY ordinal
     ) x1;
$$;

CREATE FUNCTION dashin.studydesign_generate_full_for_download
(
    _study_id  UUID
  , _owner_org UUID
)
  RETURNS TABLE
          (
            ROW_NUMBER         BIGINT,
            STUDY_ID           UUID,
            EVENT_ID           UUID,
            SUBEVENT_ID        UUID,
            SIMPLINGEVENT_ID   UUID,
            SAMPLINGTIME_ID    UUID,
            STARTGROUP_ID      UUID,
            SUBJECT_ID         UUID,
            STUDY_NAME         TEXT,
            EVENT_NAME         TEXT,
            SUBEVENT_NAME      TEXT,
            SAMPLINGEVENT_NAME TEXT,
            SAMPLINGTIME_NAME  TEXT,
            STARTGROUP_NAME    TEXT,
            SUBJECT_NAME       TEXT
          )
  LANGUAGE sql
  STABLE
AS
$$
SELECT
  row_number() OVER ()
  , *
FROM
  (
    SELECT
      z.study_id
      , a.event_id
      , c.subevent_id
      , e.samplingevent_id
      , f.samplingtime_id
      , h.startgroup_id
      , i.subject_id
      , z.name AS study_name
      , a.name AS event_name
      , c.name AS subevent_name
      , e.name AS samplingevent_name
      , f.name AS samplingtime_name
      , h.name AS startgroup_name
      , i.name AS subject_name
    FROM
      dashin.study                                               z
        LEFT JOIN dashin.study_event                       a ON z.study_id = a.study_id
        LEFT JOIN dashin.study_event_x_subevent            b ON a.event_id = b.event_id
        LEFT JOIN dashin.study_subevent                    c ON b.subevent_id = c.subevent_id
        LEFT JOIN dashin.study_subevent_x_samplingevent    d ON c.subevent_id = d.subevent_id
        LEFT JOIN dashin.study_samplingevent               e ON d.samplingevent_id = e.samplingevent_id
        LEFT JOIN dashin.study_samplingtime                f ON e.samplingevent_id = f.samplingevent_id
        LEFT JOIN dashin.study_event_subevent_x_startgroup g ON b.event_id = g.event_id AND b.subevent_id = g.subevent_id
        LEFT JOIN dashin.study_startgroup                  h ON g.startgroup_id = h.startgroup_id
        LEFT JOIN dashin.study_subject                     i ON h.startgroup_id = i.startgroup_id
    WHERE
      (z.study_id, z.owner_org) = (_study_id, _owner_org)
  ) x0;
$$
;
CREATE FUNCTION dashin.search_generate_column_query_numeric
(
  _column_type_id INT
) RETURNS TEXT
  VOLATILE
  LANGUAGE sql
AS
$$
SELECT E'SELECT\n subq.variable_id\n' ||
       E', subq.val\n' ||
       E'FROM(\n' || string_agg(qry , E'\nUNION ALL\n') || E') subq\n'
FROM (
       SELECT E'SELECT variable_id, val \n' ||
              E'FROM dashin.var_datatable\n' ||
              E', text_to_numeric(jsonb_array_element_text(datarow,' || local_id || E')) x(val)\n' ||
              E'WHERE var_dataset_id = ' || quote_literal(a.var_dataset_id) ||
              E'\n' AS qry
       FROM dashin.var_variable_x_search_column_type a
         INNER JOIN dashin.var_variable              b ON a.var_variable_id = b.var_variable_id
         INNER JOIN dashin.var_dataset               c ON a.var_dataset_id = c.var_dataset_id
         INNER JOIN dashin.dataset_x_study           d ON c.dataset_id = d.dataset_id
       WHERE a.search_column_type_id = _column_type_id
     ) x0;
$$;

ALTER FUNCTION dashin.search_generate_column_query_numeric OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics_generate_core_query
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry         TEXT;
  _mz_str     TEXT DEFAULT '';
  _rt_str     TEXT DEFAULT '';
  _mz_rt_str  TEXT DEFAULT '';
  _offset_str TEXT DEFAULT '';
  _limit_str  TEXT DEFAULT '';
BEGIN
  -- mz
  IF _mz_center IS NOT NULL AND _mz_range IS NOT NULL
  THEN
    _mz_str := '(mz BETWEEN ' || _mz_center - _mz_range || ' AND ' || _mz_center + _mz_range || ' OR mz IS NULL)';
  ELSEIF _mz_center IS NOT NULL
  THEN
    _mz_str := '(mz = ' || _mz_center || ' OR mz IS NULL)';
  END IF;
  
  -- rt
  IF _rt_center IS NOT NULL AND _rt_range IS NOT NULL
  THEN
    _rt_str := '(rt BETWEEN ' || _rt_center - _rt_range || ' AND ' || _rt_center + _rt_range || ' OR rt IS NULL)';
  ELSEIF _rt_center IS NOT NULL
  THEN
    _rt_str := '(rt = ' || _rt_center || ' OR rt IS NULL)';
  END IF;
  
  -- rt+mz
  IF length(_mz_str) > 0 AND length(_rt_str) > 0 THEN
    _mz_rt_str := _mz_str || ' AND ' || _rt_str;
  ELSEIF length(_mz_str) > 0 THEN
    _mz_rt_str := _mz_str;
  ELSEIF length(_rt_str) > 0 THEN
    _mz_rt_str := _rt_str;
  END IF;
  
  IF _limit >= 0 THEN
    _offset_str := E'OFFSET ' || coalesce(_offset , 0);
    _limit_str := E'\nLIMIT ' || _limit;
  END IF;
  
  qry := (
           SELECT
             -- @formatter:off
               E'SELECT coalesce(a.variable_id,b.variable_id) as variable_id,mz,rt FROM\n(' || dashin.search_generate_column_query_numeric(3) || E') a (variable_id,rt)\n' ||
               E'FULL JOIN\n(\n' || dashin.search_generate_column_query_numeric(2) || E')b (variable_id,mz) ON a.variable_id = b.variable_id\n' ||
               E'WHERE ' || _mz_rt_str || E'\n' ||
               E'AND NOT (rt IS NULL AND mz IS NULL)\n' ||
               _offset_str || _limit_str || E'\n' ||
               E''
           -- @formatter:on
         );
  RETURN qry;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics_generate_core_query OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics_core_result
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TABLE
          (
            VARIABLE_ID UUID,
            MZ          NUMERIC,
            RT          NUMERIC
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry TEXT;
BEGIN
  qry := dashin.search_metabolomics_generate_core_query(_mz_center , _mz_range , _rt_center , _rt_range , _offset , _limit);
  IF qry IS NULL THEN
    RETURN ;
  END IF;
  RETURN QUERY EXECUTE qry;
  RETURN;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics_core_result OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics_summary_generate_query
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
)
  RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry        TEXT;
  _mz_str    TEXT DEFAULT '';
  _rt_str    TEXT DEFAULT '';
  _mz_rt_str TEXT DEFAULT '';
BEGIN
  -- mz
  IF _mz_center IS NOT NULL AND _mz_range IS NOT NULL
  THEN
    _mz_str := '(mz BETWEEN ' || _mz_center - _mz_range || ' AND ' || _mz_center + _mz_range || ' OR mz IS NULL)';
  ELSEIF _mz_center IS NOT NULL
  THEN
    _mz_str := '(mz = ' || _mz_center || ' OR mz IS NULL)';
  END IF;
  
  -- rt
  IF _rt_center IS NOT NULL AND _rt_range IS NOT NULL
  THEN
    _rt_str := '(rt BETWEEN ' || _rt_center - _rt_range || ' AND ' || _rt_center + _rt_range || ' OR rt IS NULL)';
  ELSEIF _rt_center IS NOT NULL
  THEN
    _rt_str := '(rt = ' || _rt_center || ' OR rt IS NULL)';
  END IF;
  
  -- rt+mz
  IF length(_mz_str) > 0 AND length(_rt_str) > 0 THEN
    _mz_rt_str := _mz_str || ' AND ' || _rt_str;
  ELSEIF length(_mz_str) > 0 THEN
    _mz_rt_str := _mz_str;
  ELSEIF length(_rt_str) > 0 THEN
    _mz_rt_str := _rt_str;
  END IF;
  
  qry := (
           SELECT E'SELECT\n' ||
                  E'  f.study_id' ||
                  E', d.dataset_id\n' ||
                  E', f.name AS study_name' ||
                  E', d.name AS dataset_name' ||
                  E', min(x1.mz) AS min_mz' ||
                  E', max(x1.mz) AS max_mz' ||
                  E', min(x1.rt) AS min_rt' ||
                  E', max(x1.rt) AS max_rt' ||
                  E', count(*)::INT AS n\n' ||
                  E', max(x1.n_all) AS n_all\n' ||
                  E'FROM\n(\n' ||
                  E'SELECT\n variable_id,mz,rt,(count(*) OVER ())::INT AS n_all\nFROM\n(\n' ||
                  E'SELECT coalesce(a.variable_id,b.variable_id) as variable_id,mz,rt \n' ||
                  E'FROM\n(' || dashin.search_generate_column_query_numeric(3) || E') a (variable_id,rt)\n' ||
                  E'FULL JOIN\n(\n' || dashin.search_generate_column_query_numeric(2) || E')b (variable_id,mz) ON a.variable_id = b.variable_id\n' ||
                  E'WHERE ' || _mz_rt_str || E'\n' ||
                  E'AND NOT (rt IS NULL AND mz IS NULL)\n' ||
                  E') x0\n' ||
                  E') x1\n' ||
                  E' LEFT JOIN dashin.variable        v ON x1.variable_id = v.variable_id\n' ||
                  E' LEFT JOIN dashin.dataset         d ON v.dataset_id = d.dataset_id\n' ||
                  E' LEFT JOIN dashin.dataset_x_study e ON d.dataset_id = e.dataset_id\n' ||
                  E' LEFT JOIN dashin.study           f ON e.study_id = f.study_id\n' ||
                  E'GROUP BY f.study_id,f.name,d.dataset_id,d.name'
         );
  RETURN qry;
END;
$$
;


ALTER FUNCTION dashin.search_metabolomics_summary_generate_query OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics_summary
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
)
  RETURNS TABLE
          (
            STUDY_ID     UUID,
            DATASET_ID   UUID,
            STUDY_NAME   TEXT,
            DATASET_NAME TEXT,
            MIN_MZ       NUMERIC,
            MAX_MZ       NUMERIC,
            MIN_RT       NUMERIC,
            MAX_RT       NUMERIC,
            N            INT,
            N_ALL        INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry TEXT;
BEGIN
  qry := dashin.search_metabolomics_summary_generate_query(_mz_center , _mz_range , _rt_center , _rt_range);
  IF qry IS NULL THEN
    RETURN;
  END IF;
  RETURN QUERY EXECUTE qry;
  RETURN;
END;
$$
;
ALTER FUNCTION dashin.search_metabolomics_summary OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics_generate_query
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry         TEXT;
  _mz_str     TEXT DEFAULT '';
  _rt_str     TEXT DEFAULT '';
  _mz_rt_str  TEXT DEFAULT '';
  _offset_str TEXT DEFAULT '';
  _limit_str  TEXT DEFAULT '';
BEGIN
  -- mz
  IF _mz_center IS NOT NULL AND _mz_range IS NOT NULL
  THEN
    _mz_str := '(mz BETWEEN ' || _mz_center - _mz_range || ' AND ' || _mz_center + _mz_range || ' OR mz IS NULL)';
  ELSEIF _mz_center IS NOT NULL
  THEN
    _mz_str := '(mz = ' || _mz_center || ' OR mz IS NULL)';
  END IF;
  
  -- rt
  IF _rt_center IS NOT NULL AND _rt_range IS NOT NULL
  THEN
    _rt_str := '(rt BETWEEN ' || _rt_center - _rt_range || ' AND ' || _rt_center + _rt_range || ' OR rt IS NULL)';
  ELSEIF _rt_center IS NOT NULL
  THEN
    _rt_str := '(rt = ' || _rt_center || ' OR rt IS NULL)';
  END IF;
  
  -- rt+mz
  IF length(_mz_str) > 0 AND length(_rt_str) > 0 THEN
    _mz_rt_str := _mz_str || ' AND ' || _rt_str;
  ELSEIF length(_mz_str) > 0 THEN
    _mz_rt_str := _mz_str;
  ELSEIF length(_rt_str) > 0 THEN
    _mz_rt_str := _rt_str;
  END IF;
  
  IF _limit >= 0 THEN
    _offset_str := E'\nOFFSET ' || _offset;
    _limit_str := E'\nLIMIT ' || _limit;
  END IF;
  qry := (
           SELECT
             -- @formatter:off
             E'SELECT\n' ||
             E'  f.study_id\n' ||
             E', d.dataset_id\n' ||
             E', x1.variable_id\n' ||
             E', f.name AS study_name\n' ||
             E', d.name AS dataset_name\n' ||
             E', v.name AS variable_name\n' ||
             E', x1.mz\n' ||
             E', x1.rt\n' ||
             E'FROM\n(\n' ||
             E'SELECT\n variable_id,mz,rt\nFROM\n(\n' ||
             E'SELECT coalesce(a.variable_id,b.variable_id) as variable_id,mz,rt FROM\n(' || dashin.search_generate_column_query_numeric(3) || E') a (variable_id,rt)\n' ||
             E'FULL JOIN\n(\n' || dashin.search_generate_column_query_numeric(2) || E')b (variable_id,mz) ON a.variable_id = b.variable_id\n' ||
             E'WHERE ' || _mz_rt_str || E'\n' ||
             E'AND NOT (rt IS NULL AND mz IS NULL)\n' ||
             E') x0\n' ||
             _offset_str || _limit_str || E'\n' ||
             E') x1\n' ||
             E' LEFT JOIN dashin.variable        v ON x1.variable_id = v.variable_id\n' ||
             E' LEFT JOIN dashin.dataset         d ON v.dataset_id = d.dataset_id\n' ||
             E' LEFT JOIN dashin.dataset_x_study e ON d.dataset_id = e.dataset_id\n' ||
             E' LEFT JOIN dashin.study           f ON e.study_id = f.study_id\n' ||
             E''
             -- @formatter:on
         );
  RETURN qry;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics_generate_query OWNER TO :application_user;
CREATE FUNCTION dashin.search_metabolomics
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TABLE
          (
            STUDY_ID      UUID,
            DATASET_ID    UUID,
            VARIABLE_ID   UUID,
            STUDY_NAME    TEXT,
            DATASET_NAME  TEXT,
            VARIABLE_NAME TEXT,
            MZ            NUMERIC,
            RT            NUMERIC
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry TEXT;
BEGIN
  qry := dashin.search_metabolomics_generate_query(_mz_center , _mz_range , _rt_center , _rt_range , _offset , _limit);
  IF qry IS NULL THEN
    RETURN ;
  END IF;
  RETURN QUERY EXECUTE qry;
  RETURN;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics OWNER TO :application_user;
CREATE FUNCTION dashin.datadesign_event_x_subevent_exists
(
    _event_id    UUID
  , _subevent_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT exists(
  SELECT datadesign_event_id
       , datadesign_subevent_id
       , b.dataset_id
  FROM
    -- event-subevent link
    dashin.study_event_x_subevent                   a
      -- study-to-data link for individual elements
      INNER JOIN dashin.datadesign_x_study_event    b ON a.event_id = b.study_event_id
      INNER JOIN dashin.datadesign_x_study_subevent c ON (b.dataset_id, a.subevent_id) = (c.dataset_id, c.study_subevent_id)
                   -- event-x-subevent link in datadesign
      INNER JOIN dashin.datadesign_datatable        d ON (d.event_id, d.subevent_id) = (b.datadesign_event_id, c.datadesign_subevent_id)
    -- specific event + subevent
  WHERE (a.event_id, a.subevent_id) = (_event_id, _subevent_id)
  );
$$
;
CREATE FUNCTION dashin.studydata_add_dataset_to_study
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS
    TABLE
    (
      STATUS      TEXT,
      STATUS_ID   INT,
      DESCRIPTION TEXT
    )
  LANGUAGE plpgsql
AS
$$
DECLARE
  ds_matched            INT;
  d_unmatched           INT;
  s_unmatched           INT;
  dataset_already_added BOOL;
  -- ----------------------
  max_ordinal           INT;
  studyvar_n            INT;
  datasetvar_n          INT;
  -- ----------------------
  studydesign_rows      INT;
  study_rows            INT;
  dataset_rows          INT;
  data_rows             INT;
  event_rows            INT;
  subevent_rows         INT;
  samplingevent_rows    INT;
  samplingtime_rows     INT;
  center_rows           INT;
  startgroup_rows       INT;
  subject_rows          INT;

BEGIN
  -- ------------------------------------------------------------------
  -- check
  -- ------------------------------------------------------------------
  dataset_already_added := exists(SELECT * FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id));
  
  IF dataset_already_added
  THEN
    RETURN QUERY VALUES ('result', 2, 'Dataset already added to study.');
    RETURN;
  END IF;
  
  -- ------------------------------------------------------------------
  -- validate, match and permissions (6ms)
  -- ------------------------------------------------------------------
  SELECT a.data_study_matched
       , a.data_unmatched
       , a.study_unmatched
  INTO ds_matched,d_unmatched,s_unmatched
  FROM dashin.studydesign_validate_data_for_import(_study_id , _dataset_id , _owner_org) a
  WHERE a.element_name = 'all_summary';
  
  -- ------------------------------------------------------------------
  -- return if dataset is not a subset of study
  -- ------------------------------------------------------------------
  IF d_unmatched != 0
  THEN
    RETURN QUERY VALUES ('result', 0, 'failed. Data design does not match study design.')
                      , ('matched', ds_matched, '')
                      , ('data unmatched', d_unmatched, CASE WHEN d_unmatched > 0 THEN 'data cannot have unmatched design combinations' ELSE '' END)
                      , ('study unmatched', s_unmatched, '');
    RETURN;
  END IF;
  
  -- ------------------------------------------------------------------
  -- dataset_x_study
  -- ------------------------------------------------------------------
  INSERT INTO dashin.dataset_x_study (dataset_id, study_id)
  VALUES (_dataset_id, _study_id)
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS dataset_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- event (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_event h
  SET (study_id, study_event_id) = (k.study_id, k.study_event_id)
  FROM (
         SELECT DISTINCT a.event_id AS datadesign_event_id
                       , b.event_id AS study_event_id
                       , a.dataset_id
                       , b.study_id
                       , a.event_name
         FROM dashin.datadesign_datatable a
           FULL JOIN dashin.study_event   b ON a.event_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_event_id) = (k.dataset_id, k.datadesign_event_id);
  GET DIAGNOSTICS event_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- subevent (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_subevent h
  SET (study_id, study_subevent_id) = (k.study_id, k.study_subevent_id)
  FROM (
         SELECT DISTINCT a.subevent_id AS datadesign_subevent_id
                       , b.subevent_id AS study_subevent_id
                       , a.dataset_id
                       , b.study_id
                       , a.subevent_name
         FROM dashin.datadesign_datatable  a
           FULL JOIN dashin.study_subevent b ON a.subevent_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_subevent_id) = (k.dataset_id, k.datadesign_subevent_id);
  GET DIAGNOSTICS subevent_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- samplingevent (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_samplingevent h
  SET (study_id, study_samplingevent_id) = (k.study_id, k.study_samplingevent_id)
  FROM (
         SELECT DISTINCT a.samplingevent_id AS datadesign_samplingevent_id
                       , b.samplingevent_id AS study_samplingevent_id
                       , a.dataset_id
                       , b.study_id
                       , a.samplingevent_name
         FROM dashin.datadesign_datatable       a
           FULL JOIN dashin.study_samplingevent b ON a.samplingevent_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_samplingevent_id) = (k.dataset_id, k.datadesign_samplingevent_id);
  GET DIAGNOSTICS samplingevent_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- samplingtime (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_samplingtime m
  SET (study_id, study_samplingtime_id) = (l.study_id, l.study_samplingtime_id)
  FROM (
         SELECT DISTINCT dataset_id
                       , samplingevent_id   AS datadesign_samplingevent_id
                       , samplingtime_id    AS datadesign_samplingtime_id
                       , samplingtime_name  AS datadesign_samplingtime_name
                       , samplingevent_name AS datadesign_samplingevent_name
         FROM dashin.datadesign_datatable
         WHERE dataset_id = _dataset_id
         ORDER BY samplingtime_name
                , samplingevent_name
       ) k
    INNER JOIN
  (
    SELECT a.samplingevent_id AS study_samplingevent_id
         , a.samplingtime_id  AS study_samplingtime_id
         , a.name             AS study_samplingtime_name
         , b.name             AS study_samplingevent_name
         , b.study_id
    FROM dashin.study_samplingtime          a
      INNER JOIN dashin.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
    WHERE b.study_id = _study_id
    ORDER BY a.name
           , b.name
  )      l ON (k.datadesign_samplingtime_name, k.datadesign_samplingevent_name) = (l.study_samplingtime_name, l.study_samplingevent_name)
  WHERE (m.dataset_id, m.datadesign_samplingtime_id) = (k.dataset_id, k.datadesign_samplingtime_id);
  GET DIAGNOSTICS samplingtime_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- center (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_center h
  SET (study_id, study_center_id) = (k.study_id, k.study_center_id)
  FROM (
         SELECT DISTINCT a.center_id AS datadesign_center_id
                       , b.center_id AS study_center_id
                       , a.dataset_id
                       , b.study_id
                       , a.center_name
         FROM dashin.datadesign_datatable a
           FULL JOIN dashin.study_center  b ON a.center_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_center_id) = (k.dataset_id, k.datadesign_center_id);
  GET DIAGNOSTICS center_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- startgroup (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_startgroup h
  SET (study_id, study_startgroup_id) = (k.study_id, k.study_startgroup_id)
  FROM (
         SELECT DISTINCT a.startgroup_id AS datadesign_startgroup_id
                       , b.startgroup_id AS study_startgroup_id
                       , a.dataset_id
                       , b.study_id
                       , a.startgroup_name
         FROM dashin.datadesign_datatable    a
           FULL JOIN dashin.study_startgroup b ON a.startgroup_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_startgroup_id) = (k.dataset_id, k.datadesign_startgroup_id);
  GET DIAGNOSTICS startgroup_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- subject (2.5ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_subject h
  SET (study_id, study_subject_id) = (k.study_id, k.study_subject_id)
  FROM (
         SELECT DISTINCT g.study_id
                       , g.study_subject_id
                       , f.dataset_id
                       , f.datadesign_subject_id
         FROM (
                SELECT DISTINCT dataset_id
                              , subject_id AS datadesign_subject_id
                              , subject_name
                              , center_name
                FROM dashin.datadesign_datatable
                WHERE dataset_id = _dataset_id
              ) f
           FULL JOIN
         (
           SELECT DISTINCT a.subject_id   AS study_subject_id
                         , a.name AS subject_name
                         , a.center_id  AS study_center_id
                         , b.name         AS center_name
                         , a.study_id
           FROM dashin.study_subject       a
             FULL JOIN dashin.study_center b ON (a.center_id, a.study_id) = (b.center_id, a.study_id)
           WHERE a.study_id = _study_id
         )      g ON (f.center_name, f.subject_name) = (g.center_name, g.subject_name)
       ) k
  WHERE (h.dataset_id, h.datadesign_subject_id) = (k.dataset_id, k.datadesign_subject_id);
  GET DIAGNOSTICS subject_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- add entries to studydesign and studydatatable (1ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studydataset (study_id)
  VALUES (_study_id)
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS studydesign_rows = ROW_COUNT;
  
  CREATE TEMPORARY TABLE temp_studyrows_with_datarow_link
  (
    studyrow_id      UUID DEFAULT uuid_generate_v4(),
    datarow_id       UUID,
    study_id         UUID,
    dataset_id       UUID,
    event_id         UUID,
    subevent_id      UUID,
    samplingevent_id UUID,
    samplingtime_id  UUID,
    center_id        UUID,
    startgroup_id    UUID,
    subject_id       UUID
  )
    ON COMMIT DROP;
  
  -- ------------------------------------------------------------------
  -- generate studyrows and datarow-links (4104ms,3353ms)
  -- ------------------------------------------------------------------
  INSERT INTO temp_studyrows_with_datarow_link (studyrow_id, datarow_id, study_id, dataset_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id)
  SELECT coalesce(p.studyrow_id , uuid_generate_v4())            AS studyrow_id
       , q.datarow_id
       , coalesce(p.study_id , q.study_id)                       AS study_id
       , q.dataset_id
       , coalesce(p.event_id , q.study_event_id)                 AS event_id
       , coalesce(p.subevent_id , q.study_subevent_id)           AS subevent_id
       , coalesce(p.samplingevent_id , q.study_samplingevent_id) AS samplingevent_id
       , coalesce(p.samplingtime_id , q.study_samplingtime_id)   AS samplingtime_id
       , coalesce(p.center_id , q.study_center_id)               AS center_id
       , coalesce(p.startgroup_id , q.study_startgroup_id)       AS startgroup_id
       , coalesce(p.subject_id , q.study_subject_id)             AS subject_id
  FROM (
         SELECT studyrow_id
              , study_id
              , event_id
              , subevent_id
              , samplingevent_id
              , samplingtime_id
              , center_id
              , startgroup_id
              , subject_id
         FROM dashin.studydatatable
         WHERE study_id = _study_id
       )         p
    RIGHT JOIN (
                 SELECT a.datarow_id
                      , a.dataset_id
                      , z.study_id
                      , b.study_event_id
                      , c.study_subevent_id
                      , d.study_samplingevent_id
                      , e.study_samplingtime_id
                      , f.study_center_id
                      , g.study_startgroup_id
                      , h.study_subject_id
                 FROM dashin.datadesign_datatable                     a
                   INNER JOIN dashin.dataset_x_study                  z ON a.dataset_id = z.dataset_id
                   LEFT JOIN  dashin.datadesign_x_study_event         b ON (a.dataset_id, a.event_id) = (b.dataset_id, b.datadesign_event_id)
                   LEFT JOIN  dashin.datadesign_x_study_subevent      c ON (a.dataset_id, a.subevent_id) = (c.dataset_id, c.datadesign_subevent_id)
                   LEFT JOIN  dashin.datadesign_x_study_samplingevent d ON (a.dataset_id, a.samplingevent_id) = (d.dataset_id, d.datadesign_samplingevent_id)
                   LEFT JOIN  dashin.datadesign_x_study_samplingtime  e ON (a.dataset_id, a.samplingtime_id) = (e.dataset_id, e.datadesign_samplingtime_id)
                   LEFT JOIN  dashin.datadesign_x_study_center        f ON (a.dataset_id, a.center_id) = (f.dataset_id, f.datadesign_center_id)
                   LEFT JOIN  dashin.datadesign_x_study_startgroup    g ON (a.dataset_id, a.startgroup_id) = (g.dataset_id, g.datadesign_startgroup_id)
                   LEFT JOIN  dashin.datadesign_x_study_subject       h ON (a.dataset_id, a.subject_id) = (h.dataset_id, h.datadesign_subject_id)
                 WHERE a.dataset_id = _dataset_id
                   AND z.study_id = _study_id
               ) q
                 ON (
                     p.event_id, p.subevent_id, p.samplingevent_id, p.samplingtime_id, p.center_id, p.startgroup_id, p.subject_id
                    ) = (
                         q.study_event_id, q.study_subevent_id, q.study_samplingevent_id, q.study_samplingtime_id, q.study_center_id, q.study_startgroup_id, q.study_subject_id
                        );
  
  -- ------------------------------------------------------------------
  -- insert studyrows (4ms,2.5ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studydatatable (studyrow_id, study_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id)
  SELECT studyrow_id
       , study_id
       , event_id
       , subevent_id
       , samplingevent_id
       , samplingtime_id
       , center_id
       , startgroup_id
       , subject_id
  FROM temp_studyrows_with_datarow_link
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS study_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- insert studyrow-datarow-links (12ms,6ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyrow_x_datarow (studyrow_id, study_id, datarow_id, dataset_id)
  SELECT studyrow_id
       , study_id
       , datarow_id
       , dataset_id
  FROM temp_studyrows_with_datarow_link;
  GET DIAGNOSTICS data_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- IMPORT VARIABLES INTO STUDY
  -- ------------------------------------------------------------------
  
  -- ------------------------------------------------------------------
  -- dataset variables
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_dataset_variables
  (
    variable_id      UUID,
    dataset_id       UUID,
    name             TEXT,
    datatype_id      INT,
    variabletype_id INT,
    local_id         INT
  )
    ON COMMIT DROP;
  INSERT INTO temp_dataset_variables (variable_id, dataset_id, name, datatype_id, variabletype_id, local_id)
  SELECT b.variable_id
       , b.dataset_id
       , b.name
       , b.datatype_id
       , b.variabletype_id
       , b.local_id
  FROM dashin.variable b
  WHERE b.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset x WHERE (x.dataset_id, x.owner_org) = (_dataset_id, _owner_org));
  
  -- ------------------------------------------------------------------
  -- studyvar_x_datasetvar
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_studyvar_x_var
  (
    studyvariable_id UUID,
    variable_id      UUID,
    dataset_id       UUID,
    name             TEXT,
    datatype_id      INT,
    variabletype_id INT,
    local_id         INT
  )
    ON COMMIT DROP;
  INSERT INTO temp_studyvar_x_var (studyvariable_id, variable_id, dataset_id, name, datatype_id, variabletype_id, local_id)
  SELECT uuid_generate_v4() AS studyvariable_id
       , b.variable_id
       , b.dataset_id
       , b.name
       , b.datatype_id
       , b.variabletype_id
       , b.local_id
  FROM temp_dataset_variables b;
  
  -- ------------------------------------------------------------------
  -- max ordinal
  -- ------------------------------------------------------------------
  max_ordinal := (
                   SELECT coalesce(x.max_val , 0)::INT
                   FROM (
                          SELECT max(b.ordinal) AS max_val
                          FROM dashin.studyvariable b
                          WHERE b.study_id = _study_id
                            AND exists(SELECT FROM dashin.study x WHERE (x.study_id, x.owner_org) = (_study_id, _owner_org))
                        ) x
                 );
  
  -- ------------------------------------------------------------------
  -- prepare datavar for insert
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_datavar_for_insert
  (
    studyvariable_id UUID,
    name             TEXT,
    label            TEXT,
    ordinal          INT,
    datatype_id      INT,
    variabletype_id INT,
    variable_id      UUID,
    dataset_id       UUID
  )
    ON COMMIT DROP;
  INSERT INTO temp_datavar_for_insert (studyvariable_id, name, label, ordinal, datatype_id, variabletype_id, variable_id, dataset_id)
  SELECT b.studyvariable_id
       , b.name
       , b.name                                                AS label
       , row_number() OVER (ORDER BY b.local_id) + max_ordinal AS ordinal
       , b.datatype_id
       , b.variabletype_id
       , b.variable_id
       , b.dataset_id
  FROM temp_studyvar_x_var b;
  
  -- ------------------------------------------------------------------
  -- add studyvariables
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyvariable (studyvariable_id, study_id, name, label, ordinal, datatype_id, variabletype_id)
  SELECT studyvariable_id
       , (_study_id)::UUID
       , name
       , label
       , ordinal
       , datatype_id
       , variabletype_id
  FROM temp_datavar_for_insert
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS studyvar_n = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- link studyvar-x-datavar
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyvariable_x_variable (studyvariable_id, study_id, variable_id, dataset_id)
  SELECT coalesce(b.studyvariable_id , a.studyvariable_id)
       , b.study_id
       , a.variable_id
       , a.dataset_id
  FROM temp_datavar_for_insert     a
    LEFT JOIN dashin.studyvariable b ON a.name = b.name
  WHERE b.study_id = _study_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS datasetvar_n = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- return result summary
  -- ------------------------------------------------------------------
  RETURN QUERY VALUES ('result', 1, 'Dataset successfully added.')
                    , ('matched', ds_matched, 'matching design elements (individually matched, not cross-referenced)')
                    , ('data unmatched', d_unmatched, 'design elements in data not matching studydesign')
                    , ('study unmatched', s_unmatched, 'design elements in stiudy not matching datadesign')
                    , ('studydesign created', studydesign_rows, '')
                    , ('studyrows added', study_rows, 'number of studyrows (cross-referenced unique datarows) added to study')
                    , ('datarows linked', data_rows, 'number of new datarows linked to studyrows')
                    , ('dataset', dataset_rows, '')
                    , ('variables linked', datasetvar_n, '')
                    , ('event', event_rows, 'number of events')
                    , ('subevent', subevent_rows, 'number of subevents')
                    , ('samplingevent', samplingevent_rows, 'number of samplingevents')
                    , ('samplingtime', samplingtime_rows, 'number of samplingtimes')
                    , ('center', center_rows, 'number of centers')
                    , ('startgroup', startgroup_rows, 'number of startgroups')
                    , ('subject', subject_rows, 'number of subjects');
  RETURN;
END;
$$
;
CREATE FUNCTION dashin.studydata_remove_dataset_from_study
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
  , _keep_rows  BOOL DEFAULT FALSE
  , _keep_vars  BOOL DEFAULT FALSE
)
  RETURNS
    TABLE
    (
      STATUS      TEXT,
      STATUS_ID   INT,
      DESCRIPTION TEXT
    )
  LANGUAGE plpgsql
AS
$$
DECLARE
  valid_owner        BOOL;
  dataset_present    BOOL;
  -- ----------------------
  datasetvar_n       INT;
  studyvar_n         INT;
  -- ----------------------
  study_rows         INT;
  dataset_rows       INT;
  event_rows         INT;
  subevent_rows      INT;
  samplingevent_rows INT;
  samplingtime_rows  INT;
  center_rows        INT;
  startgroup_rows    INT;
  subject_rows       INT;
BEGIN
  -- validate permissions
  valid_owner := exists(SELECT * FROM dashin.study s WHERE (s.study_id, s.owner_org) = (_study_id, _owner_org)) AND
                 exists(SELECT * FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  IF NOT valid_owner
  THEN
    RETURN QUERY VALUES ('result', 0, 'Failed. Could not validate dataset and/or study.');
    RETURN;
  END IF;
  
  -- check
  dataset_present := exists(SELECT * FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id));
  
  IF NOT dataset_present
  THEN
    RETURN QUERY VALUES ('result', 2, 'Dataset is not present in this study.');
    RETURN;
  END IF;
  
  DELETE FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id);
  GET DIAGNOSTICS dataset_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_event h
  SET (study_id, study_event_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS event_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_subevent h
  SET (study_id, study_subevent_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS subevent_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_samplingevent h
  SET (study_id, study_samplingevent_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS samplingevent_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_samplingtime h
  SET (study_id, study_samplingtime_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS samplingtime_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_center h
  SET (study_id, study_center_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS center_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_startgroup h
  SET (study_id, study_startgroup_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS startgroup_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_subject h
  SET (study_id, study_subject_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS subject_rows = ROW_COUNT;
  
  IF NOT _keep_rows THEN
    DELETE
    FROM dashin.studydatatable
    WHERE study_id = _study_id AND studyrow_id NOT IN (
                                                        SELECT studyrow_id
                                                        FROM dashin.studyrow_x_datarow x
                                                        WHERE x.study_id = _study_id
                                                      );
    GET DIAGNOSTICS study_rows = ROW_COUNT;
  ELSE
    study_rows := 0;
  END IF;
  
  DELETE
  FROM dashin.studyvariable_x_variable
  WHERE variable_id IN (
                         SELECT v.variable_id
                         FROM dashin.variable v
                         WHERE dataset_id = _dataset_id
                       );
  GET DIAGNOSTICS datasetvar_n = ROW_COUNT;
  
  IF NOT _keep_vars THEN
    DELETE
    FROM dashin.studyvariable a
    WHERE a.study_id = _study_id AND a.studyvariable_id NOT IN (
                                                                 SELECT studyvariable_id
                                                                 FROM dashin.studyvariable_x_variable x
                                                                 WHERE x.study_id = _study_id
                                                               );
    GET DIAGNOSTICS studyvar_n = ROW_COUNT;
  ELSE
    studyvar_n := 0;
  END IF;
  
  RETURN QUERY VALUES ('result', 1, 'Dataset successfully removed for study.')
                    , ('studyrows', study_rows, 'Studyrows removed')
                    , ('dataset', dataset_rows, '')
                    , ('event', event_rows, '')
                    , ('subevent', subevent_rows, '')
                    , ('samplingevent', samplingevent_rows, '')
                    , ('samplingtime', samplingtime_rows, '')
                    , ('center', center_rows, '')
                    , ('startgroup', startgroup_rows, '')
                    , ('subject', subject_rows, '');
  RETURN;

END;
$$
;
CREATE FUNCTION dashin.studyvariablecondition_designvar_category_summary
(
    _studyvariablecondition_id UUID
  , _study_id                  UUID
  , _owner_org                 UUID
)
  RETURNS TABLE
          (
            STUDYVARIABLECONDITION_ID UUID,
            ELEMENT_ID                UUID,
            VALUE_COLUMN              TEXT,
            ELEMENT2_ID               UUID,
            ELEMENT2_NAME             TEXT,
            SELECTED_N                INTEGER,
            CATEGORY_N                INTEGER,
            ORDINAL                   INTEGER,
            IS_SELECTED               BOOLEAN,
            ANY_SELECTED              BOOLEAN,
            VARIABLETYPE_ID           INTEGER,
            CONDITIONOPERATOR_ID      INTEGER
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  is_samplingtime       BOOL DEFAULT FALSE;
  _variabletype_id      INT;
  _conditionoperator_id INT;
BEGIN
  
  -- special treatment of samplingevent:
  -- if current variabletype is samplingevent (vartype_id = 6)
  -- and an event is selected (condition_x_category NOT NULL)
  -- treat as samplingtime (vartype_id = 7)
  SELECT CASE
           WHEN b.variabletype_id = 6 AND c.element_id IS NOT NULL THEN 7
           ELSE
             b.variabletype_id
         END
       , a.conditionoperator_id
  INTO _variabletype_id,_conditionoperator_id
  FROM dashin.studyvariablecondition                    a
    INNER JOIN dashin.studyvariable                     b ON a.studyvariable_id = b.studyvariable_id
    LEFT JOIN  dashin.studyvariablecondition_x_category c ON a.studyvariablecondition_id = c.studyvariablecondition_id
  WHERE a.studyvariablecondition_id = _studyvariablecondition_id
    AND a.study_id = _study_id
    AND exists(SELECT * FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  CREATE TEMPORARY TABLE temp_intermediate_table
  (
    category_n                INT,
    conditionoperator_id      INT,
    studyvariablecondition_id UUID,
    element_id                UUID,
    element_name              TEXT,
    element2_id               UUID,
    element2_name             TEXT,
    ordinal                   INT
  )
    ON COMMIT DROP;
  
  IF _variabletype_id = 2 THEN
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.subject_id               AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , NULL::INT                  AS ordinal
    FROM dashin.study_subject          b
      INNER JOIN dashin.studydatatable d ON b.subject_id = d.subject_id
    WHERE b.study_id = _study_id
    GROUP BY b.subject_id
           , b.name;
  
  ELSEIF _variabletype_id = 3 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.startgroup_id            AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_startgroup       b
      INNER JOIN dashin.studydatatable d ON b.startgroup_id = d.startgroup_id
    WHERE b.study_id = _study_id
    GROUP BY b.startgroup_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 4 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.event_id                 AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_event            b
      INNER JOIN dashin.studydatatable d ON b.event_id = d.event_id
    WHERE b.study_id = _study_id
    GROUP BY b.event_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 5 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.subevent_id              AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_subevent         b
      INNER JOIN dashin.studydatatable d ON b.subevent_id = d.subevent_id
    WHERE b.study_id = _study_id
    GROUP BY b.subevent_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 8 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.center_id                AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_center           b
      INNER JOIN dashin.studydatatable d ON b.center_id = d.center_id
    WHERE b.study_id = _study_id
    GROUP BY b.center_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 6 THEN
    -- if no samplingevent selected, return samplingevent categories
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.samplingevent_id         AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_samplingevent    b
      INNER JOIN dashin.studydatatable d ON b.samplingevent_id = d.samplingevent_id
    WHERE b.study_id = _study_id
    GROUP BY b.samplingevent_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 7 THEN
    -- if samplinevent is selected, return samplingtime categories for that event: 
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.samplingevent_id         AS element_id
         , b.name                     AS element_name
         , c.samplingtime_id          AS element2_id
         , c.name                     AS element2_name
         , c.ordinal_time             AS ordinal
    FROM dashin.study_samplingevent                       b
      LEFT JOIN  dashin.study_samplingtime                c ON b.samplingevent_id = c.samplingevent_id
      INNER JOIN dashin.studydatatable                    d ON b.samplingevent_id = d.samplingevent_id
      INNER JOIN dashin.studyvariablecondition_x_category e ON (_studyvariablecondition_id, b.samplingevent_id) = (e.studyvariablecondition_id, e.element_id)
    WHERE b.study_id = _study_id
    GROUP BY b.samplingevent_id
           , b.name
           , c.samplingtime_id
           , c.name
           , c.ordinal_time;
    
    is_samplingtime := TRUE;
  
  ELSE
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT count(*)   AS category_n
         , x0.conditionoperator_id
         , x0.studyvariablecondition_id
         , NULL::UUID AS element_id
         , x0.element_name
         , NULL::UUID AS element2_id
         , NULL::TEXT AS element2_name
         , NULL::INT  AS ordinal
    FROM (
           SELECT d.name
                , d.local_id
                , jsonb_array_element_text(e.datarow , d.local_id) AS element_name
                , a.conditionoperator_id
                , a.studyvariablecondition_id
           FROM dashin.studyvariablecondition           a
             INNER JOIN dashin.studyvariable            b ON a.studyvariable_id = b.studyvariable_id
             INNER JOIN dashin.studyvariable_x_variable c ON a.studyvariable_id = c.studyvariable_id
             INNER JOIN dashin.variable                 d ON c.variable_id = d.variable_id
             INNER JOIN dashin.datatable                e ON d.dataset_id = e.dataset_id
           WHERE a.studyvariablecondition_id = _studyvariablecondition_id
         ) x0
    GROUP BY x0.element_name
           , x0.conditionoperator_id
           , x0.studyvariablecondition_id;
  
  END IF;
  
  IF is_samplingtime THEN
    
    RETURN QUERY
      SELECT xa.studyvariablecondition_id
           , xa.element2_id
           , xa.element2_name
           , xa.element_id
           , xa.element_name
           , coalesce(sum(xa.category_n) FILTER ( WHERE xb.samplingtime_id IS NOT NULL) OVER () , 0)::INT AS selected_n
           , xa.category_n
           , xa.ordinal
           , xb.samplingtime_id IS NOT NULL                                                               AS is_selected
           , (count(*) FILTER ( WHERE xb.samplingtime_id IS NOT NULL) OVER ()) > 0                        AS any_selected
           , _variabletype_id
           , xa.conditionoperator_id
      FROM (
             SELECT a.studyvariablecondition_id
                  , a.element_id
                  , a.element_name
                  , a.element2_id
                  , a.element2_name
                  , a.category_n
                  , a.ordinal
                  , a.conditionoperator_id
             FROM temp_intermediate_table a
           )                                                           xa
        LEFT JOIN dashin.studyvariablecondition_x_samplingtimecategory xb ON (xa.studyvariablecondition_id, xa.element2_id) = (xb.studyvariablecondition_id, xb.samplingtime_id)
      ORDER BY xa.ordinal
             , xa.element_name;
  
  ELSE
    
    RETURN QUERY
      SELECT xa.studyvariablecondition_id
           , xa.element_id
           , xa.element_name
           , xa.element2_id
           , xa.element2_name
           , coalesce(sum(xa.category_n) FILTER ( WHERE xb.element_id IS NOT NULL) OVER () , 0)::INT AS selected_n
           , xa.category_n
           , xa.ordinal
           , coalesce(xb.element_id::TEXT , xc.value_column::TEXT) IS NOT NULL                       AS is_selected
           , (count(*) FILTER ( WHERE xb.element_id IS NOT NULL) OVER ()) > 0                        AS any_selected
           , _variabletype_id
           , xa.conditionoperator_id
      FROM (
             SELECT a.category_n
                  , a.conditionoperator_id
                  , a.studyvariablecondition_id
                  , a.element_id
                  , a.element_name
                  , a.element2_id
                  , a.element2_name
                  , a.ordinal
             FROM temp_intermediate_table a
           )                                               xa
        LEFT JOIN dashin.studyvariablecondition_x_category xb ON (xa.studyvariablecondition_id, xa.element_id) = (xb.studyvariablecondition_id, xb.element_id)
        LEFT JOIN dashin.studyvariablecondition_x_category xc ON (xa.studyvariablecondition_id, xa.element_name) = (xc.studyvariablecondition_id, xc.value_column)
      ORDER BY xa.ordinal
             , xa.element_name;
  
  END IF;
  
  RETURN;
END;
$$;


CREATE FUNCTION dashin.get_study_element_info
(
  _element_id UUID
)
  RETURNS TABLE
          (
            ELEMENT_ID       UUID,
            STUDY_ID         UUID,
            NAME             TEXT,
            LABEL            TEXT,
            DESCRIPTION      TEXT,
            ORDINAL          INTEGER,
            VARIABLETYPE_ID INTEGER
          )
  LANGUAGE sql
AS
$$
(
  SELECT a.samplingevent_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 6                  AS variabletype_id
  FROM dashin.study_samplingevent a
  WHERE samplingevent_id = _element_id
)
UNION
(
  SELECT a.samplingtime_id AS element_id
       , b.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal_time    AS ordinal
       , 7                 AS variabletype_id
  FROM dashin.study_samplingtime          a
    INNER JOIN dashin.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
  WHERE a.samplingtime_id = _element_id
)
UNION
(
  SELECT a.event_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 4          AS variabletype_id
  FROM dashin.study_event a
  WHERE event_id = _element_id
)
UNION
(
  SELECT a.subevent_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 5             AS variabletype_id
  FROM dashin.study_subevent a
  WHERE subevent_id = _element_id
)
UNION
(
  SELECT a.startgroup_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 3               AS variabletype_id
  FROM dashin.study_startgroup a
  WHERE startgroup_id = _element_id
)
UNION
(
  SELECT a.center_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 8           AS variabletype_id
  FROM dashin.study_center a
  WHERE center_id = _element_id
)
UNION
(
  SELECT a.subject_id   AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , NULL::INT      AS ordinal
       , 2              AS variabletype_id
  FROM dashin.study_subject a
  WHERE subject_id = _element_id
)
$$;


