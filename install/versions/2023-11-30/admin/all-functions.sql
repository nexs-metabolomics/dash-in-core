\echo '----------------------------------------------------------------'
\echo ' CREATE UTILS FUNCTIONS'
\echo '----------------------------------------------------------------'

CREATE FUNCTION utils.asterisk_match
(
  x   JSONB
, col JSONB
) RETURNS BOOLEAN
  STABLE
  LANGUAGE SQL
AS
$$
SELECT
  CASE WHEN col = '["*"]' THEN TRUE
       ELSE col @> x
  END
$$;

ALTER FUNCTION utils.asterisk_match(x JSONB, col JSONB) OWNER TO :application_user;

CREATE FUNCTION utils.asterisk_match
(
  x   TEXT
, col TEXT
) RETURNS BOOLEAN
  STABLE
  LANGUAGE SQL
AS
$$
SELECT
  CASE WHEN col = '*' THEN TRUE
       ELSE col = x
  END
$$;

ALTER FUNCTION utils.asterisk_match(x TEXT, col TEXT) OWNER TO :application_user;
CREATE FUNCTION utils.is_numeric
(
  TEXT
) RETURNS BOOLEAN
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE x NUMERIC;
BEGIN
  x = $1 :: NUMERIC;
  RETURN TRUE;
EXCEPTION
  WHEN OTHERS THEN
    RETURN FALSE;
END;
$$;

ALTER FUNCTION utils.is_numeric(TEXT) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_array_a_intersect_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  ARRAY_TO_JSON(ARRAY(SELECT
                        jsonb_array_elements(a)
                      INTERSECT
                      SELECT
                        jsonb_array_elements(b)))::JSONB
$$;

ALTER FUNCTION utils.jsonb_array_a_intersect_b(a JSONB, b JSONB) OWNER TO :application_user;

CREATE FUNCTION utils.jsonb_array_a_intersect_range
(
  a     JSONB
, _low  INTEGER
, _high INTEGER
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_agg(value)
FROM
  jsonb_array_elements(a) WITH ORDINALITY
WHERE ordinality BETWEEN _low AND _high
$$;

ALTER FUNCTION utils.jsonb_array_a_intersect_range(a JSONB, _low INTEGER, _high INTEGER) OWNER TO :application_user;

CREATE FUNCTION utils.jsonb_array_a_minus_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  IMMUTABLE
  LANGUAGE SQL
AS
$$
SELECT
  JSONB_AGG(a)
FROM
  (
    SELECT
      jsonb_array_elements(a) AS a
      , b
  ) t0
WHERE NOT (a <@ b)
   OR b IS NULL
$$;

ALTER FUNCTION utils.jsonb_array_a_minus_b(a JSONB, b JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_array_a_union_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  ARRAY_TO_JSON(ARRAY_AGG(DISTINCT (jsarr)))::JSONB
FROM
  (
    SELECT
      jsonb_array_elements(a) AS jsarr
    UNION ALL
    SELECT
      jsonb_array_elements(b) AS jsarr
  ) t0
$$;

ALTER FUNCTION utils.jsonb_array_a_union_b(a JSONB, b JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_array_elements_by_index
(
  arr  JSONB
, idxs JSONB
) RETURNS SETOF JSONB
  LANGUAGE SQL
AS
$$
SELECT
    arr -> (n::INT)
FROM
  jsonb_array_elements_text(idxs) AS n
$$;

ALTER FUNCTION utils.jsonb_array_elements_by_index(arr JSONB, idxs JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_array_elements_by_index_text
(
  arr  JSONB
, idxs JSONB
) RETURNS SETOF TEXT
  LANGUAGE SQL
AS
$$
SELECT
    arr ->> (n :: INT)
FROM
  jsonb_array_elements_text(idxs) AS n
$$;

ALTER FUNCTION utils.jsonb_array_elements_by_index_text(arr JSONB, idxs JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_each_by_keys
(
  obj  JSONB
, keys JSONB
) RETURNS SETOF JSONB
  LANGUAGE SQL
AS
$$
SELECT
    obj -> n
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_each_by_keys(obj JSONB, keys JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_each_by_keys_text
(
  obj  JSONB
, keys JSONB
) RETURNS SETOF TEXT
  LANGUAGE SQL
AS
$$
SELECT
    obj ->> n
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_each_by_keys_text(obj JSONB, keys JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_object_extract_by_key
(
  obj  JSONB
, keys JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_object_agg(n , obj -> n)
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_object_extract_by_key(obj JSONB, keys JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_object_to_array_by_keys
(
  obj  JSONB
, keys JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_agg(obj -> n)
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_object_to_array_by_keys(obj JSONB, keys JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_set_deep
(
  target JSONB
, path   TEXT[]
, val    JSONB
) RETURNS JSONB
  LANGUAGE plpgsql
AS
$$
DECLARE k TEXT;
        p TEXT[];
BEGIN
  -- Create missing objects in the path.
  FOREACH k IN ARRAY path
    LOOP
      p := p || k;
      IF (target #> p IS NULL) THEN
        target := jsonb_set(target , p , '{}' :: JSONB);
        END IF;
    END LOOP;
  -- Set the value like normal.
  RETURN jsonb_set(target , path , val);
END;
$$;

ALTER FUNCTION utils.jsonb_set_deep(target JSONB, PATH TEXT[], val JSONB) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_text_to_numeric
(
  _x             JSONB
, _empty_as_null BOOLEAN DEFAULT TRUE
) RETURNS JSONB
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE x  NUMERIC;
        x0 TEXT;
        x1 TEXT;
BEGIN
  x0 = _x #>> '{}';
  x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
    WHEN x0 = '' AND _empty_as_null      THEN NULL
    WHEN x0 ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(x0 , '.' , '') , ',' , '.')
    -- comma dot or comma comma -> comma is thousand separator, remove comma
    WHEN x0 ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(x0 , ',' , '')
    -- else replace comma with dot
    ELSE replace(x0 , ',' , '.')
       END;
  x = x1:: NUMERIC;
  RETURN to_jsonb(x);
EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
END;
$$;

ALTER FUNCTION utils.jsonb_text_to_numeric(_x JSONB, _empty_as_null BOOLEAN) OWNER TO :application_user;
CREATE FUNCTION utils.jsonb_text_is_numeric
(
  _x             JSONB
, _empty_as_null BOOLEAN DEFAULT TRUE
) RETURNS BOOLEAN
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE x  NUMERIC;
        x0 TEXT;
        x1 TEXT;
BEGIN
  x0 = _x #>> '{}';
  x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
    WHEN x0 = '' AND _empty_as_null      THEN NULL
    WHEN x0 ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(x0 , '.' , '') , ',' , '.')
    -- comma dot or comma comma -> comma is thousand separator, remove comma
    WHEN x0 ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(x0 , ',' , '')
    -- else replace comma with dot
    ELSE replace(x0 , ',' , '.')
       END;
  x = x1:: NUMERIC;
  RETURN TRUE;
EXCEPTION
  WHEN OTHERS THEN
    RETURN FALSE;
END;
$$;

ALTER FUNCTION utils.jsonb_text_is_numeric(JSONB, BOOLEAN) OWNER TO :application_user;
CREATE FUNCTION utils.text_is_numeric
(
  _x TEXT
)
  RETURNS BOOL
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE
    x  NUMERIC;
    x1 TEXT;
BEGIN
    x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
             WHEN _x ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(_x , '.' , '') , ',' , '.')
        -- comma dot or comma comma -> comma is thousand separator, remove comma
             WHEN _x ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(_x , ',' , '')
        -- else replace comma with dot
             ELSE replace(_x , ',' , '.')
         END;
    x = x1:: NUMERIC;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RETURN FALSE;
END;
$$;
CREATE FUNCTION utils.text_to_numeric
(
      _x             TEXT
    , _empty_as_null BOOL DEFAULT TRUE
) RETURNS NUMERIC
    IMMUTABLE
    LANGUAGE plpgsql
AS
$$
DECLARE
    x  NUMERIC;
    x1 TEXT;
BEGIN
    x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
             WHEN _x = '' AND _empty_as_null      THEN NULL
             WHEN _x ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(_x , '.' , '') , ',' , '.')
        -- comma dot or comma comma -> comma is thousand separator, remove comma
             WHEN _x ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(_x , ',' , '')
        -- else replace comma with dot
             ELSE replace(_x , ',' , '.')
         END;
    x = x1:: NUMERIC;
    RETURN x;
EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
END;
$$;\echo '----------------------------------------------------------------'
\echo ' CREATE ADMIN FUNCTIONS'
\echo '----------------------------------------------------------------'

CREATE OR REPLACE FUNCTION admin.get_roles
(
  _roles    JSONB -- jsonb array of roles from manager
, _col_id   TEXT DEFAULT 'role_id' -- role_id column
, _col_incl TEXT DEFAULT 'roles_include' -- roles_include
, _col_excl TEXT DEFAULT 'roles_exclude' -- roles_exclude
, _tbl_name TEXT DEFAULT 'admin.role' -- table name with schema
, _tbl      REGCLASS DEFAULT 'admin.role' -- table name with schema
)
  RETURNS JSONB
  LANGUAGE plpgsql
  IMMUTABLE
AS
$$
DECLARE result JSONB;
BEGIN
  EXECUTE (FORMAT(
          E'SELECT jsonb_agg(x) FROM (SELECT DISTINCT\n' ||
          E'jsonb_array_elements(admin.get_role_with_children(%I,''%s'',''%s'',''%s'',''%s'')) AS x\n' ||
          E'FROM %s\n' ||
          E'WHERE to_jsonb(%I) <@ ($1)::JSONB)sq;'
    , _col_id , _col_id , _col_incl , _col_excl , _tbl_name
    , _tbl
    , _col_id
    )) USING _roles INTO result;
  RETURN result;
END;
$$;

ALTER FUNCTION admin.get_roles(_roles JSONB, _col_id TEXT, _col_incl TEXT, _col_excl TEXT, _tbl_name TEXT, _tbl REGCLASS) OWNER TO :application_user;
CREATE OR REPLACE FUNCTION admin.get_manager_roles
(
  _organization_id UUID
, _user_id         UUID
)
  RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  utils.jsonb_array_a_intersect_b(admin.get_roles(m.roles) , admin.get_roles(o.roles)) AS role_id
FROM
  admin.manager                  m
    LEFT JOIN admin.organization o USING (organization_id)
WHERE organization_id = _organization_id
  AND user_id = _user_id
$$;

ALTER FUNCTION admin.get_manager_roles(UUID, _user_id UUID) OWNER TO :application_user;
CREATE OR REPLACE FUNCTION admin.get_role_with_children
(
  _x        INT
, _col_id   TEXT DEFAULT 'role_id'
, _col_incl TEXT DEFAULT 'roles_include'
, _col_excl TEXT DEFAULT 'roles_exclude'
, _tbl_name REGCLASS DEFAULT 'admin.role'
)
  RETURNS JSONB
  STABLE
  LANGUAGE plpgsql
AS
$$
DECLARE result JSONB;
BEGIN
  EXECUTE (format(
                        E'SELECT\n' ||
                        E'  jsonb_array_a_union_b (\n' ||
                        E'    CASE\n' ||
                        E'      WHEN %I @> to_jsonb(0) THEN\n' ||
                        E'        jsonb_array_a_minus_b((SELECT jsonb_agg(%I) FROM (SELECT %I FROM %s WHERE %I NOT IN (0,$1))x),%I)\n' ||
                        E'      ELSE %I\n' ||
                        E'    END\n' ||
                        E'    , to_jsonb(''{}''::int[] || %I)\n' ||
                        E'  ) AS roles\n' ||
                        E'FROM %s\n' ||
                        E'WHERE %I = $1'
    , _col_incl
    , _col_id , _col_id , _tbl_name , _col_id , _col_excl
    , _col_incl
    , _col_id
    , _tbl_name
    , _col_id
    )) USING _x INTO result;
  RETURN result;
END;
$$;

ALTER FUNCTION admin.get_role_with_children(_x INT, _col_id TEXT, _col_incl TEXT, _col_excl TEXT, _tbl_name REGCLASS) OWNER TO :application_user;

\echo '----------------------------------------------------------------'
\echo ' CREATE DASHIN FUNCTIONS'
\echo '----------------------------------------------------------------'

