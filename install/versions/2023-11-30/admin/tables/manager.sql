CREATE TABLE admin.manager
(
  user_id UUID NOT NULL,
  organization_id UUID NOT NULL,
  label TEXT DEFAULT ''::TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  roles JSONB DEFAULT '[]'::JSONB,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details JSONB,
  CONSTRAINT manager_pkey
    PRIMARY KEY (organization_id, user_id)
);

ALTER TABLE admin.manager OWNER TO :application_user;

