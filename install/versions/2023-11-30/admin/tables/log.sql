CREATE TABLE admin.log
(
  log_id SERIAL NOT NULL,
  user_id UUID,
  route TEXT,
  uri TEXT,
  params JSONB
);

ALTER TABLE admin.log OWNER TO :application_user;
