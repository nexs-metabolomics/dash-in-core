CREATE TABLE IF NOT EXISTS dashin.assay_x_supportfile
(
  assay_id       UUID NOT NULL,
  supportfile_id UUID NOT NULL,
  CONSTRAINT assay_x_supportfile_pk PRIMARY KEY (assay_id, supportfile_id)
);

ALTER TABLE dashin.assay_x_supportfile OWNER TO :application_user;
