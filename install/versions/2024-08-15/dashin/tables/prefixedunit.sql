CREATE TABLE dashin.prefixedunit
(
  prefixedunit_id     SERIAL,
  unitprefix_id       INT,
  unit_id             INT,
  combined_symbol     TEXT,
  name                TEXT,
  unitdomain_id       INT,
  unitprefixdomain_id INT,
  description         TEXT,
  valid               INT DEFAULT 0,
  CONSTRAINT prefixedunit_pk PRIMARY KEY (unitprefix_id, unit_id),
  CONSTRAINT prefixedunit_uk UNIQUE (prefixedunit_id)
);

ALTER TABLE dashin.prefixedunit OWNER TO :application_user;
