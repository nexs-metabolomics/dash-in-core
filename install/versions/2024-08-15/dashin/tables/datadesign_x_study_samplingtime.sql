CREATE TABLE dashin.datadesign_x_study_samplingtime
(
  dataset_id                 UUID,
  datadesign_samplingtime_id UUID PRIMARY KEY,
  study_id                   UUID,
  study_samplingtime_id      UUID,
  CONSTRAINT dxs_dataset_id_samplingtime_id_key UNIQUE (dataset_id, study_samplingtime_id)
);

ALTER TABLE dashin.datadesign_x_study_samplingtime OWNER TO :application_user;
