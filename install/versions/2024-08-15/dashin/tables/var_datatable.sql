CREATE TABLE dashin.var_datatable
(
  variable_id    UUID NOT NULL,
  var_dataset_id UUID NOT NULL,
  ordinal        INTEGER NOT NULL,
  status         INT DEFAULT 1,
  created_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details        JSONB,
  datarow        JSONB,
  CONSTRAINT var_datatable_pk
    PRIMARY KEY (variable_id, var_dataset_id),
  CONSTRAINT var_datatable_ordinal_uk UNIQUE (var_dataset_id, ordinal)
);

ALTER TABLE dashin.var_datatable OWNER TO :application_user;
