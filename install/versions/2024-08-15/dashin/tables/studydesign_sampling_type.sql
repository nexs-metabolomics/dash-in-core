CREATE TABLE dashin.studydesign_sampling_type
(
  sampling_type_id       INTEGER NOT NULL
    CONSTRAINT studydesign_sampling_type_pk
      PRIMARY KEY,
  ordinal                INTEGER,
  created_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                   TEXT,
  description            TEXT,
  additional_data_schema JSONB
);

ALTER TABLE dashin.studydesign_sampling_type OWNER TO :application_user;
