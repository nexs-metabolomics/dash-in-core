CREATE TABLE dashin.unitprefixdomain
(
  unitprefixdomain_id INT PRIMARY KEY,
  ordinal             INT,
  name                TEXT,
  description         TEXT
);

ALTER TABLE dashin.unitprefixdomain OWNER TO :application_user;
