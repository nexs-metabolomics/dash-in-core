CREATE TABLE dashin.study_event_x_subevent
(
  event_id    UUID NOT NULL,
  subevent_id UUID NOT NULL,
  study_id    UUID NOT NULL,
  CONSTRAINT study_event_x_subevent_pk
    PRIMARY KEY (event_id, subevent_id)
);

ALTER TABLE dashin.study_event_x_subevent OWNER TO :application_user;
