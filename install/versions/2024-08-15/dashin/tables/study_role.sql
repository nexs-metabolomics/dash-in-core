CREATE TABLE dashin.study_role
(
  study_role_id INTEGER NOT NULL
    CONSTRAINT study_role_pk
      PRIMARY KEY,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.study_role OWNER TO :application_user;
