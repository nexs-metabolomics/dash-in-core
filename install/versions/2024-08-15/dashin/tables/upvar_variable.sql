CREATE TABLE dashin.upvar_variable
(
  upvar_variable_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_variable_pk
      PRIMARY KEY,
  upvar_dataset_id  UUID,
  local_id          INTEGER NOT NULL,
  name              TEXT,
  description       TEXT,
  status            INT DEFAULT 1,
  datatype_id       INT DEFAULT 1,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details           JSONB
);

ALTER TABLE dashin.upvar_variable OWNER TO :application_user;
