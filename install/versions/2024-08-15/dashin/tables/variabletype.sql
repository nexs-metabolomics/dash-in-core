CREATE TABLE dashin.variabletype
(
  variabletype_id INT NOT NULL
    CONSTRAINT variabletype_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT
);

ALTER TABLE dashin.variabletype OWNER TO :application_user;
