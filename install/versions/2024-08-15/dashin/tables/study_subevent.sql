CREATE TABLE dashin.study_subevent
(
  subevent_id          UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_subevent_pk
      PRIMARY KEY,
  subevent_type_id     INTEGER NOT NULL,
  study_id             UUID,
  ordinal              INTEGER,
  intervention_type_id INTEGER,
  created_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                 TEXT,
  label                TEXT,
  description          TEXT,
  row_comment          TEXT,
  additional_data      JSONB,
  CONSTRAINT study_subevent_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_subevent OWNER TO :application_user;
