CREATE FUNCTION dashin.get_designvariabletype_ids
(
  VARIADIC _vartype TEXT[] DEFAULT '{all}'::TEXT[]
)
  RETURNS TABLE
          (
            VARIABLETYPE_ID INTEGER
          )
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
BEGIN
  IF _vartype = '{all}' THEN
    RETURN QUERY VALUES (2), (3), (4), (5), (6), (7), (8);
  ELSE
    RETURN QUERY
      SELECT x0.variabletype_id
      FROM (
             VALUES ('subject', 2)
                  , ('startgroup', 3)
                  , ('event', 4)
                  , ('subevent', 5)
                  , ('samplingevent', 6)
                  , ('samplingtime', 7)
                  , ('center', 8)
           ) x0(name, variabletype_id)
      WHERE ARRAY [x0.name] <@ _vartype;
  END IF;
  RETURN;
END;
$$;
