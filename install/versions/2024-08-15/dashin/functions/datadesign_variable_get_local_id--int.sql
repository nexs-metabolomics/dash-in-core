CREATE FUNCTION dashin.datadesign_variable_get_local_id
(
  _dataset_id    UUID
, _variabletype_id INT
) RETURNS INT
  LANGUAGE SQL
  IMMUTABLE STRICT
AS $$
SELECT
  local_id
FROM
  dashin.variable
WHERE dataset_id = _dataset_id
  AND variabletype_id = _variabletype_id;
$$;

ALTER FUNCTION dashin.datadesign_variable_get_local_id(_dataset_id UUID, _variabletype_id INT) OWNER TO :application_user;

