CREATE FUNCTION dashin.datadesign_subject_x_startgroup_exists
(
    _subject_id    UUID
  , _startgroup_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT
  exists(
    SELECT
      b.study_subject_id
      , c.study_startgroup_id
      , e.subject_name
      , e.startgroup_name
    FROM
      -- element combination linked in studydesign
      dashin.study_subject                              a
        -- study-to-data link for individual elements
        INNER JOIN dashin.datadesign_x_study_subject    b ON a.subject_id = b.study_subject_id
        INNER JOIN dashin.datadesign_x_study_startgroup c ON a.startgroup_id = c.study_startgroup_id
                     -- element combination linked in datadesign
        INNER JOIN dashin.datadesign_datatable          e ON (e.subject_id, e.startgroup_id) = (b.datadesign_subject_id, c.datadesign_startgroup_id)
      -- specific element combination
    WHERE
      (a.subject_id, a.startgroup_id) = (_subject_id, _startgroup_id)
    );
$$
;
