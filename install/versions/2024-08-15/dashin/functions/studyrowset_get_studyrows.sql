CREATE FUNCTION dashin.studyrowset_get_studyrows
(
    _studyrowset_id UUID
  , _study_id       UUID
  , _owner_org      UUID
)
  RETURNS TABLE
          (
            STUDYROW_ID UUID
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  _subset_is_empty BOOL;
  result_query     TEXT;
BEGIN
  
  -- check if studyrowsubset has any conditions 
  _subset_is_empty := NOT exists(SELECT
                                 FROM dashin.studyrowsubset                                  a
                                   INNER JOIN dashin.studyrowsubset_x_studyvariablecondition b ON a.studyrowsubset_id = b.studyrowsubset_id
                                 WHERE a.studyrowset_id = _studyrowset_id
                                   AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org)));
  
  -- if studyrowset has no conditions, return all studyrows
  IF _subset_is_empty THEN
    RETURN QUERY SELECT a.studyrow_id
                 FROM dashin.studydatatable a
                 WHERE a.study_id = _study_id
                   AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
    RETURN;
  END IF;
  
  -- generate query based on conditions
  WITH
    -- setup the basics, filter on 
    -- available conditions
    -- available stydvariables, datavariables
    -- check owner permissions
    base_query         AS (
                            SELECT a.studyrowset_id            AS a_studyrowset_id
                                 , a.study_id                  AS a_study_id
                                 , b.studyrowsubset_id         AS b_studyrowsubset_id
                                 , c.studyvariablecondition_id AS c_studyvariablecondition_id
                                 , d.conditionoperator_id      AS d_conditionoperator_id
                                 , d.txtval1                   AS d_txtval1
                                 , d.txtval2                   AS d_txtval2
                                 , e.studyvariable_id          AS e_studyvariable_id
                                 , e.ordinal                   AS e_ordinal
                                 , e.variabletype_id           AS e_variabletype_id
                            FROM dashin.studyrowset                                     a
                              INNER JOIN dashin.studyrowsubset                          b ON a.studyrowset_id = b.studyrowset_id
                              INNER JOIN dashin.studyrowsubset_x_studyvariablecondition c ON b.studyrowsubset_id = c.studyrowsubset_id
                              INNER JOIN dashin.studyvariablecondition                  d ON c.studyvariablecondition_id = d.studyvariablecondition_id
                              INNER JOIN dashin.studyvariable                           e ON d.studyvariable_id = e.studyvariable_id
                            WHERE a.studyrowset_id = _studyrowset_id
                              AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org))
                          )
    -- design variable conditions: samplingevent + samplingtime
    , dvars_smpt       AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'samplingtime_id IN (' || string_agg(quote_literal(x0.samplingtime_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , c.samplingtime_id
                                   FROM base_query                                                   a
                                     INNER JOIN dashin.studyvariablecondition_x_category             b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                     INNER JOIN dashin.studyvariablecondition_x_samplingtimecategory c ON a.c_studyvariablecondition_id = c.studyvariablecondition_id AND b.element_id = c.samplingevent_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('samplingevent' , 'samplingtime') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: event
    , dvars_event      AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'event_id IN (' || string_agg(quote_literal(x0.event_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS event_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('event') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: subevent
    , dvars_subevent   AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'subevent_id IN (' || string_agg(quote_literal(x0.subevent_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS subevent_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('subevent') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: subject
    , dvars_subject    AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'subject_id IN (' || string_agg(quote_literal(x0.subject_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS subject_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('subject') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: startgroup
    , dvars_startgroup AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'startgroup_id IN (' || string_agg(quote_literal(x0.startgroup_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS startgroup_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('startgroup') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: center
    , dvars_center     AS (
                            SELECT x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , 'center_id IN (' || string_agg(quote_literal(x0.center_id) , ',') || ')' AS where_cond
                            FROM (
                                   SELECT a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , b.element_id AS center_id
                                   FROM base_query                                       a
                                     INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                                   WHERE a.e_variabletype_id IN (
                                                                  SELECT z.variabletype_id
                                                                  FROM dashin.get_designvariabletype_ids('center') z
                                                                )
                                 ) x0
                            GROUP BY x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                          )
    -- design variable conditions: combine all design variable conditions in a single query
    , dvars_all        AS (
                            SELECT *
                            FROM dvars_smpt
                            UNION
                            SELECT *
                            FROM dvars_event
                            UNION
                            SELECT *
                            FROM dvars_subevent
                            UNION
                            SELECT *
                            FROM dvars_subject
                            UNION
                            SELECT *
                            FROM dvars_startgroup
                            UNION
                            SELECT *
                            FROM dvars_center
                          )
    -- combine design-var conditions for each studyrowsubset (intersect: AND)
    , dvars_subset     AS (
                            SELECT E'(\nSELECT studyrow_id FROM dashin.studydatatable \nWHERE ' || string_agg(a.where_cond , E' AND \n') || E' \n)' AS dvars_single_rowsubset_query
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                            FROM dvars_all a
                            GROUP BY a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                          )
    -- non-design-vars, basic setup
    , othbase_query    AS (
                            SELECT a.a_studyrowset_id
                                 , a.a_study_id
                                 , a.b_studyrowsubset_id
                                 , a.c_studyvariablecondition_id
                                 , a.d_conditionoperator_id
                                 , a.d_txtval1
                                 , a.d_txtval2
                                 , a.e_studyvariable_id
                                 , a.e_ordinal
                                 , a.e_variabletype_id
                                 , g.local_id   AS g_local_id
                                 , h.dataset_id AS h_dataset_id
                            FROM base_query                              a
                              INNER JOIN dashin.studyvariable_x_variable f ON a.e_studyvariable_id = f.studyvariable_id
                              INNER JOIN dashin.variable                 g ON f.variable_id = g.variable_id
                              INNER JOIN dashin.dataset                  h ON g.dataset_id = h.dataset_id
                            WHERE a.e_variabletype_id NOT IN (
                                                               SELECT z.variabletype_id
                                                               FROM dashin.get_designvariabletype_ids('all') z
                                                             )
                          )
    -- non-design-vars: extract all conditions
    , othvars_query    AS (
                            SELECT 'jsonb_array_element_text(datarow,' || a.g_local_id || ')'                        AS varvalue
                                 , quote_ident(a.h_dataset_id || '_' || a.g_local_id || '_' || row_number() OVER ()) AS varname
                                 , a.d_txtval1
                                 , a.d_txtval2
                                 , a.d_conditionoperator_id
                                 , a.g_local_id
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.h_dataset_id
                                 , a.c_studyvariablecondition_id
                                 , a.e_studyvariable_id
                            FROM othbase_query a
                          )
    -- non-design-vars: categorical conditions
    , othvars_category AS (
                            SELECT a.varname || ' IN (' || string_agg(DISTINCT quote_literal(b.value_column) , ',') || ')' AS cond_string
                                 , a.varvalue || ' AS ' || a.varname                                                       AS catvar
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.c_studyvariablecondition_id
                                 , a.e_studyvariable_id
                                 , a.h_dataset_id
                            FROM othvars_query                                    a
                              INNER JOIN dashin.studyvariablecondition_x_category b ON a.c_studyvariablecondition_id = b.studyvariablecondition_id
                            WHERE a.d_conditionoperator_id IN (
                                                                SELECT z.conditionoperator_id
                                                                FROM dashin.conditionoperator z
                                                                WHERE z.context_flag = 3
                                                              )
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                                   , a.c_studyvariablecondition_id
                                   , a.e_studyvariable_id
                                   , a.h_dataset_id
                                   , a.g_local_id
                                   , a.d_conditionoperator_id
                                   , b.studyvariablecondition_id
                                   , a.varname
                                   , a.varvalue
                          )
    -- non-design-vars: numeric conditions
    , othvars_numeric  AS (
                            SELECT E'text_to_numeric(' || a.varvalue || ') AS ' || a.varname AS numvar
                                 , CASE
                                     WHEN a.d_conditionoperator_id IN (1, 2, 3, 4, 5, 6) THEN a.varname || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id) || a.d_txtval1
                                     WHEN a.d_conditionoperator_id IN (7, 8, 9)          THEN
                                                   coalesce(text_to_numeric(a.d_txtval1) , 0) || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id , 'l') || a.varname || ' AND ' ||
                                                   a.varname || dashin.conditionoperator_sqlcode(a.d_conditionoperator_id , 'r') || coalesce(text_to_numeric(a.d_txtval2) , 0)
                                   END                                                       AS cond_string
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.h_dataset_id
                                 , a.e_studyvariable_id
                            FROM othvars_query a
                            WHERE a.d_conditionoperator_id IN (
                                                                SELECT z.conditionoperator_id
                                                                FROM dashin.conditionoperator z
                                                                WHERE z.context_flag = 2
                                                              )
                          )
    -- non-design-vars: condition-queries for each dataset
    , othvars_dataset  AS (
                            SELECT E'(\nSELECT x.studyrow_id \nFROM (\n SELECT b.studyrow_id\n,' ||
                                   string_agg(x0.selectvar , E'\n ,') ||
                                   E'\nFROM dashin.datatable a\n' ||
                                   E'INNER JOIN dashin.studyrow_x_datarow b ON a.datarow_id = b.datarow_id\n' ||
                                   E'WHERE a.dataset_id = ' || quote_literal(x0.h_dataset_id) ||
                                   E'\n) x\n WHERE ' || string_agg('(' || x0.cond_string || ')' , E' AND \n') || E'\n)' AS dataset_query
                                 , x0.a_study_id
                                 , x0.a_studyrowset_id
                                 , x0.b_studyrowsubset_id
                                 , x0.e_studyvariable_id
                                 , x0.h_dataset_id
                            FROM (
                                   SELECT a.catvar AS selectvar
                                        , a.cond_string
                                        , a.a_study_id
                                        , a.a_studyrowset_id
                                        , a.b_studyrowsubset_id
                                        , a.e_studyvariable_id
                                        , a.h_dataset_id
                                   FROM othvars_category a
                                   UNION
                                   SELECT b.numvar AS selectvar
                                        , b.cond_string
                                        , b.a_study_id
                                        , b.a_studyrowset_id
                                        , b.b_studyrowsubset_id
                                        , b.e_studyvariable_id
                                        , b.h_dataset_id
                                   FROM othvars_numeric b
                                 ) x0
                            GROUP BY x0.a_study_id
                                   , x0.a_studyrowset_id
                                   , x0.b_studyrowsubset_id
                                   , x0.e_studyvariable_id
                                   , x0.h_dataset_id
                          )
    -- non-design-vars: map dataset-rows to study-rows
    , othvars_studyvar AS (
                            SELECT E'SELECT stv.studyrow_id\n FROM (\n' || string_agg(a.dataset_query , E'\nUNION\n') || E'\n) stv' AS stvar_query
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                                 , a.e_studyvariable_id
                            FROM othvars_dataset a
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                                   , a.e_studyvariable_id
                          )
    -- non-design-vars: combine conditions within each subset (INTERSECT ALL)
    , othvars_subset   AS (
                            SELECT E'(\n' || string_agg(a.stvar_query , E'\nINTERSECT ALL\n') || E'\n)' AS ovars_single_rowsubset_query
                                 , a.a_study_id
                                 , a.a_studyrowset_id
                                 , a.b_studyrowsubset_id
                            FROM othvars_studyvar a
                            GROUP BY a.a_study_id
                                   , a.a_studyrowset_id
                                   , a.b_studyrowsubset_id
                          )
    -- combined all conditinos within each subset (INTERSECT ALL)
    , allvars_subset   AS (
                            SELECT a.b_studyrowsubset_id
                                 , a.dvars_single_rowsubset_query
                                 , b.ovars_single_rowsubset_query
                                 , CASE
                                     WHEN a.dvars_single_rowsubset_query IS NULL OR b.ovars_single_rowsubset_query IS NULL THEN
                                       CASE
                                         WHEN a.dvars_single_rowsubset_query IS NOT NULL THEN a.dvars_single_rowsubset_query
                                         ELSE b.ovars_single_rowsubset_query
                                       END
                                     ELSE a.dvars_single_rowsubset_query || E'\n INTERSECT ALL \n' || b.ovars_single_rowsubset_query
                                   END AS dovars_subset_query
      
                            FROM dvars_subset          a
                              FULL JOIN othvars_subset b
                                          ON a.b_studyrowsubset_id = b.b_studyrowsubset_id
                          )
    -- combine subsets into one query (UNION)
    , allvars_rowset   AS (
                            SELECT E'(\n' || string_agg(dovars_subset_query , E'\n)\n UNION \n(\n') || E'\n);\n' AS search_query
                            FROM allvars_subset
                          )
  SELECT search_query
  INTO result_query
  FROM allvars_rowset;
  IF result_query IS NULL THEN
    RETURN QUERY SELECT NULL::UUID;
  ELSE
    RETURN QUERY EXECUTE result_query;
  END IF;
  
  RETURN;

END;
$$;
