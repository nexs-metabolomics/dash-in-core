CREATE FUNCTION dashin.datadesign_design_variable_distinct
(
    _dataset_id      UUID
  , _variabletype_id INT
  , _owner_org       UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID      UUID,
            LOCAL_ID        INT,
            NAME            TEXT,
            VARIABLE_ID     UUID,
            VARIABLETYPE    TEXT,
            VARIABLETYPE_ID INT
          )
  LANGUAGE sql
AS
$$
SELECT DISTINCT dataset_id
              , local_id
              , jsonb_array_element_text(d.datarow , local_id) AS name
              , variable_id
              , t.name                                         AS variabletype
              , v.variabletype_id::INT
FROM dashin.variable            v
  LEFT JOIN dashin.datatable    d USING (dataset_id)
  LEFT JOIN dashin.variabletype t USING (variabletype_id)
WHERE variable_id = (
                      SELECT variable_id
                      FROM
                        dashin.datadesign_design_variablenames(_dataset_id , _owner_org)
                      WHERE variabletype_id = _variabletype_id
                    )
ORDER BY name
$$
;


ALTER FUNCTION dashin.datadesign_design_variable_distinct(_dataset_id UUID, _variabletype_id INT, _owner_org UUID) OWNER TO :application_user;

