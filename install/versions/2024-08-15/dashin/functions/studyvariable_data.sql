CREATE FUNCTION dashin.studyvariable_data
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID   UUID,
            DATATYPE_ID  INT,
            VALUE_COLUMN TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  value_query TEXT;
BEGIN
  value_query := dashin.studyvariable_data_generate_query(_studyvariable_id , _study_id , _owner_org , _limit);
  IF value_query IS NOT NULL THEN
    RETURN QUERY EXECUTE value_query;
    RETURN;
  END IF;
  RETURN QUERY SELECT NULL::UUID, NULL::INT, NULL::TEXT;
END;
$$;

COMMENT ON FUNCTION dashin.studyvariable_data(_studyvariable_id UUID, _study_id UUID, _owner_org UUID, _limit INT) IS E'Extract data from dataset. Do not take designvariable information into account.\n'
  'Typically used in pagination queries where _limit is the pagination limit\n'
  '  @PARAM _studyvariable_id UUID\n'
  '  @PARAM _study_id UUID\n'
  '  @PARAM _owner_org UUID, organization_id for the owning organization\n'
  '  @PARAM _limit INT, number of rows to return\n'
  '  @RETURN table(datarow_id UUID, datatype_id INT, value_column TEXT';
