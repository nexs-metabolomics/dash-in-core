CREATE FUNCTION dashin.studydesign_import_design_from_data
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            NAME          TEXT,
            AFFECTED_ROWS INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  study_exists                      BOOL;
  dataset_exists                    BOOL;
  studydesign_exists                BOOL;
  is_allowed                        BOOL;
  datadesign_count                  INT;
  studydesign_x_datadesign          INT;
  event_count                       INT;
  subevent_count                    INT;
  samplingevent_count               INT;
  samplingtime_count                INT;
  center_count                      INT;
  startgroup_count                  INT;
  subject_count                     INT;
  event_x_subevent_count            INT;
  subevent_x_samplingevent_count    INT;
  event_subevent_x_startgroup_count INT;
BEGIN
  datadesign_count := (
                        SELECT count(*)
                        FROM dashin.datadesign_datatable dt
                        WHERE dt.dataset_id = _dataset_id
                      );
  IF datadesign_count = 0 THEN
    RETURN QUERY
      VALUES ('empty', 0);
    RETURN;
  END IF;
  
  studydesign_exists := exists(SELECT
                               FROM dashin.studydesign_x_datadesign x
                               WHERE study_id = _study_id);
  
  study_exists := exists(SELECT
                         FROM dashin.study s
                         WHERE (s.study_id, s.owner_org) = (_study_id, _owner_org));
  
  dataset_exists := exists(SELECT
                           FROM dashin.dataset d
                           WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  is_allowed := (
                  SELECT dataset_exists AND study_exists
                );
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- STUDY, DATASET PERMISSIONS
  -- ----------------------------------------------------------------------------------------------------------------------------
  IF NOT is_allowed THEN
    RETURN QUERY
      VALUES ('denied', 0)
           , ('study', study_exists::INT)
           , ('dataset', dataset_exists::INT)
           , ('studydesign', study_exists::INT)
           , ('allowed', is_allowed::INT);
    RETURN;
  END IF;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- DESIGN EXISTS
  -- ----------------------------------------------------------------------------------------------------------------------------
  IF NOT studydesign_exists THEN
    INSERT INTO dashin.studydesign_x_datadesign (study_id, dataset_id)
    VALUES (_study_id, _dataset_id)
    ON CONFLICT DO NOTHING;
    GET DIAGNOSTICS studydesign_x_datadesign = ROW_COUNT;
  END IF;
  
  IF studydesign_x_datadesign != 1 OR studydesign_exists THEN
    RETURN QUERY
      VALUES ('exists', 0);
    RETURN;
  END IF;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event (event_id, study_id, name, description)
  SELECT DISTINCT ON (dt.event_name) uuid_generate_v4()
                                    , _study_id
                                    , dt.event_name
                                    , 'Imported from data' || CASE
                                                                WHEN dt.event_status = 100 THEN ' (Auto-generated)'
                                                                ELSE ''
                                                              END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subevent (subevent_id, subevent_type_id, study_id, intervention_type_id, name, description)
  SELECT DISTINCT ON (dt.subevent_name) uuid_generate_v4()
                                       , 0
                                       , _study_id
                                       , 0
                                       , dt.subevent_name
                                       , 'Imported from data' || CASE
                                                                   WHEN dt.subevent_status = 100 THEN ' (Auto-generated)'
                                                                   ELSE ''
                                                                 END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SAMPLINGEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_samplingevent (samplingevent_id, sampling_type_id, study_id, name, description)
  SELECT DISTINCT ON (dt.samplingevent_name) uuid_generate_v4()
                                            , 0
                                            , _study_id
                                            , dt.samplingevent_name
                                            , 'Imported from data' || CASE
                                                                        WHEN dt.samplingevent_status = 100 THEN ' (Auto-generated)'
                                                                        ELSE ''
                                                                      END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS samplingevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SAMPLINGTIME
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_samplingtime (samplingtime_id, samplingevent_id, ordinal_time, name, description)
  SELECT uuid_generate_v4() AS samplingtime_id
       , samplingevent_id
       , row_number() OVER (PARTITION BY samplingevent_id ORDER BY samplingtime_name)
       , samplingtime_name
       , description
  FROM (
         SELECT DISTINCT ON (dt.samplingevent_name,dt.samplingtime_name) sm.samplingevent_id
                                                                          , dt.samplingtime_name
                                                                          , 'Imported from data' || CASE
                                                                                                      WHEN dt.samplingtime_status = 100 THEN ' (Auto-generated)'
                                                                                                      ELSE ''
                                                                                                    END AS description
         FROM dashin.datadesign_datatable       dt
           LEFT JOIN dashin.study_samplingevent sm ON dt.samplingevent_name = sm.name
         WHERE (dt.dataset_id, sm.study_id) = (_dataset_id, _study_id)
       ) x0
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS samplingtime_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- CENTER
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_center (center_id, study_id, name, description)
  SELECT DISTINCT ON (dt.center_name) uuid_generate_v4()
                                     , _study_id
                                     , dt.center_name
                                     , 'Imported from data' || CASE
                                                                 WHEN dt.center_status = 100 THEN ' (Auto-generated)'
                                                                 ELSE ''
                                                               END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS center_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- STARTGROUP
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_startgroup (startgroup_id, study_id, name, description)
  SELECT DISTINCT ON (dt.startgroup_name) uuid_generate_v4()
                                         , _study_id
                                         , dt.startgroup_name
                                         , 'Imported from data' || CASE
                                                                     WHEN dt.startgroup_status = 100 THEN ' (Auto-generated)'
                                                                     ELSE ''
                                                                   END AS description
  FROM dashin.datadesign_datatable dt
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS startgroup_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBJECT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subject (subject_id, study_id, center_id, startgroup_id, name, description)
  SELECT DISTINCT ON (dt.subject_name,dt.startgroup_name) uuid_generate_v4()
                                                           , _study_id
                                                           , sc.center_id
                                                           , sg.startgroup_id
                                                           , dt.subject_name
                                                           , 'Imported from data' || CASE
                                                                                       WHEN dt.subject_status = 100 THEN ' (Auto-generated)'
                                                                                       ELSE ''
                                                                                     END AS description
  FROM dashin.datadesign_datatable     dt
    INNER JOIN dashin.study_center sc ON (sc.study_id, sc.name) = (_study_id, dt.center_name)
    INNER JOIN dashin.study_startgroup sg ON (sg.study_id, sg.name) = (_study_id, dt.startgroup_name)
  WHERE dt.dataset_id = _dataset_id
  
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subject_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT_X_SUBEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event_x_subevent (event_id, subevent_id, study_id)
  SELECT DISTINCT ev.event_id
                , se.subevent_id
                , _study_id
  FROM dashin.datadesign_datatable   dt
    INNER JOIN dashin.study_event    ev ON (ev.study_id, ev.name) = (_study_id, dt.event_name)
    INNER JOIN dashin.study_subevent se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_x_subevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- SUBEVENT_X_SAMPLINGEVENT
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_subevent_x_samplingevent (subevent_id, samplingevent_id, study_id)
  SELECT DISTINCT se.subevent_id
                , sm.samplingevent_id
                , _study_id
  FROM dashin.datadesign_datatable        dt
    INNER JOIN dashin.study_subevent      se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
    INNER JOIN dashin.study_samplingevent sm ON (sm.study_id, sm.name) = (_study_id, dt.samplingevent_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS subevent_x_samplingevent_count = ROW_COUNT;
  
  -- ----------------------------------------------------------------------------------------------------------------------------
  -- EVENT_SUBEVENT_X_STARTGROUP
  -- ----------------------------------------------------------------------------------------------------------------------------
  INSERT INTO dashin.study_event_subevent_x_startgroup (event_id, subevent_id, study_id, startgroup_id)
  SELECT DISTINCT ev.event_id
                , se.subevent_id
                , _study_id
                , sg.startgroup_id
  FROM dashin.datadesign_datatable     dt
    INNER JOIN dashin.study_event      ev ON (ev.study_id, ev.name) = (_study_id, dt.event_name)
    INNER JOIN dashin.study_subevent   se ON (se.study_id, se.name) = (_study_id, dt.subevent_name)
    INNER JOIN dashin.study_startgroup sg ON (sg.study_id, sg.name) = (_study_id, dt.startgroup_name)
  WHERE dt.dataset_id = _dataset_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS event_subevent_x_startgroup_count = ROW_COUNT;
  
  RETURN QUERY VALUES ('design_count', datadesign_count)
                    , ('event_count', event_count)
                    , ('subevent_count', subevent_count)
                    , ('samplingevent_count', samplingevent_count)
                    , ('samplingtime_count', samplingtime_count)
                    , ('center_count', center_count)
                    , ('startgroup_count', startgroup_count)
                    , ('subject_count', subject_count)
                    , ('event_x_subevent_count', event_x_subevent_count)
                    , ('subevent_x_samplingevent_count', subevent_x_samplingevent_count)
                    , ('event_subevent_x_startgroup_count', event_subevent_x_startgroup_count);
  RETURN;
END;
$$;
