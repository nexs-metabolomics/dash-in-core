CREATE FUNCTION dashin.studyvariable_guess_datatype
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT 10
) RETURNS INT
  LANGUAGE plpgsql
AS
$$
DECLARE
  -- --------------------------------------
  evaluation_result         BOOL DEFAULT FALSE;
  -- --------------------------------------
  datatype_text    CONSTANT INT :=       1;
  datatype_numeric CONSTANT INT :=       2;
  -- --------------------------------------
BEGIN
  -- check numeric
  evaluation_result := (
                         SELECT TRUE = ALL (array_agg(utils.text_is_numeric(value_column)))
                         FROM dashin.studyvariable_data(_studyvariable_id , _study_id , _owner_org , _limit)
                       );
  IF evaluation_result THEN
    RETURN datatype_numeric;
  ELSE
    RETURN datatype_text;
  END IF;
END;
$$;
