CREATE FUNCTION dashin.search_generate_column_query_numeric
(
  _column_type_id INT
) RETURNS TEXT
  VOLATILE
  LANGUAGE sql
AS
$$
SELECT E'SELECT\n subq.variable_id\n' ||
       E', subq.val\n' ||
       E'FROM(\n' || string_agg(qry , E'\nUNION ALL\n') || E') subq\n'
FROM (
       SELECT E'SELECT variable_id, val \n' ||
              E'FROM dashin.var_datatable\n' ||
              E', text_to_numeric(jsonb_array_element_text(datarow,' || local_id || E')) x(val)\n' ||
              E'WHERE var_dataset_id = ' || quote_literal(a.var_dataset_id) ||
              E'\n' AS qry
       FROM dashin.var_variable_x_search_column_type a
         INNER JOIN dashin.var_variable              b ON a.var_variable_id = b.var_variable_id
         INNER JOIN dashin.var_dataset               c ON a.var_dataset_id = c.var_dataset_id
         INNER JOIN dashin.dataset_x_study           d ON c.dataset_id = d.dataset_id
       WHERE a.search_column_type_id = _column_type_id
     ) x0;
$$;

ALTER FUNCTION dashin.search_generate_column_query_numeric OWNER TO :application_user;
