/*********************************************************************
 * dash-in datasharing
 * 
 * Copyright (c) 2017-2019 dash-in team NEXS Copenhage University
 * 
 * Created 2019-10-25
 * 
 * This file is part of the 'dash-in datasharing' project.
 *
 * This source file is subject to the New BSD License that is bundled
 * with this package in the file LICENSE.txt.
 * 
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to fsp@nexs.ku.dk so we can send you a copy immediately.
 * 
 * Author: Finn Sandø <fsp@nexs.ku.dk>
 *********************************************************************
*/
\echo '-------------------------------------------------------------------------------------'
\echo ' SET ADMIN TABLES CONSTRAINTS'
\echo '-------------------------------------------------------------------------------------'

ALTER TABLE admin.manager ADD CONSTRAINT manager_user_id_fkey FOREIGN KEY (user_id)
  REFERENCES admin.users ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE admin.manager ADD CONSTRAINT manager_organization_id_fkey FOREIGN KEY (organization_id)
  REFERENCES admin.organization ON UPDATE CASCADE ON DELETE CASCADE;

-- ALTER TABLE admin.mgr_x_mgr ADD CONSTRAINT mgr_x_mgr_organization_id_fkey FOREIGN KEY (organization_id)
--   REFERENCES admin.organization ON UPDATE CASCADE ON DELETE CASCADE;
-- ALTER TABLE admin.mgr_x_mgr ADD CONSTRAINT mgr_x_mgr_child_fkey FOREIGN KEY (organization_id, child_id)
--   REFERENCES admin.manager ON UPDATE CASCADE ON DELETE CASCADE;
-- ALTER TABLE admin.mgr_x_mgr ADD CONSTRAINT mgr_x_mgr_parent_fkey FOREIGN KEY (organization_id, parent_id)
--   REFERENCES admin.manager ON UPDATE CASCADE ON DELETE CASCADE;
-- ALTER TABLE admin.mgr_x_mgr ADD CONSTRAINT mgr_x_mgr_key UNIQUE (organization_id, child_id);

ALTER TABLE admin.roleacl ADD CONSTRAINT roleacl_role_id_fkey FOREIGN KEY (role_id)
  REFERENCES admin.role ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE admin.template ADD CONSTRAINT template_template_type_id_fkey FOREIGN KEY (template_type_id)
  REFERENCES admin.template_type ON UPDATE CASCADE;

-- ALTER TABLE admin.user_x_user ADD CONSTRAINT user_x_user_origin_user_id_fkey FOREIGN KEY (origin_user_id)
--   REFERENCES admin.users (user_id);
-- ALTER TABLE admin.user_x_user ADD CONSTRAINT user_x_user_target_user_id_fkey FOREIGN KEY (target_user_id)
--   REFERENCES admin.users (user_id);

ALTER TABLE admin.users ADD CONSTRAINT users_active_org_id_fkey FOREIGN KEY (active_org_id)
  REFERENCES admin.organization (organization_id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE admin.users ADD CONSTRAINT users_active_manager_org_user_fkey FOREIGN KEY (active_org_id, user_id)
  REFERENCES admin.manager;

\echo ' SET ADMIN TABLES CONSTRAINTS DONE'
