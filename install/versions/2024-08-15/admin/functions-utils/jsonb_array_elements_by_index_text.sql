CREATE FUNCTION utils.jsonb_array_elements_by_index_text
(
  arr  JSONB
, idxs JSONB
) RETURNS SETOF TEXT
  LANGUAGE SQL
AS
$$
SELECT
    arr ->> (n :: INT)
FROM
  jsonb_array_elements_text(idxs) AS n
$$;

ALTER FUNCTION utils.jsonb_array_elements_by_index_text(arr JSONB, idxs JSONB) OWNER TO :application_user;
