CREATE FUNCTION utils.jsonb_array_elements_by_index
(
  arr  JSONB
, idxs JSONB
) RETURNS SETOF JSONB
  LANGUAGE SQL
AS
$$
SELECT
    arr -> (n::INT)
FROM
  jsonb_array_elements_text(idxs) AS n
$$;

ALTER FUNCTION utils.jsonb_array_elements_by_index(arr JSONB, idxs JSONB) OWNER TO :application_user;
