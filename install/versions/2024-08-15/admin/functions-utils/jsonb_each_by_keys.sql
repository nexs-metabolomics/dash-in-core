CREATE FUNCTION utils.jsonb_each_by_keys
(
  obj  JSONB
, keys JSONB
) RETURNS SETOF JSONB
  LANGUAGE SQL
AS
$$
SELECT
    obj -> n
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_each_by_keys(obj JSONB, keys JSONB) OWNER TO :application_user;
