CREATE TABLE admin.roleacl
(
  roleacl_id UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT roleacl_pkey
      PRIMARY KEY,
  name TEXT,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  role_id INTEGER NOT NULL,
  namespace TEXT NOT NULL,
  class_allow JSONB DEFAULT '["*"]'::JSONB NOT NULL,
  class_deny JSONB DEFAULT '[]'::JSONB NOT NULL,
  action_allow JSONB DEFAULT '["*"]'::JSONB NOT NULL,
  action_deny JSONB DEFAULT '[]'::JSONB NOT NULL,
  CONSTRAINT roleacl_key
    UNIQUE (role_id, namespace, class_allow, class_deny, action_allow, action_deny)
);

ALTER TABLE admin.roleacl OWNER TO :application_user;
