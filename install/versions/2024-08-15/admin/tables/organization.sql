CREATE TABLE admin.organization
(
  organization_id UUID NOT NULL
    CONSTRAINT organization_pkey
      PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT,
  email TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  status INTEGER DEFAULT 1 NOT NULL,
  roles JSONB DEFAULT '[]'::JSONB NOT NULL,
  details JSONB
);

ALTER TABLE admin.organization OWNER TO :application_user;
