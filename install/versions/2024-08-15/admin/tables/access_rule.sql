CREATE TABLE admin.access_rule
(
  access_rule_id  SERIAL NOT NULL
    CONSTRAINT access_rule_pkey
      PRIMARY KEY,
  access_rule_key TEXT NOT NULL,
  name            TEXT NOT NULL,
  description     TEXT,
  namespace       TEXT NOT NULL,
  class           TEXT NOT NULL,
  allow           JSONB DEFAULT '[]'::JSONB,
  deny            JSONB DEFAULT '[]'::JSONB,
  details         JSONB
);

ALTER TABLE admin.access_rule OWNER TO :application_user;

