CREATE TABLE admin.dbversion
(
  version_string TEXT PRIMARY KEY,
  updated_at     TIMESTAMPTZ DEFAULT now()
);

ALTER TABLE admin.dbversion OWNER TO :application_user;
