CREATE OR REPLACE FUNCTION admin.get_roles
(
  _roles    JSONB -- jsonb array of roles from manager
, _col_id   TEXT DEFAULT 'role_id' -- role_id column
, _col_incl TEXT DEFAULT 'roles_include' -- roles_include
, _col_excl TEXT DEFAULT 'roles_exclude' -- roles_exclude
, _tbl_name TEXT DEFAULT 'admin.role' -- table name with schema
, _tbl      REGCLASS DEFAULT 'admin.role' -- table name with schema
)
  RETURNS JSONB
  LANGUAGE plpgsql
  IMMUTABLE
AS
$$
DECLARE result JSONB;
BEGIN
  EXECUTE (FORMAT(
          E'SELECT jsonb_agg(x) FROM (SELECT DISTINCT\n' ||
          E'jsonb_array_elements(admin.get_role_with_children(%I,''%s'',''%s'',''%s'',''%s'')) AS x\n' ||
          E'FROM %s\n' ||
          E'WHERE to_jsonb(%I) <@ ($1)::JSONB)sq;'
    , _col_id , _col_id , _col_incl , _col_excl , _tbl_name
    , _tbl
    , _col_id
    )) USING _roles INTO result;
  RETURN result;
END;
$$;

ALTER FUNCTION admin.get_roles(_roles JSONB, _col_id TEXT, _col_incl TEXT, _col_excl TEXT, _tbl_name TEXT, _tbl REGCLASS) OWNER TO :application_user;
