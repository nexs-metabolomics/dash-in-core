CREATE OR REPLACE FUNCTION admin.get_manager_roles
(
  _organization_id UUID
, _user_id         UUID
)
  RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  utils.jsonb_array_a_intersect_b(admin.get_roles(m.roles) , admin.get_roles(o.roles)) AS role_id
FROM
  admin.manager                  m
    LEFT JOIN admin.organization o USING (organization_id)
WHERE organization_id = _organization_id
  AND user_id = _user_id
$$;

ALTER FUNCTION admin.get_manager_roles(UUID, _user_id UUID) OWNER TO :application_user;
