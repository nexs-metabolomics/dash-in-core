CREATE OR REPLACE FUNCTION admin.get_role_with_children
(
  _x        INT
, _col_id   TEXT DEFAULT 'role_id'
, _col_incl TEXT DEFAULT 'roles_include'
, _col_excl TEXT DEFAULT 'roles_exclude'
, _tbl_name REGCLASS DEFAULT 'admin.role'
)
  RETURNS JSONB
  STABLE
  LANGUAGE plpgsql
AS
$$
DECLARE result JSONB;
BEGIN
  EXECUTE (format(
                        E'SELECT\n' ||
                        E'  jsonb_array_a_union_b (\n' ||
                        E'    CASE\n' ||
                        E'      WHEN %I @> to_jsonb(0) THEN\n' ||
                        E'        jsonb_array_a_minus_b((SELECT jsonb_agg(%I) FROM (SELECT %I FROM %s WHERE %I NOT IN (0,$1))x),%I)\n' ||
                        E'      ELSE %I\n' ||
                        E'    END\n' ||
                        E'    , to_jsonb(''{}''::int[] || %I)\n' ||
                        E'  ) AS roles\n' ||
                        E'FROM %s\n' ||
                        E'WHERE %I = $1'
    , _col_incl
    , _col_id , _col_id , _tbl_name , _col_id , _col_excl
    , _col_incl
    , _col_id
    , _tbl_name
    , _col_id
    )) USING _x INTO result;
  RETURN result;
END;
$$;

ALTER FUNCTION admin.get_role_with_children(_x INT, _col_id TEXT, _col_incl TEXT, _col_excl TEXT, _tbl_name REGCLASS) OWNER TO :application_user;
