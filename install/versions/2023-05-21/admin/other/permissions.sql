GRANT SELECT ON ALL TABLES IN SCHEMA admin TO :application_user;
GRANT USAGE ON SCHEMA admin TO :application_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA admin GRANT SELECT ON TABLES TO :application_user;

GRANT USAGE ON SCHEMA utils TO :application_user;

ls