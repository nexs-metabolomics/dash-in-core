CREATE TABLE admin.role
(
  role_id INTEGER NOT NULL
    CONSTRAINT role_pkey
      PRIMARY KEY,
  name TEXT
    CONSTRAINT role_name_key
      UNIQUE,
  description TEXT,
  label TEXT,
  long_desc TEXT,
  details JSONB,
  status INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  roles_include JSONB DEFAULT '[]'::JSONB NOT NULL,
  roles_exclude JSONB DEFAULT '[]'::JSONB NOT NULL
);

ALTER TABLE admin.role OWNER TO :application_user;
