CREATE FUNCTION utils.text_is_numeric
(
  _x TEXT
)
  RETURNS BOOL
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE
    x  NUMERIC;
    x1 TEXT;
BEGIN
    x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
             WHEN _x ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(_x , '.' , '') , ',' , '.')
        -- comma dot or comma comma -> comma is thousand separator, remove comma
             WHEN _x ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(_x , ',' , '')
        -- else replace comma with dot
             ELSE replace(_x , ',' , '.')
         END;
    x = x1:: NUMERIC;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RETURN FALSE;
END;
$$;
