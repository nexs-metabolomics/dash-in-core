CREATE FUNCTION utils.jsonb_text_to_numeric
(
  _x             JSONB
, _empty_as_null BOOLEAN DEFAULT TRUE
) RETURNS JSONB
  IMMUTABLE
  STRICT
  LANGUAGE plpgsql
AS
$$
DECLARE x  NUMERIC;
        x0 TEXT;
        x1 TEXT;
BEGIN
  x0 = _x #>> '{}';
  x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
    WHEN x0 = '' AND _empty_as_null      THEN NULL
    WHEN x0 ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(x0 , '.' , '') , ',' , '.')
    -- comma dot or comma comma -> comma is thousand separator, remove comma
    WHEN x0 ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(x0 , ',' , '')
    -- else replace comma with dot
    ELSE replace(x0 , ',' , '.')
       END;
  x = x1:: NUMERIC;
  RETURN to_jsonb(x);
EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
END;
$$;

ALTER FUNCTION utils.jsonb_text_to_numeric(_x JSONB, _empty_as_null BOOLEAN) OWNER TO :application_user;
