CREATE TABLE dashin.upvar_datatable
(
  upvar_datarow_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_datatable_pk
      PRIMARY KEY,
  upvar_dataset_id UUID NOT NULL,
  variable_id      UUID,
  name             TEXT,
  description      TEXT,
  ordinal          INTEGER NOT NULL,
  status           INT DEFAULT 1,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB,
  datarow          JSONB
);

ALTER TABLE dashin.upvar_datatable OWNER TO :application_user;
