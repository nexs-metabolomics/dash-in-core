CREATE TABLE dashin.studyvariable
(
  studyvariable_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id         UUID NOT NULL,
  ordinal          INT,
  name             TEXT NOT NULL,
  label            TEXT,
  description      TEXT,
  datatype_id      INT,
  variabletype_id  INT,
  CONSTRAINT studyvariable_uk UNIQUE (study_id, name),
  -- needed by fk from studyrowset_x_studyvariable (prevents studyvar from wrong study)
  CONSTRAINT studyvariable_study_id_studyvariable_id_uk UNIQUE (study_id, studyvariable_id)
);

ALTER TABLE dashin.studyvariable OWNER TO :application_user;
