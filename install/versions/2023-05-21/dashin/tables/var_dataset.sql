CREATE TABLE dashin.var_dataset
(
  var_dataset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT var_dataset_pk
      PRIMARY KEY,
  dataset_id     UUID UNIQUE,
  name           TEXT,
  description    TEXT,
  status         INT DEFAULT 1,
  created_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details        JSONB,
  CONSTRAINT var_dataset_dataset_id_var_dataset_id_key UNIQUE (var_dataset_id, dataset_id)
);

ALTER TABLE dashin.var_dataset OWNER TO :application_user;
