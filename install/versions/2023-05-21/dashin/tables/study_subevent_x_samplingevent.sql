CREATE TABLE dashin.study_subevent_x_samplingevent
(
  subevent_id      UUID NOT NULL,
  samplingevent_id UUID NOT NULL,
  study_id         UUID NOT NULL,
  CONSTRAINT study_subevent_x_samplingevent_pk
    PRIMARY KEY (subevent_id, samplingevent_id)
);

ALTER TABLE dashin.study_subevent_x_samplingevent OWNER TO :application_user;
