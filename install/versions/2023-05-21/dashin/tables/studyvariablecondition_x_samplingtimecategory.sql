CREATE TABLE dashin.studyvariablecondition_x_samplingtimecategory
(
  studyvariablecondition_id UUID,
  study_id                  UUID,
  samplingevent_id          UUID,
  samplingtime_id           UUID,
  value_column              TEXT,
  CONSTRAINT studyvariablecondition_x_stimecategory_var_id_element_id_uk
    UNIQUE (studyvariablecondition_id, samplingtime_id)
);

ALTER TABLE dashin.studyvariablecondition_x_samplingtimecategory OWNER TO :application_user;
