CREATE TABLE dashin.studyexportset
(
    studyexportset_id   UUID CONSTRAINT studyexportset_pk PRIMARY KEY DEFAULT uuid_generate_v4(),
    study_id            UUID,
    studyrowset_id      UUID,
    studyvariableset_id UUID,
    name                TEXT,
    description         TEXT,
    created_at          TIMESTAMPTZ DEFAULT now(),
    updated_at          TIMESTAMPTZ DEFAULT now()
);

ALTER TABLE dashin.studyexportset OWNER TO :application_user;
