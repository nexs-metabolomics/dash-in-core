CREATE TABLE dashin.datadesign_x_study_center
(
  dataset_id           UUID,
  datadesign_center_id UUID,
  study_id             UUID,
  study_center_id      UUID,
  CONSTRAINT d_x_s_center_pk PRIMARY KEY (datadesign_center_id),
  CONSTRAINT d_x_s_center_uk UNIQUE (dataset_id, study_center_id)
);

ALTER TABLE dashin.datadesign_x_study_center OWNER TO :application_user;
