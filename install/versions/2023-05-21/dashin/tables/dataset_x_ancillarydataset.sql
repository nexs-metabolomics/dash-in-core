-- ----------------------------------------------------------------
-- table to link ancillary dataset to primary dataset
-- ----------------------------------------------------------------
CREATE TABLE IF NOT EXISTS dashin.dataset_x_ancillarydataset
(
    dataset_id          UUID,
    ancillarydataset_id UUID,
    CONSTRAINT dataset_neq_ancillarydataset_chk CHECK (dataset_id != ancillarydataset_id)
);

ALTER TABLE dashin.dataset_x_ancillarydataset OWNER TO :application_user;

