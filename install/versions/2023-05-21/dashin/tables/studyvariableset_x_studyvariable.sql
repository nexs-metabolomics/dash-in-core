CREATE TABLE dashin.studyvariableset_x_studyvariable
(
    studyvariableset_id UUID,
    studyvariable_id    UUID,
    study_id            UUID,
    CONSTRAINT studyvariableset_x_studyvariable_pk PRIMARY KEY (studyvariableset_id, studyvariable_id)
);

ALTER TABLE dashin.studyvariableset_x_studyvariable OWNER TO :application_user;
