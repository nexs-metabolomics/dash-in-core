CREATE TABLE dashin.study_samplingtime
(
  samplingtime_id  UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_samplingtime_pk
      PRIMARY KEY,
  samplingevent_id UUID NOT NULL,
  ordinal_time     INTEGER DEFAULT 0,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name             TEXT DEFAULT '0'::TEXT,
  label            TEXT DEFAULT '0'::TEXT,
  description      TEXT,
  CONSTRAINT study_samplingtime_samplingevent_name_uk UNIQUE (samplingevent_id,name)
);

ALTER TABLE dashin.study_samplingtime OWNER TO :application_user;

