CREATE TABLE IF NOT EXISTS dashin.datasettype
(
  datasettype_id         INT CONSTRAINT datasettype_pk PRIMARY KEY,
  datasettypecategory_id INT,
  name                   TEXT,
  description            TEXT
);

ALTER TABLE dashin.datasettype OWNER TO :application_user;
