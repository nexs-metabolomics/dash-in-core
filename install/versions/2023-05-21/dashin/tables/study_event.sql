CREATE TABLE dashin.study_event
(
  event_id    UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_event_pk
      PRIMARY KEY,
  study_id    UUID,
  ordinal     INT,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name        TEXT,
  label       TEXT,
  description TEXT,
  CONSTRAINT study_event_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_event OWNER TO :application_user;
