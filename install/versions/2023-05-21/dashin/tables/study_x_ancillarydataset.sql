CREATE TABLE IF NOT EXISTS dashin.study_x_ancillarydataset
(
  study_id   UUID,
  dataset_id UUID
);
ALTER TABLE dashin.study_x_ancillarydataset OWNER TO :application_user;