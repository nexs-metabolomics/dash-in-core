CREATE TABLE dashin.unitdomain
(
  unitdomain_id INT PRIMARY KEY,
  ordinal       INT,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.unitdomain OWNER TO :application_user;
