-- ----------------------------------------------------------------
-- Create table for different categories of datasettypes
-- ----------------------------------------------------------------
CREATE TABLE IF NOT EXISTS dashin.datasettypecategory
(
  datasettypecategory_id INT CONSTRAINT datasettypecategory_pk PRIMARY KEY,
  name                   TEXT,
  description            TEXT
);

ALTER TABLE dashin.datasettypecategory OWNER TO :application_user;
