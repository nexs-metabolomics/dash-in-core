CREATE TABLE dashin.studydesign_x_datadesign
(
  study_id   UUID REFERENCES dashin.study ON DELETE CASCADE ON UPDATE CASCADE,
  dataset_id UUID,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT studydesign_import_from_data_pk PRIMARY KEY (study_id, dataset_id)
);

ALTER TABLE dashin.studydesign_x_datadesign OWNER TO :application_user;
