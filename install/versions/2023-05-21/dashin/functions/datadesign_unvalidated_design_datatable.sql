CREATE FUNCTION dashin.datadesign_unvalidated_design_datatable
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID           UUID,
            DATASET_ID           UUID,
            EVENT_ID             UUID,
            SUBEVENT_ID          UUID,
            SAMPLINGEVENT_ID     UUID,
            SAMPLINGTIME_ID      UUID,
            CENTER_ID            UUID,
            STARTGROUP_ID        UUID,
            SUBJECT_ID           UUID,
            EVENT_NAME           TEXT,
            SUBEVENT_NAME        TEXT,
            SAMPLINGEVENT_NAME   TEXT,
            SAMPLINGTIME_NAME    TEXT,
            CENTER_NAME          TEXT,
            STARTGROUP_NAME      TEXT,
            SUBJECT_NAME         TEXT,
            EVENT_STATUS         INT,
            SUBEVENT_STATUS      INT,
            SAMPLINGEVENT_STATUS INT,
            SAMPLINGTIME_STATUS  INT,
            CENTER_STATUS        INT,
            STARTGROUP_STATUS    INT,
            SUBJECT_STATUS       INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  overall_n  INT;
  distinct_n INT;
BEGIN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- DISTINCT DESIGN VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE design_distinct
  (
    datarow_id           UUID,
    dataset_id           UUID,
    
    event_name           TEXT,
    subevent_name        TEXT,
    samplingevent_name   TEXT,
    samplingtime_name    TEXT,
    center_name          TEXT,
    startgroup_name      TEXT,
    subject_name         TEXT,
    
    event_status         INT DEFAULT 1,
    subevent_status      INT DEFAULT 1,
    samplingevent_status INT DEFAULT 1,
    samplingtime_status  INT DEFAULT 1,
    center_status        INT DEFAULT 1,
    startgroup_status    INT DEFAULT 1,
    subject_status       INT DEFAULT 1
  )
    ON COMMIT DROP;
  INSERT INTO design_distinct (event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT DISTINCT jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK DUPLICATES
  -- ---------------------------------------------------------------------------------------------------------------------
  
  -- @formatter:off
  -- -------------------------------
  -- distinct count
  -- -------------------------------
  distinct_n := (SELECT count(*) FROM design_distinct);
  
  IF distinct_n = 0 THEN
    RETURN QUERY VALUES (NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::UUID,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::TEXT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT,
                         NULL::INT
    ) LIMIT 0;
    RETURN;
  END IF;
  
  -- -------------------------------
  -- overall count
  -- -------------------------------
  overall_n := (SELECT count(*) FROM dashin.datatable dt WHERE dt.dataset_id = _dataset_id AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id,d.owner_org) = (_dataset_id,_owner_org)));
  -- @formatter:on

  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK MISSING VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE check_missing
  (
    name       TEXT,
    missing    INT,
    status     INT,
    statustext TEXT
  )
    ON COMMIT DROP;
  -- -------------------------------------------
  -- status:
  --  0: mixed (missing and non-missing)
  --  1: all missing
  --  2: all non-missing
  -- -------------------------------------------
  INSERT INTO check_missing
  SELECT c.name
       , c.missing
       , CASE
           WHEN c.missing = 0 THEN 2
           ELSE ((distinct_n - c.missing) = 0)::INT
         END AS status
       , CASE
           WHEN c.missing = 0                  THEN 'All non-missing'
           WHEN ((distinct_n - c.missing) = 0) THEN 'All missing'
           ELSE 'Mixed missing and non-missing'
         END AS description
       -- @formatter:off
  FROM (
         (SELECT 'event'         AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.event_name         IS NULL OR d.event_name         = '')) UNION ALL
         (SELECT 'subevent'      AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subevent_name      IS NULL OR d.subevent_name      = '')) UNION ALL
         (SELECT 'samplingevent' AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingevent_name IS NULL OR d.samplingevent_name = '')) UNION ALL
         (SELECT 'samplingtime'  AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingtime_name  IS NULL OR d.samplingtime_name  = '')) UNION ALL
         (SELECT 'center'        AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.center_name        IS NULL OR d.center_name        = '')) UNION ALL
         (SELECT 'startgroup'    AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.startgroup_name    IS NULL OR d.startgroup_name    = '')) UNION ALL
         (SELECT 'subject'       AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subject_name       IS NULL OR d.subject_name       = ''))
       )c;
--   missing_status_all := (SELECT min(c.status) FROM check_missing c);
--   IF (SELECT c.status != 2 FROM check_missing c WHERE c.name = 'subject') THEN
--     RAISE EXCEPTION 'Subject has missing values';
--   END IF;
--   
--   IF missing_status_all = 0 THEN
--     RAISE EXCEPTION 'Some variables have mixed missing and non-missing values';
--   END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- UPDATE FULL DESIGN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- insert data again (including datarow_id, datasetid now that we know it's unique)
  TRUNCATE design_distinct;
  INSERT INTO design_distinct (datarow_id, dataset_id, event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT
    dt.datarow_id
    ,  dt.dataset_id
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM
    dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id;
  
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET (event_name, event_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET event_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET (subevent_name, subevent_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET subevent_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET (samplingevent_name, samplingevent_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET samplingevent_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET (samplingtime_name, samplingtime_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET samplingtime_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET (center_name, center_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET center_status = 0;
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET (startgroup_name, startgroup_status) = ('1', 100);
  ELSEIF (SELECT c.status = 0 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET startgroup_status = 0;
  END IF;
  
  -- @formatter:on
  RETURN QUERY
    SELECT *
    FROM (
           WITH
             event_distinct           AS (
                                           SELECT uuid_generate_v4() AS event_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'event'            AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.event_name   AS name
                                                                , d.event_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , subevent_distinct      AS (
                                           SELECT uuid_generate_v4() AS subevent_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'subevent'         AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.subevent_name   AS name
                                                                , d.subevent_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , samplingevent_distinct AS (
                                           SELECT uuid_generate_v4() AS samplingevent_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'samplingevent'    AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.samplingevent_name   AS name
                                                                , d.samplingevent_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , samplingtime_distinct  AS (
                                           SELECT uuid_generate_v4() AS samplingtime_id
                                                , d.dataset_id
                                                , d.samplingevent_name
                                                , d.name
                                                , d.status
                                                , 'samplingtime'     AS element_name
                                           FROM (
                                                  SELECT DISTINCT d0.dataset_id
                                                                , d0.samplingtime_name   AS name
                                                                , d0.samplingtime_status AS status
                                                                , d0.samplingevent_name
                                                  FROM design_distinct d0
                                                ) d
                                         )
             , center_distinct        AS (
                                           SELECT uuid_generate_v4() AS center_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'center'           AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.center_name   AS name
                                                                , d.center_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , startgroup_distinct    AS (
                                           SELECT uuid_generate_v4() AS startgroup_id
                                                , x0.dataset_id
                                                , x0.name
                                                , x0.status
                                                , 'startgroup'       AS element_name
                                           FROM (
                                                  SELECT DISTINCT d.dataset_id
                                                                , d.startgroup_name   AS name
                                                                , d.startgroup_status AS status
                                                  FROM design_distinct d
                                                ) x0
                                         )
             , subject_distinct       AS (
                                           SELECT uuid_generate_v4() AS subject_id
                                                , d.dataset_id
                                                , d.name
                                                , d.status
                                                , 'subject'          AS element_name
                                           FROM (
                                                  SELECT DISTINCT d0.dataset_id
                                                                , d0.subject_name   AS name
                                                                , d0.subject_status AS status
                                                  FROM design_distinct d0
                                                ) d
                                         )
           SELECT dd.datarow_id
                , dd.dataset_id
           
                , ev.event_id
                , se.subevent_id
                , sm.samplingevent_id
                , st.samplingtime_id
                , cd.center_id
                , sg.startgroup_id
                , sb.subject_id
           
                , dd.event_name
                , dd.subevent_name
                , dd.samplingevent_name
                , dd.samplingtime_name
                , dd.center_name
                , dd.startgroup_name
                , dd.subject_name
           
                , dd.event_status
                , dd.subevent_status
                , dd.samplingevent_status
                , dd.samplingtime_status
                , dd.center_status
                , dd.startgroup_status
                , dd.subject_status
           FROM design_distinct               dd
             LEFT JOIN event_distinct         ev ON dd.event_name = ev.name
             LEFT JOIN subevent_distinct      se ON dd.subevent_name = se.name
             LEFT JOIN samplingevent_distinct sm ON dd.samplingevent_name = sm.name
             LEFT JOIN samplingtime_distinct  st ON (dd.samplingtime_name, dd.samplingevent_name) = (st.name, st.samplingevent_name)
             LEFT JOIN center_distinct        cd ON dd.startgroup_name = cd.name
             LEFT JOIN startgroup_distinct    sg ON dd.startgroup_name = sg.name
             LEFT JOIN subject_distinct       sb ON dd.subject_name = sb.name
         ) x0;
  RETURN;
END;
$$;
