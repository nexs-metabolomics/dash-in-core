CREATE FUNCTION dashin.datadesign_save_design
(
    _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            NAME          TEXT,
            AFFECTED_ROWS INT,
            DESCRIPTION   TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  overall_n          INT;
  distinct_n         INT;
  missing_status_all INT;
  design_exists      BOOL DEFAULT FALSE;
  return_early       BOOL DEFAULT FALSE;
BEGIN
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK IF DESIGN ALREADY SAVED
  -- ---------------------------------------------------------------------------------------------------------------------
  design_exists := exists(SELECT FROM dashin.datadesign_datatable dt0 WHERE dt0.dataset_id = _dataset_id AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org)));
  IF design_exists THEN
    RETURN QUERY
      VALUES ('exists', 0, 'Datadesign already saved');
    RETURN;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- DISTINCT DESIGN VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE design_distinct
  (
    datarow_id           UUID,
    dataset_id           UUID,
    
    event_name           TEXT,
    subevent_name        TEXT,
    samplingevent_name   TEXT,
    samplingtime_name    TEXT,
    center_name          TEXT,
    startgroup_name      TEXT,
    subject_name         TEXT,
    
    event_status         INT DEFAULT 1,
    subevent_status      INT DEFAULT 1,
    samplingevent_status INT DEFAULT 1,
    samplingtime_status  INT DEFAULT 1,
    center_status        INT DEFAULT 1,
    startgroup_status    INT DEFAULT 1,
    subject_status       INT DEFAULT 1
  )
    ON COMMIT DROP;
  INSERT INTO design_distinct (event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT DISTINCT jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
                , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org));
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK DUPLICATES
  -- ---------------------------------------------------------------------------------------------------------------------
  
  -- @formatter:off
  -- -------------------------------
  -- overall count
  -- -------------------------------
  overall_n := (SELECT count(*) FROM dashin.datatable dt WHERE dt.dataset_id = _dataset_id);
  
  -- -------------------------------
  -- distinct count
  -- -------------------------------
  distinct_n := (SELECT count(*) FROM design_distinct);
  -- @formatter:on
  IF overall_n = 0 OR distinct_n = 0 THEN
    RETURN QUERY VALUES ('empty', 0, 'Design was not saved because dataset or datadesign is empty');
    RETURN;
  END IF;
  
  IF overall_n != distinct_n THEN
    RETURN QUERY VALUES ('duplicate', 0, 'Design was not saved because the combination of design variables leads to duplicate entries');
    return_early := TRUE;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- CHECK MISSING VALUES
  -- ---------------------------------------------------------------------------------------------------------------------
  CREATE TEMPORARY TABLE check_missing
  (
    name       TEXT,
    missing    INT,
    status     INT,
    statustext TEXT
  )
    ON COMMIT DROP;
  -- -------------------------------------------
  -- status:
  --  0: mixed (missing and non-missing)
  --  1: all missing
  --  2: all non-missing
  -- -------------------------------------------
  INSERT INTO check_missing
  SELECT c.name
       , c.missing
       , CASE
           WHEN c.missing = 0 THEN 2
           ELSE ((distinct_n - c.missing) = 0)::INT
         END AS status
       , CASE
           WHEN c.missing = 0                  THEN 'All non-missing'
           WHEN ((distinct_n - c.missing) = 0) THEN 'All missing'
           ELSE 'Mixed missing and non-missing'
         END AS description
       -- @formatter:off
  FROM (
         (SELECT 'event'         AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.event_name         IS NULL OR d.event_name         = '')) UNION ALL
         (SELECT 'subevent'      AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subevent_name      IS NULL OR d.subevent_name      = '')) UNION ALL
         (SELECT 'samplingevent' AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingevent_name IS NULL OR d.samplingevent_name = '')) UNION ALL
         (SELECT 'samplingtime'  AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.samplingtime_name  IS NULL OR d.samplingtime_name  = '')) UNION ALL
         (SELECT 'center'        AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.center_name        IS NULL OR d.center_name        = '')) UNION ALL
         (SELECT 'startgroup'    AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.startgroup_name    IS NULL OR d.startgroup_name    = '')) UNION ALL
         (SELECT 'subject'       AS name,(count(*))::INT AS missing FROM design_distinct d WHERE (d.subject_name       IS NULL OR d.subject_name       = ''))
       )c;
  IF (SELECT c.status != 2 FROM check_missing c WHERE c.name = 'subject') THEN
    RETURN QUERY VALUES
                 ('subject_missing',0,'Design was not saved because subject variable contains missing values');
    return_early := TRUE;
  END IF;
  
  missing_status_all := (SELECT min(c.status) FROM check_missing c);
  IF missing_status_all = 0 THEN
    RETURN QUERY VALUES
                 ('mixed',0,'Design was not saved because some design variables have mixed missing and non-missing values');
    return_early := TRUE;
  END IF;
  
  IF return_early THEN
    RETURN;
  END IF;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- UPDATE FULL DESIGN
  -- ---------------------------------------------------------------------------------------------------------------------
  -- insert data again (including datarow_id, datasetid now that we know it's unique)
  TRUNCATE design_distinct;
  INSERT INTO design_distinct (datarow_id, dataset_id, event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name)
  SELECT
    dt.datarow_id
    ,  dt.dataset_id
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'event'))         AS event_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subevent'))      AS subevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingevent')) AS samplingevent_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'samplingtime'))  AS samplingtime_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'center'))        AS center_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'startgroup'))    AS startgroup_name
    , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dt.dataset_id , 'subject'))       AS subject_name
  FROM
    dashin.datatable dt
  WHERE dt.dataset_id = _dataset_id;
  
  -- ---------------------------------------------------------------------------------------------------------------------
  -- AUTOGENERATE DUMMIES FOR MISSING DESIGN VARIABLES
  -- ---------------------------------------------------------------------------------------------------------------------
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'event') THEN
    UPDATE design_distinct SET (event_name, event_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'subevent') THEN
    UPDATE design_distinct SET (subevent_name, subevent_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingevent') THEN
    UPDATE design_distinct SET (samplingevent_name, samplingevent_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'samplingtime') THEN
    UPDATE design_distinct SET (samplingtime_name, samplingtime_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'center') THEN
    UPDATE design_distinct SET (center_name, center_status) = ('1', 100);
  END IF;
  IF (SELECT c.status = 1 FROM check_missing c WHERE c.name = 'startgroup') THEN
    UPDATE design_distinct SET (startgroup_name, startgroup_status) = ('1', 100);
  END IF;
  
  -- @formatter:on
  
  INSERT INTO dashin.datadesign (dataset_id)
  SELECT _dataset_id
  ON CONFLICT DO NOTHING;
  
  -- -------------------------------------------------------------------------------------------------
  -- prepare individual elements
  -- -------------------------------------------------------------------------------------------------
  
  -- -------------------------------------------------------------
  -- event
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE event_distinct
  (
    event_id     UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO event_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.event_name   AS name
                , d.event_status AS status
                , 'event'
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- subevent
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE subevent_distinct
  (
    subevent_id  UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO subevent_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.subevent_name   AS name
                , d.subevent_status AS status
                , 'subevent'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- samplingevent
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE samplingevent_distinct
  (
    samplingevent_id UUID DEFAULT uuid_generate_v4(),
    dataset_id       UUID,
    name             TEXT,
    status           INT,
    element_name     TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO samplingevent_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.samplingevent_name   AS name
                , d.samplingevent_status AS status
                , 'samplingevent'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- samplingtime
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE samplingtime_distinct
  (
    samplingtime_id  UUID DEFAULT uuid_generate_v4(),
    dataset_id       UUID,
    samplingevent_id UUID,
    name             TEXT,
    status           INT,
    element_name     TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO samplingtime_distinct (dataset_id, samplingevent_id, name, status, element_name)
  SELECT d.dataset_id
       , sp.samplingevent_id
       , d.name
       , d.status
       , 'samplingtime' AS element_name
  FROM (
         SELECT DISTINCT d0.dataset_id
                       , d0.samplingtime_name   AS name
                       , d0.samplingtime_status AS status
                       , d0.samplingevent_name
         FROM design_distinct d0
       )                             d
    LEFT JOIN samplingevent_distinct sp ON d.samplingevent_name = sp.name;
  
  -- -------------------------------------------------------------
  -- center
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE center_distinct
  (
    center_id    UUID DEFAULT uuid_generate_v4(),
    dataset_id   UUID,
    name         TEXT,
    status       INT,
    element_name TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO center_distinct(dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.center_name   AS name
                , d.center_status AS status
                , 'center'        AS element_name
  FROM design_distinct d;
  -- -------------------------------------------------------------
  -- startgroup
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE startgroup_distinct
  (
    startgroup_id UUID DEFAULT uuid_generate_v4(),
    dataset_id    UUID,
    name          TEXT,
    status        INT,
    element_name  TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO startgroup_distinct (dataset_id, name, status, element_name)
  SELECT DISTINCT d.dataset_id
                , d.startgroup_name   AS name
                , d.startgroup_status AS status
                , 'startgroup'        AS element_name
  FROM design_distinct d;
  
  -- -------------------------------------------------------------
  -- subject
  -- -------------------------------------------------------------
  CREATE TEMPORARY TABLE subject_distinct
  (
    subject_id    UUID DEFAULT uuid_generate_v4(),
    dataset_id    UUID,
    center_id     UUID,
    startgroup_id UUID,
    name          TEXT,
    status        INT,
    element_name  TEXT
  )
    ON COMMIT DROP;
  -- ----------------
  INSERT INTO subject_distinct (dataset_id, center_id, startgroup_id, name, status, element_name)
  SELECT d.dataset_id
       , e.center_id
       , g.startgroup_id
       , d.name
       , d.status
       , 'subject' AS element_name
  FROM (
         SELECT DISTINCT d0.dataset_id
                       , d0.center_name
                       , d0.subject_name   AS name
                       , d0.subject_status AS status
                       , d0.startgroup_name
         FROM design_distinct d0
       )                          d
    LEFT JOIN startgroup_distinct g ON d.startgroup_name = g.name
    LEFT JOIN center_distinct     e ON d.center_name = e.name;
  
  -- ----------------------------------------------------------------------------------
  -- insert into datadesign_x_studydesign link tables
  -- ----------------------------------------------------------------------------------
  -- @formatter:off
  INSERT INTO dashin.datadesign_x_study_event (dataset_id, datadesign_event_id)
  SELECT dataset_id,event_id FROM event_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_subevent (dataset_id, datadesign_subevent_id)
  SELECT dataset_id,subevent_id FROM subevent_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_samplingevent (dataset_id, datadesign_samplingevent_id)
  SELECT dataset_id,samplingevent_id FROM samplingevent_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_samplingtime (dataset_id, datadesign_samplingtime_id)
  SELECT dataset_id,samplingtime_id FROM samplingtime_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_center (dataset_id, datadesign_center_id)
  SELECT dataset_id,center_id FROM center_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_startgroup (dataset_id, datadesign_startgroup_id)
  SELECT dataset_id,startgroup_id FROM startgroup_distinct;
  -- ------------
  INSERT INTO dashin.datadesign_x_study_subject (dataset_id, datadesign_subject_id)
  SELECT dataset_id,subject_id FROM subject_distinct;
  -- @formatter:on

  -- ----------------------------------------------------------------------------------
  -- save datadesign
  -- ----------------------------------------------------------------------------------
  INSERT INTO dashin.datadesign_datatable ( datarow_id, dataset_id
                                          , event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id
                                          , event_name, subevent_name, samplingevent_name, samplingtime_name, center_name, startgroup_name, subject_name
                                          , event_status, subevent_status, samplingevent_status, samplingtime_status, center_status, startgroup_status, subject_status)
  SELECT dd.datarow_id
       , dd.dataset_id
       
       , ev.event_id
       , se.subevent_id
       , sm.samplingevent_id
       , st.samplingtime_id
       , cd.center_id
       , sg.startgroup_id
       , sb.subject_id
       
       , dd.event_name
       , dd.subevent_name
       , dd.samplingevent_name
       , dd.samplingtime_name
       , dd.center_name
       , dd.startgroup_name
       , dd.subject_name
       
       , dd.event_status
       , dd.subevent_status
       , dd.samplingevent_status
       , dd.samplingtime_status
       , dd.center_status
       , dd.startgroup_status
       , dd.subject_status
  FROM design_distinct               dd
    LEFT JOIN event_distinct         ev ON dd.event_name = ev.name
    LEFT JOIN subevent_distinct      se ON dd.subevent_name = se.name
    LEFT JOIN samplingevent_distinct sm ON dd.samplingevent_name = sm.name
    LEFT JOIN samplingtime_distinct  st ON (dd.samplingtime_name, sm.samplingevent_id) = (st.name, st.samplingevent_id)
    LEFT JOIN center_distinct        cd ON dd.center_name = cd.name
    LEFT JOIN startgroup_distinct    sg ON dd.startgroup_name = sg.name
    LEFT JOIN subject_distinct       sb ON dd.subject_name = sb.name
  ON CONFLICT
    DO NOTHING;
  
  RETURN QUERY
    VALUES ('saved', overall_n, 'Design was saved');
  RETURN;
END;
$$;
