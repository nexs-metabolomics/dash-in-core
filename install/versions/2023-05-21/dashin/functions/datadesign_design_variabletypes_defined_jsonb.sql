CREATE FUNCTION dashin.datadesign_design_variabletypes_defined_jsonb
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
  , _char       TEXT DEFAULT 'x'
  , _null_char  TEXT DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID    UUID,
            VARIABLETYPES JSONB
          )
  LANGUAGE sql
AS
$$
SELECT _dataset_id::UUID                        AS dataset_id
     , jsonb_object_agg(type_code , is_defined) AS design_variables
FROM (
       SELECT CASE
                WHEN variable_id IS NOT NULL THEN _char
                ELSE _null_char
              END AS is_defined
            , *
       FROM (
              SELECT variabletype_id
                   , CASE variabletype_id
                       WHEN 8 THEN 1
                       WHEN 4 THEN 2
                       WHEN 5 THEN 3
                       WHEN 6 THEN 4
                       WHEN 7 THEN 5
                       WHEN 3 THEN 6
                       WHEN 2 THEN 7
                     END AS sort_order
                   , CASE variabletype_id
                       WHEN 4 THEN 'ev'
                       WHEN 3 THEN 'sg'
                       WHEN 2 THEN 'sb'
                       WHEN 5 THEN 'se'
                       WHEN 6 THEN 'sm'
                       WHEN 7 THEN 'st'
                       WHEN 8 THEN 'cn'
                       WHEN 7 THEN 'st'
                     END AS type_code
                   , name
                   , description
              FROM dashin.variabletype
              WHERE variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
              ORDER BY variabletype_id
            ) t
         LEFT JOIN
       (
         SELECT variable_id
              , variabletype_id
              , v.name AS variablename
         FROM dashin.variable       v
           LEFT JOIN dashin.dataset d USING (dataset_id)
         WHERE v.variabletype_id IN (2, 3, 4, 5, 6, 7, 8)
           AND v.dataset_id = _dataset_id
           AND (d.owner_org = _owner_org OR _owner_org IS NULL)
       )      v USING (variabletype_id)
     ) x0;
$$
;


ALTER FUNCTION dashin.datadesign_design_variabletypes_defined_jsonb(_dataset_id UUID, _owner_org UUID, _char TEXT, _null_char TEXT) OWNER TO :application_user;

