CREATE FUNCTION dashin.studyvariablecondition_designvar_category_summary
(
    _studyvariablecondition_id UUID
  , _study_id                  UUID
  , _owner_org                 UUID
)
  RETURNS TABLE
          (
            STUDYVARIABLECONDITION_ID UUID,
            ELEMENT_ID                UUID,
            VALUE_COLUMN              TEXT,
            ELEMENT2_ID               UUID,
            ELEMENT2_NAME             TEXT,
            SELECTED_N                INTEGER,
            CATEGORY_N                INTEGER,
            ORDINAL                   INTEGER,
            IS_SELECTED               BOOLEAN,
            ANY_SELECTED              BOOLEAN,
            VARIABLETYPE_ID           INTEGER,
            CONDITIONOPERATOR_ID      INTEGER
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  is_samplingtime       BOOL DEFAULT FALSE;
  _variabletype_id      INT;
  _conditionoperator_id INT;
BEGIN
  
  -- special treatment of samplingevent:
  -- if current variabletype is samplingevent (vartype_id = 6)
  -- and an event is selected (condition_x_category NOT NULL)
  -- treat as samplingtime (vartype_id = 7)
  SELECT CASE
           WHEN b.variabletype_id = 6 AND c.element_id IS NOT NULL THEN 7
           ELSE
             b.variabletype_id
         END
       , a.conditionoperator_id
  INTO _variabletype_id,_conditionoperator_id
  FROM dashin.studyvariablecondition                    a
    INNER JOIN dashin.studyvariable                     b ON a.studyvariable_id = b.studyvariable_id
    LEFT JOIN  dashin.studyvariablecondition_x_category c ON a.studyvariablecondition_id = c.studyvariablecondition_id
  WHERE a.studyvariablecondition_id = _studyvariablecondition_id
    AND a.study_id = _study_id
    AND exists(SELECT * FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  CREATE TEMPORARY TABLE temp_intermediate_table
  (
    category_n                INT,
    conditionoperator_id      INT,
    studyvariablecondition_id UUID,
    element_id                UUID,
    element_name              TEXT,
    element2_id               UUID,
    element2_name             TEXT,
    ordinal                   INT
  )
    ON COMMIT DROP;
  
  IF _variabletype_id = 2 THEN
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.subject_id               AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , NULL::INT                  AS ordinal
    FROM dashin.study_subject          b
      INNER JOIN dashin.studydatatable d ON b.subject_id = d.subject_id
    WHERE b.study_id = _study_id
    GROUP BY b.subject_id
           , b.name;
  
  ELSEIF _variabletype_id = 3 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.startgroup_id            AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_startgroup       b
      INNER JOIN dashin.studydatatable d ON b.startgroup_id = d.startgroup_id
    WHERE b.study_id = _study_id
    GROUP BY b.startgroup_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 4 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.event_id                 AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_event            b
      INNER JOIN dashin.studydatatable d ON b.event_id = d.event_id
    WHERE b.study_id = _study_id
    GROUP BY b.event_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 5 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.subevent_id              AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_subevent         b
      INNER JOIN dashin.studydatatable d ON b.subevent_id = d.subevent_id
    WHERE b.study_id = _study_id
    GROUP BY b.subevent_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 8 THEN
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.center_id                AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_center           b
      INNER JOIN dashin.studydatatable d ON b.center_id = d.center_id
    WHERE b.study_id = _study_id
    GROUP BY b.center_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 6 THEN
    -- if no samplingevent selected, return samplingevent categories
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.samplingevent_id         AS element_id
         , b.name                     AS element_name
         , NULL::UUID                 AS element2_id
         , NULL::TEXT                 AS element2_name
         , b.ordinal
    FROM dashin.study_samplingevent    b
      INNER JOIN dashin.studydatatable d ON b.samplingevent_id = d.samplingevent_id
    WHERE b.study_id = _study_id
    GROUP BY b.samplingevent_id
           , b.name
           , b.ordinal;
  
  ELSEIF _variabletype_id = 7 THEN
    -- if samplinevent is selected, return samplingtime categories for that event: 
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT (count(*))::INT            AS category_n
         , _conditionoperator_id      AS conditionoperator_id
         , _studyvariablecondition_id AS studyvariablecondition_id
         , b.samplingevent_id         AS element_id
         , b.name                     AS element_name
         , c.samplingtime_id          AS element2_id
         , c.name                     AS element2_name
         , c.ordinal_time             AS ordinal
    FROM dashin.study_samplingevent                       b
      LEFT JOIN  dashin.study_samplingtime                c ON b.samplingevent_id = c.samplingevent_id
      INNER JOIN dashin.studydatatable                    d ON b.samplingevent_id = d.samplingevent_id
      INNER JOIN dashin.studyvariablecondition_x_category e ON (_studyvariablecondition_id, b.samplingevent_id) = (e.studyvariablecondition_id, e.element_id)
    WHERE b.study_id = _study_id
    GROUP BY b.samplingevent_id
           , b.name
           , c.samplingtime_id
           , c.name
           , c.ordinal_time;
    
    is_samplingtime := TRUE;
  
  ELSE
    
    INSERT INTO temp_intermediate_table (category_n, conditionoperator_id, studyvariablecondition_id, element_id, element_name, element2_id, element2_name, ordinal)
    SELECT count(*)   AS category_n
         , x0.conditionoperator_id
         , x0.studyvariablecondition_id
         , NULL::UUID AS element_id
         , x0.element_name
         , NULL::UUID AS element2_id
         , NULL::TEXT AS element2_name
         , NULL::INT  AS ordinal
    FROM (
           SELECT d.name
                , d.local_id
                , jsonb_array_element_text(e.datarow , d.local_id) AS element_name
                , a.conditionoperator_id
                , a.studyvariablecondition_id
           FROM dashin.studyvariablecondition           a
             INNER JOIN dashin.studyvariable            b ON a.studyvariable_id = b.studyvariable_id
             INNER JOIN dashin.studyvariable_x_variable c ON a.studyvariable_id = c.studyvariable_id
             INNER JOIN dashin.variable                 d ON c.variable_id = d.variable_id
             INNER JOIN dashin.datatable                e ON d.dataset_id = e.dataset_id
           WHERE a.studyvariablecondition_id = _studyvariablecondition_id
         ) x0
    GROUP BY x0.element_name
           , x0.conditionoperator_id
           , x0.studyvariablecondition_id;
  
  END IF;
  
  IF is_samplingtime THEN
    
    RETURN QUERY
      SELECT xa.studyvariablecondition_id
           , xa.element2_id
           , xa.element2_name
           , xa.element_id
           , xa.element_name
           , coalesce(sum(xa.category_n) FILTER ( WHERE xb.samplingtime_id IS NOT NULL) OVER () , 0)::INT AS selected_n
           , xa.category_n
           , xa.ordinal
           , xb.samplingtime_id IS NOT NULL                                                               AS is_selected
           , (count(*) FILTER ( WHERE xb.samplingtime_id IS NOT NULL) OVER ()) > 0                        AS any_selected
           , _variabletype_id
           , xa.conditionoperator_id
      FROM (
             SELECT a.studyvariablecondition_id
                  , a.element_id
                  , a.element_name
                  , a.element2_id
                  , a.element2_name
                  , a.category_n
                  , a.ordinal
                  , a.conditionoperator_id
             FROM temp_intermediate_table a
           )                                                           xa
        LEFT JOIN dashin.studyvariablecondition_x_samplingtimecategory xb ON (xa.studyvariablecondition_id, xa.element2_id) = (xb.studyvariablecondition_id, xb.samplingtime_id)
      ORDER BY xa.ordinal
             , xa.element_name;
  
  ELSE
    
    RETURN QUERY
      SELECT xa.studyvariablecondition_id
           , xa.element_id
           , xa.element_name
           , xa.element2_id
           , xa.element2_name
           , coalesce(sum(xa.category_n) FILTER ( WHERE xb.element_id IS NOT NULL) OVER () , 0)::INT AS selected_n
           , xa.category_n
           , xa.ordinal
           , coalesce(xb.element_id::TEXT , xc.value_column::TEXT) IS NOT NULL                       AS is_selected
           , (count(*) FILTER ( WHERE xb.element_id IS NOT NULL) OVER ()) > 0                        AS any_selected
           , _variabletype_id
           , xa.conditionoperator_id
      FROM (
             SELECT a.category_n
                  , a.conditionoperator_id
                  , a.studyvariablecondition_id
                  , a.element_id
                  , a.element_name
                  , a.element2_id
                  , a.element2_name
                  , a.ordinal
             FROM temp_intermediate_table a
           )                                               xa
        LEFT JOIN dashin.studyvariablecondition_x_category xb ON (xa.studyvariablecondition_id, xa.element_id) = (xb.studyvariablecondition_id, xb.element_id)
        LEFT JOIN dashin.studyvariablecondition_x_category xc ON (xa.studyvariablecondition_id, xa.element_name) = (xc.studyvariablecondition_id, xc.value_column)
      ORDER BY xa.ordinal
             , xa.element_name;
  
  END IF;
  
  RETURN;
END;
$$;


