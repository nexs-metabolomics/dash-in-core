CREATE FUNCTION dashin.datadesign_startgroup_event_sequence_text
(
    _dataset_id         UUID
  , _owner_org          UUID DEFAULT NULL
  , _group_name_prefix  TEXT DEFAULT 'group '
  , _group_name_postfix TEXT DEFAULT ' (estimated)'
)
  RETURNS TABLE
          (
            DATASET_ID     UUID,
            STARTGROUP     TEXT,
            EVENT_SEQUENCE TEXT,
            SUBJECTS       TEXT
          )
  LANGUAGE sql
AS
$$
SELECT dataset_id
     , coalesce(startgroup , _group_name_prefix || rn || _group_name_postfix) AS startgroup
     , group_sequence
     , group_members
FROM (
       SELECT dataset_id
            , startgroup
            , group_sequence
            , string_agg(subject , ', ') AS group_members
            , row_number() OVER ()       AS rn
       FROM (
              SELECT dataset_id
                   , startgroup
                   , subject
                   , string_agg(group_event , ', ' ORDER BY event,subevent) AS group_sequence
              FROM (
                     SELECT -- DISTINCT 
                       dataset_id
                          , '[' || event || ', ' || subevent || ']' AS group_event
                          , subject
                          , startgroup
                          , event
                          , subevent
                     FROM (
                            SELECT dataset_id::UUID
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))      AS event
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))   AS subevent
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup')) AS startgroup
                                 , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))    AS subject
                            FROM dashin.datatable      dt
                              LEFT JOIN dashin.dataset d USING (dataset_id)
                            WHERE dt.dataset_id = _dataset_id
                              AND (d.owner_org = _owner_org OR _owner_org IS NULL)
                            GROUP BY dataset_id
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup'))
                                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))
                          ) x0
                   ) x1
              GROUP BY dataset_id
                     , startgroup
                     , subject
            ) x2
       GROUP BY dataset_id
              , startgroup
              , group_sequence
     ) x3;
$$;
-- -------------------------------------------------------------------------

ALTER FUNCTION dashin.datadesign_startgroup_event_sequence_text OWNER TO :application_user;

