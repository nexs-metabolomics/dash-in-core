INSERT INTO
  dashin.unit (unit_id, unitdomain_id, ordinal, symbol, name, description)
VALUES
(0, 0, 1, '', 'no-unit', '')
  , (1, 1, 2, 'g', 'gram', '')
  , (2, 2, 3, 'L', 'liter', '')
  , (3, 3, 4, 'mol', 'mole', '')
  , (4, 4, 5, 'm', 'meter', '')
  , (5, 5, 6, 'd', 'day', '')
  , (6, 5, 7, 'h', 'hour', '')
  , (7, 5, 8, 'hr', 'hour', '')
  , (8, 5, 9, 'min', 'minute', '')
  , (9, 5, 10, 's', 'second', '')
  , (10, 6, 11, '%', 'percent', '')
  , (11, 6, 12, 'Pr', 'proportion', '')
  , (12, 7, 13, 'Eq', 'equivalent', '')
  , (13, 7, 14, 'IU', 'international units', '')
  , (14, 7, 15, 'U', 'units', '')
  , (15, 7, 16, 'units', 'units', '')
  , (16, 7, 17, 'x', 'count', '')
  , (17, 100, 18, 'mm Hg', 'milimeter Hg', '')
  , (18, 100, 19, 'pH units', 'pH-units', '')
  , (19, 100, 20, 'Osm', 'osmole', '')
;

INSERT INTO
  dashin.unitprefix (unitprefix_id, unitprefixdomain_id, ordinal, symbol, numeric_label, name, description, value)
VALUES
(0, 0, 0, '', '', 'no-prefix', '', 0)
  , (1, 1, 1, '', '10^0', 'one', '', 1)
  , (2, 1, 2, 'k', '10^3', 'kilo', '', 10e3)
  , (3, 1, 3, 'd', '10^-1', 'deci', '', 10e-1)
  , (4, 1, 4, 'm', '10^-3', 'milli', '', 10e-3)
  , (5, 1, 5, 'μ', '10^-6', 'micro', '', 10e-5)
  , (6, 1, 6, 'n', '10^-9', 'nano', '', 10e-9)
  , (7, 1, 7, 'p', '10^-12', 'pico', '', 10e-12)
  , (8, 1, 8, 'M', '10^6', 'mega', '', 10e6)
  , (9, 1, 9, 'G', '10^9', 'giga', '', 10e9)
  , (10, 1, 10, 'T', '10^12', 'tera', '', 10e12)
  , (11, 3, 11, '24', '24', '24', 'day', 24)
  , (12, 3, 12, '1', '1', '1', 'hour', 1)
  , (13, 2, 13, '1', '1', '1', 'hour', 1)
;

INSERT INTO
  dashin.unitdomain (unitdomain_id, ordinal, name, description)
VALUES
(0, 0, 'si-none', '')
  , (1, 1, 'si-weight', '')
  , (2, 2, 'si-volume', '')
  , (3, 3, 'si-mole', '')
  , (4, 4, 'si-length', '')
  , (5, 5, 'time', '')
  , (6, 6, 'proportion', '')
  , (7, 7, 'unit/count', '')
  , (100, 8, 'other', '')
;

INSERT INTO
  dashin.unitprefixdomain (unitprefixdomain_id, ordinal, name, description)
VALUES
(0, 0, 'none', '')
  , (1, 1, 'si-metric', '')
  , (2, 2, 'count', '')
  , (3, 3, 'time-hour', '')
  , (4, 4, 'time-day', '')
;
