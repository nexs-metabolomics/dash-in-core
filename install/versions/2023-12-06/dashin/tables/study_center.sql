CREATE TABLE dashin.study_center
(
  center_id   UUID CONSTRAINT study_center_pk PRIMARY KEY,
  study_id    UUID,
  ordinal     INT,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now(),
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT now(),
  name        TEXT,
  label       TEXT,
  description TEXT,
  CONSTRAINT study_center_name_uk UNIQUE (study_id, name)
);
ALTER TABLE dashin.study_center OWNER TO :application_user;

