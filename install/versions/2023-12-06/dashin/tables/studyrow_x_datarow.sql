CREATE TABLE dashin.studyrow_x_datarow
(
  studyrow_id UUID NOT NULL,
  study_id    UUID NOT NULL,
  datarow_id  UUID NOT NULL,
  dataset_id  UUID NOT NULL,
  CONSTRAINT studyrow_x_datarow_datarow_uk UNIQUE (datarow_id),
  CONSTRAINT studyrow_x_datarow_all_uk UNIQUE (studyrow_id, datarow_id)
);

ALTER TABLE dashin.studyrow_x_datarow OWNER TO :application_user;
