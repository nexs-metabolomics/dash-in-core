CREATE TABLE dashin.study
(
  study_id             UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_pk
      PRIMARY KEY,
  name                 TEXT,
  title                TEXT,
  description          TEXT,
  start_date           TIMESTAMP,
  endpoint             TEXT,
  objectives           TEXT,
  conclusion           TEXT,
  exclusion            TEXT,
  inclusion            TEXT,
  institute            TEXT,
  country_id           INTEGER,
  consortium_id        UUID,
  published            TEXT,
  researchdesign_id    INTEGER,
  num_treat            TEXT,
  num_factor           INTEGER,
  num_arm              INTEGER,
  researchdesign_text  TEXT,
  num_volun            TEXT,
  num_volun_terminated TEXT,
  recruit_start_year   INTEGER,
  recruit_end_year     INTEGER,
  blinding             INTEGER,
  blinding_method      TEXT,
  owner_org            UUID,
  owner_user           UUID,
  created_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at           TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details              JSONB
);

ALTER TABLE dashin.study OWNER TO :application_user;
