CREATE TABLE dashin.researchfield
(
  researchfield_id INT NOT NULL
    CONSTRAINT researchfield_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT
);

ALTER TABLE dashin.researchfield OWNER TO :application_user;
