CREATE TABLE dashin.studydataset
(
  study_id   UUID PRIMARY KEY,
  status     INT DEFAULT 1,
  created_at TIMESTAMPTZ DEFAULT now()
);

ALTER TABLE dashin.studydataset OWNER TO :application_user;
