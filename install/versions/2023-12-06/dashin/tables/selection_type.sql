CREATE TABLE dashin.selection_type
(
  selection_type_id INTEGER NOT NULL
    CONSTRAINT selection_type_pk
      PRIMARY KEY,
  name              TEXT,
  description       TEXT
);

ALTER TABLE dashin.selection_type OWNER TO :application_user;
