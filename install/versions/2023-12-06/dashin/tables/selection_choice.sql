CREATE TABLE dashin.selection_choice
(
  selection_choice_id INTEGER NOT NULL,
  selection_type_id   INTEGER NOT NULL,
  ordinal             INTEGER,
  name                TEXT,
  description         TEXT,
  CONSTRAINT selection_choice_pk
    PRIMARY KEY (selection_type_id, selection_choice_id)
);

ALTER TABLE dashin.selection_choice OWNER TO :application_user;
