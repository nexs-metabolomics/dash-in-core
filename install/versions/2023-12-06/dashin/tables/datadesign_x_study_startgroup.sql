CREATE TABLE dashin.datadesign_x_study_startgroup
(
  dataset_id               UUID,
  datadesign_startgroup_id UUID PRIMARY KEY,
  study_id                 UUID,
  study_startgroup_id      UUID,
  CONSTRAINT dxs_dataset_id_startgroup_id_key UNIQUE (dataset_id, study_startgroup_id)
);

ALTER TABLE dashin.datadesign_x_study_startgroup OWNER TO :application_user;
