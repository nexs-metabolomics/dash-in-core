CREATE TABLE dashin.resultset
(
  resultset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT resultset_pk
      PRIMARY KEY,
  name         TEXT,
  description  TEXT,
  owner_user   UUID NOT NULL,
  created_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details      JSONB
);

ALTER TABLE dashin.resultset OWNER TO :application_user;
