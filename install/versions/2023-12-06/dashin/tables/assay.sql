CREATE TABLE dashin.assay
(
  assay_id        UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT assay_pk
      PRIMARY KEY,
  name            TEXT,
  description     TEXT,
  researchfield_id INTEGER DEFAULT 1 NOT NULL,
  created_at      TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at      TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);

ALTER TABLE dashin.assay OWNER TO :application_user;
