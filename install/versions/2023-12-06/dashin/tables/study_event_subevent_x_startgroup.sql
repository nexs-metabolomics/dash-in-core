CREATE TABLE dashin.study_event_subevent_x_startgroup
(
  event_id      UUID NOT NULL,
  subevent_id   UUID NOT NULL,
  study_id      UUID NOT NULL,
  startgroup_id UUID NOT NULL,
  CONSTRAINT study_event_subevent_x_startgroup_pk
    PRIMARY KEY (event_id, subevent_id, study_id, startgroup_id)
);

ALTER TABLE dashin.study_event_subevent_x_startgroup OWNER TO :application_user;
