CREATE TABLE dashin.datadesign_x_study_subject
(
  dataset_id            UUID,
  datadesign_subject_id UUID PRIMARY KEY,
  study_id              UUID,
  study_subject_id      UUID,
  CONSTRAINT dxs_dataset_id_subject_id_key UNIQUE (dataset_id, study_subject_id)
);

ALTER TABLE dashin.datadesign_x_study_subject OWNER TO :application_user;

