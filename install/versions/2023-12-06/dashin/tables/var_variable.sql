CREATE TABLE dashin.var_variable
(
  var_variable_id   UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT var_variable_pk
      PRIMARY KEY,
  var_dataset_id    UUID,
  local_id          INTEGER NOT NULL,
  name              TEXT,
  description       TEXT,
  status            INT DEFAULT 1,
  search_single_id  INT,
  search_generic_id INT,
  datatype_id       INT DEFAULT 1,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details           JSONB,
  CONSTRAINT var_variable_search_map_uk UNIQUE (var_dataset_id, search_single_id),
  CONSTRAINT var_variable_local_id_uk UNIQUE (var_dataset_id, local_id)
);

ALTER TABLE dashin.var_variable OWNER TO :application_user;
