CREATE TABLE dashin.datadesign
(
  dataset_id UUID NOT NULL
    CONSTRAINT datadesign_pk
      PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now()
);

ALTER TABLE dashin.datadesign OWNER TO :application_user;
