CREATE FUNCTION dashin.studyvariable_data_designvariable
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
)
  RETURNS TABLE
          (
            DATAROW_ID   UUID,
            ELEMENT_ID   UUID,
            VALUE_COLUMN TEXT,
            LABEL_COLUMN TEXT,
            DATATYPE_ID  INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  _dataquery        TEXT;
  _variabletype_id INT;
BEGIN
  _variabletype_id := (
                         SELECT
                           variabletype_id
                         FROM dashin.studyvariable
                         WHERE studyvariable_id = _studyvariable_id
                       );
  IF _variabletype_id = 2 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.subject_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_subject      b ON b.subject_id = a.subject_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 3 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.startgroup_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_startgroup   b ON b.startgroup_id = a.startgroup_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 4 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.event_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_event        b ON b.event_id = a.event_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 5 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.subevent_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_subevent     b ON b.subevent_id = a.subevent_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 6 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.samplingevent_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable               a
        LEFT JOIN dashin.study_samplingevent b ON b.samplingevent_id = a.samplingevent_id
        LEFT JOIN dashin.studyrow_x_datarow  c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable       d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 7 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.samplingtime_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_samplingtime b ON b.samplingtime_id = a.samplingtime_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id = 8 THEN
    
    RETURN QUERY
      SELECT
        c.datarow_id
           , b.center_id
           , b.name
           , b.label
           , d.datatype_id
      FROM dashin.studydatatable              a
        LEFT JOIN dashin.study_center       b ON b.center_id = a.center_id
        LEFT JOIN dashin.studyrow_x_datarow c ON a.studyrow_id = c.studyrow_id
        LEFT JOIN dashin.studyvariable      d ON (d.study_id, d.variabletype_id) = (_study_id, _variabletype_id)
      WHERE a.study_id = _study_id
        AND exists(SELECT FROM dashin.study z WHERE (z.study_id, z.owner_org) = (_study_id, _owner_org));
  
  ELSEIF _variabletype_id IN (0, 1) AND _studyvariable_id IS NOT NULL THEN
    _dataquery := (
                    SELECT
                          E'SELECT\n datarow_id\n ,NULL::UUID\n,value_column\n,NULL::TEXT\n,datatype_id\n FROM (\n' ||
                          b.dataquery ||
                          E'\n) x (datarow_id, datatype_id, value_column)'
                    FROM dashin.studyvariable_data_generate_query(_studyvariable_id , _study_id , _owner_org) b (dataquery)
                  );
    RETURN QUERY EXECUTE _dataquery;
  END IF;
  RETURN;
END;
$$;
