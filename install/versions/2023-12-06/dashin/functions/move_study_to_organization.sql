CREATE OR REPLACE FUNCTION dashin.move_study_to_organization
(
    _study_id       UUID
  , _from_owner_org UUID
  , _to_owner_org   UUID
)
  RETURNS TABLE
          (
            STUDY_ID      UUID,
            DATASET_ID    UUID,
            OWNER_ORG     UUID,
            NEW_ORG_NAME  TEXT,
            OLD_ORG_NAME  TEXT,
            OLD_OWNER_ORG UUID,
            ORG_MATCH     BOOL
          )
  LANGUAGE sql
AS
$$
WITH
  study_set_owner_org     AS (
                               UPDATE dashin.study SET owner_org = _to_owner_org
                                 WHERE (study_id, owner_org) = (_study_id, _from_owner_org)
                                 RETURNING study_id,owner_org
                             )
  , dataset_set_owner_org AS (
                               UPDATE dashin.dataset a SET owner_org = _to_owner_org
                                 WHERE a.dataset_id IN (
                                                         SELECT dataset_id
                                                         FROM dashin.dataset_x_study x
                                                         WHERE x.study_id = _study_id
                                                       )
                                   AND a.owner_org = _from_owner_org
                                 RETURNING dataset_id,owner_org
                             )
SELECT a.study_id
     , b.dataset_id
     , a.owner_org
     , c1.name                   AS new_org_name
     , c2.name                   AS old_org_name
     , _from_owner_org           AS old_owner_org
     , a.owner_org = b.owner_org AS matching_orgs
FROM study_set_owner_org          a
     LEFT JOIN admin.organization c1 ON a.owner_org = c1.organization_id
     LEFT JOIN admin.organization c2 ON _from_owner_org = c2.organization_id
   , dataset_set_owner_org        b
  ;
$$;
