CREATE FUNCTION dashin.studydata_remove_dataset_from_study
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
  , _keep_rows  BOOL DEFAULT FALSE
  , _keep_vars  BOOL DEFAULT FALSE
)
  RETURNS
    TABLE
    (
      STATUS      TEXT,
      STATUS_ID   INT,
      DESCRIPTION TEXT
    )
  LANGUAGE plpgsql
AS
$$
DECLARE
  valid_owner        BOOL;
  dataset_present    BOOL;
  -- ----------------------
  datasetvar_n       INT;
  studyvar_n         INT;
  -- ----------------------
  study_rows         INT;
  dataset_rows       INT;
  event_rows         INT;
  subevent_rows      INT;
  samplingevent_rows INT;
  samplingtime_rows  INT;
  center_rows        INT;
  startgroup_rows    INT;
  subject_rows       INT;
BEGIN
  -- validate permissions
  valid_owner := exists(SELECT * FROM dashin.study s WHERE (s.study_id, s.owner_org) = (_study_id, _owner_org)) AND
                 exists(SELECT * FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  
  IF NOT valid_owner
  THEN
    RETURN QUERY VALUES ('result', 0, 'Failed. Could not validate dataset and/or study.');
    RETURN;
  END IF;
  
  -- check
  dataset_present := exists(SELECT * FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id));
  
  IF NOT dataset_present
  THEN
    RETURN QUERY VALUES ('result', 2, 'Dataset is not present in this study.');
    RETURN;
  END IF;
  
  DELETE FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id);
  GET DIAGNOSTICS dataset_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_event h
  SET (study_id, study_event_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS event_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_subevent h
  SET (study_id, study_subevent_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS subevent_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_samplingevent h
  SET (study_id, study_samplingevent_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS samplingevent_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_samplingtime h
  SET (study_id, study_samplingtime_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS samplingtime_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_center h
  SET (study_id, study_center_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS center_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_startgroup h
  SET (study_id, study_startgroup_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS startgroup_rows = ROW_COUNT;
  
  UPDATE dashin.datadesign_x_study_subject h
  SET (study_id, study_subject_id) = (NULL, NULL)
  WHERE (study_id, dataset_id) = (_study_id, _dataset_id);
  GET DIAGNOSTICS subject_rows = ROW_COUNT;
  
  IF NOT _keep_rows THEN
    DELETE
    FROM dashin.studydatatable
    WHERE study_id = _study_id AND studyrow_id NOT IN (
                                                        SELECT studyrow_id
                                                        FROM dashin.studyrow_x_datarow x
                                                        WHERE x.study_id = _study_id
                                                      );
    GET DIAGNOSTICS study_rows = ROW_COUNT;
  ELSE
    study_rows := 0;
  END IF;
  
  DELETE
  FROM dashin.studyvariable_x_variable
  WHERE variable_id IN (
                         SELECT v.variable_id
                         FROM dashin.variable v
                         WHERE dataset_id = _dataset_id
                       );
  GET DIAGNOSTICS datasetvar_n = ROW_COUNT;
  
  IF NOT _keep_vars THEN
    DELETE
    FROM dashin.studyvariable a
    WHERE a.study_id = _study_id AND a.studyvariable_id NOT IN (
                                                                 SELECT studyvariable_id
                                                                 FROM dashin.studyvariable_x_variable x
                                                                 WHERE x.study_id = _study_id
                                                               );
    GET DIAGNOSTICS studyvar_n = ROW_COUNT;
  ELSE
    studyvar_n := 0;
  END IF;
  
  RETURN QUERY VALUES ('result', 1, 'Dataset successfully removed for study.')
                    , ('studyrows', study_rows, 'Studyrows removed')
                    , ('dataset', dataset_rows, '')
                    , ('event', event_rows, '')
                    , ('subevent', subevent_rows, '')
                    , ('samplingevent', samplingevent_rows, '')
                    , ('samplingtime', samplingtime_rows, '')
                    , ('center', center_rows, '')
                    , ('startgroup', startgroup_rows, '')
                    , ('subject', subject_rows, '');
  RETURN;

END;
$$
;
