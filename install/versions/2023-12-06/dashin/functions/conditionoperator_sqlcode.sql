CREATE FUNCTION dashin.conditionoperator_sqlcode
(
      _conditionoperator_id INT
    , _secondary_parm       TEXT DEFAULT ''
)
    RETURNS TEXT
    LANGUAGE sql
AS
$$
SELECT
    CASE
        WHEN _conditionoperator_id = 1                            THEN ' > '
        WHEN _conditionoperator_id = 2                            THEN ' >= '
        WHEN _conditionoperator_id = 3                            THEN ' = '
        WHEN _conditionoperator_id = 4                            THEN ' <= '
        WHEN _conditionoperator_id = 5                            THEN ' < '
        WHEN _conditionoperator_id = 6                            THEN ' <> '
        WHEN _conditionoperator_id = 7 AND _secondary_parm = 'l'  THEN ' < '
        WHEN _conditionoperator_id = 7 AND _secondary_parm = 'r'  THEN ' < '
        WHEN _conditionoperator_id = 8 AND _secondary_parm = 'l'  THEN ' < '
        WHEN _conditionoperator_id = 8 AND _secondary_parm = 'r'  THEN ' <= '
        WHEN _conditionoperator_id = 9 AND _secondary_parm = 'l'  THEN ' <= '
        WHEN _conditionoperator_id = 9 AND _secondary_parm = 'r'  THEN ' < '
        WHEN _conditionoperator_id = 10 AND _secondary_parm = 'l' THEN ' <= '
        WHEN _conditionoperator_id = 10 AND _secondary_parm = 'r' THEN ' <= '
    END
$$;
