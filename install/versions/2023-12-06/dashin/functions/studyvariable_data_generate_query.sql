CREATE FUNCTION dashin.studyvariable_data_generate_query
(
    _studyvariable_id UUID
  , _study_id         UUID
  , _owner_org        UUID
  , _limit            INT DEFAULT NULL
) RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  limit_val  TEXT DEFAULT '';
  return_val TEXT;
BEGIN
  
  IF _limit >= 0 THEN
    limit_val := E'LIMIT ' || _limit;
  END IF;
  
  return_val := (
                  SELECT E'(\n' || string_agg(query_inner , E'\n)\nUNION ALL\n(\n') || E'\n)\n' || limit_val
                  FROM (
                         SELECT 'SELECT datarow_id,(' || x0.datatype_id || ')::INT, jsonb_array_element_text(datarow,' || x0.local_id || E') AS ' || quote_ident(x0.name) || E'\n' ||
                                E'FROM dashin.datatable WHERE dataset_id = ' || quote_literal(x0.dataset_id) AS query_inner
                         FROM (
                                SELECT c.local_id
                                     , b.name
                                     , b.datatype_id
                                     , a.dataset_id
                                FROM dashin.studyvariable_x_variable a
                                  LEFT JOIN  dashin.studyvariable    b ON a.studyvariable_id = b.studyvariable_id
                                  LEFT JOIN  dashin.variable         c ON a.variable_id = c.variable_id
                                  INNER JOIN dashin.study            d ON a.study_id = d.study_id
                                WHERE a.studyvariable_id = _studyvariable_id
                                  AND (d.study_id, d.owner_org) = (_study_id, _owner_org)
                              ) x0
                       ) x1
  );
  RETURN return_val;
END ;
$$;

