CREATE FUNCTION dashin.search_metabolomics_generate_core_query
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TEXT
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry         TEXT;
  _mz_str     TEXT DEFAULT '';
  _rt_str     TEXT DEFAULT '';
  _mz_rt_str  TEXT DEFAULT '';
  _offset_str TEXT DEFAULT '';
  _limit_str  TEXT DEFAULT '';
BEGIN
  -- mz
  IF _mz_center IS NOT NULL AND _mz_range IS NOT NULL
  THEN
    _mz_str := '(mz BETWEEN ' || _mz_center - _mz_range || ' AND ' || _mz_center + _mz_range || ' OR mz IS NULL)';
  ELSEIF _mz_center IS NOT NULL
  THEN
    _mz_str := '(mz = ' || _mz_center || ' OR mz IS NULL)';
  END IF;
  
  -- rt
  IF _rt_center IS NOT NULL AND _rt_range IS NOT NULL
  THEN
    _rt_str := '(rt BETWEEN ' || _rt_center - _rt_range || ' AND ' || _rt_center + _rt_range || ' OR rt IS NULL)';
  ELSEIF _rt_center IS NOT NULL
  THEN
    _rt_str := '(rt = ' || _rt_center || ' OR rt IS NULL)';
  END IF;
  
  -- rt+mz
  IF length(_mz_str) > 0 AND length(_rt_str) > 0 THEN
    _mz_rt_str := _mz_str || ' AND ' || _rt_str;
  ELSEIF length(_mz_str) > 0 THEN
    _mz_rt_str := _mz_str;
  ELSEIF length(_rt_str) > 0 THEN
    _mz_rt_str := _rt_str;
  END IF;
  
  IF _limit >= 0 THEN
    _offset_str := E'OFFSET ' || coalesce(_offset , 0);
    _limit_str := E'\nLIMIT ' || _limit;
  END IF;
  
  qry := (
           SELECT
             -- @formatter:off
               E'SELECT coalesce(a.variable_id,b.variable_id) as variable_id,mz,rt FROM\n(' || dashin.search_generate_column_query_numeric(3) || E') a (variable_id,rt)\n' ||
               E'FULL JOIN\n(\n' || dashin.search_generate_column_query_numeric(2) || E')b (variable_id,mz) ON a.variable_id = b.variable_id\n' ||
               E'WHERE ' || _mz_rt_str || E'\n' ||
               E'AND NOT (rt IS NULL AND mz IS NULL)\n' ||
               _offset_str || _limit_str || E'\n' ||
               E''
           -- @formatter:on
         );
  RETURN qry;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics_generate_core_query OWNER TO :application_user;
