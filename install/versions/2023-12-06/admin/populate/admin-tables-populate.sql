-- email template
INSERT INTO admin.template_type (template_type_id, name, description)
VALUES (1, 'email', 'email template');
INSERT INTO admin.template (template_id, template_type_id, template_key, name, description, status, details)
VALUES ('61a87121-d0eb-4863-a466-171d200c58db', 1, 'email_reset_pw', 'Forgot password email - reset', 'Reset email when forgot email', 1, '{"lang": {"en": {"body": "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>Reset your password</title></head><body><p>Dear user of Dashin</p><p>This mail has been sent to you because you have requested to reset your password.</p><p>To reset your password follow the link below and type in your new password.</p><p><a href=\"[[LINK_URL]]\">[[LINK_TEXT]]</a></p><p>Best wishes<br>The Dashin team</p></body></html>", "subject": "Dashin password reset"}}}');

INSERT INTO admin.role (role_id, name, description, roles_include, roles_exclude)
VALUES (0, 'WILD_CARD', 'Wild card role', '[]', '[]')
     , (1, 'ROOT', 'Root role', '[0]', '[]')
     , (11, 'SYS_SYSADMIN', 'Permission Administrator', '[0]', '[]')
     , (12, 'SYS_PERMADMIN', 'User Administrator', '[]', '[]')
     , (100, 'SYS_USERADMIN', 'Standard user role', '[]', '[]')
--      , (4000, 'DATA_UPLOAD', 'Allow data upload', '[]', '[]')
     , (10, 'APP_ADMIN', 'Application administrator', '[]', '[]')
     , (500, 'ORG_ADMIN', 'Organization administrator', '[]', '[]')
     , (510, 'ORG_USER', 'Standard user attached to organization', '[]', '[]')
     , (590, 'ORG_READONLY', 'Read-only user attached to organization', '[]', '[]')
;

INSERT INTO admin.roleacl (roleacl_id, name, description, role_id, namespace, class_allow, class_deny, action_allow, action_deny)
VALUES ('0f224d2b-7220-4fe3-adfb-f80618b2d433', 'Root', 'Root access', 1, '*', '["*"]', '[]', '["*"]', '[]')
     , ('e5c6443d-92d4-43f6-aad5-5d7178d91d9d', 'sys-perm', 'Access to permission administration', 11, 'system/controllers/sysadmin', '["*"]', '[]', '["*"]', '[]')
     , ('6caabb4b-a9b3-402f-a328-22c206388164', 'sys-perm', 'Access to permission administration', 11, 'system/controllers', '["sysadmin", "system"]', '[]', '["*"]', '[]')
     , ('2cc92bb5-b848-436c-9f04-9af1b21746f6', 'sys-user', 'Access to user admin', 12, 'system/controllers/admin', '["*"]', '[]', '["*"]', '[]')
     , ('3f7c25c6-1679-492c-892a-a8e88526142e', 'sys-user', 'Access to user admin', 12, 'system/controllers', '["admin", "system"]', '[]', '["*"]', '[]')
     , ('da207d9b-40d9-4e6d-aedb-acf63a15505c', 'user-standard', 'Access to System admin', 100, 'system/controllers/user', '["*"]', '[]', '["*"]', '[]')
     , ('bd77f234-d5d8-4c1a-b924-d3c99e46caed', 'user-standard', 'Access to System admin', 100, 'system/controllers', '["user"]', '[]', '["*"]', '[]')
     , ('26db3ad1-ed29-44cc-908b-648d3d8c19df', 'welcome', 'General user access', 100, 'app/controllers', '["*"]', '[]', '["*"]', '[]')
--      , ('e5d1c7bf-2ea0-4082-8975-8788c7ca3a3a', 'Dat03-owner', 'Allows data upload', 4000, 'dat03/controllers/owner', '["*"]', '[""]', '["*"]', '[""]')
;

INSERT INTO admin.users
  (user_id, code, code_canonical, first_name, last_name, status, password)
VALUES ( '9deee782-ba62-412e-91df-54e58138c174', 'USRAB-1234-5678', 'usrab12345678', 'Administrator', 'password-is-admin'
       , 1, '$2y$10$GvlUdnqZQ1gJ44nhyBsZOOpHU7SOGFoKUhEjJoupW5ZVtKjhr3QDm')
;

-- amdin user
INSERT INTO admin.organization
  (organization_id, name, description, email, status, roles)
VALUES ('1de448a2-f4f1-4491-b6a5-a7064f9090d9', 'Administrator Organization', 'This is the administrator organization account', 'dashin-admin@example.com', '1', '[1]');

INSERT INTO admin.manager
  (user_id, organization_id, label, status, roles)
VALUES ('9deee782-ba62-412e-91df-54e58138c174', '1de448a2-f4f1-4491-b6a5-a7064f9090d9', 'Administrator manager role', '1', '[1]');

UPDATE admin.users
SET active_org_id = '1de448a2-f4f1-4491-b6a5-a7064f9090d9'
WHERE user_id = '9deee782-ba62-412e-91df-54e58138c174';
