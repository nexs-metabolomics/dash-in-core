CREATE TABLE admin.country
(
  country_id INTEGER NOT NULL
    CONSTRAINT country_pkey
      PRIMARY KEY,
  short_name TEXT,
  name TEXT,
  alpha2 TEXT,
  alpha3 TEXT,
  iso3166 INTEGER DEFAULT 0,
  group1 INTEGER
);

ALTER TABLE admin.country OWNER TO :application_user;
