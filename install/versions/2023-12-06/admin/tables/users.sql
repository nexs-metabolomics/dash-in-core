CREATE TABLE admin.users
(
  user_id UUID NOT NULL
    CONSTRAINT users_pkey
      PRIMARY KEY,
  code TEXT NOT NULL
    CONSTRAINT users_code_key
      UNIQUE,
  code_canonical TEXT NOT NULL,
  first_name TEXT,
  last_name TEXT,
  title TEXT,
  label TEXT,
  email TEXT,
  email_canonical TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  password TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  active_org_id UUID,
  details JSONB
);

ALTER TABLE admin.users OWNER TO :application_user;
