CREATE TABLE admin.template_type
(
  template_type_id INTEGER NOT NULL
    CONSTRAINT template_type_pkey
      PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT
);

ALTER TABLE admin.template_type OWNER TO :application_user;
