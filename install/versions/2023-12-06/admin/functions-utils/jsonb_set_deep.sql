CREATE FUNCTION utils.jsonb_set_deep
(
  target JSONB
, path   TEXT[]
, val    JSONB
) RETURNS JSONB
  LANGUAGE plpgsql
AS
$$
DECLARE k TEXT;
        p TEXT[];
BEGIN
  -- Create missing objects in the path.
  FOREACH k IN ARRAY path
    LOOP
      p := p || k;
      IF (target #> p IS NULL) THEN
        target := jsonb_set(target , p , '{}' :: JSONB);
        END IF;
    END LOOP;
  -- Set the value like normal.
  RETURN jsonb_set(target , path , val);
END;
$$;

ALTER FUNCTION utils.jsonb_set_deep(target JSONB, PATH TEXT[], val JSONB) OWNER TO :application_user;
