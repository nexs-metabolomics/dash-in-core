-- for database owned by test user
CREATE SCHEMA IF NOT EXISTS utils;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" SCHEMA utils;
CREATE EXTENSION IF NOT EXISTS btree_gist SCHEMA utils;

ALTER DATABASE :application_dbname SET SEARCH_PATH = public,utils;
