CREATE FUNCTION dashin.datadesign_subject_x_center_exists
(
    _subject_id    UUID
  , _center_id UUID
)
  RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT
  exists(
    SELECT *
    FROM
      -- element combination linked in studydesign
      dashin.study_subject                              a
        -- study-to-data link for individual elements
        INNER JOIN dashin.datadesign_x_study_subject    b ON a.subject_id = b.study_subject_id
        INNER JOIN dashin.datadesign_x_study_center c ON a.center_id = c.study_center_id
                     -- element combination linked in datadesign
        INNER JOIN dashin.datadesign_datatable          e ON (e.subject_id, e.center_id) = (b.datadesign_subject_id, c.datadesign_center_id)
      -- specific element combination
    WHERE
      (a.subject_id, a.center_id) = (_subject_id, _center_id)
    );
$$
;
