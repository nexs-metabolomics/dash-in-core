CREATE FUNCTION dashin.search_metabolomics_summary
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
)
  RETURNS TABLE
          (
            STUDY_ID     UUID,
            DATASET_ID   UUID,
            STUDY_NAME   TEXT,
            DATASET_NAME TEXT,
            MIN_MZ       NUMERIC,
            MAX_MZ       NUMERIC,
            MIN_RT       NUMERIC,
            MAX_RT       NUMERIC,
            N            INT,
            N_ALL        INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry TEXT;
BEGIN
  qry := dashin.search_metabolomics_summary_generate_query(_mz_center , _mz_range , _rt_center , _rt_range);
  IF qry IS NULL THEN
    RETURN;
  END IF;
  RETURN QUERY EXECUTE qry;
  RETURN;
END;
$$
;
ALTER FUNCTION dashin.search_metabolomics_summary OWNER TO :application_user;
