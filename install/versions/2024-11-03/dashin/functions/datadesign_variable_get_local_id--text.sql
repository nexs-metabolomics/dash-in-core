CREATE FUNCTION dashin.datadesign_variable_get_local_id
(
    _dataset_id    UUID
  , _variabletype TEXT
) RETURNS INT
  LANGUAGE sql
  IMMUTABLE STRICT
AS
$$
SELECT local_id
FROM dashin.variable
WHERE dataset_id = _dataset_id
  AND variabletype_id = (
  CASE _variabletype
    WHEN 'subject'       THEN 2
    WHEN 'startgroup'    THEN 3
    WHEN 'event'         THEN 4
    WHEN 'subevent'      THEN 5
    WHEN 'samplingevent' THEN 6
    WHEN 'sampling'      THEN 6
    WHEN 'time'          THEN 7
    WHEN 'samplingtime'  THEN 7
    WHEN 'center'        THEN 8
  END
  );
$$;

ALTER FUNCTION dashin.datadesign_variable_get_local_id(_dataset_id UUID, _variabletype TEXT) OWNER TO :application_user;

