CREATE FUNCTION dashin.datadesign_design_variable_distinct
(
    _dataset_id        UUID
  , _variabletype_name TEXT
  , _owner_org         UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID      UUID,
            LOCAL_ID        INT,
            NAME            TEXT,
            VARIABLE_ID     UUID,
            VARIABLETYPE    TEXT,
            VARIABLETYPE_ID INT
          )
  LANGUAGE sql
AS
$$
SELECT DISTINCT dataset_id
              , local_id
              , jsonb_array_element_text(d.datarow , local_id) AS name
              , variable_id
              , t.name                                         AS variabletype
              , v.variabletype_id::INT
FROM dashin.variable            v
  LEFT JOIN dashin.datatable    d USING (dataset_id)
  LEFT JOIN dashin.variabletype t USING (variabletype_id)
WHERE variable_id IN (
                       SELECT variable_id
                       FROM
                         dashin.datadesign_design_variablenames(_dataset_id , _owner_org)
                       WHERE variabletype_id = (
                         CASE _variabletype_name
                           WHEN 'subject'       THEN 2
                           WHEN 'startgroup'    THEN 3
                           WHEN 'event'         THEN 4
                           WHEN 'subevent'      THEN 5
                           WHEN 'samplingevent' THEN 6
                           WHEN 'sampling'      THEN 6
                           WHEN 'time'          THEN 7
                           WHEN 'samplingtime'  THEN 7
                           WHEN 'center'        THEN 8
                         END)
                     )
ORDER BY name
$$
;

ALTER FUNCTION dashin.datadesign_design_variable_distinct(_dataset_id UUID, _variabletype_name TEXT, _owner_org UUID) OWNER TO :application_user;

