CREATE FUNCTION dashin.datadesign_subject_event_sequence_text
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATASET_ID     UUID,
            SUBJECT        TEXT,
            STARTGROUP     TEXT,
            EVENT_SEQUENCE TEXT
          )
  LANGUAGE sql
AS
$$
SELECT dataset_id
     , subject
     , startgroup
     , string_agg(group_event , ', ' ORDER BY event,subevent) AS event_sequence
FROM (
       SELECT dataset_id
            , '[' || event || ', ' || subevent || ']' AS group_event
            , subject
            , startgroup
            , event
            , subevent
       FROM (
              SELECT dataset_id::UUID
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))      AS event
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))   AS subevent
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup')) AS startgroup
                   , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))    AS subject
              FROM dashin.datatable      dt
                LEFT JOIN dashin.dataset d USING (dataset_id)
              WHERE dt.dataset_id = _dataset_id
                AND (d.owner_org = _owner_org OR _owner_org IS NULL)
              GROUP BY dataset_id
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'event'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subevent'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'startgroup'))
                     , jsonb_array_element_text(datarow , dashin.datadesign_variable_get_local_id(dataset_id , 'subject'))
            ) x0
     ) x1
GROUP BY dataset_id
       , startgroup
       , subject
$$;

ALTER FUNCTION dashin.datadesign_subject_event_sequence_text OWNER TO :application_user;

