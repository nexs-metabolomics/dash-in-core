CREATE TABLE dashin.studycontact
(
  studycontact_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id        UUID,
  first_name      TEXT,
  last_name       TEXT,
  email           TEXT,
  description     TEXT,
  status          INT DEFAULT 1 NOT NULL,
  study_role_id   INT DEFAULT 0 NOT NULL
);
ALTER TABLE dashin.studycontact OWNER TO :application_user;

