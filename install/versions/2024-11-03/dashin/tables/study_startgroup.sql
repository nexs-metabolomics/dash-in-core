CREATE TABLE dashin.study_startgroup
(
  startgroup_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_startgroup_pk
      PRIMARY KEY,
  study_id      UUID,
  ordinal       INT,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name          TEXT,
  label         TEXT,
  description   TEXT,
  -- needed by fk from studydesign_startgroup_x_subject
  CONSTRAINT study_startgroup_startgroup_id_study_id_uk UNIQUE (startgroup_id, study_id),
  CONSTRAINT study_startgroup_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_startgroup OWNER TO :application_user;
