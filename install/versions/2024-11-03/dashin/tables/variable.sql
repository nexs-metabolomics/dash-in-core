CREATE TABLE dashin.variable
(
  variable_id      UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT variable_pk
      PRIMARY KEY,
  local_id         INTEGER NOT NULL,
  dataset_id       UUID,
  name             TEXT,
  description      TEXT,
  status           INT DEFAULT 1,
  variabletype_id  INT DEFAULT 1,
  datatype_id      INT DEFAULT 1,
  researchfield_id INT DEFAULT 1,
  assay_id         UUID,
  nunitprefix_id   INT DEFAULT 0,
  nunit_id         INT DEFAULT 0,
  dunitprefix_id   INT DEFAULT 0,
  dunit_id         INT DEFAULT 0,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB,
  CONSTRAINT dataset_local_id_uk UNIQUE (dataset_id, local_id),
  CONSTRAINT variable_variable_id_variabletype_id_uk
    UNIQUE (variable_id, variabletype_id),
  -- needed by studyvariable_x_variable (prevent dataset-variable mismatch in studyvariable and studyrowset)
  CONSTRAINT variable_dataset_id_variable_id_uk
    UNIQUE (dataset_id, variable_id)
);

ALTER TABLE dashin.variable OWNER TO :application_user;

