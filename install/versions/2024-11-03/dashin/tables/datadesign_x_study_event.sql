CREATE TABLE dashin.datadesign_x_study_event
(
  dataset_id          UUID,
  datadesign_event_id UUID,
  study_id            UUID,
  study_event_id      UUID,
  CONSTRAINT dxs_event_pk PRIMARY KEY (datadesign_event_id),
  CONSTRAINT dxs_dataset_id_event_id_uk UNIQUE (dataset_id, study_event_id)
);

ALTER TABLE dashin.datadesign_x_study_event OWNER TO :application_user;

