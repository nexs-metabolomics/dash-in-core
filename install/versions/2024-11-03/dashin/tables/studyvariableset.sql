CREATE TABLE dashin.studyvariableset
(
  studyvariableset_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id            UUID,
  name                TEXT,
  description         TEXT,
  created_at          TIMESTAMPTZ DEFAULT now(),
  updated_at          TIMESTAMPTZ DEFAULT now(),
  CONSTRAINT studyvariableset_study_id_studyvariableset_id_uk UNIQUE (studyvariableset_id, study_id)
);

ALTER TABLE dashin.studyvariableset OWNER TO :application_user;
