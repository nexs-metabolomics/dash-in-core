CREATE TABLE dashin.datatype
(
  datatype_id SMALLINT NOT NULL
    CONSTRAINT datatype_pk
      PRIMARY KEY,
  name        TEXT,
  description TEXT
);

ALTER TABLE dashin.datatype OWNER TO :application_user;
