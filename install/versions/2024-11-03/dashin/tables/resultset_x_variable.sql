CREATE TABLE dashin.resultset_x_variable
(
  resultset_id UUID NOT NULL,
  variable_id  UUID NOT NULL,
  CONSTRAINT resultset_x_variable_pk PRIMARY KEY (resultset_id, variable_id)
);

ALTER TABLE dashin.resultset_x_variable OWNER TO :application_user;

