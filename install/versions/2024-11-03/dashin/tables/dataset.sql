CREATE TABLE dashin.dataset
(
  dataset_id     UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT dataset_pkey
      PRIMARY KEY,
  name           TEXT,
  description    TEXT,
  status         SMALLINT DEFAULT 1,
  datasettype_id INT DEFAULT 1,
  owner_org      UUID,
  owner_user     UUID,
  created_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at     TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details        JSONB,
  CONSTRAINT dataset_owner_key
    UNIQUE (dataset_id, owner_org)
);

ALTER TABLE dashin.dataset OWNER TO :application_user;
