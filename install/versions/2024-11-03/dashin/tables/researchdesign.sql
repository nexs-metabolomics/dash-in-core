CREATE TABLE dashin.researchdesign
(
  researchdesign_id INTEGER NOT NULL
    CONSTRAINT researchdesign_pk
      PRIMARY KEY,
  name              TEXT NOT NULL,
  description       TEXT
);

ALTER TABLE dashin.researchdesign OWNER TO :application_user;
