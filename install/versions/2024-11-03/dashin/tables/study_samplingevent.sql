CREATE TABLE dashin.study_samplingevent
(
  samplingevent_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT study_samplingevent_pk
      PRIMARY KEY,
  sampling_type_id INTEGER,
  study_id         UUID NOT NULL,
  ordinal          INTEGER,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name             TEXT,
  label            TEXT,
  description      TEXT,
  row_comment      TEXT,
  additional_data  JSONB,
  CONSTRAINT study_samplingevent_study_name_uk UNIQUE (study_id, name)
);

ALTER TABLE dashin.study_samplingevent OWNER TO :application_user;

