CREATE TABLE IF NOT EXISTS dashin.study_x_supportfile
(
  study_id       UUID NOT NULL,
  supportfile_id UUID NOT NULL,
  CONSTRAINT study_x_supportfile_pk PRIMARY KEY (study_id, supportfile_id)
);

ALTER TABLE dashin.study_x_supportfile OWNER TO :application_user;
