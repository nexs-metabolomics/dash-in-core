CREATE TABLE dashin.studydesign_subevent_type
(
  subevent_type_id       INTEGER NOT NULL
    CONSTRAINT studydesign_subevent_type_pk
      PRIMARY KEY,
  ordinal                TEXT,
  created_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at             TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  name                   TEXT NOT NULL,
  description            TEXT,
  additional_data_schema JSONB
);

ALTER TABLE dashin.studydesign_subevent_type OWNER TO :application_user;
