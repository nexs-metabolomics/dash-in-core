CREATE TABLE dashin.consortium
(
  consortium_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT consortium_pk
      PRIMARY KEY,
  name          TEXT,
  description   TEXT,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details       JSONB
);

ALTER TABLE dashin.consortium OWNER TO :application_user;
