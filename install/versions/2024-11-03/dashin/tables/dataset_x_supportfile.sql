CREATE TABLE IF NOT EXISTS dashin.dataset_x_supportfile
(
  dataset_id     UUID NOT NULL,
  supportfile_id UUID NOT NULL,
  CONSTRAINT dataset_x_supportfile_pk PRIMARY KEY (dataset_id, supportfile_id)
);

ALTER TABLE dashin.dataset_x_supportfile OWNER TO :application_user;
