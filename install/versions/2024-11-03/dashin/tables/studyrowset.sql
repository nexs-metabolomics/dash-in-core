CREATE TABLE dashin.studyrowset
(
  studyrowset_id UUID CONSTRAINT studyrowset_pk PRIMARY KEY DEFAULT uuid_generate_v4(),
  study_id       UUID,
  name           TEXT,
  description    TEXT,
  created_at     TIMESTAMPTZ DEFAULT now(),
  updated_at     TIMESTAMPTZ DEFAULT now(),
  CONSTRAINT studyrowset_study_id_studyrowset_id_uk UNIQUE (studyrowset_id, study_id)
);

ALTER TABLE dashin.studyrowset OWNER TO :application_user;
