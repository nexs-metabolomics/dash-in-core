CREATE TABLE dashin.studyvariablecondition_x_category
(
  studyvariablecondition_id UUID,
  study_id                  UUID,
  element_id                UUID,
  value_column              TEXT,
  CONSTRAINT studyvariablecondition_x_category_var_id_element_id_uk UNIQUE (studyvariablecondition_id, study_id, element_id),
  CONSTRAINT studyvariablecondition_x_category_var_id_value_uk UNIQUE (studyvariablecondition_id, value_column)
);

ALTER TABLE dashin.studyvariablecondition_x_category OWNER TO :application_user;
