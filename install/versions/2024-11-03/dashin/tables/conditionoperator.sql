CREATE TABLE dashin.conditionoperator
(
    conditionoperator_id INT PRIMARY KEY,
    context_flag         INT NOT NULL,
    symbol               TEXT NOT NULL,
    shortname            TEXT NOT NULL,
    name                 TEXT NOT NULL,
    label                TEXT NOT NULL,
    description          TEXT NOT NULL
);

ALTER TABLE dashin.conditionoperator OWNER TO :application_user;

