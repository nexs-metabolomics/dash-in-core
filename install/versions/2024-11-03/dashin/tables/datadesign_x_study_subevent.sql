CREATE TABLE dashin.datadesign_x_study_subevent
(
  dataset_id             UUID,
  datadesign_subevent_id UUID PRIMARY KEY,
  study_id               UUID,
  study_subevent_id      UUID,
  CONSTRAINT dxs_dataset_id_subevent_id_key UNIQUE (dataset_id, study_subevent_id)
);

ALTER TABLE dashin.datadesign_x_study_subevent OWNER TO :application_user;
