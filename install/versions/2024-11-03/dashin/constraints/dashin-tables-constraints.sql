-- ------------------------------------------------------------
ALTER TABLE dashin.assay
  ADD CONSTRAINT assay_researchfield_id_fk
    FOREIGN KEY (researchfield_id) REFERENCES dashin.researchfield (researchfield_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.resultset
  ADD CONSTRAINT resultset_owner_user_fk
    FOREIGN KEY (owner_user) REFERENCES admin.users (user_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.resultset_x_variable
  ADD CONSTRAINT resultset_x_variable_resultset_id_fk
    FOREIGN KEY (resultset_id) REFERENCES dashin.resultset (resultset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.resultset_x_variable
  ADD CONSTRAINT resultset_x_variable_variable_id_fk
    FOREIGN KEY (variable_id) REFERENCES dashin.variable (variable_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.search_column_type
  ADD CONSTRAINT search_column_type_researchfield_id_fk
    FOREIGN KEY (researchfield_id) REFERENCES dashin.researchfield (researchfield_id);

ALTER TABLE dashin.search_column_type
  ADD CONSTRAINT search_column_type_datatype_id_fk
    FOREIGN KEY (datatype_id) REFERENCES dashin.datatype (datatype_id);

-- ------------------------------------------------------------
-- upvar_dataset
-- ------------------------------------------------------------
ALTER TABLE dashin.upvar_dataset
  ADD CONSTRAINT upvar_dataset_owner_org_fk
    FOREIGN KEY (owner_org) REFERENCES admin.organization (organization_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.upvar_dataset
  ADD CONSTRAINT upvar_dataset_owner_user_fk
    FOREIGN KEY (owner_user) REFERENCES admin.users (user_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.upvar_datatable
  ADD CONSTRAINT upvar_datatable_upvar_dataset_id_fk
    FOREIGN KEY (upvar_dataset_id) REFERENCES dashin.upvar_dataset (upvar_dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.upvar_variable
  ADD CONSTRAINT upvar_variable_upvar_dataset_id_fk
    FOREIGN KEY (upvar_dataset_id) REFERENCES dashin.upvar_dataset (upvar_dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.upvar_variable
  ADD CONSTRAINT upvar_variable_datatype_id_fk
    FOREIGN KEY (datatype_id) REFERENCES dashin.datatype (datatype_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
-- var_dataset
-- ------------------------------------------------------------
ALTER TABLE dashin.var_dataset
  ADD CONSTRAINT var_dataset_dataset_id_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.var_datatable
  ADD CONSTRAINT var_datatable_variable_fk
    FOREIGN KEY (variable_id) REFERENCES dashin.variable (variable_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.var_datatable
  ADD CONSTRAINT var_datatable_var_dataset_fk
    FOREIGN KEY (var_dataset_id) REFERENCES dashin.var_dataset (var_dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.var_variable
  ADD CONSTRAINT var_variable_var_dataset_id_fk
    FOREIGN KEY (var_dataset_id) REFERENCES dashin.var_dataset (var_dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ALTER TABLE dashin.var_variable
--   ADD CONSTRAINT var_variable_search_single_id_fk
--     FOREIGN KEY (search_single_id) REFERENCES dashin.search_column_type ON UPDATE CASCADE;
-- 
-- ALTER TABLE dashin.var_variable
--   ADD CONSTRAINT var_variable_search_generic_id_fk
--     FOREIGN KEY (search_generic_id) REFERENCES dashin.search_column_type ON UPDATE CASCADE;

ALTER TABLE dashin.var_variable
  ADD CONSTRAINT var_variable_datatype_id_fk
    FOREIGN KEY (datatype_id) REFERENCES dashin.datatype (datatype_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.var_variable_x_search_column_type
  ADD CONSTRAINT var_variable_x_search_column_type_search_column_type_id_fk
    FOREIGN KEY (search_column_type_id) REFERENCES dashin.search_column_type (search_column_type_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.var_variable_x_search_column_type
  ADD CONSTRAINT var_variable_x_search_column_type_var_variable_id_fk
    FOREIGN KEY (var_variable_id) REFERENCES dashin.var_variable (var_variable_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.var_variable_x_search_column_type
  ADD CONSTRAINT var_variable_x_search_column_type_dataset_var_dataset
    FOREIGN KEY (var_dataset_id, dataset_id) REFERENCES dashin.var_dataset (var_dataset_id, dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- dataset
-- ------------------------------------------------------------
ALTER TABLE dashin.datatable
  ADD CONSTRAINT datatable_dataset_id_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_dataset_id_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_variabletype_id_fk
    FOREIGN KEY (variabletype_id) REFERENCES dashin.variabletype (variabletype_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_datatype_id_fk
    FOREIGN KEY (datatype_id) REFERENCES dashin.datatype (datatype_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_researchfield_id_fk
    FOREIGN KEY (researchfield_id) REFERENCES dashin.researchfield (researchfield_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_assay_id_fk
    FOREIGN KEY (assay_id) REFERENCES dashin.assay (assay_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_nmerator_unit_fk
    FOREIGN KEY (nunitprefix_id, nunit_id) REFERENCES dashin.prefixedunit (unitprefix_id, unit_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.variable
  ADD CONSTRAINT variable_denominator_unit_fk
    FOREIGN KEY (dunitprefix_id, dunit_id) REFERENCES dashin.prefixedunit (unitprefix_id, unit_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
-- study
-- ------------------------------------------------------------
ALTER TABLE dashin.study
  ADD CONSTRAINT study_country_id_fk
    FOREIGN KEY (country_id) REFERENCES admin.country (country_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.study
  ADD CONSTRAINT study_consortium_id_fk
    FOREIGN KEY (consortium_id) REFERENCES dashin.consortium (consortium_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.study
  ADD CONSTRAINT study_researchdesign_id_fk
    FOREIGN KEY (researchdesign_id) REFERENCES dashin.researchdesign (researchdesign_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.study
  ADD CONSTRAINT study_owner_org_fk
    FOREIGN KEY (owner_org) REFERENCES admin.organization (organization_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.study
  ADD CONSTRAINT study_owner_user_fk
    FOREIGN KEY (owner_user) REFERENCES admin.users (user_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
ALTER TABLE dashin.studycontact
  ADD CONSTRAINT studycontact_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studycontact
  ADD CONSTRAINT studycontact_study_role_id_fk
    FOREIGN KEY (study_role_id) REFERENCES dashin.study_role (study_role_id)
      ON UPDATE CASCADE ON DELETE SET DEFAULT;

-- ------------------------------------------------------------
-- dataset-x-study
-- ------------------------------------------------------------
ALTER TABLE dashin.dataset_x_study
  ADD CONSTRAINT dataset_x_study_dataset_id_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset (dataset_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.dataset_x_study
  ADD CONSTRAINT dataset_x_study_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- studydesign
-- ------------------------------------------------------------
-- import datadesign
-- ALTER TABLE dashin.studydesign_x_datadesign ADD CONSTRAINT studydesign_x_datadesign_study_id_fk FOREIGN KEY (study_id)
--     REFERENCES dashin.study ON UPDATE CASCADE ON DELETE CASCADE;
-- ALTER TABLE dashin.studydesign_x_datadesign ADD CONSTRAINT studydesign_x_datadesign_dataset_id_fk FOREIGN KEY (dataset_id)
--     REFERENCES dashin.dataset ON UPDATE CASCADE ON DELETE SET NULL;
-- 
-- UPDATE dashin.studydesign_x_datadesign
-- SET
--     dataset_id = NULL
-- WHERE dataset_id NOT IN (
--                             SELECT dataset_id
--                             FROM dashin.dataset
--                         );

-- ------------------------------------------------------------
-- selection choices
-- ------------------------------------------------------------
ALTER TABLE dashin.selection_choice
  ADD CONSTRAINT selection_choice_selection_type_id_fk
    FOREIGN KEY (selection_type_id) REFERENCES dashin.selection_type;

-- ------------------------------------------------------------
-- event_x...
-- ------------------------------------------------------------
ALTER TABLE dashin.study_event
  ADD CONSTRAINT study_event_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_event_x_subevent
  ADD CONSTRAINT study_event_x_subevent_event_id_fk
    FOREIGN KEY (event_id) REFERENCES dashin.study_event (event_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_event_x_subevent
  ADD CONSTRAINT study_event_x_subevent_subevent_id_fk
    FOREIGN KEY (subevent_id) REFERENCES dashin.study_subevent (subevent_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_event_x_subevent
  ADD CONSTRAINT study_event_x_subevent_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_event_subevent_x_startgroup
  ADD CONSTRAINT study_event_subevent_x_startgroup_event_subevent_fk
    FOREIGN KEY (event_id, subevent_id) REFERENCES dashin.study_event_x_subevent (event_id, subevent_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_event_subevent_x_startgroup
  ADD CONSTRAINT study_event_subevent_x_startgroup_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- subevent
-- ------------------------------------------------------------
ALTER TABLE dashin.study_subevent
  ADD CONSTRAINT study_subevent_subevent_type_id_fk
    FOREIGN KEY (subevent_type_id) REFERENCES dashin.studydesign_subevent_type;

ALTER TABLE dashin.study_subevent
  ADD CONSTRAINT study_subevent_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- subevent_x...
-- ------------------------------------------------------------
ALTER TABLE dashin.study_subevent_x_samplingevent
  ADD CONSTRAINT study_subevent_x_samplingevent_subevent_id_fk
    FOREIGN KEY (subevent_id) REFERENCES dashin.study_subevent (subevent_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_subevent_x_samplingevent
  ADD CONSTRAINT study_subevent_x_samplingevent_samplingevent_id_fk
    FOREIGN KEY (samplingevent_id) REFERENCES dashin.study_samplingevent (samplingevent_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_subevent_x_samplingevent
  ADD CONSTRAINT study_subevent_x_samplingevent_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- sampling_event
-- ------------------------------------------------------------
ALTER TABLE dashin.study_samplingevent
  ADD CONSTRAINT study_samplingevent_sampling_type_id_fk
    FOREIGN KEY (sampling_type_id) REFERENCES dashin.studydesign_sampling_type (sampling_type_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.study_samplingevent
  ADD CONSTRAINT study_samplingevent_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- sampling_time
-- ------------------------------------------------------------
ALTER TABLE dashin.study_samplingtime
  ADD CONSTRAINT study_samplingtime_samplingevent_id_fk
    FOREIGN KEY (samplingevent_id) REFERENCES dashin.study_samplingevent (samplingevent_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- center
-- ------------------------------------------------------------
ALTER TABLE dashin.study_center
  ADD CONSTRAINT study_center_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- startgroup
-- ------------------------------------------------------------
ALTER TABLE dashin.study_startgroup
  ADD CONSTRAINT study_startgroup_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- subject
-- ------------------------------------------------------------
ALTER TABLE dashin.study_subject
  ADD CONSTRAINT study_subject_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_subject
  ADD CONSTRAINT study_subject_center_fk
    FOREIGN KEY (center_id) REFERENCES dashin.study_center (center_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.study_subject
  ADD CONSTRAINT study_subject_startgroup_fk
    FOREIGN KEY (startgroup_id) REFERENCES dashin.study_startgroup (startgroup_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
-- datadesign-x-studydesign
-- ------------------------------------------------------------

-- ------------------------------------------------------------
-- datadesign
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign
  ADD CONSTRAINT datadesign_dataset_id_owner_org_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- datadesign_datatable
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_datarow_id_fk
    FOREIGN KEY (datarow_id, dataset_id) REFERENCES dashin.datatable (datarow_id, dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- event
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_event_fk
    FOREIGN KEY (event_id) REFERENCES dashin.datadesign_x_study_event (datadesign_event_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_event
  ADD CONSTRAINT d_x_s_event_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_event
  ADD CONSTRAINT d_x_s_event_studydesign_fk
    FOREIGN KEY (study_event_id) REFERENCES dashin.study_event (event_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_event
  ADD CONSTRAINT d_x_s_event_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- subevent
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_subevent_fk
    FOREIGN KEY (subevent_id) REFERENCES dashin.datadesign_x_study_subevent (datadesign_subevent_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_subevent
  ADD CONSTRAINT d_x_s_subevent_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_subevent
  ADD CONSTRAINT d_x_s_subevent_studydesign_fk
    FOREIGN KEY (study_subevent_id) REFERENCES dashin.study_subevent (subevent_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_subevent
  ADD CONSTRAINT d_x_s_subevent_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- samplingevent
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_samplingevent_fk
    FOREIGN KEY (samplingevent_id) REFERENCES dashin.datadesign_x_study_samplingevent (datadesign_samplingevent_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_samplingevent
  ADD CONSTRAINT d_x_s_samplingevent_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_samplingevent
  ADD CONSTRAINT d_x_s_samplingevent_studydesign_fk
    FOREIGN KEY (study_samplingevent_id) REFERENCES dashin.study_samplingevent (samplingevent_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_samplingevent
  ADD CONSTRAINT d_x_s_samplingevent_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- samplingtime
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_samplingtime_fk
    FOREIGN KEY (samplingtime_id) REFERENCES dashin.datadesign_x_study_samplingtime (datadesign_samplingtime_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_samplingtime
  ADD CONSTRAINT d_x_s_samplingtime_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_samplingtime
  ADD CONSTRAINT d_x_s_samplingtime_studydesign_fk
    FOREIGN KEY (study_samplingtime_id) REFERENCES dashin.study_samplingtime (samplingtime_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_samplingtime
  ADD CONSTRAINT d_x_s_samplingtime_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- center
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_center_fk
    FOREIGN KEY (center_id) REFERENCES dashin.datadesign_x_study_center (datadesign_center_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_center
  ADD CONSTRAINT d_x_s_center_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_center
  ADD CONSTRAINT d_x_s_center_studydesign_fk
    FOREIGN KEY (study_center_id) REFERENCES dashin.study_center (center_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_center
  ADD CONSTRAINT d_x_s_center_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- startgroup
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_startgroup_fk
    FOREIGN KEY (startgroup_id) REFERENCES dashin.datadesign_x_study_startgroup (datadesign_startgroup_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_startgroup
  ADD CONSTRAINT d_x_s_startgroup_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_startgroup
  ADD CONSTRAINT d_x_s_startgroup_studydesign_fk
    FOREIGN KEY (study_startgroup_id) REFERENCES dashin.study_startgroup (startgroup_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_startgroup
  ADD CONSTRAINT d_x_s_startgroup_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- subject
-- ------------------------------------------------------------
ALTER TABLE dashin.datadesign_datatable
  ADD CONSTRAINT datadesign_datatable_subject_fk
    FOREIGN KEY (subject_id) REFERENCES dashin.datadesign_x_study_subject (datadesign_subject_id)
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE dashin.datadesign_x_study_subject
  ADD CONSTRAINT d_x_s_subject_datadesign_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.datadesign (dataset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.datadesign_x_study_subject
  ADD CONSTRAINT d_x_s_subject_studydesign_fk
    FOREIGN KEY (study_subject_id) REFERENCES dashin.study_subject (subject_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.datadesign_x_study_subject
  ADD CONSTRAINT d_x_s_subject_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

-- ------------------------------------------------------------
-- studyvariable
-- ------------------------------------------------------------
-- link to datatype
ALTER TABLE dashin.studyvariable
  ADD CONSTRAINT studyvariable_datatype_fk
    FOREIGN KEY (datatype_id) REFERENCES dashin.datatype (datatype_id)
      ON UPDATE CASCADE;

ALTER TABLE dashin.studyvariable
  ADD CONSTRAINT studyvariable_variabletype_id_fk
    FOREIGN KEY (variabletype_id) REFERENCES dashin.variabletype (variabletype_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
-- link to studydataset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariable
  ADD CONSTRAINT studyvariable_studydataset_fk
    FOREIGN KEY (study_id) REFERENCES dashin.studydataset (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- link to dataset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariable_x_variable
  ADD CONSTRAINT studyvariable_x_variable_studyvariable_fk
    FOREIGN KEY (study_id, studyvariable_id) REFERENCES dashin.studyvariable (study_id, studyvariable_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studyvariable_x_variable
  ADD CONSTRAINT studyvariable_x_variable_dataset_fk
    FOREIGN KEY (dataset_id, variable_id) REFERENCES dashin.variable (dataset_id, variable_id);

ALTER TABLE dashin.studyvariable_x_variable
  ADD CONSTRAINT studyvariable_x_variable_fk
    FOREIGN KEY (dataset_id, study_id) REFERENCES dashin.dataset_x_study (dataset_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- link to dataset
-- ------------------------------------------------------------
-- studydesign dataset
-- ------------------------------------------------------------
ALTER TABLE dashin.studydataset
  ADD CONSTRAINT studydataset_study_id_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ensure linked datarow belongs to dataset in study - remove link if dataset removed from study
ALTER TABLE dashin.studyrow_x_datarow
  ADD CONSTRAINT studyrow_x_datarow_dataset_fk
    FOREIGN KEY (dataset_id, study_id) REFERENCES dashin.dataset_x_study (dataset_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ensure linked studyrow-datarow exists in studydata - remove link if datairemoved
ALTER TABLE dashin.studyrow_x_datarow
  ADD CONSTRAINT studyrow_x_datarow_studyrow_fk
    FOREIGN KEY (study_id, studyrow_id) REFERENCES dashin.studydatatable (study_id, studyrow_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ensure linked datarow exists in dataset - remove if dataset removed
ALTER TABLE dashin.studyrow_x_datarow
  ADD CONSTRAINT studyrow_x_datarow_datarow_fk
    FOREIGN KEY (dataset_id, datarow_id) REFERENCES dashin.datadesign_datatable (dataset_id, datarow_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- studydatatable
-- ------------------------------------------------------------
ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_studydataset_fk
    FOREIGN KEY (study_id) REFERENCES dashin.studydataset (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_event_id
    FOREIGN KEY (event_id) REFERENCES dashin.study_event (event_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_subevent_id
    FOREIGN KEY (subevent_id) REFERENCES dashin.study_subevent (subevent_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_samplingevent_id
    FOREIGN KEY (samplingevent_id) REFERENCES dashin.study_samplingevent (samplingevent_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_samplingtime_id
    FOREIGN KEY (samplingtime_id) REFERENCES dashin.study_samplingtime (samplingtime_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_center_id
    FOREIGN KEY (center_id) REFERENCES dashin.study_center (center_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_startgroup_id
    FOREIGN KEY (startgroup_id) REFERENCES dashin.study_startgroup (startgroup_id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE dashin.studydatatable
  ADD CONSTRAINT studydatatable_subject_id
    FOREIGN KEY (subject_id) REFERENCES dashin.study_subject (subject_id)
      DEFERRABLE INITIALLY DEFERRED;

-- ------------------------------------------------------------
-- studyvariablecondition
-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariablecondition
  ADD CONSTRAINT studyvariablecondition_studyvariable_fk
    FOREIGN KEY (study_id, studyvariable_id) REFERENCES dashin.studyvariable (study_id, studyvariable_id)
      ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE dashin.studyvariablecondition
  ADD CONSTRAINT studyvariablecondition_conditionoperator_fk
    FOREIGN KEY (conditionoperator_id) REFERENCES dashin.conditionoperator (conditionoperator_id)
      ON UPDATE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariablecondition_x_category
  ADD CONSTRAINT studyvarcond_x_category_studyvarcond_fk
    FOREIGN KEY (studyvariablecondition_id, study_id) REFERENCES dashin.studyvariablecondition (studyvariablecondition_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariablecondition_x_samplingtimecategory
  ADD CONSTRAINT studyvarcond_x_samptimecat_studyvarcond_fk
    FOREIGN KEY (studyvariablecondition_id, study_id) REFERENCES dashin.studyvariablecondition (studyvariablecondition_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studyvariablecondition_x_samplingtimecategory
  ADD CONSTRAINT studvarcond_x_samptimecat_studyvarcat_fk
    FOREIGN KEY (studyvariablecondition_id, study_id, samplingevent_id) REFERENCES dashin.studyvariablecondition_x_category (studyvariablecondition_id, study_id, element_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- studyrowset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyrowset
  ADD CONSTRAINT studyrowset_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE dashin.studyrowset_x_studyvariable
--   ADD CONSTRAINT studyrowset_x_studyvariable_studyrowset_fk
--     FOREIGN KEY (study_id, studyrowset_id) REFERENCES dashin.studyrowset (study_id, studyrowset_id)
--       ON DELETE CASCADE ON UPDATE CASCADE;
-- 
-- ALTER TABLE dashin.studyrowset_x_studyvariable
--   ADD CONSTRAINT studyrowset_x_studyvariable_studyvariable_fk
--     FOREIGN KEY (study_id, studyvariable_id) REFERENCES dashin.studyvariable (study_id, studyvariable_id)
--       ON DELETE CASCADE ON UPDATE CASCADE;

-- ------------------------------------------------------------
-- NEW TABLES
-- ------------------------------------------------------------
-- ALTER TABLE dashin.studyrowset_x_studyvariablecondition
--   ADD CONSTRAINT studyrowset_x_studyvarcond_studyrowset_fk
--     FOREIGN KEY (study_id, studyrowset_id) REFERENCES dashin.studyrowset (study_id, studyrowset_id)
--       ON UPDATE CASCADE ON DELETE CASCADE;
-- 
-- ALTER TABLE dashin.studyrowset_x_studyvariablecondition
--   ADD CONSTRAINT studyrowset_x_studyvarcond_studyvarcond_fk
--     FOREIGN KEY (study_id, studyvariablecondition_id) REFERENCES dashin.studyvariablecondition (study_id, studyvariablecondition_id)
--       ON UPDATE CASCADE ON DELETE CASCADE;
-- 
-- ------------------------------------------------------------
-- ------------------------------------------------------------
-- studyrowsubset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyrowsubset
  ADD CONSTRAINT studyrowsubset_studyrowset_id_fk
    FOREIGN KEY (studyrowset_id, study_id) REFERENCES dashin.studyrowset (studyrowset_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studyrowsubset_x_studyvariablecondition
  ADD CONSTRAINT studyrowsubset_x_studyvariablecondition_subset_fk
    FOREIGN KEY (studyrowsubset_id, study_id) REFERENCES dashin.studyrowsubset (studyrowsubset_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studyrowsubset_x_studyvariablecondition
  ADD CONSTRAINT studyrowsubset_x_studyvariablecondition_studyvarcond_fk
    FOREIGN KEY (studyvariablecondition_id, study_id) REFERENCES dashin.studyvariablecondition (studyvariablecondition_id, study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;



-- ------------------------------------------------------------
-- studyvariableset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariableset
  ADD CONSTRAINT studyvariableset_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
ALTER TABLE dashin.studyvariableset_x_studyvariable
  ADD CONSTRAINT studyvariableset_x_studyvariable_studyvariableset_fk
    FOREIGN KEY (study_id, studyvariableset_id) REFERENCES dashin.studyvariableset (study_id, studyvariableset_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.studyvariableset_x_studyvariable
  ADD CONSTRAINT studyvariableset_x_studyvariable_studyvariable_fk
    FOREIGN KEY (study_id, studyvariable_id) REFERENCES dashin.studyvariable (study_id, studyvariable_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- studyexportset
-- ------------------------------------------------------------
ALTER TABLE dashin.studyexportset
  ADD CONSTRAINT studyexportset_studyvariableset_fk
    FOREIGN KEY (studyvariableset_id, study_id) REFERENCES dashin.studyvariableset (studyvariableset_id, study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.studyexportset
  ADD CONSTRAINT studyexportset_studyrowset_fk
    FOREIGN KEY (studyrowset_id, study_id) REFERENCES dashin.studyrowset (studyrowset_id, study_id)
      ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE dashin.studyexportset
  ADD CONSTRAINT studyexportset_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study (study_id)
      ON UPDATE CASCADE ON DELETE CASCADE;

-- ------------------------------------------------------------
-- data import guide
-- ------------------------------------------------------------
-- datasettype
ALTER TABLE dashin.dataset
  ADD CONSTRAINT dataset_datasettype_fk
    FOREIGN KEY (datasettype_id) REFERENCES dashin.datasettype (datasettype_id)
      ON UPDATE CASCADE ON DELETE NO ACTION;

-- ------------------------------------------------------------
-- support files
-- ------------------------------------------------------------
-- study
ALTER TABLE dashin.study_x_supportfile
  ADD CONSTRAINT study_x_supportfile_study_fk
    FOREIGN KEY (study_id) REFERENCES dashin.study
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.study_x_supportfile
  ADD CONSTRAINT study_x_supportfile_supportfile_fk
    FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
      ON UPDATE CASCADE ON DELETE CASCADE;

-- dataset
ALTER TABLE dashin.dataset_x_supportfile
  ADD CONSTRAINT dataset_x_supportfile_study_fk
    FOREIGN KEY (dataset_id) REFERENCES dashin.dataset
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.dataset_x_supportfile
  ADD CONSTRAINT dataset_x_supportfile_supportfile_fk
    FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
      ON UPDATE CASCADE ON DELETE CASCADE;

-- assay
ALTER TABLE dashin.assay_x_supportfile
  ADD CONSTRAINT assay_x_supportfile_study_fk
    FOREIGN KEY (assay_id) REFERENCES dashin.assay
      ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE dashin.assay_x_supportfile
  ADD CONSTRAINT assay_x_supportfile_supportfile_fk
    FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
      ON UPDATE CASCADE ON DELETE CASCADE;
