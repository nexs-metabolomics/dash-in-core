CREATE TABLE admin.password_reset_token
(
  token UUID NOT NULL
    CONSTRAINT password_reset_token_pkey
      PRIMARY KEY,
  user_id UUID,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  status INTEGER DEFAULT 1,
  details JSONB
);

ALTER TABLE admin.password_reset_token OWNER TO :application_user;
