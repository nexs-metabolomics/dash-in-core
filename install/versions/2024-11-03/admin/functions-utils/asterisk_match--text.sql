CREATE FUNCTION utils.asterisk_match
(
  x   TEXT
, col TEXT
) RETURNS BOOLEAN
  STABLE
  LANGUAGE SQL
AS
$$
SELECT
  CASE WHEN col = '*' THEN TRUE
       ELSE col = x
  END
$$;

ALTER FUNCTION utils.asterisk_match(x TEXT, col TEXT) OWNER TO :application_user;
