CREATE FUNCTION utils.jsonb_object_to_array_by_keys
(
  obj  JSONB
, keys JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_agg(obj -> n)
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_object_to_array_by_keys(obj JSONB, keys JSONB) OWNER TO :application_user;
