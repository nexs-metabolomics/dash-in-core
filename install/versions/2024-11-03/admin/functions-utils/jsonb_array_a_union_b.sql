CREATE FUNCTION utils.jsonb_array_a_union_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  ARRAY_TO_JSON(ARRAY_AGG(DISTINCT (jsarr)))::JSONB
FROM
  (
    SELECT
      jsonb_array_elements(a) AS jsarr
    UNION ALL
    SELECT
      jsonb_array_elements(b) AS jsarr
  ) t0
$$;

ALTER FUNCTION utils.jsonb_array_a_union_b(a JSONB, b JSONB) OWNER TO :application_user;
