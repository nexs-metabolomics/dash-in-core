CREATE FUNCTION utils.jsonb_array_a_intersect_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  ARRAY_TO_JSON(ARRAY(SELECT
                        jsonb_array_elements(a)
                      INTERSECT
                      SELECT
                        jsonb_array_elements(b)))::JSONB
$$;

ALTER FUNCTION utils.jsonb_array_a_intersect_b(a JSONB, b JSONB) OWNER TO :application_user;

