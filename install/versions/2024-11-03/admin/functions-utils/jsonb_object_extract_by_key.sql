CREATE FUNCTION utils.jsonb_object_extract_by_key
(
  obj  JSONB
, keys JSONB
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_object_agg(n , obj -> n)
FROM
  jsonb_array_elements_text(keys) AS n
$$;

ALTER FUNCTION utils.jsonb_object_extract_by_key(obj JSONB, keys JSONB) OWNER TO :application_user;
