CREATE FUNCTION utils.jsonb_array_a_minus_b
(
  a JSONB
, b JSONB
) RETURNS JSONB
  IMMUTABLE
  LANGUAGE SQL
AS
$$
SELECT
  JSONB_AGG(a)
FROM
  (
    SELECT
      jsonb_array_elements(a) AS a
      , b
  ) t0
WHERE NOT (a <@ b)
   OR b IS NULL
$$;

ALTER FUNCTION utils.jsonb_array_a_minus_b(a JSONB, b JSONB) OWNER TO :application_user;
