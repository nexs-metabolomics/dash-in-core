CREATE FUNCTION utils.jsonb_array_a_intersect_range
(
  a     JSONB
, _low  INTEGER
, _high INTEGER
) RETURNS JSONB
  LANGUAGE SQL
AS
$$
SELECT
  jsonb_agg(value)
FROM
  jsonb_array_elements(a) WITH ORDINALITY
WHERE ordinality BETWEEN _low AND _high
$$;

ALTER FUNCTION utils.jsonb_array_a_intersect_range(a JSONB, _low INTEGER, _high INTEGER) OWNER TO :application_user;

