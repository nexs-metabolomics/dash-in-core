\echo '-------------------------------------------------------------------------------------'
\echo ' POPULATE DASHIN TABLES'
\echo '-------------------------------------------------------------------------------------'

INSERT INTO dashin.consortium (consortium_id, name, description)
VALUES ('5471824b-de9e-43e7-bde7-206a0d9391ba', 'BIOCLAIMS', 'The project "BIOmarkers of Robustness of Metabolic Homeostasis for Nutrigenomics-derived Health CLAIMS Made on Food" is a Collaborative - Large-scale integrating research project funded by the European Commission through it''s Seventh Framework Programme.')
     , ('583e0e25-8979-4780-9b0b-23cfedfdcb8e', 'NutriTech', 'NutriTech is a European Union funded research project (6 M€, 4 years) evaluating the use of cutting-edge analytical technologies and methods to comprehensively evaluate the diet-health relationship and critically assess their usefulness for the future of nutrition research and human well-being.')
     , ('cfbd2057-9c05-4ab3-b75a-4fbd5ed76257', 'NuGO', 'NuGO is an Association of Universities and Research Institutes focusing on the joint development of the research areas of molecular nutrition, personalised nutrition, nutrigenomics and nutritional systems biology')
     , ('3966a44c-02db-4c9b-82b7-f2a9445ea8ff', 'FoodBAll', 'The Food Biomarkers Alliance (FOODBALL) is an initiative aimed at identifying and quantifying dietary biomarkers in order to improve the capabilities of nutritional assessment and research. ');


INSERT INTO dashin.datatype (datatype_id, name, description)
VALUES (1, 'TEXT', 'Data of generic type text, string')
     , (2, 'NUMERIC', 'Data of generic numeric type');


INSERT INTO dashin.datasettype (datasettype_id, name, description)
VALUES (1, 'Regular dataset', 'Regular standard type of dataset');


INSERT INTO dashin.researchfield (researchfield_id, name, description)
VALUES (1, 'UNDEFINED', 'Undefined method category')
     , (2, 'GENERIC', 'Generic')
     , (3, 'METABOLOMICS', 'Metabolomics')
     , (4, 'QUESTIONNAIRE', 'Questionnaire')
     , (5, 'ANTHROPOMETRICS', 'Anthropometrics');


INSERT INTO dashin.assay (assay_id, name, description, researchfield_id)
VALUES ('00000000-0000-0000-0000-000000000000', 'UNDEFINED', 'No assay is defined', 1);

INSERT INTO dashin.search_column_type (search_column_type_id, multiple, researchfield_id, search_column_type_key, name, description)
VALUES (2, 1, 3, 'mz', 'Metabolmics m/z', 'Metabolomics Mass')
     , (3, 1, 3, 'rt', 'Metabolomics Rt', 'Metabolomics Retention time');

INSERT INTO dashin.researchdesign (researchdesign_id, name)
VALUES (1, 'Other')
     , (2, 'Parallel')
     , (3, 'Crossover')
     , (4, 'Cross-sectional')
     , (5, 'Case-control')
     , (6, 'Case referent')
     , (7, 'Prospective Cohort')
     , (8, 'Nested Case control')
     , (9, 'Nested Case-referent');

INSERT INTO dashin.study_role (study_role_id, name)
VALUES (0, 'Unknown')
     , (1, 'Data submission')
     , (2, 'Principal Investigator')
     , (3, 'Scientist involved in study');

INSERT INTO dashin.variabletype (variabletype_id, name, description)
VALUES (0, 'UNDEFINED', 'Variable type is undefined')
     , (1, 'MEASUREMENT', 'Measurement variable. Defines the variables containing measurements')
     , (2, 'SUBJECT_ID', 'Unique id of subject or unit of analysis (within center)')
     , (3, 'STARTGROUP', 'Startgroup is the intervention group to which the subject is assigned during the study.')
     , (4, 'EVENT', 'Event defines a main event (typically a visit) in the study.')
     , (5, 'SUBEVENT', 'Subevent defines a specific action or intervention during an event (eg. exercise).')
     , (6, 'SAMPLING EVENT', 'Sampling event defines an event where a specific sampling type is performed (eg. blood sample).')
     , (7, 'SAMPLING ORDINAL TIME', 'Sampling ordinal time defines ordinal time when the same sampleing event is repeated.')
     , (8, 'CENTER', 'Center to make subjects unique when data is from a multi-center study. Subjects must be unique within center.')
;

INSERT INTO dashin.selection_type (selection_type_id, name, description)
VALUES (1, '3-level high,mid,low', '3-level high,mid,low')
     , (2, 'Route', 'Route')
     , (3, 'Vehicle', 'Vehicle')
     , (4, 'Chemical database', 'Chemical database')
     , (5, 'Dose base', 'Dose base')
     , (6, 'Dose unit', 'Compound dose unit')
     , (7, 'Event subtype', 'Related Event/Challenge')
     , (8, 'Body fluid', 'Body fluid')
     , (9, 'sampling method for body fluid', 'sampling method for body fluid')
     , (10, 'Study excretion sample', 'Study excretion sample')
     , (11, 'Sampling method for excretion sample', 'Sampling method for excretion sample')
     , (12, 'Organ', 'Organ')
     , (13, 'Sampling method for organ', 'Sampling method for organ')
     , (14, 'Time units', 'Time units')
     , (15, 'Gender', 'Gender')
     , (16, 'Age units', 'Age units')
     , (17, 'Body weight units', 'Body weight units')
;


INSERT INTO dashin.selection_choice (selection_type_id, selection_choice_id, ordinal, name)
VALUES
     -- -----------
  (1, 0, 0, 'Not defined')
     , (1, 1, 1, 'Low')
     , (1, 2, 2, 'Mid')
     , (1, 3, 3, 'High')
     -- -----------
     , (2, 0, 0, 'Not relevant/Undefined')
     , (2, 1, 1, 'Intramuscular')
     , (2, 2, 2, 'Oral')
     , (2, 3, 3, 'Gavage')
     , (2, 4, 4, 'Rectal')
     , (2, 5, 5, 'Subcutaneous')
     , (2, 6, 6, 'By inhalation')
     , (2, 7, 7, 'Topically (skin)')
     , (2, 8, 8, 'Other')
     -- -----------
     , (3, 0, 0, 'Not defined')
     , (3, 1, 1, 'Topical product')
     , (3, 2, 2, 'adjuvant')
     , (3, 3, 3, 'Food')
     , (3, 4, 4, 'Drink')
     , (3, 5, 5, 'Other fluid')
     , (3, 6, 6, 'Air/gas')
     , (3, 7, 7, 'Other')
     -- -----------
     , (4, 0, 0, 'Other')
     , (4, 1, 1, 'CHEBI')
     , (4, 2, 2, 'PubChem')
     , (4, 3, 3, 'CAS')
     -- -----------
     , (5, 0, 0, 'Not specified')
     , (5, 1, 1, 'Concentration in food')
     , (5, 2, 2, 'Concentration in drink')
     , (5, 3, 3, 'Concentration in injection fluid')
     , (5, 4, 4, 'Concentration in spray')
     , (5, 5, 5, 'Concentration in topical ointment/fluid')
     , (5, 6, 6, 'Absolute dose to each individual')
     , (5, 7, 7, 'Other')
     -- -----------
     , (6, 0, 0, 'Not defined')
     , (6, 1, 1, 'ppm (mg/kg)')
     , (6, 2, 2, 'ppb (µg/kg)')
     , (6, 3, 3, 'g/kg')
     , (6, 4, 4, 'IU/kg')
     , (6, 5, 5, 'mg/g')
     , (6, 6, 6, 'mg/individual')
     , (6, 7, 7, 'µg/individual')
     , (6, 8, 8, 'IU/individual')
     -- -----------
     , (7, 0, 0, 'Not defined')
     , (7, 1, 1, 'Exercise')
     , (7, 2, 2, 'Meal')
     , (7, 3, 3, 'Drink')
     , (7, 4, 4, 'Diet')
     , (7, 5, 5, 'Fasting')
     , (7, 6, 6, 'Glucose tolerance test')
     , (7, 7, 7, 'Insulin tolerance test')
     , (7, 8, 8, 'Sleep')
     , (7, 9, 9, 'Psychological')
     , (7, 10, 10, 'Vaccination')
     , (7, 11, 11, 'Other biological challenge')
     , (7, 12, 12, 'Chemical/drug challenge')
     , (7, 13, 13, 'Other')
     -- -----------
     , (8, 0, 0, 'Not defined')
     , (8, 1, 1, 'Whole blood')
     , (8, 2, 2, 'Plasma')
     , (8, 3, 3, 'Serum')
     , (8, 4, 4, 'White blood cells')
     , (8, 5, 5, 'Red blood cells')
     , (8, 6, 6, 'Saliva')
     , (8, 7, 7, 'Semen')
     , (8, 8, 8, 'Lymph')
     , (8, 9, 9, 'Cerebrospinal fluid')
     , (8, 10, 10, 'Other')
     -- -----------
     , (9, 0, 0, 'Not defined')
     , (9, 1, 1, 'Direct sampling')
     , (9, 2, 2, 'Needle')
     , (9, 3, 3, 'Swap')
     , (9, 4, 4, 'Scraping')
     , (9, 5, 5, 'Biopsy')
     , (9, 6, 6, 'Fractionation')
     , (9, 7, 7, 'Other')
     -- -----------
     , (10, 0, 0, 'Not defined')
     , (10, 1, 1, 'Urine')
     , (10, 2, 2, 'Feces')
     , (10, 3, 3, 'Sweat')
     , (10, 4, 4, 'Menstruation blood')
     , (10, 5, 5, 'Sebacceous matter')
     , (10, 6, 6, 'Breath')
     , (10, 7, 7, 'Other')
     -- -----------
     , (11, 0, 0, 'Not defined')
     , (11, 1, 1, 'Direct collection')
     , (11, 2, 2, 'Swap (absorbent)')
     , (11, 3, 3, 'Other')
     -- -----------
     , (12, 0, 0, 'Not defined')
     , (12, 1, 1, 'Colon')
     , (12, 2, 2, 'Adipose tissue')
     , (12, 3, 3, 'Muscle')
     , (12, 4, 4, 'Skin')
     , (12, 5, 5, 'Bones')
     , (12, 6, 6, 'Other')
     , (13, 0, 0, 'Not defined')
     , (13, 1, 1, 'Needle biopsy')
     , (13, 2, 2, 'Incision')
     , (13, 3, 3, 'Other')
     -- -----------
     , (14, 0, 0, 'Select time unit')
     , (14, 1, 1, 'Seconds')
     , (14, 2, 2, 'Minutes')
     , (14, 3, 3, 'Hours')
     , (14, 4, 4, 'Days')
     , (14, 5, 5, 'Weeks')
     , (14, 6, 6, 'Years')
     -- -----------
     , (15, 0, 0, 'Unknown')
     , (15, 1, 1, 'Female')
     , (15, 2, 2, 'Male')
     -- -----------
     , (16, 0, 0, 'Select age unit')
     , (16, 1, 1, 'Days')
     , (16, 2, 2, 'Weeks')
     , (16, 3, 3, 'Months')
     , (16, 4, 4, 'Years')
     -- -----------
     , (17, 0, 0, 'Not defined')
     , (17, 1, 1, 'g')
     , (17, 2, 2, 'kg')
;

INSERT INTO dashin.intervention_type (intervention_type_id, name, description)
VALUES (0, 'None', 'None')
     , (1, 'Intervention', 'Intervention')
     , (2, 'Challenge', 'Intervention')
;

INSERT INTO dashin.studydesign_subevent_type (subevent_type_id, ordinal, name, description, additional_data_schema)
VALUES (0, 0, 'Not defined', 'Not defined', NULL)
     , (1, 1, 'Human base', 'Human subject base event', '{"fields": {"country": {"label": "Country","type": "plaintext","content":"text","ordinal": 1},"ethnicity": {"label": "Ethnicity","type": "plaintext","content":"text","ordinal": 2},"gender": {"label": "Gender","type": "plaintext","content":"Femail (1), Male (2)","ordinal": 3},"age_unit": {"label": "Age unit","type": "select","sourceid": 16,"ordinal": 4},"age": {"label": "Age, (time since birth)","type": "plaintext","ordinal": 5}}}')
     , (2, 2, 'Anthropometrics', 'Anthropometric event', '{"fields": {"body_weight_unit": {"label": "Body Weight unit","type": "select","sourceid": 17,"ordinal": 1},"body_weight": {"label": "Body Weight","type": "plaintext","content": "Decimal nr","ordinal": 2},"body_height": {"label": "Body height","type": "plaintext","content": "Body height in meters","ordinal": 3},"bmi": {"label": "BMI","type": "plaintext","content": "BMI (kg/m2)","ordinal": 4},"hip": {"label": "Hip circumference","type": "plaintext","content": "Hip circumference (cm)","ordinal": 5},"waist": {"label": "Waist circumference (cm)","type": "plaintext","ordinal": 5},"waist_hip_ratio": {"label": "Waist/hip ratio","type": "plaintext","ordinal": 6},"supplement_use": {"label": "Medication/Supplement/Diet Use","type": "plaintext","ordinal": 7},"adverse_events": {"label": "Adverse events","type": "plaintext","ordinal": 8},"subject_description": {"label": "Subject description","type": "plaintext","ordinal": 8}}}')
     , (3, 3, 'Diet', 'Diet event', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric","ordinal": 2},"end_time": {"label": "End time","type": "plaintext","content": "numeric","ordinal": 3},"component": {"label": "Diet or food/component","type": "plaintext","content": "text","ordinal": 4},"component_level": {"label": "Food or food component level","type": "select","sourceid": 1,"ordinal": 5},"fat_level": {"label": "Dietary fat-level","type": "select","sourceid": 1,"ordinal": 6},"cho_level": {"label": "Dietary carbohydrate level","type": "select","sourceid": 1,"ordinal": 7},"protein_level": {"label": "Dietary protein/N-level","type": "select","sourceid": 1,"ordinal": 8},"fiber_level": {"label": "Dietary fiber level","type": "select","sourceid": 1,"ordinal": 9}}}')
     , (4, 4, 'Feed or fasting', 'Feed or fasting event', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric","ordinal": 2},"end_time": {"label": "End time","type": "plaintext","content": "numeric","ordinal": 3}}}')
     , (5, 5, 'Compound', 'Compound event', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric","ordinal": 2},"end_time": {"label": "End time","type": "plaintext","content": "numeric","ordinal": 3},"compound_name": {"label": "Compound name","type": "plaintext","content": "text","ordinal": 4},"compound_identifier": {"label": "Compound identifier","type": "plaintext","content": "text","ordinal": 5},"database": {"label": "Database name","type": "select","sourceid": 4,"ordinal": 6},"route": {"label": "Route","type": "select","sourceid": 2,"ordinal": 7},"vehicle": {"label": "Vehicle","type": "select","sourceid": 3,"ordinal": 8},"dose_base": {"label": "Dose based on","type": "select","sourceid": 4,"ordinal": 9},"dose_unit": {"label": "Compound dose unit","type": "select","sourceid": 6,"ordinal": 10},"compound_freq": {"label": "Compound frequency","type": "plaintext","content": "text","ordinal": 11}}}')
     , (6, 6, 'Exercise', 'Exercise event', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric","ordinal": 2},"end_time": {"label": "End time","type": "plaintext","content": "numeric","ordinal": 3}}}')
;

INSERT INTO dashin.studydesign_sampling_type (sampling_type_id, ordinal, name, description, additional_data_schema)
VALUES (0, 0, 'Basic', 'Basic measures', NULL)
     , (1, 1, 'Physiology', 'Physiology sampling', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric (relative time)","ordinal": 2},"duration": {"label": "Duration","type": "plaintext","content": "numeric (relative time)","ordinal": 3},"relative_time": {"label": "Relative time","type": "plaintext","content": "numeric (relative time)","ordinal": 4}}}')
     , (2, 2, 'Body fluid', 'Body fluid sampling', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric (relative time)","ordinal": 2},"duration": {"label": "Duration","type": "plaintext","content": "numeric (relative time)","ordinal": 3},"relative_time": {"label": "Relative time","type": "plaintext","content": "numeric (relative time)","ordinal": 4},"sampling_method": {"type": "select","label": "Sampling method","ordinal": 5,"sourceid": 9},"fluid_desc": {"type": "plaintext","content": "Body fluid description","ordinal": 6},"fluid_type": {"type": "select","label": "Body fluid","ordinal": 7,"sourceid": 8}}}')
     , (3, 3, 'Excretion', 'Excretion sampling', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric (relative time)","ordinal": 2},"duration": {"label": "Duration","type": "plaintext","content": "numeric (relative time)","ordinal": 3},"relative_time": {"label": "Relative time","type": "plaintext","content": "numeric (relative time)","ordinal": 4},"sampling_method": {"type": "select","label": "Sampling method","ordinal": 5,"sourceid": 11},"escretion_type": {"type": "select","label": "Excretion sample","ordinal": 6,"sourceid": 10}}}')
     , (4, 4, 'Organ', 'Organ sampling', '{"fields": {"time_unit": {"label": "Time unit","type": "select","sourceid": 14,"ordinal": 1},"start_time": {"label": "Start time","type": "plaintext","content": "numeric (relative time)","ordinal": 2},"duration": {"label": "Duration","type": "plaintext","content": "numeric (relative time)","ordinal": 3},"relative_time": {"label": "Relative time","type": "plaintext","content": "numeric (relative time)","ordinal": 4},"sampling_method": {"type": "select","label": "Sampling method","ordinal": 5,"sourceid": 13},"organ": {"type": "select","label": "Organ sample","ordinal": 6,"sourceid": 12}}}')
;

INSERT INTO dashin.conditionoperator (conditionoperator_id, context_flag, symbol, shortname, name, label, description)
-- @formatter:off
VALUES
    ( 0, 0, ''              , 'nop'   ,'noop'                   , 'No operation'            , 'Dummy/noop operator. Operator that does nothing')
  , ( 1, 2, 'x > a'         , 'gt'    ,'greater_than'           , 'Greater than'            , 'Left value greater than right value')
  , ( 2, 2, 'x >= a'        , 'ge'    ,'greater_than_or_even'   , 'Greater than or even'    , 'Left value is greater than or even to right value')
  , ( 3, 2, 'x = a'         , 'eq'    ,'equals'                 , 'Equals'                  , 'Left value equals right value')
  , ( 4, 2, 'x <= a'        , 'le'    ,'less_than_or_even'      , 'Less than or even'       , 'Left value is less than or even to right value')
  , ( 5, 2, 'x < a'         , 'lt'    ,'less_than'              , 'Less than'               , 'Left value is less than right value')
  , ( 6, 2, 'x != a'        , 'neq'   ,'not_equal'              , 'Not equal'               , 'Left value is not equal to right value')
  , ( 7, 2, 'a < x < b'     , 'bte'   ,'between_exclusive'      , 'Between (exclusive)'     , 'Left value is greater than right value-1 and less than right value-2')
  , ( 8, 2, 'a < x <= b'    , 'btu'   ,'between_include_upper'  , 'Between (include upper)' , 'Left value is greater than or even to right value-1 and less than right value-2')
  , ( 9, 2, 'a <= x < b'    , 'btl'   ,'between_include_lower'  , 'Between (include lower)' , 'Left value is greater than right value-1 and less than or even to right value-2')
  , (10, 2, 'a <= x <= b'   , 'bti'   ,'between_inclusive'      , 'Between inclusive'       , 'Left value is greater than right or even to value-1 and less than or even to right value-2')
  , (11, 3, 'IN a'          , 'cnt'   ,'contained_in'           , 'Contained in'            , 'Left value is contained in right value-list')
  , (12, 3, 'NOT IN a'      , 'ncnt'  ,'not_contained_in'       , 'Not contained in'        , 'Left value is not contained in right value-list')
;
-- @formatter:on

\echo '-------------------------------------------------------------------------------------'
\echo ' POPULATE DASHIN TABLES DONE'
\echo '-------------------------------------------------------------------------------------'
