CREATE FUNCTION dashin.studyexport_copy_to_schema
(
    _study_id    UUID
  , _app_schema  TEXT
  , _schema_name TEXT DEFAULT NULL::TEXT
  , _update_ids  BOOLEAN DEFAULT TRUE
)
  RETURNS TABLE
          (
            STATUS      BOOLEAN,
            STATUS_TEXT TEXT
          )
  LANGUAGE plpgsql
AS
$fun$
DECLARE
  _main_query       TEXT;
  _update_query     TEXT;
  _permission_query TEXT;
  _is_public        BOOL;
BEGIN
  
  _permission_query := format(
    $qry$
       SELECT coalesce(jsonb_extract_path_text(details, 'permissions', 'public') = 'yes', FALSE)
       FROM %2$I.study
       WHERE study_id = %1$L;
    $qry$
    , _study_id
    , _app_schema);
  
  EXECUTE _permission_query INTO _is_public;
  IF NOT _is_public THEN
    RETURN QUERY SELECT FALSE, 'study is not public';
    RETURN;
  END IF;
  
  IF _schema_name IS NULL THEN
    _schema_name := 'studyexport_' || to_char(clock_timestamp(), 'YYYYMMDDHH24MI_') || replace(uuid_generate_v4()::TEXT, '-', '');
  END IF;
  EXECUTE format($$CREATE SCHEMA %1$I;$$, _schema_name);
  
  _main_query := format(
    $qry$
      
      -- ================================================================================================

      CREATE TABLE %1$I.study
      (
        study_id             UUID CONSTRAINT study_pk PRIMARY KEY,
        name                 TEXT,
        title                TEXT,
        description          TEXT,
        start_date           TIMESTAMP,
        endpoint             TEXT,
        objectives           TEXT,
        conclusion           TEXT,
        exclusion            TEXT,
        inclusion            TEXT,
        institute            TEXT,
        country_id           INTEGER,
        consortium_id        UUID,
        published            TEXT,
        researchdesign_id    INTEGER,
        num_treat            TEXT,
        num_factor           INTEGER,
        num_arm              INTEGER,
        researchdesign_text  TEXT,
        num_volun            TEXT,
        num_volun_terminated TEXT,
        recruit_start_year   INTEGER,
        recruit_end_year     INTEGER,
        blinding             INTEGER,
        blinding_method      TEXT,
        created_at           TIMESTAMP WITH TIME ZONE,
        updated_at           TIMESTAMP WITH TIME ZONE,
        details              JSONB
      );
      
      INSERT INTO %1$I.study (study_id, name, title, description, start_date, endpoint, objectives, conclusion, exclusion
                                    , inclusion, institute, country_id, consortium_id, published, researchdesign_id, num_treat
                                    , num_factor, num_arm, researchdesign_text, num_volun, num_volun_terminated, recruit_start_year
                                    , recruit_end_year, blinding, blinding_method, created_at, updated_at, details)

      SELECT study_id
           , name
           , title
           , description
           , start_date
           , endpoint
           , objectives
           , conclusion
           , exclusion
           , inclusion
           , institute
           , country_id
           , consortium_id
           , published
           , researchdesign_id
           , num_treat
           , num_factor
           , num_arm
           , researchdesign_text
           , num_volun
           , num_volun_terminated
           , recruit_start_year
           , recruit_end_year
           , blinding
           , blinding_method
           , created_at
           , updated_at
           , details
      FROM %3$I.study
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.dataset
      (
        dataset_id     UUID CONSTRAINT dataset_pk PRIMARY KEY,
        name           TEXT,
        description    TEXT,
        status         SMALLINT,
        datasettype_id INTEGER,
        created_at     TIMESTAMP WITH TIME ZONE,
        updated_at     TIMESTAMP WITH TIME ZONE,
        details        JSONB
      );
      
      INSERT INTO %1$I.dataset  (dataset_id, name, description, status, datasettype_id, created_at, updated_at, details)
      SELECT dataset_id
           , name
           , description
           , status
           , datasettype_id
           , created_at
           , updated_at
           , details
      FROM %3$I.dataset
      WHERE dataset_id IN (
                            SELECT dataset_id
                            FROM %3$I.dataset_x_study
                            WHERE study_id = (%2$L)::UUID
                          )
        AND jsonb_extract_path_text(details, 'permissions', 'public') = 'yes';

      -- ================================================================================================

      CREATE TABLE %1$I.study_event
      (
        event_id UUID CONSTRAINT study_event_pk PRIMARY KEY
      );
  

      INSERT INTO %1$I.study_event(event_id)
      SELECT event_id
      FROM %3$I.study_event
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.study_subevent
      (
        subevent_id UUID CONSTRAINT study_subevent_pk PRIMARY KEY
      );

      INSERT INTO %1$I.study_subevent(subevent_id)
      SELECT subevent_id
      FROM %3$I.study_subevent
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.study_samplingevent
      (
        samplingevent_id UUID CONSTRAINT study_samplingevent_pk PRIMARY KEY
      );

      INSERT INTO %1$I.study_samplingevent(samplingevent_id)
      SELECT samplingevent_id
      FROM %3$I.study_samplingevent
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.study_samplingtime
      (
        samplingtime_id UUID CONSTRAINT study_samplingtime_pk PRIMARY KEY
      );

      INSERT INTO %1$I.study_samplingtime(samplingtime_id)
      SELECT samplingtime_id
      FROM %3$I.study_samplingtime a
      INNER JOIN %3$I.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
      WHERE b.study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.study_center
      (
        center_id UUID CONSTRAINT study_center_pk PRIMARY KEY
      );

      INSERT INTO %1$I.study_center(center_id)
      SELECT center_id
      FROM %3$I.study_center
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.study_startgroup
      (
        startgroup_id UUID CONSTRAINT study_startgroup_pk PRIMARY KEY
      );
  
      INSERT INTO %1$I.study_startgroup(startgroup_id)
      SELECT startgroup_id
      FROM %3$I.study_startgroup
      WHERE study_id = (%2$L)::UUID;

      -- ----------------------------------------------------------------------------------

      CREATE TABLE %1$I.study_subject
      (
        subject_id UUID CONSTRAINT study_subject_pk PRIMARY KEY
      );
      
      INSERT INTO %1$I.study_subject(subject_id)
      SELECT subject_id
      FROM %3$I.study_subject
      WHERE study_id = (%2$L)::UUID;

      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_x_study_event
      (
        dataset_id          UUID,
        datadesign_event_id UUID
          CONSTRAINT datadesign_x_study_event_pk PRIMARY KEY,
        study_id            UUID,
        study_event_id      UUID,
        CONSTRAINT dxs_event_uq UNIQUE (dataset_id, study_event_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_event
        ADD CONSTRAINT datadesign_x_study_event_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_event
        ADD CONSTRAINT datadesign_x_study_event_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_event
        ADD CONSTRAINT datadesign_x_study_event_studyevent_fk
          FOREIGN KEY (study_event_id) REFERENCES %1$I.study_event (event_id)
            ON UPDATE CASCADE;

      INSERT INTO %1$I.datadesign_x_study_event(dataset_id, datadesign_event_id, study_id, study_event_id)
      SELECT a.dataset_id
           , a.datadesign_event_id
           , a.study_id
           , a.study_event_id
      FROM %3$I.datadesign_x_study_event a
      WHERE a.study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );
      
      -- ================================================================================================
      CREATE TABLE %1$I.datadesign_x_study_subevent
      (
        dataset_id             UUID,
        datadesign_subevent_id UUID
          CONSTRAINT dxs_datadesign_subevent_id_pk PRIMARY KEY,
        study_id               UUID,
        study_subevent_id      UUID,
        CONSTRAINT dxs_subevent_uq UNIQUE (dataset_id, study_subevent_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_subevent
        ADD CONSTRAINT datadesign_x_study_subevent_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_subevent
        ADD CONSTRAINT datadesign_x_study_subevent_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_subevent
        ADD CONSTRAINT datadesign_x_study_subevent_studysubevent_fk
          FOREIGN KEY (study_subevent_id) REFERENCES %1$I.study_subevent (subevent_id)
            ON UPDATE CASCADE;

      INSERT INTO %1$I.datadesign_x_study_subevent(dataset_id, datadesign_subevent_id, study_id, study_subevent_id)
      SELECT a.dataset_id
           , a.datadesign_subevent_id
           , a.study_id
           , a.study_subevent_id
      FROM %3$I.datadesign_x_study_subevent a
      WHERE a.study_id = (%2$L)::UUID
      AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );

      
      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_x_study_samplingevent
      (
        dataset_id                  UUID,
        datadesign_samplingevent_id UUID
          CONSTRAINT dxs_datadesign_samplingevent_id_pk PRIMARY KEY,
        study_id                    UUID,
        study_samplingevent_id      UUID,
        CONSTRAINT dxs_samplingevent_uq UNIQUE (dataset_id, study_samplingevent_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_samplingevent
        ADD CONSTRAINT datadesign_x_study_samplingevent_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_samplingevent
        ADD CONSTRAINT datadesign_x_study_samplingevent_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_samplingevent
        ADD CONSTRAINT datadesign_x_study_samplingevent_studysamplingevent_fk
          FOREIGN KEY (study_samplingevent_id) REFERENCES %1$I.study_samplingevent (samplingevent_id)
            ON UPDATE CASCADE;
      
      INSERT INTO %1$I.datadesign_x_study_samplingevent(dataset_id, datadesign_samplingevent_id, study_id, study_samplingevent_id)
      SELECT a.dataset_id
           , a.datadesign_samplingevent_id
           , a.study_id
           , a.study_samplingevent_id
      FROM %3$I.datadesign_x_study_samplingevent a
      WHERE study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );
      
      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_x_study_samplingtime
      (
        dataset_id                 UUID,
        datadesign_samplingtime_id UUID
          CONSTRAINT dxs_datadesign_samplingtime_id_pk PRIMARY KEY,
        study_id                   UUID,
        study_samplingtime_id      UUID,
        CONSTRAINT dxs_samplingtime_uq UNIQUE (dataset_id, study_samplingtime_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_samplingtime
        ADD CONSTRAINT datadesign_x_study_samplingtime_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_samplingtime
        ADD CONSTRAINT datadesign_x_study_samplingtime_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_samplingtime
        ADD CONSTRAINT datadesign_x_study_samplingtime_studysamplingtime_fk
          FOREIGN KEY (study_samplingtime_id) REFERENCES %1$I.study_samplingtime (samplingtime_id)
            ON UPDATE CASCADE;

      INSERT INTO %1$I.datadesign_x_study_samplingtime(dataset_id, datadesign_samplingtime_id, study_id, study_samplingtime_id)
      SELECT a.dataset_id
           , a.datadesign_samplingtime_id
           , a.study_id
           , a.study_samplingtime_id
      FROM %3$I.datadesign_x_study_samplingtime a
      WHERE a.study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );
      
      -- ================================================================================================
      
      CREATE TABLE %1$I.datadesign_x_study_center
      (
        dataset_id           UUID,
        datadesign_center_id UUID
          CONSTRAINT dxs_datadesign_center_id_pk PRIMARY KEY,
        study_id             UUID,
        study_center_id      UUID,
        CONSTRAINT dxs_center_uq UNIQUE (dataset_id, study_center_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_center
        ADD CONSTRAINT datadesign_x_study_center_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_center
        ADD CONSTRAINT datadesign_x_study_center_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_center
        ADD CONSTRAINT datadesign_x_study_center_studycenter_fk
          FOREIGN KEY (study_center_id) REFERENCES %1$I.study_center (center_id)
            ON UPDATE CASCADE;
      
      INSERT INTO %1$I.datadesign_x_study_center(dataset_id, datadesign_center_id, study_id, study_center_id)
      SELECT a.dataset_id
           , a.datadesign_center_id
           , a.study_id
           , a.study_center_id
      FROM %3$I.datadesign_x_study_center a
      WHERE a.study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );
      
      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_x_study_startgroup
      (
        dataset_id               UUID,
        datadesign_startgroup_id UUID
          CONSTRAINT dxs_datadesign_startgroup_id_pk PRIMARY KEY,
        study_id                 UUID,
        study_startgroup_id      UUID,
        CONSTRAINT dxs_startgroup_uq UNIQUE (dataset_id, study_startgroup_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_startgroup
        ADD CONSTRAINT datadesign_x_study_startgroup_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_startgroup
        ADD CONSTRAINT datadesign_x_study_startgroup_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_startgroup
        ADD CONSTRAINT datadesign_x_study_startgroup_studystartgroup_fk
          FOREIGN KEY (study_startgroup_id) REFERENCES %1$I.study_startgroup (startgroup_id)
            ON UPDATE CASCADE;
      
      INSERT INTO %1$I.datadesign_x_study_startgroup(dataset_id, datadesign_startgroup_id, study_id, study_startgroup_id)
      SELECT a.dataset_id
           , a.datadesign_startgroup_id
           , a.study_id
           , a.study_startgroup_id
      FROM %3$I.datadesign_x_study_startgroup a
      WHERE a.study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );
      
      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_x_study_subject
      (
        dataset_id            UUID,
        datadesign_subject_id UUID
          CONSTRAINT dxs_datadesign_subject_id_pk PRIMARY KEY,
        study_id              UUID,
        study_subject_id      UUID,
        CONSTRAINT dxs_subject_uq UNIQUE (dataset_id, study_subject_id)
      );

      ALTER TABLE %1$I.datadesign_x_study_subject
        ADD CONSTRAINT datadesign_x_study_subject_dataset_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_subject
        ADD CONSTRAINT datadesign_x_study_subject_study_fk
          FOREIGN KEY (study_id) REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_x_study_subject
        ADD CONSTRAINT datadesign_x_study_subject_studysubject_fk
          FOREIGN KEY (study_subject_id) REFERENCES %1$I.study_subject (subject_id)
            ON UPDATE CASCADE;
      
      INSERT INTO %1$I.datadesign_x_study_subject(dataset_id, datadesign_subject_id, study_id, study_subject_id)
      SELECT a.dataset_id
           , a.datadesign_subject_id
           , a.study_id
           , a.study_subject_id
      FROM %3$I.datadesign_x_study_subject a
      WHERE a.study_id = (%2$L)::UUID
        AND a.dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );

      -- ================================================================================================
      CREATE TABLE %1$I.studydesign_datatable
      (
        studyrow_id                        UUID CONSTRAINT studydesign_datatable_pk PRIMARY KEY,
        study_id                           UUID,
        studyevent_id                      UUID,
        studysubevent_id                   UUID,
        studysamplingevent_id              UUID,
        studysamplingtime_id               UUID,
        studycenter_id                     UUID,
        studystartgroup_id                 UUID,
        studysubject_id                    UUID,
        studyevent_ordinal                 INTEGER,
        studyevent_created_at              TIMESTAMP WITH TIME ZONE,
        studyevent_updated_at              TIMESTAMP WITH TIME ZONE,
        studyevent_name                    TEXT,
        studyevent_label                   TEXT,
        studyevent_description             TEXT,
        studysubevent_subeventtype_id      INTEGER,
        studysubevent_ordinal              INTEGER,
        studysubevent_intervention_type_id INTEGER,
        studysubevent_created_at           TIMESTAMP WITH TIME ZONE,
        studysubevent_updated_at           TIMESTAMP WITH TIME ZONE,
        studysubevent_name                 TEXT,
        studysubevent_label                TEXT,
        studysubevent_description          TEXT,
        studysubevent_row_comment          TEXT,
        studysubevent_additional_data      JSONB,
        studysamplingevent_samplingtype_id INTEGER,
        studysamplingevent_ordinal         INTEGER,
        studysamplingevent_created_at      TIMESTAMP WITH TIME ZONE,
        studysamplingevent_updated_at      TIMESTAMP WITH TIME ZONE,
        studysamplingevent_name            TEXT,
        studysamplingevent_label           TEXT,
        studysamplingevent_description     TEXT,
        studysamplingevent_row_comment     TEXT,
        studysamplingevent_additional_data JSONB,
        studysamplingtime_ordinal_time     INTEGER,
        studysamplingtime_created_at       TIMESTAMP WITH TIME ZONE,
        studysamplingtime_updated_at       TIMESTAMP WITH TIME ZONE,
        studysamplingtime_name             TEXT,
        studysamplingtime_label            TEXT,
        studysamplingtime_description      TEXT,
        studycenter_ordinal                INTEGER,
        studycenter_created_at             TIMESTAMP WITH TIME ZONE,
        studycenter_updated_at             TIMESTAMP WITH TIME ZONE,
        studycenter_name                   TEXT,
        studycenter_label                  TEXT,
        studycenter_description            TEXT,
        studystartgroup_ordinal            INTEGER,
        studystartgroup_created_at         TIMESTAMP WITH TIME ZONE,
        studystartgroup_updated_at         TIMESTAMP WITH TIME ZONE,
        studystartgroup_name               TEXT,
        studystartgroup_label              TEXT,
        studystartgroup_description        TEXT,
        studysubject_name                  TEXT,
        studysubject_label                 TEXT,
        studysubject_description           TEXT,
        studysubject_status                INTEGER,
        studysubject_created_at            TIMESTAMP WITH TIME ZONE,
        studysubject_updated_at            TIMESTAMP WITH TIME ZONE,
        studysubject_details               JSONB
      );
      
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_study_fk
          FOREIGN KEY (study_id)
            REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studyevent_fk
          FOREIGN KEY (studyevent_id)
            REFERENCES %1$I.study_event (event_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studysubevent_fk
          FOREIGN KEY (studysubevent_id)
            REFERENCES %1$I.study_subevent (subevent_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studysamplingevent_fk
          FOREIGN KEY (studysamplingevent_id)
            REFERENCES %1$I.study_samplingevent (samplingevent_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studysamplingtime_fk
          FOREIGN KEY (studysamplingtime_id)
            REFERENCES %1$I.study_samplingtime (samplingtime_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studycenter_fk
          FOREIGN KEY (studycenter_id)
            REFERENCES %1$I.study_center (center_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studystartgroup_fk
          FOREIGN KEY (studystartgroup_id)
            REFERENCES %1$I.study_startgroup (startgroup_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.studydesign_datatable
        ADD CONSTRAINT studydesign_datatable_studysubject_fk
          FOREIGN KEY (studysubject_id)
            REFERENCES %1$I.study_subject (subject_id)
            ON UPDATE CASCADE;
      

      INSERT INTO %1$I.studydesign_datatable ( studyrow_id, study_id, studyevent_id, studysubevent_id, studysamplingevent_id, studysamplingtime_id
                                             , studycenter_id, studystartgroup_id, studysubject_id, studyevent_ordinal, studyevent_created_at
                                             , studyevent_updated_at, studyevent_name, studyevent_label, studyevent_description, studysubevent_subeventtype_id
                                             , studysubevent_ordinal, studysubevent_intervention_type_id, studysubevent_created_at, studysubevent_updated_at
                                             , studysubevent_name, studysubevent_label, studysubevent_description, studysubevent_row_comment
                                             , studysubevent_additional_data, studysamplingevent_samplingtype_id, studysamplingevent_ordinal, studysamplingevent_created_at
                                             , studysamplingevent_updated_at, studysamplingevent_name, studysamplingevent_label, studysamplingevent_description
                                             , studysamplingevent_row_comment, studysamplingevent_additional_data, studysamplingtime_ordinal_time
                                             , studysamplingtime_created_at, studysamplingtime_updated_at, studysamplingtime_name, studysamplingtime_label
                                             , studysamplingtime_description, studycenter_ordinal, studycenter_created_at, studycenter_updated_at, studycenter_name
                                             , studycenter_label, studycenter_description, studystartgroup_ordinal, studystartgroup_created_at, studystartgroup_updated_at
                                             , studystartgroup_name, studystartgroup_label, studystartgroup_description, studysubject_name, studysubject_label
                                             , studysubject_description, studysubject_status, studysubject_created_at, studysubject_updated_at, studysubject_details)
      WITH
        studyinfo_query     AS (
                                 SELECT a.study_id
                                 FROM %3$I.study a
                                 WHERE a.study_id = (%2$L)::UUID
                               )
        , studydesign_query AS (
                                 SELECT a.studyrow_id
                                      , a.study_id
                                      
                                      , a.event_id             AS studyevent_id
                                      , a.subevent_id          AS studysubevent_id
                                      , a.samplingevent_id     AS studysamplingevent_id
                                      , a.samplingtime_id      AS studysamplingtime_id
                                      , a.center_id            AS studycenter_id
                                      , a.startgroup_id        AS studystartgroup_id
                                      , a.subject_id           AS studysubject_id
                                      
                                      , c.ordinal              AS studyevent_ordinal
                                      , c.created_at           AS studyevent_created_at
                                      , c.updated_at           AS studyevent_updated_at
                                      , c.name                 AS studyevent_name
                                      , c.label                AS studyevent_label
                                      , c.description          AS studyevent_description
                                      
                                      , d.subevent_type_id     AS studysubevent_subeventtype_id
                                      , d.ordinal              AS studysubevent_ordinal
                                      , d.intervention_type_id AS studysubevent_intervention_type_id
                                      , d.created_at           AS studysubevent_created_at
                                      , d.updated_at           AS studysubevent_updated_at
                                      , d.name                 AS studysubevent_name
                                      , d.label                AS studysubevent_label
                                      , d.description          AS studysubevent_description
                                      , d.row_comment          AS studysubevent_row_comment
                                      , d.additional_data      AS studysubevent_additional_data
                                      
                                      , e.sampling_type_id     AS studysamplingevent_samplingtype_id
                                      , e.ordinal              AS studysamplingevent_ordinal
                                      , e.created_at           AS studysamplingevent_created_at
                                      , e.updated_at           AS studysamplingevent_updated_at
                                      , e.name                 AS studysamplingevent_name
                                      , e.label                AS studysamplingevent_label
                                      , e.description          AS studysamplingevent_description
                                      , e.row_comment          AS studysamplingevent_row_comment
                                      , e.additional_data      AS studysamplingevent_additional_data
                                      
                                      , f.ordinal_time         AS studysamplingtime_ordinal_time
                                      , f.created_at           AS studysamplingtime_created_at
                                      , f.updated_at           AS studysamplingtime_updated_at
                                      , f.name                 AS studysamplingtime_name
                                      , f.label                AS studysamplingtime_label
                                      , f.description          AS studysamplingtime_description
                                      
                                      , g.ordinal              AS studycenter_ordinal
                                      , g.created_at           AS studycenter_created_at
                                      , g.updated_at           AS studycenter_updated_at
                                      , g.name                 AS studycenter_name
                                      , g.label                AS studycenter_label
                                      , g.description          AS studycenter_description
                                      
                                      , h.ordinal              AS studystartgroup_ordinal
                                      , h.created_at           AS studystartgroup_created_at
                                      , h.updated_at           AS studystartgroup_updated_at
                                      , h.name                 AS studystartgroup_name
                                      , h.label                AS studystartgroup_label
                                      , h.description          AS studystartgroup_description
                                      
                                      , j.name                 AS studysubject_name
                                      , j.label                AS studysubject_label
                                      , j.description          AS studysubject_description
                                      , j.status               AS studysubject_status
                                      , j.created_at           AS studysubject_created_at
                                      , j.updated_at           AS studysubject_updated_at
                                      , j.details              AS studysubject_details
                                 
                                 FROM %3$I.studydatatable              a
                                   INNER JOIN studyinfo_query            b ON a.study_id = b.study_id
                                   INNER JOIN %3$I.study_event         c ON a.event_id = c.event_id
                                   INNER JOIN %3$I.study_subevent      d ON a.subevent_id = d.subevent_id
                                   INNER JOIN %3$I.study_samplingevent e ON a.samplingevent_id = e.samplingevent_id
                                   INNER JOIN %3$I.study_samplingtime  f ON a.samplingtime_id = f.samplingtime_id
                                   INNER JOIN %3$I.study_center        g ON a.center_id = g.center_id
                                   INNER JOIN %3$I.study_startgroup    h ON a.startgroup_id = h.startgroup_id
                                   INNER JOIN %3$I.study_subject       j ON a.subject_id = j.subject_id
                               )
      SELECT studyrow_id
           , study_id
           , studyevent_id
           , studysubevent_id
           , studysamplingevent_id
           , studysamplingtime_id
           , studycenter_id
           , studystartgroup_id
           , studysubject_id
           , studyevent_ordinal
           , studyevent_created_at
           , studyevent_updated_at
           , studyevent_name
           , studyevent_label
           , studyevent_description
           , studysubevent_subeventtype_id
           , studysubevent_ordinal
           , studysubevent_intervention_type_id
           , studysubevent_created_at
           , studysubevent_updated_at
           , studysubevent_name
           , studysubevent_label
           , studysubevent_description
           , studysubevent_row_comment
           , studysubevent_additional_data
           , studysamplingevent_samplingtype_id
           , studysamplingevent_ordinal
           , studysamplingevent_created_at
           , studysamplingevent_updated_at
           , studysamplingevent_name
           , studysamplingevent_label
           , studysamplingevent_description
           , studysamplingevent_row_comment
           , studysamplingevent_additional_data
           , studysamplingtime_ordinal_time
           , studysamplingtime_created_at
           , studysamplingtime_updated_at
           , studysamplingtime_name
           , studysamplingtime_label
           , studysamplingtime_description
           , studycenter_ordinal
           , studycenter_created_at
           , studycenter_updated_at
           , studycenter_name
           , studycenter_label
           , studycenter_description
           , studystartgroup_ordinal
           , studystartgroup_created_at
           , studystartgroup_updated_at
           , studystartgroup_name
           , studystartgroup_label
           , studystartgroup_description
           , studysubject_name
           , studysubject_label
           , studysubject_description
           , studysubject_status
           , studysubject_created_at
           , studysubject_updated_at
           , studysubject_details
      FROM studydesign_query;

      -- ================================================================================================

      CREATE TABLE %1$I.datadesign_datatable
      (
        datarow_id                  UUID CONSTRAINT datadesign_datatable_pk PRIMARY KEY,
        datarow_ordinal             INTEGER,
        studyrow_id                 UUID,
        dataset_id                  UUID,
        study_id                    UUID,
        datasetevent_id             UUID,
        datasetsubevent_id          UUID,
        datasetsamplingevent_id     UUID,
        datasetsamplingtime_id      UUID,
        datasetcenter_id            UUID,
        datasetstartgroup_id        UUID,
        datasetsubject_id           UUID,
        datasetevent_name           TEXT,
        datasetsubevent_name        TEXT,
        datasetsamplingevent_name   TEXT,
        datasetsamplingtime_name    TEXT,
        datasetcenter_name          TEXT,
        datasetstartgroup_name      TEXT,
        datasetsubject_name         TEXT,
        datasetevent_status         INTEGER,
        datasetsubevent_status      INTEGER,
        datasetsamplingevent_status INTEGER,
        datasetsamplingtime_status  INTEGER,
        datasetcenter_status        INTEGER,
        datasetstartgroup_status    INTEGER,
        datasetsubject_status       INTEGER,
        studyevent_id               UUID,
        studysubevent_id            UUID,
        studysamplingevent_id       UUID,
        studysamplingtime_id        UUID,
        studycenter_id              UUID,
        studystartgroup_id          UUID,
        studysubject_id             UUID,
        datarow                     JSONB
      );
      
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_study_fk
          FOREIGN KEY (study_id)
            REFERENCES %1$I.study (study_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_dataset_fk
          FOREIGN KEY (dataset_id)
            REFERENCES %1$I.dataset (dataset_id)
            ON UPDATE CASCADE;
      
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studyrow_fk
          FOREIGN KEY (studyrow_id)
            REFERENCES %1$I.studydesign_datatable (studyrow_id)
            ON UPDATE CASCADE;
      
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetevent_fk
          FOREIGN KEY (datasetevent_id)
            REFERENCES %1$I.datadesign_x_study_event (datadesign_event_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studyevent_fk
          FOREIGN KEY (studyevent_id)
            REFERENCES %1$I.study_event (event_id)
            ON UPDATE CASCADE;

      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetsubevent_fk
          FOREIGN KEY (datasetsubevent_id)
            REFERENCES %1$I.datadesign_x_study_subevent (datadesign_subevent_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studysubevent_fk
          FOREIGN KEY (studysubevent_id)
            REFERENCES %1$I.study_subevent (subevent_id)
            ON UPDATE CASCADE;
      
      -- -----------------------------------------------------------------------------------------------------------------------
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetsamplingevent_fk
          FOREIGN KEY (datasetsamplingevent_id)
            REFERENCES %1$I.datadesign_x_study_samplingevent (datadesign_samplingevent_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studysamplingevent_fk
          FOREIGN KEY (studysamplingevent_id)
            REFERENCES %1$I.study_samplingevent (samplingevent_id)
            ON UPDATE CASCADE;
      
      -- -----------------------------------------------------------------------------------------------------------------------
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetsamplingtime_fk
          FOREIGN KEY (datasetsamplingtime_id)
            REFERENCES %1$I.datadesign_x_study_samplingtime (datadesign_samplingtime_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studysamplingtime_fk
          FOREIGN KEY (studysamplingtime_id)
            REFERENCES %1$I.study_samplingtime (samplingtime_id)
            ON UPDATE CASCADE;
      
      -- -----------------------------------------------------------------------------------------------------------------------
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetcenter_fk
          FOREIGN KEY (datasetcenter_id)
            REFERENCES %1$I.datadesign_x_study_center (datadesign_center_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studycenter_fk
          FOREIGN KEY (studycenter_id)
            REFERENCES %1$I.study_center (center_id)
            ON UPDATE CASCADE;
      
      -- -----------------------------------------------------------------------------------------------------------------------
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetstartgroup_fk
          FOREIGN KEY (datasetstartgroup_id)
            REFERENCES %1$I.datadesign_x_study_startgroup (datadesign_startgroup_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studystartgroup_fk
          FOREIGN KEY (studystartgroup_id)
            REFERENCES %1$I.study_startgroup (startgroup_id)
            ON UPDATE CASCADE;
      
      -- -----------------------------------------------------------------------------------------------------------------------
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_datasetsubject_fk
          FOREIGN KEY (datasetsubject_id)
            REFERENCES %1$I.datadesign_x_study_subject (datadesign_subject_id)
            ON UPDATE CASCADE;
      ALTER TABLE %1$I.datadesign_datatable
        ADD CONSTRAINT datadesign_datatable_studysubject_fk
          FOREIGN KEY (studysubject_id)
            REFERENCES %1$I.study_subject (subject_id)
            ON UPDATE CASCADE;

      -- -----------------------------------------------------------------------------------------------------------------------
      
      INSERT INTO %1$I.datadesign_datatable (datarow_id, datarow_ordinal, studyrow_id, dataset_id, study_id, datasetevent_id, datasetsubevent_id
                                                                                    , datasetsamplingevent_id, datasetsamplingtime_id, datasetcenter_id, datasetstartgroup_id, datasetsubject_id
                                                                                    , datasetevent_name, datasetsubevent_name, datasetsamplingevent_name, datasetsamplingtime_name
                                                                                    , datasetcenter_name, datasetstartgroup_name, datasetsubject_name, datasetevent_status, datasetsubevent_status
                                                                                    , datasetsamplingevent_status, datasetsamplingtime_status, datasetcenter_status, datasetstartgroup_status
                                                                                    , datasetsubject_status, studyevent_id, studysubevent_id, studysamplingevent_id, studysamplingtime_id
                                                                                    , studycenter_id, studystartgroup_id, studysubject_id, datarow) 
      WITH
        dataset_query      AS (
                                SELECT a.dataset_id
                                     , a.study_id
                                     , a.created_at
                                FROM %3$I.dataset_x_study a
                                WHERE a.dataset_id IN (
                                                      SELECT dataset_id
                                                      FROM %1$I.dataset
                                                    )
                              )
        , datadesign_query AS (
                                SELECT a.datarow_id
                                     , l.ordinal                AS datarow_ordinal
                                     , k.studyrow_id
                                     , a.dataset_id
                                     , k.study_id
                                     
                                     , a.event_id               AS datasetevent_id
                                     , a.subevent_id            AS datasetsubevent_id
                                     , a.samplingevent_id       AS datasetsamplingevent_id
                                     , a.samplingtime_id        AS datasetsamplingtime_id
                                     , a.center_id              AS datasetcenter_id
                                     , a.startgroup_id          AS datasetstartgroup_id
                                     , a.subject_id             AS datasetsubject_id
                                     
                                     , a.event_name             AS datasetevent_name
                                     , a.subevent_name          AS datasetsubevent_name
                                     , a.samplingevent_name     AS datasetsamplingevent_name
                                     , a.samplingtime_name      AS datasetsamplingtime_name
                                     , a.center_name            AS datasetcenter_name
                                     , a.startgroup_name        AS datasetstartgroup_name
                                     , a.subject_name           AS datasetsubject_name
                                     
                                     , a.event_status           AS datasetevent_status
                                     , a.subevent_status        AS datasetsubevent_status
                                     , a.samplingevent_status   AS datasetsamplingevent_status
                                     , a.samplingtime_status    AS datasetsamplingtime_status
                                     , a.center_status          AS datasetcenter_status
                                     , a.startgroup_status      AS datasetstartgroup_status
                                     , a.subject_status         AS datasetsubject_status
                                     
                                     , c.study_event_id         AS studyevent_id
                                     , d.study_subevent_id      AS studysubevent_id
                                     , e.study_samplingevent_id AS studysamplingevent_id
                                     , f.study_samplingtime_id  AS studysamplingtime_id
                                     , g.study_center_id        AS studycenter_id
                                     , h.study_startgroup_id    AS studystartgroup_id
                                     , j.study_subject_id       AS studysubject_id
                                     , l.datarow
                                FROM %3$I.datadesign_datatable                     a
                                  INNER JOIN dataset_query                         b ON a.dataset_id = b.dataset_id
                                  INNER JOIN %3$I.datadesign_x_study_event         c ON a.event_id = c.datadesign_event_id
                                  INNER JOIN %3$I.datadesign_x_study_subevent      d ON a.subevent_id = d.datadesign_subevent_id
                                  INNER JOIN %3$I.datadesign_x_study_samplingevent e ON a.samplingevent_id = e.datadesign_samplingevent_id
                                  INNER JOIN %3$I.datadesign_x_study_samplingtime  f ON a.samplingtime_id = f.datadesign_samplingtime_id
                                  INNER JOIN %3$I.datadesign_x_study_center        g ON a.center_id = g.datadesign_center_id
                                  INNER JOIN %3$I.datadesign_x_study_startgroup    h ON a.startgroup_id = h.datadesign_startgroup_id
                                  INNER JOIN %3$I.datadesign_x_study_subject       j ON a.subject_id = j.datadesign_subject_id
                                  INNER JOIN %3$I.studyrow_x_datarow               k ON a.datarow_id = k.datarow_id
                                  INNER JOIN %3$I.datatable                        l ON a.datarow_id = l.datarow_id
                              )
      SELECT datarow_id
           , datarow_ordinal
           , studyrow_id
           , dataset_id
           , study_id
           , datasetevent_id
           , datasetsubevent_id
           , datasetsamplingevent_id
           , datasetsamplingtime_id
           , datasetcenter_id
           , datasetstartgroup_id
           , datasetsubject_id
           , datasetevent_name
           , datasetsubevent_name
           , datasetsamplingevent_name
           , datasetsamplingtime_name
           , datasetcenter_name
           , datasetstartgroup_name
           , datasetsubject_name
           , datasetevent_status
           , datasetsubevent_status
           , datasetsamplingevent_status
           , datasetsamplingtime_status
           , datasetcenter_status
           , datasetstartgroup_status
           , datasetsubject_status
           , studyevent_id
           , studysubevent_id
           , studysamplingevent_id
           , studysamplingtime_id
           , studycenter_id
           , studystartgroup_id
           , studysubject_id
           , datarow
      FROM datadesign_query;

      -- ================================================================================================

      CREATE TABLE %1$I.assay
      (
        assay_id         UUID CONSTRAINT assay_pk PRIMARY KEY,
        name             TEXT,
        description      TEXT,
        researchfield_id INTEGER,
        created_at       TIMESTAMP WITH TIME ZONE,
        updated_at       TIMESTAMP WITH TIME ZONE
      );

      INSERT INTO %1$I.assay (assay_id, name, description, researchfield_id, created_at, updated_at)
      SELECT assay_id
           , name
           , description
           , researchfield_id
           , created_at
           , updated_at
      FROM %3$I.assay
      WHERE assay_id IN (
                          SELECT assay_id
                          FROM %3$I.variable
                          WHERE dataset_id IN (
                                                SELECT dataset_id
                                                FROM %3$I.dataset_x_study
                                                WHERE study_id = (%2$L)::UUID
                                              )
                        );

      -- ================================================================================================


      CREATE TABLE %1$I.variable
      (
        variable_id      UUID CONSTRAINT variable_pk PRIMARY KEY,
        local_id         INTEGER NOT NULL,
        dataset_id       UUID,
        name             TEXT,
        description      TEXT,
        status           INTEGER,
        variabletype_id  INTEGER,
        datatype_id      INTEGER,
        researchfield_id INTEGER,
        assay_id         UUID,
        nunitprefix_id   INTEGER DEFAULT 0,
        nunit_id         INTEGER DEFAULT 0,
        dunitprefix_id   INTEGER DEFAULT 0,
        dunit_id         INTEGER DEFAULT 0,
        created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
        updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
        details          JSONB
      );
      
      ALTER TABLE %1$I.variable
        ADD CONSTRAINT variable_dataset_id_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset
            ON UPDATE CASCADE ON DELETE CASCADE;
      
      ALTER TABLE %1$I.variable
        ADD CONSTRAINT variable_assay_id_fk
          FOREIGN KEY (assay_id) REFERENCES %1$I.assay
            ON UPDATE CASCADE ON DELETE CASCADE;

      INSERT INTO %1$I.variable (variable_id, local_id, dataset_id, name, description, status, variabletype_id, datatype_id, researchfield_id, assay_id, nunitprefix_id, nunit_id, dunitprefix_id, dunit_id, created_at, updated_at, details)
      SELECT variable_id
           , local_id
           , dataset_id
           , name
           , description
           , status
           , variabletype_id
           , datatype_id
           , researchfield_id
           , assay_id
           , nunitprefix_id
           , nunit_id
           , dunitprefix_id
           , dunit_id
           , created_at
           , updated_at
           , details
      FROM %3$I.variable
      WHERE dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );

      -- ================================================================================================

      CREATE TABLE %1$I.var_dataset
      (
        var_dataset_id UUID CONSTRAINT var_dataset_pk PRIMARY KEY,
        dataset_id     UUID CONSTRAINT var_dataset_dataset_uq UNIQUE,
        name           TEXT,
        description    TEXT,
        status         INTEGER,
        created_at     TIMESTAMP WITH TIME ZONE NOT NULL,
        updated_at     TIMESTAMP WITH TIME ZONE NOT NULL,
        details        JSONB,
        CONSTRAINT var_dataset_var_dataset_dataset_uq
          UNIQUE (var_dataset_id, dataset_id)
      );
      
      ALTER TABLE %1$I.var_dataset
        ADD CONSTRAINT var_dataset_dataset_id_fk
          FOREIGN KEY (dataset_id) REFERENCES %1$I.dataset
            ON UPDATE CASCADE ON DELETE NO ACTION;

      INSERT INTO %1$I.var_dataset (var_dataset_id, dataset_id, name, description, status, created_at, updated_at, details)
      SELECT var_dataset_id
           , dataset_id
           , name
           , description
           , status
           , created_at
           , updated_at
           , details
      FROM %3$I.var_dataset
      WHERE dataset_id IN (
                            SELECT dataset_id
                            FROM %1$I.dataset
                          );

      -- ================================================================================================

      CREATE TABLE %1$I.var_variable
      (
        var_variable_id   UUID CONSTRAINT var_variable_pk PRIMARY KEY,
        var_dataset_id    UUID,
        local_id          INTEGER NOT NULL,
        name              TEXT,
        description       TEXT,
        status            INTEGER,
        search_single_id  INTEGER,
        search_generic_id INTEGER,
        datatype_id       INTEGER,
        created_at        TIMESTAMP WITH TIME ZONE NOT NULL,
        updated_at        TIMESTAMP WITH TIME ZONE NOT NULL,
        details           JSONB,
        CONSTRAINT var_variable_search_map_uq
          UNIQUE (var_dataset_id, search_single_id),
        CONSTRAINT var_variable_local_id_uq
          UNIQUE (var_dataset_id, local_id)
      );

      ALTER TABLE %1$I.var_variable
        ADD CONSTRAINT var_variable_var_dataset_id_fk
          FOREIGN KEY (var_dataset_id)
            REFERENCES %1$I.var_dataset (var_dataset_id)
            ON UPDATE CASCADE ON DELETE CASCADE;

      INSERT INTO %1$I.var_variable (var_variable_id, var_dataset_id, local_id, name, description, status, search_single_id, search_generic_id, datatype_id, created_at, updated_at, details)
      SELECT var_variable_id
           , var_dataset_id
           , local_id
           , name
           , description
           , status
           , search_single_id
           , search_generic_id
           , datatype_id
           , created_at
           , updated_at
           , details
      FROM %3$I.var_variable
      WHERE var_dataset_id IN (
                                SELECT var_dataset_id
                                FROM %3$I.var_dataset z
                                WHERE dataset_id IN (
                                                       SELECT dataset_id
                                                       FROM %1$I.dataset
                                                    )
                              );

      -- ================================================================================================
  
      CREATE TABLE %1$I.var_datatable
      (
        variable_id    UUID NOT NULL,
        var_dataset_id UUID NOT NULL,
        ordinal        INTEGER NOT NULL,
        status         INTEGER,
        created_at     TIMESTAMP WITH TIME ZONE,
        updated_at     TIMESTAMP WITH TIME ZONE,
        details        JSONB,
        datarow        JSONB,
        CONSTRAINT var_datatable_pk
          PRIMARY KEY (variable_id, var_dataset_id),
        CONSTRAINT var_datatable_ordinal_uk
          UNIQUE (var_dataset_id, ordinal)
      );
      
      ALTER TABLE %1$I.var_datatable
        ADD CONSTRAINT var_datatable_var_dataset_fk
          FOREIGN KEY (var_dataset_id) REFERENCES
            %1$I.var_dataset (var_dataset_id)
            ON UPDATE CASCADE ON DELETE CASCADE;
      
      ALTER TABLE %1$I.var_datatable
        ADD CONSTRAINT var_datatable_variable_fk
          FOREIGN KEY (variable_id)
            REFERENCES %1$I.variable (variable_id)
            ON UPDATE CASCADE ON DELETE CASCADE;
      
      INSERT INTO %1$I.var_datatable (variable_id, var_dataset_id, ordinal, status, created_at, updated_at, details, datarow) 
      SELECT variable_id
           , var_dataset_id
           , ordinal
           , status
           , created_at
           , updated_at
           , details
           , datarow
      FROM %3$I.var_datatable
      WHERE var_dataset_id IN (
                                SELECT var_dataset_id
                                FROM %3$I.var_dataset
                                WHERE dataset_id IN (
                                                      SELECT dataset_id
                                                      FROM %1$I.dataset
                                                    )
                              );

      -- ==================================================================================================================
  
    $qry$
    , _schema_name
    , _study_id
    , _app_schema);
  
  EXECUTE _main_query;
  
  IF _update_ids THEN
    
    _update_query := format(
      $qry$
      UPDATE %1$I.study SET study_id = uuid_generate_v4();
      UPDATE %1$I.dataset SET dataset_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_event SET datadesign_event_id = uuid_generate_v4();
      UPDATE %1$I.study_event SET event_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_subevent SET datadesign_subevent_id = uuid_generate_v4();
      UPDATE %1$I.study_subevent SET subevent_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_samplingevent SET datadesign_samplingevent_id = uuid_generate_v4();
      UPDATE %1$I.study_samplingevent SET samplingevent_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_samplingtime SET datadesign_samplingtime_id = uuid_generate_v4();
      UPDATE %1$I.study_samplingtime SET samplingtime_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_center SET datadesign_center_id = uuid_generate_v4();
      UPDATE %1$I.study_center SET center_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_startgroup SET datadesign_startgroup_id = uuid_generate_v4();
      UPDATE %1$I.study_startgroup SET startgroup_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_x_study_subject SET datadesign_subject_id = uuid_generate_v4();
      UPDATE %1$I.study_subject SET subject_id = uuid_generate_v4();
      UPDATE %1$I.datadesign_datatable SET datarow_id = uuid_generate_v4();
      UPDATE %1$I.studydesign_datatable SET studyrow_id = uuid_generate_v4();
      UPDATE %1$I.variable SET variable_id = uuid_generate_v4();
      UPDATE %1$I.var_dataset SET var_dataset_id = uuid_generate_v4();
      UPDATE %1$I.var_variable SET var_variable_id = uuid_generate_v4();
    $qry$
      , _schema_name);
    
    EXECUTE _update_query;
  
  END IF;
  
  RETURN QUERY SELECT TRUE, _schema_name;
END;
$fun$;

