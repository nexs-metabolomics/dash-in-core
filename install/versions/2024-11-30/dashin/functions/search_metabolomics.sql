CREATE FUNCTION dashin.search_metabolomics
(
    _mz_center NUMERIC
  , _mz_range  NUMERIC
  , _rt_center NUMERIC
  , _rt_range  NUMERIC
  , _offset    INT DEFAULT NULL
  , _limit     INT DEFAULT NULL
)
  RETURNS TABLE
          (
            STUDY_ID      UUID,
            DATASET_ID    UUID,
            VARIABLE_ID   UUID,
            STUDY_NAME    TEXT,
            DATASET_NAME  TEXT,
            VARIABLE_NAME TEXT,
            MZ            NUMERIC,
            RT            NUMERIC
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  qry TEXT;
BEGIN
  qry := dashin.search_metabolomics_generate_query(_mz_center , _mz_range , _rt_center , _rt_range , _offset , _limit);
  IF qry IS NULL THEN
    RETURN ;
  END IF;
  RETURN QUERY EXECUTE qry;
  RETURN;
END;
$$
;

ALTER FUNCTION dashin.search_metabolomics OWNER TO :application_user;
