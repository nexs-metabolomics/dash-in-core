CREATE FUNCTION dashin.generate_upvar_dataset_data_full_as_csv
(
    _upvar_dataset_id UUID
  , _owner_org        UUID
)
  RETURNS TABLE
          (
            UPVAR_DATASET TEXT
          )
  LANGUAGE sql
AS
$$
SELECT string_agg(dat , E'\n')
FROM (
       (
         -- header
         SELECT a.upvar_dataset_id                                         AS upvar_datarow_id
              , 0                                                          AS ordinal
              , 'varname;' || string_agg(a.name , ';' ORDER BY a.local_id) AS dat
         FROM dashin.upvar_variable        a
           INNER JOIN dashin.upvar_dataset b ON a.upvar_dataset_id = b.upvar_dataset_id
         WHERE (b.upvar_dataset_id, b.owner_org) = (_upvar_dataset_id, _owner_org)
         GROUP BY a.upvar_dataset_id
       )
       UNION ALL
       (
         -- data
         SELECT x0.upvar_datarow_id
              , x0.ordinal
              , x0.name || ';' || string_agg(x0.value , ';' ORDER BY x0.ordinality) AS dat
         FROM (
                SELECT a.upvar_datarow_id
                     , a.ordinal
                     , a.name
                     , c.value
                     , c.ordinality
                FROM dashin.upvar_datatable                             a
                     INNER JOIN dashin.upvar_dataset                    b ON a.upvar_dataset_id = b.upvar_dataset_id
                   , jsonb_array_elements_text(datarow) WITH ORDINALITY c(value, ordinality)
                WHERE (b.upvar_dataset_id, b.owner_org) = (_upvar_dataset_id, _owner_org)
              ) x0
         GROUP BY x0.upvar_datarow_id
                , x0.ordinal
                , x0.name
       )
       ORDER BY ordinal
     ) x1;
$$;

