CREATE FUNCTION dashin.datadesign_delete_design
(
    _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            MESSAGE TEXT,
            STATUS  TEXT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  status TEXT DEFAULT 'none';
BEGIN
  DELETE
  FROM dashin.datadesign
  WHERE dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d WHERE (d.dataset_id, d.owner_org) = (_dataset_id, _owner_org));
  IF found THEN
    status := 'deleted';
  END IF;
  RETURN QUERY
    SELECT t.element_name
         , t.affected_rows
    FROM (
           VALUES ('status', status)
         ) t(element_name, affected_rows);
END ;
$$;

ALTER FUNCTION dashin.datadesign_delete_design(_dataset_id UUID, _owner_org UUID) OWNER TO :application_user;

