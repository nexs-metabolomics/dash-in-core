CREATE FUNCTION dashin.studydesign_validate_data_for_import
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS TABLE
          (
            ELEMENT_NAME       TEXT,
            ID                 UUID,
            ID2                UUID,
            NAME               TEXT,
            NAME2              TEXT,
            DESCRIPTION        TEXT,
            DESCRIPTION2       TEXT,
            DATA_STUDY_MATCHED INT,
            DATA_UNMATCHED     INT,
            STUDY_UNMATCHED    INT,
            MATCH_TEXT         TEXT
          )
  LANGUAGE sql
AS
$$
WITH
  gatekeeper
                         AS (
                              SELECT _study_id
                                   , _dataset_id
                                   , _owner_org
                              WHERE exists(SELECT FROM dashin.study st WHERE (st.study_id, st.owner_org) = (_study_id, _owner_org))
                                AND exists(SELECT FROM dashin.dataset ds WHERE (ds.dataset_id, ds.owner_org) = (_dataset_id, _owner_org))
                            )
  , match_event          AS (
                              SELECT study_event_id
                                   , study_event_name
                                   , study_event_description
                                   , data_event_id
                                   , data_event_name
                              FROM (
                                     SELECT event_id    AS study_event_id
                                          , name        AS study_event_name
                                          , description AS study_event_description
                                     FROM dashin.study_event
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT event_id   AS data_event_id
                                              , event_name AS data_event_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_event_name = d.data_event_name
                            )
  , match_subevent       AS (
                              SELECT study_subevent_id
                                   , study_subevent_name
                                   , study_subevent_description
                                   , data_subevent_id
                                   , data_subevent_name
                              FROM (
                                     SELECT subevent_id AS study_subevent_id
                                          , name        AS study_subevent_name
                                          , description AS study_subevent_description
                                     FROM dashin.study_subevent
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT subevent_id   AS data_subevent_id
                                              , subevent_name AS data_subevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_subevent_name = d.data_subevent_name
                            )
  , match_samplingevent  AS (
                              SELECT study_samplingevent_id
                                   , study_samplingevent_name
                                   , study_samplingevent_description
                                   , data_samplingevent_id
                                   , data_samplingevent_name
                              FROM (
                                     SELECT samplingevent_id AS study_samplingevent_id
                                          , name             AS study_samplingevent_name
                                          , description      AS study_samplingevent_description
                                     FROM dashin.study_samplingevent
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT samplingevent_id   AS data_samplingevent_id
                                              , samplingevent_name AS data_samplingevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_samplingevent_name = d.data_samplingevent_name
                            )
  , match_samplingtime   AS (
                              SELECT study_samplingtime_id
                                   , study_samplingevent_id
                                   , study_samplingevent
                                   , study_samplingevent_description
                                   , study_samplingtime
                                   , study_samplingtime_description
                                   , data_samplingtime_id
                                   , data_samplingevent_id
                                   , data_samplingtime_name
                                   , data_samplingevent_name
                              FROM (
                                     SELECT samplingtime_id     AS study_samplingtime_id
                                          , sm.samplingevent_id AS study_samplingevent_id
                                          , sm.name             AS study_samplingevent
                                          , sm.description      AS study_samplingevent_description
                                          , st.name             AS study_samplingtime
                                          , st.description      AS study_samplingtime_description
                                     FROM dashin.study_samplingtime             st
                                          INNER JOIN dashin.study_samplingevent sm USING (samplingevent_id)
                                        ,                                       gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT samplingtime_id    AS data_samplingtime_id
                                              , samplingevent_id   AS data_samplingevent_id
                                              , samplingtime_name  AS data_samplingtime_name
                                              , samplingevent_name AS data_samplingevent_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON (s.study_samplingevent, s.study_samplingtime) = (d.data_samplingevent_name, d.data_samplingtime_name)
                            )
  , match_center         AS (
                              SELECT *
                              FROM (
                                     SELECT center_id   AS study_center_id
                                          , name        AS study_center_name
                                          , description AS study_center_description
                                     FROM dashin.study_center
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT center_id   AS data_center_id
                                              , center_name AS data_center_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_center_name = d.data_center_name
                            )
  , match_startgroup     AS (
                              SELECT *
                              FROM (
                                     SELECT startgroup_id AS study_startgroup_id
                                          , name          AS study_startgroup_name
                                          , description   AS study_startgroup_description
                                     FROM dashin.study_startgroup
                                        ,        gatekeeper
                                     WHERE study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT startgroup_id   AS data_startgroup_id
                                              , startgroup_name AS data_startgroup_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON s.study_startgroup_name = d.data_startgroup_name
                            )
  , match_subject        AS (
                              SELECT *
                              FROM (
                                     SELECT subject_id     AS study_subject_id
                                          , a.center_id    AS study_center_id
                                          , a.name AS study_subject_name
                                          , a.description  AS study_subject_description
                                          , b.name         AS study_center_name
                                          , b.description  AS study_center_description
                                     FROM dashin.study_subject           a
                                          INNER JOIN dashin.study_center b ON a.center_id = b.center_id
                                        ,                                gatekeeper
                                     WHERE a.study_id = _study_id
                                   ) s
                                FULL JOIN
                              (
                                SELECT DISTINCT subject_id   AS data_subject_id
                                              , center_id    AS data_center_id
                                              , subject_name AS data_subject_name
                                              , center_name  AS data_center_name
                                FROM dashin.datadesign_datatable
                                   ,        gatekeeper
                                WHERE dataset_id = _dataset_id
                              )      d ON (s.study_center_name, s.study_subject_name) = (d.data_center_name, d.data_subject_name)
                            )
  , design_match         AS (
                              SELECT *
                              FROM (
                                     (
                                       SELECT 'event'                           AS element_name
                                            , data_event_id                     AS data_id
                                            , NULL::UUID                        AS data_id2
                                            , data_event_name                   AS data_name
                                            , NULL::TEXT                        AS data_name2
                                            , study_event_description           AS data_description
                                            , NULL::TEXT                        AS data_description2
                                            , study_event_id                    AS study_id
                                            , NULL::UUID                        AS study_id2
                                            , study_event_name                  AS study_name
                                            , NULL::TEXT                        AS study_name2
                                            , study_event_description           AS study_description
                                            , NULL::TEXT                        AS study_description2
                                            , (data_event_id IS NOT NULL)::INT  AS data_exists
                                            , (study_event_id IS NOT NULL)::INT AS study_exists
                                       FROM match_event
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'subevent'                           AS element_name
                                            , data_subevent_id                     AS data_id
                                            , NULL::UUID                           AS data_id2
                                            , data_subevent_name                   AS data_name
                                            , NULL::TEXT                           AS data_name2
                                            , study_subevent_description           AS data_description
                                            , NULL::TEXT                           AS data_description2
                                            , study_subevent_id                    AS study_id
                                            , NULL::UUID                           AS study_id2
                                            , study_subevent_name                  AS study_name
                                            , NULL::TEXT                           AS study_name2
                                            , study_subevent_description           AS study_description
                                            , NULL::TEXT                           AS study_description2
                                            , (data_subevent_id IS NOT NULL)::INT  AS data_exists
                                            , (study_subevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_subevent
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'samplingevent'                           AS element_name
                                            , data_samplingevent_id                     AS data_id
                                            , NULL::UUID                                AS data_id2
                                            , data_samplingevent_name                   AS data_name
                                            , NULL::TEXT                                AS data_name2
                                            , study_samplingevent_description           AS data_description
                                            , NULL::TEXT                                AS data_description2
                                            , study_samplingevent_id                    AS study_id
                                            , NULL::UUID                                AS study_id2
                                            , study_samplingevent_name                  AS study_name
                                            , NULL::TEXT                                AS study_name2
                                            , study_samplingevent_description           AS study_description
                                            , NULL::TEXT                                AS study_description2
                                            , (data_samplingevent_id IS NOT NULL)::INT  AS data_exists
                                            , (study_samplingevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_samplingevent
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'samplingtime'                            AS element_name
                                            , data_samplingtime_id                      AS data_id
                                            , data_samplingevent_id                     AS data_id2
                                            , data_samplingtime_name                    AS data_name
                                            , data_samplingevent_name                   AS data_name2
                                            , study_samplingtime_description            AS data_description
                                            , study_samplingevent_description           AS data_description2
                                            , study_samplingtime_id                     AS study_id
                                            , study_samplingevent_id                    AS study_id2
                                            , study_samplingtime                        AS study_name
                                            , study_samplingevent                       AS study_name2
                                            , study_samplingtime_description            AS study_description
                                            , study_samplingevent_description           AS study_description2
                                            , (data_samplingtime_id IS NOT NULL)::INT   AS data_exists
                                            , (study_samplingevent_id IS NOT NULL)::INT AS study_exists
                                       FROM match_samplingtime
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'center'                           AS element_name
                                            , data_center_id                     AS data_id
                                            , NULL::UUID                         AS data_id2
                                            , data_center_name                   AS data_name
                                            , NULL::TEXT                         AS data_name2
                                            , study_center_description           AS data_description
                                            , NULL::TEXT                         AS data_description2
                                            , study_center_id                    AS study_id
                                            , NULL::UUID                         AS study_id2
                                            , study_center_name                  AS study_name
                                            , NULL::TEXT                         AS study_name2
                                            , study_center_description           AS study_description
                                            , NULL::TEXT                         AS study_description2
                                            , (data_center_id IS NOT NULL)::INT  AS data_exists
                                            , (study_center_id IS NOT NULL)::INT AS study_exists
                                       FROM match_center
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'startgroup'                           AS element_name
                                            , data_startgroup_id                     AS data_id
                                            , NULL::UUID                             AS data_id2
                                            , data_startgroup_name                   AS data_name
                                            , NULL::TEXT                             AS data_name2
                                            , study_startgroup_description           AS data_description
                                            , NULL::TEXT                             AS data_description2
                                            , study_startgroup_id                    AS study_id
                                            , NULL::UUID                             AS study_id2
                                            , study_startgroup_name                  AS study_name
                                            , NULL::TEXT                             AS study_name2
                                            , study_startgroup_description           AS study_description
                                            , NULL::TEXT                             AS study_description2
                                            , (data_startgroup_id IS NOT NULL)::INT  AS data_exists
                                            , (study_startgroup_id IS NOT NULL)::INT AS study_exists
                                       FROM match_startgroup
                                     )
                                     UNION ALL
                                     (
                                       SELECT 'subject'                           AS element_name
                                            , data_subject_id                     AS data_id
                                            , data_center_id                      AS data_id2
                                            , data_subject_name                   AS data_name
                                            , data_center_name                    AS data_name2
                                            , study_subject_description           AS data_description
                                            , study_center_description            AS data_description2
                                            , study_subject_id                    AS study_id
                                            , study_center_id                     AS study_id2
                                            , study_subject_name                  AS study_name
                                            , study_center_name                   AS study_name2
                                            , study_subject_description           AS study_description
                                            , study_center_description            AS study_description2
                                            , (data_subject_id IS NOT NULL)::INT  AS data_exists
                                            , (study_subject_id IS NOT NULL)::INT AS study_exists
                                       FROM match_subject
                                     )
                                   ) x0
                            )
  , design_match_summary AS (
                              SELECT element_name
                                   , data_id
                                   , data_id2
                                   , data_name
                                   , data_name2
                                   , data_description
                                   , data_description2
                                   , study_id
                                   , study_id2
                                   , study_name
                                   , study_name2
                                   , study_description
                                   , study_description2
                                   , ((data_exists, study_exists) = (1, 1))::INT AS data_and_study
                                   , ((data_exists, study_exists) = (1, 0))::INT AS data_not_study
                                   , ((data_exists, study_exists) = (0, 1))::INT AS study_not_data
                                   , CASE
                                       WHEN (data_exists, study_exists) = (1, 1) THEN 'data_study_matched'
                                       WHEN (data_exists, study_exists) = (1, 0) THEN 'data_unmatched'
                                       WHEN (data_exists, study_exists) = (0, 1) THEN 'study_unmatched'
                                     END                                         AS match_text
                              FROM design_match
                            )
  , data_unmatched       AS (
                              SELECT 'data_unmatched_' || element_name AS element_name
                                   , data_id                           AS id
                                   , data_id2                          AS id2
                                   , data_name                         AS name
                                   , data_name2                        AS name2
                                   , data_description                  AS description
                                   , data_description2                 AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_unmatched'
                            )
  , study_unmatched      AS (
                              SELECT 'study_unmatched_' || element_name AS element_name
                                   , study_id                           AS id
                                   , study_id2                          AS id2
                                   , study_name                         AS name
                                   , study_name2                        AS name2
                                   , study_description                  AS description
                                   , study_description2                 AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'study_unmatched'
                            )
  , study_matched        AS (
                              SELECT 'study_matched_' || element_name AS element_name
                                   , study_id                         AS id
                                   , study_id2                        AS id2
                                   , study_name                       AS name
                                   , study_name2                      AS name2
                                   , study_description                AS description
                                   , study_description2               AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_study_matched'
                            )
  , data_matched         AS (
                              SELECT 'data_matched_' || element_name AS element_name
                                   , data_id                         AS id
                                   , data_id2                        AS id2
                                   , data_name                       AS name
                                   , data_name2                      AS name2
                                   , data_description                AS description
                                   , data_description2               AS description2
                                   , data_and_study
                                   , data_not_study
                                   , study_not_data
                                   , match_text
                              FROM design_match_summary
                              WHERE match_text = 'data_study_matched'
                            )
  , all_summary          AS (
    
                              SELECT 'all_summary'            AS element_name
                                   , NULL::UUID               AS id
                                   , NULL::UUID               AS id2
                                   , NULL::TEXT               AS name
                                   , NULL::TEXT               AS name2
                                   , NULL::TEXT               AS description
                                   , NULL::TEXT               AS description2
                                   , sum(data_and_study)::INT AS data_study_matched
                                   , sum(data_not_study)::INT AS data_unmatched
                                   , sum(study_not_data)::INT AS study_unmatched
                                   , NULL::TEXT               AS match_text
                              FROM design_match_summary
                            )
SELECT *
FROM all_summary
UNION ALL
SELECT *
FROM data_unmatched
UNION ALL
SELECT *
FROM study_unmatched
UNION ALL
SELECT *
FROM data_matched
UNION ALL
SELECT *
FROM study_matched
ORDER BY element_name
       , name
       , name2;
$$;
