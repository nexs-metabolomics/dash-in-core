CREATE FUNCTION dashin.datadesign_unvalidated_or_datatable
(
    _dataset_id UUID
  , _owner_org  UUID DEFAULT NULL
)
  RETURNS TABLE
          (
            DATAROW_ID           UUID,
            DATASET_ID           UUID,
            EVENT_ID             UUID,
            SUBEVENT_ID          UUID,
            SAMPLINGEVENT_ID     UUID,
            SAMPLINGTIME_ID      UUID,
            CENTER_ID            UUID,
            STARTGROUP_ID        UUID,
            SUBJECT_ID           UUID,
            EVENT_NAME           TEXT,
            SUBEVENT_NAME        TEXT,
            SAMPLINGEVENT_NAME   TEXT,
            SAMPLINGTIME_NAME    TEXT,
            CENTER_NAME          TEXT,
            STARTGROUP_NAME      TEXT,
            SUBJECT_NAME         TEXT,
            EVENT_STATUS         INT,
            SUBEVENT_STATUS      INT,
            SAMPLINGEVENT_STATUS INT,
            SAMPLINGTIME_STATUS  INT,
            CENTER_STATUS        INT,
            STARTGROUP_STATUS    INT,
            SUBJECT_STATUS       INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  design_exists BOOL;
BEGIN
  design_exists := dashin.datadesign_exists(_dataset_id , _owner_org);
  IF design_exists THEN
    RETURN QUERY SELECT a.datarow_id
                      , a.dataset_id
                      , a.event_id
                      , a.subevent_id
                      , a.samplingevent_id
                      , a.samplingtime_id
                      , a.center_id
                      , a.startgroup_id
                      , a.subject_id
                      , a.event_name
                      , a.subevent_name
                      , a.samplingevent_name
                      , a.samplingtime_name
                      , a.center_name
                      , a.startgroup_name
                      , a.subject_name
                      , a.event_status
                      , a.subevent_status
                      , a.samplingevent_status
                      , a.samplingtime_status
                      , a.center_status
                      , a.startgroup_status
                      , a.subject_status
                 FROM dashin.datadesign_datatable a
                 WHERE a.dataset_id = _dataset_id;
    ELSE
      RETURN QUERY SELECT a.datarow_id
                        , a.dataset_id
                        , a.event_id
                        , a.subevent_id
                        , a.samplingevent_id
                        , a.samplingtime_id
                        , a.center_id
                        , a.startgroup_id
                        , a.subject_id
                        , a.event_name
                        , a.subevent_name
                        , a.samplingevent_name
                        , a.samplingtime_name
                        , a.center_name
                        , a.startgroup_name
                        , a.subject_name
                        , a.event_status
                        , a.subevent_status
                        , a.samplingevent_status
                        , a.samplingtime_status
                        , a.center_status
                        , a.startgroup_status
                        , a.subject_status
                   FROM
                     dashin.datadesign_unvalidated_design_datatable(_dataset_id , _owner_org) a;
  END IF;
  RETURN;
END;
$$;
