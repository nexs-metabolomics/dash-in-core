CREATE FUNCTION dashin.studydata_add_dataset_to_study
(
    _study_id   UUID
  , _dataset_id UUID
  , _owner_org  UUID
)
  RETURNS
    TABLE
    (
      STATUS      TEXT,
      STATUS_ID   INT,
      DESCRIPTION TEXT
    )
  LANGUAGE plpgsql
AS
$$
DECLARE
  ds_matched            INT;
  d_unmatched           INT;
  s_unmatched           INT;
  dataset_already_added BOOL;
  -- ----------------------
  max_ordinal           INT;
  studyvar_n            INT;
  datasetvar_n          INT;
  -- ----------------------
  studydesign_rows      INT;
  study_rows            INT;
  dataset_rows          INT;
  data_rows             INT;
  event_rows            INT;
  subevent_rows         INT;
  samplingevent_rows    INT;
  samplingtime_rows     INT;
  center_rows           INT;
  startgroup_rows       INT;
  subject_rows          INT;

BEGIN
  -- ------------------------------------------------------------------
  -- check
  -- ------------------------------------------------------------------
  dataset_already_added := exists(SELECT * FROM dashin.dataset_x_study x WHERE (x.dataset_id, x.study_id) = (_dataset_id, _study_id));
  
  IF dataset_already_added
  THEN
    RETURN QUERY VALUES ('result', 2, 'Dataset already added to study.');
    RETURN;
  END IF;
  
  -- ------------------------------------------------------------------
  -- validate, match and permissions (6ms)
  -- ------------------------------------------------------------------
  SELECT a.data_study_matched
       , a.data_unmatched
       , a.study_unmatched
  INTO ds_matched,d_unmatched,s_unmatched
  FROM dashin.studydesign_validate_data_for_import(_study_id , _dataset_id , _owner_org) a
  WHERE a.element_name = 'all_summary';
  
  -- ------------------------------------------------------------------
  -- return if dataset is not a subset of study
  -- ------------------------------------------------------------------
  IF d_unmatched != 0
  THEN
    RETURN QUERY VALUES ('result', 0, 'failed. Data design does not match study design.')
                      , ('matched', ds_matched, '')
                      , ('data unmatched', d_unmatched, CASE WHEN d_unmatched > 0 THEN 'data cannot have unmatched design combinations' ELSE '' END)
                      , ('study unmatched', s_unmatched, '');
    RETURN;
  END IF;
  
  -- ------------------------------------------------------------------
  -- dataset_x_study
  -- ------------------------------------------------------------------
  INSERT INTO dashin.dataset_x_study (dataset_id, study_id)
  VALUES (_dataset_id, _study_id)
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS dataset_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- event (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_event h
  SET (study_id, study_event_id) = (k.study_id, k.study_event_id)
  FROM (
         SELECT DISTINCT a.event_id AS datadesign_event_id
                       , b.event_id AS study_event_id
                       , a.dataset_id
                       , b.study_id
                       , a.event_name
         FROM dashin.datadesign_datatable a
           FULL JOIN dashin.study_event   b ON a.event_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_event_id) = (k.dataset_id, k.datadesign_event_id);
  GET DIAGNOSTICS event_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- subevent (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_subevent h
  SET (study_id, study_subevent_id) = (k.study_id, k.study_subevent_id)
  FROM (
         SELECT DISTINCT a.subevent_id AS datadesign_subevent_id
                       , b.subevent_id AS study_subevent_id
                       , a.dataset_id
                       , b.study_id
                       , a.subevent_name
         FROM dashin.datadesign_datatable  a
           FULL JOIN dashin.study_subevent b ON a.subevent_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_subevent_id) = (k.dataset_id, k.datadesign_subevent_id);
  GET DIAGNOSTICS subevent_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- samplingevent (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_samplingevent h
  SET (study_id, study_samplingevent_id) = (k.study_id, k.study_samplingevent_id)
  FROM (
         SELECT DISTINCT a.samplingevent_id AS datadesign_samplingevent_id
                       , b.samplingevent_id AS study_samplingevent_id
                       , a.dataset_id
                       , b.study_id
                       , a.samplingevent_name
         FROM dashin.datadesign_datatable       a
           FULL JOIN dashin.study_samplingevent b ON a.samplingevent_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_samplingevent_id) = (k.dataset_id, k.datadesign_samplingevent_id);
  GET DIAGNOSTICS samplingevent_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- samplingtime (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_samplingtime m
  SET (study_id, study_samplingtime_id) = (l.study_id, l.study_samplingtime_id)
  FROM (
         SELECT DISTINCT dataset_id
                       , samplingevent_id   AS datadesign_samplingevent_id
                       , samplingtime_id    AS datadesign_samplingtime_id
                       , samplingtime_name  AS datadesign_samplingtime_name
                       , samplingevent_name AS datadesign_samplingevent_name
         FROM dashin.datadesign_datatable
         WHERE dataset_id = _dataset_id
         ORDER BY samplingtime_name
                , samplingevent_name
       ) k
    INNER JOIN
  (
    SELECT a.samplingevent_id AS study_samplingevent_id
         , a.samplingtime_id  AS study_samplingtime_id
         , a.name             AS study_samplingtime_name
         , b.name             AS study_samplingevent_name
         , b.study_id
    FROM dashin.study_samplingtime          a
      INNER JOIN dashin.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
    WHERE b.study_id = _study_id
    ORDER BY a.name
           , b.name
  )      l ON (k.datadesign_samplingtime_name, k.datadesign_samplingevent_name) = (l.study_samplingtime_name, l.study_samplingevent_name)
  WHERE (m.dataset_id, m.datadesign_samplingtime_id) = (k.dataset_id, k.datadesign_samplingtime_id);
  GET DIAGNOSTICS samplingtime_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- center (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_center h
  SET (study_id, study_center_id) = (k.study_id, k.study_center_id)
  FROM (
         SELECT DISTINCT a.center_id AS datadesign_center_id
                       , b.center_id AS study_center_id
                       , a.dataset_id
                       , b.study_id
                       , a.center_name
         FROM dashin.datadesign_datatable a
           FULL JOIN dashin.study_center  b ON a.center_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_center_id) = (k.dataset_id, k.datadesign_center_id);
  GET DIAGNOSTICS center_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- startgroup (1ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_startgroup h
  SET (study_id, study_startgroup_id) = (k.study_id, k.study_startgroup_id)
  FROM (
         SELECT DISTINCT a.startgroup_id AS datadesign_startgroup_id
                       , b.startgroup_id AS study_startgroup_id
                       , a.dataset_id
                       , b.study_id
                       , a.startgroup_name
         FROM dashin.datadesign_datatable    a
           FULL JOIN dashin.study_startgroup b ON a.startgroup_name = b.name
         WHERE a.dataset_id = _dataset_id
           AND b.study_id = _study_id
       ) k
  WHERE (h.dataset_id, h.datadesign_startgroup_id) = (k.dataset_id, k.datadesign_startgroup_id);
  GET DIAGNOSTICS startgroup_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- subject (2.5ms)
  -- ------------------------------------------------------------------
  UPDATE dashin.datadesign_x_study_subject h
  SET (study_id, study_subject_id) = (k.study_id, k.study_subject_id)
  FROM (
         SELECT DISTINCT g.study_id
                       , g.study_subject_id
                       , f.dataset_id
                       , f.datadesign_subject_id
         FROM (
                SELECT DISTINCT dataset_id
                              , subject_id AS datadesign_subject_id
                              , subject_name
                              , center_name
                FROM dashin.datadesign_datatable
                WHERE dataset_id = _dataset_id
              ) f
           FULL JOIN
         (
           SELECT DISTINCT a.subject_id   AS study_subject_id
                         , a.name AS subject_name
                         , a.center_id  AS study_center_id
                         , b.name         AS center_name
                         , a.study_id
           FROM dashin.study_subject       a
             FULL JOIN dashin.study_center b ON (a.center_id, a.study_id) = (b.center_id, a.study_id)
           WHERE a.study_id = _study_id
         )      g ON (f.center_name, f.subject_name) = (g.center_name, g.subject_name)
       ) k
  WHERE (h.dataset_id, h.datadesign_subject_id) = (k.dataset_id, k.datadesign_subject_id);
  GET DIAGNOSTICS subject_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- add entries to studydesign and studydatatable (1ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studydataset (study_id)
  VALUES (_study_id)
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS studydesign_rows = ROW_COUNT;
  
  CREATE TEMPORARY TABLE temp_studyrows_with_datarow_link
  (
    studyrow_id      UUID DEFAULT uuid_generate_v4(),
    datarow_id       UUID,
    study_id         UUID,
    dataset_id       UUID,
    event_id         UUID,
    subevent_id      UUID,
    samplingevent_id UUID,
    samplingtime_id  UUID,
    center_id        UUID,
    startgroup_id    UUID,
    subject_id       UUID
  )
    ON COMMIT DROP;
  
  -- ------------------------------------------------------------------
  -- generate studyrows and datarow-links (4104ms,3353ms)
  -- ------------------------------------------------------------------
  INSERT INTO temp_studyrows_with_datarow_link (studyrow_id, datarow_id, study_id, dataset_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id)
  SELECT coalesce(p.studyrow_id , uuid_generate_v4())            AS studyrow_id
       , q.datarow_id
       , coalesce(p.study_id , q.study_id)                       AS study_id
       , q.dataset_id
       , coalesce(p.event_id , q.study_event_id)                 AS event_id
       , coalesce(p.subevent_id , q.study_subevent_id)           AS subevent_id
       , coalesce(p.samplingevent_id , q.study_samplingevent_id) AS samplingevent_id
       , coalesce(p.samplingtime_id , q.study_samplingtime_id)   AS samplingtime_id
       , coalesce(p.center_id , q.study_center_id)               AS center_id
       , coalesce(p.startgroup_id , q.study_startgroup_id)       AS startgroup_id
       , coalesce(p.subject_id , q.study_subject_id)             AS subject_id
  FROM (
         SELECT studyrow_id
              , study_id
              , event_id
              , subevent_id
              , samplingevent_id
              , samplingtime_id
              , center_id
              , startgroup_id
              , subject_id
         FROM dashin.studydatatable
         WHERE study_id = _study_id
       )         p
    RIGHT JOIN (
                 SELECT a.datarow_id
                      , a.dataset_id
                      , z.study_id
                      , b.study_event_id
                      , c.study_subevent_id
                      , d.study_samplingevent_id
                      , e.study_samplingtime_id
                      , f.study_center_id
                      , g.study_startgroup_id
                      , h.study_subject_id
                 FROM dashin.datadesign_datatable                     a
                   INNER JOIN dashin.dataset_x_study                  z ON a.dataset_id = z.dataset_id
                   LEFT JOIN  dashin.datadesign_x_study_event         b ON (a.dataset_id, a.event_id) = (b.dataset_id, b.datadesign_event_id)
                   LEFT JOIN  dashin.datadesign_x_study_subevent      c ON (a.dataset_id, a.subevent_id) = (c.dataset_id, c.datadesign_subevent_id)
                   LEFT JOIN  dashin.datadesign_x_study_samplingevent d ON (a.dataset_id, a.samplingevent_id) = (d.dataset_id, d.datadesign_samplingevent_id)
                   LEFT JOIN  dashin.datadesign_x_study_samplingtime  e ON (a.dataset_id, a.samplingtime_id) = (e.dataset_id, e.datadesign_samplingtime_id)
                   LEFT JOIN  dashin.datadesign_x_study_center        f ON (a.dataset_id, a.center_id) = (f.dataset_id, f.datadesign_center_id)
                   LEFT JOIN  dashin.datadesign_x_study_startgroup    g ON (a.dataset_id, a.startgroup_id) = (g.dataset_id, g.datadesign_startgroup_id)
                   LEFT JOIN  dashin.datadesign_x_study_subject       h ON (a.dataset_id, a.subject_id) = (h.dataset_id, h.datadesign_subject_id)
                 WHERE a.dataset_id = _dataset_id
                   AND z.study_id = _study_id
               ) q
                 ON (
                     p.event_id, p.subevent_id, p.samplingevent_id, p.samplingtime_id, p.center_id, p.startgroup_id, p.subject_id
                    ) = (
                         q.study_event_id, q.study_subevent_id, q.study_samplingevent_id, q.study_samplingtime_id, q.study_center_id, q.study_startgroup_id, q.study_subject_id
                        );
  
  -- ------------------------------------------------------------------
  -- insert studyrows (4ms,2.5ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studydatatable (studyrow_id, study_id, event_id, subevent_id, samplingevent_id, samplingtime_id, center_id, startgroup_id, subject_id)
  SELECT studyrow_id
       , study_id
       , event_id
       , subevent_id
       , samplingevent_id
       , samplingtime_id
       , center_id
       , startgroup_id
       , subject_id
  FROM temp_studyrows_with_datarow_link
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS study_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- insert studyrow-datarow-links (12ms,6ms)
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyrow_x_datarow (studyrow_id, study_id, datarow_id, dataset_id)
  SELECT studyrow_id
       , study_id
       , datarow_id
       , dataset_id
  FROM temp_studyrows_with_datarow_link;
  GET DIAGNOSTICS data_rows = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- IMPORT VARIABLES INTO STUDY
  -- ------------------------------------------------------------------
  
  -- ------------------------------------------------------------------
  -- dataset variables
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_dataset_variables
  (
    variable_id      UUID,
    dataset_id       UUID,
    name             TEXT,
    datatype_id      INT,
    variabletype_id INT,
    local_id         INT
  )
    ON COMMIT DROP;
  INSERT INTO temp_dataset_variables (variable_id, dataset_id, name, datatype_id, variabletype_id, local_id)
  SELECT b.variable_id
       , b.dataset_id
       , b.name
       , b.datatype_id
       , b.variabletype_id
       , b.local_id
  FROM dashin.variable b
  WHERE b.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset x WHERE (x.dataset_id, x.owner_org) = (_dataset_id, _owner_org));
  
  -- ------------------------------------------------------------------
  -- studyvar_x_datasetvar
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_studyvar_x_var
  (
    studyvariable_id UUID,
    variable_id      UUID,
    dataset_id       UUID,
    name             TEXT,
    datatype_id      INT,
    variabletype_id INT,
    local_id         INT
  )
    ON COMMIT DROP;
  INSERT INTO temp_studyvar_x_var (studyvariable_id, variable_id, dataset_id, name, datatype_id, variabletype_id, local_id)
  SELECT uuid_generate_v4() AS studyvariable_id
       , b.variable_id
       , b.dataset_id
       , b.name
       , b.datatype_id
       , b.variabletype_id
       , b.local_id
  FROM temp_dataset_variables b;
  
  -- ------------------------------------------------------------------
  -- max ordinal
  -- ------------------------------------------------------------------
  max_ordinal := (
                   SELECT coalesce(x.max_val , 0)::INT
                   FROM (
                          SELECT max(b.ordinal) AS max_val
                          FROM dashin.studyvariable b
                          WHERE b.study_id = _study_id
                            AND exists(SELECT FROM dashin.study x WHERE (x.study_id, x.owner_org) = (_study_id, _owner_org))
                        ) x
                 );
  
  -- ------------------------------------------------------------------
  -- prepare datavar for insert
  -- ------------------------------------------------------------------
  CREATE TEMPORARY TABLE temp_datavar_for_insert
  (
    studyvariable_id UUID,
    name             TEXT,
    label            TEXT,
    ordinal          INT,
    datatype_id      INT,
    variabletype_id INT,
    variable_id      UUID,
    dataset_id       UUID
  )
    ON COMMIT DROP;
  INSERT INTO temp_datavar_for_insert (studyvariable_id, name, label, ordinal, datatype_id, variabletype_id, variable_id, dataset_id)
  SELECT b.studyvariable_id
       , b.name
       , b.name                                                AS label
       , row_number() OVER (ORDER BY b.local_id) + max_ordinal AS ordinal
       , b.datatype_id
       , b.variabletype_id
       , b.variable_id
       , b.dataset_id
  FROM temp_studyvar_x_var b;
  
  -- ------------------------------------------------------------------
  -- add studyvariables
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyvariable (studyvariable_id, study_id, name, label, ordinal, datatype_id, variabletype_id)
  SELECT studyvariable_id
       , (_study_id)::UUID
       , name
       , label
       , ordinal
       , datatype_id
       , variabletype_id
  FROM temp_datavar_for_insert
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS studyvar_n = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- link studyvar-x-datavar
  -- ------------------------------------------------------------------
  INSERT INTO dashin.studyvariable_x_variable (studyvariable_id, study_id, variable_id, dataset_id)
  SELECT coalesce(b.studyvariable_id , a.studyvariable_id)
       , b.study_id
       , a.variable_id
       , a.dataset_id
  FROM temp_datavar_for_insert     a
    LEFT JOIN dashin.studyvariable b ON a.name = b.name
  WHERE b.study_id = _study_id
  ON CONFLICT DO NOTHING;
  GET DIAGNOSTICS datasetvar_n = ROW_COUNT;
  
  -- ------------------------------------------------------------------
  -- return result summary
  -- ------------------------------------------------------------------
  RETURN QUERY VALUES ('result', 1, 'Dataset successfully added.')
                    , ('matched', ds_matched, 'matching design elements (individually matched, not cross-referenced)')
                    , ('data unmatched', d_unmatched, 'design elements in data not matching studydesign')
                    , ('study unmatched', s_unmatched, 'design elements in stiudy not matching datadesign')
                    , ('studydesign created', studydesign_rows, '')
                    , ('studyrows added', study_rows, 'number of studyrows (cross-referenced unique datarows) added to study')
                    , ('datarows linked', data_rows, 'number of new datarows linked to studyrows')
                    , ('dataset', dataset_rows, '')
                    , ('variables linked', datasetvar_n, '')
                    , ('event', event_rows, 'number of events')
                    , ('subevent', subevent_rows, 'number of subevents')
                    , ('samplingevent', samplingevent_rows, 'number of samplingevents')
                    , ('samplingtime', samplingtime_rows, 'number of samplingtimes')
                    , ('center', center_rows, 'number of centers')
                    , ('startgroup', startgroup_rows, 'number of startgroups')
                    , ('subject', subject_rows, 'number of subjects');
  RETURN;
END;
$$
;
