CREATE FUNCTION dashin.get_study_element_info
(
  _element_id UUID
)
  RETURNS TABLE
          (
            ELEMENT_ID       UUID,
            STUDY_ID         UUID,
            NAME             TEXT,
            LABEL            TEXT,
            DESCRIPTION      TEXT,
            ORDINAL          INTEGER,
            VARIABLETYPE_ID INTEGER
          )
  LANGUAGE sql
AS
$$
(
  SELECT a.samplingevent_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 6                  AS variabletype_id
  FROM dashin.study_samplingevent a
  WHERE samplingevent_id = _element_id
)
UNION
(
  SELECT a.samplingtime_id AS element_id
       , b.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal_time    AS ordinal
       , 7                 AS variabletype_id
  FROM dashin.study_samplingtime          a
    INNER JOIN dashin.study_samplingevent b ON a.samplingevent_id = b.samplingevent_id
  WHERE a.samplingtime_id = _element_id
)
UNION
(
  SELECT a.event_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 4          AS variabletype_id
  FROM dashin.study_event a
  WHERE event_id = _element_id
)
UNION
(
  SELECT a.subevent_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 5             AS variabletype_id
  FROM dashin.study_subevent a
  WHERE subevent_id = _element_id
)
UNION
(
  SELECT a.startgroup_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 3               AS variabletype_id
  FROM dashin.study_startgroup a
  WHERE startgroup_id = _element_id
)
UNION
(
  SELECT a.center_id AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , a.ordinal
       , 8           AS variabletype_id
  FROM dashin.study_center a
  WHERE center_id = _element_id
)
UNION
(
  SELECT a.subject_id   AS element_id
       , a.study_id
       , a.name
       , a.label
       , a.description
       , NULL::INT      AS ordinal
       , 2              AS variabletype_id
  FROM dashin.study_subject a
  WHERE subject_id = _element_id
)
$$;


