CREATE FUNCTION dashin.datadesign_exists
(
    _dataset_id UUID
  , _owner_org  UUID
) RETURNS BOOL
  LANGUAGE sql
AS
$$
SELECT exists(
  SELECT
  FROM dashin.datadesign_datatable dt0
  WHERE dt0.dataset_id = _dataset_id
    AND exists(SELECT FROM dashin.dataset d0 WHERE (d0.dataset_id, d0.owner_org) = (_dataset_id, _owner_org))
  );
$$;
