CREATE FUNCTION dashin.studydesign_generate_full_for_download
(
    _study_id  UUID
  , _owner_org UUID
)
  RETURNS TABLE
          (
            ROW_NUMBER         BIGINT,
            STUDY_ID           UUID,
            EVENT_ID           UUID,
            SUBEVENT_ID        UUID,
            SIMPLINGEVENT_ID   UUID,
            SAMPLINGTIME_ID    UUID,
            STARTGROUP_ID      UUID,
            SUBJECT_ID         UUID,
            STUDY_NAME         TEXT,
            EVENT_NAME         TEXT,
            SUBEVENT_NAME      TEXT,
            SAMPLINGEVENT_NAME TEXT,
            SAMPLINGTIME_NAME  TEXT,
            STARTGROUP_NAME    TEXT,
            SUBJECT_NAME       TEXT
          )
  LANGUAGE sql
  STABLE
AS
$$
SELECT
  row_number() OVER ()
  , *
FROM
  (
    SELECT
      z.study_id
      , a.event_id
      , c.subevent_id
      , e.samplingevent_id
      , f.samplingtime_id
      , h.startgroup_id
      , i.subject_id
      , z.name AS study_name
      , a.name AS event_name
      , c.name AS subevent_name
      , e.name AS samplingevent_name
      , f.name AS samplingtime_name
      , h.name AS startgroup_name
      , i.name AS subject_name
    FROM
      dashin.study                                               z
        LEFT JOIN dashin.study_event                       a ON z.study_id = a.study_id
        LEFT JOIN dashin.study_event_x_subevent            b ON a.event_id = b.event_id
        LEFT JOIN dashin.study_subevent                    c ON b.subevent_id = c.subevent_id
        LEFT JOIN dashin.study_subevent_x_samplingevent    d ON c.subevent_id = d.subevent_id
        LEFT JOIN dashin.study_samplingevent               e ON d.samplingevent_id = e.samplingevent_id
        LEFT JOIN dashin.study_samplingtime                f ON e.samplingevent_id = f.samplingevent_id
        LEFT JOIN dashin.study_event_subevent_x_startgroup g ON b.event_id = g.event_id AND b.subevent_id = g.subevent_id
        LEFT JOIN dashin.study_startgroup                  h ON g.startgroup_id = h.startgroup_id
        LEFT JOIN dashin.study_subject                     i ON h.startgroup_id = i.startgroup_id
    WHERE
      (z.study_id, z.owner_org) = (_study_id, _owner_org)
  ) x0;
$$
;
