CREATE FUNCTION dashin.studydesign_delete_design
(
    _study_id        UUID
  , _owner_org       UUID DEFAULT NULL
  , _delete_subjects BOOL DEFAULT FALSE
)
  RETURNS TABLE
          (
            ELEMENT_NAME  TEXT,
            AFFECTED_ROWS INT
          )
  LANGUAGE plpgsql
AS
$$
DECLARE
  event                       INT;
  subevent                    INT;
  samplingevent               INT;
  center                      INT;
  startgroup                  INT;
  subject                     INT;
  samplingtime                INT;
  event_x_subevent            INT;
  event_subevent_x_startgroup INT;
  subevent_x_samplingevent    INT;
  datadesign                  INT;
  study_exists                INT;
BEGIN
  
  --   @formatter:off
  study_exists := (SELECT count(*) FROM dashin.study st WHERE (st.study_id,st.owner_org) = (_study_id,_owner_org));
  IF study_exists != 1 THEN 
    RETURN QUERY VALUES 
    ('no_study',0);
    RETURN;
  END IF;
  
  UPDATE dashin.datadesign_x_study_event SET (study_id,study_event_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_subevent SET (study_id,study_subevent_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_samplingevent SET (study_id,study_samplingevent_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_samplingtime SET (study_id,study_samplingtime_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_center SET (study_id,study_center_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_startgroup SET (study_id,study_startgroup_id) = (NULL,NULL) WHERE study_id = _study_id;
  UPDATE dashin.datadesign_x_study_subject SET (study_id,study_subject_id) = (NULL,NULL) WHERE study_id = _study_id;
 
  -- -------------------------------------------------------------------------------------------
  -- event_subevent_x_startgroup
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event_subevent_x_startgroup WHERE study_id = _study_id;
  GET DIAGNOSTICS event_subevent_x_startgroup = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- subevent_x_samplingevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_subevent_x_samplingevent WHERE study_id = _study_id;
  GET DIAGNOSTICS subevent_x_samplingevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- event_x_subevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event_x_subevent WHERE study_id = _study_id;
  GET DIAGNOSTICS event_x_subevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- event
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_event WHERE study_id = _study_id;
  GET DIAGNOSTICS event = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- subevent
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_subevent WHERE study_id = _study_id;
  GET DIAGNOSTICS subevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- samplingtime get count - deleted by FK cascade
  -- -------------------------------------------------------------------------------------------
  samplingtime := (SELECT count(*) FROM dashin.study_samplingtime
                   WHERE samplingevent_id IN (SELECT se.samplingevent_id
                                              FROM dashin.study_samplingevent se WHERE se.study_id = _study_id));
  
  -- -------------------------------------------------------------------------------------------
  -- samplingevent - deletes samplingtime by FK cascade
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_samplingevent WHERE study_id = _study_id;
  GET DIAGNOSTICS samplingevent = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- do not automatically delete subjects
  -- -------------------------------------------------------------------------------------------
  IF _delete_subjects THEN
    DELETE FROM dashin.study_subject WHERE study_id = _study_id;
    GET DIAGNOSTICS subject = ROW_COUNT;
  ELSE
    -- cannot set NULL on delete, so do it explicitly before delete
    UPDATE dashin.study_subject SET startgroup_id = NULL WHERE study_id = _study_id;
    subject := 0;
  END IF;
  
  -- can only delete center if subjects have been deleted
  IF _delete_subjects then
    DELETE FROM dashin.study_center WHERE study_id = _study_id;
    get DIAGNOSTICS center = ROW_COUNT;
  ELSE
    center := 0;
  END IF;
  
  -- -------------------------------------------------------------------------------------------
  -- startgroup
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.study_startgroup WHERE study_id = _study_id;
  GET DIAGNOSTICS startgroup = ROW_COUNT;
  
  -- -------------------------------------------------------------------------------------------
  -- datadesign
  -- -------------------------------------------------------------------------------------------
  DELETE FROM dashin.studydesign_x_datadesign                WHERE study_id = _study_id;
  GET DIAGNOSTICS datadesign                  = ROW_COUNT;
  --   @formatter:on
  RETURN QUERY
    SELECT *
    FROM (
           VALUES ('event', event)
                , ('subevent', subevent)
                , ('event_subevent_x_startgroup', event_subevent_x_startgroup)
                , ('subevent_x_samplingevent', subevent_x_samplingevent)
                , ('event_x_subevent', event_x_subevent)
                , ('samplingtime', samplingtime)
                , ('samplingevent', samplingevent)
                , ('subject', subject)
                , ('center', center)
                , ('startgroup', startgroup)
                , ('datadesign', datadesign)
         ) t(name, n);
END
$$;
