CREATE TABLE dashin.upvar_dataset
(
  upvar_dataset_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT upvar_dataset_pk
      PRIMARY KEY,
  name             TEXT,
  description      TEXT,
  status           SMALLINT DEFAULT 1,
  owner_org        UUID,
  owner_user       UUID,
  created_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details          JSONB
);

ALTER TABLE dashin.upvar_dataset OWNER TO :application_user;
