CREATE TABLE dashin.unit_conversion_factor
(
  conventional_id   INT,
  si_id             INT,
  conversion_factor NUMERIC
);
ALTER TABLE dashin.unit_conversion_factor OWNER TO :application_user;
