CREATE TABLE dashin.studyrowsubset
(
  studyrowsubset_id UUID CONSTRAINT studyrowsubset_pk DEFAULT uuid_generate_v4() NOT NULL,
  studyrowset_id    UUID,
  study_id          UUID,
  name              TEXT,
  description       TEXT,
  CONSTRAINT studyrowsubset_subset_x_varset_uk
    UNIQUE (studyrowsubset_id, study_id)
);

ALTER TABLE dashin.studyrowsubset OWNER TO :application_user;
