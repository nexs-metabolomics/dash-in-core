CREATE TABLE dashin.unitprefix
(
  unitprefix_id     INT PRIMARY KEY,
  unitprefixdomain_id INT,
  ordinal           INT,
  symbol            TEXT,
  numeric_label     TEXT,
  name              TEXT,
  description       TEXT,
  value             NUMERIC

);

ALTER TABLE dashin.unitprefix OWNER TO :application_user;
