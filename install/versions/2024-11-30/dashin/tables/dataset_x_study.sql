CREATE TABLE dashin.dataset_x_study
(
  dataset_id UUID NOT NULL
    CONSTRAINT dataset_x_study_pk
      PRIMARY KEY,
  study_id   UUID NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  -- target constraint for studyrow_x_datarow_dataset_fk on dashin.studyrow_x_datarow
  CONSTRAINT dataset_x_study_uk
    UNIQUE (dataset_id, study_id)
);

ALTER TABLE dashin.dataset_x_study OWNER TO :application_user;
