CREATE TABLE dashin.var_variable_x_search_column_type
(
  search_column_type_id INTEGER NOT NULL,
  var_variable_id       UUID NOT NULL,
  dataset_id            UUID,
  var_dataset_id        UUID,
  CONSTRAINT var_variable_x_search_column_type_pk
    PRIMARY KEY (var_variable_id)
);

ALTER TABLE dashin.var_variable_x_search_column_type OWNER TO :application_user;

