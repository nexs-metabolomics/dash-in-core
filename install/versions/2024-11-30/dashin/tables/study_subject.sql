CREATE TABLE dashin.study_subject
(
  subject_id    UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT subject_pk
      PRIMARY KEY,
  study_id      UUID NOT NULL,
  center_id     UUID,
  startgroup_id UUID,
  name  TEXT NOT NULL,
  label         TEXT,
  description   TEXT,
  status        INTEGER DEFAULT 1,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT now(),
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT now(),
  details       JSONB,
  CONSTRAINT study_subject_study_id_name_uk UNIQUE (study_id, center_id, name)
);

ALTER TABLE dashin.study_subject OWNER TO :application_user;
