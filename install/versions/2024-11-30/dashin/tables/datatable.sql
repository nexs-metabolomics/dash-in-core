CREATE TABLE dashin.datatable
(
  datarow_id   UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT datatable_pk
      PRIMARY KEY,
  dataset_id   UUID,
  subject_name TEXT,
  ordinal      INTEGER,
  status       SMALLINT DEFAULT 1,
  created_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at   TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details      JSONB,
  datarow      JSONB,
  CONSTRAINT datatable_unique_uk UNIQUE (datarow_id, dataset_id)
);

ALTER TABLE dashin.datatable OWNER TO :application_user;
