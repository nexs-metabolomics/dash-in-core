CREATE TABLE dashin.datadesign_datatable
(
  datarow_id           UUID NOT NULL CONSTRAINT datadesign_datatable_pk PRIMARY KEY,
  dataset_id           UUID,
  event_id             UUID,
  subevent_id          UUID,
  samplingevent_id     UUID,
  samplingtime_id      UUID,
  center_id            UUID,
  startgroup_id        UUID,
  subject_id           UUID,
  event_name           TEXT,
  subevent_name        TEXT,
  samplingevent_name   TEXT,
  samplingtime_name    TEXT,
  center_name          TEXT,
  startgroup_name      TEXT,
  subject_name         TEXT,
  event_status         INTEGER,
  subevent_status      INTEGER,
  samplingevent_status INTEGER,
  samplingtime_status  INTEGER,
  center_status        INTEGER,
  startgroup_status    INTEGER,
  subject_status       INTEGER,
  -- used by studyrow_x_datarow
  CONSTRAINT datadesign_datatable_dataset_datarow_uk UNIQUE (dataset_id, datarow_id)
);

ALTER TABLE dashin.datadesign_datatable OWNER TO :application_user;

