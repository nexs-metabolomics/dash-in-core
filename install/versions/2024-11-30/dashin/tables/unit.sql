CREATE TABLE dashin.unit
(
  unit_id       INT PRIMARY KEY ,
  unitdomain_id INT,
  ordinal       INT,
  symbol        TEXT,
  name          TEXT,
  description   TEXT
);

ALTER TABLE dashin.unit OWNER TO :application_user;

