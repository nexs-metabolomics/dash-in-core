CREATE TABLE dashin.studyrowsubset_x_studyvariablecondition
(
  studyrowsubset_id         UUID NOT NULL,
  studyvariablecondition_id UUID NOT NULL,
  study_id                  UUID,
  CONSTRAINT studyrowsubset_x_studyvariablecondition_pk
    PRIMARY KEY (studyrowsubset_id, studyvariablecondition_id)
);

ALTER TABLE dashin.studyrowsubset_x_studyvariablecondition OWNER TO :application_user;
