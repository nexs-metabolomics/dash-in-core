CREATE TABLE dashin.search_column_type
(
  search_column_type_id  INTEGER NOT NULL
    CONSTRAINT search_column_type_pk
      PRIMARY KEY,
  datatype_id            INT,
  multiple               INTEGER DEFAULT 1,
  researchfield_id       INTEGER,
  search_column_type_key TEXT
    CONSTRAINT search_column_type_search_column_type_key_uk
      UNIQUE,
  name                   TEXT,
  description            TEXT
);

ALTER TABLE dashin.search_column_type OWNER TO :application_user;
