CREATE TABLE dashin.studyvariablecondition
(
  studyvariablecondition_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  studyvariable_id          UUID NOT NULL,
  study_id                  UUID NOT NULL,
  name                      TEXT,
  description               TEXT,
  datatype_id               INT,
  conditionoperator_id      INT NOT NULL DEFAULT 0,
  txtval1                   TEXT,
  txtval2                   TEXT,
  details                   JSONB,
  CONSTRAINT studyvariablecondition_uk UNIQUE (study_id, studyvariablecondition_id)
);

ALTER TABLE dashin.studyvariablecondition OWNER TO :application_user;
