CREATE TABLE admin.template
(
  template_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT template_pkey
      PRIMARY KEY,
  template_type_id INTEGER NOT NULL,
  template_key TEXT DEFAULT ''::TEXT,
  name TEXT,
  description TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details JSONB
);

ALTER TABLE admin.template OWNER TO :application_user;
