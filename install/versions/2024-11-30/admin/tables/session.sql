CREATE TABLE admin.session
(
  session_id VARCHAR(32) NOT NULL
    CONSTRAINT session_pkey
      PRIMARY KEY,
  data TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);

ALTER TABLE admin.session OWNER TO :application_user;
