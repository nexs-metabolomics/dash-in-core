CREATE FUNCTION utils.text_to_numeric
(
      _x             TEXT
    , _empty_as_null BOOL DEFAULT TRUE
) RETURNS NUMERIC
    IMMUTABLE
    LANGUAGE plpgsql
AS
$$
DECLARE
    x  NUMERIC;
    x1 TEXT;
BEGIN
    x1 = CASE -- dot comma or dot dot -> dot is thousand separator, remove dots, change comma to dot
             WHEN _x = '' AND _empty_as_null      THEN NULL
             WHEN _x ~ '(\.[0-9]+,)|(\.[0-9]+\.)' THEN replace(replace(_x , '.' , '') , ',' , '.')
        -- comma dot or comma comma -> comma is thousand separator, remove comma
             WHEN _x ~ '(,[0-9]+\.)|(,[0-9]+,)'   THEN replace(_x , ',' , '')
        -- else replace comma with dot
             ELSE replace(_x , ',' , '.')
         END;
    x = x1:: NUMERIC;
    RETURN x;
EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
END;
$$;