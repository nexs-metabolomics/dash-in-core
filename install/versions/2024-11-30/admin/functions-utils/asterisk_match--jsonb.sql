CREATE FUNCTION utils.asterisk_match
(
  x   JSONB
, col JSONB
) RETURNS BOOLEAN
  STABLE
  LANGUAGE SQL
AS
$$
SELECT
  CASE WHEN col = '["*"]' THEN TRUE
       ELSE col @> x
  END
$$;

ALTER FUNCTION utils.asterisk_match(x JSONB, col JSONB) OWNER TO :application_user;

