\echo '----------------------------------------------------------------'
\echo ' CREATE ADMIN TABLES'
\echo '----------------------------------------------------------------'

CREATE TABLE admin.country
(
  country_id INTEGER NOT NULL
    CONSTRAINT country_pkey
      PRIMARY KEY,
  short_name TEXT,
  name TEXT,
  alpha2 TEXT,
  alpha3 TEXT,
  iso3166 INTEGER DEFAULT 0,
  group1 INTEGER
);

ALTER TABLE admin.country OWNER TO :application_user;
CREATE TABLE admin.access_rule
(
  access_rule_id  SERIAL NOT NULL
    CONSTRAINT access_rule_pkey
      PRIMARY KEY,
  access_rule_key TEXT NOT NULL,
  name            TEXT NOT NULL,
  description     TEXT,
  namespace       TEXT NOT NULL,
  class           TEXT NOT NULL,
  allow           JSONB DEFAULT '[]'::JSONB,
  deny            JSONB DEFAULT '[]'::JSONB,
  details         JSONB
);

ALTER TABLE admin.access_rule OWNER TO :application_user;

CREATE TABLE admin.log
(
  log_id SERIAL NOT NULL,
  user_id UUID,
  route TEXT,
  uri TEXT,
  params JSONB
);

ALTER TABLE admin.log OWNER TO :application_user;
CREATE TABLE admin.manager
(
  user_id UUID NOT NULL,
  organization_id UUID NOT NULL,
  label TEXT DEFAULT ''::TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  roles JSONB DEFAULT '[]'::JSONB,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details JSONB,
  CONSTRAINT manager_pkey
    PRIMARY KEY (organization_id, user_id)
);

ALTER TABLE admin.manager OWNER TO :application_user;

CREATE TABLE admin.organization
(
  organization_id UUID NOT NULL
    CONSTRAINT organization_pkey
      PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT,
  email TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  status INTEGER DEFAULT 1 NOT NULL,
  roles JSONB DEFAULT '[]'::JSONB NOT NULL,
  details JSONB
);

ALTER TABLE admin.organization OWNER TO :application_user;
CREATE TABLE admin.password_reset_token
(
  token UUID NOT NULL
    CONSTRAINT password_reset_token_pkey
      PRIMARY KEY,
  user_id UUID,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  status INTEGER DEFAULT 1,
  details JSONB
);

ALTER TABLE admin.password_reset_token OWNER TO :application_user;
CREATE TABLE admin.role
(
  role_id INTEGER NOT NULL
    CONSTRAINT role_pkey
      PRIMARY KEY,
  name TEXT
    CONSTRAINT role_name_key
      UNIQUE,
  description TEXT,
  label TEXT,
  long_desc TEXT,
  details JSONB,
  status INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  roles_include JSONB DEFAULT '[]'::JSONB NOT NULL,
  roles_exclude JSONB DEFAULT '[]'::JSONB NOT NULL
);

ALTER TABLE admin.role OWNER TO :application_user;
CREATE TABLE admin.roleacl
(
  roleacl_id UUID DEFAULT utils.uuid_generate_v4() NOT NULL
    CONSTRAINT roleacl_pkey
      PRIMARY KEY,
  name TEXT,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  role_id INTEGER NOT NULL,
  namespace TEXT NOT NULL,
  class_allow JSONB DEFAULT '["*"]'::JSONB NOT NULL,
  class_deny JSONB DEFAULT '[]'::JSONB NOT NULL,
  action_allow JSONB DEFAULT '["*"]'::JSONB NOT NULL,
  action_deny JSONB DEFAULT '[]'::JSONB NOT NULL,
  CONSTRAINT roleacl_key
    UNIQUE (role_id, namespace, class_allow, class_deny, action_allow, action_deny)
);

ALTER TABLE admin.roleacl OWNER TO :application_user;
CREATE TABLE admin.session
(
  session_id VARCHAR(32) NOT NULL
    CONSTRAINT session_pkey
      PRIMARY KEY,
  data TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
);

ALTER TABLE admin.session OWNER TO :application_user;
CREATE TABLE admin.template
(
  template_id UUID DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT template_pkey
      PRIMARY KEY,
  template_type_id INTEGER NOT NULL,
  template_key TEXT DEFAULT ''::TEXT,
  name TEXT,
  description TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  details JSONB
);

ALTER TABLE admin.template OWNER TO :application_user;
CREATE TABLE admin.template_type
(
  template_type_id INTEGER NOT NULL
    CONSTRAINT template_type_pkey
      PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT
);

ALTER TABLE admin.template_type OWNER TO :application_user;
CREATE TABLE admin.users
(
  user_id UUID NOT NULL
    CONSTRAINT users_pkey
      PRIMARY KEY,
  code TEXT NOT NULL
    CONSTRAINT users_code_key
      UNIQUE,
  code_canonical TEXT NOT NULL,
  first_name TEXT,
  last_name TEXT,
  title TEXT,
  label TEXT,
  email TEXT,
  email_canonical TEXT,
  status INTEGER DEFAULT 1 NOT NULL,
  password TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  active_org_id UUID,
  details JSONB
);

ALTER TABLE admin.users OWNER TO :application_user;

\echo '----------------------------------------------------------------'
\echo ' ADMIN TABLES CREATED'
\echo '----------------------------------------------------------------'
