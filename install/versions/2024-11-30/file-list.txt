#-------------------------------------------------
# admin
#-------------------------------------------------

# admin schema
admin/other/schema.sql

# admin tables
admin/tables/dbversion.sql
#--
admin/tables/access_rule.sql
admin/tables/role.sql
admin/tables/roleacl.sql
#--
admin/tables/session.sql
admin/tables/log.sql
#--
admin/tables/password_reset_token.sql
#--
admin/tables/template.sql
admin/tables/template_type.sql
#--
admin/tables/country.sql
#--
admin/tables/manager.sql
admin/tables/organization.sql
admin/tables/users.sql

# admin tables permissions
admin/other/permissions.sql

# admin table constraints
admin/constraints/admin-tables-constraints.sql

# utils functions
admin/functions-utils/asterisk_match--jsonb.sql
admin/functions-utils/asterisk_match--text.sql
admin/functions-utils/cast_text_to_numeric--maybe-delete.sql
admin/functions-utils/is_numeric.sql
admin/functions-utils/jsonb_array_a_intersect_b.sql
admin/functions-utils/jsonb_array_a_intersect_range.sql
admin/functions-utils/jsonb_array_a_minus_b.sql
admin/functions-utils/jsonb_array_a_union_b.sql
admin/functions-utils/jsonb_array_elements_by_index.sql
admin/functions-utils/jsonb_array_elements_by_index_text.sql
admin/functions-utils/jsonb_each_by_keys.sql
admin/functions-utils/jsonb_each_by_keys_text.sql
admin/functions-utils/jsonb_object_extract_by_key.sql
admin/functions-utils/jsonb_object_to_array_by_keys.sql
admin/functions-utils/jsonb_set_deep.sql
admin/functions-utils/jsonb_text_to_numeric.sql
admin/functions-utils/jsonb_text_is_numeric.sql
admin/functions-utils/text_is_numeric.sql
admin/functions-utils/text_to_numeric.sql

# admin functions
admin/functions-admin/get_roles.sql
admin/functions-admin/get_manager_roles.sql
admin/functions-admin/get_role_with_children.sql

# admin populate
admin/populate/admin-country-populate.sql
admin/populate/admin-tables-populate.sql



#=================================================
# DASHIN
#=================================================

#-------------------------------------------------
# dashin schema
#-------------------------------------------------
dashin/other/schema.sql

#-------------------------------------------------
# dashin tables
#-------------------------------------------------
dashin/tables/assay.sql
dashin/tables/consortium.sql
dashin/tables/datacolumntype.sql

dashin/tables/datadesign.sql
dashin/tables/datadesign_datatable.sql
dashin/tables/datadesign_x_study_center.sql
dashin/tables/datadesign_x_study_event.sql
dashin/tables/datadesign_x_study_samplingevent.sql
dashin/tables/datadesign_x_study_samplingtime.sql
dashin/tables/datadesign_x_study_startgroup.sql
dashin/tables/datadesign_x_study_subevent.sql
dashin/tables/datadesign_x_study_subject.sql

dashin/tables/supportfile.sql
dashin/tables/study_x_supportfile.sql
dashin/tables/dataset_x_supportfile.sql
dashin/tables/assay_x_supportfile.sql

dashin/tables/datasettype.sql

dashin/tables/dataset.sql
dashin/tables/dataset_x_study.sql
dashin/tables/datatable.sql
dashin/tables/datatype.sql
dashin/tables/intervention_type.sql
dashin/tables/researchfield.sql
dashin/tables/resultset.sql
dashin/tables/resultset_x_variable.sql
dashin/tables/search_column_type.sql
dashin/tables/study.sql
dashin/tables/studycontact.sql
dashin/tables/researchdesign.sql
dashin/tables/study_role.sql
dashin/tables/upvar_dataset.sql
dashin/tables/upvar_datatable.sql
dashin/tables/upvar_variable.sql
dashin/tables/var_dataset.sql
dashin/tables/var_datatable.sql
dashin/tables/var_variable.sql
dashin/tables/variable.sql
dashin/tables/variabletype.sql

dashin/tables/studydesign_sampling_type.sql
dashin/tables/studydesign_subevent_type.sql
dashin/tables/studydesign_x_datadesign.sql

dashin/tables/study_center.sql
dashin/tables/study_event.sql
dashin/tables/study_subevent.sql
dashin/tables/study_samplingevent.sql
dashin/tables/study_samplingtime.sql
dashin/tables/study_startgroup.sql
dashin/tables/study_subject.sql
dashin/tables/study_event_x_subevent.sql
dashin/tables/study_subevent_x_samplingevent.sql
dashin/tables/study_event_subevent_x_startgroup.sql

dashin/tables/studyvariable.sql
dashin/tables/studyvariable_x_variable.sql
dashin/tables/studydataset.sql
dashin/tables/studydatatable.sql
dashin/tables/studyrow_x_datarow.sql

dashin/tables/selection_choice.sql
dashin/tables/selection_type.sql
dashin/tables/var_variable_x_search_column_type.sql

dashin/tables/unit.sql
dashin/tables/unitdomain.sql
dashin/tables/unitprefix.sql
dashin/tables/unitprefixdomain.sql
dashin/tables/unit_conversion_factor.sql
dashin/tables/prefixedunit.sql

dashin/tables/conditionoperator.sql
dashin/tables/studyvariablecondition.sql
dashin/tables/studyvariablecondition_x_category.sql
dashin/tables/studyvariablecondition_x_samplingtimecategory.sql

dashin/tables/studyrowset.sql
dashin/tables/studyrowsubset.sql
dashin/tables/studyrowsubset_x_studyvariablecondition.sql

dashin/tables/studyvariableset.sql
dashin/tables/studyvariableset_x_studyvariable.sql

dashin/tables/studyexportset.sql
# dashin/tables/studyrowset_x_studyvariable.sql

#-------------------------------------------------
# constraints
#-------------------------------------------------
dashin/constraints/dashin-tables-constraints.sql
dashin/constraints/dashin-variable-exclude-constraint.sql

#-------------------------------------------------
# populate
#-------------------------------------------------
dashin/populate/dashin-tables-populate.sql
dashin/populate/dashin-unit-base-tables-populate.sql
dashin/populate/dashin-unit-prefixedunit-populate.sql

#-------------------------------------------------
# dashin functions
#-------------------------------------------------

#--------------------
# must come first
#--------------------
dashin/functions/get_designvariabletype_ids.sql
dashin/functions/datadesign_design_variablenames.sql
dashin/functions/datadesign_variable_get_local_id--int.sql
dashin/functions/datadesign_variable_get_local_id--text.sql
dashin/functions/datadesign_unvalidated_or_datatable.sql
#--------------------

dashin/functions/conditionoperator_sqlcode.sql

dashin/functions/studyrowset_get_studyrows.sql
dashin/functions/studyvariable_data_generate_query.sql
dashin/functions/studyvariable_data.sql
dashin/functions/studyvariable_data_designvariable.sql
dashin/functions/studyvariable_guess_datatype.sql

dashin/functions/datadesign_design_variable_distinct--int.sql
dashin/functions/datadesign_design_variable_distinct--text.sql

dashin/functions/datadesign_delete_design.sql
dashin/functions/datadesign_design_variabletypes_defined_jsonb.sql

dashin/functions/datadesign_design_variabletypes_defined_text.sql
dashin/functions/datadesign_exists.sql
dashin/functions/datadesign_save_design.sql
dashin/functions/datadesign_startgroup_event_sequence_text.sql
dashin/functions/datadesign_subject_event_sequence_text.sql
dashin/functions/datadesign_summary.sql
dashin/functions/datadesign_unvalidated_design_datatable.sql

dashin/functions/generate_dataset_full_as_csv.sql

dashin/functions/studydesign_delete_design.sql
dashin/functions/studydesign_import_design_from_data.sql
dashin/functions/studydesign_validate_data_for_import.sql

dashin/functions/prefixedunit_combined_symbol.sql
dashin/functions/datadesign_subject_x_startgroup_exists.sql
dashin/functions/datadesign_subject_x_center_exists.sql

dashin/functions/move_study_to_organization.sql

dashin/functions/generate_upvar_dataset_data_full_as_csv.sql
dashin/functions/studydesign_generate_full_for_download.sql

#--------------------
# must be loaded in this order
#--------------------
dashin/functions/search_generate_column_query_numeric.sql
dashin/functions/search_metabolomics_generate_core_query.sql
dashin/functions/search_metabolomics_core_result.sql
dashin/functions/search_metabolomics_summary_generate_query.sql
dashin/functions/search_metabolomics_summary.sql
dashin/functions/search_metabolomics_generate_query.sql
dashin/functions/search_metabolomics.sql
#--------------------

dashin/functions/datadesign_event_x_subevent_exists.sql

dashin/functions/studydata_add_dataset_to_study.sql
dashin/functions/studydata_remove_dataset_from_study.sql
dashin/functions/studyvariablecondition_designvar_category_summary.sql
dashin/functions/get_study_element_info.sql

# db-version
admin/populate/admin-dbversion-populate.sql
