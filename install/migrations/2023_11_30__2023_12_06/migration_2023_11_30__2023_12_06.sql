/*
 Summary:

 Add/update datasettype entry for datasettype_id = 1 to the datasettype table
  
 */
DROP PROCEDURE IF EXISTS admin.migration_2023_11_30__2023_12_06;
CREATE PROCEDURE admin.migration_2023_11_30__2023_12_06
(
    _application_user REGROLE
  , _force            BOOL = FALSE
  , _clean            BOOL = TRUE
)
  LANGUAGE plpgsql
AS
$proc$
DECLARE
  _to_version_exists      BOOL;
  _from_version_is_latest BOOL;
  _from_version_string    TEXT = '2023-11-30';
  _to_version_string      TEXT = '2023-12-06';
BEGIN
  
  -- ----------------------------------------------------------------
  -- create new table to keep track of current db version
  -- ----------------------------------------------------------------
  RAISE NOTICE 'creating version table';
  CREATE TABLE IF NOT EXISTS admin.dbversion
  (
    version_string TEXT PRIMARY KEY,
    updated_at     TIMESTAMPTZ DEFAULT now()
  );
  
  -- ----------------------------------------------------------------
  -- check if we are migrating from the correct version
  -- ----------------------------------------------------------------
  _from_version_is_latest := (exists
    (
      SELECT version_string = _from_version_string
      FROM admin.dbversion
      ORDER BY version_string DESC
      LIMIT 1
    ));
  IF NOT _from_version_is_latest  THEN
    RAISE EXCEPTION 'Migration ''%'' has already been applied. Use _force = TRUE to reapply migration',_from_version_string;
  END IF;
  
  -- ----------------------------------------------------------------
  -- check if migration already applied
  -- ----------------------------------------------------------------
  _to_version_exists := (exists
    (
      SELECT
      FROM admin.dbversion
      WHERE version_string = _to_version_string
    ));
  IF _to_version_exists AND NOT _force THEN
    RAISE EXCEPTION 'Migration ''%'' has already been applied. Use _force = TRUE to reapply migration',_to_version_string;
  END IF;
  
  -- ----------------------------------------------------------------
  -- update database version
  -- ----------------------------------------------------------------
  INSERT INTO admin.dbversion (version_string)
  VALUES (_to_version_string)
  ON CONFLICT (version_string) DO UPDATE SET (version_string, updated_at) = (_to_version_string, now());
  
  -- ----------------------------------------------------------------
  -- MIGRATION
  -- ----------------------------------------------------------------
  
  -- ----------------------------------------------------------------
  -- populate datasttype table
  -- ----------------------------------------------------------------
  INSERT INTO dashin.datasettype (datasettype_id, name, description)
  VALUES (1, 'Regular dataset', 'Regular standard type of dataset')
  ON CONFLICT (datasettype_id) DO UPDATE SET (name, description) = (excluded.name, excluded.description);
  
  -- ----------------------------------------------------------------
  -- cleanup
  -- ----------------------------------------------------------------
  IF _clean THEN
    DROP PROCEDURE admin.migration_2023_11_30__2023_12_06(
                                                             REGROLE
                                                           , BOOL
                                                           , BOOL
                                                         );
  END IF;
END;
$proc$;