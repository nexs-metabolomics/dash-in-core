--
/*
 Summary:
 
 A column name was misnamed 'subject_xxcode' by accident it should have been 'subject_name'
 A column 'details' JSONB was missing from 'supportfile' table
 */
DROP PROCEDURE IF EXISTS admin.migration_2023_12_06__2024_11_03;
CREATE PROCEDURE admin.migration_2023_12_06__2024_11_03
(
)
  LANGUAGE plpgsql
AS
$proc$
DECLARE
  _to_version_exists      BOOL;
  _from_version_is_latest BOOL;
  _latest_version_string  TEXT;
  _wrong_column_exists    BOOL;
  _from_version_string    TEXT = '2023-12-06';
  _to_version_string      TEXT = '2024-11-03';
BEGIN
  
  -- ----------------------------------------------------------------
  -- create version table (if it doesn't exist)
  -- ----------------------------------------------------------------
  RAISE NOTICE 'creating version table';
  CREATE TABLE IF NOT EXISTS admin.dbversion
  (
    version_string TEXT PRIMARY KEY,
    updated_at     TIMESTAMPTZ DEFAULT now()
  );
  
  -- ----------------------------------------------------------------
  -- check if we are migrating from the correct version
  -- ----------------------------------------------------------------
  _latest_version_string := (
                              SELECT coalesce(version_string, 'no version')
                              FROM admin.dbversion
                              ORDER BY version_string DESC
                              LIMIT 1
                            );
  _from_version_is_latest := (_from_version_string = _latest_version_string);
  
  IF NOT _from_version_is_latest THEN
    RAISE EXCEPTION $exc$This migration is for migrating from version '%' to version '%'. Your current version is '%'.$exc$, _from_version_string, _to_version_string, _latest_version_string;
  END IF;
  
  -- ----------------------------------------------------------------
  -- check if migration already applied
  -- ----------------------------------------------------------------
  _to_version_exists := (exists
    (
      SELECT
      FROM admin.dbversion
      WHERE version_string = _to_version_string
    ));
  IF _to_version_exists AND _to_version_string = _latest_version_string THEN
    RAISE EXCEPTION $exc$This migration is for migrating from version '%' to version '%'. You are already on version '%'.$exc$, _from_version_string, _to_version_string, _to_version_string;
  END IF;
  
  -- check if the wrong column name exists and correct name doesn't exist
  _wrong_column_exists := (
                            SELECT exists
                                     (
                                       SELECT
                                       FROM information_schema.columns
                                       WHERE column_name = 'subject_xxcode'
                                         AND table_name = 'datatable'
                                         AND table_schema = 'dashin'
                                     )
                              AND NOT exists
                                     (
                                       SELECT
                                       FROM information_schema.columns
                                       WHERE column_name = 'subject_name'
                                         AND table_name = 'datatable'
                                         AND table_schema = 'dashin'
                                     
                                     )
                          );
  IF _wrong_column_exists THEN
    RAISE NOTICE $ntc$Renaming column from 'subject_xxcode' TO 'subject_name'$ntc$;
    ALTER TABLE dashin.datatable RENAME subject_xxcode TO subject_name;
  END IF;
  
  -- add column 'details' to supportfile table
  ALTER TABLE dashin.supportfile ADD COLUMN IF NOT EXISTS details JSONB;
  
  -- ----------------------------------------------------------------
  -- update database version
  -- ----------------------------------------------------------------
  INSERT INTO admin.dbversion (version_string)
  VALUES (_to_version_string)
  ON CONFLICT (version_string) DO UPDATE SET (version_string, updated_at) = (_to_version_string, now());
  
  RAISE NOTICE $ntc$Successfully migrated from version '%' to version '%'$ntc$,_from_version_string, _to_version_string;
  DROP PROCEDURE admin.migration_2023_12_06__2024_11_03;

END;
$proc$;
