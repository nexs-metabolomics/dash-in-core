#!/bin/bash

USAGE_MESSAGE="
    Usage:
    
    migration_2023_05_21__2023_12_06.sh -[fnp]
    
    Arguments:
    
      -p <path-to-parameter-file> (required)
      
      -f: force migration even if already applied (default: don't force migration)
      
      -n: don't delete migration function after migration (default: delete)
"

#-----------------------------------------------------
# arguments for migration functions
#-----------------------------------------------------
# reset getopts environment variable
OPTIND=1

# initialize arguments
ARG_FORCE="FALSE"
ARG_CLEAN="TRUE"
ARG_PARAM_FILE=""

while getopts ":fnp:" opt; do
    case "$opt" in
      p) ARG_PARAM_FILE="$OPTARG"
        ;;
      f) ARG_FORCE="TRUE"
        ;;
      n) ARG_CLEAN="FALSE"
        ;;
      \?) 
        echo -e "$USAGE_MESSAGE"
        exit 0
        ;;
    esac
done

#-----------------------------------------------------
# check if param-file exists
#-----------------------------------------------------
[[ -f "$ARG_PARAM_FILE" ]] || { echo "paramter file '$ARG_PARAM_FILE' could not be found";echo -e "$USAGE_MESSAGE"; exit 0;}

echo "using '$ARG_PARAM_FILE'"
source "$ARG_PARAM_FILE"

# set path to password file
CWD=$(pwd -P)
PGPSS_PATH="$CWD/.pgpass"
echo "creating pg password file '$PGPSS_PATH'"

export PGPASSFILE="$PGPSS_PATH"



# setup pgpass
echo "${HOST}:${PORT}:${SUPER_USER_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}"  > "$PGPSS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}" >> "$PGPSS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${APPLICATION_USER}:${APPLICATION_USER_PASSWORD}" >> "$PGPSS_PATH"
chmod 0600 "$PGPSS_PATH"

# make psql be more quiet
export PGOPTIONS='--client-min-messages=warning'


# prepare migration query
echo "-------------------------------------------------------------------"
echo "Running migration 2023_05_21 -> 2023_12_06"
echo "-------------------------------------------------------------------"
MIGRATION_CALL="CALL admin.migration_2023_05_21__2023_12_06('$APPLICATION_USER',$ARG_FORCE,$ARG_CLEAN);"

echo "migration query:"
echo "$MIGRATION_CALL"

# create migration procedure
psql -v ON_ERROR_STOP=1 -v application_user="$APPLICATION_USER" -U $SUPER_USER -d $APPLICATION_DBNAME -h $HOST -p $PORT -f "migration_2023_05_21__2023_12_06.sql"
# run the procedure
psql -v ON_ERROR_STOP=1 -v application_user="$APPLICATION_USER" -U $SUPER_USER -d $APPLICATION_DBNAME -h $HOST -p $PORT -c "$MIGRATION_CALL"

echo "-------------------------------------------------------------------"
echo "migration 2023_05_21 -> 2023_12_06 complete"
echo "-------------------------------------------------------------------"

rm -f "$PGPASSFILE"
