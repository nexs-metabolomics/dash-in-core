/*
 Summary:

 ancillary files
 Drop tables related to "ancillary" files strategy
 
 datasettype:
 Remove datasettypecateory
 
 Add datasettype entry for datasettype_id = 1 to the datasettype table
 
 support files
 Uploading files related to dataset, study, assay
 Add table to register uploades files for those three object types
  
 */
DROP PROCEDURE IF EXISTS admin.migration_2023_05_21__2023_12_06;
CREATE PROCEDURE admin.migration_2023_05_21__2023_12_06
(
    _application_user REGROLE
  , _force            BOOL = FALSE
  , _clean            BOOL = TRUE
)
  LANGUAGE plpgsql
AS
$proc$
DECLARE
  _version_exists BOOL;
  _version_string TEXT = '2023-12-06';
BEGIN
  
  -- ----------------------------------------------------------------
  -- create new table to keep track of current db version
  -- ----------------------------------------------------------------
  RAISE NOTICE 'creating version table';
  CREATE TABLE IF NOT EXISTS admin.dbversion
  (
    version_string TEXT PRIMARY KEY,
    updated_at     TIMESTAMPTZ DEFAULT now()
  );
  
  -- ----------------------------------------------------------------
  -- check if migration already applied
  -- ----------------------------------------------------------------
  _version_exists := (exists
    (
      SELECT
      FROM admin.dbversion
      WHERE version_string = _version_string
    ));
  IF _version_exists AND NOT _force THEN
    RAISE EXCEPTION 'Migration ''%'' has already been applied. Use _force = TRUE to reapply migration',_version_string;
  END IF;
  
  -- ----------------------------------------------------------------
  -- update database version
  -- ----------------------------------------------------------------
  INSERT INTO admin.dbversion (version_string)
  VALUES (_version_string)
  ON CONFLICT (version_string) DO UPDATE SET (version_string, updated_at) = (_version_string, now());
  
  -- ----------------------------------------------------------------
  -- MIGRATION
  -- ----------------------------------------------------------------
  
  -- ----------------------------------------------------------------
  -- drop unused tables and columns in relation
  -- to special type datasets.
  -- We upload simple files instead
  -- ----------------------------------------------------------------
  RAISE NOTICE 'Dropping ancillary tables';
  
  DROP TABLE IF EXISTS dashin.dataset_x_ancillarydataset;
  
  DROP TABLE IF EXISTS dashin.study_x_ancillarydataset;
  
  ALTER TABLE dashin.datasettype DROP CONSTRAINT IF EXISTS datasettype_datasettypecategory_fk;
  
  ALTER TABLE dashin.datasettype DROP COLUMN IF EXISTS datasettypecategory_id;
  
  DROP TABLE IF EXISTS dashin.datasettypecategory;
  
  -- ----------------------------------------------------------------
  -- populate datasttype table
  -- ----------------------------------------------------------------
  INSERT INTO dashin.datasettype (datasettype_id, name, description)
  VALUES (1, 'Regular dataset', 'Regular standard type of dataset')
  ON CONFLICT (datasettype_id) DO UPDATE SET (name,description) = (excluded.name,excluded.description);
  
  -- ----------------------------------------------------------------
  -- add tables in relation to supportfiles
  -- ----------------------------------------------------------------
  CREATE TABLE IF NOT EXISTS dashin.supportfile
  (
    supportfile_id  UUID DEFAULT uuid_generate_v4() NOT NULL
      CONSTRAINT supportfile_pk PRIMARY KEY,
    organization_id UUID,
    name            TEXT,
    description     TEXT,
    filename        TEXT,
    fileextension   TEXT,
    mimetype        TEXT,
    updated_at      TIMESTAMP WITH TIME ZONE DEFAULT now(),
    created_at      TIMESTAMP WITH TIME ZONE DEFAULT now()
  );
  
  EXECUTE format('ALTER TABLE dashin.supportfile OWNER TO %s;', _application_user);
  
  -- -----------
  CREATE TABLE IF NOT EXISTS dashin.study_x_supportfile
  (
    study_id       UUID NOT NULL,
    supportfile_id UUID NOT NULL,
    CONSTRAINT study_x_supportfile_pk PRIMARY KEY (study_id, supportfile_id)
  );
  
  EXECUTE format('ALTER TABLE dashin.study_x_supportfile OWNER TO %s;', _application_user);
  
  -- -----------
  CREATE TABLE IF NOT EXISTS dashin.dataset_x_supportfile
  (
    dataset_id     UUID NOT NULL,
    supportfile_id UUID NOT NULL,
    CONSTRAINT dataset_x_supportfile_pk PRIMARY KEY (dataset_id, supportfile_id)
  );
  
  EXECUTE format('ALTER TABLE dashin.dataset_x_supportfile OWNER TO %s;', _application_user);
  
  -- -----------
  CREATE TABLE IF NOT EXISTS dashin.assay_x_supportfile
  (
    assay_id       UUID NOT NULL,
    supportfile_id UUID NOT NULL,
    CONSTRAINT assay_x_supportfile_pk PRIMARY KEY (assay_id, supportfile_id)
  );
  
  EXECUTE format('ALTER TABLE dashin.assay_x_supportfile OWNER TO %s;', _application_user);
  
  -- -------------------------------------------------------
  
  ALTER TABLE dashin.study_x_supportfile
    DROP CONSTRAINT IF EXISTS study_x_supportfile_study_fk;
  
  ALTER TABLE dashin.study_x_supportfile
    ADD CONSTRAINT study_x_supportfile_study_fk
      FOREIGN KEY (study_id) REFERENCES dashin.study
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  ALTER TABLE dashin.study_x_supportfile
    DROP CONSTRAINT IF EXISTS study_x_supportfile_supportfile_fk;
  
  ALTER TABLE dashin.study_x_supportfile
    DROP CONSTRAINT IF EXISTS study_x_supportfile_supportfile_fk;
  
  ALTER TABLE dashin.study_x_supportfile
    ADD CONSTRAINT study_x_supportfile_supportfile_fk
      FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  -- -------------------------------------------------------
  ALTER TABLE dashin.dataset_x_supportfile
    DROP CONSTRAINT IF EXISTS dataset_x_supportfile_study_fk;
  
  ALTER TABLE dashin.dataset_x_supportfile
    ADD CONSTRAINT dataset_x_supportfile_study_fk
      FOREIGN KEY (dataset_id) REFERENCES dashin.dataset
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  ALTER TABLE dashin.dataset_x_supportfile
    DROP CONSTRAINT IF EXISTS dataset_x_supportfile_supportfile_fk;
  
  ALTER TABLE dashin.dataset_x_supportfile
    ADD CONSTRAINT dataset_x_supportfile_supportfile_fk
      FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  -- -------------------------------------------------------
  
  ALTER TABLE dashin.assay_x_supportfile
    DROP CONSTRAINT IF EXISTS assay_x_supportfile_study_fk;
  
  ALTER TABLE dashin.assay_x_supportfile
    ADD CONSTRAINT assay_x_supportfile_study_fk
      FOREIGN KEY (assay_id) REFERENCES dashin.assay
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  ALTER TABLE dashin.assay_x_supportfile
    DROP CONSTRAINT IF EXISTS assay_x_supportfile_supportfile_fk;
  
  ALTER TABLE dashin.assay_x_supportfile
    ADD CONSTRAINT assay_x_supportfile_supportfile_fk
      FOREIGN KEY (supportfile_id) REFERENCES dashin.supportfile
        ON UPDATE CASCADE ON DELETE CASCADE;
  
  -- ----------------------------------------------------------------
  -- cleanup
  -- ----------------------------------------------------------------
  IF _clean THEN
    DROP PROCEDURE admin.migration_2023_05_21__2023_12_06(
                                                             REGROLE
                                                           , BOOL
                                                           , BOOL
                                                         );
  END IF;
END;
$proc$;