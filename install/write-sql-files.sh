#!/bin/bash

VERSION="$1"
echo "version: $VERSION"
SCRIPT_DIR="generated-sql/$VERSION"
mkdir -p "$SCRIPT_DIR"

# function to handle filenames from a file with a list of filenames
handlefiles()(
  [[ -f "$FILE_LIST" ]] || { echo "$FILE_LIST does not exist"; exit 0; }
  while read -r; do
    
    sleep 0.01
    if [[ -f "$INPUT_DIR/$REPLY" ]]; then
      echo "$INPUT_DIR/$REPLY"
      cat "$INPUT_DIR/$REPLY" >> "$OUT_FILE"
      printf ";\n\n-- ----------------------------------------------------------------------------------------\n\n" >> "$OUT_FILE"
    else 
      echo "$INPUT_DIR/$REPLY does not exist"
      exit 1
    fi
    # exclude lines beginning with # and empty lines
  done < <(grep -v -E '^#|^ *$' "$FILE_LIST")
)

INPUT_DIR="versions/$VERSION"
FILE_LIST="$INPUT_DIR/file-list.txt"
OUT_FILE="$SCRIPT_DIR/${VERSION}_run-02.sql"
printf -- "-- ------------------------------------------------------------\n"    > "$OUT_FILE"
printf -- "-- version %s \n" "$VERSION"                                         >> "$OUT_FILE"
printf -- "-- ------------------------------------------------------------\n\n" >> "$OUT_FILE"
handlefiles
cp "$INPUT_DIR/db-sql/01-create-extensions.sql" "$SCRIPT_DIR/${VERSION}_run-01.sql"
