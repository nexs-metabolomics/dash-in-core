#!/bin/bash

#---------------------------------------
# database server address
HOST="localhost"
# database server port
PORT=5432

#---------------------------------------
# superuser (the user who creates the application user and application database)
SUPER_USER="postgres"
# superuser database
SUPER_USER_DBNAME="postgres"
# superuser password
SUPER_USER_PASSWORD="super-user-password"

#---------------------------------------
# application user (the user that the application will use to access the database)
APPLICATION_USER="appuser"
# application database
APPLICATION_DBNAME="appdb"
# application user password
APPLICATION_USER_PASSWORD="appuser-password"

#---------------------------------------
# database version string
DB_VERSION="2023-11-30"


