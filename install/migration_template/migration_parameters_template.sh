#!/bin/bash

#---------------------------------------
# OBS:
# RENAME THIS FILE TO START WITH
#     
#     "parameter"
#     
# in order to have it ignored by git
#
# DO NOT accidentally commit your passwords to git !!
#---------------------------------------

#---------------------------------------
# database host
#---------------------------------------
# database server address
HOST="localhost"
# database server port
PORT=5432

#---------------------------------------
# superuser
#---------------------------------------
# superuser (the user who creates the application user and application database)
SUPER_USER="postgres"
# superuser database
SUPER_USER_DBNAME="postgres"
# superuser password
SUPER_USER_PASSWORD="superuser_secret_password"

#---------------------------------------
# application
#---------------------------------------
# application user (the user that the application will use to access the database)
APPLICATION_USER="appuser"
# application database
APPLICATION_DBNAME="appdb"
# application user password
APPLICATION_USER_PASSWORD="appuser_secret_password"



