#!/bin/bash

MIGRATION_FROM=""
MIGRATION_TO=""
MIGRATION_NAME="migration_${MIGRATION_FROM}__${MIGRATION_TO}"

USAGE_MESSAGE="
    Usage:
    
    ${MIGRATION_NAME}.sh -[p]
    
    Arguments:
    
      -p <path-to-parameter-file> (default path: parameters_${MIGRATION_NAME}.sh)
"

#-----------------------------------------------------
# arguments for migration functions
#-----------------------------------------------------
# reset getopts environment variable
OPTIND=1

# initialize arguments
ARG_PARAM_FILE="parameters_${MIGRATION_NAME}.sh"

while getopts ":p:" opt; do
    case "$opt" in
      p) ARG_PARAM_FILE="$OPTARG"
        ;;
      \?) 
        echo -e "$USAGE_MESSAGE"
        exit 0
        ;;
    esac
done

#-----------------------------------------------------
# check if param-file exists
#-----------------------------------------------------
[[ -f "$ARG_PARAM_FILE" ]] || { echo "paramter file '$ARG_PARAM_FILE' could not be found";echo -e "$USAGE_MESSAGE"; exit 0;}

echo "using '$ARG_PARAM_FILE'"
source "$ARG_PARAM_FILE"

# set path to password file
CWD=$(pwd -P)
PGPSS_PATH="$CWD/.pgpass"
echo "creating pg password file '$PGPSS_PATH'"

export PGPASSFILE="$PGPSS_PATH"



# setup pgpass
echo "${HOST}:${PORT}:${SUPER_USER_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}"  > "$PGPSS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${SUPER_USER}:${SUPER_USER_PASSWORD}" >> "$PGPSS_PATH"
echo "${HOST}:${PORT}:${APPLICATION_DBNAME}:${APPLICATION_USER}:${APPLICATION_USER_PASSWORD}" >> "$PGPSS_PATH"
chmod 0600 "$PGPSS_PATH"

# make psql be more quiet
export PGOPTIONS='--client-min-messages=warning'


# prepare migration query
echo "-------------------------------------------------------------------"
echo "Running migration ${MIGRATION_FROM} -> ${MIGRATION_TO}"
echo "-------------------------------------------------------------------"
MIGRATION_CALL="CALL admin.${MIGRATION_NAME}();"

echo "migration query:"
echo "$MIGRATION_CALL"

# create migration procedure
psql -v ON_ERROR_STOP=1 -v application_user="$APPLICATION_USER" -U $SUPER_USER -d $APPLICATION_DBNAME -h $HOST -p $PORT -f "${MIGRATION_NAME}.sql"
# run the procedure
psql -v ON_ERROR_STOP=1 -v application_user="$APPLICATION_USER" -U $SUPER_USER -d $APPLICATION_DBNAME -h $HOST -p $PORT -c "$MIGRATION_CALL"

echo "-------------------------------------------------------------------"
echo "migration ${MIGRATION_FROM} -> ${MIGRATION_TO} complete"
echo "-------------------------------------------------------------------"

rm -f "$PGPASSFILE"
