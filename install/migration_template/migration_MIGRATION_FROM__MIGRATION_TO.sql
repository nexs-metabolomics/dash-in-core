/*
 summary:
 TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE 
 TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE 
 TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE 
 TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE 
 TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE TEMPLATE 
 */
CREATE PROCEDURE admin.migration_MIGRATION_FROM__MIGRATION_TO()
  LANGUAGE plpgsql
AS
$proc$
DECLARE
  _to_version_exists      BOOL;
  _from_version_is_latest BOOL;
  _latest_version_string  TEXT;
  
  -- ----------------------------------------------------------------
  -- use underscore in version strings
  -- (same format in both version strings and identifiers)
  -- ----------------------------------------------------------------
  _migration_from         TEXT = 'MIGRATION_FROM';
  _migration_to           TEXT = 'MIGRATION_TO';
BEGIN
  
  -- ================================================================================================================================
  -- PREREQUISITES
  -- ================================================================================================================================
  
  -- ----------------------------------------------------------------
  -- create version table (if it doesn't exist)
  -- ----------------------------------------------------------------
  RAISE NOTICE 'creating version table';
  CREATE TABLE IF NOT EXISTS admin.dbversion
  (
    version_string TEXT PRIMARY KEY,
    updated_at     TIMESTAMPTZ DEFAULT now()
  );
  
  -- ----------------------------------------------------------------
  -- check if we are migrating from the correct version (use underscore)
  -- ----------------------------------------------------------------
  _latest_version_string := (
                              SELECT coalesce(replace(version_string, '-', '_'), 'no version')
                              FROM admin.dbversion
                              ORDER BY replace(version_string, '-', '_') DESC
                              LIMIT 1
                            );
  _from_version_is_latest := (_migration_from = _latest_version_string);
  
  IF NOT _from_version_is_latest THEN
    RAISE EXCEPTION $exc$This migration is for migrating from version '%' to version '%'. Your current version is '%'.$exc$, _migration_from, _migration_to, _latest_version_string;
  END IF;
  
  -- ----------------------------------------------------------------
  -- check if migration already applied
  -- ----------------------------------------------------------------
  _to_version_exists := (exists
    (
      SELECT
      FROM admin.dbversion
      WHERE version_string = _migration_to
    ));
  IF _to_version_exists AND _migration_to = _latest_version_string THEN
    RAISE EXCEPTION $exc$This migration is for migrating from version '%' to version '%'. You are already on version '%'.$exc$, _migration_from, _migration_to, _migration_to;
  END IF;
  
  -- ================================================================================================================================
  -- MIGRATION
  -- ================================================================================================================================
  
  -- put migration here  
  
  -- ================================================================================================================================
  -- CLEANUP
  -- ================================================================================================================================
  
  -- ----------------------------------------------------------------
  -- update database version
  -- ----------------------------------------------------------------
  INSERT INTO admin.dbversion (version_string)
  VALUES (_migration_to)
  ON CONFLICT (version_string) DO UPDATE SET (version_string, updated_at) = (_migration_to, now());
  
  RAISE NOTICE $ntc$Successfully migrated from version '%' to version '%'$ntc$,_migration_from, _migration_to;
  
  CALL format($$DROP PROCEDURE admin.migration_%1$s__%2$s$$, _migration_from, _migration_to);

END ;

$proc$;
