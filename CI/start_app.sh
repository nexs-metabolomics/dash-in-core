#!/bin/bash

cd /var/www/html
cat "CI/app-parameters-docker.yml"  | envsubst > "app/config/parameters.yml"

chown www-data:www-data /var/www/html/app/storage

apache2-foreground
