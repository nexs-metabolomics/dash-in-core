#!/bin/bash

cd /dashin
cat "CI/app-parameters-docker.yml"  | envsubst > "app/config/parameters.yml"

docker-entrypoint.sh postgres
