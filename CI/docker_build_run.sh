#!/bin/bash
docker-compose --file CI/docker-compose.yml --project-directory "." pull
docker-compose --file CI/docker-compose.yml --project-directory "." up --build --force-recreate
