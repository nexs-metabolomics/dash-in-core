#!/bin/bash

#----------------------------------------------------
# database server address
#----------------------------------------------------
HOST=172.20.0.2

#----------------------------------------------------
# database server port
#----------------------------------------------------
PORT=5432

#----------------------------------------------------
# superuser (the user who creates the 
# application user and application database)
#----------------------------------------------------
SUPER_USER="postgres"
# superuser database
SUPER_USER_DBNAME="postgres"
# superuser password
SUPER_USER_PASSWORD=$POSTGRES_PASSWORD
#----------------------------------------------------
# application user (the user that the 
# application will use to access the database)
#----------------------------------------------------
APPLICATION_USER=$POSTGRES_APPUSER
# application database
APPLICATION_DBNAME="dashindb"
# application user password
APPLICATION_USER_PASSWORD=$POSTGRES_APPPASSWORD


#---------------------------------------
# database version string
DB_VERSION="2023-11-30"

