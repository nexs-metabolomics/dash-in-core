# DASH-IN data sharing application
This application is programmed in the php language and built with the [phalcon framework](https://phalcon.io/en-us) and uses [PostgreSQL](https://www.postgresql.org/) as database backend.

The easiest way to install it is to use [docker](docs/docker_install.md). You can also install it [directly on a server](docs/manual_install.md).

Before committing code or making pull request please read the [contribution guidelines](docs/CONTRIBUTING.md).

